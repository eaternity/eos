import json
import os

host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("PORT", "8040")

workers = int(os.getenv("NUM_WORKERS", "2"))
accesslog = os.getenv("ACCESS_LOG", None)
errorlog = os.getenv("ERROR_LOG", "-")
graceful_timeout = int(os.getenv("GRACEFUL_TIMEOUT", "300"))
timeout = int(os.getenv("TIMEOUT", "300"))
keepalive_str = os.getenv("KEEP_ALIVE", "5")
loglevel = os.getenv("LOG_LEVEL", "info")
bind = f"{host}:{port}"
worker_tmp_dir = None
if os.path.exists("/dev/shm"):
    worker_tmp_dir = "/dev/shm"
keepalive = int(keepalive_str)


log_data = {
    "loglevel": loglevel,
    "workers": workers,
    "bind": bind,
    "graceful_timeout": graceful_timeout,
    "timeout": timeout,
    "keepalive": keepalive,
    "errorlog": errorlog,
    "accesslog": accesslog,
    "host": host,
    "port": port,
    "worker_tmp_dir": worker_tmp_dir,
}
print(json.dumps(log_data))
