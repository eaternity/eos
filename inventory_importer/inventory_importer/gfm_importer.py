from __future__ import print_function

from typing import Optional

from gap_filling_modules.greenhouse_gfm import GreenhouseGapFillingFactory
from gap_filling_modules.location_gfm import LocationGapFillingFactory
from structlog import get_logger

from core.service.service_provider import ServiceLocator

logger = get_logger()


async def gfm_import_data(countries_alpha3: Optional[list[str]] = None, remove_gadm_files: bool = False) -> None:
    service_locator = ServiceLocator()
    service_provider = service_locator.service_provider

    gap_filling_factory = GreenhouseGapFillingFactory(service_provider.postgres_db, service_provider)
    await gap_filling_factory.import_data()

    gap_filling_factory = LocationGapFillingFactory(service_provider.postgres_db, service_provider)
    await gap_filling_factory.import_data(
        seed_glossary=True, countries_alpha3=countries_alpha3, remove_gadm_files=remove_gadm_files
    )

    logger.info("finished gfm_import_data. shutting down services...")
