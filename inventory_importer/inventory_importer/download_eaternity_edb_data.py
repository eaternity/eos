from __future__ import print_function

import base64
import os
import subprocess
from tempfile import NamedTemporaryFile

from structlog import get_logger

from inventory_importer.settings import Settings

logger = get_logger()


def download_from_github(edb_path: str) -> None:
    """Download the eaternity edb data."""
    os.makedirs(edb_path, exist_ok=True)

    settings = Settings()

    decoded_ssh_key = base64.b64decode(settings.GITHUB_SSH_KEY)

    # Create a temporary file to store the SSH key
    with NamedTemporaryFile(delete=True) as ssh_key_file:
        ssh_key_file.write(decoded_ssh_key)
        ssh_key_file.flush()
        ssh_key_file_path = ssh_key_file.name

        # Set the GIT_SSH_COMMAND environment variable to use the SSH key
        os.environ["GIT_SSH_COMMAND"] = f"ssh -i {ssh_key_file_path} -o StrictHostKeyChecking=no"

        repository_url = "git@github.com:Eaternity/eaternity-edb-data.git"

        eaternity_edb_data_dir = os.path.join(edb_path, "eaternity-edb-data")
        if os.path.exists(eaternity_edb_data_dir) and os.path.isdir(eaternity_edb_data_dir):
            try:
                subprocess.check_output(["git", "-C", eaternity_edb_data_dir, "pull"])
                logger.info("Git pull eaternity-edb-data operation successful.")
            except subprocess.CalledProcessError as e:
                logger.error(f"Error during git pull of eaternity-edb-data: {e}")
        else:
            try:
                # Clone the Git repository
                subprocess.check_output(["git", "clone", repository_url, os.path.join(edb_path, "eaternity-edb-data")])
                logger.info("Git clone eaternity-edb-data successful.")
            except subprocess.CalledProcessError as e:
                logger.error(f"Error during git clone of eaternity-edb-data: {e}")
