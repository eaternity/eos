import base64

from pydantic_settings import SettingsConfigDict

from core.envprops import EnvProps


class Settings(EnvProps):
    AIRTABLE_API_KEY: str = ""
    AIRTABLE_BASE_ID: str = ""
    AIRTABLE_TABLE_ID: str = ""
    AIRTABLE_MATCHING_TABLE_ID: str = ""
    AIRTABLE_VIEW_NAME: str = ""
    EATERNITY_TOKEN: str = "EATERNITY_TOKEN"  # this is NOT base64 encoded
    GITHUB_SSH_KEY: str = ""
    PRODUCTMATCHINGS_COLUMN_NAME: str = "ID Brightway (from Brightway Datasets)"
    FOODEX2_COLUMN_NAME: str = "Name"
    MATCHING_FOODEX2_COLUMN_NAME: str = "Name (from Food Item Glossary - in use)"
    MATCHING_LANGUAGE_COLUMN_NAME: str = "String language"
    MATCHING_NAME_COLUMN_NAME: str = "Ingredient string"
    EDB_ID_COLUMN_NAME: str = "EDB ID (from EDB ID Matchings all)"
    FAO_ID_COLUMN_NAME: str = "FAO Code (from FAO food items all)"
    EATERNITY_DB_EATERNITY_NAMESPACE: str = ""
    NUTRITION_ID_COLUMN_NAME: str = "id (from EDB Nutrients)"
    EDB_GSHEET_ID: str = "1IewpKmOCUDFtxZiVSO_AW1zFq0hM50WIfZjq3asu6ZQ"
    EDB_GSHEET_NAME: str = "EDB_vs_Brightway_ID_2024-03-12"

    @property
    def EATERNITY_AUTH_KEY(self) -> str:
        basic_auth_key = base64.b64encode(f"{self.EATERNITY_TOKEN}:".encode("utf-8"))
        return basic_auth_key.decode("utf-8")

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")
