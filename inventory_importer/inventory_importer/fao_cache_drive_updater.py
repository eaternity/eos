from gap_filling_modules.origin_gfm import OriginGapFillingFactory
from structlog import get_logger

from core.service.service_provider import ServiceLocator


async def upload_file_list_to_drive(origin_gap_filling_factory: OriginGapFillingFactory, filenames: list[str]) -> None:
    drive_service = origin_gap_filling_factory.create_drive_service()
    for filename in filenames:
        origin_gap_filling_factory.upload_cache_file_to_google_drive(drive_service, filename)


async def fao_cache_drive_updater(source_of_source_files: str, upload_files_to_drive: str) -> None:
    logger = get_logger()
    service_locator = ServiceLocator()
    service_provider = service_locator.service_provider

    gap_filling_factory = OriginGapFillingFactory(service_provider.postgres_db, service_provider)
    gap_filling_factory.default_production_fao_code_term = (
        await gap_filling_factory.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            term_xid="200000",
            access_group_uid=str(gap_filling_factory.root_fao_term.access_group_uid),
        )
    )

    fao_snapshot_suffix_in_db = gap_filling_factory.db_fao_snapshot_suffix
    new_fao_snapshot_suffix = gap_filling_factory.origin_gfm_settings.FAO_SNAPSHOT_SUFFIX

    gap_filling_factory.initialize_file_names(new_fao_snapshot_suffix)

    cache_filenames_to_upload = [
        gap_filling_factory.import_export_cache_file_name,
        gap_filling_factory.domestic_cache_file_name,
        gap_filling_factory.itemcodes_cache_file_name,
    ]
    source_filenames_to_upload = [
        gap_filling_factory.import_export_zip_source_file_name,
        gap_filling_factory.domestic_zip_source_file_name,
    ]
    filenames_to_upload = cache_filenames_to_upload + source_filenames_to_upload
    logger.info(f"filenames to upload: {filenames_to_upload}")
    file_hashes_in_database = gap_filling_factory.root_fao_term.data.get("file_hashes", {})
    logger.info(f"file_hashes in the database: {file_hashes_in_database}")

    if upload_files_to_drive:
        # check if the files already exist in the drive
        drive_service = gap_filling_factory.create_drive_service()
        drive_info_by_name = gap_filling_factory.get_drive_file_info_by_name(drive_service)
        if drive_info_by_name:
            filenames_that_exist_in_drive = [filename for filename in drive_info_by_name.keys()]
            file_hashes_in_drive = {filename: file_info["hash"] for filename, file_info in drive_info_by_name.items()}
        else:
            filenames_that_exist_in_drive = []
            file_hashes_in_drive = {}
        logger.info(f"filenames that exist in drive: {filenames_that_exist_in_drive}")
        logger.info(f"file_hashes in the drive: {file_hashes_in_drive}")
        if filenames_that_exist_in_drive:
            if set(filenames_that_exist_in_drive) == set(filenames_to_upload):
                if (
                    fao_snapshot_suffix_in_db == new_fao_snapshot_suffix
                    and file_hashes_in_drive == file_hashes_in_database
                ):
                    logger.info(
                        "All files already exist in the drive and the file hashes match the current file "
                        "hashes in the database. Nothing to do."
                    )
                    return
                else:
                    if fao_snapshot_suffix_in_db != new_fao_snapshot_suffix:
                        info_message = (
                            "the FAO_SNAPSHOT_SUFFIX in the database does not match the current FAO_SNAPSHOT_SUFFIX"
                        )
                    else:
                        info_message = "some file hashes do not match the current file hashes in the database"
                    logger.info(
                        f"All files already exist in the drive but {info_message}. "
                        "We will download the files from the drive and update the snapshot_suffix and the file hashes."
                    )
                    logger.info("start downloading source files from Google Drive")
                    for filename in cache_filenames_to_upload:
                        gap_filling_factory.download_file_from_google_drive(
                            drive_service, filename, drive_info_by_name[filename]["id"]
                        )
                    logger.info("finished downloading source files from Google Drive")
                    logger.info("start to update the FAO-codes in the database")
                    await gap_filling_factory.process_fao_codes_data_from_fao(
                        gap_filling_factory.root_fao_term.uid, gap_filling_factory.root_fao_term.access_group_uid
                    )
                    logger.info("finished updating the FAO-codes in the database")
                    logger.info("start updating the root FAO term")
                    await gap_filling_factory.update_root_fao_term(
                        fao_snapshot_suffix=new_fao_snapshot_suffix, file_hashes=file_hashes_in_drive
                    )
                    logger.info("finished updating the root FAO term")
                    return
            else:
                error_message = (
                    "Some files have different hashes in the drive than in the database: "
                    f"{set(file_hashes_in_database) - set(file_hashes_in_drive)}. "
                    "Make sure that FAO_SNAPSHOT_SUFFIX was correctly set "
                    "(to a new snapshot date) if you want to create a new snapshot."
                )
                logger.error(error_message)
                raise ValueError(error_message)

    if source_of_source_files == "fao":
        logger.info("start downloading FAO source files")
        await gap_filling_factory.download_fao_source_files()
        logger.info("finished downloading FAO source files")
    elif source_of_source_files == "local":
        logger.info("Using local source files")
    else:
        raise NotImplementedError(f"source_of_source_files {source_of_source_files} is not implemented")

    logger.info("start rebuilding FAO cache files")
    await gap_filling_factory.rebuild_fao_cache_files()
    logger.info("finished rebuilding FAO cache files")

    logger.info("start to update the FAO-codes in the database")
    await gap_filling_factory.process_fao_codes_data_from_fao(
        gap_filling_factory.root_fao_term.uid, gap_filling_factory.root_fao_term.access_group_uid
    )
    logger.info("finished updating the FAO-codes in the database")

    new_file_hashes = None

    if upload_files_to_drive:
        logger.info(f"start uploading files {filenames_to_upload} to Google Drive")
        # we rename the files and upload them to gdrive
        await upload_file_list_to_drive(gap_filling_factory, filenames_to_upload)
        logger.info("finished uploading files to Google Drive")

        logger.info("start getting the new file hashes")
        drive_service = gap_filling_factory.create_drive_service()
        drive_info_by_name = gap_filling_factory.get_drive_file_info_by_name(drive_service)
        new_file_hashes = {filename: file_info["hash"] for filename, file_info in drive_info_by_name.items()}
        logger.info(f"new file hashes: {new_file_hashes}")
        logger.info("finished getting the new file hashes")

    logger.info("start updating the root FAO term")
    await gap_filling_factory.update_root_fao_term(
        file_hashes=new_file_hashes, fao_snapshot_suffix=new_fao_snapshot_suffix
    )
    logger.info("finished updating the root FAO term")
