"Upsert Helper."
import uuid

from structlog import get_logger

from core.service.node_service import NodeService

from .node_id_mapper import BwKey, NodeIdMapper

logger = get_logger()


class UpsertHelper:
    def __init__(self):
        # store traversed nodes.
        self.activity_nodes_to_compare: dict[BwKey, dict] = {}
        self.exchange_nodes_to_compare: dict[str, dict] = {}
        self.exchanges_of_activity: dict[str, list[str]] = {}
        self.activity_nodes_to_update: set[BwKey] = set()
        self.delete_system_process_cache: set[str] = set()
        self.system_processes_to_update: set[uuid.UUID] = set()
        self.all_new_nodes: set[uuid.UUID] = set()

    def add_to_compare(self, bw_key: BwKey, raw_input: dict, exchanges: dict) -> None:
        xid = NodeIdMapper.bw_key_to_xid(bw_key)
        self.activity_nodes_to_compare[bw_key] = raw_input
        self.exchange_nodes_to_compare.update(exchanges)
        self.exchanges_of_activity[xid] = list(exchanges.keys())

    def add_activity_to_update(self, bw_key: BwKey) -> None:
        self.activity_nodes_to_update.add(bw_key)

    def add_delete_system_process_cache(self, bw_key: BwKey) -> None:
        xid = NodeIdMapper.bw_key_to_xid(bw_key)
        self.delete_system_process_cache.add(xid)

    def clear_to_compare(self) -> None:
        self.activity_nodes_to_compare = {}
        self.exchange_nodes_to_compare = {}
        self.exchanges_of_activity = {}

    @staticmethod
    def _compare_raw_inputs(cached_raw_input: dict, raw_input: dict) -> bool:
        """Compare cached data with new data.

        In the cached data, tuples are replaced with lists.

        Args:
            cached_raw_input: cached data with all tuples converted to lists.
            raw_input: new data from brightway dump.
        """
        # need to remove "prop_type" from comparison:
        cached_raw_input = {k: v for k, v in cached_raw_input.items() if k != "prop_type" and k != "source_data_raw"}

        def convert_tuples_to_lists(data: dict | tuple | list | str | int | float) -> dict | list:
            if isinstance(data, dict):
                return {key: convert_tuples_to_lists(value) for key, value in data.items()}
            elif isinstance(data, tuple):
                return [convert_tuples_to_lists(item) for item in data]
            elif isinstance(data, list):
                return [convert_tuples_to_lists(item) for item in data]
            else:
                return data

        converted_raw_input = convert_tuples_to_lists(raw_input)
        converted_cached_raw_input = convert_tuples_to_lists(cached_raw_input)
        converted_raw_input_none_removed = {key: val for key, val in converted_raw_input.items() if val is not None}
        converted_cached_raw_input_none_removed = {
            key: val for key, val in converted_cached_raw_input.items() if val is not None
        }

        return converted_cached_raw_input_none_removed == converted_raw_input_none_removed

    async def compare(self, node_id_mapper: NodeIdMapper, node_service: NodeService) -> None:
        logger.debug(f"comparing {len(self.activity_nodes_to_compare)} nodes with the database.")

        for bw_key, raw_input in self.activity_nodes_to_compare.items():
            # First compare all nodes without looking at their exchanges.
            xid = node_id_mapper.bw_key_to_xid(bw_key)
            cached_node = node_service.cache.get_cached_node_by_uid(node_id_mapper[xid])
            cached_raw_input = cached_node.raw_input.model_dump()
            for key in ("production_amount", "activity_location"):
                if key == "production_amount":
                    if hasattr(cached_node, key) and getattr(cached_node, key) is not None:
                        cached_raw_input[key] = getattr(cached_node, key).value
                        if getattr(cached_node, key).unit:
                            cached_raw_input["unit"] = getattr(cached_node, key).unit
                elif key == "activity_location":
                    if hasattr(cached_node, key) and getattr(cached_node, key) is not None:
                        cached_raw_input["location"] = getattr(cached_node, key)

            if not (self._compare_raw_inputs(cached_raw_input, raw_input)):
                self.add_activity_to_update(bw_key)
                continue

            exchanges_to_compare = self.exchanges_of_activity[xid]
            cached_exchanged_count = await self.get_exchange_count_of_activity(node_id_mapper, node_service, xid)
            if len(exchanges_to_compare) != cached_exchanged_count:
                self.add_activity_to_update(bw_key)
                continue

            for exchange_xid in exchanges_to_compare:
                try:
                    cached_node = node_service.cache.get_cached_node_by_uid(node_id_mapper[exchange_xid])
                    cached_raw_input = cached_node.raw_input.model_dump()
                    for key in ("amount_in_original_source_unit", "flow_location"):
                        if key == "amount_in_original_source_unit":
                            if hasattr(cached_node, key) and getattr(cached_node, key) is not None:
                                cached_raw_input["amount"] = getattr(cached_node, key).value
                                if getattr(cached_node, key).unit:
                                    cached_raw_input["unit"] = getattr(cached_node, key).unit
                        elif key == "flow_location":
                            if hasattr(cached_node, key) and getattr(cached_node, key) is not None:
                                cached_raw_input["location"] = getattr(cached_node, key)
                    cached_edges = node_service.cache.get_sub_graph_by_uid(node_id_mapper[exchange_xid], max_depth=1)
                    if not len(cached_edges) == 1:
                        logger.debug(
                            "Flow in the database has more than one child activity. Reimporting the parent activity."
                        )
                        self.add_activity_to_update(bw_key)
                        break

                    cached_input_activity = node_service.cache.get_cached_node_by_uid(cached_edges[0].child_uid)

                    # Compare if the input activities of the exchange nodes are the same.
                    # tuple is replaced with a list in the cached data.
                    if not (
                        list(cached_input_activity.raw_input.key)
                        == list(self.exchange_nodes_to_compare[exchange_xid]["input_activity"])
                    ):
                        self.add_activity_to_update(bw_key)
                        break

                    # Compare if the raw_inputs of the exchange nodes are the same.
                    if not (
                        self._compare_raw_inputs(
                            cached_raw_input,
                            self.exchange_nodes_to_compare[exchange_xid]["raw_input"],
                        )
                    ):
                        self.add_activity_to_update(bw_key)
                        break
                except KeyError:
                    self.add_activity_to_update(bw_key)
                    break

        self.clear_to_compare()

    @staticmethod
    async def get_exchange_count_of_activity(node_id_mapper: NodeIdMapper, node_service: NodeService, xid: str) -> int:
        """Count the number of exchanges associated with an activity.

        Args:
            node_id_mapper: Needed for mapping node xid to uid.
            node_service: Needed for getting the child nodes (exchanges).
            xid: xid of the activity of interest.

        Returns:
            int: Number of exchanges associated with an activity.
        """
        uid = node_id_mapper[xid]
        edges = await node_service.get_sub_graph_by_uid(uid, max_depth=1)
        return len([edge for edge in edges if edge.parent_uid == uid])

    def in_delete_system_process_cache(self, item: str or BwKey) -> bool:
        if isinstance(item, str):
            return item in self.delete_system_process_cache
        else:
            xid = NodeIdMapper.bw_key_to_xid(item)
            return xid in self.delete_system_process_cache

    def __len__(self) -> int:
        return len(self.activity_nodes_to_update)
