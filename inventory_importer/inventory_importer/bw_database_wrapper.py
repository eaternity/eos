from typing import TYPE_CHECKING, TypedDict

from bw2data import Database, databases, methods, projects
from structlog import get_logger

from inventory_importer.node_id_mapper import BwKey

if TYPE_CHECKING:
    from bw2data.meta import Databases, Methods


logger = get_logger()


ActivityDict = TypedDict(
    "ActivityDict",
    {
        "comment": str,
        "classifications": list,
        "categories": list,
        "activity type": str,
        "activity": str,
        "database": str,
        "filename": str,
        "location": str,
        "name": str,
        "parameters": list,
        "authors": dict,
        "type": str,
        "reference product": str,
        "flow": str,
        "unit": str,
        "production amount": float,
        "code": str,
        "exchanges": list,
    },
)


def check_local_bw_database() -> "Databases":
    """Checks that we have a local BW database with our EDB export loaded into it."""
    # loads EDB into BW. takes some time at the first time...
    projects.set_current("Base")

    # smoke test to ensure our EDB database 'Base' is here
    pnames = [p.name for p in list(projects)]
    assert "Base" in pnames, "could not find EDB's 'Base' inside of edb_path"

    edb = databases.get("EDB")
    # sometimes I have to rename the folder Base.095a1b43effec73955e31e790438de49 to Base.095a1b43, not sure why...
    assert edb, "can't load EDB database from edb_path."
    assert edb["number"] >= 2, f"can't find products in {edb}"

    return databases


def check_local_bw_method() -> "Methods":
    """Checks that we have a local BW methods with at least IPCC 2013 GWP 100a.

    Must be called after check_local_bw_database since projects.set_current("Base")
    is executed there. Only a warning is written as IPCC 2013 is not needed in
    test_bw_importer.
    """
    ipcc_2013_gwp_100_key = ("IPCC 2013", "climate change", "GWP 100a")
    ipcc_2013_gwp_100 = methods.get(ipcc_2013_gwp_100_key)
    if not ipcc_2013_gwp_100:
        logger.warning(f"can't load {ipcc_2013_gwp_100_key} from edb_path.")
    return methods


class BwDatabaseWrapper:
    def __init__(self):
        self.databases = check_local_bw_database()
        self.methods = check_local_bw_method()

        dbs_to_preload = ["biosphere3", "EDB", "ecoinvent 3.6 cutoff", "Quantis MeatDatasets ecoinventv3.6"]
        self.dbs_blacklist = ["ecoinvent 3.2 cutoff", "ecoinvent 3.3 cutoff"]
        self.loaded_dbs = dict()
        for db_name in dbs_to_preload:
            _db = Database(db_name)
            self.loaded_dbs[db_name] = _db.load()

        self.used_uncached_dbs = set()

    def get_activity(self, key: BwKey) -> ActivityDict:
        db_name = key[0]
        if db_name in self.dbs_blacklist:
            raise KeyError(f"activity {key} is from a blacklisted database {db_name}")

        if db_name in self.loaded_dbs:
            return self.loaded_dbs[db_name][key]
        else:
            _db = Database(db_name)
            act_obj = _db.get(key[1])
            act_dict = act_obj.as_dict()

            # exchanges are stored with foreign key and therefore are still peewee objects, not dicts.
            # convert them to dicts, to be compatible with the in-memory loaded databases:
            act_dict["exchanges"] = [x.as_dict() for x in act_obj.exchanges()]

            # create a warning if a database is used that was not initially loaded into memory:
            if db_name not in self.used_uncached_dbs:
                self.used_uncached_dbs.add(db_name)
                logger.warn(f"using activity from database {db_name} that was not preloaded (cached in memory)")

            return act_dict
