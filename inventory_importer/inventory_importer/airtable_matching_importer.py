import asyncio
import json
import math
import os
import re
import unicodedata
import uuid
from abc import ABC, abstractmethod
from json import JSONDecodeError
from typing import Sequence, Tuple, TypedDict

import numpy as np
import pandas as pd
from pyairtable import Api
from structlog import get_logger

from core.domain.glossary_link import GlossaryLink
from core.domain.matching_item import MatchingItem
from core.domain.term import Term
from core.service.matching_service import UNICODE_NORMALIZATION_FORM
from core.service.service_provider import ServiceLocator, ServiceProvider
from inventory_importer.settings import Settings

logger = get_logger()


class Row(TypedDict):
    brightway_id: str
    edb_id: str
    fao_id: str
    foodex2_terms: list[str]
    nutrition_id: str


class MatchingRow(TypedDict):
    foodex2_terms: list[str]
    language: str
    name: str


class MatchingLinkingImporter:
    def __init__(self, edb_data_path: str):
        service_locator = ServiceLocator()
        self.service_provider = service_locator.service_provider
        self.edb_data_path = edb_data_path
        self.settings = Settings()
        assert self.settings.AIRTABLE_TABLE_ID, "please setup your .env file correctly"
        self.table = Api(self.settings.AIRTABLE_API_KEY).all(
            self.settings.AIRTABLE_BASE_ID,
            self.settings.AIRTABLE_TABLE_ID,
            view=self.settings.AIRTABLE_VIEW_NAME,
        )
        self.root_foodex2_access_group_uid: uuid.UUID | None = None

        self.rows: [Row] = []
        self.matching_rows: [MatchingRow] = []

        for record in self.table:
            record_fields = record.get("fields")
            foodex2_term_xids = self.foodex2_terms_string_processing(
                record_fields.get(self.settings.FOODEX2_COLUMN_NAME, "")
            )
            brightway_id = record_fields.get(self.settings.PRODUCTMATCHINGS_COLUMN_NAME, [None])[0]

            if brightway_id:
                brightway_id = brightway_id.replace('"', "")

            row: Row = {
                "brightway_id": brightway_id,
                # keep as string, because there are some other items in there that are not integers:
                "edb_id": record_fields.get(self.settings.EDB_ID_COLUMN_NAME, [None])[0],
                "fao_id": record_fields.get(self.settings.FAO_ID_COLUMN_NAME, [None])[0],
                "foodex2_terms": foodex2_term_xids,
                "nutrition_id": record_fields.get(self.settings.NUTRITION_ID_COLUMN_NAME, [None])[0],
            }
            self.rows.append(row)

        logger.info(f"read {len(self.rows)} rows from the main airtable")
        assert len(self.rows) > 500, "Expected at least 500 rows"

        assert self.settings.AIRTABLE_MATCHING_TABLE_ID, "please setup your .env file correctly"
        matching_table = Api(self.settings.AIRTABLE_API_KEY).all(
            self.settings.AIRTABLE_BASE_ID,
            self.settings.AIRTABLE_MATCHING_TABLE_ID,
            view=self.settings.AIRTABLE_VIEW_NAME,
        )

        for record in matching_table:
            record_fields = record.get("fields")
            foodex2_term_xids = self.foodex2_terms_string_processing(
                record_fields.get(self.settings.MATCHING_FOODEX2_COLUMN_NAME, [""])[0],
            )

            row: MatchingRow = {
                "foodex2_terms": foodex2_term_xids,
                "language": record_fields.get(self.settings.MATCHING_LANGUAGE_COLUMN_NAME),
                "name": record_fields.get(self.settings.MATCHING_NAME_COLUMN_NAME),
            }
            self.matching_rows.append(row)

        logger.info(f"read {len(self.matching_rows)} rows from the matching airtable")
        assert len(self.matching_rows) > 500, "Expected at least 500 rows"

        # insert edb-id to foodex2-id mapping
        len_matching_rows_before = len(self.matching_rows)
        for record in self.table:
            record_fields = record.get("fields")
            edb_id = record_fields.get(self.settings.EDB_ID_COLUMN_NAME, [None])[0]
            foodex2_term_xids = self.foodex2_terms_string_processing(
                record_fields.get(self.settings.FOODEX2_COLUMN_NAME, ""),
            )
            if edb_id is not None and foodex2_term_xids:
                row: MatchingRow = {
                    "foodex2_terms": foodex2_term_xids,
                    "language": "edb_id",
                    "name": edb_id,
                }
                self.matching_rows.append(row)
        len_matching_rows_after = len(self.matching_rows)
        logger.info(
            f"Read {len_matching_rows_after-len_matching_rows_before} edb-ids from the main airtable "
            "and added them into matching rows"
        )

        self.food_category_linker: FoodCategoryLinker | None = None
        self.critical_product_content_linker: CriticalProductContentLinker | None = None
        self.weight_conversion_linker: WeightConversionLinker | None = None
        self.perishability_linker: PerishabilityLinker | None = None
        self.nutrients_linker: NutrientsLinker | None = None
        self.fao_linker: FaoLinker | None = None
        self.water_scarcity_linker: WaterScarcityLinker | None = None
        self.foodex2_extensions_importer: Foodex2ExtensionsImporter | None = None
        self.subdivision_importer: SubdivisionImporter | None = None

    @staticmethod
    def foodex2_terms_string_processing(foodex2_term_xids_string: str) -> list[str]:
        foodex2_term_xids = []
        # making a clear terms separator & cleaning up
        # note: there are 10 FoodEx2 terms that use " symbol, but they're very unlikely to be used at all
        foodex2_term_xids_string = foodex2_term_xids_string.replace("],", "] | ").replace('"', "")
        foodex2_terms = foodex2_term_xids_string.split(" | ")

        for term in foodex2_terms:
            match = re.match(r"^(.*) \[(.*)]$", term)

            if match:
                foodex2_term_xids.append(match.group(2))
            else:
                continue

        return foodex2_term_xids

    async def init_importers_and_linkers(self) -> None:
        # TODO really get the root foodex2 term
        root_foodex2_term: Term = (await self.service_provider.glossary_service.get_terms_by_xid("A0361"))[0]
        self.root_foodex2_access_group_uid = root_foodex2_term.access_group_uid

        self.foodex2_extensions_importer = Foodex2ExtensionsImporter(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path
        )
        self.nutrients_linker = NutrientsLinker(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path, self.rows
        )
        self.fao_linker = FaoLinker(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path, self.rows
        )
        self.food_category_linker = FoodCategoryLinker(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path, self.rows
        )
        self.water_scarcity_linker = WaterScarcityLinker(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path, self.rows
        )
        self.critical_product_content_linker = CriticalProductContentLinker(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path, self.rows
        )
        self.weight_conversion_linker = WeightConversionLinker(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path, self.rows
        )
        self.perishability_linker = PerishabilityLinker(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path, self.rows
        )
        self.subdivision_importer = SubdivisionImporter(
            self.service_provider, self.root_foodex2_access_group_uid, self.edb_data_path
        )

    async def import_from_airtable(self) -> None:
        await self.init_importers_and_linkers()
        logger.info("Import FoodEx2 extensions")
        await self.foodex2_extensions_importer.import_extensions()
        logger.info("Import matching between name_de / name_en to FoodEx2 terms")
        await self.add_string_matchings()
        logger.info("Import nutrients, then Link FoodEx2 to nutrients")
        await self.nutrients_linker.link_foodex2_to_terms()
        logger.info("Link FoodEx2 to FAO")
        await self.fao_linker.link_foodex2_to_terms()
        logger.info("Import food category tag data, then Link FoodEx2 to food categories")
        await self.food_category_linker.link_foodex2_to_terms()
        logger.info("Import water scarcity data, then Link FoodEx2 to water scarcity")
        await self.water_scarcity_linker.link_foodex2_to_terms()
        logger.info("Import critical product content data, then Link them to FoodEx2")
        await self.critical_product_content_linker.link_foodex2_to_terms()
        logger.info("Import weight conversion data, then Link them to FoodEx2")
        await self.weight_conversion_linker.link_foodex2_to_terms()
        logger.info("Import perishability tag data, then Link FoodEx2 to perishability")
        await self.perishability_linker.link_foodex2_to_terms()
        logger.info("Import subdivision data")
        await self.subdivision_importer.import_subdivisions()

    async def add_string_matchings(self) -> None:
        """Matching btw name (in DE or EN) to FoodEx2 term."""
        matching_cnt = 0

        match_product_name_gfm = "MatchProductName"
        default_eaternity_access_group_uid = (
            await self.service_provider.access_group_service.get_all_access_group_ids_by_namespace(
                self.settings.EATERNITY_NAMESPACE_UUID
            )
        )[0].uid

        existing_matching_items = await self.service_provider.matching_service.pg_matching_mgr.get_matching_items(
            gap_filling_module=match_product_name_gfm
        )
        existing_matching_strings_with_lang = {
            (matching_item.matching_string, matching_item.lang): matching_item.uid
            for matching_item in existing_matching_items
            if matching_item.access_group_uid == default_eaternity_access_group_uid
        }

        for row in self.matching_rows:
            foodex2_term_uuids = [
                (
                    await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                        xid, str(self.root_foodex2_access_group_uid)
                    )
                ).uid
                for xid in row["foodex2_terms"]
            ]
            language = row["language"].lower() if row["language"] else "de"

            name = row["name"]
            if (
                name is not None
                and name != ""
                and foodex2_term_uuids
                and all(foodex2_term_uid is not None for foodex2_term_uid in foodex2_term_uuids)
            ):
                if language == "edb_id":
                    gap_filling_module = "EDB_Temp"
                else:
                    gap_filling_module = match_product_name_gfm
                await self.service_provider.matching_service.put_matching_item(
                    MatchingItem(
                        gap_filling_module=gap_filling_module,
                        matching_string=str(name).strip(),
                        term_uids=[str(foodex2_term_uid) for foodex2_term_uid in foodex2_term_uuids],
                        access_group_uid=default_eaternity_access_group_uid,
                        lang=language,
                    )
                )
                normalized_names = unicodedata.normalize(UNICODE_NORMALIZATION_FORM, str(name).strip().casefold())
                if (normalized_names, language) in existing_matching_strings_with_lang:
                    del existing_matching_strings_with_lang[(normalized_names, language)]
                matching_cnt += 1

            # Remove some of the existing matchings if they are not present in the airtable anymore.

        # Keep data error matchings
        if ("data error", "de") in existing_matching_strings_with_lang:
            del existing_matching_strings_with_lang[("data error", "de")]
        if ("data error", "en") in existing_matching_strings_with_lang:
            del existing_matching_strings_with_lang[("data error", "en")]

        if existing_matching_strings_with_lang:
            for existing_matching, uid in existing_matching_strings_with_lang.items():
                matching_string_to_delete, matching_lang_to_delete = existing_matching
                await self.service_provider.matching_service.delete_matching_item(
                    MatchingItem(
                        uid=uid,
                        gap_filling_module=match_product_name_gfm,
                        matching_string=matching_string_to_delete,
                        access_group_uid=default_eaternity_access_group_uid,
                        lang=matching_lang_to_delete,
                    )
                )

        assert matching_cnt > 500, "Expected at least 500 matchings"
        logger.info(f"added {matching_cnt} matchings")


class GeneralLinker(ABC):
    def __init__(
        self,
        service_provider: ServiceProvider,
        root_foodex2_access_group_uid: uuid.UUID,
        edb_data_path: str,
        rows: [Row],
    ):
        self.service_provider = service_provider
        self.edb_data_path = edb_data_path
        self.rows = rows
        self.settings = Settings()
        self.root_foodex2_access_group_uid: uuid.UUID = root_foodex2_access_group_uid
        self.gfm_name: str = "General"
        self.term_uid_to_xid: dict[uuid.UUID, str] = {}

    @abstractmethod
    async def init_cache(self) -> None:
        pass

    @staticmethod
    def prepare_link_index_lookup(glossary_links: list[GlossaryLink]) -> dict:
        lookup_dict: dict[uuid.UUID, list[int]] = {}
        for idx, glossary_link in enumerate(glossary_links):
            for term in glossary_link.term_uids:
                if term in lookup_dict:
                    lookup_dict[term].append(idx)
                else:
                    lookup_dict[term] = [idx]

        return lookup_dict

    async def link_foodex2_to_terms(self) -> None:
        await self.init_cache()

        existing_glossary_links: list[
            GlossaryLink
        ] = await self.service_provider.glossary_link_service.get_glossary_links_by_gfm(self.gfm_name)
        lookup_glossary_links: dict[uuid.UUID, list[int]] = self.prepare_link_index_lookup(existing_glossary_links)

        glossary_link_uids_to_delete: set[uuid.UUID] = set()

        added_cnt = 0
        total_cnt = 0

        for row in self.rows:
            foodex2_term_xids = row["foodex2_terms"]
            foodex2_terms = [
                (
                    await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                        xid, str(self.root_foodex2_access_group_uid)
                    )
                )
                for xid in foodex2_term_xids
            ]

            if all(term is not None for term in foodex2_terms):
                foodex2_term_uuids = [foodex2_term.uid for foodex2_term in foodex2_terms]
            else:
                foodex2_term_uuids = []

            existing_term_xids_in_db: set[str] = set()
            delete_on_conflict: set[uuid.UUID] = set()

            if foodex2_term_uuids:
                first_term = foodex2_term_uuids[0]
                if first_term in lookup_glossary_links:
                    for potential_link in lookup_glossary_links[first_term]:
                        if set(foodex2_term_uuids) == set(existing_glossary_links[potential_link].term_uids):
                            if existing_glossary_links[potential_link].linked_term_uid in self.term_uid_to_xid:
                                existing_term_xids_in_db.add(
                                    self.term_uid_to_xid[existing_glossary_links[potential_link].linked_term_uid]
                                )
                                delete_on_conflict.add(existing_glossary_links[potential_link].uid)
                            else:
                                glossary_link_uids_to_delete.add(existing_glossary_links[potential_link].uid)

                eos_linked_terms = await self.eos_linked_terms(row)
                if eos_linked_terms:
                    eos_linked_term_xids = {term.xid for term in eos_linked_terms}
                    total_cnt += len(eos_linked_terms)
                    if existing_term_xids_in_db:
                        if existing_term_xids_in_db == eos_linked_term_xids:
                            continue
                        else:
                            glossary_link_uids_to_delete.update(delete_on_conflict)

                    logger.info(
                        f"adding linked {self.gfm_name} terms {eos_linked_term_xids} "
                        f"for foodex2 terms {row['foodex2_terms']}"
                    )
                    for eos_linked_term in eos_linked_terms:
                        await self.service_provider.glossary_link_service.insert_glossary_link(
                            GlossaryLink(
                                gap_filling_module=self.gfm_name,
                                term_uids=foodex2_term_uuids,
                                linked_term_uid=eos_linked_term.uid,
                            )
                        )
                        added_cnt += 1

        if glossary_link_uids_to_delete:
            await self.service_provider.glossary_link_service.delete_glossary_links_by_uids(
                list(glossary_link_uids_to_delete)
            )

        logger.info(f"attached {added_cnt} {self.gfm_name} to terms")
        logger.info(f"total of {total_cnt} {self.gfm_name} links to term")

    @abstractmethod
    async def eos_linked_terms(self, row: Row) -> list:
        return []


class WeightConversionLinker(GeneralLinker):
    def __init__(
        self,
        service_provider: ServiceProvider,
        root_foodex2_access_group_uid: uuid.UUID,
        edb_data_path: str,
        rows: [Row],
    ):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path, rows)
        self.access_group_uid: uuid.UUID | None = None
        self.gfm_name = "UnitWeightConversionGapFillingFactory"
        self.edb_ids_with_unit_weight_or_density: set[int] = set()

    async def init_cache(self) -> None:
        await super().init_cache()
        await self.import_weight_conversion_terms()

    async def import_weight_conversion_terms(self) -> None:
        root_unit_term: Term = self.service_provider.glossary_service.root_subterms["EOS_units"]
        self.access_group_uid = root_unit_term.access_group_uid
        if (
            "EOS_weight_conversion",
            self.access_group_uid,
        ) not in self.service_provider.glossary_service.terms_by_xid_ag_uid:
            weight_conversion_term = Term(
                name="Weight Conversion",
                sub_class_of=root_unit_term.uid,
                data={},
                access_group_uid=self.access_group_uid,
                uid=uuid.uuid4(),
                xid="EOS_weight_conversion",
            )
            await self.service_provider.glossary_service.put_term_by_uid(weight_conversion_term)

        imported_cnt = 0
        for prod_file in os.listdir(os.path.join(self.edb_data_path, "prods")):
            if prod_file.endswith(".json"):
                logger.debug(f"processing weight conversion information in prod_file: {prod_file}")
                prod_json = json.load(open(os.path.join(self.edb_data_path, "prods", prod_file)))
                prod_id = int(prod_json.get("id"))
                prod_name = f"{str(prod_json.get('name', ''))} weight conversion"
                prod_unit_weight = float(prod_json.get("unit-weight", 0.0))
                prod_density = float(prod_json.get("density", 0.0))

                if prod_unit_weight or prod_density:
                    self.edb_ids_with_unit_weight_or_density.add(prod_id)
                    xid = self.weight_conversion_xid(prod_id)
                    xid_ag_uid = (xid, self.access_group_uid)

                    data_dict = {}
                    if prod_density:
                        data_dict["density"] = prod_density
                    if prod_unit_weight:
                        data_dict["unit_weight"] = prod_unit_weight

                    if xid_ag_uid in self.service_provider.glossary_service.terms_by_xid_ag_uid:
                        existing_term: Term = self.service_provider.glossary_service.terms_by_xid_ag_uid[xid_ag_uid]
                        if not (existing_term.data == data_dict and existing_term.name == prod_name):
                            new_term_uid = existing_term.uid
                        else:
                            new_term_uid = None

                        self.term_uid_to_xid[existing_term.uid] = xid

                    else:
                        new_term_uid = uuid.uuid4()
                        self.term_uid_to_xid[new_term_uid] = xid

                    if new_term_uid is not None:
                        new_term = Term(
                            name=prod_name,
                            sub_class_of=root_unit_term.uid,
                            data=data_dict,
                            access_group_uid=self.access_group_uid,
                            uid=new_term_uid,
                            xid=xid,
                        )
                        await self.service_provider.glossary_service.put_term_by_uid(new_term)
                        imported_cnt += 1

        logger.info(f"successfully imported {imported_cnt} weight conversions.")

    async def eos_linked_terms(self, row: dict) -> list[Term]:
        edb_id = row["edb_id"]
        if edb_id and int(edb_id) in self.edb_ids_with_unit_weight_or_density:
            xid = self.weight_conversion_xid(int(edb_id))
            if (xid, self.access_group_uid) in self.service_provider.glossary_service.terms_by_xid_ag_uid:
                return [self.service_provider.glossary_service.terms_by_xid_ag_uid[(xid, self.access_group_uid)]]

    @staticmethod
    def weight_conversion_xid(prod_id: str | int) -> str:
        return f"Weight_conversion_{prod_id}"


class WaterScarcityLinker(GeneralLinker):
    def __init__(
        self,
        service_provider: ServiceProvider,
        root_foodex2_access_group_uid: uuid.UUID,
        edb_data_path: str,
        rows: [Row],
    ):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path, rows)
        self.access_group_uid: uuid.UUID | None = None
        self.gfm_name = "WaterScarcity"
        self.edb_id_to_water_scarcity_id_mapping: dict[int, str] = {}

    async def init_cache(self) -> None:
        await super().init_cache()
        await self.import_water_scarcity()
        self.link_edb_id_to_water_scarcity_id()

    async def import_water_scarcity(self) -> None:
        # get root_nutrients Term uuid
        root_water_scarcity_term: Term = self.service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
        self.access_group_uid = root_water_scarcity_term.access_group_uid

        # read EDB nutrition files
        imported_cnt = 0
        water_scarcity_file_path = os.path.join(self.edb_data_path, "waterfootprint")
        water_scarcity_csv_file = os.path.join(
            water_scarcity_file_path, "EDB_water_expected_wsi_per_kg_country_excel_corrected.csv"
        )

        df = pd.read_csv(water_scarcity_csv_file)
        for _, row in df.iterrows():
            row_dict = {}
            # Initialize an empty dictionary for the current row
            # Iterate through columns with 2-letter country codes
            for column in df.columns:
                if len(column) == 2 and isinstance(column, str):
                    # Extract only float values
                    value = row[column]
                    if pd.notna(value):
                        try:
                            float_value = float(value)
                            if not math.isinf(float_value):
                                row_dict[column] = float_value
                        except ValueError:
                            pass

            if row_dict:
                median_value = np.median(list(row_dict.values()))
                row_dict["median"] = median_value

                # Append the row dictionary to the result list
                xid = row["WS_ID"]
                product_name = row["product"]
                xid_ag_uid = (xid, self.access_group_uid)

                if xid_ag_uid in self.service_provider.glossary_service.terms_by_xid_ag_uid:
                    existing_term: Term = self.service_provider.glossary_service.terms_by_xid_ag_uid[xid_ag_uid]
                    if not (existing_term.data == row_dict and existing_term.name == product_name):
                        new_term_uid = existing_term.uid
                    else:
                        new_term_uid = None

                    self.term_uid_to_xid[existing_term.uid] = xid

                else:
                    new_term_uid = uuid.uuid4()
                    self.term_uid_to_xid[new_term_uid] = xid

                if new_term_uid is not None:
                    new_term = Term(
                        name=product_name,
                        sub_class_of=root_water_scarcity_term.uid,
                        data=row_dict,
                        access_group_uid=self.access_group_uid,
                        uid=new_term_uid,
                        xid=xid,
                    )
                    await self.service_provider.glossary_service.put_term_by_uid(new_term)
                    imported_cnt += 1

        logger.info(f"successfully imported {imported_cnt} water scarcity terms with data.")

    def link_edb_id_to_water_scarcity_id(self) -> None:
        # read EDB nutrition files
        imported_cnt = 0
        for prod_file in os.listdir(os.path.join(self.edb_data_path, "prods")):
            if prod_file.endswith(".json"):
                logger.debug(f"processing water scarcity ID in prod_file: {prod_file}")
                prod_json = json.load(open(os.path.join(self.edb_data_path, "prods", prod_file)))
                prod_id = prod_json.get("id")
                ws_id = prod_json.get("water-scarcity-footprint-id", "")
                if ws_id:
                    self.edb_id_to_water_scarcity_id_mapping[int(prod_id)] = ws_id
                    imported_cnt += 1

        logger.info(f"successfully imported {imported_cnt} links between EDB ID and water scarcity ID.")

    async def eos_linked_terms(self, row: dict) -> list[Term]:
        edb_id = row["edb_id"]
        if edb_id and int(edb_id) in self.edb_id_to_water_scarcity_id_mapping:
            ws_id = self.edb_id_to_water_scarcity_id_mapping[int(edb_id)]
            if (ws_id, self.access_group_uid) in self.service_provider.glossary_service.terms_by_xid_ag_uid:
                return [self.service_provider.glossary_service.terms_by_xid_ag_uid[(ws_id, self.access_group_uid)]]


class NutrientsLinker(GeneralLinker):
    prefix_nutr = "nutr:"

    def __init__(
        self,
        service_provider: ServiceProvider,
        root_foodex2_access_group_uid: uuid.UUID,
        edb_data_path: str,
        rows: [Row],
    ):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path, rows)
        self.access_group_uid: uuid.UUID | None = None
        self.gfm_name = "Nutrients"

    async def init_cache(self) -> None:
        await super().init_cache()
        await self.import_nutrients()

    def nutrition_id_to_xid(self, nutrition_id: str) -> str:
        return f"{self.prefix_nutr}{nutrition_id}"

    async def import_nutrients(self) -> None:
        # get root_nutrients Term uuid
        root_nutrients_term: Term = (await self.service_provider.glossary_service.get_terms_by_xid("Root_Nutrients"))[0]
        root_nutrients_term_uuid = root_nutrients_term.uid
        self.access_group_uid = root_nutrients_term.access_group_uid

        # read EDB nutrition files
        imported_cnt = 0
        for nutr_file in os.listdir(os.path.join(self.edb_data_path, "nutrs")):
            if nutr_file.endswith(".json"):
                logger.debug(f"importing nutr_file: {nutr_file}")
                filepath = os.path.join(self.edb_data_path, "nutrs", nutr_file)
                try:
                    nutr_json = json.load(open(filepath))
                except JSONDecodeError as e:
                    logger.error(f"Error decoding JSON for {filepath}: {e}")
                    raise e
                # importing in original euroFIR format
                data = {"nutr-vals": nutr_json["nutr-vals"], "format": "euroFIR"}
                if "comment" in nutr_json:
                    data["comment"] = nutr_json["comment"]
                if "country" in nutr_json:
                    data["country"] = nutr_json["country"]
                if "original_id" in nutr_json:
                    data["original_id"] = nutr_json["original_id"]  # original id from euroFIR

                xid = self.nutrition_id_to_xid(nutr_json["id"])

                existing_term: Term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                    xid, str(self.access_group_uid)
                )
                if existing_term:
                    if not (existing_term.data == data and existing_term.name == nutr_json["name"]):
                        new_term_uid = existing_term.uid
                    else:
                        new_term_uid = None
                    self.term_uid_to_xid[existing_term.uid] = xid
                else:
                    new_term_uid = uuid.uuid4()
                    self.term_uid_to_xid[new_term_uid] = xid

                if new_term_uid is not None:
                    new_term = Term(
                        name=nutr_json["name"],
                        sub_class_of=root_nutrients_term_uuid,
                        data=data,
                        access_group_uid=self.access_group_uid,
                        uid=new_term_uid,
                        xid=xid,
                    )
                    await self.service_provider.glossary_service.put_term_by_uid(new_term)
                    imported_cnt += 1

        logger.info(f"successfully imported {imported_cnt} nutr files")

    async def eos_linked_terms(self, row: dict) -> list[Term]:
        nutrition_id = row["nutrition_id"]

        if nutrition_id:
            nutrition_xid = self.nutrition_id_to_xid(nutrition_id)
            nutrition_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                nutrition_xid, str(self.access_group_uid)
            )
            if nutrition_term:
                return [nutrition_term]


class FaoLinker(GeneralLinker):
    def __init__(
        self,
        service_provider: ServiceProvider,
        root_foodex2_access_group_uid: uuid.UUID,
        edb_data_path: str,
        rows: [Row],
    ):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path, rows)
        self.access_group_uid: uuid.UUID | None = None
        self.gfm_name = "FAO"

    async def init_cache(self) -> None:
        await super().init_cache()
        root_fao_term: Term = (await self.service_provider.glossary_service.get_terms_by_xid("Root_FAO"))[0]
        self.access_group_uid = root_fao_term.access_group_uid
        subterms = self.service_provider.glossary_service.get_sub_terms_by_uid(root_fao_term.uid, depth=1)
        for term in subterms:
            self.term_uid_to_xid[term.uid] = term.xid

    async def eos_linked_terms(self, row: dict) -> list[Term]:
        fao_id = row["fao_id"]

        if fao_id:
            fao_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                f"{fao_id}", str(self.access_group_uid)
            )
            if fao_term:
                return [fao_term]


class CriticalProductContentLinker(GeneralLinker):
    def __init__(
        self,
        service_provider: ServiceProvider,
        root_foodex2_access_group_uid: uuid.UUID,
        edb_data_path: str,
        rows: [Row],
    ):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path, rows)
        self.access_group_uid: uuid.UUID | None = None
        self.gfm_name = "CriticalProductContent"
        self.edb_ids_with_critical_product_content: set[int] = set()

    async def init_cache(self) -> None:
        await super().init_cache()
        await self.import_critical_product_content_terms()

    async def import_critical_product_content_terms(self) -> None:
        # get root_nutrients Term uuid
        root_critical_product_content_term: Term = self.service_provider.glossary_service.root_subterms[
            "Root_Critical_Product_Content"
        ]
        self.access_group_uid = root_critical_product_content_term.access_group_uid

        # read EDB product files
        imported_cnt = 0
        for prod_file in os.listdir(os.path.join(self.edb_data_path, "prods")):
            if prod_file.endswith(".json"):
                logger.debug(f"processing critical product content in prod_file: {prod_file}")
                prod_json = json.load(open(os.path.join(self.edb_data_path, "prods", prod_file)))
                prod_id = int(prod_json.get("id"))
                prod_name = str(prod_json.get("name", ""))
                prod_contains = prod_json.get("contains", [])

                critical_product_content_percentage: dict[str, float] = {}

                for prod_contains_element in prod_contains:
                    substance = prod_contains_element.get("substance", "")
                    if substance.lower() in ("soy", "palm-oil"):
                        critical_product_content_percentage[substance.lower()] = prod_contains_element.get(
                            "percentage", 0.0
                        )
                if critical_product_content_percentage:
                    self.edb_ids_with_critical_product_content.add(prod_id)
                    xid = self.critical_product_xid(prod_id)
                    xid_ag_uid = (xid, self.access_group_uid)

                    if xid_ag_uid in self.service_provider.glossary_service.terms_by_xid_ag_uid:
                        existing_term: Term = self.service_provider.glossary_service.terms_by_xid_ag_uid[xid_ag_uid]
                        if not (
                            existing_term.data == critical_product_content_percentage
                            and existing_term.name == prod_name
                        ):
                            new_term_uid = existing_term.uid
                        else:
                            new_term_uid = None

                        self.term_uid_to_xid[existing_term.uid] = xid

                    else:
                        new_term_uid = uuid.uuid4()
                        self.term_uid_to_xid[new_term_uid] = xid

                    if new_term_uid is not None:
                        new_term = Term(
                            name=prod_name,
                            sub_class_of=root_critical_product_content_term.uid,
                            data=critical_product_content_percentage,
                            access_group_uid=self.access_group_uid,
                            uid=new_term_uid,
                            xid=xid,
                        )
                        await self.service_provider.glossary_service.put_term_by_uid(new_term)
                        imported_cnt += 1

        logger.info(f"successfully imported {imported_cnt} critical product content with data.")

    async def eos_linked_terms(self, row: dict) -> list[Term]:
        edb_id = row["edb_id"]
        if edb_id and int(edb_id) in self.edb_ids_with_critical_product_content:
            xid = self.critical_product_xid(int(edb_id))
            if (xid, self.access_group_uid) in self.service_provider.glossary_service.terms_by_xid_ag_uid:
                return [self.service_provider.glossary_service.terms_by_xid_ag_uid[(xid, self.access_group_uid)]]

    @staticmethod
    def critical_product_xid(prod_id: str | int) -> str:
        return f"CriticalProduct_{prod_id}"


class PerishabilityLinker(GeneralLinker):
    STABLE = "stable"
    LOW = "low"
    PERISHABLE = "perishable"
    HIGH = "high"
    HIGH_PERISHABLE = "high-perishable"
    HIGHLY_PERISHABLE = "highly-perishable"

    def __init__(
        self,
        service_provider: ServiceProvider,
        root_foodex2_access_group_uid: uuid.UUID,
        edb_data_path: str,
        rows: [Row],
    ):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path, rows)

        self.gfm_name = "Perishability"

        root_perishability_term = self.service_provider.glossary_service.root_subterms.get("EOS_Perishability")
        shelf_stable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_STABLE", root_perishability_term.access_group_uid)
        ]
        assert shelf_stable_term, "Could not find 'shelf-stable' perishability term."

        perishable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_PERISHABLE", root_perishability_term.access_group_uid)
        ]
        assert perishable_term, "Could not find 'perishable' perishability term."

        high_perishable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_HIGH-PERISHABLE", root_perishability_term.access_group_uid)
        ]
        assert high_perishable_term, "Could not find 'perishable' perishability term."

        self.string_to_term_map: dict = {
            self.STABLE: shelf_stable_term,
            self.LOW: perishable_term,
            self.PERISHABLE: perishable_term,
            self.HIGH_PERISHABLE: high_perishable_term,
            self.HIGHLY_PERISHABLE: high_perishable_term,
            self.HIGH: high_perishable_term,
        }
        self.edb_id_to_perishability: dict[int, Term] = self.prepare_edb_id_to_perishability()

    async def init_cache(self) -> None:
        await super().init_cache()

    def prepare_edb_id_to_perishability(self) -> dict[int, Term]:
        # read EDB product files
        edb_id_to_perishability: dict[int, Term] = {}
        imported_cnt = 0
        for prod_file in os.listdir(os.path.join(self.edb_data_path, "prods")):
            if prod_file.endswith(".json"):
                logger.debug(f"processing perishability in prod_file: {prod_file}")
                filepath = os.path.join(self.edb_data_path, "prods", prod_file)
                try:
                    prod_json = json.load(open(filepath))
                except JSONDecodeError as e:
                    logger.error(f"Error decoding JSON for {filepath}: {e}")
                    raise e
                prod_id = prod_json.get("id")
                prod_perishability = prod_json.get("perishability")
                if prod_perishability:
                    if prod_perishability.lower() in self.string_to_term_map:
                        edb_id_to_perishability[int(prod_id)] = self.string_to_term_map[prod_perishability.lower()]
                        imported_cnt += 1
                    else:
                        raise ValueError(f"{prod_perishability} is not a recognized perishability.")

        logger.info(f"successfully imported perishability of {imported_cnt} products")
        return edb_id_to_perishability

    async def eos_linked_terms(self, row: Row) -> list | None:
        edb_id = row["edb_id"]
        if edb_id and int(edb_id) in self.edb_id_to_perishability:
            return [self.edb_id_to_perishability[int(edb_id)]]


class FoodCategoryLinker(GeneralLinker):
    ROOT_FOOD_CATEGORIES = "Root_Food_Categories"
    NUTS_SEEDS = "nuts-seeds"
    OIL_FATS = "oil-fats"
    ANIMAL_BASED = "animal-based"
    GRAINS = "grains"
    PASTA = "pasta"
    BREAD = "bread"
    CEREALS = "cereals"
    RICE = "rice"
    MEAT = "meat"
    FISH = "fish"
    SEAFOOD = "seafood"
    RIPENED = "ripened"
    EGGS = "eggs"
    ALCOHOL = "alcohol"
    MILK = "milk"
    LEGUMES = "legumes"
    FRUITS = "fruits"
    VEGETABLES = "vegetables"
    MUSHROOMS = "mushrooms"
    SUGAR = "sugar"
    SALT = "salt"
    CONDIMENT = "condiment"
    HERBS_SPICES = "herbs-spices"
    FLOWERS = "flowers"
    CONSERV_BAKE = "conserv-bake"
    BEEF = "beef"
    PORK = "pork"
    POULTRY = "poultry"
    DAIRY = "dairy"
    CHEESE = "cheese"
    FRESH = "fresh"
    MILK_DRINKS = "milk-drinks"
    BEVERAGE = "beverage"
    COFFEE = "coffee"
    JUICE = "juice"
    TEA = "tea"
    HERBAL_TEA = "herbal-tea"
    CREAM = "cream"
    YOGHURT = "yoghurt"
    HONEY = "honey"
    PLANT_BASED = "plant-based"
    STARCH = "starch"
    WHOLEGRAIN = "wholegrain"
    PROCMEAT = "procmeat"
    RUMINANT = "ruminant"
    EOS_EATERNITY_FOOD_CATEGORIES = "EOS_Eaternity-food-categories"
    EOS_ANIMAL_PRODUCTS = "EOS_Animal-products"
    EOS_PLANT_BASED_PRODUCTS = "EOS_Plant-based-products"
    tag_to_term_map_gbd: dict[str, str] = {
        FRUITS: "EOS_Diet-low-in-fruits",
        WHOLEGRAIN: "EOS_Diet-low-in-whole-grains",
        NUTS_SEEDS: "EOS_Diet-low-in-nuts-and-seeds",
        MILK: "EOS_Diet-low-in-milk",
        PROCMEAT: "EOS_Diet-high-in-processed-meat",
    }
    tag_to_term_map_categories: dict[str, str] = {
        NUTS_SEEDS: "EOS_Nuts-and-seeds",
        RIPENED: "EOS_Ripened-cheese",
        EGGS: "EOS_Eggs",
        ALCOHOL: "EOS_Alcohol",
        MILK: "EOS_Milk",
        RICE: "EOS_Rice",
        LEGUMES: "EOS_Legumes",
        FRUITS: "EOS_Fruits",
        VEGETABLES: "EOS_Vegetables",
        MUSHROOMS: "EOS_Mushrooms",
        SUGAR: "EOS_Sugar",
        SALT: "EOS_Salt",
        CONSERV_BAKE: "EOS_Preservatives-and-Baking-additives",
        JUICE: "EOS_Juice",
        COFFEE: "EOS_Coffee-beans",
        BEEF: "EOS_Beef-and-veal",
        PORK: "EOS_Pork",
        POULTRY: "EOS_Poultry",
        FRESH: "EOS_Fresh-cheese",
        CREAM: "EOS_Cream",
        YOGHURT: "EOS_Curd-and-yoghurt",
        HONEY: "EOS_Honey",
        PLANT_BASED: "EOS_Plant-based-products",
        ANIMAL_BASED: "EOS_Animal-products",
        DAIRY: "EOS_Dairy",
    }

    def __init__(
        self,
        service_provider: ServiceProvider,
        root_foodex2_access_group_uid: uuid.UUID,
        edb_data_path: str,
        rows: [Row],
    ):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path, rows)
        self.default_eaternity_access_group_uid: str | None = None
        self.food_category_term_cache: dict[str, Term] = {}
        self.animal_product_categories_xids: list[str] = []
        self.plant_product_categories_xids: list[str] = []
        self.main_food_categories_xids: list[str] = []
        self.eaternity_product_categories_xids: list[str] = []
        self.food_categories_dict: dict[int, list[Term]] = {}
        self.gfm_name = "FoodCategories"

    async def init_cache(self) -> None:
        await super().init_cache()
        self.default_eaternity_access_group_uid = (
            await self.service_provider.access_group_service.get_all_access_group_ids_by_namespace(
                self.settings.EATERNITY_NAMESPACE_UUID
            )
        )[0].uid

        async def get_subterms_by_xid(xid: str, depth: int) -> [Term]:
            root_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                xid, self.default_eaternity_access_group_uid
            )
            all_sub_terms = self.service_provider.glossary_service.get_sub_terms_by_uid(root_term.uid, depth=depth)
            return all_sub_terms

        sub_terms = await get_subterms_by_xid(self.ROOT_FOOD_CATEGORIES, depth=5)
        for term in sub_terms:
            self.food_category_term_cache[term.xid] = term
            self.term_uid_to_xid[term.uid] = term.xid

        self.animal_product_categories_xids = [
            term.xid for term in await get_subterms_by_xid(self.EOS_ANIMAL_PRODUCTS, depth=1)
        ]
        self.plant_product_categories_xids = [
            term.xid for term in await get_subterms_by_xid(self.EOS_PLANT_BASED_PRODUCTS, depth=1)
        ]
        self.main_food_categories_xids = [
            term.xid for term in await get_subterms_by_xid(self.EOS_EATERNITY_FOOD_CATEGORIES, depth=1)
        ]
        self.eaternity_product_categories_xids = [
            term.xid for term in await get_subterms_by_xid(self.EOS_EATERNITY_FOOD_CATEGORIES, depth=4)
        ]

        self.link_edb_tags_to_food_categories()

    @staticmethod
    def tags_contain_any(tags: list[str] | set[str], tags_contain: list[str]) -> bool:
        """Returns whether any one of tags_contain is in tags."""
        return any(st in tags for st in tags_contain)

    def edb_tags_to_eos_terms(self, tags: list[str]) -> list[Term]:
        eos_terms = []
        try:
            # Global burden of disease dietary risk factors:
            for tag in tags:
                term_cache_key_gbd = self.tag_to_term_map_gbd.get(tag)
                if term_cache_key_gbd:
                    eos_terms.append(self.food_category_term_cache[term_cache_key_gbd])
            if self.tags_contain_any(tags, [self.VEGETABLES, self.LEGUMES]) and (self.STARCH not in tags):
                eos_terms.append(self.food_category_term_cache["EOS_Diet-low-in-vegetables"])
            if self.tags_contain_any(tags, [self.RUMINANT, self.PORK]) and (self.PROCMEAT not in tags):
                eos_terms.append(self.food_category_term_cache["EOS_Diet-high-in-red-meat"])

            # Eaternity food categories:
            for tag in tags:
                term_cache_key_categories = self.tag_to_term_map_categories.get(tag)
                if term_cache_key_categories:
                    eos_terms.append(self.food_category_term_cache[term_cache_key_categories])
            if (self.OIL_FATS in tags) and (self.ANIMAL_BASED not in tags):
                eos_terms.append(self.food_category_term_cache["EOS_Plant-based-oil"])
            if self.tags_contain_any(tags, [self.GRAINS, self.PASTA, self.BREAD, self.CEREALS]) and (
                self.RICE not in tags
            ):
                eos_terms.append(self.food_category_term_cache["EOS_Grains"])
            if self.tags_contain_any(tags, [self.MEAT, self.FISH]):
                eos_terms.append(self.food_category_term_cache["EOS_Meat-products"])
            if self.tags_contain_any(tags, [self.FISH, self.SEAFOOD]):
                eos_terms.append(self.food_category_term_cache["EOS_Fish-and-seafood"])
            if self.tags_contain_any(tags, [self.CONDIMENT, self.HERBS_SPICES, self.FLOWERS]):
                eos_terms.append(self.food_category_term_cache["EOS_Spices-and-condiments"])
            if self.tags_contain_any(tags, [self.TEA, self.HERBAL_TEA]):
                eos_terms.append(self.food_category_term_cache["EOS_Tea-leaves"])
            if self.OIL_FATS in tags and self.PLANT_BASED not in tags:
                eos_terms.append(self.food_category_term_cache["EOS_Butter"])

        except KeyError as e:
            logger.error(f"Reinitialize EOS terms. Food category term does not exist: {e}")
            raise e

        return eos_terms

    def add_other_categories(self, term_xids: set[str]) -> list[Term]:
        other_food_categories: list[Term] = []
        if not self.tags_contain_any(term_xids, self.main_food_categories_xids):
            other_food_categories.append(self.food_category_term_cache["EOS_Other"])

        if self.EOS_ANIMAL_PRODUCTS in term_xids:
            if not self.tags_contain_any(term_xids, self.animal_product_categories_xids):
                other_food_categories.append(self.food_category_term_cache["EOS_Other-animal-products"])

        if self.EOS_PLANT_BASED_PRODUCTS in term_xids:
            if not self.tags_contain_any(term_xids, self.plant_product_categories_xids):
                other_food_categories.append(self.food_category_term_cache["EOS_Other-plant-based"])
        return other_food_categories

    def parent_food_categories_to_add(self, terms: list[Term], term_xids: set[str]) -> list[Term]:
        parent_terms_to_add_xids: set[str] = set()
        for term in terms:
            parent_term_xid = self.term_uid_to_xid[term.sub_class_of]
            while not (
                parent_term_xid == "EOS_Eaternity-food-categories"
                or parent_term_xid == "EOS_GBD-Positive"
                or parent_term_xid == "EOS_GBD-Negative"
            ):
                if parent_term_xid not in term_xids:
                    parent_terms_to_add_xids.add(parent_term_xid)
                parent_term_xid = self.term_uid_to_xid[self.food_category_term_cache[parent_term_xid].sub_class_of]
        return [self.food_category_term_cache[xid] for xid in parent_terms_to_add_xids]

    def process_conflicting_animal_and_plant(self, terms: [Term], tags: list[str]) -> None:
        """For a conflict (both animal and plant products according to the tags), place it in 'Other' category."""
        indices_to_remove: list[int] = []
        for idx, term in enumerate(terms):
            if term.xid in self.eaternity_product_categories_xids:
                indices_to_remove.append(idx)

        for idx in sorted(indices_to_remove, reverse=True):
            terms.pop(idx)

        if self.ANIMAL_BASED in tags:
            terms.append(self.food_category_term_cache[self.EOS_ANIMAL_PRODUCTS])
            terms.append(self.food_category_term_cache["EOS_Other-animal-products"])
        elif self.PLANT_BASED in tags:
            terms.append(self.food_category_term_cache[self.EOS_PLANT_BASED_PRODUCTS])
            terms.append(self.food_category_term_cache["EOS_Other-plant-based"])
        else:
            terms.append(self.food_category_term_cache["EOS_Other"])

    def link_edb_tags_to_food_categories(self) -> None:
        # read EDB nutrition files
        imported_cnt = 0
        for prod_file in os.listdir(os.path.join(self.edb_data_path, "prods")):
            if prod_file.endswith(".json"):
                logger.debug(f"processing tags in prod_file: {prod_file}")
                prod_json = json.load(open(os.path.join(self.edb_data_path, "prods", prod_file)))
                prod_id = prod_json.get("id")
                prod_tags = prod_json.get("tags", [])
                if prod_tags:
                    prod_tags = prod_tags.split(", ")
                eos_terms = self.edb_tags_to_eos_terms(prod_tags)

                term_xids = {term.xid for term in eos_terms}
                # Add all parent food categories if they are not already present.
                parent_food_categories = self.parent_food_categories_to_add(eos_terms, term_xids)
                eos_terms.extend(parent_food_categories)
                if parent_food_categories:
                    term_xids.update({term.xid for term in parent_food_categories})

                # Process "others" categories
                other_food_categories = self.add_other_categories(term_xids)
                eos_terms.extend(other_food_categories)

                # Handle conflicting plant and animal based categorization
                if self.EOS_ANIMAL_PRODUCTS in term_xids and self.EOS_PLANT_BASED_PRODUCTS in term_xids:
                    self.process_conflicting_animal_and_plant(eos_terms, prod_tags)

                if eos_terms:
                    self.food_categories_dict[int(prod_id)] = eos_terms
                    imported_cnt += 1

        logger.info(f"successfully imported tags of {imported_cnt} products")

    async def eos_linked_terms(self, row: Row) -> list | None:
        edb_id = row["edb_id"]
        if edb_id and int(edb_id) in self.food_categories_dict:
            eos_food_category_terms = self.food_categories_dict[int(edb_id)]
            return eos_food_category_terms


class GenericTermImporter:
    def __init__(self, service_provider: ServiceProvider, root_foodex2_access_group_uid: uuid.UUID, edb_data_path: str):
        self.edb_data_path = edb_data_path
        self.service_provider = service_provider
        self.root_foodex2_access_group_uid = root_foodex2_access_group_uid
        self.parent_term_uuid: uuid.UUID | None = None
        self.root_access_group_uid: uuid.UUID | None = None

    async def upsert_term(self, xid: str, name: str, data: dict) -> bool:
        existing_term: Term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            xid, str(self.root_access_group_uid)
        )

        if existing_term:
            if not (existing_term.data == data and existing_term.name == name):
                new_term_uid = existing_term.uid
            else:
                new_term_uid = None
        else:
            new_term_uid = uuid.uuid4()

        if new_term_uid is not None:
            new_term = Term(
                name=name,
                xid=xid,
                sub_class_of=self.parent_term_uuid,
                data=data,
                access_group_uid=self.root_access_group_uid,
            )
            await self.service_provider.glossary_service.put_term_by_uid(new_term)
            return True

        return False


class Foodex2ExtensionsImporter(GenericTermImporter):
    def __init__(self, service_provider: ServiceProvider, root_foodex2_access_group_uid: uuid.UUID, edb_data_path: str):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path)
        self.extension_json = os.path.join(self.edb_data_path, "foodex2", "foodex2_extensions.json")

    def read_extension_json_file(self) -> Sequence[Tuple[str, str, str, str, dict]]:
        with open(self.extension_json) as f:
            ext_file = json.load(f)
            for ext_entry in ext_file:
                # "name": "xyz - LACTOBACILLUS CASEI",
                # "xid": "EAT-0001",
                # "sub_class_of_name": "44540 - STARTER CULTURES (EFSA FOODEX2)",
                # "sub_class_of_xid": "A048Y",
                # "data": ""
                yield ext_entry["name"], ext_entry["xid"], ext_entry["sub_class_of_xid"], ext_entry["data"]

    async def import_extensions(self) -> None:
        self.root_access_group_uid = self.root_foodex2_access_group_uid
        imported_cnt = 0
        for ext_name, ext_xid, ext_subclass_of_xid, ext_data in self.read_extension_json_file():
            parent_term: Term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                ext_subclass_of_xid, str(self.root_access_group_uid)
            )
            if parent_term is None:
                print(f"WARNING, Can't find parent_term for {ext_subclass_of_xid}")
            else:
                self.parent_term_uuid = parent_term.uid
                imported = await self.upsert_term(ext_xid, ext_name, ext_data)
                if imported:
                    imported_cnt += 1

        logger.info(f"successfully imported {imported_cnt} Foodex2 extension terms")


class SubdivisionImporter(GenericTermImporter):
    def __init__(self, service_provider: ServiceProvider, root_foodex2_access_group_uid: uuid.UUID, edb_data_path: str):
        super().__init__(service_provider, root_foodex2_access_group_uid, edb_data_path)
        self.subdivision_json_file = os.path.join(self.edb_data_path, "nutrient-subdivision.json")

    async def init_cache(self) -> None:
        root_subdivision_term: Term = (
            await self.service_provider.glossary_service.get_terms_by_xid("Root_Nutrients_Subdivision")
        )[0]
        self.parent_term_uuid = root_subdivision_term.uid
        assert self.parent_term_uuid is not None, "Can't find Root_Nutrients_Subdivision"
        self.root_access_group_uid = root_subdivision_term.access_group_uid

    @staticmethod
    def foodex2_fullname2id(fullname: str) -> str | None:
        try:
            start_index = fullname.rindex("[") + 1
            end_index = fullname.rindex("]")
            return fullname[start_index:end_index]
        except ValueError:
            return None

    def read_subdivision_json_file(self) -> Sequence[Tuple[list[str], list[list[[str]]]]]:
        with open(self.subdivision_json_file) as f:
            sub_file = json.load(f)

            for sub_entry in sub_file:
                if "replace_base_id" in sub_entry:
                    base_ids = [self.foodex2_fullname2id(b) for b in sub_entry["replace_base_id"]]
                    replace_by_ids = []
                    max_evaporation = sub_entry.get("max_evaporation")

                    for bb in sub_entry.get("replace_by_cpc_base_ids", []):
                        replace_by_ids.append([self.foodex2_fullname2id(b) for b in bb])

                    yield base_ids, replace_by_ids, max_evaporation, sub_entry

    async def import_subdivisions(self) -> None:
        await self.init_cache()

        imported_cnt = 0

        for base_edbid, by_edbids, max_evaporation, entry in self.read_subdivision_json_file():
            if base_edbid is None:
                print(f"WARNING: skipping subdivision entry {entry} since it does not have valid foodex2 id")
            else:
                # these are EDB ids, we need to convert them to the uuids from foodEx2 Terms
                # FIXME for now only the first foodex2 id used
                base_foodex2_term: Term = (
                    await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                        base_edbid[0], str(self.root_foodex2_access_group_uid)
                    )
                )
                if base_foodex2_term is None:
                    print(f"WARNING, Can't find base_foodex2_uid for {base_edbid} in {entry}")
                else:
                    subdivision_term_data = {}
                    by_foodex2_uids = []

                    # some entries may contain `max_evaporation` only
                    if by_edbids:
                        if all(v is None for vv in by_edbids for v in vv):
                            print(
                                f"WARNING: skipping subdivision entry {entry} "
                                f"since it does not have valid foodex2 ids in `replace_by_cpc_base_ids` field"
                            )

                        for by_edbid in by_edbids:
                            by_foodex2_terms: list[Term] = [
                                await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                                    xid, str(self.root_foodex2_access_group_uid)
                                )
                                for xid in by_edbid
                            ]

                            if any(by_foodex2_term is None for by_foodex2_term in by_foodex2_terms):
                                print(f"WARNING, Can't find by_foodex2_uid for {by_edbid} in {entry}")
                                continue
                            else:
                                by_foodex2_uids.append([str(by_term.uid) for by_term in by_foodex2_terms])

                        assert (
                            len(by_foodex2_uids) >= 2
                        ), f"WARNING: Should have at least 2 ingredients to subdivide, {len(by_foodex2_uids)}, {entry}"

                    subdivision_term_data["subproduct_terms_uuids"] = by_foodex2_uids

                    if max_evaporation is not None:
                        subdivision_term_data["max_evaporation"] = max_evaporation

                    subdivision_xid = str(base_foodex2_term.uid)

                    # FIXME for now only the first foodex2 id used
                    subdivision_name = f"Subdivision of {base_edbid[0]}"

                    imported = await self.upsert_term(subdivision_xid, subdivision_name, subdivision_term_data)
                    if imported:
                        imported_cnt += 1

        logger.info(f"successfully imported {imported_cnt} subdivisions")


if __name__ == "__main__":

    async def main() -> None:
        matching_linking_importer = MatchingLinkingImporter("./temp_data/brightway/eaternity-edb-data")
        await matching_linking_importer.import_from_airtable()

    asyncio.run(main())
