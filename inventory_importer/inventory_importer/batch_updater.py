import uuid

from structlog import get_logger

from core.service.service_provider import ServiceLocator

logger = get_logger()


class BatchUpdater:
    """Deletes and updates EDB's products into our database, in batches (much faster)."""

    def __init__(self):
        service_locator = ServiceLocator()
        self.service_provider = service_locator.service_provider
        self.nodes_to_delete: list[uuid.UUID] = []
        self.namespace_uid_xid_uid_to_delete: list[(str, str, uuid.UUID)] = []
        self.nodes_to_update: set[uuid.UUID] = set()  # Update node and the associated edges.
        self.namespace_uid_xid_uid_to_update: list[(str, str, uuid.UUID)] = []
        self.pruned_nodes_to_cache: list[uuid.UUID] = []

    def insert_node_to_delete(self, uid: uuid.UUID) -> None:
        self.nodes_to_delete.append(uid)

    def insert_node_to_update(self, uid: uuid.UUID) -> None:
        self.nodes_to_update.add(uid)

    def insert_namespace_xid_mappings_to_delete(self, namespace_uid: str, xid: str, uid: uuid.UUID) -> None:
        self.namespace_uid_xid_uid_to_delete.append((namespace_uid, xid, uid))

    def clear(self) -> None:
        self.nodes_to_delete = []
        self.namespace_uid_xid_uid_to_delete = []
        self.nodes_to_update = []
        self.namespace_uid_xid_uid_to_update = []

    async def flush(self) -> None:
        logger.debug(f"deleting {len(self.nodes_to_delete)} nodes from database")
        if len(self.namespace_uid_xid_uid_to_delete):
            logger.info("deleting xid_uid mappings from database")
            await self.service_provider.postgres_db.product_mgr.bulk_delete_xid_uid_mappings(
                self.namespace_uid_xid_uid_to_delete
            )
        if len(self.namespace_uid_xid_uid_to_update):
            logger.info("updating xid_uid mappings in database")
            await self.service_provider.postgres_db.product_mgr.bulk_insert_xid_uid_mappings(
                self.namespace_uid_xid_uid_to_update
            )
        if len(self.nodes_to_update):
            logger.info("updating nodes and edges in database")
            await self.service_provider.node_service.update_many_nodes_and_edges_from_local_cache(
                list(self.nodes_to_update)
            )
        logger.info("updating pruned nodes to cache")
        await self.service_provider.postgres_db.get_gfm_cache_mgr().set_cache_entry_by_gfm_name_and_key(
            gfm_name="InventoryConnectorGapFillingFactory",
            cache_key="pruned_nodes_to_cache",
            cache_data={"uids": [str(pruned_node_uid) for pruned_node_uid in self.pruned_nodes_to_cache]},
            load_on_boot=False,
        )
        if len(self.nodes_to_delete):
            logger.info("deleting nodes and edges from database")
            logger.info(f"deleting {self.nodes_to_delete} nodes from database")
            await self.service_provider.node_service.delete_many_nodes_and_edges(self.nodes_to_delete)
            logger.info("deleted nodes and edges from database")
        self.clear()
