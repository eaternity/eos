import uuid
from typing import Any
from unittest.mock import patch

import pytest
import pytest_asyncio
from deepdiff import DeepDiff

from core.domain.term import Term
from core.service.service_provider import ServiceLocator, ServiceProvider
from database.postgres.postgres_db import PostgresDb
from database.postgres.settings import PgSettings
from inventory_importer.glossary_importer import GlossaryImporter


@pytest_asyncio.fixture
async def setup_services() -> tuple[PostgresDb, ServiceProvider]:
    """Fixture to get a clean (resetted) db for testing."""
    service_locator = ServiceLocator()
    service_locator.recreate()
    service_provider = service_locator.service_provider

    # disable cache invalidation, as resetting the database changes all terms
    async def mock_emit_invalidate(*args: Any, **kwargs: Any) -> None:  # noqa: ANN401
        pass

    with patch.object(service_provider.messaging_service, "emit_invalidate", new=mock_emit_invalidate):
        # reset the database
        postgres_db = service_provider.postgres_db
        await postgres_db.connect(schema="test_pg")
        await postgres_db.reset_db(schema="test_pg")

        await service_provider.messaging_service.start()
        await service_provider.init_caches()
        await service_provider.messaging_service.start_worker_channel()
        postgres_db.pg_term_mgr.insert_instead_of_upsert = False
        yield postgres_db, service_provider
        await service_provider.shutdown_services()


@pytest.mark.asyncio
async def test_that_glossary_stays_the_same_when_upserting_twice(
    setup_services: tuple[PostgresDb, ServiceProvider]
) -> None:
    """Import the test glossary."""
    pg_db, service_provider = setup_services
    schema = "test_pg"

    # get all the terms that are in the database after reset
    all_terms_after_reset = await pg_db.pg_term_mgr.find_all()

    # upsert the glossary for the first time
    await GlossaryImporter().upsert_glossary_and_matching(schema=schema)

    # get all the terms that are in the database after the first upsert
    all_terms_after_first_upsert = await pg_db.pg_term_mgr.find_all()

    # upsert the glossary for the second time
    await GlossaryImporter().upsert_glossary_and_matching(schema=schema)

    # get all the terms that are in the database after the second upsert
    all_terms_after_second_upsert = await pg_db.pg_term_mgr.find_all()

    # check that the database is identical after the reset and the first upsert
    diff_after_reset_and_first_upsert = DeepDiff(all_terms_after_first_upsert, all_terms_after_reset, ignore_order=True)
    assert not diff_after_reset_and_first_upsert

    # check that the database is identical after the first and the second upsert
    diff_after_first_and_second_upsert = DeepDiff(
        all_terms_after_first_upsert, all_terms_after_second_upsert, ignore_order=True
    )
    assert not diff_after_first_and_second_upsert


@pytest.mark.asyncio
async def test_upsert_term(setup_services: tuple[PostgresDb, ServiceProvider]) -> None:
    """Test if upsert_term works as expected."""
    pg_db, service_provider = setup_services

    # get all the terms that are in the database after reset
    all_terms_after_reset = await pg_db.pg_term_mgr.find_all()

    length_term = (await pg_db.pg_term_mgr.get_terms_by_xid("EOS_units_length"))[0]
    settings = PgSettings()
    eaternity_ags = await service_provider.access_group_service.get_all_access_group_ids_by_namespace_xid(
        settings.EATERNITY_NAMESPACE_XID
    )
    default_eaternity_access_group_uid = uuid.UUID(eaternity_ags[0].uid)
    span_term = Term(
        xid="TEST_span",
        name="Span",
        sub_class_of=length_term.uid,
        data={
            "length-in-m": 0.2286,
            "alias": [
                "great span",
                "full span",
            ],
        },
        access_group_uid=default_eaternity_access_group_uid,
    )

    await pg_db.pg_term_mgr.upsert_term(span_term)

    # get all the terms after upsert_term
    all_terms_after_upsert_term = await pg_db.pg_term_mgr.find_all()

    diff_after_reset_and_first_upsert = DeepDiff(all_terms_after_reset, all_terms_after_upsert_term, ignore_order=True)

    assert list(diff_after_reset_and_first_upsert.keys()) == ["iterable_item_added"]

    diff_term_added = diff_after_reset_and_first_upsert["iterable_item_added"]

    assert len(diff_term_added) == 1

    diff_span_term = [diff_term for diff_term in diff_term_added.values() if diff_term.xid == "TEST_span"][0]

    # check that the linking was done correctly
    assert diff_span_term.sub_class_of == length_term.uid

    assert diff_span_term.name == span_term.name
    assert diff_span_term.data == span_term.data
    assert diff_span_term.access_group_uid == span_term.access_group_uid

    # check that upserting a second time works
    new_span_term = Term(
        xid="TEST_span",
        name="Span",
        sub_class_of=length_term.uid,
        data={
            "length-in-m": 0.2286,
            "alias": [
                "great span",
                "full span",
            ],
        },
        access_group_uid=default_eaternity_access_group_uid,
    )

    await pg_db.pg_term_mgr.upsert_term(new_span_term)

    # get all the terms after upsert_term
    all_terms_after_second_upsert_term = await pg_db.pg_term_mgr.find_all()

    diff_after_first_and_second_upsert = DeepDiff(
        all_terms_after_upsert_term, all_terms_after_second_upsert_term, ignore_order=True
    )

    assert diff_after_first_and_second_upsert == {}


@pytest.mark.asyncio
async def test_upsert_many_terms(setup_services: tuple[PostgresDb, ServiceProvider]) -> None:
    """Test if upserting_many_terms works as expected."""
    pg_db, service_provider = setup_services

    # get all the terms that are in the database after reset
    all_terms_after_reset = await pg_db.pg_term_mgr.find_all()

    length_term = (await pg_db.pg_term_mgr.get_terms_by_xid("EOS_units_length"))[0]
    settings = PgSettings()
    eaternity_ags = await service_provider.access_group_service.get_all_access_group_ids_by_namespace_xid(
        settings.EATERNITY_NAMESPACE_XID
    )
    default_eaternity_access_group_uid = uuid.UUID(eaternity_ags[0].uid)
    human_based_unit_term = Term(
        uid=uuid.uuid4(),  # explicitly create uuid, so that setting sub_class_of below works
        xid="TEST_human_based_units_of_measurement",
        name="Human based units of measurement",
        sub_class_of=length_term.uid,
        data={},
        access_group_uid=default_eaternity_access_group_uid,
    )
    span_term = Term(
        xid="TEST_span",
        name="Span",
        sub_class_of=human_based_unit_term.uid,
        data={
            "length-in-m": 0.2286,
            "alias": [
                "great span",
                "full span",
            ],
        },
        access_group_uid=default_eaternity_access_group_uid,
    )

    terms_to_upsert = [human_based_unit_term, span_term]

    await pg_db.pg_term_mgr.upsert_many_terms(terms_to_upsert)

    # get all the terms after upsert_many_terms
    all_terms_after_upsert_many_terms = await pg_db.pg_term_mgr.find_all()

    diff_after_reset_and_first_upsert = DeepDiff(
        all_terms_after_reset, all_terms_after_upsert_many_terms, ignore_order=True
    )

    assert list(diff_after_reset_and_first_upsert.keys()) == ["iterable_item_added"]

    diff_term_added = diff_after_reset_and_first_upsert["iterable_item_added"]

    assert len(diff_term_added) == len(terms_to_upsert)

    diff_human_based_unit_term = [
        diff_term for diff_term in diff_term_added.values() if diff_term.xid == "TEST_human_based_units_of_measurement"
    ][0]

    diff_span_term = [diff_term for diff_term in diff_term_added.values() if diff_term.xid == "TEST_span"][0]

    # check that the linking was done correctly
    assert diff_span_term.sub_class_of == diff_human_based_unit_term.uid

    # check that except the uid all other attributes are the same
    assert diff_human_based_unit_term.name == human_based_unit_term.name
    assert diff_human_based_unit_term.data == human_based_unit_term.data
    assert diff_human_based_unit_term.access_group_uid == human_based_unit_term.access_group_uid

    assert diff_span_term.name == span_term.name
    assert diff_span_term.data == span_term.data
    assert diff_span_term.access_group_uid == span_term.access_group_uid
