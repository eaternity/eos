"""Brightway data importer tests."""
import os
import uuid
from argparse import Namespace
from typing import TYPE_CHECKING, Any, Dict, Tuple

import pytest

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.glossary_link import GlossaryLink
from core.domain.nodes import ElementaryResourceEmissionNode, FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import prepend_flow_node
from database.postgres.postgres_db import PostgresDb
from database.postgres.settings import PgSettings
from inventory_importer.bw_import_controller import parse

if TYPE_CHECKING:
    from bw2data import Database


@pytest.mark.filterwarnings("ignore::ResourceWarning")  # ignore this warning which is due to internal code of bw2data.
@pytest.mark.asyncio
async def test_import_database(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    create_brightway_test_database: str,
    brightway_test_database_new_unit: Tuple["Database", Dict[str, Any]],
    brightway_test_database_new_unit_and_location: Tuple["Database", Dict[str, Any]],
    brightway_test_database_new_production_amount_unit_and_location: Tuple["Database", Dict[str, Any]],
    brightway_test_database_modified_flow_amount: Tuple["Database", Dict[str, Any]],
) -> None:
    """Imports a test Brightway database (with just a few nodes)."""
    edb_path = create_brightway_test_database
    assert os.path.isdir(edb_path), f"could not find test database at {edb_path}."

    # simulate command line arguments
    # TODO '--import_using_glossary',
    argv1 = ["--edb_path", edb_path, "--import_by_name", "electricity production", "--import_all_impact_assessments"]
    args: Namespace = parse(argv1)

    # This is BW's way to set its local database path. Needs to be called BEFORE importing any of the BW's libraries!
    os.environ["BRIGHTWAY2_DIR"] = edb_path

    # import after setting the environment variable:
    from inventory_importer.bw_importer import BwImporter

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(args)

    assert num_new_nodes == 11, f"expected 11 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 1, f"expected 1 new impact assessment to be imported, got {num_new_char}"

    pg_db, service_provider = setup_services
    pg_term_mgr = pg_db.get_term_mgr()
    root_impact_assessment_term = await pg_term_mgr.get_terms_by_xid("Root_Impact_Assessments")
    assert root_impact_assessment_term, "Root impact assessment term should be imported."
    all_impact_assessment_terms = await pg_term_mgr.get_sub_terms_by_uid(str(root_impact_assessment_term[0].uid))
    assert len(all_impact_assessment_terms) == 5, "Impact assessment terms are not imported."
    for impact_assessment_term in all_impact_assessment_terms:
        if impact_assessment_term.xid == "ipcc-2013-test-abbreviation":
            assert (
                impact_assessment_term.data.get("description") == "ipcc-2013-test-description"
            ), "Impact assessment term description not imported."

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # second run should not import anything.

    assert num_new_nodes == 0, f"expected 0 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 0, f"expected 0 new impact assessment to be imported, got {num_new_char}"
    assert num_updated_activities == 0, f"expected 0 nodes to update, got {num_updated_activities}"

    new_edb, edb = brightway_test_database_new_unit
    new_edb.write(edb)

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # With new database, one node should be updated.

    assert num_new_nodes == 0, f"expected 0 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 0, f"expected 0 new impact assessment to be imported, got {num_new_char}"
    assert num_updated_activities == 1, f"expected 1 node to update, got {num_updated_activities}"

    new_edb, edb = brightway_test_database_new_unit_and_location
    new_edb.write(edb)

    service_provider.node_service.cache.clear()
    await service_provider.node_service.init()

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # With new database, one node should be updated.

    assert num_new_nodes == 0, f"expected 0 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 0, f"expected 0 new impact assessment to be imported, got {num_new_char}"
    assert num_updated_activities == 1, f"expected 1 node to update, got {num_updated_activities}"

    new_edb, edb = brightway_test_database_new_production_amount_unit_and_location
    new_edb.write(edb)

    service_provider.node_service.cache.clear()
    await service_provider.node_service.init()

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # With new database, one node should be updated.

    assert num_new_nodes == 0, f"expected 0 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 0, f"expected 0 new impact assessment to be imported, got {num_new_char}"
    assert num_updated_activities == 1, f"expected 1 node to update, got {num_updated_activities}"

    new_edb, edb = brightway_test_database_modified_flow_amount
    new_edb.write(edb)

    service_provider.node_service.cache.clear()
    await service_provider.node_service.init()

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # With new database, both nodes should be updated.

    assert num_new_nodes == 0, f"expected 0 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 0, f"expected 0 new impact assessment to be imported, got {num_new_char}"
    assert num_updated_activities == 2, f"expected 2 nodes to update, got {num_updated_activities}"


@pytest.mark.filterwarnings("ignore::ResourceWarning")  # ignore this warning which is due to internal code of bw2data.
@pytest.mark.asyncio
async def test_modified_flow_input(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    create_brightway_test_database: str,
    brightway_test_database_modified_flow_input: Tuple["Database", Dict[str, Any]],
) -> None:
    _, _ = setup_services
    """Imports a test Brightway database (with just a few nodes)."""
    edb_path = create_brightway_test_database
    assert os.path.isdir(edb_path), f"could not find test database at {edb_path}."

    # simulate command line arguments
    # TODO '--import_using_glossary',
    argv1 = ["--edb_path", edb_path, "--import_by_name", "electricity production", "--import_all_impact_assessments"]
    args: Namespace = parse(argv1)

    # This is BW's way to set its local database path. Needs to be called BEFORE importing any of the BW's libraries!
    os.environ["BRIGHTWAY2_DIR"] = edb_path

    # import after setting the environment variable:
    from inventory_importer.bw_importer import BwImporter

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # use the test database

    assert num_new_nodes == 11, f"expected 11 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 1, f"expected 1 new impact assessment to be imported, got {num_new_char}"

    new_edb, edb = brightway_test_database_modified_flow_input
    new_edb.write(edb)

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # With new database, both nodes should be updated.

    assert num_new_nodes == 1, f"expected 1 new node to be imported, got {num_new_nodes}"
    assert num_new_char == 0, f"expected 0 new impact assessment to be imported, got {num_new_char}"
    assert num_updated_activities == 1, f"expected 1 node to update, got {num_updated_activities}"


@pytest.mark.filterwarnings("ignore::ResourceWarning")  # ignore this warning which is due to internal code of bw2data.
@pytest.mark.asyncio
async def test_added_flows(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    create_brightway_test_database: str,
    brightway_test_database_added_flow: Tuple["Database", Dict[str, Any]],
) -> None:
    _, _ = setup_services
    """Imports a test Brightway database (with just a few nodes)."""
    edb_path = create_brightway_test_database
    assert os.path.isdir(edb_path), f"could not find test database at {edb_path}."

    # simulate command line arguments
    # TODO '--import_using_glossary',
    argv1 = ["--edb_path", edb_path, "--import_by_name", "electricity production", "--import_all_impact_assessments"]
    args: Namespace = parse(argv1)

    # This is BW's way to set its local database path. Needs to be called BEFORE importing any of the BW's libraries!
    os.environ["BRIGHTWAY2_DIR"] = edb_path

    # import after setting the environment variable:
    from inventory_importer.bw_importer import BwImporter

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # use the test database

    assert num_new_nodes == 11, f"expected 11 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 1, f"expected 1 new impact assessment to be imported, got {num_new_char}"

    new_edb, edb = brightway_test_database_added_flow
    new_edb.write(edb)

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # With new database, one node should be updated.

    assert num_new_nodes == 0, f"expected 0 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 0, f"expected 0 new impact assessment to be imported, got {num_new_char}"
    assert num_updated_activities == 1, f"expected 1 node to update, got {num_updated_activities}"


@pytest.mark.filterwarnings("ignore::ResourceWarning")  # ignore this warning which is due to internal code of bw2data.
@pytest.mark.asyncio
async def test_system_process_cache_recalculation(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    create_brightway_test_database: str,
    brightway_test_database_deleted_flow: Tuple["Database", Dict[str, Any]],
) -> None:
    """Imports a test Brightway database (with just a few nodes)."""
    pg_db, service_provider = setup_services

    service_provider.node_service.cache.clear()
    await service_provider.node_service.init()

    pg_graph_mgr = pg_db.get_graph_mgr()

    recipe = await pg_graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            titles=[{"language": "en", "value": "A recipe to test LCA of electricity production"}],
            recipe_portions=10,
            production_portions=4,
        )
    )

    electricity_ingredient = await pg_graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "en", "value": "electricity"}],
            amount_in_original_source_unit={
                "value": 1000,
                "unit": "kWh",
            },  # this becomes our reference flow / functional unit, used by matrix_calculation module
            type="conceptual-ingredients",
        )
    )

    # edge recipe --> ingredient
    await pg_graph_mgr.add_edge(
        Edge(
            parent_uid=recipe.uid,
            child_uid=electricity_ingredient.uid,
            edge_type=EdgeTypeEnum.ingredient,
        )
    )

    settings = PgSettings()
    term_mgr = pg_db.get_term_mgr()

    settings = PgSettings()
    eaternity_ags = await service_provider.access_group_service.get_all_access_group_ids_by_namespace_xid(
        settings.EATERNITY_NAMESPACE_XID
    )
    eaternity_term_group_uid = uuid.UUID(eaternity_ags[0].uid)

    electricity_term = await term_mgr.get_term_by_xid_and_access_group_uid(
        "EOS_electricity",
        eaternity_term_group_uid,
    )
    assert electricity_term

    edb_path = create_brightway_test_database
    assert os.path.isdir(edb_path), f"could not find test database at {edb_path}."

    # simulate command line arguments
    argv1 = ["--edb_path", edb_path, "--import_by_name", "electricity production", "--import_all_impact_assessments"]
    args: Namespace = parse(argv1)

    # This is BW's way to set its local database path. Needs to be called BEFORE importing any of the BW's libraries!
    os.environ["BRIGHTWAY2_DIR"] = edb_path

    # TODO '--import_using_glossary',
    # import after setting the environment variable:
    from inventory_importer.bw_importer import BwImporter

    bw_importer = BwImporter()
    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await bw_importer.import_bw_products_and_impact_assessments(
        args
    )  # use the test database
    assert num_new_char == 1

    electricity_production_xid = "EDB_cdf21b8781dd46fc9479558be2f62175"
    root_node_uid = bw_importer.node_id_mapper[electricity_production_xid]

    await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[electricity_term.uid],
            linked_node_uid=root_node_uid,
        )
    )

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    root_flow = await prepend_flow_node(pg_graph_mgr, recipe.uid)

    # check if calculation can use the aggregated system process cache from the brightway importer:
    calc_using_cache = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow.uid,
        child_of_root_node_uid=recipe.uid,
        return_log=False,
        save_as_system_process=False,
    )

    calc_using_cache, root_node = await calc_service.calculate(calc_using_cache)

    # get the process node and check if it is the electricity_production_process:
    sub_system_process: Node = root_node._sub_nodes[0]._sub_nodes[0]._sub_nodes[0]
    assert sub_system_process.uid == root_node_uid

    # check if the process node was actually using the system process cache:
    for sub_flow_node in sub_system_process.get_sub_nodes():
        assert isinstance(sub_flow_node.get_sub_nodes()[0], ElementaryResourceEmissionNode)

    # check if calculated co2-value of a recipe using electricity was correctly calculated:
    impact_assessment = list(root_node.impact_assessment.quantities.values())[0].value
    assert abs(impact_assessment - 120.0) < 1e-7

    assert num_new_nodes == 11, f"expected 11 new nodes to be imported, got {num_new_nodes}"
    assert num_new_char == 1, f"expected 1 new impact assessment to be imported, got {num_new_char}"

    new_edb, edb = brightway_test_database_deleted_flow
    new_edb.write(edb)

    (
        num_new_nodes,
        num_updated_activities,
        num_new_char,
    ) = await BwImporter().import_bw_products_and_impact_assessments(
        args
    )  # With new database, one node should be updated.

    assert num_new_nodes == 0, f"expected 0 new node, got {num_new_nodes} new nodes"
    assert num_new_char == 0, f"expected 0 new impact assessment to be imported, got {num_new_char}"
    assert num_updated_activities == 1, f"expected 1 node to update, got {num_updated_activities}"

    calc_using_cache = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow.uid,
        child_of_root_node_uid=recipe.uid,
        return_log=False,
        save_as_system_process=False,
    )
    calc_using_cache, root_node = await calc_service.calculate(calc_using_cache)

    # get the process node and check if it is the electricity_production_process:
    sub_system_process: Node = root_node._sub_nodes[0]._sub_nodes[0]._sub_nodes[0]
    assert sub_system_process.uid == root_node_uid

    # check if the process node has actually updated the system process cache:
    for sub_flow_node in sub_system_process.get_sub_nodes():
        assert isinstance(sub_flow_node.get_sub_nodes()[0], ElementaryResourceEmissionNode)

    impact_assessment = list(root_node.impact_assessment.quantities.values())[0].value
    assert abs(impact_assessment - 100.0) < 1e-7
