import argparse
import asyncio
import logging
import os
import sys

import structlog

from core.service.service_provider import ServiceLocator
from inventory_importer.airtable_matching_importer import MatchingLinkingImporter
from inventory_importer.download_eaternity_edb_data import download_from_github
from inventory_importer.fao_cache_drive_updater import fao_cache_drive_updater
from inventory_importer.gfm_importer import gfm_import_data
from inventory_importer.glossary_importer import GlossaryImporter

structlog.configure(
    wrapper_class=structlog.make_filtering_bound_logger(logging.INFO),
)

logger = structlog.get_logger()


def tuple_type(strings: str) -> tuple[str, ...]:
    """Convert a string to a tuple of strings."""
    strings = strings.replace("(", "").replace(")", "")
    strings = strings.replace("'", "").replace("'", "")
    strings = strings.replace('"', "").replace('"', "")
    mapped_str = [s.lstrip() for s in map(str, strings.split(","))]
    return tuple(mapped_str)


def parse(args_: list[str]) -> argparse.Namespace:
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="InventoryImporter")
    parser.add_argument("--edb_path", default="/eos/data/edb_export", help="path to the brightway project")
    parser.add_argument("--import_edb_database", action="store_true")
    parser.add_argument("--import_using_glossary", action="store_true")
    parser.add_argument("--import_using_airtable", action="store_true")
    parser.add_argument("--import_matchings_from_airtable", action="store_true")
    parser.add_argument(
        "--mock_legacy_co2_values",
        action="store_true",
        help="Used to mock legacy CO2 values for easier comparison between the new and the legacy systems. "
        "Only works in conjunction with import_using_airtable. "
        "The airtable's Brightway node is used as a fallback if EDB ID linking is missing.",
    )
    parser.add_argument("--download_from_gdrive")
    parser.add_argument("--gfm_import_data", action="store_true")
    parser.add_argument("--remove_gadm_files", action="store_true")
    parser.add_argument("--countries_alpha3", nargs="+", default=[], help="a list of 3-letter country codes to import")
    parser.add_argument(
        "--import_by_name",
        nargs="+",
        default=[],
        help="a list of EDB product names to import, encapsulated by double quotes, separated by space",
    )
    parser.add_argument(
        "--import_impact_assessments_by_name",
        nargs="+",
        default=[],
        type=tuple_type,
        help="a list of impact impact assessment methods to import, each method is a tuple of length 3, "
        'should be specified as, e.g., "(IPCC 2013, climate change, GWP 100a)", for a full list '
        "see bw2data.methods",
    )
    parser.add_argument(
        "--import_all_impact_assessments",
        action="store_true",
        help="import every impact assessment existing in the local bw folder",
    )

    parser.add_argument(
        "--import_glossary",
        action="store_true",
        help="upsert glossary terms",
    )
    parser.add_argument(
        "--source_of_source_files",
        type=str,
        choices=["fao", "gdrive", "local"],
        default="local",
        help="Source of the source files (fao, gdrive, or local)",
    )
    parser.add_argument(
        "--upload_files_to_drive", type=bool, default=False, help="Whether to upload files to Google Drive"
    )
    parser.add_argument(
        "--fao_update_google_drive_files_mode",
        type=str,
        choices=[
            "do_not_update",
            "take_local_source_and_do_not_upload",
            "download_source_from_fao_and_do_not_upload",
            "download_source_from_fao_and_upload_source_and_cache_to_drive",
        ],
        default="do_not_update",
        help="Mode for updating the google-drive FAO files. "
        "Normally, these files don't need to update these files (do_not_update). "
        "However, if we want to use different years for the import statistics we need to rebuild the "
        "cache files and reupload them to the gdrive (download_source_from_fao_and_upload_source_and_cache_to_drive)."
        "For testing the cache building locally we can also disable the uploading to drive "
        "(take_local_source_and_do_not_upload/ download_source_from_fao_and_do_not_upload). ",
    )

    return parser.parse_args(args_)


async def import_controller(schema: str = "public") -> None:
    service_locator = ServiceLocator()
    service_provider = service_locator.service_provider
    args: argparse.Namespace = parse(sys.argv[1:])
    await service_provider.postgres_db.connect(schema)
    await service_provider.messaging_service.start()
    await service_provider.init_caches()

    if args.import_glossary:
        logger.info("start importing glossary")
        importer = GlossaryImporter()
        await importer.upsert_glossary_and_matching()

    if args.import_matchings_from_airtable:
        logger.info("start downloading from eaternity-edb-data")
        download_from_github(args.edb_path)

        edb_path = args.edb_path
        edb_data_path = os.path.join(edb_path, "eaternity-edb-data")
        assert os.path.isdir(edb_data_path), f"could not find local eaternity-edb-data at {edb_data_path}."

        logger.info("start importing matchings, nutrients, FAO, perishability, and tags from airtable")

        matching_linking_importer = MatchingLinkingImporter(edb_data_path)
        await matching_linking_importer.import_from_airtable()

        logger.info("done importing")

    # This is BW's way to set its local database path.
    # Needs to be called BEFORE importing any of the BW's libraries!
    os.environ["BRIGHTWAY2_DIR"] = os.path.join(args.edb_path, "Base")

    if args.download_from_gdrive is not None:
        logger.info("start downloading from gdrive")

        # import after setting the environment variable:
        from inventory_importer.bw_download_gdrive import download_from_gdrive

        download_from_gdrive(args.download_from_gdrive, args.edb_path)

    trigger_character_import = len(args.import_impact_assessments_by_name) > 0 or args.import_all_impact_assessments
    trigger_database_import = (
        len(args.import_by_name) > 0
        or args.import_edb_database
        or args.import_using_glossary
        or args.import_using_airtable
    )

    if trigger_database_import or trigger_character_import:
        edb_path = args.edb_path
        assert os.path.isdir(edb_path), f"could not find local EDB database at {edb_path}."

        # import after setting the environment variable:
        from inventory_importer.bw_importer import BwImporter

        importer = BwImporter()

        logger.info("start seeding inventory sample data and impact assessments")

        await importer.import_bw_products_and_impact_assessments(args)

        logger.info("done seeding")

    if args.fao_update_google_drive_files_mode != "do_not_update":
        logger.info("start fao_cache_handler")
        if args.fao_update_google_drive_files_mode == "take_local_source_and_do_not_upload":
            await fao_cache_drive_updater(source_of_source_files="local", upload_files_to_drive=False)
        elif args.fao_update_google_drive_files_mode == "download_source_from_fao_and_do_not_upload":
            await fao_cache_drive_updater(source_of_source_files="fao", upload_files_to_drive=False)
        elif args.fao_update_google_drive_files_mode == "download_source_from_fao_and_upload_source_and_cache_to_drive":
            await fao_cache_drive_updater(source_of_source_files="fao", upload_files_to_drive=True)
        else:
            raise ValueError(f"Unknown fao_cache_rebuild_mode: {args.fao_cache_rebuild_mode}")
        logger.info("done fao_cache_handler")

    if args.gfm_import_data:
        logger.info("start gfm_import_data")
        await gfm_import_data(args.countries_alpha3, args.remove_gadm_files)

    await service_provider.shutdown_services()


if __name__ == "__main__":
    asyncio.run(import_controller())
