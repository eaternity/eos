#!/bin/bash

HOST="http://localhost:8040"
CUST_NAMESPACE="customer01"
KITCHEN_ID="kitchen01"
RECIPE_ID="recipe02"
SUB_RECIPE_ID="recipe01"

AUTH_KEY=$(echo -n $CUST_NAMESPACE | base64)

echo "start adding recipe ${RECIPE_ID}"
curl -i --location --request PUT ${HOST}'/v2/calculation/'${RECIPE_ID} \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}" \
 --data-raw '{
  "recipe": {
    "titles": [
      {
        "language": "de",
        "value": "Parent Recipe with child recipes"
      }
    ],
    "date": "2013-10-14",
    "location": "Zürich Schweiz",
    "servings": 140,
    "ingredients": [
      {
        "id": "'${SUB_RECIPE_ID}'",
        "type": "recipes",
        "names": [
          {
            "language": "de",
            "value": "Kürbisrisotto"
          }
        ],
        "amount": 50,
        "unit": "gram",
        "origin": "spain",
        "transport": "air",
        "production": "greenhouse",
        "processing": "raw",
        "conservation": "fresh",
        "packaging": "plastic"
      },
      {
        "id": "242342343",
        "type": "conceptual-ingredients",
        "names": [
          {
            "language": "de",
            "value": "Kartoffeln"
          }
        ],
        "amount": 78,
        "unit": "gram",
        "origin": "france",
        "transport": "ground",
        "production": "organic",
        "processing": "",
        "conservation": "dried",
        "packaging": ""
      }
    ]
  }
}'
