#!/bin/bash

if [ ! -f .env ]
then
  export $(cat ../.env | xargs)
else
  export $(cat .env | xargs)
fi

echo "backing up database ${POSTGRES_DB}..."

export PGPASSWORD=$POSTGRES_PASSWORD
pg_dump -h localhost -p 5444 -U $POSTGRES_USER $POSTGRES_DB | gzip > database_backup.sql.gz

echo "backup done"
