#!/bin/bash

HOST="http://localhost:8040"
CUST_NAMESPACE="customer02"
CUST_NAME="name02"
KITCHEN_ID="kitchen05"

echo "start adding customer..."

if [ ! -f .env ]
then
  export $(cat ../.env | xargs)
fi

EATERNITY_AUTH_KEY=$(echo -n $EATERNITY_USERNAME | base64)

echo $EATERNITY_AUTH_KEY


AUTH_KEY=$(echo -n $CUST_NAMESPACE | base64)

echo "use newly created customer namespace ${CUST_NAMESPACE}"
echo "use auth key ${AUTH_KEY}"
echo "start adding kitchen ${KITCHEN_ID}"

curl -i --location --request PUT ${HOST}'/api/kitchens/'${KITCHEN_ID} \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}" \
 --data-raw '{
     "kitchen": {
         "name": "'"$KITCHEN_ID"'",
         "location": "schweiz"
     }
 }'
