"""Script for end to end carrot testing.

Use command line argument eos or eaternity-cloud to run calculation on EOS (Kale) or Eaternity Cloud (Javaland).
"""

import json
import os
import uuid
from copy import deepcopy

import httpx
from dotenv import find_dotenv, load_dotenv
from structlog import get_logger

from api.tests.test_api_for_recipes import TEST_RECIPE_ID, TEST_RECIPE_WITH_SUBRECIPE
from legacy_api.tests.test_legacy_recipe_router import (
    TEST_RECIPE,
    add_field_from_recipe_or_ingredient,
    delete_field_from_recipe_or_ingredient,
)
from legacy_api.tests.test_legacy_supply_router import TEST_SUPPLY

# from legacy_api.tests.test_legacy_product_router import TEST_PRODUCT_SUPERUSER

logger = get_logger()


def convert_api_recipe_to_legacy_api_recipe(api_recipe: dict) -> dict:
    def convert_new_to_legacy_field_name(recipe: dict, new_name: str, legacy_name: str) -> None:
        if new_name in recipe:
            if new_name.startswith("raw_"):
                recipe[legacy_name] = recipe[new_name]["value"]
            else:
                recipe[legacy_name] = recipe[new_name]
            del recipe[new_name]

    mapping_dict = {
        "servings_deprecated": "servings",
        "activity_date": "date",
        "xid": "id",
        "product_name": "names",
        "flow_location": "location",
        "activity_location": "location",
        "raw_production": "production",
        "raw_conservation": "conservation",
    }

    legacy_api_recipe = deepcopy(api_recipe["activity"])
    legacy_api_recipe["ingredients"] = deepcopy(api_recipe["sub_flows"])
    for new_name, legacy_name in mapping_dict.items():
        convert_new_to_legacy_field_name(legacy_api_recipe, new_name, legacy_name)
    del legacy_api_recipe["node_type"]

    for ingredient in legacy_api_recipe["ingredients"]:
        for new_name, legacy_name in mapping_dict.items():
            convert_new_to_legacy_field_name(ingredient, new_name, legacy_name)
        del ingredient["node_type"]
        if "link_to_sub_node" in ingredient:
            ingredient["type"] = "recipes"
            if not ingredient["link_to_sub_node"].get("xid"):
                raise ValueError(
                    f"Invalid link_to_sub_node: [{ingredient['link_to_sub_node']}]. "
                    "Only linking by xid can be handled. "
                )
            ingredient["id"] = ingredient["link_to_sub_node"]["xid"]
            del ingredient["link_to_sub_node"]
        else:
            ingredient["type"] = "conceptual-ingredients"

        if "amount_in_original_source_unit" in ingredient:
            ingredient["amount"] = ingredient["amount_in_original_source_unit"]["value"]
            ingredient["unit"] = ingredient["amount_in_original_source_unit"]["unit"]
            if ingredient["unit"] == "gram":
                ingredient["unit"] = "kilogram"  # scale up ingredient amounts so that the CO2 value is also scaled up.
            del ingredient["amount_in_original_source_unit"]

    return legacy_api_recipe


def delete_field_wrapper(main_recipe: dict, field_to_delete: str, delete_from: str = "recipe") -> dict:
    tmp_recipe = {"recipe": main_recipe}
    new_recipe = delete_field_from_recipe_or_ingredient(tmp_recipe, field_to_delete, delete_from)
    return new_recipe["recipe"]


def add_field_wrapper(
    main_recipe: dict, field_to_add: str, new_value: str | float | int | dict | list, add_to: str = "recipe"
) -> dict:
    tmp_recipe = {"recipe": main_recipe}
    new_recipe = add_field_from_recipe_or_ingredient(tmp_recipe, field_to_add, new_value, add_to)
    return new_recipe["recipe"]


CARROT_RECIPE = {
    "id": "carrot-cake-recipe-1",
    "titles": [{"language": "de", "value": "Karrotenkuchen"}],
    "author": "Eckart Witzigmann",
    "date": "2013-10-14",
    "location": "Zürich Schweiz",
    "servings": 10,
    "instructions": [
        {"language": "de", "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."},
        {"language": "en", "value": "Bake the carrot cake in the oven and enjoy while still hot."},
    ],
    "ingredients": [
        {
            "id": "100100894",
            "type": "conceptual-ingredients",
            "names": [{"language": "de", "value": "Karotten"}],
            "amount": 1000,
            "unit": "gram",
            "origin": "france",
            "transport": "ground",
            "production": "organic",
            "processing": "",
            "conservation": "fresh",
            "packaging": "",
        }
    ],
}

CARROT_KITCHEN = {
    "id": "kitchen01",
    "name": "carrot-making kitchen",
    "location": "Switzerland",
}

EOS_TEST_RECIPE_KITCHEN = {
    "id": "kitchen02",
    "name": "EOS test recipe kitchen",
    "location": "Germany",
}

EOS_TEST_RECIPE_KITCHEN_FOR_COMBINED = {
    "id": "kitchen03",
    "name": "EOS test recipe kitchen",
    "location": "Germany",
}

TEST_LEGACY_RECIPE = TEST_RECIPE["recipe"]
TEST_LEGACY_RECIPE = add_field_wrapper(TEST_LEGACY_RECIPE, "id", TEST_RECIPE_ID)
TEST_LEGACY_RECIPE = add_field_wrapper(TEST_LEGACY_RECIPE, "recipe-portions", TEST_LEGACY_RECIPE["servings"])
TEST_LEGACY_RECIPE = add_field_wrapper(TEST_LEGACY_RECIPE, "kitchen-id", EOS_TEST_RECIPE_KITCHEN.get("id"))
del TEST_LEGACY_RECIPE["servings"]


TEST_LEGACY_RECIPE_WITH_SUBRECIPE = convert_api_recipe_to_legacy_api_recipe(TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"])

TEST_LEGACY_RECIPE_WITH_SUBRECIPE = add_field_wrapper(TEST_LEGACY_RECIPE_WITH_SUBRECIPE, "production-portions", 500)
TEST_LEGACY_RECIPE_WITH_SUBRECIPE = add_field_wrapper(TEST_LEGACY_RECIPE_WITH_SUBRECIPE, "sold-portions", 300)
TEST_LEGACY_RECIPE_WITH_SUBRECIPE = add_field_wrapper(
    TEST_LEGACY_RECIPE_WITH_SUBRECIPE, "menu-line-name", "EOS test recipe with subrecipe"
)
TEST_LEGACY_RECIPE_WITH_SUBRECIPE = add_field_wrapper(TEST_LEGACY_RECIPE_WITH_SUBRECIPE, "menu-line-id", "10101010")
TEST_LEGACY_RECIPE_WITH_SUBRECIPE = add_field_wrapper(
    TEST_LEGACY_RECIPE_WITH_SUBRECIPE, "instruction", "This is an arbitrary instruction"
)
TEST_LEGACY_RECIPE_WITH_SUBRECIPE = add_field_wrapper(
    TEST_LEGACY_RECIPE_WITH_SUBRECIPE, "kitchen-id", EOS_TEST_RECIPE_KITCHEN.get("id")
)


# Second test recipe with the same subrecipe, but with fields modified.
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = deepcopy(TEST_LEGACY_RECIPE_WITH_SUBRECIPE)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE, "amount", [10000, 78], add_to="ingredient"
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE, "unit", ["gram", "kilogram"], add_to="ingredient"
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE, "transport", ["ground", "air"], add_to="ingredient"
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE, "processing", ["", "raw"], add_to="ingredient"
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE, "conservation", ["", "fresh"], add_to="ingredient"
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE, "packaging", ["", "plastic"], add_to="ingredient"
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE, "gtin", ["00123456789023", ""], add_to="ingredient"
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE, "production", ["", "greenhouse"], add_to="ingredient"
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = add_field_wrapper(
    SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE,
    "names",
    [[{"language": "de", "value": "New risotto"}], [{"language": "de", "value": "Tomaten"}]],
    add_to="ingredient",
)
SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE["id"] = "second-test-legacy-recipe"

# Third test recipe with subrecipe, with fields deleted.
THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = deepcopy(SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE)
THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = delete_field_wrapper(
    THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE,
    "transport",
    delete_from="ingredient",
)
THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = delete_field_wrapper(
    THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE,
    "processing",
    delete_from="ingredient",
)
THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE = delete_field_wrapper(
    THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE,
    "production",
    delete_from="ingredient",
)
THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE["id"] = "third-test-legacy-recipe"

# TEST_PRODUCT_WITH_KNOWN_NAME = TEST_PRODUCT_SUPERUSER["product"]
# TEST_PRODUCT_WITH_KNOWN_NAME["names"][0]["value"] = "Tomaten"
# TEST_PRODUCT_WITH_KNOWN_NAME["id"] = "tomato-id"

TEST_LEGACY_SUPPLY = deepcopy(TEST_SUPPLY["supply"])
TEST_LEGACY_SUPPLY = add_field_wrapper(TEST_LEGACY_SUPPLY, "order-date", "2023-04-10")
TEST_LEGACY_SUPPLY["id"] = "test-supply-id"

TEST_LEGACY_SUPPLY_WITH_NEW_INGREDIENT_ID = deepcopy(TEST_LEGACY_SUPPLY)
TEST_LEGACY_SUPPLY_WITH_NEW_INGREDIENT_ID["id"] = "supply-with-new-ingredient-id"
# Using Onion ID for Tomato ingredient.
TEST_LEGACY_SUPPLY_WITH_NEW_INGREDIENT_ID = add_field_wrapper(
    TEST_LEGACY_SUPPLY_WITH_NEW_INGREDIENT_ID, "id", ["100199894"], add_to="ingredient"
)

TEST_LEGACY_SUPPLY_WITH_STRANGE_DECLARATION = deepcopy(TEST_LEGACY_SUPPLY)
TEST_LEGACY_SUPPLY_WITH_STRANGE_DECLARATION["id"] = "supply-with-strange-declaration"
# Using Onion ID for Tomato ingredient.
TEST_LEGACY_SUPPLY_WITH_STRANGE_DECLARATION = add_field_wrapper(
    TEST_LEGACY_SUPPLY_WITH_STRANGE_DECLARATION, "ingredients-declaration", ["STRANGE DECLARATION"], add_to="ingredient"
)

# Fourth test recipe with subrecipe, with nutrient_values.
FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE = add_field_wrapper(
    deepcopy(THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE),
    "nutrient-values",
    [TEST_SUPPLY["supply"]["ingredients"][0]["nutrient-values"], None],
    add_to="ingredient",
)

FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE = add_field_wrapper(
    FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE,
    "ingredients-declaration",
    [TEST_SUPPLY["supply"]["ingredients"][0]["ingredients-declaration"], None],
    add_to="ingredient",
)

FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE = add_field_wrapper(
    FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE,
    "id",
    ["test-recipe-id-1", FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE["ingredients"][1]["id"]],
    add_to="ingredient",
)
FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE = add_field_wrapper(
    FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE,
    "names",
    [
        [{"language": "de", "value": "TEST recipe with many ingredients"}],
        FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE["ingredients"][1]["names"],
    ],
    add_to="ingredient",
)
FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE["id"] = "fourth-test-legacy-recipe"

FIFTH_TEST_RECIPE_WITH_ONE_UNMATCHED_INGREDIENT_WITH_NUTRIENTS = {
    "id": "fifth-test-legacy-recipe-1",
    "titles": [{"language": "de", "value": "Kürbisrisotto"}],
    "author": "Eckart Witzigmann",
    "date": "2023-06-20",
    "location": "Zürich Schweiz",
    "servings": 111,
    "instructions": [
        {"language": "de", "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."},
        {"language": "en", "value": "Bake the carrot cake in the oven and enjoy while still hot."},
    ],
    "ingredients": [
        {
            "id": "unmatched-combined-product-1",
            "type": "conceptual-ingredients",
            "names": [
                {
                    "language": "de",
                    "value": "Karottenkuchen",
                },
            ],
            "amount": 150,
            "unit": "kilogram",
            "origin": "china",
            "transport": "air",
            "packaging": "plastic",
            "nutrient-values": {
                "carbohydrates-gram": 11.1,
                "chlorine-milligram": 54.5,
                "energy-kcal": 57.120999999999995,
                "fat-gram": 0.2,
                "protein-gram": 1.4,
                "saturated-fat-gram": 0.0,
                "sodium-milligram": 15.5,
                "sucrose-gram": 3.5500000000000003,
                "water-gram": 83.95,
            },
            "ingredient-declaration": "Karotten, Kartoffeln",
        },
    ],
}

# Linking to this recipe has to be done manually.
# Should we somehow setup an infrastructure for it?
FIFTY_FIFTY_CARROT_POTATO_RECIPE = {
    "id": "fifty-fifty-carrot-potato-recipe",
    "titles": [{"language": "en", "value": "Fifty-fifty carrot potato"}],
    "author": "S. Choi",
    "date": "2024-03-26",
    "location": "Zürich Schweiz",
    "recipe-portions": 1,
    "instructions": [
        {"language": "de", "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."},
        {"language": "en", "value": "Bake the carrot cake in the oven and enjoy while still hot."},
    ],
    "ingredients": [
        {
            "id": "carrot-1",
            "type": "conceptual-ingredients",
            "names": [{"language": "de", "value": "Karotten"}],
            "amount": 50,
            "unit": "gram",
            "origin": "",
            "transport": "ground",
            "production": "organic",
            "processing": "",
            "conservation": "fresh",
            "packaging": "",
        },
        {
            "id": "potato-1",
            "type": "conceptual-ingredients",
            "names": [{"language": "de", "value": "Kartoffeln"}],
            "amount": 50,
            "unit": "gram",
            "origin": "",
            "transport": "ground",
            "production": "organic",
            "processing": "",
            "conservation": "fresh",
            "packaging": "",
        },
    ],
}


class PostRecipeHelper:
    cust_namespace = "eos-migration-testing-3-3"
    cust_name = "EOS Migration Testing"
    kitchen_id = "MigrationKitchen3"

    def __init__(self, host_api: str = "https://test.eaternity.ch"):
        """Prepare the URL and data."""
        self.host_api: str = host_api
        env_path = find_dotenv(".env.eaternity-cloud")

        logger.info(f"Using environmental variables from {env_path}")
        load_dotenv(env_path, override=True)

        self.eaternity_token = os.getenv("EATERNITY_TOKEN")

    def add_customer(self, cust_namespace: str = None) -> None:
        """Adding customer."""
        if not cust_namespace:
            cust_namespace = self.cust_namespace

        logger.info("Started to add customer...")
        url = f"{self.host_api}/batch/customers/{cust_namespace}"
        data = {
            "customer": {
                "name": self.cust_name,
                "realCustomer": False,
                "comparedToOthers": True,
                "sharedWithOthers": True,
                "kitchen-blacklist": [],
                "menu-line-blacklist": [],
                "product-blacklist": [],
                "recipe-blacklist": [],
                "normalizeApiByFU": False,
                "delegateUser": False,
            }
        }

        response = httpx.put(url, json=data, auth=(self.eaternity_token, ""), timeout=100)
        response.raise_for_status()
        logger.info(f"New customer added: {data.get('customer').get('name')} with namespace {cust_namespace}")

    def add_kitchen(self, kitchen: dict, cust_namespace: str = None) -> str:
        if not cust_namespace:
            cust_namespace = self.cust_namespace
        logger.info("Started to add kitchen...")
        kitchen_id = kitchen.get("id")
        if not kitchen_id:
            kitchen_id = uuid.uuid4()
        else:
            del kitchen["id"]

        url = f"{self.host_api}/api/kitchens/{kitchen_id}"
        data = {"kitchen": kitchen}
        response = httpx.put(url, json=data, auth=(cust_namespace, ""), timeout=100)
        response.raise_for_status()
        logger.info(f"Kitchen: {data.get('kitchen').get('name')} was added")
        return kitchen_id

    def add_recipe(self, kitchen_id: str, recipe: dict, cust_namespace: str = None) -> None:
        recipe_id = recipe.get("id")
        if not recipe_id:
            recipe_id = uuid.uuid4()
        else:
            del recipe["id"]

        if not cust_namespace:
            cust_namespace = self.cust_namespace

        logger.info(f"Running request for {recipe_id}...")
        logger.info(json.dumps(recipe, indent=3))
        data = {"recipe": recipe}
        url = f"{self.host_api}/api/kitchens/{kitchen_id}/recipes/{recipe_id}"
        response = httpx.put(url, json=data, auth=(cust_namespace, ""), timeout=600)
        try:
            results = response.json()

            logger.info(f"Response: {json.dumps(results, indent=3)}")
        except json.JSONDecodeError:
            pass

    def add_supply(self, kitchen_id: str, supply: dict) -> None:
        supply_id = supply.get("id")
        if not supply_id:
            supply_id = uuid.uuid4()
        else:
            del supply["id"]

        logger.info(f"Running request for {supply_id}...")
        logger.info(json.dumps(supply, indent=3))
        data = {"supply": supply}
        url = f"{self.host_api}/api/kitchens/{kitchen_id}/supplies/{supply_id}"
        response = httpx.put(url, json=data, auth=(self.cust_namespace, ""), timeout=600)
        try:
            results = response.json()

            logger.info(f"Response: {json.dumps(results, indent=3)}")
        except json.JSONDecodeError:
            pass

    def add_product(self, product: dict) -> None:
        product_id = product.get("id")
        if not product_id:
            product_id = uuid.uuid4()
        else:
            del product["id"]

        logger.info(f"Running request for {product_id}...")
        logger.info(json.dumps(product, indent=3))
        url = f"{self.host_api}/api/products/{product_id}"
        response = httpx.put(url, json=product, auth=(self.cust_namespace, ""), timeout=600)
        response.raise_for_status()


if __name__ == "__main__":
    # post_recipe_helper = PostRecipeHelper(host_api="https://cloud-2.eaternity.ch")
    post_recipe_helper = PostRecipeHelper()
    logger.info("Running test recipe seeding for migration...")
    kitchen_exists = False  # Set to True then rerun after completing matchings.

    if not kitchen_exists:
        post_recipe_helper.add_customer()
        carrot_kitchen_id = post_recipe_helper.add_kitchen(CARROT_KITCHEN)
        eos_test_recipe_kitchen_id = post_recipe_helper.add_kitchen(EOS_TEST_RECIPE_KITCHEN)

        post_recipe_helper.add_recipe(carrot_kitchen_id, CARROT_RECIPE)
        post_recipe_helper.add_recipe(eos_test_recipe_kitchen_id, TEST_LEGACY_RECIPE)
        post_recipe_helper.add_recipe(eos_test_recipe_kitchen_id, TEST_LEGACY_RECIPE_WITH_SUBRECIPE)
        post_recipe_helper.add_recipe(eos_test_recipe_kitchen_id, SECOND_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE)
        post_recipe_helper.add_recipe(eos_test_recipe_kitchen_id, THIRD_TEST_LEGACY_RECIPE_WITH_SAME_SUBRECIPE)
        post_recipe_helper.add_recipe(eos_test_recipe_kitchen_id, FOURTH_TEST_RECIPE_WITH_EMPTY_SUBRECIPE)
        post_recipe_helper.add_supply(eos_test_recipe_kitchen_id, TEST_LEGACY_SUPPLY)
        # We do not post supply with same id and different name. The matching remains the same, but name and ingredients
        # declaration is updated.
        post_recipe_helper.add_supply(eos_test_recipe_kitchen_id, TEST_LEGACY_SUPPLY_WITH_NEW_INGREDIENT_ID)
        post_recipe_helper.add_supply(eos_test_recipe_kitchen_id, TEST_LEGACY_SUPPLY_WITH_STRANGE_DECLARATION)
    else:
        eos_test_recipe_kitchen_id = EOS_TEST_RECIPE_KITCHEN["id"]

    customer_namespace_combined_product = "eos-migration-testing-combined-product-3-3"
    if not kitchen_exists:
        post_recipe_helper.add_customer(cust_namespace=customer_namespace_combined_product)
        combined_product_kitchen_id = post_recipe_helper.add_kitchen(
            EOS_TEST_RECIPE_KITCHEN_FOR_COMBINED, cust_namespace=customer_namespace_combined_product
        )
        post_recipe_helper.add_recipe(
            combined_product_kitchen_id,
            FIFTY_FIFTY_CARROT_POTATO_RECIPE,
            cust_namespace=customer_namespace_combined_product,
        )

    post_recipe_helper.add_recipe(
        eos_test_recipe_kitchen_id, FIFTH_TEST_RECIPE_WITH_ONE_UNMATCHED_INGREDIENT_WITH_NUTRIENTS
    )

    # post_recipe_helper.add_product(TEST_PRODUCT_WITH_KNOWN_NAME)
