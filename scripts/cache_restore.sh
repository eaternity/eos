#!/bin/bash

# Check if an argument is provided
if [ "$#" -eq 1 ]; then
  SQL_FILE_PATH="$1"
else
  # Default SQL file path
  SQL_FILE_PATH="./postgres_localhost-production_24-02-06_18-51-20.sql"
fi

if [ ! -f .env ]
then
  export $(cat ../.env | xargs)
else
  export $(cat .env | xargs)
fi

echo "restoring cache of database ${POSTGRES_DB}..."

export PGPASSWORD=$POSTGRES_PASSWORD

psql -h localhost -p 5444 -U $POSTGRES_USER -d $POSTGRES_DB -c "DROP TABLE IF EXISTS gap_filling_modules_cache;"
psql -h localhost -p 5444 -U $POSTGRES_USER -d $POSTGRES_DB -a -f "$SQL_FILE_PATH"

echo "restore done"
