#!/bin/bash

HOST="http://localhost:8050"
CUST_NAMESPACE="customer01"
KITCHEN_ID="kitchen01"
PRODUCT_ID="product01"

AUTH_KEY=$(echo -n $CUST_NAMESPACE | base64)

echo "start getting recipe ${PRODUCT_ID}"
curl -i --location --request GET ${HOST}'/api/products/'${PRODUCT_ID} \
 --header 'Content-Type: application/json' \
 --header "Authorization: Basic ${AUTH_KEY}"