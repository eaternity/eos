#!/bin/bash

# updates all poetry deps
# this script need to be executed in the parent directory.

# respects the dependencies btw sub-projects

cd database
poetry update  || FAILED=true
poetry install  || FAILED=true

cd ../core
poetry update  || FAILED=true
poetry install  || FAILED=true

cd ../api
poetry update  || FAILED=true
poetry install  || FAILED=true

cd ../legacy_api
poetry update  || FAILED=true
poetry install  || FAILED=true

cd ../inventory_importer
poetry update  || FAILED=true
poetry install  || FAILED=true

cd ..
poetry update  || FAILED=true
poetry install  || FAILED=true

if [ $FAILED ]
then
  echo "update failed." >/dev/stderr
  exit 1
else
  echo 'done updating all dependencies'
fi
