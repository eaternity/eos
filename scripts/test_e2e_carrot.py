#!/usr/bin/env python3
"""Script for end to end carrot response comparing between backends.

Example usage: `poetry run scripts/test_e2e_carrot.py eos eaternity-test-2`
"""

import json
import os
import sys
from enum import Enum
from typing import Tuple

import httpx
from deepdiff import DeepDiff
from dotenv import find_dotenv, load_dotenv
from structlog import get_logger

logger = get_logger()


class CodebaseEnum(str, Enum):
    eos = "eos"
    eos_staging = "eos-staging"
    eos_production = "eos-production"
    eaternity_cloud = "eaternity-cloud"
    eaternity_test_2 = "eaternity-test-2"


CODEBASE_VALUES = set(item.value for item in CodebaseEnum)


class E2eTestRunner:
    cust_namespace = "4bb97d53-cbef-4e52-aa04-9b4aeb25fcec"
    cust_name = "name01"
    kitchen_id = "kitchen01"
    recipe_id = "recipe01"
    host_api: str
    host_legacy_api: str

    def __init__(self, codebase: CodebaseEnum):
        self.codebase: CodebaseEnum = codebase
        """Prepare the URL and data"""
        if self.codebase == CodebaseEnum.eos:
            env_path = find_dotenv()
            self.host_api = "http://localhost:8040/v2"
            self.host_legacy_api = "http://localhost:8050/api"
        elif self.codebase == CodebaseEnum.eos_staging:
            env_path = find_dotenv(".env.eos-staging")
            self.host_api = "https://eos.eaternity.ch/staging/v2"
            self.host_legacy_api = "https://eos.eaternity.ch/staging/api"
        elif self.codebase == CodebaseEnum.eos_production:
            env_path = find_dotenv(".env.eos-production")
            self.host_api = "https://eos.eaternity.ch/v2"
            self.host_legacy_api = "https://eos.eaternity.ch/api"
        elif self.codebase == CodebaseEnum.eaternity_cloud:
            env_path = find_dotenv(".env.eaternity-cloud")
            self.host_api = "https://cloud-2.eaternity.ch"
            self.host_legacy_api = "https://cloud-2.eaternity.ch/api"
        elif self.codebase == CodebaseEnum.eaternity_test_2:
            env_path = find_dotenv(".env.eaternity-test-2")
            self.host_api = "https://test.eaternity.ch"
            self.host_legacy_api = "https://test.eaternity.ch/api"
        else:
            raise ValueError(f"Unrecognized codebase {self.codebase}")

        logger.info(f"Using environmental variables from {env_path}")
        load_dotenv(env_path, override=True)

        self.eaternity_token = os.getenv("EATERNITY_TOKEN")

    def add_customer(self) -> str:
        """Adding customer."""
        logger.info("Started to add customer...")
        url = f"{self.host_api}/batch/customers/{self.cust_namespace}"
        data = {
            "customer": {
                "name": self.cust_name,
                "realCustomer": False,
                "comparedToOthers": True,
                "sharedWithOthers": True,
                "kitchen-blacklist": [],
                "menu-line-blacklist": [],
                "product-blacklist": [],
                "recipe-blacklist": [],
                "normalizeApiByFU": False,
                "delegateUser": False,
            }
        }

        response = httpx.put(url, json=data, auth=(self.eaternity_token, ""), timeout=100)
        response.raise_for_status()
        if self.codebase == CodebaseEnum.eos:
            logger.info(f"New customer added: {json.dumps(response.json(), indent=3)}")
            return response.json().get("auth_token")
        if self.codebase == CodebaseEnum.eos_staging or self.codebase == CodebaseEnum.eos_production:
            logger.info(f"New customer added: {json.dumps(response.json(), indent=3)}")
            return response.json().get("auth_token")
        elif self.codebase == CodebaseEnum.eaternity_cloud:
            logger.info(f"New customer added: {data.get('customer').get('name')} with namespace {self.cust_namespace}")
            return self.cust_namespace
        elif self.codebase == CodebaseEnum.eaternity_test_2:
            logger.info(f"New customer added: {data.get('customer').get('name')} with namespace {self.cust_namespace}")
            return self.cust_namespace
        else:
            raise ValueError(f"Unrecognized codebase {self.codebase}")

    def add_kitchen(self, customer_token: str) -> None:
        logger.info("Started to add kitchen...")
        url = f"{self.host_legacy_api}/kitchens/{self.kitchen_id}"
        data = {
            "kitchen": {
                "name": "carrot-making kitchen",
                "location": "Switzerland",
            }
        }
        response = httpx.put(url, json=data, auth=(customer_token, ""), timeout=100)
        response.raise_for_status()
        logger.info(f"Kitchen: {data.get('kitchen').get('name')} was added")

    def get_kitchens(self, customer_token: str) -> dict:
        logger.info("Started to add kitchen...")
        url = f"{self.host_legacy_api}/kitchens"
        response = httpx.get(url, auth=(customer_token, ""), timeout=100)
        response.raise_for_status()
        kitchens = response.json()
        return kitchens

    def add_recipe(self, customer_token: str) -> dict:
        url = f"{self.host_legacy_api}/kitchens/{self.kitchen_id}/recipes/{self.recipe_id}"
        data = {
            "recipe": {
                "titles": [{"language": "de", "value": "Karrotenkuchen"}],
                "author": "Eckart Witzigmann",
                "date": "2013-10-14",
                "location": "Zürich Schweiz",
                "servings": 10,
                "instructions": [
                    {"language": "de", "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."},
                    {"language": "en", "value": "Bake the carrot cake in the oven and enjoy while still hot."},
                ],
                "ingredients": [
                    {
                        "id": "100100894",
                        "type": "conceptual-ingredients",
                        "names": [{"language": "de", "value": "Karotten"}],
                        "amount": 1000,
                        "unit": "gram",
                        "origin": "france",
                        "transport": "ground",
                        "production": "organic",
                        "processing": "",
                        "conservation": "fresh",
                        "packaging": "",
                    }
                ],
            }
        }
        response = httpx.put(url, json=data, auth=(customer_token, ""), timeout=600)
        response.raise_for_status()
        results = response.json()
        logger.info(f"Response: {json.dumps(results, indent=3)}")
        return results

    def run_e2e_carrot(self) -> Tuple[dict, str, dict]:
        customer_token = self.add_customer()
        self.add_kitchen(customer_token)
        kitchens = self.get_kitchens(customer_token)
        results = self.add_recipe(customer_token)
        return results, customer_token, kitchens


if __name__ == "__main__":
    try:
        (codebase1, codebase2) = sys.argv[1:3]
    except ValueError:
        # default
        print(f"USAGE: {sys.argv[0]} <backend1> <backend2>")
        print(f"Backends: {CODEBASE_VALUES}")
        print(
            "Use two command line argument to run calculation on e.g. EOS (Kale) and Eaternity Cloud (Javaland)"
            " and compare the responses."
        )
        (codebase1, codebase2) = (CodebaseEnum.eos, CodebaseEnum.eaternity_test_2)
        print(f"default fallback to backends: {codebase1}, {codebase2}")
    assert codebase1 in CODEBASE_VALUES, f"pick a valid codebase1 from {CODEBASE_VALUES}"
    assert codebase2 in CODEBASE_VALUES, f"pick a valid codebase2 from {CODEBASE_VALUES}"
    codebase1 = CodebaseEnum(codebase1)
    test_runner1 = E2eTestRunner(codebase=codebase1)
    logger.info(f"Running end-to-end carrot test with {codebase1}...")
    results1, customer_token, kitchens1 = test_runner1.run_e2e_carrot()

    codebase2 = CodebaseEnum(codebase2)
    test_runner2 = E2eTestRunner(codebase=codebase2)
    test_runner2.cust_namespace = customer_token
    logger.info(f"Running end-to-end carrot test with {codebase2}...")
    results2, customer_token_eaternity_cloud, kitchens2 = test_runner2.run_e2e_carrot()

    dict_diff = DeepDiff(results2, results1)
    logger.info(f"DeepDiff between {codebase2} and {codebase1} results: {dict_diff}")

    logger.info(f"DeepDiff between kitchens: {DeepDiff(kitchens1, kitchens2)}")
