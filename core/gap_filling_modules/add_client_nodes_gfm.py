"Add client Nodes gap filling module."
from typing import Optional
from uuid import UUID, uuid4

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from structlog import get_logger

from core.domain.access_group_node import AccessGroup
from core.domain.data_error import ErrorClassification
from core.domain.edge import XidAccessGroupUid
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, ModeledActivityNode
from core.domain.nodes.activity.linking_activity_node import LinkingActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.node import Node, PreAndPostCalcNodes
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.legacy_api_information_prop import LegacyIngredientTypeEnum
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.props.link_to_xid_prop import LinkToXidProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class AddClientNodesGapFillingWorker(AbstractGapFillingWorker):
    "Add client nodes to the root node."

    def __init__(self, node: Node, gfm_factory: "AddClientNodesGapFillingFactory"):
        """Load child nodes of recipes, sub-recipes and ingredients."""
        super().__init__(node)
        self.gfm_factory = gfm_factory
        self.child_uids_by_parent_uid: dict[UUID, set[UUID]] = {}
        self.local_node_cache_by_uid: dict[UUID, PreAndPostCalcNodes] = {}

    def should_be_scheduled(self) -> bool:
        "Scheduled for only the root node."
        if isinstance(self.node, (ModeledActivityNode, ElementaryResourceEmissionNode)):
            return False

        # Run on the child of root node.
        if self.node.uid == self.node.get_calculation().child_of_root_node_uid and not self.node.get_sub_nodes():
            return True
        return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        "No special conditions needed, always ready."
        logger.debug("[AddClientNode]--> can_run_now.")
        return GapFillingWorkerStatusEnum.ready

    def _node_to_add(self, node_uid: UUID, parent_node: Optional[Node] = None) -> Node:
        pre_and_post_calc_node_to_add = self.local_node_cache_by_uid.get(node_uid, None)
        if pre_and_post_calc_node_to_add:
            if (
                parent_node
                # The following check prevents cached node from being added for sub-recipes
                # (because we do not have cache invalidation yet.):
                and parent_node.added_by == "Orchestrator"
                and self.node.get_calculation()
                and self.node.get_calculation().use_cached_final_root
                and pre_and_post_calc_node_to_add.post_calc
            ):
                node_to_add = pre_and_post_calc_node_to_add.post_calc
            else:
                node_to_add = pre_and_post_calc_node_to_add.pre_calc
        else:
            node_to_add = None
        return node_to_add

    async def add_node_mutation(
        self, calc_graph: CalcGraph, node_uid: UUID, parent_uid: UUID, duplicate_node_uid: UUID | None = None
    ) -> None:
        """Helper function to add a node mutation to the calc_graph."""
        # check if node already exists in this calc-graph:
        add_new_node_to_graph = False
        existing_node_in_graph = None

        if duplicate_node_uid:
            add_new_node_to_graph = True
        else:
            existing_node_in_graph = calc_graph.get_node_by_uid(node_uid)
            if existing_node_in_graph is None:
                add_new_node_to_graph = True

        if add_new_node_to_graph:
            parent_node = calc_graph.get_node_by_uid(parent_uid)
            node_to_add = self._node_to_add(node_uid=node_uid, parent_node=parent_node)

            if node_to_add is None:
                logger.debug(
                    f"node not found for uid {node_uid}. "
                    "node must be ModeledActivityNode or ElementaryResourceEmissionNode. Skipping..."
                )
                return

            if parent_node is None:
                raise ValueError(f"parent node not found for uid {parent_uid}")

            original_node_uid = node_to_add.uid
            if duplicate_node_uid:
                node_to_add = node_to_add.model_copy(deep=True)
                node_to_add.uid = duplicate_node_uid
                node_to_add.xid = None
                if (
                    getattr(node_to_add, "activity_date", None)
                    and node_to_add.legacy_API_information
                    and node_to_add.legacy_API_information.type == LegacyIngredientTypeEnum.recipes.value
                ):
                    # Fix to delete activity date of sub-recipes that are coming through the legacy API.
                    # We do not want to use the activity date of sub-recipes in the legacy API since the clients expect
                    # that only the activity date of the root recipe is used. We still have to persist the activity_date
                    # nevertheless because the clients can send requests of the sub-recipe as the top-recipe (even in
                    # the same batch).
                    node_to_add.activity_date = None

                    old_legacy_API_information = node_to_add.legacy_API_information.duplicate()
                    old_legacy_API_information.recipe_portions = getattr(node_to_add, "recipe_portions", None)

                    node_to_add.legacy_API_information = old_legacy_API_information

            # need to add a property to store the original_node_uid:
            node_to_add.original_node_uid = LinkToUidProp(uid=original_node_uid)

            if isinstance(node_to_add, FlowNode) and isinstance(parent_node, FlowNode):
                # we have a link from a flow to a flow, therefore we need to add on activity in between
                # Step 1: add an auxiliary activity node in between:
                logger.debug(f"create auxiliary activity node for {node_to_add}...")
                if isinstance(node_to_add.amount, QuantityProp):
                    flow_amount = node_to_add.amount.amount_for_activity_production_amount()
                    production_amount = QuantityProp.unvalidated_construct(
                        amount=flow_amount.value,
                        unit_term_uid=flow_amount.unit_term_uid,
                        for_reference=ReferenceAmountEnum.self_reference,
                    )
                else:
                    # FIXME: if "unit": "production amount", then convert properly
                    production_amount = node_to_add.amount_in_original_source_unit
                auxiliary_activity_node = LinkingActivityNode.model_construct(
                    uid=uuid4(),
                    access_group_uid=parent_node.access_group_uid if parent_node.access_group_uid else None,
                    access_group_xid=parent_node.access_group_xid if parent_node.access_group_xid else None,
                    production_amount=production_amount,
                )
                auxiliary_activity_node.gfm_state = GfmStateProp.unvalidated_construct(
                    worker_states={
                        "AddClientNodesGapFillingWorker": NodeGfmStateEnum.canceled,
                    }
                )
                logger.debug(f"create AddNodeMutation for {auxiliary_activity_node}...")
                add_auxiliary_activity_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=parent_uid,
                    new_node=auxiliary_activity_node,
                    copy=True,
                )
                calc_graph.apply_mutation(add_auxiliary_activity_node_mutation)
                # step 2: connect to the linked Flow:
                logger.debug(f"create AddNodeMutation for {node_to_add}...")
                add_child_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=auxiliary_activity_node.uid,
                    new_node=node_to_add,
                    copy=True,
                )
                calc_graph.apply_mutation(add_child_node_mutation)
            else:
                logger.debug(f"create AddNodeMutation for {node_to_add}...")
                add_child_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=parent_uid,
                    new_node=node_to_add,
                    copy=True,
                )
                calc_graph.apply_mutation(add_child_node_mutation)

            if isinstance(node_to_add, (ModeledActivityNode, ElementaryResourceEmissionNode)):
                # Nodes below modeled activity node and elementary
                # resource emission nodes are handled by inventory connector.
                return

            if isinstance(node_to_add, ActivityNode) and node_to_add.environmental_flows:
                # If the node already has environmental flows,
                # we simply use those environmental flows to compute the CO2 values.
                if self.node.get_calculation().explicitly_attach_cached_elementary_resource_emission:
                    elementary_resource_emissions_missing_in_calc_graph = set()
                    for biosphere_node_uid in node_to_add.environmental_flows.flow_quantities:
                        if not calc_graph.get_node_by_uid(biosphere_node_uid):
                            elementary_resource_emissions_missing_in_calc_graph.add(biosphere_node_uid)

                    elementary_resource_nodes_from_db = (
                        await self.gfm_factory.service_provider.node_service.find_many_by_uids(
                            list(elementary_resource_emissions_missing_in_calc_graph), include_post_calc_data=False
                        )
                    )

                    for biosphere_node_uid, value in node_to_add.environmental_flows.flow_quantities.items():
                        flow_node = FlowNode.model_construct(
                            uid=uuid4(),
                            amount=QuantityProp.unvalidated_construct(
                                value=value,
                                unit_term_uid=self.gfm_factory.emission_unit_term.uid,
                                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                            ),
                        )
                        calc_graph.apply_mutation(
                            AddNodeMutation(
                                created_by_module=self.__class__.__name__,
                                parent_node_uid=node_to_add.uid,
                                new_node=flow_node,
                                copy=False,
                            )
                        )
                        if calc_graph.get_node_by_uid(biosphere_node_uid):
                            add_edge_mutation = AddEdgeMutation(
                                created_by_module=self.__class__.__name__,
                                from_node_uid=flow_node.uid,
                                to_node_uid=biosphere_node_uid,
                            )
                            calc_graph.apply_mutation(add_edge_mutation)
                        else:
                            try:
                                calc_graph.apply_mutation(
                                    AddNodeMutation(
                                        created_by_module=self.__class__.__name__,
                                        parent_node_uid=flow_node.uid,
                                        new_node=elementary_resource_nodes_from_db[biosphere_node_uid].pre_calc,
                                        copy=True,
                                    )
                                )
                            except KeyError as e:
                                logger.error(f"Node with uid={biosphere_node_uid} does not exist in the database.")
                                raise e

                    # TODO Implement explicit attaching of cached elementary resource emission.
                    return
                else:
                    return

            child_uids = self.child_uids_by_parent_uid.get(original_node_uid)

            link_to_sub_node = getattr(node_to_add, "link_to_sub_node", None)

            if link_to_sub_node:
                db_edge_reflects_link: bool = False
                uid_to_link_to: Optional[UUID] = None

                linking_represents_matching: bool = False  # for ingredients-aggregation

                if duplicate_node_uid or link_to_sub_node.duplicate_sub_node:
                    new_node_uid = uuid4()
                else:
                    new_node_uid = None

                if child_uids:
                    if len(child_uids) == 1:
                        # Check if the uid matches the information in link_to_sub_node.
                        child_uid = list(child_uids)[0]

                        if isinstance(link_to_sub_node, LinkToUidProp):
                            uid_to_link_to = link_to_sub_node.uid

                            # TODO: As a temporary-solution we can identify the if the linking is a matching based on
                            # whether the linking was based on UID or XID. In the future, we could have an additional
                            # field in the LinkToUidProp and LinkToXidProp to identify if the linking is a matching,
                            # e.g. an author field.
                            linking_represents_matching = True

                            if child_uid == link_to_sub_node.uid:
                                db_edge_reflects_link = True

                        elif isinstance(link_to_sub_node, LinkToXidProp):
                            node_from_child_uid = self._node_to_add(node_uid=child_uid)

                            if node_from_child_uid and node_from_child_uid.xid == link_to_sub_node.xid:
                                db_edge_reflects_link = True
                                uid_to_link_to = child_uid
                        else:
                            raise ValueError(
                                f"{type(link_to_sub_node).__name__} should be one from LinkToUidProp or LinkToXidProp."
                            )

                    else:
                        logger.info(
                            f"Flow node with uid={original_node_uid} has multiple child edges. Using {link_to_sub_node}"
                            " to find the correct node."
                        )
                        if isinstance(link_to_sub_node, LinkToUidProp):
                            # TODO: As a temporary-solution we can identify the if the linking is a matching based on
                            # whether the linking was based on UID or XID. In the future, we could have an additional
                            # field in the LinkToUidProp and LinkToXidProp to identify if the linking is a matching,
                            # e.g. an author field.
                            linking_represents_matching = True

                            if link_to_sub_node.uid in child_uids:
                                db_edge_reflects_link = True
                                uid_to_link_to = link_to_sub_node.uid

                        elif isinstance(link_to_sub_node, LinkToXidProp):
                            for child_uid in child_uids:
                                node_from_child_uid = self._node_to_add(node_uid=child_uid)

                                if node_from_child_uid and node_from_child_uid.xid == link_to_sub_node.xid:
                                    db_edge_reflects_link = True
                                    uid_to_link_to = child_uid
                                    break
                        else:
                            raise ValueError(
                                f"{type(link_to_sub_node).__name__} should be one from LinkToUidProp or LinkToXidProp."
                            )

                if uid_to_link_to is not None and db_edge_reflects_link is True:
                    await self.add_node_mutation(
                        calc_graph, uid_to_link_to, node_to_add.uid, duplicate_node_uid=new_node_uid
                    )
                    if linking_represents_matching and isinstance(node_to_add, FoodProductFlowNode):
                        node_to_add.add_prop_for_amount_per_matching_required_for_flow(
                            calc_graph, uid_to_link_to, self.gfm_factory.gram_term.uid
                        )

                elif uid_to_link_to is None and db_edge_reflects_link is True:
                    raise ValueError(
                        f"uid_to_link_to cannot be {uid_to_link_to} if "
                        f"db_edge_reflects_link is {db_edge_reflects_link}."
                    )
                else:
                    logger.warning(
                        "db_edge_reflects_link is False",
                        uid_to_link_to=str(uid_to_link_to),
                        link_to_sub_node=link_to_sub_node.model_dump(),
                    )
                    if uid_to_link_to is None:
                        product_mgr = self.gfm_factory.service_provider.postgres_db.product_mgr
                        if isinstance(link_to_sub_node, LinkToUidProp):
                            uid_to_link_to = link_to_sub_node.uid
                        elif isinstance(link_to_sub_node, LinkToXidProp):
                            xid_to_link_to = link_to_sub_node.xid
                            xid_access_group_uid = XidAccessGroupUid(
                                xid=xid_to_link_to, access_group_uid=node_to_add.access_group_uid
                            )
                            mapping_dict = await product_mgr.map_xid_access_group_uids_to_uids([xid_access_group_uid])
                            uid_to_link_to = mapping_dict.get(xid_access_group_uid, None)
                            if uid_to_link_to is None:
                                raise ValueError(
                                    f"link_to_sub_node.xid={link_to_sub_node.xid} with "
                                    f"access_group_uid={node_to_add.access_group_uid} does not exist in the database."
                                )

                    # Add missing information (children of link_to_sub_node) to local caches.
                    await self.update_child_uids_by_parent_uid_and_local_node_cache(
                        root_node_uid=uid_to_link_to, service_provider=calc_graph.service_provider
                    )
                    await self.add_node_mutation(
                        calc_graph, uid_to_link_to, node_to_add.uid, duplicate_node_uid=new_node_uid
                    )

            elif child_uids:
                for child_uid in child_uids:
                    new_node_uid = None
                    if duplicate_node_uid:
                        new_node_uid = uuid4()
                    elif link_to_sub_node := getattr(node_to_add, "link_to_sub_node", None):
                        if link_to_sub_node.duplicate_sub_node:
                            new_node_uid = uuid4()

                    await self.add_node_mutation(
                        calc_graph, child_uid, node_to_add.uid, duplicate_node_uid=new_node_uid
                    )
            elif isinstance(node_to_add, ActivityNode):
                # this means that a recipe has no sub-recipes or ingredients
                logger.warning(
                    "Client-received activity has no sub-flows",
                    node=node_to_add,
                    original_node_uid=node_to_add.original_node_uid,
                )
                calc_graph.append_data_errors_log_entry(
                    f"Client-received activity {node_to_add} has no sub-flows",
                    error_classification=ErrorClassification.missing_sub_flows_in_client_sent_data,
                    additional_specification={
                        "xid": node_to_add.xid,
                    },
                    node_uid=node_to_add.uid,
                )
                return

        else:
            logger.debug(
                f"dont add child node because it already exists in the graph ({existing_node_in_graph}); only add edge."
            )
            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=parent_uid,
                to_node_uid=existing_node_in_graph.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

    async def update_child_uids_by_parent_uid_and_local_node_cache(
        self, root_node_uid: UUID, service_provider: ServiceProvider
    ) -> None:
        child_uids_by_parent_uid = {}
        edges = await service_provider.postgres_db.get_graph_mgr().get_infinite_depth_sub_graph_by_uid(root_node_uid)
        for edge in edges:
            if edge.parent_uid in child_uids_by_parent_uid:
                child_uids_by_parent_uid[edge.parent_uid].add(edge.child_uid)
            else:
                child_uids_by_parent_uid[edge.parent_uid] = {edge.child_uid}

        all_encountered_node_uids: set[UUID] = set()

        for key, val in child_uids_by_parent_uid.items():
            all_encountered_node_uids.add(key)
            for child_uid in val:
                all_encountered_node_uids.add(child_uid)

        self.child_uids_by_parent_uid.update(child_uids_by_parent_uid)
        self.local_node_cache_by_uid.update(
            await self.gfm_factory.service_provider.node_service.find_many_by_uids(
                list(all_encountered_node_uids),
                exclude_node_types=["ModeledActivityNode", "ElementaryResourceEmissionNode"],
                include_post_calc_data=self.node.get_calculation()
                and self.node.get_calculation().use_cached_final_root,
            )
        )
        access_groups_uids = set(
            pre_and_post_calc_node.pre_calc.access_group_uid
            for pre_and_post_calc_node in self.local_node_cache_by_uid.values()
        )
        access_groups_uids.discard(None)
        access_groups_dict: dict[
            UUID, AccessGroup
        ] = await service_provider.postgres_db.access_group_mgr.find_many_by_uids(list(access_groups_uids))
        for pre_and_post_calc_node in self.local_node_cache_by_uid.values():
            access_group = access_groups_dict.get(pre_and_post_calc_node.pre_calc.access_group_uid)

            if access_group:
                pre_and_post_calc_node.pre_calc.namespace_uid = access_group.namespace_uid
                if pre_and_post_calc_node.post_calc is not None:
                    pre_and_post_calc_node.post_calc.namespace_uid = access_group.namespace_uid

            if access_group and access_group.data:
                pre_and_post_calc_node.pre_calc.ignore_fields(access_group.data.get("ignore_fields"))
                # for post_calc we don't need to ignore fields because it shouldn't include the
                # corresponding client-sent-fields in the first place (since we ignored them in the calculation
                # that generated the post_calc_data-field)

    async def run(self, calc_graph: CalcGraph) -> None:
        "Run the worker."
        logger.debug(f"start running gap-filling-module AddClientNodesGFM on {self.node}")

        self.child_uids_by_parent_uid = {}
        self.local_node_cache_by_uid = {}

        await self.update_child_uids_by_parent_uid_and_local_node_cache(
            root_node_uid=self.node.uid, service_provider=calc_graph.service_provider
        )

        child_uids_of_current_node = self.child_uids_by_parent_uid.get(self.node.uid)
        if child_uids_of_current_node:
            for child_uid in child_uids_of_current_node:
                await self.add_node_mutation(calc_graph, child_uid, self.node.uid)
        elif isinstance(self.node, ActivityNode):
            # this means that a recipe has no sub-recipes or ingredients
            logger.warning("Client-received activity has no sub-flows", node=self.node)
            calc_graph.append_data_errors_log_entry(
                f"Client-received activity {self.node} has no sub-flows",
                error_classification=ErrorClassification.missing_sub_flows_in_client_sent_data,
                additional_specification={
                    "xid": self.node.xid,
                },
                node_uid=self.node.uid,
            )
            return


class AddClientNodesGapFillingFactory(AbstractGapFillingFactory):
    "Factory."

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.unit_term_access_group_uid = root_unit_term.access_group_uid
        # For efficiency, we do not retrieve from the database specific units of the emissions. We simply
        # assume that the flow amount unit matches the units of the emissions.
        # To mark this assumption, we simply put a generic Emission units term.
        self.emission_unit_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_units_emission", self.unit_term_access_group_uid)
        ]
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", root_unit_term.access_group_uid)
        ]

    async def init_cache(self) -> None:
        "Do nothing."
        pass

    def spawn_worker(self, node: Node) -> AddClientNodesGapFillingWorker:
        "Spawn worker."
        return AddClientNodesGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = AddClientNodesGapFillingFactory
