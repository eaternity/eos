"""Aggregation gap filling module."""

import uuid
from enum import Enum
from typing import List, Optional, Type

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from structlog import get_logger

from core.domain.data_error import ErrorClassification
from core.domain.nodes import ElementaryResourceEmissionNode, ModeledActivityNode
from core.domain.nodes.flow.practice_flow_node import PracticeFlowNode
from core.domain.nodes.node import Node
from core.domain.props import (
    AnimalProductsProp,
    NamesProp,
    QuantityPackageProp,
    RainforestCriticalProductsProp,
    ReferencelessQuantityProp,
)
from core.domain.props.quantity_package_prop_with_additional_info_on_quantities import (
    QuantityPackagePropWithAdditionalInfoOnQuantities,
)
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop_with_additional_info import (
    ReferencelessQuantityPropWithAdditionalInfo,
)
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class AggregatedDataEnum(str, Enum):
    """Enumerator for aggregated data."""

    nutrient_values = "nutrient_values"
    amount_per_category_in_flow = "amount_per_category_in_flow"
    rainforest_critical_products = "rainforest_critical_products"
    animal_products = "animal_products"
    amount_per_glossary_link_required_for_flow = "amount_per_glossary_link_required_for_flow"
    amount_per_category_required_for_flow = "amount_per_category_required_for_flow"
    amount_per_matching_required_for_flow = "amount_per_matching_required_for_flow"
    dfu_per_category_required_for_flow = "dfu_per_category_required_for_flow"
    impact_assessment_per_glossary_link = "impact_assessment_per_glossary_link"
    impact_assessment_per_category = "impact_assessment_per_category"
    impact_assessment_per_matching = "impact_assessment_per_matching"


class ReferencelessQuantity:
    # Lighter ReferencelessQuantity used here temporarily for efficiency.
    def __init__(
        self,
        value: float,
        unit_term_uid: uuid.UUID,
        examples: Optional[List[str]] = None,
        dfu_per_100g: Optional[float] = None,
    ):
        self.value = value
        self.unit_term_uid = unit_term_uid
        self.examples = examples
        self.dfu_per_100g = dfu_per_100g


class QuantityPackage:
    # Lighter QuantityPackage used here temporarily for efficiency.
    def __init__(self, quantities: dict[uuid.UUID, ReferencelessQuantity]):
        self.quantities = quantities


class AggregationHelper:
    """Helper class containing methods for aggregation."""

    def __init__(self, gfm_factory: "AbstractAggregationGapFillingFactory"):
        self.gfm_factory = gfm_factory
        self.aggregated_props: list[AggregatedDataEnum] = self.gfm_factory.aggregated_props
        self.required_data = self.gfm_factory.required_data
        # The following can be removed after nutrient_values property is introduced.
        self.aggregated_prop_name: dict[AggregatedDataEnum, str] = {
            AggregatedDataEnum(member): AggregatedDataEnum(member).value for member in AggregatedDataEnum
        }
        self.aggregated_prop_name[AggregatedDataEnum.nutrient_values] = "aggregated_nutrients"

        self.prop_to_compute_from_aggregation: dict[AggregatedDataEnum, str] = {
            AggregatedDataEnum.nutrient_values: "daily food unit",
            AggregatedDataEnum.amount_per_category_in_flow: "vita score and Eaternity categorization",
            AggregatedDataEnum.rainforest_critical_products: "Rainforest rating",
            AggregatedDataEnum.animal_products: "Animal welfare rating",
        }

    def retrieve_data_from_node(self, node: Node, prop_name: str) -> QuantityPackageProp:
        """Retrieve known data (nutrients, food categories).

        :param node: activity node for which we wish to determine the aggregated values of the product.
        :param prop_name: type of data we wish to retrieve.
        :return: known data values.
        """
        if len(node.get_parent_nodes()) > 0:
            if hasattr(node.get_parent_nodes()[0], prop_name) and (
                flow_prop := getattr(node.get_parent_nodes()[0], prop_name)
            ):
                return flow_prop
            if prop_name != self.aggregated_prop_name[prop_name]:
                if hasattr(node.get_parent_nodes()[0], self.aggregated_prop_name[prop_name]) and (
                    flow_prop := getattr(node.get_parent_nodes()[0], self.aggregated_prop_name[prop_name])
                ):
                    return flow_prop

    def retrieve_all_data_from_node(self, node: Node) -> dict[str, QuantityPackageProp]:
        """Retrieve all property data.

        :param node: activity node for which we wish to determine the aggregated values of the product.
        :return: dictionary of known property data.
        """
        data_dict: dict[str, QuantityPackageProp] = {}
        for prop_name in self.aggregated_props:
            data_dict[prop_name] = self.retrieve_data_from_node(node, prop_name)
        return data_dict

    def check_for_missing_data(
        self, data: dict[uuid.UUID, ReferencelessQuantity], prop_name: AggregatedDataEnum
    ) -> list[uuid.UUID]:
        """Check for gaps in provided data.

        For nutrients, fat, protein, water, and energy values are required for daily food unit calculation.
        :param data: dictionary containing data.
        :param prop_name: name of the property (e.g., nutrient_values or amount_per_category_in_flow).
        :return: all missing data fields that are required.
        """
        required_prop_data = self.required_data[prop_name]
        if required_prop_data:
            missing = [d for d in required_prop_data if d not in data]
            return missing
        else:
            return []

    @staticmethod
    def multiply_data_w_scalar(
        data: dict[uuid.UUID, ReferencelessQuantity | ReferencelessQuantityProp], scalar: float
    ) -> dict[uuid.UUID, ReferencelessQuantity]:
        """Multiplies each value in the dictionary with a scalar.

        :param data: dictionary containing data values.
        :param scalar: scalar with which to multiply the values.
        :return: dictionary with each value multiplied by a scalar.
        """
        return {
            key: ReferencelessQuantity(
                value=data[key].value * scalar,
                unit_term_uid=data[key].unit_term_uid,
                examples=getattr(data[key], "examples", None),
                dfu_per_100g=getattr(data[key], "dfu_per_100g", None),
            )
            for key in data
        }

    def add_two_dictionaries(
        self,
        sc1: float,
        dict1: dict[uuid.UUID, ReferencelessQuantity | ReferencelessQuantityProp],
        sc2: float,
        dict2: dict[uuid.UUID, ReferencelessQuantity | ReferencelessQuantityProp],
    ) -> dict[uuid.UUID, ReferencelessQuantity]:
        """Perform sc1 * dict1 + sc2 * dict2.

        Sum and multiplication is for the value in every dictionary key.
        :param sc1: scalar1
        :param dict1: dictionary1
        :param sc2: scalar2
        :param dict2: dictionary2.
        """
        summed_dict: dict[uuid.UUID, tuple[float, uuid.UUID]] = {}
        for key in set(dict1) | set(dict2):
            examples_1 = []
            examples_2 = []
            dfu_per_100g = None
            if key in dict1 and key in dict2:
                if dict1[key].unit_term_uid != dict2[key].unit_term_uid:
                    terms_by_uid = self.gfm_factory.service_provider.glossary_service.terms_by_uid
                    unit1_term = terms_by_uid[dict1[key].unit_term_uid]
                    unit2_term = terms_by_uid[dict2[key].unit_term_uid]

                    if (unit1_term.sub_class_of == unit2_term.sub_class_of) and (
                        unit1_term.sub_class_of
                        in (self.gfm_factory.gram_term.sub_class_of, self.gfm_factory.kilojoule_term.sub_class_of)
                    ):
                        if unit1_term.sub_class_of == self.gfm_factory.gram_term.sub_class_of:
                            unit = self.gfm_factory.gram_term.uid
                        else:
                            unit = self.gfm_factory.kilojoule_term.uid

                        val1 = UnitWeightConverter.unit_convert_to_g_kj(
                            dict1[key], terms_by_uid, self.gfm_factory.gram_term, self.gfm_factory.kilojoule_term
                        )
                        val2 = UnitWeightConverter.unit_convert_to_g_kj(
                            dict2[key], terms_by_uid, self.gfm_factory.gram_term, self.gfm_factory.kilojoule_term
                        )
                    else:
                        logger.error("[Aggregation GFM] units cannot be matched.")
                        return {}
                else:
                    unit = dict1[key].unit_term_uid
                    val1 = dict1[key].value
                    val2 = dict2[key].value
                examples_1 = list(dict1[key].examples) if dict1[key].examples else []
                examples_2 = list(dict2[key].examples) if dict2[key].examples else []
                if dict1[key].dfu_per_100g is None and dict2[key].dfu_per_100g is None:
                    dfu_per_100g = None
                else:
                    assert dict1[key].dfu_per_100g is not None and dict2[key].dfu_per_100g is not None
                    if dict1[key].dfu_per_100g != 0.0 and dict2[key].dfu_per_100g != 0.0:
                        if abs(dict1[key].dfu_per_100g - dict2[key].dfu_per_100g) < 1e-8:
                            # If the dfu_per_100g is the same, we can use either of the two values.
                            dfu_per_100g = dict1[key].dfu_per_100g
                        else:
                            # This should in theory not happen, but currently it can if an ingredient
                            # was once specified as dried and once not but both match to the node.
                            # As a quick fix, we currently just take the smaller dfu_per_100g to get a more conservative
                            # estimate (i.e. higher co2 per dfu).
                            # TODO: properly handle this case in the future (e.g. by identifying the two as two
                            # different matchings).
                            dfu_per_100g = min(dict1[key].dfu_per_100g, dict2[key].dfu_per_100g)
                            logger.warning(
                                f"dfu_per_100g values differ for the same matching {key}: {dict1[key].dfu_per_100g} "
                                f"and {dict2[key].dfu_per_100g}. Using the smaller value."
                            )
                    # it can happen (e.g. when one of the values is 0.0) that the dfu_per_100g is also 0.0.
                    # In this case, we want to use the dfu_per_100g from the other value.
                    elif dict1[key].dfu_per_100g == 0.0 and dict2[key].dfu_per_100g != 0.0:
                        dfu_per_100g = dict2[key].dfu_per_100g
                    elif dict1[key].dfu_per_100g != 0.0 and dict2[key].dfu_per_100g == 0.0:
                        dfu_per_100g = dict1[key].dfu_per_100g
                    else:
                        dfu_per_100g = 0.0
            elif key in dict1:
                unit = dict1[key].unit_term_uid
                val1 = dict1[key].value
                val2 = 0.0
                examples_1 = list(dict1[key].examples) if dict1[key].examples else []
                dfu_per_100g = dict1[key].dfu_per_100g
            elif key in dict2:
                unit = dict2[key].unit_term_uid
                val1 = 0.0
                val2 = dict2[key].value
                examples_2 = list(dict2[key].examples) if dict2[key].examples else []
                dfu_per_100g = dict2[key].dfu_per_100g
            else:
                raise ValueError(f"Unit is not present in {dict1} or {dict2}")

            if examples_1 or examples_2:
                examples = list(set(examples_1 + examples_2))
            else:
                examples = None

            summed_dict[key] = (sc1 * val1 + sc2 * val2, unit, examples, dfu_per_100g)
        return {
            k: ReferencelessQuantity(value=v[0], unit_term_uid=v[1], examples=v[2], dfu_per_100g=v[3])
            for k, v in summed_dict.items()
        }


class AbstractAggregationGapFillingWorker(AbstractGapFillingWorker):
    """Aggregation gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "AbstractAggregationGapFillingFactory"):
        """Aggregate information (nutrition, food categories, etc.) from sub-nodes into the parent nodes."""
        super().__init__(node)
        self.gfm_factory = gfm_factory
        self.helper = AggregationHelper(gfm_factory)

    def should_be_scheduled(self) -> bool:
        """Only run on root node."""
        if len(self.node.get_parent_nodes()) == 0:
            return True
        else:
            logger.debug("[Aggregation] not on root node --> not scheduled.")
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """We want to perform aggregation after all the flow amounts for the ingredients have been calculated."""
        # return MatrixCalculationGapFillingWorker.graph_building_gfm_workers_finished(self)
        pass

    async def run(self, calc_graph: CalcGraph) -> None:
        """Depth-first graph traversal to fill in missing data (nutrients, food categories, etc.).

        If data are available from raw_input or from glossary, use these values.
        Otherwise, aggregate from sub-ingredients.
        """
        if any(
            error.error_classification == ErrorClassification.missing_matching for error in calc_graph.data_errors_log
        ):
            logger.warn("Skipping Aggregation GFM because matching failed.")
            return
        elif any(
            error.error_classification == ErrorClassification.missing_lca_inventory
            for error in calc_graph.data_errors_log
        ):
            logger.warn("Skipping Aggregation GFM because at least flow-node has no LCA inventory node attached.")
            return
        elif any(
            error.error_classification == ErrorClassification.failed_amount_estimation
            for error in calc_graph.data_errors_log
        ):
            logger.warn("Skipping Aggregation GFM because flow amount estimation failed.")
            return

        child_activity_node = self.node.get_sub_nodes()[0]
        stack = [child_activity_node.uid]

        # {aggregated_prop_type: {node_uid: QuantityPackageProp}}
        # aggregated_prop_type = aggregated_nutrients, amount_per_category_in_flow, etc.
        aggregated_data: dict[str, dict[uuid, QuantityPackage]] = {key: {} for key in self.helper.aggregated_props}

        # collection of uids for available data that we don't need to rely on the aggregated one.
        data_known: dict[str, dict] = {key: {} for key in self.helper.aggregated_props}

        visited = set()
        visited_flow = set()

        # Set to keep a list of xids for nutrient files that are incomplete.
        incomplete_nutr_file_xids: set = set()

        # uid of the characterization method used in the aggregated impact assessments.
        char_method_uid = None

        while stack:
            if stack[-1] not in visited:
                visited.add(stack[-1])
                current_activity = calc_graph.get_node_by_uid(stack[-1])

                self.create_and_update_amount_props_if_necessary(
                    current_activity, calc_graph, self.helper.aggregated_props
                )

                known_data = self.helper.retrieve_all_data_from_node(
                    current_activity
                )  # Nutrients in 100g of ingredient.
                for prop in self.helper.aggregated_props:
                    if (
                        known_data[prop]
                        and isinstance(known_data[prop], QuantityPackageProp)
                        and known_data[prop].quantities
                    ):
                        # multiply 0.01 to convert to per g,
                        try:
                            amount_per_prod_amount = self.helper.multiply_data_w_scalar(
                                known_data[prop].amount_for_100g().quantities,
                                0.01 * self.get_production_amount_in_grams(current_activity),
                            )
                        except ValueError as e:
                            logger.error(f"Could not aggregate quantity packages: {e}")
                            calc_graph.append_data_errors_log_entry(f"Could not aggregate quantity packages: {e}")
                            return

                        if missing_data := self.helper.check_for_missing_data(amount_per_prod_amount, prop):
                            # Add warnings:
                            if prop == AggregatedDataEnum.nutrient_values:
                                if len(current_activity.get_parent_nodes()) == 1:
                                    if nutrient_values := current_activity.get_parent_nodes()[0].nutrient_values:
                                        if nutrient_term := nutrient_values.get_prop_term():
                                            if nutrient_term.xid not in incomplete_nutr_file_xids:
                                                # Insufficient nutrient data for daily food unit calculation.
                                                # Aggregate nutrient values from sub-nodes.
                                                logger.warn(
                                                    f"Missing nutrients {missing_data} in "
                                                    f"nutrients file {nutrient_term.xid}"
                                                )
                                                incomplete_nutr_file_xids.add(nutrient_term.xid)
                                                calc_graph.append_data_errors_log_entry(
                                                    f"Missing nutrients {missing_data} in "
                                                    f"nutrients file {nutrient_term.xid}"
                                                )

                            elif prop == AggregatedDataEnum.amount_per_category_in_flow:
                                logger.debug(
                                    "Not all information required for food categorization is provided. "
                                    "Aggregating from child nodes instead.",
                                    node_uid=current_activity.uid,
                                    missing_data=missing_data,
                                )

                            # Determine whether to use the known_data. Use known_data only if current activity
                            # is the deepest level node.
                            if len(current_activity.get_sub_nodes()) == 0:
                                use_known_data = True
                            elif all(
                                len(sub_node.get_sub_nodes()) == 0 for sub_node in current_activity.get_sub_nodes()
                            ):
                                use_known_data = False
                            elif all(
                                self.deepest_level_condition(current_activity, sub_node.get_sub_nodes()[0])
                                for sub_node in current_activity.get_sub_nodes()
                            ):
                                use_known_data = True
                            else:
                                use_known_data = False

                        else:
                            use_known_data = True

                        if use_known_data:
                            quantities_for_prod_amount = QuantityPackage(quantities=amount_per_prod_amount)
                            data_known[prop][current_activity.uid] = quantities_for_prod_amount
                            if char_method_uid is None and prop in (
                                AggregatedDataEnum.impact_assessment_per_glossary_link,
                                AggregatedDataEnum.impact_assessment_per_category,
                            ):
                                char_method_uid = getattr(known_data[prop], "prop_term_uid", None)

            all_sub_nodes_visited = True  # Flag for checking if we have visited all sub_nodes.
            current_activity = calc_graph.get_node_by_uid(stack[-1])
            for sub_flow in current_activity.get_sub_nodes():
                if len(sub_flow.get_sub_nodes()) == 0:
                    # Leaf flow reached. Simply retrieve values from leaf flows and aggregate them into all
                    # parent nodes.
                    if sub_flow.uid not in visited_flow:
                        visited_flow.add(sub_flow.uid)
                        for prop in self.helper.aggregated_props:
                            self.initialize_aggregated_data(aggregated_data[prop], current_activity.uid)
                            if (getattr(sub_flow, prop, None)) or (
                                prop != self.helper.aggregated_prop_name[prop]
                                and getattr(sub_flow, self.helper.aggregated_prop_name[prop], None)
                            ):
                                try:
                                    flow_amount = self.get_flow_amount_in_grams(sub_flow)
                                except ValueError as e:
                                    logger.error(f"Could not aggregate quantity packages: {e}")
                                    calc_graph.append_data_errors_log_entry(
                                        f"Could not aggregate quantity packages: {e}"
                                    )
                                    return

                                if getattr(sub_flow, prop, None):
                                    amount_in_flow = getattr(sub_flow, prop).amount_for_100g()
                                elif getattr(sub_flow, self.helper.aggregated_prop_name[prop], None):
                                    amount_in_flow = getattr(
                                        sub_flow, self.helper.aggregated_prop_name[prop], None
                                    ).amount_for_100g()

                                # divide by 100 to convert to per g,
                                # multiply by flow amount to scale by ratios of ingredients.
                                aggregated_data[prop][
                                    current_activity.uid
                                ].quantities = self.helper.add_two_dictionaries(
                                    1.0,
                                    aggregated_data[prop][current_activity.uid].quantities,
                                    0.01 * flow_amount,
                                    amount_in_flow.quantities,
                                )
                                if char_method_uid is None and prop in (
                                    AggregatedDataEnum.impact_assessment_per_glossary_link,
                                    AggregatedDataEnum.impact_assessment_per_category,
                                ):
                                    char_method_uid = getattr(amount_in_flow, "prop_term_uid", None)
                else:
                    sub_activity = sub_flow.get_sub_nodes()[0]  # Visiting the activity_node next level down.
                    if not (
                        sub_activity.uid in visited or self.deepest_level_condition(current_activity, sub_activity)
                    ):
                        # Add the first unvisited node to the stack.
                        stack.append(sub_activity.uid)
                        all_sub_nodes_visited = False
                        break

            if all_sub_nodes_visited:
                for prop in self.helper.aggregated_props:
                    if current_activity.uid in aggregated_data[prop]:
                        final_aggregate = aggregated_data[prop][current_activity.uid]
                        if final_aggregate and current_activity.uid not in data_known[prop]:
                            await self.apply_parent_flows_mutation(
                                self.helper.aggregated_prop_name[prop],
                                current_activity,
                                calc_graph,
                                final_aggregate,
                                prop_term_uid=(
                                    char_method_uid
                                    if prop
                                    in (
                                        AggregatedDataEnum.impact_assessment_per_glossary_link,
                                        AggregatedDataEnum.impact_assessment_per_category,
                                    )
                                    else None
                                ),
                            )

                    if current_activity.uid in data_known[prop]:
                        current_aggregated_prop = data_known[prop][current_activity.uid]
                    elif current_activity.uid in aggregated_data[prop]:
                        current_aggregated_prop = aggregated_data[prop][current_activity.uid]
                    else:
                        current_aggregated_prop = None

                    if current_aggregated_prop is not None:
                        for parent_flow in current_activity.get_parent_nodes():
                            if parent_flow.get_parent_nodes():
                                parent_activity = parent_flow.get_parent_nodes()[0]
                                self.initialize_aggregated_data(aggregated_data[prop], parent_activity.uid)
                                # Continue aggregating nutrients for the parent_activity if the nutrient value for the
                                # current activity are available.
                                try:
                                    this_production_amount = self.get_production_amount_in_grams(current_activity)
                                    flow_amount = self.get_flow_amount_in_grams(parent_flow)
                                except ValueError as e:
                                    logger.error(f"Could not aggregate quantity packages: {e}")
                                    calc_graph.append_data_errors_log_entry(
                                        f"Could not aggregate quantity packages: {e}"
                                    )
                                    return

                                if this_production_amount == 0.0:
                                    if flow_amount != 0.0:
                                        logger.error(
                                            f"Production amount is 0.0, but flow amount is {flow_amount} for "
                                            f"node {parent_flow}"
                                        )
                                        calc_graph.append_data_errors_log_entry(
                                            f"Production amount is 0.0, but flow amount is {flow_amount} for "
                                            f"node {parent_flow}"
                                        )
                                        return
                                    else:
                                        value = 0.0
                                else:
                                    value = flow_amount / this_production_amount
                                flow_amount_per_production = value

                                aggregated_data[prop][
                                    parent_activity.uid
                                ].quantities = self.helper.add_two_dictionaries(
                                    1.0,
                                    aggregated_data[prop][parent_activity.uid].quantities,
                                    flow_amount_per_production,
                                    current_aggregated_prop.quantities,
                                )
                stack.pop()

    @staticmethod
    def get_prop_class(
        aggregated_prop_name: str,
    ) -> Type[QuantityPackageProp | RainforestCriticalProductsProp | AnimalProductsProp]:
        match aggregated_prop_name:
            case AggregatedDataEnum.rainforest_critical_products:
                prop_class = RainforestCriticalProductsProp
            case AggregatedDataEnum.animal_products:
                prop_class = AnimalProductsProp
            case AggregatedDataEnum.amount_per_matching_required_for_flow:
                prop_class = QuantityPackagePropWithAdditionalInfoOnQuantities
            case _:
                prop_class = QuantityPackageProp
        return prop_class

    async def apply_parent_flows_mutation(
        self,
        aggregated_prop_name: str,
        node: Node,
        calc_graph: CalcGraph,
        data: QuantityPackage,
        prop_term_uid: uuid.UUID = None,
    ) -> None:
        """Apply prop mutation to add daily food unit for local node.

        The daily food unit and nutrients of the flow required to produce production amount of the
        activity node that is the parent of the flow node. That is the ingredient nutrients and
        daily food unit in the quantity needed to generate production_amount of the parent activity.
        :param aggregated_prop_name: name of the aggregated property
        :param node: parent flows of this node are mutated.
        :param calc_graph: calculation graph.
        :param data: data values per 1kg of product.
        """
        module_name = self.__class__.__name__
        try:
            this_production_amount = self.get_production_amount_in_grams(node)
        except ValueError:
            logger.debug("Production amount is None.")
            return
        if this_production_amount == 0.0:
            logger.debug("Production amount is 0.0")

        prop_class = self.get_prop_class(aggregated_prop_name)
        for parent_flow in node.get_parent_nodes():
            if hasattr(parent_flow, aggregated_prop_name):
                prop_data = getattr(parent_flow, aggregated_prop_name)
                if prop_data is None:
                    try:
                        flow_amount = self.get_flow_amount_in_grams(parent_flow)
                    except ValueError:
                        logger.debug("Parent flow has no flow_amount")
                        return

                    if this_production_amount == 0.0:
                        if flow_amount != 0.0:
                            logger.error(f"Production amount is 0.0, but flow amount is {flow_amount} for node {node}")
                            calc_graph.append_data_errors_log_entry(
                                f"Production amount is 0.0, but flow amount is {flow_amount} for node {node}"
                            )
                            return
                        else:
                            value = 0.0
                    else:
                        value = flow_amount / this_production_amount

                    aggregated_quantities = self.helper.multiply_data_w_scalar(data.quantities, value)

                    if aggregated_prop_name == AggregatedDataEnum.amount_per_matching_required_for_flow:
                        quantities = {
                            key: ReferencelessQuantityPropWithAdditionalInfo.unvalidated_construct(
                                value=val.value,
                                unit_term_uid=val.unit_term_uid,
                                examples=val.examples,
                                dfu_per_100g=val.dfu_per_100g,
                            )
                            for key, val in aggregated_quantities.items()
                        }
                    else:
                        quantities = {
                            key: ReferencelessQuantityProp.unvalidated_construct(
                                value=val.value, unit_term_uid=val.unit_term_uid
                            )
                            for key, val in aggregated_quantities.items()
                        }

                    calc_graph.apply_mutation(
                        PropMutation(
                            created_by_module=module_name,
                            node_uid=parent_flow.uid,
                            prop_name=aggregated_prop_name,
                            prop=(
                                prop_class.unvalidated_construct(
                                    quantities=quantities,
                                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                                    prop_term_uid=prop_term_uid,
                                )
                                if prop_class == QuantityPackageProp
                                else prop_class.unvalidated_construct(
                                    quantities=quantities,
                                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                                )
                            ),
                        )
                    )

    def get_flow_amount_in_grams(self, flow_node: Node) -> float:
        """Return flow amount in grams."""
        if flow_node.amount is None:
            raise ValueError(f"flow {flow_node} has no flow amount (converted amount).")

        return UnitWeightConverter.get_flow_amount_in_grams(flow_node, self.gfm_factory.gram_term)

    def get_production_amount_in_grams(self, activity_node: Node) -> float:
        """Return production amount in grams."""
        if activity_node.production_amount is None:
            raise ValueError(f"activity {activity_node} has no production amount.")
        return UnitWeightConverter.get_production_amount_in_grams(activity_node, self.gfm_factory.gram_term)

    def deepest_level_condition(self, current_activity: Node, sub_activity: Node) -> bool:
        """Whether we have reached the deepest level of the graph from which we aggregate."""
        is_practice_flow = isinstance(sub_activity.get_parent_nodes()[0], PracticeFlowNode)
        if is_practice_flow:
            return True
        is_elementary_resource_emission = isinstance(sub_activity, ElementaryResourceEmissionNode)
        if is_elementary_resource_emission:
            return True
        is_deep_modeled_activity = isinstance(current_activity, ModeledActivityNode) and isinstance(
            sub_activity, ModeledActivityNode
        )
        if is_deep_modeled_activity:
            return True
        is_non_food = isinstance(sub_activity.get_parent_nodes()[0].product_name, NamesProp) and any(
            t.term_uid in self.gfm_factory.non_food_term_uids
            for t in sub_activity.get_parent_nodes()[0].product_name.terms
        )
        if is_non_food:
            return True
        return False

    @staticmethod
    def initialize_aggregated_data(
        aggregated_data: dict,
        uid: uuid.UUID,
    ) -> None:
        """Initialize the aggregated data container for a node."""
        if uid not in aggregated_data:
            aggregated_data[uid] = QuantityPackage(quantities={})

    def create_and_update_amount_props_if_necessary(
        self, node: Node, calc_graph: CalcGraph, aggregated_props: list[AggregatedDataEnum]
    ) -> None:
        """Create amount properties and update amount_props if necessary."""
        pass  # only necessary for the quantities in post_nutrients_aggregation


class AbstractAggregationGapFillingFactory(AbstractGapFillingFactory):
    """Aggregation gap filling factory."""

    required_nutrients = ["fat", "protein", "water", "energy"]

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.required_data = {AggregatedDataEnum(member): [] for member in AggregatedDataEnum}
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        root_nutr_name_term = self.service_provider.glossary_service.root_subterms.get("EOS_nutrient_names")
        self.nutr_ag_uid = root_nutr_name_term.access_group_uid
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", root_unit_term.access_group_uid)
        ]
        self.kilojoule_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilojoule", root_unit_term.access_group_uid)
        ]
        self.required_data[AggregatedDataEnum.nutrient_values] = [
            self.service_provider.glossary_service.terms_by_xid_ag_uid[(f"EOS_{req_nutr}", self.nutr_ag_uid)].uid
            for req_nutr in self.required_nutrients
        ]

        self.non_food_term_uids: frozenset[uuid.UUID] = frozenset()
        self.health_category_term_uids: list[uuid.UUID] = []
        self.aggregated_props: list[AggregatedDataEnum] = []  # needs to be filled in by the subclass

    async def init_cache(self) -> None:
        """Initialize cache for aggregation gap filling factory."""
        root_access_group_uid = self.service_provider.glossary_service.root_term.access_group_uid
        foodex2_access_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )

        self.non_food_term_uids = frozenset(
            [
                self.service_provider.glossary_service.terms_by_xid_ag_uid[("EAT-0002", foodex2_access_group_uid)].uid,
                self.service_provider.glossary_service.terms_by_xid_ag_uid[
                    ("EOS_data_error", root_access_group_uid)
                ].uid,
            ]
        )

        root_food_categories_term = self.service_provider.glossary_service.root_subterms["Root_Food_Categories"]
        all_food_category_terms = self.service_provider.glossary_service.get_sub_terms_by_uid(
            root_food_categories_term.uid
        )
        self.health_category_term_uids = [
            term.uid
            for term in all_food_category_terms
            if isinstance(term.xid, str) and term.xid.startswith("EOS_Diet-")
        ]

    def spawn_worker(self, node: Node) -> AbstractAggregationGapFillingWorker:
        """Spawn aggregation gap filling worker."""
        return AbstractAggregationGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = AbstractAggregationGapFillingFactory
