"""Transportation mode distance gap filling module."""

import math
from typing import Optional
from uuid import UUID, uuid4

from gap_filling_modules.abstract_gfm import GFM_STATE_PROP_NAME, AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.conservation_gfm import ConservationGapFillingWorker
from gap_filling_modules.origin_gfm import OriginGapFillingWorker
from gap_filling_modules.transportation_util.ecotransit_requests import (
    EcotransitRoutingClient,
    RoutingClientLocation,
    RoutingRequest,
)
from gap_filling_modules.transportation_util.enum import (
    USER_TRANSPORT_MODE_TO_SERVICE_MODE_MAPPING,
    CoolingTypeEnum,
    ServiceTransportModeEnum,
    TransportModeDistanceEnum,
)
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingWorker, UnitWeightConverter
from pyproj import Geod
from structlog import get_logger
from zeep.exceptions import TransportError

from core.domain.deep_mapping_view import DeepMappingView, undo_mapping_view
from core.domain.nodes import FlowNode, FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.legacy_api_information_prop import LegacyAPIInformationProp
from core.domain.props.location_prop import LocationProp, LocationQualifierEnum, get_gadm_term_level
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp
from core.domain.props.transport_modes_distances_prop import TransportDataset, TransportModesDistancesProp
from core.domain.props.utils import flow_location_is_known
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.gfm_cache_service import GfmCacheService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class TransportModeDistanceGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    @staticmethod
    def cheapest_mode_defined(node: Node) -> bool:
        if (
            node.get_parent_nodes()
            and isinstance(node.get_parent_nodes()[0], FoodProcessingActivityNode)
            and getattr(node.get_parent_nodes()[0].transport, "cheapest_mode", None)
        ):
            return True
        return False

    def should_be_scheduled(self) -> bool:
        # Do not run on every inventory node for now, because otherwise we run into EcoTransit api limits:
        # decide if we want to run on this brightway node
        if isinstance(
            self.node, FlowNode
        ) and self.gfm_factory.service_provider.modeled_activity_node_service.should_bw_node_be_computed(
            self.node, self.__class__.__name__
        ):
            return True

        if not isinstance(self.node, FoodProductFlowNode):
            # FIXME: Surely we want to transport something that is not FoodProductFlow in the future.
            logger.debug(f"[TransportModeDistance] {self.node} is not an ingredient --> cancel.")
            return False

        if len(self.node.get_parent_nodes()) == 0:
            logger.debug(f"[TransportModeDistance] {self.node} has no parent nodes --> cancel.")

            return False

        # TODO: should we not do this line because we still need to figure out distance?
        if self.cheapest_mode_defined(self.node):
            logger.debug(
                f"[TransportModeDistance] {self.node} already has a transportation mode specified --> "
                "can't run TransportModeDistance GFM."
            )

            return False

        return True

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        if self.cheapest_mode_defined(self.node):
            logger.debug(f"{self.node} already has a transportation mode specified.")

            return GapFillingWorkerStatusEnum.cancel

        if getattr(self.node, GFM_STATE_PROP_NAME) and (
            unit_weight_conversion_gfm_status := getattr(self.node, GFM_STATE_PROP_NAME).worker_states.get(
                UnitWeightConversionGapFillingWorker.__name__
            )
        ):
            if unit_weight_conversion_gfm_status == NodeGfmStateEnum.scheduled:
                logger.debug("[TransportDecision] wait for UnitWeightConversionGFM to finish" " --> not can_run_now.")
                return GapFillingWorkerStatusEnum.reschedule

        # Requires child activity production amount, which is added by the
        # UnitWeightConversionGFM in the case of food product nodes.
        sub_node = self.node.get_sub_nodes()
        if sub_node and isinstance(sub_node[0], FoodProcessingActivityNode):
            if getattr(sub_node[0], GFM_STATE_PROP_NAME) and (
                unit_weight_conversion_gfm_status := getattr(sub_node[0], GFM_STATE_PROP_NAME).worker_states.get(
                    UnitWeightConversionGapFillingWorker.__name__
                )
            ):
                if unit_weight_conversion_gfm_status == NodeGfmStateEnum.scheduled:
                    logger.debug(
                        "[TransportDecision] wait for UnitWeightConversionGFM to finish" " --> not can_run_now."
                    )
                    return GapFillingWorkerStatusEnum.reschedule
            else:
                logger.debug("[TransportDecision] wait for UnitWeightConversionGFM to finish" " --> not can_run_now.")
                return GapFillingWorkerStatusEnum.reschedule

        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(OriginGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[TransportModeDistance] wait for OriginGapFillingWorker to finish --> not can_run_now.")

            return GapFillingWorkerStatusEnum.reschedule

        if self.gfm_factory.service_provider.modeled_activity_node_service.should_bw_node_be_computed(
            self.node, self.__class__.__name__
        ):
            # wait for the locationGFM to write the location prop
            if not self.node.flow_location:
                return GapFillingWorkerStatusEnum.reschedule
            else:
                return GapFillingWorkerStatusEnum.ready

        if isinstance(self.node, FoodProductFlowNode) and self.node.is_subdivision:
            logger.debug(f"[TransportModeDistance] {self.node} is a subdivision --> cancel.")

            return GapFillingWorkerStatusEnum.cancel

        origin_location = self.node.get_origin_flow_location()
        if not origin_location:
            logger.debug(f"[TransportModeDistance] {self.node} has no origin location --> cancel.")

            return GapFillingWorkerStatusEnum.cancel

        if getattr(self.node, GFM_STATE_PROP_NAME) and (
            conservation_gfm_status := getattr(self.node, GFM_STATE_PROP_NAME).worker_states.get(
                ConservationGapFillingWorker.__name__
            )
        ):
            if conservation_gfm_status == NodeGfmStateEnum.scheduled:
                logger.debug("[TransportDecision] wait for Conservation to finish" " --> not can_run_now.")
                return GapFillingWorkerStatusEnum.reschedule

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        origin_prop, origin_gadm_term = await self.get_origin_prop_and_gadm_term(calc_graph)

        try:
            # this is starting to search for a location in the parent node and further up the tree:
            location_prop, location_gadm_term = await self.get_location_prop_and_gadm_term(calc_graph)
        except (AttributeError, IndexError, AssertionError, KeyError, TypeError):
            return

        if not self.node.get_sub_nodes():
            logger.warning(f"TransportationModeDistanceGFM can only run with a sub activity; {self.node} has none.")
            return

        if location_prop and not origin_prop:
            # handling "unknown production" FAO code
            if (
                self.node.origin_data
                and self.node.origin_data.fao_code
                and (fao_id := self.node.origin_data.fao_code.get_term().xid)
                and (
                    fao_id == self.gfm_factory.failed_production_fao_code_term.xid
                    or fao_id == self.gfm_factory.unknown_fish_production_fao_code_term.xid
                )
            ):
                logger.debug(
                    f"There is no origin-prop for FAO code {fao_id}. Setting default distances...",
                    node=str(self.node),
                )
                default_modes = self.set_default_transport_distances()
                await self.add_nodes_and_set_transport_modes_distances_property(
                    calc_graph, default_modes, location_prop, origin_prop
                )

                return
            # TODO: handling "unknown fish production" FAO code
            #  and adding appropriate distance values after Science team figures out what they should be
            # elif fao_id == self.gfm_factory.unknown_fish_production_fao_code_term.xid:
            #     return
            # handling "local production" FAO code
            else:
                return

        if not location_prop:
            return

        try:
            # getting transport methods from EcoTransit, if nothing's specified
            # Note that self.node.transport is a string since the GFM is cancelled otherwise.
            assert isinstance(self.node, FlowNode)
            if self.node.raw_transport not in (
                TransportModeDistanceEnum.air,
                TransportModeDistanceEnum.ground,
            ):
                (
                    ecotransit_responses,
                    requested_origin_gadm_term,
                    requested_location_gadm_term,
                ) = await self.get_transport_mode_responses(origin_gadm_term, location_gadm_term)
            else:
                # query just this specific mode
                # FIXME Not testsed:
                pre_defined_mode = USER_TRANSPORT_MODE_TO_SERVICE_MODE_MAPPING.get(self.node.raw_transport)

                if not pre_defined_mode:
                    log_msg = (
                        f"Invalid value '{self.node.raw_transport}' for transport in raw input of node {self.node}!"
                    )
                    logger.error(log_msg)
                    calc_graph.append_data_errors_log_entry(log_msg)
                    return

                (
                    ecotransit_responses,
                    requested_origin_gadm_term,
                    requested_location_gadm_term,
                ) = await self.get_transport_mode_responses(
                    origin_gadm_term,
                    location_gadm_term,
                    pre_defined_mode=pre_defined_mode,
                )

        except TransportError as e:
            logger.error(f"Cannot connect to EcoTransit: {e}")
            calc_graph.append_data_errors_log_entry(
                "Cannot connect to EcoTransit!",
            )
            return

        if not ecotransit_responses:
            return

        # calculating distance
        valid_modes = await self.get_transport_distances(
            ecotransit_responses,
            origin_gadm_term,
            requested_origin_gadm_term,
            origin_prop,
            location_gadm_term,
            requested_location_gadm_term,
            location_prop,
        )

        # add property to node
        await self.add_nodes_and_set_transport_modes_distances_property(
            calc_graph, valid_modes, location_prop, origin_prop
        )

    async def get_transport_mode_responses(
        self,
        origin_gadm_term: Term,
        location_gadm_term: Term,
        pre_defined_mode: ServiceTransportModeEnum = None,
    ) -> tuple[dict, Term, Term] | None:
        # get departure point from origin
        start_location_lon, start_location_lat, start_location_gadm_term = await self.get_transport_endpoint_location(
            origin_gadm_term,
        )

        if not start_location_lon or not start_location_lat:
            raise ValueError(f"No coordinates found for origin term {origin_gadm_term}!")

        # get destination point from location
        end_location_lon, end_location_lat, end_location_gadm_term = await self.get_transport_endpoint_location(
            location_gadm_term,
        )

        if not end_location_lon or not end_location_lat:
            raise ValueError(f"No coordinates found for location term {location_gadm_term}!")

        # handling the case where both points are equal
        if start_location_gadm_term == end_location_gadm_term:
            start_location_lon, start_location_lat = self.add_offset_to_endpoint(
                start_location_lon,
                start_location_lat,
                -50_000,
            )
            end_location_lon, end_location_lat = self.add_offset_to_endpoint(end_location_lon, end_location_lat, 50_000)
            # TODO: there is a very slim chance that this might bite in the future
            #  in cases where 2 regions are a very big country or when transport mode is already set
            # setting transport mode to road
            pre_defined_mode = ServiceTransportModeEnum.GROUND

        start_location = RoutingClientLocation(longitude=start_location_lon, latitude=start_location_lat)
        end_location = RoutingClientLocation(longitude=end_location_lon, latitude=end_location_lat)

        # preparing EcoTransit requests
        client = EcotransitRoutingClient()
        responses = {}

        if pre_defined_mode:
            modes_to_request = (pre_defined_mode,)
        else:
            modes_to_request = (
                ServiceTransportModeEnum.GROUND,
                ServiceTransportModeEnum.SEA,
                ServiceTransportModeEnum.AIR,
            )

        for mode in modes_to_request:
            request_key = f"{start_location_gadm_term.xid}_{end_location_gadm_term.xid}_{mode.value}"

            if (response := await self._search_cached_requests(request_key)) is None:
                # TODO: let's keep cooling types here just in case,
                #  they aren't used now but may be in the future
                # call EcoTransit
                request = RoutingRequest(
                    start_location,
                    end_location,
                    mode,
                    CoolingTypeEnum.NONE,
                )

                response = client.run_calculation(request)
                if len(response["error"]) > 0:
                    if response["error"][0]["message"] == "Incorrect login.":
                        raise ValueError(f"Error in EcoTransit response: {response['error'][0]['message']}")

                responses[mode.value] = response

                await self._update_cached_requests(request_key, responses[mode.value])
            else:
                responses[mode.value] = response

        return responses, start_location_gadm_term, end_location_gadm_term

    async def get_transport_endpoint_location(self, endpoint_term: Term) -> tuple[float, float, Term]:
        if get_gadm_term_level(endpoint_term) < 3:
            end_location_lon = endpoint_term.data.get("centroid_lon")
            end_location_lat = endpoint_term.data.get("centroid_lat")
        else:
            location_gadm_term_xid_split = endpoint_term.xid.split(".")
            end_location_gadm_id = f"{location_gadm_term_xid_split[0]}.{location_gadm_term_xid_split[1]}_1"

            regional_location_gadm_term = (
                await self.gfm_factory.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                    term_xid=end_location_gadm_id,
                    access_group_uid=str(self.gfm_factory.root_gadm_term.access_group_uid),
                )
            )

            end_location_lon = regional_location_gadm_term.data.get("centroid_lon")
            end_location_lat = regional_location_gadm_term.data.get("centroid_lat")
            endpoint_term = regional_location_gadm_term

        return end_location_lon, end_location_lat, endpoint_term

    @staticmethod
    def add_offset_to_endpoint(longitude: float, latitude: float, offset: int) -> tuple[float, float]:
        geod = Geod(ellps="WGS84")
        offset_longitude, offset_latitude, _ = geod.fwd(longitude, latitude, 0, offset)

        return offset_longitude, offset_latitude

    async def get_transport_distances(
        self,
        ecotransit_responses: dict,
        origin_gadm_term: Term,
        requested_origin_gadm_term: Term,
        origin_prop: LocationProp,
        location_gadm_term: Term,
        requested_location_gadm_term: Term,
        location_prop: LocationProp,
    ) -> dict:
        valid_modes = {}

        for mode, response in ecotransit_responses.items():
            if response.get("error"):
                continue

            detailed_co2eq = {}
            detailed_distances = {}
            distance_total = 0.0

            # calculating main carriage total distance

            detailed_co2eq["main_carriage"] = 0.0
            detailed_distances["main_carriage"] = 0.0
            mainhaul_sections = response["result"]["section"][0]["mainhaul"]
            invalid_mode = False

            # if both locations are the same, we just calculate approximate mainhaul distance
            if requested_origin_gadm_term == requested_location_gadm_term:
                # calculating mainhaul total distance

                # handling the case where one term is GADM level 2, while the other is GADM level 3+
                # we need to pick the term with the highest level to pass the necessary check in the next method call
                term_to_use = location_gadm_term

                if max(get_gadm_term_level(origin_gadm_term), get_gadm_term_level(location_gadm_term)) >= 3:
                    if get_gadm_term_level(origin_gadm_term) > 2:
                        term_to_use = origin_gadm_term

                detailed_distances["main_carriage"] = await self.calculate_pre_post_carriage_estimated_distance(
                    origin_prop.longitude,
                    origin_prop.latitude,
                    location_prop.longitude,
                    location_prop.latitude,
                    term_to_use,
                    sqrt_area_to_distance_scaling_factor=0.56905,
                )
                detailed_distances["pre_carriage"] = 0.0
                detailed_distances["post_carriage"] = 0.0
                detailed_distances["total"] = detailed_distances["main_carriage"]

                # calculating mainhaul co2eq value

                detailed_co2eq["main_carriage"] = await self.process_pre_post_carriage_co2eq_value(
                    mainhaul_sections,
                    detailed_distances,
                    0,
                    mode,
                    "main_carriage",
                )
                detailed_co2eq["pre_carriage"] = 0.0
                detailed_co2eq["post_carriage"] = 0.0

                valid_modes[mode] = {
                    "distances": detailed_distances,
                    "co2": detailed_co2eq,
                }

                continue

            # sections can be multiple for routes that have transfer points
            for section in mainhaul_sections:
                try:
                    # get distance via "tankToWheel" field
                    section_distance = section["emissions"]["distances"][mode]["tankToWheel"]
                    detailed_distances["main_carriage"] += section_distance
                    distance_total += section_distance
                    detailed_co2eq["main_carriage"] += section["emissions"]["co2Equivalents"][mode]["_value_1"]
                except TypeError:
                    # TODO: rethink this approach?
                    # this mode is not present in output --> skip it
                    invalid_mode = True

                    break

            if invalid_mode:
                continue

            # skipping pre- & post-carriage estimation
            # if main carriage transport mode is "road"
            if mode == ServiceTransportModeEnum.GROUND:
                detailed_distances["pre_carriage"] = 0.0
                detailed_distances["post_carriage"] = 0.0
                detailed_distances["total"] = detailed_distances["main_carriage"]

                detailed_co2eq["pre_carriage"] = 0.0
                detailed_co2eq["post_carriage"] = 0.0
            else:
                # calculating pre-haul total distance
                mainhaul_start_location = response["result"]["section"][0]["mainhaul"][0]["route"]["startLocation"][
                    "wgs84"
                ]

                match mode:
                    case ServiceTransportModeEnum.SEA:
                        sqrt_area_to_distance_scaling_factor = 0.29041823129
                    case ServiceTransportModeEnum.AIR:
                        sqrt_area_to_distance_scaling_factor = 0.11781574324
                    case _:
                        raise ValueError(f"{mode} is not a recognized mode of transport.")

                detailed_distances["pre_carriage"] = await self.calculate_pre_post_carriage_estimated_distance(
                    origin_prop.longitude,
                    origin_prop.latitude,
                    mainhaul_start_location["longitude"],
                    mainhaul_start_location["latitude"],
                    origin_gadm_term,
                    sqrt_area_to_distance_scaling_factor,
                )
                distance_total += detailed_distances["pre_carriage"]

                # calculating pre-haul co2eq value

                detailed_co2eq["pre_carriage"] = await self.process_pre_post_carriage_co2eq_value(
                    response["result"]["section"][0]["preCarriage"],
                    detailed_distances,
                    detailed_co2eq["main_carriage"],
                    mode,
                    "pre_carriage",
                )

                # calculating post-haul total distance

                mainhaul_end_location = response["result"]["section"][0]["mainhaul"][-1]["route"]["endLocation"][
                    "wgs84"
                ]

                detailed_distances["post_carriage"] = await self.calculate_pre_post_carriage_estimated_distance(
                    location_prop.longitude,
                    location_prop.latitude,
                    mainhaul_end_location["longitude"],
                    mainhaul_end_location["latitude"],
                    location_gadm_term,
                    sqrt_area_to_distance_scaling_factor,
                )
                distance_total += detailed_distances["post_carriage"]

                # calculating post-haul co2eq value

                detailed_co2eq["post_carriage"] = await self.process_pre_post_carriage_co2eq_value(
                    response["result"]["section"][0]["postCarriage"],
                    detailed_distances,
                    detailed_co2eq["main_carriage"],
                    mode,
                    "post_carriage",
                )

                detailed_distances["total"] = distance_total

            valid_modes[mode] = {
                "distances": detailed_distances,
                "co2": detailed_co2eq,
            }

        return valid_modes

    async def calculate_pre_post_carriage_estimated_distance(
        self,
        local_prop_lon: float,
        local_prop_lat: float,
        ecotransit_response_lon: float,
        ecotransit_response_lat: float,
        gadm_term: Term,
        sqrt_area_to_distance_scaling_factor: float,
    ) -> float:
        """Estimate pre- and post-carriage distances. For details, see the dedicated notion page."""

        def convert_m_to_km(length_in_m: float) -> float:
            return UnitWeightConverter.convert_between_same_unit_types(
                length_in_m, self.gfm_factory.meter_term, self.gfm_factory.kilometer_term, "length-in-m"
            )

        gadm_term_level = get_gadm_term_level(gadm_term)
        geod = Geod(ellps="WGS84")
        ribbon_factor = 1.417

        if gadm_term_level > 2:
            _, _, distance_in_m = geod.inv(
                local_prop_lon,  # origin prop
                local_prop_lat,
                ecotransit_response_lon,  # location prop
                ecotransit_response_lat,
            )
            estimated_distance_km = convert_m_to_km(distance_in_m)
        else:
            sqrt_area_in_m = math.sqrt(gadm_term.data.get("area_m2"))
            area_estimated_distance_km = convert_m_to_km(sqrt_area_in_m) * sqrt_area_to_distance_scaling_factor

            _, _, distance_from_centroid_in_m = geod.inv(
                gadm_term.data.get("centroid_lon"),  # origin term
                gadm_term.data.get("centroid_lat"),
                ecotransit_response_lon,  # location prop
                ecotransit_response_lat,
            )
            distance_from_centroid_km = convert_m_to_km(distance_from_centroid_in_m)

            estimated_distance_km = max(area_estimated_distance_km, distance_from_centroid_km)
        return ribbon_factor * estimated_distance_km

    async def process_pre_post_carriage_co2eq_value(
        self,
        pre_post_carriage_sections: list,
        detailed_distances: dict[str, float],
        mainhaul_co2eq_value: float,
        mode: str,
        pre_post_carriage_key: str,
    ) -> float:
        co2eq_value = 0.0

        if pre_post_carriage_sections:
            post_carriage_ecotransit_distance = 0.0

            for section in pre_post_carriage_sections:
                co2eq_value += section["emissions"]["co2Equivalents"][ServiceTransportModeEnum.GROUND]["_value_1"]
                post_carriage_ecotransit_distance += section["emissions"]["distances"][ServiceTransportModeEnum.GROUND][
                    "tankToWheel"
                ]

            return await self.calculate_pre_post_carriage_co2eq_value(
                co2eq_value,
                post_carriage_ecotransit_distance,
                detailed_distances[pre_post_carriage_key],
            )
        else:
            if mode == ServiceTransportModeEnum.GROUND:
                return await self.calculate_pre_post_carriage_co2eq_value(
                    mainhaul_co2eq_value,
                    detailed_distances["main_carriage"],
                    detailed_distances[pre_post_carriage_key],
                )
            else:
                raise ValueError(
                    f"No {pre_post_carriage_key} response found for mode {mode}, "
                    f"therefore it's not possible to calculate the approximate CO2 value."
                )

    @staticmethod
    async def calculate_pre_post_carriage_co2eq_value(
        ecotransit_co2eq_value: float,
        ecotransit_distance: float,
        estimated_distance: float,
    ) -> float:
        return ecotransit_co2eq_value * (estimated_distance / ecotransit_distance)

    async def get_origin_prop_and_gadm_term(
        self, calc_graph: CalcGraph
    ) -> tuple[LocationProp, Term] | tuple[None, None]:
        try:
            assert isinstance(self.node, FlowNode)
            origin_prop_list = self.node.get_origin_flow_location()
            assert origin_prop_list
            origin_prop = origin_prop_list[0]

            if origin_prop.location_qualifier == LocationQualifierEnum.unknown:
                logger.debug(f"Unknown location found in origin location prop of node {self.node}!")
                calc_graph.append_data_errors_log_entry(
                    f"Unknown location found in origin location prop of node {self.node}!",
                )
                return None, None

            origin_gadm_term = origin_prop.get_term()
            assert origin_gadm_term

            return origin_prop, origin_gadm_term
        except (AttributeError, IndexError, AssertionError, KeyError, TypeError):
            logger.warning("No GADM term found in origin location prop!")
            calc_graph.append_data_errors_log_entry(
                f"No GADM term found in origin location prop of node {self.node}!",
            )

            return None, None

    async def get_location_prop_and_gadm_term(
        self, calc_graph: CalcGraph
    ) -> tuple[LocationProp, Term] | tuple[None, None]:
        if self.node.get_parent_nodes() and (parent_flows := self.node.get_parent_nodes()[0].get_parent_nodes()):
            parent_flow = parent_flows[0]
            assert isinstance(parent_flow, FlowNode)
            location_props, _ = parent_flow.get_prop_by_inheritance(
                "flow_location", ignore_if=(lambda prop: not flow_location_is_known(prop))
            )
            location_prop = location_props[0]
            assert isinstance(location_prop, LocationProp)
            return location_prop, location_prop.get_term()
        logger.warning("No valid destination location found in node tree!")
        calc_graph.append_data_errors_log_entry(
            f"No valid destination location found in node tree of node {self.node}!",
        )
        return None, None

    @staticmethod
    def set_default_transport_distances() -> dict:
        default_distances = {
            "sea": {
                "distances": {
                    "main_carriage": 11_428.6755,
                    "pre_carriage": 666.024184,
                    "post_carriage": 2569.737467,
                    "total": 14_664.437150999998,
                },
                # CO2Eq values taken from Argentina -> Portugal -> Germany transportation via sea
                "co2": {
                    "main_carriage": 0.12019801,
                    "pre_carriage": 0.0952182809,
                    "post_carriage": 0.19076337,
                },
            }
        }

        return default_distances

    @staticmethod
    def get_transport_data_with_units(transport_data: dict, unit_uid: UUID) -> dict[str, ReferencelessQuantityProp]:
        transport_data_with_units: dict = {}
        for key, val in transport_data.items():
            transport_data_with_units[key] = ReferencelessQuantityProp.unvalidated_construct(
                value=val, unit_term_uid=unit_uid
            )
        return transport_data_with_units

    def convert_valid_modes_into_transport_modes_distances_prop(
        self, valid_modes: dict
    ) -> tuple[dict[ServiceTransportModeEnum, TransportDataset], dict[ServiceTransportModeEnum, TransportDataset]]:
        distances_data: dict[ServiceTransportModeEnum, TransportDataset] = {}
        co2_data: dict[ServiceTransportModeEnum, TransportDataset] = {}
        for mode, data in valid_modes.items():
            distances_data[mode] = TransportDataset.unvalidated_construct(
                **self.get_transport_data_with_units(data["distances"], self.gfm_factory.kilometer_term.uid)
            )
            co2_data[mode] = TransportDataset.unvalidated_construct(
                **self.get_transport_data_with_units(data["co2"], self.gfm_factory.ton_term.uid)
            )
        return distances_data, co2_data

    async def add_nodes_and_set_transport_modes_distances_property(
        self, calc_graph: CalcGraph, valid_modes: dict, location_prop: LocationProp, origin_prop: LocationProp
    ) -> None:
        distances, co2 = self.convert_valid_modes_into_transport_modes_distances_prop(valid_modes)
        kwargs_dict = {"distances": distances, "co2": co2}
        if distances or co2:
            # No need to create new nodes if neither distances nor co2 exists.

            # 1) delete link between node and parent.

            parent_node: Node = self.node.get_parent_nodes()[0]
            remove_edge_mutation = RemoveEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=parent_node.uid,
                to_node_uid=self.node.uid,
            )
            calc_graph.apply_mutation(remove_edge_mutation)

            # 2) copy original flow to transported flow.

            transported_flow_node_uid = uuid4()

            calc_graph.apply_mutation(
                DuplicateNodeMutation(
                    created_by_module=self.__class__.__name__,
                    new_node_uid=transported_flow_node_uid,
                    source_node_uid=self.node.uid,
                    parent_node_uid=parent_node.uid,
                    gfms_to_not_schedule=("OriginGapFillingWorker", self.__class__.__name__),
                    duplicate_child_nodes=False,
                )
            )

            prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="amount",
                prop=QuantityProp.unvalidated_construct(
                    value=1.0,
                    unit_term_uid=self.node.get_sub_nodes()[0].production_amount.unit_term_uid,
                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                ),
            )
            calc_graph.apply_mutation(prop_mutation)

            # Overwrite the flow location to the destination location
            prop_mutation = PropListMutation(
                created_by_module=self.__class__.__name__,
                node_uid=transported_flow_node_uid,
                prop_name="flow_location",
                props=[location_prop.duplicate()],
                append=False,
            )
            calc_graph.apply_mutation(prop_mutation)

            # Add origin to legacyAPIInformation:
            if origin_prop and origin_prop.source_data_raw and isinstance(origin_prop.source_data_raw, str):
                if self.node.legacy_API_information:
                    legacy_API_information = self.node.legacy_API_information.duplicate()
                    if legacy_API_information.properties_pre_gfms:
                        if isinstance(legacy_API_information.properties_pre_gfms, DeepMappingView):
                            temp_properties_pre_gfms = undo_mapping_view(legacy_API_information.properties_pre_gfms)
                        else:
                            temp_properties_pre_gfms = legacy_API_information.properties_pre_gfms
                        temp_properties_pre_gfms["origin"] = origin_prop.source_data_raw
                        legacy_API_information.properties_pre_gfms = temp_properties_pre_gfms
                    else:
                        legacy_API_information.properties_pre_gfms = {"origin": origin_prop.source_data_raw}
                else:
                    legacy_API_information = LegacyAPIInformationProp.unvalidated_construct(
                        origin=origin_prop.source_data_raw
                    )
                prop_mutation = PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=transported_flow_node_uid,
                    prop_name="legacy_API_information",
                    prop=legacy_API_information,
                )
                calc_graph.apply_mutation(prop_mutation)

            # 3) create transport activity

            transport_activity_node_uid = uuid4()

            legacy_api_information = None
            if self.node.get_sub_nodes()[0].legacy_API_information:
                # we need to preserve the legacy API information so that ingredient type is preserved:
                legacy_api_information = self.node.get_sub_nodes()[0].legacy_API_information.duplicate()

            # TODO: Should be modified to generic ActivityNode in the future.
            # Currently, Making this a generic ActivityNode breaks tests.
            transport_activity_node = FoodProcessingActivityNode.model_construct(
                uid=transport_activity_node_uid,
                gfm_state=GfmStateProp.unvalidated_construct(
                    worker_states={
                        # No more origin split necessary:
                        "OriginGapFillingWorker": NodeGfmStateEnum.canceled,
                        # Ingredient amount estimation should be already finished:
                        "IngredientAmountEstimatorGapFillingWorker": NodeGfmStateEnum.canceled,
                        # No Location gap filling necessary on this dummy node:
                        "LocationGapFillingWorker": NodeGfmStateEnum.canceled,
                    }
                ),
                production_amount=QuantityProp.unvalidated_construct(
                    value=1.0,
                    unit_term_uid=self.node.get_sub_nodes()[0].production_amount.unit_term_uid,
                    for_reference=ReferenceAmountEnum.self_reference,
                ),
                legacy_API_information=legacy_api_information,
            )

            calc_graph.apply_mutation(
                AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    new_node=transport_activity_node,
                    parent_node_uid=transported_flow_node_uid,
                    copy=False,
                )
            )

            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=transport_activity_node_uid,
                to_node_uid=self.node.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

            prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=transport_activity_node_uid,
                prop_name="transport",
                prop=TransportModesDistancesProp.unvalidated_construct(**kwargs_dict),
            )

            calc_graph.apply_mutation(prop_mutation)

    async def _search_cached_requests(self, request_key: str) -> dict[str, list] | None:
        return await self.gfm_factory.gfm_cache_service.get_cache_entry_by_gfm_name_and_key(
            self.__class__.__name__,
            request_key,
            deep_copy=False,
        )

    async def _update_cached_requests(self, request_key: str, response_data: dict[str, list]) -> None:
        await self.gfm_factory.gfm_cache_service.set_cache_entry_by_gfm_name_and_key(
            self.__class__.__name__,
            request_key,
            response_data,
        )


class TransportModeDistanceGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)

        self.gfm_cache_service: GfmCacheService = service_provider.gfm_cache_service

        self.root_gadm_term = self.service_provider.glossary_service.root_subterms.get("Root_GADM")

        self.local_production_fao_code_term: Term = None  # noqa
        self.default_production_fao_code_term: Term = None  # noqa
        self.unknown_fish_production_fao_code_term: Term = None  # noqa
        self.failed_production_fao_code_term: Term = None  # noqa
        self.ton_term: Optional[Term] = None
        self.kilometer_term: Optional[Term] = None
        self.meter_term: Optional[Term] = None

    async def init_cache(self) -> None:
        await self.gfm_cache_service.init_individual_gfm_cache("TransportModeDistanceGapFillingWorker")

        root_fao_term = self.service_provider.glossary_service.root_subterms.get("Root_FAO")
        assert root_fao_term, "No root FAO term found."

        # loading custom FAO codes
        self.local_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="100000",
                access_group_uid=str(root_fao_term.access_group_uid),
            )
        )
        assert self.local_production_fao_code_term, "No 'local production' term found."

        self.default_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="200000",
                access_group_uid=str(root_fao_term.access_group_uid),
            )
        )
        assert self.default_production_fao_code_term, "No 'default production' term found."

        self.unknown_fish_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="300000",
                access_group_uid=str(root_fao_term.access_group_uid),
            )
        )
        assert self.unknown_fish_production_fao_code_term, "No 'unknown fish production' term found."

        self.failed_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="400000",
                access_group_uid=str(root_fao_term.access_group_uid),
            )
        )
        assert self.failed_production_fao_code_term, "No 'failed production' term found."

        root_unit_term = self.service_provider.glossary_service.root_subterms["EOS_units"]
        self.ton_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_ton", root_unit_term.access_group_uid)
        ]
        self.kilometer_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilometer", root_unit_term.access_group_uid)
        ]
        self.meter_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_meter", root_unit_term.access_group_uid)
        ]

    def spawn_worker(self, node: Node) -> TransportModeDistanceGapFillingWorker:
        return TransportModeDistanceGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = TransportModeDistanceGapFillingFactory
