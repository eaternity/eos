"""Daily food unit gap filling module."""

from uuid import UUID

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.aggregation_gfm import AggregationGapFillingFactory, AggregationGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from structlog import get_logger

from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props import QuantityPackageProp, QuantityProp, ReferencelessQuantityProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class DailyFoodUnitGapFillingWorker(AbstractGapFillingWorker):
    """Daily food unit gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "DailyFoodUnitGapFillingFactory"):
        """Compute daily food unit from aggregated nutrient values."""
        super().__init__(node)
        self.gfm_factory = gfm_factory
        self.nutrient_content_prop_for_reference = None

    def should_be_scheduled(self) -> bool:
        """Only run on nodes that are not origin or nutrient split nodes."""
        if not isinstance(self.node, FoodProductFlowNode):
            logger.debug("[DailyFoodUnit] node is not a food product --> not scheduled.")
            return False
        else:
            return True

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """We want to perform daily food unit calculation after all aggregation is complete."""
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(AggregationGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for AggregationGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        required_nutrients_present = {nutr_key: False for nutr_key in self.gfm_factory.required_nutrients}

        try:
            if aggregated_nutrients := self.node.aggregated_nutrients:
                self.nutrient_content_prop_for_reference = aggregated_nutrients.amount_for_activity_production_amount()
                if self.nutrient_content_prop_for_reference:
                    for key in self.nutrient_content_prop_for_reference.quantities:
                        if key in required_nutrients_present:
                            required_nutrients_present[key] = True
            elif isinstance(self.node, FoodProductFlowNode) and self.node.nutrient_values:
                self.nutrient_content_prop_for_reference = (
                    self.node.nutrient_values.amount_for_activity_production_amount()
                )
                if self.nutrient_content_prop_for_reference:
                    for key in self.nutrient_content_prop_for_reference.quantities:
                        if key in required_nutrients_present:
                            required_nutrients_present[key] = True
        except ValueError:
            logger.debug(
                "[DailyFoodUnit] cancel GFM worker because required nutrients "
                "for local flow/activity cannot be obtained."
            )
            return GapFillingWorkerStatusEnum.cancel

        if all(value for value in required_nutrients_present.values()):
            logger.debug("[DailyFoodUnit] All required nutrients aggregated --> can_run_now.")
            return GapFillingWorkerStatusEnum.ready
        else:
            logger.debug("[DailyFoodUnit] cancel GFM worker because required nutrients are not present.")
            return GapFillingWorkerStatusEnum.cancel

    async def run(self, calc_graph: CalcGraph) -> None:
        """Perform daily food unit calculation from nutrients."""
        assert isinstance(self.node, FoodProductFlowNode)

        if not self.node.amount:
            logger.debug("[DailyFoodUnit] Missing flow amount.")
            return
        amount_in_gram = self.get_flow_amount_in_grams()

        # Add daily food unit values to the node:
        dfu_for_local = self.compute_daily_food_unit(self.nutrient_content_prop_for_reference, amount_in_gram)
        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="daily_food_unit",
                prop=dfu_for_local,
            )
        )

    def unit_convert(self, qty: ReferencelessQuantityProp) -> float:
        """Convert units to g and kj."""
        return UnitWeightConverter.unit_convert_to_g_kj(
            qty,
            self.gfm_factory.service_provider.glossary_service.terms_by_uid,
            self.gfm_factory.gram_term,
            self.gfm_factory.kj_term,
        )

    def compute_daily_food_unit(
        self, nutrient_content_props_for_reference: QuantityPackageProp, amount_gram: float
    ) -> QuantityProp:
        """Compute daily food unit.

        :param nutrient_content_props_for_reference: "nutrient_values" or "aggregated_nutrients" already converted
        :param amount_gram: amount of food product or ingredient in grams.
        :return: daily food unit.
        """
        extracted_nutrients: dict[UUID, ReferencelessQuantityProp] = nutrient_content_props_for_reference.quantities

        energy_kj = self.unit_convert(extracted_nutrients[self.gfm_factory.nutr_name_to_term["energy"]])
        water_gram = self.unit_convert(extracted_nutrients[self.gfm_factory.nutr_name_to_term["water"]])
        fat_gram = self.unit_convert(extracted_nutrients[self.gfm_factory.nutr_name_to_term["fat"]])
        protein_gram = self.unit_convert(extracted_nutrients[self.gfm_factory.nutr_name_to_term["protein"]])

        dry_weight = amount_gram - water_gram
        remainder_energy = energy_kj - 17.0 * protein_gram - 37.0 * fat_gram

        dfu_quantity = QuantityProp(
            value=(protein_gram / 50 + fat_gram / 66 + remainder_energy / 6000 + water_gram / 2500 + dry_weight / 600)
            / 5,
            unit_term_uid=self.gfm_factory.dfu_unit_term.uid,
            for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
        )
        return dfu_quantity

    def get_flow_amount_in_grams(self) -> float:
        """Obtain flow amount in grams."""
        return UnitWeightConverter.get_flow_amount_in_grams(self.node, self.gfm_factory.gram_term)

    def get_production_amount_in_grams(self) -> float:
        """Obtain production amount in grams."""
        return UnitWeightConverter.get_production_amount_in_grams(self.node, self.gfm_factory.gram_term)


class DailyFoodUnitGapFillingFactory(AbstractGapFillingFactory):
    """Daily food unit gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.gram_term = Term(data={}, name="", sub_class_of=None)
        self.kj_term = Term(data={}, name="", sub_class_of=None)
        self.dfu_unit_term = Term(data={}, name="", sub_class_of=None)
        self.nutr_name_to_term: dict[str, UUID] = {}
        self.required_nutrients: [UUID] = []

    async def init_cache(self) -> None:
        """Initialize required cache."""
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", root_unit_term.access_group_uid)
        ]
        self.kj_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilojoule", root_unit_term.access_group_uid)
        ]
        self.dfu_unit_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_daily_food_unit", root_unit_term.access_group_uid)
        ]
        root_nutr_term = self.service_provider.glossary_service.root_subterms.get("EOS_nutrient_names")
        self.nutr_name_to_term = {
            req_nutr.lower(): self.service_provider.glossary_service.terms_by_xid_ag_uid[
                (f"EOS_{req_nutr}", root_nutr_term.access_group_uid)
            ].uid
            for req_nutr in AggregationGapFillingFactory.required_nutrients
        }
        self.required_nutrients = list(self.nutr_name_to_term.values())

    def spawn_worker(self, node: Node) -> DailyFoodUnitGapFillingWorker:
        """Spawn daily food unit gap filling worker."""
        return DailyFoodUnitGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = DailyFoodUnitGapFillingFactory
