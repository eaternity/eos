"Abstract gap filling module."
from typing import TYPE_CHECKING

from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from structlog import get_logger

from core.graph_manager.calc_graph import CalcGraph
from database.postgres.postgres_db import PostgresDb

if TYPE_CHECKING:
    from core.domain.nodes.node import Node
    from core.service.service_provider import ServiceProvider


logger = get_logger()


GFM_STATE_PROP_NAME = "gfm_state"
GLOBAL_GFM_STATE_PROP_NAME = "global_gfm_state"


class AbstractGapFillingWorker:
    """A GapFillingWorker should always have a reference to a single node in the graph.

    On which the main calculation is performed. It is spawn on each new node that is added to a calculation graph.
    """

    def __init__(self, node: "Node"):
        """Constructor.

        Args:
            node: The graph node where the main calculation or mutation is performed.
        """
        self.node = node

        # counter that is incremented each time this worker is rescheduled:
        self.reschedule_counter = 0  # can be used by individual GapFillingWorkers to make rescheduling decisions.

    def get_global_gfm_state(self) -> dict:
        # get the global GFM scheduling state:
        if calculation := self.node.get_calculation():
            global_gfm_state = calculation.global_gfm_state
            if global_gfm_state:
                global_gfm_state_data = global_gfm_state.worker_states
            else:
                global_gfm_state_data = {}
            return global_gfm_state_data
        else:
            return {}  # In some tests, global_gfm_state is not defined.

    def should_be_scheduled(self) -> bool:
        "This method is used by the orchestrator to check if this worker should be scheduled on the given node."
        pass

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """This method is used by the orchestrator to check if this worker is ready to run now.

        Or still needs to wait for other gaps to be filled before it can run.
        """
        pass

    async def run(self, calc_graph: CalcGraph) -> None:
        pass

    def __repr__(self):
        return self.__class__.__name__


class AbstractGapFillingFactory:
    """The GapFillingFactory is initialized only once, per api service and it's lifetime spans many calculations.

    It can hold persistent connections to external services (like databases, or external calculation microservices).
    It can also hold internally persistent caches that the GapFillingWorker might need.
    The main functionality that it needs to provide is to span a GapFillingWorker on individual graph nodes.
    """

    def __init__(self, postgres_db: PostgresDb, service_provider: "ServiceProvider"):
        self.postgres_db = postgres_db
        self.service_provider = service_provider

        # do custom initializations here for your gap filling module

    async def init_cache(self) -> None:
        """You can do some custom initalizations of internal caches here that should be async."""
        pass

    def spawn_worker(self, node: "Node") -> AbstractGapFillingWorker:
        return AbstractGapFillingWorker(node)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = AbstractGapFillingFactory
