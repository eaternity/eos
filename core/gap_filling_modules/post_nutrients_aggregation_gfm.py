"""Aggregation gap filling module."""

import copy
import uuid
from typing import Dict, List

from gap_filling_modules.abstract_aggregation_gfm import (
    AbstractAggregationGapFillingFactory,
    AbstractAggregationGapFillingWorker,
    AggregatedDataEnum,
)
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.daily_food_unit_gfm import DailyFoodUnitGapFillingWorker
from gap_filling_modules.impact_assessment_gfm import ImpactAssessmentGapFillingWorker
from structlog import get_logger

from core.domain.deep_mapping_view import DeepMappingView
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.names_prop import NamesProp
from core.domain.props.quantity_package_prop import QuantityPackageProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class PostNutrientsAggregationGapFillingWorker(AbstractAggregationGapFillingWorker):
    """Aggregation gap filling worker."""

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """We want to perform aggregation after all the flow amounts for the ingredients have been calculated."""
        # return MatrixCalculationGapFillingWorker.graph_building_gfm_workers_finished(self)
        global_gfm_state = self.get_global_gfm_state()
        if global_gfm_state.get(ImpactAssessmentGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for ImpactAssessmentGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        if global_gfm_state.get(DailyFoodUnitGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for DailyFoodUnitGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        return GapFillingWorkerStatusEnum.ready

    def create_and_update_amount_props_if_necessary(
        self, node: Node, calc_graph: CalcGraph, aggregated_props: list[AggregatedDataEnum]
    ) -> None:
        """Create amount properties and update amount_props if necessary."""
        for parent_flow in node.get_parent_nodes():
            if isinstance(parent_flow, FoodProductFlowNode):
                if (
                    AggregatedDataEnum.amount_per_matching_required_for_flow in aggregated_props
                    and parent_flow.amount_per_matching_required_for_flow
                    and len(parent_flow.amount_per_matching_required_for_flow.quantities) == 1
                    and list(parent_flow.amount_per_matching_required_for_flow.quantities.values())[0].dfu_per_100g
                    is None
                    and parent_flow.daily_food_unit
                ):  # add the dfu to the amount_per_matching_required_for_flow
                    amount_per_matching_prop = parent_flow.amount_per_matching_required_for_flow.duplicate()
                    for qty in amount_per_matching_prop.quantities.values():
                        qty.dfu_per_100g = parent_flow.daily_food_unit.amount_for_100g().value
                        calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=parent_flow.uid,
                                prop_name="amount_per_matching_required_for_flow",
                                prop=amount_per_matching_prop,
                            )
                        )

                # add amount-props if it is the deepest node with a glossary link
                if (
                    isinstance(parent_flow.product_name, NamesProp)
                    and parent_flow.product_name.glossary_link_uid
                    and not any([isinstance(sub_node, FoodProductFlowNode) for sub_node in node.get_sub_nodes()])
                ):
                    if (
                        AggregatedDataEnum.amount_per_glossary_link_required_for_flow in aggregated_props
                        and not parent_flow.amount_per_glossary_link_required_for_flow
                    ):
                        # add this glossary_link_uid to the aggregated_amount_per_glossary_link_required_for_flow
                        amount_per_glossary_link_required_for_flow: dict[uuid.UUID, ReferencelessQuantityProp] = {
                            parent_flow.product_name.glossary_link_uid: ReferencelessQuantityProp.unvalidated_construct(
                                value=100, unit_term_uid=self.gfm_factory.gram_term.uid
                            )
                        }
                        calc_graph.apply_mutation(
                            # When the base product is tagged, the entire amount corresponds to the tag.
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=parent_flow.uid,
                                prop_name="amount_per_glossary_link_required_for_flow",
                                prop=QuantityPackageProp.unvalidated_construct(
                                    quantities=amount_per_glossary_link_required_for_flow,
                                    for_reference=ReferenceAmountEnum.amount_for_100g,
                                ),
                            )
                        )

                    if (
                        AggregatedDataEnum.amount_per_category_required_for_flow in aggregated_props
                        and not parent_flow.amount_per_category_required_for_flow
                    ):
                        inherited_amount_per_category_in_flow, _ = parent_flow.get_prop_by_inheritance(
                            "amount_per_category_in_flow",
                            ignore_if=(
                                lambda prop: (not isinstance(prop, QuantityPackageProp) or len(prop.quantities) == 0)
                            ),
                        )

                        if isinstance(inherited_amount_per_category_in_flow, QuantityPackageProp):
                            amount_per_category_required_for_flow = copy.copy(
                                inherited_amount_per_category_in_flow.amount_for_100g().quantities
                            )
                            amount_per_category_required_for_flow_without_health_categories = {
                                k: v
                                for k, v in amount_per_category_required_for_flow.items()
                                if k not in self.gfm_factory.health_category_term_uids
                            }
                            calc_graph.apply_mutation(
                                PropMutation(
                                    created_by_module=self.__class__.__name__,
                                    node_uid=parent_flow.uid,
                                    prop_name="amount_per_category_required_for_flow",
                                    prop=QuantityPackageProp.unvalidated_construct(
                                        quantities=amount_per_category_required_for_flow_without_health_categories,
                                        for_reference=ReferenceAmountEnum.amount_for_100g,
                                    ),
                                )
                            )

                    if (
                        AggregatedDataEnum.amount_per_matching_required_for_flow in aggregated_props
                        and not parent_flow.amount_per_matching_required_for_flow
                    ):
                        parent_flow.add_prop_for_amount_per_matching_required_for_flow(
                            calc_graph, node.uid, self.gfm_factory.gram_term.uid
                        )

                # add impact_assessment_props to the parent-flows if the corresponding amount-props are present
                def _add_impact_assessment_prop(
                    weight_prop_name: str, impact_prop_name: str, flow_node: FoodProductFlowNode
                ) -> None:
                    impact_assessment_per_quantity_list: List[QuantityPackageProp] = []
                    if not flow_node.impact_assessment:
                        return
                    for (
                        impact_assessment_term_uid,
                        impact_assessment_quantity,
                    ) in flow_node.impact_assessment.amount_for_activity_production_amount().quantities.items():
                        impact_assessment_per_quantity: Dict[uuid.UUID, ReferencelessQuantityProp] = {}
                        quantities = getattr(flow_node, weight_prop_name).quantities
                        assert isinstance(quantities, (dict, DeepMappingView))
                        for (
                            quantity_uid,
                            _,
                        ) in quantities.items():
                            impact_assessment_per_quantity[quantity_uid] = impact_assessment_quantity
                        impact_assessment_per_quantity_list.append(
                            QuantityPackageProp.unvalidated_construct(
                                quantities=impact_assessment_per_quantity,
                                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                                prop_term_uid=impact_assessment_term_uid,
                            )
                        )
                    # TODO: For the moment we only add the first impact-assessment.
                    # In the future we can decide if it is necessary to add all of them,
                    # (and have e.g. impact_assessment_per_glossary_link be a list of QuantityPackageProp)
                    calc_graph.apply_mutation(
                        PropMutation(
                            created_by_module=self.__class__.__name__,
                            node_uid=flow_node.uid,
                            prop_name=impact_prop_name,
                            prop=impact_assessment_per_quantity_list[0],
                        )
                    )

                for weight_prop_name, impact_prop_name in [
                    (
                        AggregatedDataEnum.amount_per_glossary_link_required_for_flow,
                        AggregatedDataEnum.impact_assessment_per_glossary_link,
                    ),
                    (
                        AggregatedDataEnum.amount_per_category_required_for_flow,
                        AggregatedDataEnum.impact_assessment_per_category,
                    ),
                    (
                        AggregatedDataEnum.amount_per_matching_required_for_flow,
                        AggregatedDataEnum.impact_assessment_per_matching,
                    ),
                ]:
                    if (
                        impact_prop_name in aggregated_props
                        and getattr(parent_flow, weight_prop_name)
                        and not getattr(parent_flow, impact_prop_name)
                    ):
                        _add_impact_assessment_prop(weight_prop_name, impact_prop_name, parent_flow)

                if AggregatedDataEnum.dfu_per_category_required_for_flow in aggregated_props:
                    if (
                        parent_flow.amount_per_category_required_for_flow
                        and not parent_flow.dfu_per_category_required_for_flow
                    ):
                        dfu_per_category = {}
                        for k in parent_flow.amount_per_category_required_for_flow.quantities.keys():
                            if parent_flow.daily_food_unit:
                                qty = ReferencelessQuantityProp.unvalidated_construct(
                                    value=parent_flow.daily_food_unit.amount_for_100g().value,
                                    unit_term_uid=parent_flow.daily_food_unit.unit_term_uid,
                                )
                                dfu_per_category[k] = qty
                        calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=parent_flow.uid,
                                prop_name="dfu_per_category_required_for_flow",
                                prop=QuantityPackageProp.unvalidated_construct(
                                    quantities=dfu_per_category,
                                    for_reference=ReferenceAmountEnum.amount_for_100g,
                                ),
                            )
                        )


class PostNutrientsAggregationGapFillingFactory(AbstractAggregationGapFillingFactory):
    """Aggregation gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.aggregated_props: list[AggregatedDataEnum] = [
            AggregatedDataEnum.amount_per_glossary_link_required_for_flow,
            AggregatedDataEnum.amount_per_category_required_for_flow,
            AggregatedDataEnum.amount_per_matching_required_for_flow,
            AggregatedDataEnum.dfu_per_category_required_for_flow,
            AggregatedDataEnum.impact_assessment_per_glossary_link,
            AggregatedDataEnum.impact_assessment_per_category,
            AggregatedDataEnum.impact_assessment_per_matching,
        ]

    def spawn_worker(self, node: Node) -> PostNutrientsAggregationGapFillingWorker:
        """Spawn aggregation gap filling worker."""
        return PostNutrientsAggregationGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = PostNutrientsAggregationGapFillingFactory
