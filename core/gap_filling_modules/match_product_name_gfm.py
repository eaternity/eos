import copy
from typing import TYPE_CHECKING, Dict
from uuid import UUID, uuid4

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingFactory, AddClientNodesGapFillingWorker
from gap_filling_modules.ingredient_splitter_gfm import IngredientSplitterGapFillingWorker
from gap_filling_modules.merge_linked_nodes_gfm import MergeLinkedNodesGapFillingWorker
from structlog import get_logger

from core.domain.data_error import ErrorClassification
from core.domain.deep_mapping_view import DeepListView
from core.domain.matching_item import MatchingItem
from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.names_prop import NamesProp
from core.domain.props.required_matching_prop import MatchingFailureReasonEnum, RequiredMatchingProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.glossary_link_service import GlossaryLinkService
from core.service.glossary_service import GlossaryService
from core.service.matching_service import MatchingService
from core.service.node_service import NodeService
from core.service.service_provider import ServiceProvider

if TYPE_CHECKING:
    from database.postgres.postgres_db import PostgresDb

logger = get_logger()

# used when retrieving from matching_service's cache
MATCH_PRODUCT_GFM_NAME = "MatchProductName"


class MatchProductNameGapFillingWorker(AddClientNodesGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "MatchProductNameGapFillingFactory"):
        """Initialize MatchProductNameGapFillingWorker.

        Matches the "raw_input"'s "names" of a product[Node] or ingredient[Node] to a tuple of Terms, using the
        matching db table. Performs lowercasing of the name before matching.
        E.g. matches a product with the name "trockene Karotten" to the Terms ("A1791", "J0116") from the glossary
        (="CARROT" and "DEHYDRATED OR DRIED" from FoodEx2).
        Then adds these Terms to the product's Props.
        """
        super().__init__(node, gfm_factory.add_client_nodes_factory)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        if isinstance(self.node, FoodProductFlowNode) and self.node.is_subdivision:
            logger.debug("[MatchProductName] subdivision node is always already matched --> skip")
            return False

        if not isinstance(self.node, FoodProductFlowNode):  # Should we include other flow nodes here?
            logger.debug("[MatchProductName] node is not a food product --> not scheduled.")
            return False
        else:
            if self.node.product_name and isinstance(self.node.product_name, (list, DeepListView)):
                logger.debug("[MatchProductName] found raw product_name --> scheduled.")
                return True
            else:
                logger.debug("[MatchProductName] node does not have raw product_name --> not scheduled.")
                return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(IngredientSplitterGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            # we want to match product names *after* splitting the ingredients; else we will not have a chance to match
            # the ingredients_declaration's ingredient names to their terms.
            logger.debug("[MatchProductName] waiting for IngredientSplitter GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        if global_gfm_state.get(MergeLinkedNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[MatchProductName] waiting for MergeLinkedNodesGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        logger.debug("[MatchProductName] --> ready.")
        return GapFillingWorkerStatusEnum.ready

    def matching_context(self) -> Dict[str, str | None]:
        context = dict()
        namespace_uid_prop, _ = self.node.get_prop_by_inheritance("namespace_uid")
        if namespace_uid_prop:
            context["namespace_uid"] = str(namespace_uid_prop)
        if self.node.supplier:
            context["supplier"] = self.node.supplier
        if self.node.producer:
            context["producer"] = self.node.producer
        parent_nodes = self.node.get_parent_nodes()
        if len(parent_nodes) > 0:
            parent_names, _ = parent_nodes[0].get_prop_by_inheritance("product_name")
            if parent_names:
                if isinstance(parent_names, NamesProp):
                    parent_names = parent_names.source_data_raw
                if parent_names:
                    for name in parent_names:
                        context[f"parent_name_{name['language']}"] = name["value"]
        return context

    async def run(self, calc_graph: CalcGraph) -> None:
        logger.debug("start running gap-filling-module MatchProductName...")

        matches: list[tuple[list[Term], str]] = []
        # the string we want to match to a Term
        names_user_input = self.node.product_name
        matching_context = self.matching_context()
        node_uid_in_db = None
        node_to_update_in_graph = None

        # check if all inputs are empty
        if not any([name_user_input["value"] for name_user_input in names_user_input]):
            matches = self.gfm_factory.matching_service.get_matching_by_string(
                "data error",
                MATCH_PRODUCT_GFM_NAME,
            )

            logger.warn('Empty name values! Matching this product with "data error" Term...', node=self.node)
            calc_graph.append_data_errors_log_entry(
                f'Empty name values! Matching node {self.node} with "data error" Term...',
            )
        else:
            # we first try to find an exact language match. Only if that does not succeed, we ignore the language:
            for do_exact_lang_match in [True, False]:
                for name_user_input in names_user_input:  # there can be different names because of different languages
                    # handling an edge case where the value is empty or `None`
                    if not name_user_input["value"]:
                        continue  # we continue to the next name entry

                    if do_exact_lang_match:
                        filter_lang = name_user_input.get("language", None)
                    else:
                        filter_lang = None

                    matches = self.gfm_factory.matching_service.get_matching_by_string(
                        name_user_input["value"],
                        MATCH_PRODUCT_GFM_NAME,
                        filter_lang=filter_lang,
                        filter_context=matching_context,
                    )

                    if len(matches) > 0:
                        # we break on the first found matching name
                        break

                if len(matches) > 0:
                    # if the first loop succeeded finding an exact language match, we do not continue searching
                    break

        if len(matches) == 0:  # no match --> let's do some logging
            # whether this node is a "compound product" (a product that is made of other food products),
            # for example Soy-sauce (Water, Soybeans, Salt, Sugar, Wheat)
            is_compound_product = self.node.is_combining_ingredients()

            if (
                getattr(self.node, "ingredients_declaration", None) is not None
                and self.node.gfm_state.worker_states.get(IngredientSplitterGapFillingWorker.__name__, "")
                == NodeGfmStateEnum.finished
                and not self.node.get_sub_nodes()
            ):
                # No sub-node after ingredient splitter GFM has been completed means that the calculation was
                # relying on product_name based matching...
                logger_lvl = logger.warn
                req_matching_prop = RequiredMatchingProp(
                    matching_failure_reason=MatchingFailureReasonEnum.ingredient_splitter,
                    faulty_declarations=self.node.ingredients_declaration,
                    names_to_match=names_user_input,
                    matching_context=matching_context,
                )
            elif len(self.node.get_sub_nodes()) > 0 or is_compound_product or self.node.link_to_sub_node is not None:
                logger_lvl = logger.debug
                # currently we don't aim at matching compound products to terms but in the future we might want to
                req_matching_prop = None
                # req_matching_prop = RequiredMatchingProp(
                #     matching_failure_reason=MatchingFailureReasonEnum.compound_product,
                #     names_to_match=self.node.product_name,
                #     matching_context=matching_context,
                # )
            elif self.node.added_by == IngredientSplitterGapFillingWorker.__name__:
                logger_lvl = logger.warn
                if (
                    self.node.declaration_index
                    and len(self.node.get_parent_nodes()[0].get_sub_nodes()) > 1
                    and len(self.node.get_parent_nodes()[0].get_sub_nodes()) - 1 == self.node.declaration_index.value
                ):
                    req_matching_prop = RequiredMatchingProp(
                        matching_failure_reason=MatchingFailureReasonEnum.after_splitter_and_last_ingredient,
                        names_to_match=self.node.product_name,
                        matching_context=matching_context,
                    )
                else:
                    req_matching_prop = RequiredMatchingProp(
                        matching_failure_reason=MatchingFailureReasonEnum.after_splitter,
                        names_to_match=self.node.product_name,
                        matching_context=matching_context,
                    )
            else:
                logger_lvl = logger.warn
                req_matching_prop = RequiredMatchingProp(
                    matching_failure_reason=MatchingFailureReasonEnum.no_matching_product,
                    names_to_match=self.node.product_name,
                    matching_context=matching_context,
                )

            if req_matching_prop is not None:
                # mark the node in the database as "required_matching" so that it can be matched manually:
                if self.node.original_node_uid:
                    node_uid_in_db = self.node.original_node_uid.uid
                    node_to_update_in_graph = self.node
                else:
                    # we need to crawl up the tree until we find the original node_uid
                    original_node_uid_prop, parent_node_in_db = self.node.get_prop_by_inheritance("original_node_uid")
                    if not original_node_uid_prop:
                        raise ValueError("Could not find original_node_uid in node or parent nodes.")
                    node_uid_in_db = original_node_uid_prop.uid
                    node_to_update_in_graph = parent_node_in_db

                # check if there is already a "required_matching" prop in the parent node:
                is_duplicate = False
                existing_req_matching = node_to_update_in_graph.required_matching
                if existing_req_matching and len(existing_req_matching) > 0:
                    # Check if this is a duplicate:
                    if req_matching_prop in existing_req_matching:
                        logger.debug("Duplicate required_matching prop --> skip")
                        is_duplicate = True

                # always update matching_required table
                await self.gfm_factory.matching_service.add_matching_required(
                    node_uid_in_db, calc_graph.get_root_node().original_node_uid.uid
                )
                # but only update the node if it is not a duplicate:
                if not is_duplicate:
                    await self.gfm_factory.node_service.update_node_prop(
                        node_uid_in_db, "required_matching", req_matching_prop, append=True
                    )
                    calc_graph.apply_mutation(
                        PropListMutation(
                            created_by_module=self.__class__.__name__,
                            node_uid=node_to_update_in_graph.uid,
                            prop_name="required_matching",
                            props=[req_matching_prop],
                            append=True,
                        )
                    )

            if logger_lvl.__name__ == logger.warn.__name__:
                calc_graph.append_data_errors_log_entry(
                    f"Could not match product name {names_user_input} in node {self.node}.",
                    error_classification=ErrorClassification.missing_matching,
                    node_uid=self.node.uid,
                    additional_specification={
                        "name": [name["value"] for name in names_user_input],
                    },
                )

            logger_lvl(
                "Could not match product name",
                names_user_input=names_user_input,
                node_uid=self.node.uid,
                is_compound_product=is_compound_product,
            )
        else:
            context_precedence = [
                "namespace_uid",
                "supplier",
                "producer",
            ]
            for context_key in context_precedence:
                # keep only the matches that have this context key:
                matches_filtered = [match for match in matches if match.data and context_key in match.data]
                if len(matches_filtered) >= 1:
                    # we continue with more specific context filtering:
                    matches = matches_filtered
                    continue
                elif len(matches_filtered) == 0:
                    # do not use this context key, but continue with the next one:
                    continue
                elif len(matches_filtered) == 1:
                    matches = matches_filtered
                    break

            matching_item: MatchingItem = matches[0]  # we go with the first match (for now...)
            term_uids = matching_item.term_uids
            product_name_terms: list[Term] = [
                self.gfm_factory.glossary_service.get_term_by_id(UUID(term_uid)) for term_uid in term_uids
            ]
            matched_node_uid: str | None = matching_item.matched_node_uid

            # log the applied matching:
            if not node_to_update_in_graph or not node_uid_in_db:
                if self.node.original_node_uid:
                    node_uid_in_db = self.node.original_node_uid.uid
                    node_to_update_in_graph = self.node
                else:
                    # we need to crawl up the tree until we find the original node_uid
                    original_node_uid_prop, parent_node_in_db = self.node.get_prop_by_inheritance("original_node_uid")
                    if original_node_uid_prop:
                        node_uid_in_db = original_node_uid_prop.uid

            if node_uid_in_db and matching_context.get("namespace_uid") and matching_item.uid:
                node_uid_in_db_copy = str(copy.deepcopy(node_uid_in_db))
                matching_item_uid_copy = str(copy.deepcopy(matching_item.uid))
                root_node_uid_copy = str(calc_graph.get_root_node().original_node_uid.uid)
                matching_context_copy = copy.deepcopy(
                    {key: value for key, value in matching_context.items() if value is not None}
                )
                self.gfm_factory.matching_service.add_apply_matching_item(
                    node_uid_in_db_copy, root_node_uid_copy, matching_item_uid_copy, matching_context_copy
                )
            else:
                logger.warn(
                    "Could not log apply_matching_item because at least one of the required fields is missing",
                    node_uid_in_db=node_uid_in_db,
                    namespace_uid=matching_context.get("namespace_uid"),
                    matching_uid=matching_item.uid,
                )

            if len(product_name_terms) > 0:
                logger.debug(
                    "create PropMutation to add product_name and nutrient_values", prod_term=product_name_terms
                )
                calc_graph.apply_mutation(
                    PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=self.node.uid,
                        prop_name="product_name",
                        prop=NamesProp(
                            terms=[GlossaryTermProp(term_uid=product_name.uid) for product_name in product_name_terms],
                            source_data_raw=names_user_input,
                        ),
                    )
                )

            if matched_node_uid and not self.node.get_sub_nodes():
                matched_node_uuid = UUID(matched_node_uid)
                await self.update_child_uids_by_parent_uid_and_local_node_cache(
                    root_node_uid=matched_node_uuid, service_provider=calc_graph.service_provider
                )
                # this is important such that flow-to-flow linking also works if the node already exists in
                # the calculation graph
                duplicate_node_uid = uuid4()
                await self.add_node_mutation(calc_graph, matched_node_uuid, self.node.uid, duplicate_node_uid)

                if isinstance(self.node, FoodProductFlowNode):
                    self.node.add_prop_for_amount_per_matching_required_for_flow(
                        calc_graph, matched_node_uuid, self.gfm_factory.gram_term.uid
                    )


class MatchProductNameGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: "PostgresDb", service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.matching_service: MatchingService = service_provider.matching_service
        self.glossary_link_service: GlossaryLinkService = service_provider.glossary_link_service
        self.glossary_service: GlossaryService = service_provider.glossary_service
        self.node_service: NodeService = service_provider.node_service
        self.add_client_nodes_factory: AddClientNodesGapFillingFactory = AddClientNodesGapFillingFactory(
            postgres_db, service_provider
        )
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", root_unit_term.access_group_uid)
        ]

    async def init_cache(self) -> None:
        pass  # caching is done at the matching service level

    def spawn_worker(self, node: Node) -> MatchProductNameGapFillingWorker:
        return MatchProductNameGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = MatchProductNameGapFillingFactory
