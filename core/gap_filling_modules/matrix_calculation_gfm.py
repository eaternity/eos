"Matrix calculation gap filling module."
import re
import time
from dataclasses import dataclass
from typing import List, Optional
from uuid import UUID

import numpy as np
from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.greenhouse_gfm import GreenhouseGapFillingWorker
from gap_filling_modules.ingredient_splitter_gfm import IngredientSplitterGapFillingWorker
from gap_filling_modules.inventory_connector_gfm import InventoryConnectorGapFillingWorker
from gap_filling_modules.processing_gfm import ProcessingGapFillingWorker
from gap_filling_modules.transportation_decision_gfm import TransportDecisionGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingWorker
from gap_filling_modules.water_scarcity_gfm import WaterScarcityGapFillingWorker
from scipy.sparse import csr_array
from structlog import get_logger

from core.domain.data_error import ErrorClassification
from core.domain.nodes import ElementaryResourceEmissionNode, ModeledActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.environmental_flows_prop import EnvironmentalFlowsProp
from core.domain.props.matrix_gfm_error_prop import MatrixGfmErrorProp
from core.domain.props.names_prop import NamesProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.domain.term import Term
from core.domain.util import get_flow_amount, get_production_amount
from core.graph_manager.calc_graph import CalcGraph, MetaDataForMostShallowNodes
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

try:
    from pypardiso import spsolve  # fastest solver; requires Intel MKL
except ImportError:
    from scipy.sparse.linalg import spsolve  # fallback, slower solver e.g. for M1 Macs

logger = get_logger()


@dataclass
class IdMapper:
    """A kind of bidict to efficiently map ids btw uuids and matrix ids. UUIDs are stored as ints."""

    def __init__(self):
        # initially, it was implemented with a simple list, but index() was taking too long
        self.uids2mids: dict[UUID, int] = {}
        self.mids2uids: list[UUID] = []

    def map(self, uid: UUID) -> int:
        """Returns the matrix id for a Node."""
        try:  # already mapped?
            mid = self.uids2mids[uid]
        except KeyError:  # then: create a new mapping
            mid = len(self.mids2uids)
            self.mids2uids.append(uid)
            self.uids2mids[uid] = mid
        return mid

    def __len__(self):
        "Returns length of mit to uid mapping."
        return len(self.mids2uids)

    def __getitem__(self, mid: int) -> UUID:
        """Returns the uuid for a matrix id."""
        return self.mids2uids[mid]


class MatrixBuilder:
    """A helper to build the technosphere and biosphere sparse matrces.

    The matrices are built in two steps:
    1. append() all matrix cells
    2. build() the matrices
    (you must NOT append() more cells after build()ing the matrices, otherwise mappings for technosphere will be wrong)
    """

    def __init__(self):
        self.id_mapper = IdMapper()

        # collection of demand vector indices used for subnode supply calculations.
        self.demand_vec_idx: list[int] = []

        # technosphere arrays (row, col, amount); later converted to sparse matrix
        self.tech_row_arr: list[int] = []
        self.tech_col_arr: list[int] = []
        self.tech_amount_arr: list[float] = []

        # biosphere list; only converted to sparse matrix when calling build()
        self.biosphere_arr: list[tuple[UUID, UUID, float]] = []

    def append_cache_data(
        self,
        node_uid: UUID,
        environmental_flows: EnvironmentalFlowsProp,
        required_environmental_flows: Optional[List] = None,
    ) -> None:
        for biosphere_node_uid, value in environmental_flows.flow_quantities.items():
            if required_environmental_flows is None or biosphere_node_uid in required_environmental_flows:
                self.biosphere_arr.append((node_uid, biosphere_node_uid, -value))

    def append_aggregated_cache(self, calc_graph: CalcGraph, modeled_activity_node: ModeledActivityNode) -> None:
        self.append_cache_data(
            node_uid=modeled_activity_node.uid,
            environmental_flows=modeled_activity_node.aggregated_cache,
            required_environmental_flows=calc_graph.required_environmental_flows,
        )

    def append(self, from_node: Node, to_node: Node, amount: float) -> None:
        """Sets a cell value in this Node's matrix property.

        Args:
            from_node: the node from which the flow originates
            to_node: the node to which the flow goes
            amount: the amount of the flow
        """
        if isinstance(
            to_node, ElementaryResourceEmissionNode
        ):  # TODO should we handle the ecoinvent type 'natural resource' here?
            # don't map biosphere ids yet because it would mess up the tech. ids; only do that when build() the matrix
            self.biosphere_arr.append((from_node.uid, to_node.uid, amount))
        else:
            self.tech_row_arr.append(self.id_mapper.map(from_node.uid))
            self.tech_col_arr.append(self.id_mapper.map(to_node.uid))
            self.tech_amount_arr.append(amount)

    @staticmethod
    def find_nodes_that_are_more_shallow_than_fixed_depth_and_add_them_to_supply_list(calc_graph: CalcGraph) -> None:
        """Check for which nodes we want to calculate the supply according to fixed_depth_for_calculating_supply.

        The dict of these nodes gets stored in calc_graph.nodes_to_calculate_supply.

        :param calc_graph: the calc_graph object
        """
        # get the root node and initialize the list of nodes to visit:
        root_node = calc_graph.get_root_node()
        nodes_to_visit = [root_node]

        # get the fixed_depth_for_calculating_supply from the calculation object
        fixed_depth_for_calculating_supply = calc_graph.calculation.fixed_depth_for_calculating_supply

        # set the default value for the path lengths to a value that is larger than fixed_depth_for_calculating_supply
        default_value = fixed_depth_for_calculating_supply + 1

        # initialize the shortest_path_lengths dictionary with the root node:
        shortest_path_lengths: dict = {root_node.uid: 0}

        # initialize the nodes_to_calculate_supply dictionary:
        nodes_to_calculate_supply: set[UUID] = set()

        # iterate over all nodes in the graph and calculate the path lengths until we exceed
        # the fixed_depth_for_calculating_supply:
        while nodes_to_visit:
            # get the walking node and its depth:
            walking_node = nodes_to_visit.pop()
            depth_of_walking_node = shortest_path_lengths[walking_node.uid]

            # if depth of walking node is smaller than fixed_depth_for_calculating_supply, iterate over its children:
            if depth_of_walking_node < fixed_depth_for_calculating_supply:
                for child_node in walking_node.get_sub_nodes():
                    # if current path for arriving at the child node is shorter than the stored shortest path length,
                    # update the shortest path length for this child and add the child node to the nodes_to_visit list:
                    if depth_of_walking_node + 1 < shortest_path_lengths.get(child_node.uid, default_value):
                        shortest_path_lengths[child_node.uid] = depth_of_walking_node + 1
                        nodes_to_visit.append(child_node)

        # iterate over all nodes in shortest_path_lengths and add the activities with a path length smaller than
        # fixed_depth_for_calculating_supply to nodes_to_calculate_supply:
        for uid, path_length in shortest_path_lengths.items():
            if path_length <= fixed_depth_for_calculating_supply:
                current_node = calc_graph.get_node_by_uid(uid)
                if isinstance(current_node, ActivityNode):
                    nodes_to_calculate_supply.add(current_node.uid)

        # store the result in calc_graph.nodes_to_calculate_supply:
        calc_graph.nodes_to_calculate_supply.update(nodes_to_calculate_supply)

    @staticmethod
    def find_most_shallow_node_for_each_gfm_and_add_them_to_supply_list(calc_graph: CalcGraph) -> None:
        """For each GFM find the most shallow node such that no node below it changes dynamically.

        For all most shallow nodes that are found, store meta information about these nodes
        in calc_graph.meta_data_for_most_shallow_nodes and add them to calc_graph.nodes_to_calculate_supply.

        :param calc_graph: the calc_graph object
        """

        def find_all_nodes_that_can_change_dynamically(
            calc_graph: CalcGraph, node_uids_that_can_change: set[UUID]
        ) -> set[UUID]:
            """Given a set of node_uids_that_can_change return all the nodes above these nodes.

            These are all the nodes that can change dynamically.

            :param calc_graph: the calc_graph object
            :param node_uids_that_can_change: a set of UUIDS of nodes that are tagged that they change dynamically
            :return all_node_uids_that_can_change: a set of UUIDS of all the nodes that can change dynamically
            """
            all_node_uids_that_can_change: set[UUID] = set()
            # starting from original nodes_that_can_change, store all their parent nodes in all_nodes_that_can_change
            for node_uid_that_can_change in node_uids_that_can_change:
                # intialize the nodes_to_visit and nodes_visited sets
                nodes_visited: set[Node] = set()
                node_that_can_change: Node = calc_graph.get_node_by_uid(node_uid_that_can_change)
                nodes_to_visit: list[str] = [node_that_can_change]
                # iteratively find all parents of this node and add them to the nodes that can change dynamically
                while nodes_to_visit:
                    node = nodes_to_visit.pop()
                    nodes_visited.add(node)
                    all_node_uids_that_can_change.add(node.uid)
                    for parent in node.get_parent_nodes():
                        if parent not in nodes_visited:
                            nodes_to_visit.append(parent)
            return all_node_uids_that_can_change

        def find_most_shallow_nodes_that_do_not_change_dynamically(
            calc_graph: CalcGraph, all_node_uids_that_can_change: dict[str, set[UUID]], gfm_name: str
        ) -> dict[UUID, list[str]]:
            """From a set of all_node_uids_that_can_change return the most shallow nodes that do not change dynamically.

            Args:
                calc_graph: the calc_graph object
                all_node_uids_that_can_change: a set of the UUIDS of all the nodes that can change dynamically
                gfm_name: the name of the GFM for which we want to find the most shallow nodes

            Returns:
                a dictionary containing the most shallow nodes that do not change dynamically
            """
            most_shallow_nodes: dict[UUID, dict[str, bool]] = {}
            # intialize the nodes_to_visit and nodes_visited sets
            nodes_visited: set[Node] = set()
            root_node = calc_graph.get_root_node()
            nodes_to_visit: list[str] = [root_node]
            # starting from root_node iterate over all subnodes until we find a node that does not change dynamically
            while nodes_to_visit:
                node = nodes_to_visit.pop()
                nodes_visited.add(node)
                if (
                    # to be a most shallow node it must be a brightway_process
                    isinstance(node, ModeledActivityNode)
                    # and it must not change dynamically for the current gfm
                    and node.uid not in all_node_uids_that_can_change[gfm_name]
                    # and be a node that persits in the database
                    and node.is_persistent
                ):
                    # add the node to the most_shallow_nodes as well as the information for which GFMs
                    # it has subnodes that can change dynamically.
                    # It has subnodes that can change dynamically for the GFM gfm
                    # if and only if this node is in all_node_uids_that_can_change[gfm]
                    most_shallow_nodes[node.uid] = {
                        gfm: (node.uid in all_node_uids_that_can_change[gfm])
                        for gfm in all_node_uids_that_can_change.keys()
                    }
                else:
                    # if it is not a most_shallow_node add its subnodes to the nodes_to_visit list
                    for child in node.get_sub_nodes():
                        if child not in nodes_visited:
                            nodes_to_visit.append(child)
            return most_shallow_nodes

        # for each GFM find all the nodes that can change dynamically:
        all_node_uids_that_can_change: dict[str, set[UUID]] = {}
        for gfm_name in calc_graph.calculation.gfms_for_which_to_save_most_shallow_nodes:
            node_uids_that_can_change = calc_graph.nodes_that_can_change_dynamically_for_each_gfm.get(gfm_name, set())
            # find all the nodes that can change dynamically:
            all_node_uids_that_can_change[gfm_name] = find_all_nodes_that_can_change_dynamically(
                calc_graph, node_uids_that_can_change
            )

        # for each GFM find all most_shallow_nodes that do not change dynamically:
        for gfm_name in calc_graph.calculation.gfms_for_which_to_save_most_shallow_nodes:
            # find the most shallow nodes that do not change dynamically:
            most_shallow_nodes = find_most_shallow_nodes_that_do_not_change_dynamically(
                calc_graph, all_node_uids_that_can_change, gfm_name
            )
            # calculate the supply for all nodes that are most shallow for at least one gfm:
            calc_graph.nodes_to_calculate_supply.update(most_shallow_nodes.keys())
            # add them to the set of all most shallow nodes
            for most_shallow_node in most_shallow_nodes.keys():
                if most_shallow_node not in calc_graph.meta_data_for_most_shallow_nodes:
                    calc_graph.meta_data_for_most_shallow_nodes[most_shallow_node] = MetaDataForMostShallowNodes()
                calc_graph.meta_data_for_most_shallow_nodes[most_shallow_node].is_most_shallow_node_for_gfms.append(
                    gfm_name
                )
                calc_graph.meta_data_for_most_shallow_nodes[
                    most_shallow_node
                ].has_sub_nodes_that_can_change_dynamically_for_gfms.update(most_shallow_nodes[most_shallow_node])

    def add_demand_vector(
        self,
        node: Node,
        child_of_root_node_uid: UUID,
        calc_graph: CalcGraph,
        depth_of_walking_node: int,
        max_depth: int | None = None,
    ) -> None:
        "Add demand vector."
        # don't add the demand vector for the nodes deeper than max_depth
        if max_depth is not None and max_depth > 1 and depth_of_walking_node > max_depth:
            return

        # never perform supply calculations on emissions:
        if isinstance(node, ElementaryResourceEmissionNode):
            return

        # supply calculations are only performed on a brightway_process
        # if the node is contained in calc_graph.nodes_to_calculate_supply
        # or if the node is the recipe node
        # or if the node has a grandparent node that is not a ModeledActivityNode:
        # The last condition is needed as otherwise, we need the impact_assessment_supply_for_root to compute
        # parent flow's amount for root.
        if (
            isinstance(node, ModeledActivityNode)
            and node.uid != child_of_root_node_uid
            and node.uid not in calc_graph.nodes_to_calculate_supply
            and all(
                isinstance(grandparent_node, ModeledActivityNode)
                for grandparent_node in node.get_parent_nodes()[0].get_parent_nodes()
            )
        ):
            return

        # add an entry to later add a vector for this node to the demand matrix:
        self.demand_vec_idx.append(self.id_mapper.map(node.uid))

    def build(self) -> tuple[csr_array, csr_array, csr_array]:
        """Builds the technosphere, biosphere, and demand sparse matrices."""
        # build technosphere matrix; easy, since we already mapped ids
        sparse_array_building_start_time = time.perf_counter_ns()
        tech = csr_array(
            (self.tech_amount_arr, (self.tech_row_arr, self.tech_col_arr)),
            shape=(len(self.id_mapper), len(self.id_mapper)),
            dtype=float,
        )

        demand_col_arr = np.arange(len(self.demand_vec_idx))
        demand_amount = np.ones(len(self.demand_vec_idx), dtype=float)
        demand = csr_array(
            (demand_amount, (self.demand_vec_idx, demand_col_arr)),
            shape=(len(self.id_mapper), len(self.demand_vec_idx)),
            dtype=float,
        )

        # biosphere matrix; here we need to map ids first
        bio_arr_list = [
            (self.id_mapper.map(from_uid), self.id_mapper.map(to_uid), amount)
            for from_uid, to_uid, amount in self.biosphere_arr
        ]
        if bio_arr_list:
            bio_row_arr, bio_col_arr, bio_amount_arr = zip(*bio_arr_list)
        else:
            bio_row_arr, bio_col_arr, bio_amount_arr = [], [], []
        # now build sparse matrix
        bio = csr_array(
            (bio_amount_arr, (bio_row_arr, bio_col_arr)), shape=(len(self.id_mapper), len(self.id_mapper)), dtype=float
        )
        logger.info(
            "[MatrixGFM] finished building sparce matrices.",
            duration=(time.perf_counter_ns() - sparse_array_building_start_time) / 1e9,
        )

        return tech, bio, demand


class MatrixCalculationGapFillingWorker(AbstractGapFillingWorker):
    """Perform MatrixCalculation calculation on this recipe.

    - Collects flow nodes into a matrix
    - Create scipy sparse matrices for technosphere, biosphere and functional unit
    - Solves the linear system :math:`Ax=B`. This is the part that should be the most time-consuming
    - Stores the resulting demand array and environmental flows in Props
    """

    def __init__(self, node: Node, gfm_factory: "MatrixCalculationGapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Only run on recipe (=root) node."""
        if self.node.uid == self.node.get_calculation().root_node_uid:
            return True
        # if len(self.node.get_parent_nodes()) == 0:
        #     return True
        else:
            logger.debug("[MatrixCalculation] not on root node --> not scheduled.")
            return False

    @staticmethod
    def graph_building_gfm_workers_finished(gfm_worker: AbstractGapFillingWorker) -> GapFillingWorkerStatusEnum:
        """Check if workers for building the life cycle assessment graph have finished running."""
        msg_prefix = re.sub("GapFillingWorker", "", gfm_worker.__class__.__name__)
        global_gfm_state = gfm_worker.get_global_gfm_state()

        if global_gfm_state.get(UnitWeightConversionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(
                f"[{msg_prefix}] recipe node or sub-nodes do not yet have converted units --> not can_run_now."
            )
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(GreenhouseGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(f"[{msg_prefix}] greenhouse activities are not yet added to the graph --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(ProcessingGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(f"[{msg_prefix}] wait for all ProcessingGapFillingWorker --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(IngredientSplitterGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(f"[{msg_prefix}] wait for all IngredientSplitterGapFillingWorker --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(WaterScarcityGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(f"[{msg_prefix}] wait for all WaterScarcityGapFillingWorker --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(AddClientNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(
                f"[{msg_prefix}] recipe root-node does not yet have a child process|emission loaded"
                " --> not can_run_now."
            )
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(InventoryConnectorGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(
                f"[{msg_prefix}] waiting for InventoryConnectorGapFillingWorker to finish" " --> not can_run_now."
            )
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(TransportDecisionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug(f"[{msg_prefix}] wait for TransportDecisionGapFillingWorker to finish" " --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        logger.debug(f"[{msg_prefix}] recipe root-node has a child process|emission loaded. --> can_run_now.")
        return GapFillingWorkerStatusEnum.ready

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        "Can the calculation run now."
        return self.graph_building_gfm_workers_finished(self)

    async def run(self, calc_graph: CalcGraph) -> None:
        "Run the calculation."
        if any(
            error.error_classification == ErrorClassification.missing_matching for error in calc_graph.data_errors_log
        ):
            logger.warn("Skipping Matrix calculation GFM because matching failed.")
            return
        elif any(
            error.error_classification == ErrorClassification.missing_lca_inventory
            for error in calc_graph.data_errors_log
        ):
            logger.warn(
                "Skipping Matrix calculation GFM because at least flow-node has no LCA inventory node attached."
            )
            return
        elif any(
            error.error_classification == ErrorClassification.failed_amount_estimation
            for error in calc_graph.data_errors_log
        ):
            logger.warn("Skipping Matrix calculation GFM because flow amount estimation failed.")
            return
        logger.debug("start running matrix_calculation gfm...")

        # 1ST PART: collect technosphere, biosphere, and demand matrices
        # demand matrix is an N x k matrix, where k is the number of subnodes for which
        # supply is computed (N is the size of technosphere).
        matrix_builder = MatrixBuilder()
        visited_activity_nodes: set[UUID] = set()  # to avoid double-counting
        zero_flow_nodes: set[UUID] = set()

        # Start building the matrix from the first child of the root.
        nodes_to_visit = [calc_graph.child_of_root_node()]
        shortest_path_lengths: dict[int, int] = {calc_graph.child_of_root_node().uid.int: 1}

        def handle_flow_node(flow: FlowNode, calc_graph: CalcGraph) -> None:
            # print(flow.gfm_state.model_dump())
            if flow.amount is None:
                if isinstance(flow.product_name, NamesProp) and any(
                    term.term_uid in self.gfm_factory.non_food_term_uids for term in flow.product_name.terms
                ):
                    # Skip non-food flows
                    logger.debug(f"[MatrixGFM]: Skipping non-food flow {flow}")
                    return
                elif flow.added_by == IngredientSplitterGapFillingWorker.__name__:
                    declaration, parent_with_ingredient_declaration = flow.get_prop_by_inheritance(
                        "ingredients_declaration"
                    )
                    if parent_with_ingredient_declaration and declaration:
                        calc_graph.append_data_errors_log_entry(
                            f"Could not estimate the amounts of ingredients in {declaration}"
                            f" in node {parent_with_ingredient_declaration}.",
                            error_classification=ErrorClassification.failed_amount_estimation,
                            additional_specification={
                                "declaration": [dec["value"] for dec in declaration],
                            },
                        )
                sub_nodes = flow.get_sub_nodes()
                if len(sub_nodes) == 0:
                    info_about_sub_nodes = "It has no sub-activity."
                elif len(sub_nodes) == 1:
                    info_about_sub_nodes = (
                        f"It has one sub-activity: {sub_nodes[0]} with ",
                        f"production_amount={sub_nodes[0].production_amount}.",
                    )
                else:
                    info_about_sub_nodes = "It has multiple sub-activities."
                raise ValueError((f"Flow {flow} has no flow amount (converted amount). ", info_about_sub_nodes))

            flow_amount = get_flow_amount(flow)
            if flow_amount != 0.0:
                parent_nodes = flow.get_parent_nodes()
                sub_nodes = flow.get_sub_nodes()
                if len(sub_nodes) == 0:
                    raise ValueError(f"flow {flow} has no sub-nodes")

                assert len(parent_nodes) == 1, f"flow {flow} has {len(parent_nodes)} parents but should have 1"
                assert len(sub_nodes) == 1, f"flow {flow} has {len(sub_nodes)} sub-nodes but should have 1"

                # notice the minus sign here, this is a consumption
                matrix_builder.append(parent_nodes[0], sub_nodes[0], -flow_amount)

                if depth_of_walking_node + 1 < shortest_path_lengths.get(sub_nodes[0].uid.int, np.inf):
                    shortest_path_lengths[sub_nodes[0].uid.int] = depth_of_walking_node + 1
                nodes_to_visit.append(sub_nodes[0])
            else:
                zero_flow_nodes.add(flow.uid)

        try:
            # First pass: build technosphere matrix.
            if calc_graph.calculation and calc_graph.calculation.save_as_system_process:
                matrix_builder.find_nodes_that_are_more_shallow_than_fixed_depth_and_add_them_to_supply_list(calc_graph)
                matrix_builder.find_most_shallow_node_for_each_gfm_and_add_them_to_supply_list(calc_graph)

            while nodes_to_visit:
                walking_node = nodes_to_visit.pop()
                depth_of_walking_node = shortest_path_lengths[walking_node.uid.int]

                # handle a flow node
                if isinstance(walking_node, FlowNode):
                    handle_flow_node(walking_node, calc_graph)

                else:  # handle an activity node
                    # don't visit twice activity nodes, since their descendants are already in the technosphere matrix
                    if walking_node.uid not in visited_activity_nodes:
                        production_amount = get_production_amount(walking_node)
                        if production_amount == 0.0:
                            if (
                                walking_node.uid == calc_graph.child_of_root_node().uid
                                and calc_graph.get_root_node()
                                and calc_graph.get_root_node().amount
                                and calc_graph.get_root_node().amount.value == 0.0
                            ):  # Zero production amount with zero parent flow amount is handled below separately.
                                zero_flow_nodes.add(calc_graph.get_root_node().uid)
                                continue

                            # Raise as the parent flow amount is non-zero, but the production amount is zero.
                            # We cannot satisfy this condition as the supply for root would be infinite.
                            raise ValueError(
                                "Matrix GFM could not run since one of the production amounts is zero with"
                                "non-zero parent flow amount."
                            )

                        visited_activity_nodes.add(walking_node.uid)

                        # add a self-connection (i.e. production_amount, which is usually 1 kg for brightway nodes).
                        matrix_builder.append(walking_node, walking_node, production_amount)

                        # Add aggregated cache.
                        if (
                            isinstance(walking_node, ModeledActivityNode)
                            and walking_node.aggregated_cache
                            and walking_node.aggregated_cache.implicitly_added_to_calc_graph
                        ):
                            matrix_builder.append_aggregated_cache(calc_graph, walking_node)
                        # Handle cached child of root activity node:
                        # If the environmental flows are not attached explicitly as children nodes,
                        # we have to read it from the environmental_flows property.
                        elif (
                            (not self.node.get_calculation().explicitly_attach_cached_elementary_resource_emission)
                            and isinstance(walking_node, ActivityNode)
                            and walking_node.environmental_flows is not None
                        ):
                            matrix_builder.append_cache_data(
                                node_uid=walking_node.uid,
                                environmental_flows=walking_node.environmental_flows,
                            )

                        # add an entry for this node to later add it as a vector to the demand matrix:
                        matrix_builder.add_demand_vector(
                            walking_node,
                            calc_graph.calculation.child_of_root_node_uid,
                            calc_graph,
                            depth_of_walking_node,
                            calc_graph.calculation.max_depth_of_returned_graph,
                        )

                        child_nodes = walking_node.get_sub_nodes()
                        if len(child_nodes) == 0 and not (
                            isinstance(walking_node, (ModeledActivityNode, ElementaryResourceEmissionNode))
                            or (isinstance(walking_node, ActivityNode) and walking_node.environmental_flows)
                        ):
                            if not any(
                                error.error_classification == ErrorClassification.missing_sub_flows_in_client_sent_data
                                and error.node_uid == walking_node.uid
                                for error in calc_graph.data_errors_log
                            ):
                                raise ValueError(
                                    f"activity node {walking_node} has no child nodes and is not an emission"
                                )

                        # iterate recursively over sub-nodes
                        for child in child_nodes:
                            if depth_of_walking_node + 1 < shortest_path_lengths.get(child.uid.int, np.inf):
                                shortest_path_lengths[child.uid.int] = depth_of_walking_node + 1
                            nodes_to_visit.append(child)

        except ValueError as e:
            logger.error(f"could not build matrices: {e}")
            # we do not add any mutation to the graph, so we can just return
            prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="matrix_gfm_error",
                prop=MatrixGfmErrorProp(value=f"Error: {str(e)}"),
            )
            calc_graph.apply_mutation(prop_mutation)
            return

        # Second pass:
        # Add zero impact_assessment_supply_for_root and empty environmental flows for children nodes of a flow node
        # with zero flow amount.
        nodes_to_visit = list(calc_graph.get_node_by_uid(node_uid) for node_uid in zero_flow_nodes)
        while nodes_to_visit:
            walking_node = nodes_to_visit.pop()

            # handle a flow node
            if isinstance(walking_node, FlowNode):
                if walking_node.get_sub_nodes():
                    nodes_to_visit.append(walking_node.get_sub_nodes()[0])

            else:  # handle an activity node
                # don't visit twice activity nodes:
                # their children nodes are either already in the technosphere matrix.
                # or have been assigned zero impact_assessment_supply_for_root & empty environmental flows.
                if (
                    walking_node.uid not in visited_activity_nodes
                ):  # It is crucial to use the same visited_activity_nodes from the first pass.
                    visited_activity_nodes.add(walking_node.uid)

                    prop_mutation = PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=walking_node.uid,
                        prop_name="impact_assessment_supply_for_root",
                        prop=QuantityProp.unvalidated_construct(
                            value=0.0,
                            unit_term_uid=self.gfm_factory.production_amount_unit_term.uid,
                            for_reference=ReferenceAmountEnum.amount_for_root_node,
                        ),
                    )
                    calc_graph.apply_mutation(prop_mutation)

                    env_flows_prop_mutation = PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=walking_node.uid,
                        prop_name="environmental_flows",
                        prop=EnvironmentalFlowsProp.unvalidated_construct(flow_quantities={}),
                    )
                    calc_graph.apply_mutation(env_flows_prop_mutation)

                    # iterate recursively over sub-nodes
                    for child in walking_node.get_sub_nodes():
                        nodes_to_visit.append(child)

        technosphere_mx, biosphere_mx, demand_mx = matrix_builder.build()

        debug_output_matrices = False
        # FIXME Why is this neither used nor tested?
        if debug_output_matrices:
            import pandas as pd

            matrix_ids = pd.DataFrame(
                [
                    calc_graph.get_node_by_uid(m).props["raw_input"][0].data["key"]
                    for m in matrix_builder.id_mapper.mids2uids
                ]
            )
            matrix_ids.to_csv("matrix_ids2.csv")
            from scipy.sparse import save_npz

            save_npz("technosphere_mx2.npz", technosphere_mx)
            save_npz("biosphere_mx2.npz", biosphere_mx)

        # production_amount that results when removing the loss due to self-connections:
        child_of_root_node = calc_graph.child_of_root_node()
        assert isinstance(child_of_root_node, ActivityNode)
        # Multiply with root flow node amount.
        if child_of_root_node.production_amount.value == 0.0:
            if self.node.amount.value != 0.0:
                logger.error(f"Production amount is 0.0, but root-flow amount is {self.node.amount.value}")
                return
            else:
                root_node_net_production_amount = 0.0
        else:
            root_node_net_production_amount = (
                technosphere_mx[0, 0] * self.node.amount.value / child_of_root_node.production_amount.value
            )

        # 2ND PART: build demand (functional unit) vector
        demand_mx_dense = demand_mx.todense()

        # 3RD PART: solve linear system & store results in a Prop
        linear_system_solver_start_time = time.perf_counter_ns()
        if technosphere_mx.shape == (0, 0):
            logger.debug("Technosphere matrix is empty. No matrix spsolve is not necessary.")
            return

        supply_mat: np.ndarray = spsolve(technosphere_mx.transpose(), demand_mx_dense)
        logger.info(
            "[MatrixGFM] finished solving linear system.",
            duration=(time.perf_counter_ns() - linear_system_solver_start_time) / 1e9,
        )

        if len(supply_mat.shape) == 1:
            supply_mat = np.reshape(
                supply_mat, (supply_mat.shape[0], 1)
            )  # If supply_mat is a vector, convert it to Nx1 matrix.
        if len(supply_mat.shape) == 0:
            supply_mat = np.array([[supply_mat]])
        if np.isnan(supply_mat).any():
            raise ValueError("MatrixCalculation calculation: NaN in scaling matrix")
        logger.debug("MatrixCalculation completed", supply_mat=supply_mat)
        supply_mat_for_env_flows = supply_mat.copy()

        # Loop over all sub-nodes (except brightway_process):
        production_amount_term_uid = self.gfm_factory.production_amount_unit_term.uid

        # Initialize matrix to collect all supply vectors
        biosphere_mx_transpose_truncated = biosphere_mx[: supply_mat.shape[0], :].transpose()

        for idx, sn_id in enumerate(matrix_builder.demand_vec_idx):
            sn_uid = matrix_builder.id_mapper[sn_id]

            # save supply amount in each activity node that would be necessary to produce root flow amount.
            prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=sn_uid,
                prop_name="impact_assessment_supply_for_root",
                prop=QuantityProp.unvalidated_construct(
                    value=supply_mat[sn_id, 0] * root_node_net_production_amount,
                    unit_term_uid=production_amount_term_uid,
                    for_reference=ReferenceAmountEnum.amount_for_root_node,
                ),
            )
            calc_graph.apply_mutation(prop_mutation)

            # we rescale the supply vector to the production amount of the node:
            production_amount_per_unit_process = get_production_amount(calc_graph.get_node_by_uid(sn_uid))
            supply_mat_for_env_flows[:, idx] *= production_amount_per_unit_process

        # 4TH PART: calculate environmental flows
        # pad the supply vector with zeros to match the size of the biosphere matrix
        env_flow_calculation_start_time = time.perf_counter_ns()
        environmental_flows_matrix = biosphere_mx_transpose_truncated.dot(supply_mat_for_env_flows)
        logger.info(
            "[MatrixGFM] finished calculating environmental flows for all nodes by sparse matrix multiplication.",
            duration=(time.perf_counter_ns() - env_flow_calculation_start_time) / 1e9,
        )

        # Use NumPy to find non-zero flows and negate the amounts
        negative_environmental_flow_matrix = -environmental_flows_matrix

        # Process the resulting matrix to create the necessary mutations
        for idx, sn_id in enumerate(matrix_builder.demand_vec_idx):
            sn_uid = matrix_builder.id_mapper[sn_id]

            non_zero_indices = np.nonzero(negative_environmental_flow_matrix[:, idx])[0]
            # Construct the dictionary
            mapped_environmental_nonzero_flows = {
                matrix_builder.id_mapper.mids2uids[i]: negative_environmental_flow_matrix[i, idx]
                for i in non_zero_indices
            }

            # mutation
            logger.debug("create PropMutation to add MatrixCalculation environmental flows")
            env_flows_prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=sn_uid,
                prop_name="environmental_flows",
                prop=EnvironmentalFlowsProp.unvalidated_construct(flow_quantities=mapped_environmental_nonzero_flows),
            )
            calc_graph.apply_mutation(env_flows_prop_mutation)


class MatrixCalculationGapFillingFactory(AbstractGapFillingFactory):
    "Factory."

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.production_amount_unit_term = Term(data={}, name="", sub_class_of=None)
        self.non_food_term_uids: frozenset[UUID] = frozenset()

    async def init_cache(self) -> None:
        "Initialise the cache."
        root_access_group_uid = self.service_provider.glossary_service.root_term.access_group_uid
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.production_amount_unit_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_production_amount", root_unit_term.access_group_uid)
        ]
        foodex2_access_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )
        self.non_food_term_uids = frozenset(
            [
                self.service_provider.glossary_service.terms_by_xid_ag_uid[("EAT-0002", foodex2_access_group_uid)].uid,
                self.service_provider.glossary_service.terms_by_xid_ag_uid[
                    ("EOS_data_error", root_access_group_uid)
                ].uid,
            ]
        )

    def spawn_worker(self, node: Node) -> MatrixCalculationGapFillingWorker:
        "Spawn a worker."
        return MatrixCalculationGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = MatrixCalculationGapFillingFactory
