"Ingredient amount estimator gap filling module."
import uuid
from typing import Optional, Tuple, Union
from uuid import UUID

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.ingredicalc.helpers import nutrients_dict_to_prop
from gap_filling_modules.ingredicalc.optimiser import run
from gap_filling_modules.ingredient_splitter_gfm import IngredientSplitterGapFillingWorker
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingWorker
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from gap_filling_modules.nutrient_subdivision_gfm import NutrientSubdivisionGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingWorker, UnitWeightConverter
from structlog import get_logger

from core.domain.data_error import ErrorClassification
from core.domain.deep_mapping_view import DeepMappingView
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode, ModeledActivityNode
from core.domain.nodes.node import Node
from core.domain.props import (
    IngredientAmountEstimatorStatusProp,
    QuantityPackageProp,
    QuantityProp,
    ReferencelessQuantityProp,
)
from core.domain.props.names_prop import NamesProp
from core.domain.props.quantity_package_prop import QuantityPackageSourceEnum
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop import UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()

# TODO: for now, this default amount will be equal to 1 -- this needs to be discussed & fixed later
DEFAULT_CONVERTED_AMOUNT = 1
IngredientKey = Tuple[tuple, str, UUID, int, str]


class IngredientAmountEstimatorGapFillingWorker(AbstractGapFillingWorker):
    """Estimate the amount of each ingredient in a product, based on the nutrient values of individual ingredients.

    And the overall nutrient values of the product. Relies on a library `ingredicalc` previousely written at Eaternity.
    Uses CVXPY under the hood, a library for convex optimization problems.
    """

    def __init__(self, node: Node, gfm_factory: "IngredientAmountEstimatorGapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    @staticmethod
    def nutrient_values_exist(nutrient_values: Optional[Union[DeepMappingView, dict, QuantityPackageProp]]) -> bool:
        """Returns true if nutrient values exist and are greater than 0."""
        if not nutrient_values:
            return False
        if isinstance(nutrient_values, (DeepMappingView, dict)) and any(
            nutrient_value > 0 for nutrient_value in nutrient_values.values()
        ):
            return True
        elif isinstance(nutrient_values, QuantityPackageProp) and any(
            nutrient_value.value > 0 for nutrient_value in nutrient_values.quantities.values()
        ):
            return True
        else:
            return False

    @staticmethod
    def parent_nutrients_available(current_node: Node) -> bool:
        parent_nutrient_values_available = False
        if current_node.get_parent_nodes():
            for parent_node in current_node.get_parent_nodes():
                parent_nutrient_values = getattr(parent_node, "nutrient_values", None)
                parent_nutrient_values_available = IngredientAmountEstimatorGapFillingWorker.nutrient_values_exist(
                    parent_nutrient_values
                )
                if parent_nutrient_values_available:
                    break
        return parent_nutrient_values_available

    def should_be_scheduled(self) -> bool:
        "Scheduled if the node holds nutrient_values and is a Foodprocessing activity."
        if not isinstance(self.node, FoodProcessingActivityNode):
            logger.debug("[IngredientAmountEstimator] Node is not recipe or food_product --> not scheduled.")
            return False
        else:
            if (
                self.node.get_parent_nodes()
                and getattr(self.node.get_parent_nodes()[0], "ingredients_declaration", None) is not None
                and self.node.added_by == IngredientSplitterGapFillingWorker.__name__
            ):
                logger.debug(
                    "[IngredientAmountEstimator] Node's parent has ingredients_declaration, "
                    "and the node itself was added by the ingredient splitter --> scheduled."
                )
                return True

            parent_nutrient_values_available = self.parent_nutrients_available(self.node)

            if parent_nutrient_values_available:
                logger.debug("[IngredientAmountEstimator] Found nutrient_values inside node --> scheduled.")
                return True
            else:
                logger.debug(
                    f"[IngredientAmountEstimator] nutrient_values is empty " f"on node {self.node} --> not scheduled."
                )
                return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """We want to estimate ingredient amounts *after* matching for product names and performing subdivision.

        Because it attaches the Term's nutrient values.
        """
        # check if sub-node and parent-node have the same nutrient-values, in this case we don't need to run the GFM
        parent_food_product_flow_nodes = [
            parent_flow for parent_flow in self.node.get_parent_nodes() if isinstance(parent_flow, FoodProductFlowNode)
        ]
        child_food_product_flow_nodes = [
            child_flow for child_flow in self.node.get_sub_nodes() if isinstance(child_flow, FoodProductFlowNode)
        ]
        if (
            len(parent_food_product_flow_nodes) == 1
            and len(child_food_product_flow_nodes) == 1
            and not child_food_product_flow_nodes[0].added_by == IngredientSplitterGapFillingWorker.__name__
        ):
            if parent_food_product_flow_nodes[0].nutrient_values == child_food_product_flow_nodes[0].nutrient_values:
                logger.debug(
                    f"[IngredientAmountEstimator] Parent and sub-node have the same nutrient values, "
                    f"no need to run the GFM on {self.node} --> cancel."
                )
                return GapFillingWorkerStatusEnum.cancel

        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(UnitWeightConversionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for UnitWeightConversionGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(AddClientNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for AddClientNodesGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(MatchProductNameGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for MatchProductNameGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(IngredientSplitterGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for IngredientSplitterGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(NutrientSubdivisionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[IngredientAmountEstimator] waiting for NutrientSubdivision GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(LinkTermToActivityNodeGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[IngredientAmountEstimator] waiting for LinkTermToActivity GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        # ensure that this GFM is run first on the subnodes.
        # if any child nodes of the current node still have
        # ingredient amount estimator scheduled, it is *not* ready to run on it
        # have to recurse the whole tree, some intermediate notes might not have nutrient values and wont be scheduled
        def has_child_scheduled(current_node: Node) -> bool:
            for sub_flow_node in current_node.get_sub_nodes():
                for sub_activity_node in sub_flow_node.get_sub_nodes():
                    if not isinstance(sub_activity_node, FoodProcessingActivityNode):
                        continue
                    if (
                        sub_activity_node.gfm_state.worker_states.get(self.__class__.__name__)
                        == NodeGfmStateEnum.scheduled
                    ):
                        return True
                    if has_child_scheduled(sub_activity_node):
                        return True
            return False

        if not hasattr(self.node, "production_amount") or not (
            self.node.production_amount and isinstance(self.node.production_amount, QuantityProp)
        ):
            logger.debug("[IngredientAmountEstimator] waiting for production amount to be added --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        else:
            logger.debug("[IngredientAmountEstimator] Found nutrient_values and production_amount --> ready.")
            if has_child_scheduled(self.node):
                return GapFillingWorkerStatusEnum.reschedule
            else:
                return GapFillingWorkerStatusEnum.ready

    def handle_fixed_percentages(self, flat_nutrients: dict, fixed_percentages: dict, calc_graph: CalcGraph) -> None:
        for key in flat_nutrients:
            _, _, ingredient_uid, _, _ = key
            ingredient_node = calc_graph.get_node_by_uid(ingredient_uid)
            if (getattr(ingredient_node, "amount", None) is not None) or (key not in fixed_percentages):
                continue

            if not ingredient_node.get_parent_nodes() or not (
                ingredient_node.get_parent_nodes()[0].production_amount
                and isinstance(ingredient_node.get_parent_nodes()[0].production_amount, QuantityProp)
            ):
                logger.debug("[IngredientAmountEstimator] fixed percentage node has no parent. Skip.")
                continue

            if ingredient_node.get_sub_nodes() and ingredient_node.get_sub_nodes()[0].production_amount:
                sub_node_unit = ingredient_node.get_sub_nodes()[0].production_amount.get_unit_term()
            else:
                # Leaf nodes attach to ModeledActivityNodes with production_amount = 1kg. Assume kilogram unit.
                sub_node_unit = self.gfm_factory.kilogram_term

            parent_production_amount = ingredient_node.get_parent_nodes()[0].production_amount
            if parent_production_amount.get_unit_term().sub_class_of != sub_node_unit.sub_class_of:
                logger.debug(
                    "[IngredientAmountEstimator] fixed percentage node has parent and child with"
                    " inconsistent production_amount unit. Skip."
                )
                continue

            parent_production_amount_unit = parent_production_amount.get_unit_term()

            if parent_production_amount_unit.uid == sub_node_unit.uid:
                parent_amount_in_sub_node_unit = parent_production_amount.value
            else:
                unit_base_term = self.gfm_factory.service_provider.glossary_service.terms_by_xid_ag_uid.get(
                    (sub_node_unit.sub_class_of, sub_node_unit.access_group_uid)
                )
                if not unit_base_term or not UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING.get(unit_base_term.xid):
                    logger.debug(
                        "[IngredientAmountEstimator] fixed percentage node has parent and child with"
                        " inconsistent production_amount unit. Skip."
                    )
                    continue

                parent_amount_in_sub_node_unit = UnitWeightConverter.convert_between_same_unit_types(
                    parent_production_amount.value,
                    parent_production_amount.get_unit_term(),
                    sub_node_unit,
                    UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING[unit_base_term.xid],
                )

            flow_amount = QuantityProp(
                value=parent_amount_in_sub_node_unit * fixed_percentages[key],
                unit_term_uid=sub_node_unit.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            )

            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=ingredient_uid,
                    prop_name="amount",
                    prop=flow_amount,
                )
            )

    async def run(self, calc_graph: CalcGraph) -> None:
        "Rund the GFM."
        logger.debug(f"start running gap-filling-module IngredientAmountEstimatorGFM on {self.node}")

        # collecting data for algorithm -- see ingredicalc.optimiser.run() for details
        flat_nutrients: dict[IngredientKey, QuantityPackageProp | None] = {}
        fixed_percentages: dict[IngredientKey, Optional[float]] = {}
        missing_leaf_uids: set[uuid.UUID] = set()

        async def visit_sub_ingredients(parent_food_product: Node, parent_level: Tuple[int, ...]) -> None:
            # Populates flat_nutrients and fixed_percentages: recursively visits all sub-ingredients; captures the
            # ingredients and their nutrient values.
            # regarding the graph structure: sub-ingredients nodes have been created by ingredient_splitter_gfm,
            # which interlaces a food_product_flow and a food_product for each ingredient level.
            this_level_index = 0

            sub_nodes = parent_food_product.get_sub_nodes()
            sub_nodes = sorted(
                sub_nodes,
                key=lambda x: (
                    x.declaration_index.value if hasattr(x, "declaration_index") and x.declaration_index else 0
                ),
            )

            for recipe_ingredient in sub_nodes:
                if isinstance(recipe_ingredient, FoodProductFlowNode) and not (
                    isinstance(recipe_ingredient.product_name, NamesProp)
                    and any(
                        term.term_uid in self.gfm_factory.non_food_term_uids
                        for term in recipe_ingredient.product_name.terms
                    )
                ):
                    # this tuple informs the optimiser about the hierarchy of the ingredients, e.g.:
                    # "ingredients_declaration": 'Main0 (Sub00, Sub01), Main1 (Sub10, Sub11)',
                    # (0,)    is for  Main0
                    # (0, 0)  is for  Sub00  (the last 0 here is given by `this_level_index`)
                    # (0, 1)  is for  Sub01
                    # (1,)    is for  Main1
                    # (1, 0)  is for  Sub10
                    # (1, 1)  is for  Sub11
                    level_tuple = (*parent_level, this_level_index)

                    # nutrient_values are attached by MatchProductNameGFM
                    nutrients = recipe_ingredient.nutrient_values

                    try:
                        nutrient_xid = nutrients.get_prop_term().xid
                    except AttributeError:
                        nutrient_xid = None

                    if recipe_ingredient.is_subdivision:
                        if recipe_ingredient.nutrient_upscale_ratio:
                            # No direct ingredient amount estimation is necessary for nutrient scale dummy node.
                            continue

                        is_subdivision = True
                    else:
                        is_subdivision = False

                    # we have to pass a key to the optimiser, and it gives it back to us, so we can match results
                    # we need to pass the child's uid (not grandchild uid), because we update amounts on child nodes
                    key: IngredientKey = (level_tuple, "_", recipe_ingredient.uid, is_subdivision, nutrient_xid)
                    # 2nd element is namespace, not used
                    # 4th element is important: If it is True, it will not apply the order constraint!
                    # 5th element is ID of nutrient file used

                    if nutrients is not None and isinstance(nutrients, QuantityPackageProp) and nutrients.quantities:
                        flat_nutrients[key] = nutrients.amount_for_100g()
                    else:
                        # we don't have nutrient values for this ingredient, but need to pass it to the optimiser
                        flat_nutrients[key] = None

                        # having no nutrients is a problem, if this is a leaf node (none or only brightway sub-nodes)
                        if not recipe_ingredient.get_sub_nodes() or all(
                            [
                                isinstance(sub_node, ModeledActivityNode)
                                for sub_node in recipe_ingredient.get_sub_nodes()
                            ]
                        ):
                            missing_leaf_uids.add(recipe_ingredient.uid)

                    sub_nodes = recipe_ingredient.get_sub_nodes()

                    if len(sub_nodes) > 0 and isinstance(sub_nodes[0], FoodProcessingActivityNode):
                        # skipping estimation of ingredients with converted amount
                        sub_sub_nodes = sub_nodes[0].get_sub_nodes()
                        has_converted_amount = [node.amount is not None for node in sub_sub_nodes]

                        if not all(has_converted_amount):
                            # if there is at least 1 product with unknown amount,
                            # sub_product nutrients should be used instead
                            flat_nutrients[key] = None

                    # Convert raw percentage float into amount property.
                    percentage_value = None
                    add_original_amount_source = False
                    if original_amount := recipe_ingredient.amount_in_original_source_unit:
                        percentage_term = self.gfm_factory.percentage_term
                        if isinstance(original_amount, (DeepMappingView, dict)) and (
                            original_amount.get("unit", "").lower() == percentage_term.name.lower()
                            or original_amount.get("unit", "").lower() in percentage_term.data.get("alias")
                        ):
                            percentage_value = original_amount.get("value")
                            add_original_amount_source = True

                    if percentage_value:
                        # convert percentage to QuantityProp if it is a float
                        calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=recipe_ingredient.uid,
                                prop_name="amount_in_original_source_unit",
                                prop=QuantityProp(
                                    value=percentage_value,
                                    unit_term_uid=self.gfm_factory.percentage_term.uid,
                                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                                    source_data_raw=(
                                        recipe_ingredient.amount_in_original_source_unit
                                        if add_original_amount_source
                                        else None
                                    ),
                                ),
                            )
                        )

                    if original_amount := recipe_ingredient.amount_in_original_source_unit:
                        if (
                            isinstance(original_amount, QuantityProp)
                            and original_amount.get_unit_term().uid == self.gfm_factory.percentage_term.uid
                        ):
                            # percentage provided as a number btw 0 and 100, but optimiser expects a probability
                            fixed_percentages[key] = original_amount.value / 100

                    original_amount = recipe_ingredient.amount
                    if fixed_percentages.get(key) is None and original_amount:
                        parent_product_amount = recipe_ingredient.get_parent_nodes()[0].production_amount
                        parent_product_amount_in_g = UnitWeightConverter.convert_between_same_unit_types(
                            parent_product_amount.value,
                            parent_product_amount.get_unit_term(),
                            self.gfm_factory.gram_term,
                            "mass-in-g",
                        )
                        original_amount_in_g = UnitWeightConverter.convert_between_same_unit_types(
                            original_amount.value,
                            original_amount.get_unit_term(),
                            self.gfm_factory.gram_term,
                            "mass-in-g",
                        )
                        fixed_percentages[key] = original_amount_in_g / parent_product_amount_in_g

                    this_level_index += 1

                    for sub_node in recipe_ingredient.get_sub_nodes():
                        if isinstance(sub_node, FoodProcessingActivityNode):
                            await visit_sub_ingredients(sub_node, level_tuple)  # recurse

        await visit_sub_ingredients(self.node, ())

        if len(flat_nutrients) == 0:
            logger.debug("cannot run ingredient amount estimation, because there are no ingredients with nutrients")
            return

        if missing_leaf_uids:
            # return if any of the leaf ingredients have no matching
            if any(
                error.error_classification == ErrorClassification.missing_matching
                and error.node_uid in missing_leaf_uids
                for error in calc_graph.data_errors_log
            ):
                logger.info(
                    f"Skipping ingredient amount estimation for {self.node} due to "
                    "missing matchings in any of the child nodes."
                )
                return
            # Don't have to raise for missing leaf if the len(flat_nutrients) == len(fixed_percentages) and
            # all percentages are defined.:
            elif len(flat_nutrients) == len(fixed_percentages) and len(missing_leaf_uids) == len(flat_nutrients):
                if set(flat_nutrients.keys()) == set(fixed_percentages.keys()):
                    self.handle_fixed_percentages(flat_nutrients, fixed_percentages, calc_graph)
                    return

            raise ValueError(
                "No nutrients found for "
                f"ingredients {[calc_graph.get_node_by_uid(elem) for elem in missing_leaf_uids]}!"
                " Ask Science team to provide this data."
            )

        # read the nutrient values from this recipe or product; again: no Views object here, else fails in optimiser
        # we have already checked for the presence of nutrient_values in should_be_scheduled()
        incoming_nutrients_prop = None
        for parent_node in self.node.get_parent_nodes():
            parent_nutrient_values = parent_node.nutrient_values
            if parent_nutrient_values and isinstance(parent_nutrient_values, (DeepMappingView, dict)):
                incoming_nutrients_prop = nutrients_dict_to_prop(parent_nutrient_values)
                incoming_nutrients_prop.source_data_raw = parent_nutrient_values
                incoming_nutrients_prop.source = QuantityPackageSourceEnum.api

                calc_graph.apply_mutation(
                    PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=parent_node.uid,
                        prop_name="nutrient_values",
                        prop=incoming_nutrients_prop.duplicate(),
                    )
                )
                break
            elif parent_nutrient_values and parent_nutrient_values.quantities:
                incoming_nutrients_prop = parent_nutrient_values.duplicate()
                break

        if (
            not incoming_nutrients_prop
            or incoming_nutrients_prop.source != QuantityPackageSourceEnum.api
            or len([val for val in incoming_nutrients_prop.quantities.values() if val is not None and val.value > 0])
            == 0
        ):
            if len(flat_nutrients) == len(fixed_percentages) and set(flat_nutrients.keys()) == set(
                fixed_percentages.keys()
            ):
                self.handle_fixed_percentages(flat_nutrients, fixed_percentages, calc_graph)
                return
            logger.debug(f"No nutrients available in {self.node}")
            return

        all_level_tuples = {level_tuple for (level_tuple, _, _, _, _) in flat_nutrients.keys()}
        keys_to_delete = []
        for key, nutrient_amounts in flat_nutrients.items():
            (level_tuple, _, _uid, _, _) = key
            if (level_tuple + (0,)) in all_level_tuples:
                if nutrient_amounts is not None:
                    keys_to_delete.append(key)
            elif nutrient_amounts is None:
                logger.warn("nutrient_amounts are None for leaf ingredient --> try fallback without estimation...")
                try:
                    await self.try_fallback_without_estimation(calc_graph, fixed_percentages, flat_nutrients)
                except (KeyError, AttributeError, ValueError):
                    logger.warn(f"Missing amounts, but also missing nutrients for leaf ingredients in {self.node}.")
                    calc_graph.append_data_errors_log_entry(
                        f"Could not solve ingredient amount estimation for node {self.node}.",
                        error_classification=ErrorClassification.failed_amount_estimation,
                    )
                return

        for key in keys_to_delete:
            logger.debug(f"Deleting intermediate ingredient nutrient values for {key}.")
            flat_nutrients[key] = None

        # run estimation algorithm
        try:
            solution, error_squares, flat_nutrients_converted, solution_status = run(
                incoming_nutrients_prop, flat_nutrients, fixed_percentages
            )  # incoming_nutrients must be duplicated since otherwise, the property is modified directly.
        except ValueError:
            try:
                await self.try_fallback_without_estimation(calc_graph, fixed_percentages, flat_nutrients)
            except (KeyError, AttributeError, ValueError):
                logger.warn(f"Could not solve ingredient amount estimation for node {self.node}.")
                calc_graph.append_data_errors_log_entry(
                    f"Could not solve ingredient amount estimation for node {self.node}.",
                    error_classification=ErrorClassification.failed_amount_estimation,
                )
            return

        solution_uids = [uid for (_, _, uid, _, _), ingredient_amount in solution.items()]
        if len(set(solution_uids)) != len(solution_uids):
            raise ValueError("a node uid appears multiple times in the recipe. This should not happen")

        # write the estimated amount percentages to the ingredients
        estimated_nutrients = {}

        weight_stack = [1.0]
        for ingredient_key, ingredient_amount in solution.items():
            if ingredient_amount <= 0:
                if ingredient_amount < -1e-5:
                    # if the amount is negative but larger than -1e-5, we raise an error
                    # The value of this threshold is arbitrary and up for discussion.
                    logger.warn(f"Negative ingredient amount: {ingredient_amount}")
                    raise ValueError(f"Negative ingredient amount: {ingredient_amount}")
                else:
                    # if the amount is negative but smaller than -1e-5, we set it to 1E-10
                    # This is to avoid negative flow amounts, which lead to negative co2-values,
                    # nutrient-values etc.
                    ingredient_amount = 1e-10
            # The ingredient_amount is in percentage of the product that is estimated.
            ingredient_amount_fraction_of_root_amount = ingredient_amount / 100

            ingredient_nutrients = flat_nutrients_converted[ingredient_key]
            if ingredient_nutrients is not None:
                for nutrient, val in ingredient_nutrients.quantities.items():
                    if nutrient not in estimated_nutrients:
                        estimated_nutrients[nutrient] = ReferencelessQuantityProp(
                            value=0.0, unit_term_uid=val.unit_term_uid
                        )
                    estimated_nutrients[nutrient].value += val.value * ingredient_amount / 100

            (level_tuple, _, uid, _, _) = ingredient_key

            # weight_stack should always have 1 element more than len(level_tuple)
            if len(level_tuple) >= len(weight_stack):
                # we are going deeper into the ingredient tree
                weight_stack.append(ingredient_amount_fraction_of_root_amount)
            elif len(level_tuple) < len(weight_stack) - 1:
                # we are going up in the ingredient tree
                weight_stack = weight_stack[: len(level_tuple)]
                weight_stack.append(ingredient_amount_fraction_of_root_amount)
            else:
                # we are going to the next sibling
                weight_stack[-1] = ingredient_amount_fraction_of_root_amount

            # normalize with the parent weight, such that this amount describes fraction of the parent node
            ingredient_amount_fraction_of_parent_amount = ingredient_amount_fraction_of_root_amount / weight_stack[-2]

            # compute the parent-production_amount in kg
            ingredient_node = calc_graph.get_node_by_uid(uid)
            parent_product_amount = ingredient_node.get_parent_nodes()[0].production_amount
            parent_production_amount_in_kg = UnitWeightConverter.convert_between_same_unit_types(
                parent_product_amount.value,
                parent_product_amount.get_unit_term(),
                self.gfm_factory.kilogram_term,
                "mass-in-g",
            )

            # normalize with the parent weight, such that this amount describes amount per unit of the parent node:
            ingredient_kg_amount_per_parent_unit = (
                ingredient_amount_fraction_of_parent_amount * parent_production_amount_in_kg
            )

            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=uid,
                    prop_name="amount",
                    prop=QuantityProp(
                        value=ingredient_kg_amount_per_parent_unit,
                        unit_term_uid=self.gfm_factory.kilogram_term.uid,
                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    ),  # in kg
                )
            )

        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="ingredient_amount_estimator_solution_status",
                prop=IngredientAmountEstimatorStatusProp(value=solution_status),
            )
        )

        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="ingredient_amount_estimator_error_squares",
                prop=QuantityPackageProp(quantities=error_squares, for_reference=ReferenceAmountEnum.amount_for_100g),
            )
        )

        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="ingredient_amount_estimator_estimated_nutrients",
                prop=QuantityPackageProp(
                    quantities=estimated_nutrients, for_reference=ReferenceAmountEnum.amount_for_100g
                ),
            )
        )

    async def try_fallback_without_estimation(
        self,
        calc_graph: CalcGraph,
        fixed_percentages: dict[tuple, Optional[float]],
        flat_nutrients: dict[tuple, QuantityPackageProp],
    ) -> None:
        non_subdivision_nodes_without_fixed_percentages_by_lvl = {}
        fixed_percentage_sum_by_lvl = {}
        for key in flat_nutrients:
            (level_tuple, _, _, is_subdivision, _) = key
            if is_subdivision is False:
                if key in fixed_percentages:
                    if len(level_tuple) in fixed_percentage_sum_by_lvl:
                        fixed_percentage_sum_by_lvl[len(level_tuple)] += fixed_percentages[key]
                    else:
                        fixed_percentage_sum_by_lvl[len(level_tuple)] = fixed_percentages[key]

                else:
                    if len(level_tuple) in non_subdivision_nodes_without_fixed_percentages_by_lvl:
                        non_subdivision_nodes_without_fixed_percentages_by_lvl[len(level_tuple)].append(key)
                    else:
                        non_subdivision_nodes_without_fixed_percentages_by_lvl[len(level_tuple)] = [key]
        for len_lvl_tuple, non_subdivision_nodes in non_subdivision_nodes_without_fixed_percentages_by_lvl.items():
            if len(non_subdivision_nodes) == 1:
                fixed_percentages[non_subdivision_nodes[0]] = 1.0 - fixed_percentage_sum_by_lvl[len_lvl_tuple]
        ings_without_fixed_percentages = {key for key in flat_nutrients if key not in fixed_percentages}

        # Check if all ingredients without fixed percentages are subdivisions:
        if not all(is_subdivision is True for (_, _, _, is_subdivision, _) in ings_without_fixed_percentages):
            raise ValueError("Ingredients without fixed amounts")

        self.handle_fixed_percentages(flat_nutrients, fixed_percentages, calc_graph)
        for _, _, uid, _, _ in ings_without_fixed_percentages:
            subdivision_node = calc_graph.get_node_by_uid(uid)
            parent_of_subdivision = subdivision_node.get_parent_nodes()[0]
            parent_production_amount = parent_of_subdivision.production_amount

            if subdivision_node.is_dried():
                calc_graph.apply_mutation(
                    PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=uid,
                        prop_name="amount",
                        prop=QuantityProp.unvalidated_construct(
                            value=0.0,
                            unit_term_uid=parent_production_amount.unit_term_uid,
                            for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                        ),
                    )
                )
            else:
                calc_graph.apply_mutation(
                    PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=uid,
                        prop_name="amount",
                        prop=QuantityProp.unvalidated_construct(
                            value=parent_production_amount.value,
                            unit_term_uid=parent_production_amount.unit_term_uid,
                            for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                        ),
                    )
                )
            logger.warn(
                f"Could not solve ingredient amount estimation for node {self.node},"
                " but percentages are complete."
                " Assuming that the dry nutrition subdivision has zero amount."
            )
            calc_graph.append_data_errors_log_entry(
                f"Assumed that dry nutrition subdivision has zero amount for subnodes of node {self.node}.",
                error_classification=ErrorClassification.fallback_without_amount_estimation,
            )


class IngredientAmountEstimatorGapFillingFactory(AbstractGapFillingFactory):  # noqa: D101
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", self.root_unit_term.access_group_uid)
        ]
        self.kilogram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilogram", self.root_unit_term.access_group_uid)
        ]
        self.percentage_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_percentage", self.root_unit_term.access_group_uid)
        ]
        self.non_food_term_uids: frozenset[UUID] = frozenset()

    async def init_cache(self) -> None:  # noqa: D102
        root_access_group_uid = self.service_provider.glossary_service.root_term.access_group_uid
        foodex2_access_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )
        self.non_food_term_uids = frozenset(
            [
                self.service_provider.glossary_service.terms_by_xid_ag_uid[("EAT-0002", foodex2_access_group_uid)].uid,
                self.service_provider.glossary_service.terms_by_xid_ag_uid[
                    ("EOS_data_error", root_access_group_uid)
                ].uid,
            ]
        )

    def spawn_worker(self, node: Node) -> IngredientAmountEstimatorGapFillingWorker:  # noqa: D102
        return IngredientAmountEstimatorGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = IngredientAmountEstimatorGapFillingFactory
