"""Nutrient subdivision gap filling module."""

import uuid
from typing import Optional

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingWorker
from gap_filling_modules.ingredicalc.helpers import get_water_gram_per_100g, transform_eurofir_to_qty_pkg
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from structlog import get_logger

from core.domain.glossary_link import GlossaryLink
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.node import Node
from core.domain.props import GlossaryTermProp, NamesProp, QuantityPackageProp, QuantityProp, ReferencelessQuantityProp
from core.domain.props.glossary_term_prop import SourceEnum
from core.domain.props.names_prop import RawName
from core.domain.props.quantity_package_prop import QuantityPackageSourceEnum
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.utils import has_one_of_predefined_tags
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.glossary_link_service import GlossaryLinkService
from core.service.glossary_service import GlossaryService
from core.service.service_provider import ServiceProvider
from database.postgres.pg_glossary_link_mgr import PgGlossaryLinkMgr
from database.postgres.postgres_db import PostgresDb

logger = get_logger()
DEFAULT_MAX_EVAPORATION = 90.0


async def get_set_of_nutrients_coming_from_dried_tagged(
    pg_glossary_link_mgr: PgGlossaryLinkMgr, dried_term: Term
) -> set:
    # We need to know whether the nutrients are coming from a list of
    # terms containing the dried term
    nutrient_glossary_links: list[GlossaryLink] = await pg_glossary_link_mgr.get_data_of_gfm("Nutrients")
    linked_nutrient_term_for_dried_tagged: set[uuid.UUID] = set()

    assert len(nutrient_glossary_links) > 0

    for glossary_link in nutrient_glossary_links:
        if dried_term.uid in glossary_link.term_uids:
            linked_nutrient_term_for_dried_tagged.add(glossary_link.linked_term_uid)

    return linked_nutrient_term_for_dried_tagged


class NutrientSubdivisionGapFillingWorker(AbstractGapFillingWorker):
    """Subdivides an ingredient into variants (e.g. cocoa powder -> low-fat cocoa powder and high-fat cocoa powder)."""

    def __init__(self, node: Node, gfm_factory: "NutrientSubdivisionGapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        # we do not subdivide if there is already a sub-node, because this is just an intermediate node of a recipe:
        if len(self.node.get_sub_nodes()) > 0:
            return False

        if not (isinstance(self.node, FoodProductFlowNode) and self.node.product_name):
            logger.debug("[NutrientSubdivision] node is not a food product --> not scheduled.")
            return False

        # we only need the subdivision if we run the IngredientAmountEstimator for one of the nodes above in the tree.
        # The following checks are to determine if IngredientAmountEstimator will be scheduled for a parent node.
        parent_activity_node = self.node.get_parent_nodes()
        if len(parent_activity_node) == 0:
            logger.debug("[NutrientSubdivision] node is root-flow --> not scheduled.")
            return False

        logger.debug("[NutrientSubdivision] --> scheduled.")
        return True

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        # if in the meantime sub-nodes were added, we do not mutate anything, because this is just an intermediate node
        # now of the recipe:
        if len(self.node.get_sub_nodes()) > 0:
            return GapFillingWorkerStatusEnum.cancel

        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(MatchProductNameGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            # we want to subdivide *after* matching product names; else there will be no terms attached to the
            # ingredients to be subdivided.
            logger.debug("[NutrientSubdivision] waiting for MatchProductName GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        elif global_gfm_state.get(AttachFoodTagsGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            # we want to subdivide *after* attach food tags; else there will be no terms attached to the
            # ingredients to be subdivided.
            logger.debug("[NutrientSubdivision] waiting for AttachFoodTagsGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        else:
            logger.debug("[NutrientSubdivision] --> ready.")
            return GapFillingWorkerStatusEnum.ready

    def is_fresh_in_tags(self, tag_term_xids: frozenset[str]) -> bool:
        """Whether the fresh term is in the set of tags for the food product."""
        return self.gfm_factory.fresh_term.xid in tag_term_xids

    def add_default_max_evaporation(self, calc_graph: CalcGraph, tag_term_xids: frozenset[str]) -> None:
        if self.is_fresh_in_tags(tag_term_xids):
            max_evaporation_value = 5.0
        else:
            max_evaporation_value = DEFAULT_MAX_EVAPORATION

        prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=self.node.uid,
            prop_name="default_max_evaporation",
            prop=QuantityProp(
                value=max_evaporation_value,
                unit_term_uid=self.gfm_factory.gram_term.uid,
                # set reference as amount for 100g such that the default max evaporation value is in percentage.
                for_reference=ReferenceAmountEnum.amount_for_100g,
            ),
        )
        calc_graph.apply_mutation(prop_mutation)

    async def run(self, calc_graph: CalcGraph) -> None:
        logger.debug("start running gap-filling-module NutrientSubdivision...")

        def has_parent_scheduled_ingredient_amount_estimator(current_node: Node) -> bool:
            for parent_activity_node in current_node.get_parent_nodes():
                if not isinstance(parent_activity_node, FoodProcessingActivityNode):
                    continue
                if parent_activity_node.gfm_state and (
                    parent_activity_node.gfm_state.worker_states.get("IngredientAmountEstimatorGapFillingWorker")
                    == NodeGfmStateEnum.scheduled
                ):
                    return True
                if parent_activity_node.get_parent_nodes():
                    if has_parent_scheduled_ingredient_amount_estimator(parent_activity_node.get_parent_nodes()[0]):
                        return True
            return False

        parent_ingredient_amount_estimator_scheduled = has_parent_scheduled_ingredient_amount_estimator(self.node)

        # to check if the node needs to have a dried subdivision we just check it self is marked as dried
        # or if any of the parents is set to dried. The later is in some cases an inaccurate modeling of
        # reality and we might want to improve this in the future.
        (
            dried_term,
            _,
        ) = self.node.get_prop_by_inheritance(
            "tag_term_xids",
            ignore_if=(
                lambda prop_name, tag_term_xids_to_search=frozenset(
                    {self.gfm_factory.dried_state_term.xid}
                ): not has_one_of_predefined_tags(prop_name, tag_term_xids_to_search)
            ),
        )
        node_is_dried = dried_term is not None

        if not parent_ingredient_amount_estimator_scheduled and not node_is_dried:
            # If node is dried, we still need to run nutrient subdivision GFM to correctly
            # create a dried upscaling node.
            logger.debug(
                "[NutrientSubdivision] no parent node has nutrient_values. Therefore IngredientAmountEstimator"
                "will not be scheduled and we don't need a NutrientSubdivision --> skip."
            )
            return

        # if in the meantime sub-nodes were added, we do not mutate anything, because this is just an intermediate node
        # now of the recipe:
        if len(self.node.get_sub_nodes()) > 0:
            return

        if isinstance(self.node, FoodProductFlowNode):
            tag_term_xids = self.node.tag_term_xids
            self.add_default_max_evaporation(calc_graph, tag_term_xids)

        # 1. check if we have a term attached to the node
        # 2. prevent recursion (if this node is already a subdivision, don't subdivide it again)
        product_name = self.node.product_name
        successful_subdivision = False

        if (product_name and isinstance(product_name, NamesProp)) and not self.node.is_subdivision:
            all_product_terms: list[Term] = [product_name_term.get_term() for product_name_term in product_name.terms]

            if all(all_product_terms):
                # TODO: use glossary links!! using '+' sign here as a placeholder for multiple terms for now
                #  until linking from multiple terms is implemented
                # We check for subdivisions. These are stored in the Glossary in a specific namespace, where the term
                # xid is the uuid of the term to subdivide.
                manual_subdivision: Optional[
                    Term
                ] = await self.gfm_factory.glossary_service.get_term_by_xid_and_access_group_uid(
                    "+".join([str(term.uid) for term in all_product_terms]),
                    str(self.gfm_factory.subdivision_access_group_uid),
                )

                if manual_subdivision and parent_ingredient_amount_estimator_scheduled:
                    # Manual subdivision is only valid if ingredient amount estimation is running, which is the case
                    # if parent_ingredient_amount_estimator_scheduled = True.
                    logger.debug("found manual subdivision", terms=all_product_terms, subdivision=manual_subdivision)

                    parent_uid = self.prepare_parent_uid(calc_graph)

                    max_evaporation = manual_subdivision.data.get("max_evaporation")

                    # in case when we have only `max_evaporation` defined,
                    # we substitute the original node term for a subdivision term
                    if not manual_subdivision.data.get("subproduct_terms_uuids") and max_evaporation is not None:
                        manual_subdivision.data["subproduct_terms_uuids"] = [
                            [str(term.uid) for term in all_product_terms],
                        ]

                    # retrieve the subdivided terms
                    for sub_uuids in manual_subdivision.data["subproduct_terms_uuids"]:
                        subdivision_terms = [
                            self.gfm_factory.glossary_service.get_term_by_id(uuid.UUID(sub_uuid))
                            for sub_uuid in sub_uuids
                        ]

                        nutrient_term_uid = self.gfm_factory.linked_nutrient_terms.get(
                            frozenset([term.uid for term in subdivision_terms])
                        )
                        if nutrient_term_uid:
                            nutrition_term = await self.gfm_factory.service_provider.glossary_service.get_term_by_uid(
                                nutrient_term_uid
                            )
                        else:
                            nutrition_term = None

                        if not nutrition_term:
                            raise ValueError(
                                f"No nutrition term found in subdivision term set {subdivision_terms}! "
                                f"Ask Science team to provide one."
                            )

                        food_product_node = await self.create_subdivision_node(
                            calc_graph,
                            parent_uid,
                            subdivision_terms,
                        )
                        successful_subdivision = True

                        logger.debug(
                            "create PropMutation to add nutrient_values",
                            node=food_product_node,
                            nutrition_term=nutrition_term,
                        )

                        nutrition_pkg = transform_eurofir_to_qty_pkg(
                            nutrition_term.data["nutr-vals"], data_source=nutrition_term.xid
                        )
                        nutrition_pkg.prop_term_uid = nutrition_term.uid
                        nutrition_pkg.source = QuantityPackageSourceEnum.database

                        calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=food_product_node.uid,
                                prop_name="nutrient_values",
                                prop=nutrition_pkg,
                            )
                        )

                        if self.is_possible_to_create_dried_product_node(
                            nutrition_pkg, max_evaporation=max_evaporation
                        ):
                            await self.create_dried_product_node(
                                calc_graph,
                                nutrition_pkg,
                                parent_uid,
                                subdivision_terms,
                                max_evaporation=max_evaporation,
                            )
                else:
                    logger.debug("using automatic subdivision", terms=all_product_terms)
                    nutrient_values = self.node.nutrient_values
                    if nutrient_values is not None and isinstance(nutrient_values, QuantityPackageProp):
                        nutrient_term = self.node.nutrient_values.get_prop_term()
                    else:
                        nutrient_term = None

                    if nutrient_values is None:
                        logger.info(f"No nutrition data found in {self.node}. Ask Science team to provide it.")

                        calc_graph.append_data_errors_log_entry(
                            f"No nutrition data found in {self.node}. Ask Science team to provide it."
                        )
                        return
                    elif nutrient_term is None:
                        logger.info(
                            f"Nutrient comes from client data for {self.node}. Not invoking nutrient subdivision."
                        )
                        return

                    if node_is_dried:
                        nutrient_term_from_dried = (
                            nutrient_term.uid in self.gfm_factory.linked_nutrient_term_for_dried_tagged
                        )

                        if nutrient_term_from_dried:
                            if (
                                isinstance(self.node.product_name, NamesProp)
                                and self.gfm_factory.dried_state_term not in all_product_terms
                            ):
                                product_name_terms = list(self.node.product_name.terms)
                                product_name_terms.append(
                                    GlossaryTermProp.unvalidated_construct(
                                        term_uid=self.gfm_factory.dried_state_term.uid
                                    )
                                )

                                # Add dried term if it is not there to the product_name so that it is correctly matched
                                # to the dried modeled activity.
                                prop_mutation = PropMutation(
                                    created_by_module=self.__class__.__name__,
                                    node_uid=self.node.uid,
                                    prop_name="product_name",
                                    prop=NamesProp.unvalidated_construct(
                                        terms=product_name_terms,
                                        source_data_raw=self.node.product_name.source_data_raw,
                                    ),
                                )
                                calc_graph.apply_mutation(prop_mutation)

                            logger.info(f"Skipping nutrient subdivision for {self.node} because it is already dried.")
                            return
                        else:
                            if self.gfm_factory.dried_state_term in all_product_terms:
                                product_name_terms_without_dried = [
                                    t
                                    for t in self.node.product_name.terms
                                    if t.term_uid != self.gfm_factory.dried_state_term.uid
                                ]

                                # Remove dried term if it is in product_name so that the node is correctly linked to
                                # to the non-dried modeled activity.
                                prop_mutation = PropMutation(
                                    created_by_module=self.__class__.__name__,
                                    node_uid=self.node.uid,
                                    prop_name="product_name",
                                    prop=NamesProp.unvalidated_construct(
                                        terms=product_name_terms_without_dried,
                                        source_data_raw=self.node.product_name.source_data_raw,
                                    ),
                                )
                                calc_graph.apply_mutation(prop_mutation)

                                all_product_terms: list[Term] = [
                                    product_name_term.get_term() for product_name_term in self.node.product_name.terms
                                ]

                            if parent_ingredient_amount_estimator_scheduled:
                                if self.is_possible_to_create_dried_product_node(
                                    nutrient_values,
                                    max_evaporation=self.calculate_percent_water_evaporated_for_percent_dry_weight(
                                        nutrient_values, 80
                                    )
                                    / 100,
                                ) or self.is_possible_to_create_dried_product_node(
                                    nutrient_values,
                                    max_evaporation=self.calculate_percent_water_evaporated_for_percent_dry_weight(
                                        nutrient_values, 100
                                    )
                                    / 100,
                                ):
                                    # If possible, divide into 80% and 100% dried nodes to let
                                    # ingredient amount estimator determine what the exact evaporation percentage is.
                                    # If not, just take the already dried parent node.
                                    parent_uid = self.prepare_parent_uid(calc_graph)
                                    await self.create_dried_product_node(
                                        calc_graph,
                                        nutrient_values,
                                        parent_uid,
                                        all_product_terms,
                                        max_evaporation=self.calculate_percent_water_evaporated_for_percent_dry_weight(
                                            nutrient_values, 80
                                        )
                                        / 100,
                                    )
                                    await self.create_dried_product_node(
                                        calc_graph,
                                        nutrient_values,
                                        parent_uid,
                                        all_product_terms,
                                        max_evaporation=self.calculate_percent_water_evaporated_for_percent_dry_weight(
                                            nutrient_values, 100
                                        )
                                        / 100,
                                    )
                                    successful_subdivision = True

                            elif manual_subdivision and (manual_subdivision.data.get("max_evaporation") is not None):
                                # If parent_ingredient_amount_estimator_scheduled = False
                                # and if max_evaporation is defined in manual_subdivision.data,
                                # ingredient amount estimation cannot be performed. Therefore,
                                # dry the ingredient to max_evaporation defined in manual_subdivision.data.
                                if self.is_possible_to_create_dried_product_node(
                                    nutrient_values,
                                    max_evaporation=manual_subdivision.data.get("max_evaporation"),
                                ):
                                    await self.create_dried_product_node(
                                        calc_graph,
                                        nutrient_values,
                                        self.node.uid,
                                        all_product_terms,
                                        add_extra_flow_with_unknown_amount=False,
                                        max_evaporation=manual_subdivision.data.get("max_evaporation"),
                                    )
                            else:
                                # If parent_ingredient_amount_estimator_scheduled = False,
                                # ingredient amount estimation cannot be performed.
                                # We therefore assume 90% evaporation of the node.
                                if self.is_possible_to_create_dried_product_node(
                                    nutrient_values,
                                    max_evaporation=self.calculate_percent_water_evaporated_for_percent_dry_weight(
                                        nutrient_values, 90
                                    )
                                    / 100,
                                ):
                                    await self.create_dried_product_node(
                                        calc_graph,
                                        nutrient_values,
                                        self.node.uid,
                                        all_product_terms,
                                        add_extra_flow_with_unknown_amount=False,
                                        max_evaporation=self.calculate_percent_water_evaporated_for_percent_dry_weight(
                                            nutrient_values, 90
                                        )
                                        / 100,
                                    )

                    elif parent_ingredient_amount_estimator_scheduled:
                        # Subdivision is only necessary for ingredient estimation, which is only triggered when
                        # parent_ingredient_amount_estimator_scheduled.
                        parent_uid = self.prepare_parent_uid(calc_graph)
                        food_product_node = await self.create_subdivision_node(
                            calc_graph,
                            parent_uid,
                            all_product_terms,
                        )
                        successful_subdivision = True

                        logger.debug("create PropMutation to add nutrient_values", node=food_product_node)

                        calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=food_product_node.uid,
                                prop_name="nutrient_values",
                                prop=self.node.nutrient_values.duplicate(),
                            )
                        )

                        if self.is_possible_to_create_dried_product_node(nutrient_values):
                            await self.create_dried_product_node(
                                calc_graph,
                                nutrient_values,
                                parent_uid,
                                all_product_terms,
                            )

        if successful_subdivision:
            # If subdivision was successful, the original nutrient_values (on the non-subdivided node)
            # are no longer valid:
            if isinstance(self.node, ActivityNode) and len(self.node.get_parent_nodes()) > 0:
                flow_node_uid = self.node.get_parent_nodes()[0].uid
                if isinstance(self.node.get_parent_nodes()[0].nutrient_values, QuantityPackageProp):
                    source_data_raw = self.node.get_parent_nodes()[0].nutrient_values.source_data_raw
                else:
                    source_data_raw = self.node.get_parent_nodes()[0].nutrient_values
            else:
                flow_node_uid = self.node.uid
                if isinstance(self.node.nutrient_values, QuantityPackageProp):
                    source_data_raw = self.node.nutrient_values.source_data_raw
                else:
                    source_data_raw = self.node.nutrient_values

            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=flow_node_uid,
                    prop_name="nutrient_values",
                    prop=QuantityPackageProp(
                        quantities={},
                        for_reference=ReferenceAmountEnum.amount_for_100g,
                        source_data_raw=source_data_raw,
                    ),
                )
            )

    async def create_subdivision_node(
        self,
        calc_graph: CalcGraph,
        parent_uid: uuid.UUID,
        parent_terms: list[Term],
        is_dried: bool = False,
    ) -> Node:
        # now create subdivision nodes; the process below is similar to the one in
        # IngredientSplitterGapFillingWorker.create_nodes_for_ingredients()

        # create a new flow node for each subdivision
        raw_names = (
            self.node.product_name.source_data_raw
            if isinstance(self.node.product_name, NamesProp)
            else self.node.product_name
        )

        original_node_name = raw_names[0]["value"]
        subdivision_term_name = original_node_name + " (dried)" if is_dried else original_node_name

        food_product_node = FoodProductFlowNode(
            uid=uuid.uuid4(),
            product_name=NamesProp(
                terms=[GlossaryTermProp(term_uid=parent_term.uid) for parent_term in parent_terms],
                source_data_raw=[RawName(language="de", value=subdivision_term_name)],
            ),  # TODO don't hardcode language
            # We have to add labels and origin to the newly created subdivision nodes for water scarcity and rainforest
            # calculations to work properly.
            # TODO: Decide whether more properties have to be copied to the subdivision food product flow nodes.
            raw_labels=self.node.raw_labels,
            is_subdivision=True,  # set "is_subdivision" flag on the subdivision node
        )

        # Add dried glossary term to the list of tags if this subdivision node is a dried one
        if is_dried:
            food_product_node.glossary_tags = [
                GlossaryTermProp(term_uid=self.gfm_factory.dried_state_term.uid, source=SourceEnum.eos_assumed)
            ]

        calc_graph.apply_mutation(
            AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=parent_uid,
                new_node=food_product_node,
                copy=True,
            )
        )

        return food_product_node

    def prepare_parent_uid(self, calc_graph: CalcGraph) -> uuid.UUID:
        parent_uid = self.node.uid

        # create an activity node, if necessary
        if not isinstance(self.node, ActivityNode):
            activity_node = FoodProcessingActivityNode(
                uid=uuid.uuid4(),
            )
            calc_graph.apply_mutation(
                AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=self.node.uid,
                    new_node=activity_node,
                    copy=False,
                )
            )
            parent_uid = activity_node.uid

        return parent_uid

    def is_possible_to_create_dried_product_node(
        self, nutrition_data: QuantityPackageProp, max_evaporation: float = None
    ) -> bool:
        # creating a node that contains nutrition info
        # about the dried version of this product
        # if defined evaporation does not equal 0
        # or water component is > 1% of total nutrients mass
        return self.check_for_water_component(nutrition_data) and max_evaporation != 0

    async def create_dried_product_node(
        self,
        calc_graph: CalcGraph,
        nutrition_data: QuantityPackageProp,
        parent_uid: uuid.UUID,
        parent_terms: list[Term],
        add_extra_flow_with_unknown_amount: bool = True,
        max_evaporation: float = None,
    ) -> None:
        # 1) adding an ingredient node that will store an amount value of dried ingredient
        if add_extra_flow_with_unknown_amount:
            dried_ingredient_node = await self.create_subdivision_node(
                calc_graph,
                parent_uid,
                parent_terms,
                is_dried=True,
            )
        else:
            dried_ingredient_node = self.node

        dried_product_nutrients, upscale_ratio = self.calculate_dried_product_nutrients(
            nutrition_data,
            max_evaporation,
        )

        # keeping this property for debug purposes,
        # although it is unused by ingredient amount estimator GFM
        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=dried_ingredient_node.uid,
                prop_name="nutrient_values",
                prop=dried_product_nutrients,
            )
        )

        # 2) adding an intermediate activity node

        intermediate_activity_node = FoodProcessingActivityNode(
            uid=uuid.uuid4(),
        )

        logger.debug(f"create recipe AddNodeMutation for {intermediate_activity_node}...")
        calc_graph.apply_mutation(
            AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=dried_ingredient_node.uid,
                new_node=intermediate_activity_node,
                copy=False,
            )
        )

        # Intermediate activity node is given a production amount of 1 kg to be consistent with the Brightway nodes.
        production_amount_prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=intermediate_activity_node.uid,
            prop_name="production_amount",
            prop=QuantityProp(
                value=1.0,
                unit_term_uid=self.gfm_factory.kilogram_term.uid,
                for_reference=ReferenceAmountEnum.self_reference,
            ),
        )
        calc_graph.apply_mutation(production_amount_prop_mutation)

        # 3) adding a flow node w/ upscale ratio for ingredient amount estimator

        upscaled_leaf_node = await self.create_subdivision_node(
            calc_graph,
            intermediate_activity_node.uid,
            parent_terms,
            is_dried=False,
        )

        # To produce 1 kg of dried product, we need upscale_ratio kg of the original product.
        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=upscaled_leaf_node.uid,
                prop_name="amount",
                prop=QuantityProp(
                    value=upscale_ratio,
                    unit_term_uid=self.gfm_factory.kilogram_term.uid,
                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                ),
            )
        )

        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=upscaled_leaf_node.uid,
                prop_name="nutrient_upscale_ratio",
                prop=QuantityProp(
                    value=upscale_ratio,
                    unit_term_uid=self.gfm_factory.dimensionless_unit_term.uid,
                    for_reference=ReferenceAmountEnum.self_reference,
                    # Nutrient upscale ratio is independent of reference.
                ),
            )
        )

        # This is the flow for the "original" undried product so the nutrients should not be upscaled.
        # The upscaling is taken care of by the upscale_ratio flow node.

        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=upscaled_leaf_node.uid,
                prop_name="nutrient_values",
                prop=nutrition_data.duplicate(),
            )
        )

    def check_for_water_component(self, nutrition_data: QuantityPackageProp) -> bool:
        # TODO: clarify if total nutrients mass must be used or ingredient mass?..
        # calculating 1% threshold
        water_value = nutrition_data.quantities.get(self.gfm_factory.water_term.uid)
        if water_value:
            water_threshold = sum(self.calculate_sum_of_nutrients(nutrition_data)) / 100

            if water_value is not None:
                water_value = water_value.value
            else:
                water_value = 0.0

            return water_value > water_threshold
        else:
            water_gram_per_100g = get_water_gram_per_100g(nutrition_data)
            return water_gram_per_100g > 1.0

    def calculate_sum_of_nutrients(self, nutrition_data: QuantityPackageProp) -> list[float]:
        """Calculates the sum of nutrients in grams."""
        components = []

        for _, val in nutrition_data.quantities.items():
            if val.get_unit_term().sub_class_of == self.gfm_factory.gram_term.sub_class_of:
                assert val.unit_term_uid == self.gfm_factory.gram_term.uid
                components.append(val.value)

        return components

    def calculate_percent_water_evaporated_for_percent_dry_weight(
        self, nutrition_data: QuantityPackageProp, desired_percentage_dry_weight: float
    ) -> float:
        total_weight = 100

        desired_ratio_water_weight = 1 - desired_percentage_dry_weight / total_weight
        water_gram_per_100g = get_water_gram_per_100g(nutrition_data)

        # prevent division by zero for cases when water_mass is very small
        if water_gram_per_100g < 1e-6:
            return 0.0

        required_water_after_drying = (
            desired_ratio_water_weight * (total_weight - water_gram_per_100g) / (1 - desired_ratio_water_weight)
        )
        percentage_water_evaporated = 100 * (1 - required_water_after_drying / water_gram_per_100g)
        return percentage_water_evaporated

    def calculate_dried_product_nutrients(
        self,
        nutrition_data_input: QuantityPackageProp,
        max_evaporation: float | None,
    ) -> tuple[QuantityPackageProp, float]:
        nutrition_data = nutrition_data_input.duplicate()

        if not max_evaporation:
            if self.node.default_max_evaporation:
                assert (
                    self.node.default_max_evaporation.get_unit_term().name.lower() == "gram"
                    and self.node.default_max_evaporation.for_reference == ReferenceAmountEnum.amount_for_100g
                ), (
                    f"Default max evaporation {self.node.default_max_evaporation} must be given as a percentage, i.e.,"
                    f"grams per 100g of food product."
                )
                max_evaporation = self.node.default_max_evaporation.value / 100  # Convert percentage into fraction.
            else:
                max_evaporation = DEFAULT_MAX_EVAPORATION / 100

        water_g_per_100g = get_water_gram_per_100g(nutrition_data)

        # calculating upscale ratio to correct for the weight loss due to evaporation (i.e. after water loss, we need to
        # correct all nutrients such that they would correspond to 100g of the DRIED product):
        nutrient_upscale_ratio: float = 100 / (100 - max_evaporation * water_g_per_100g)

        new_quantities = {}
        # recalculate all the nutrients
        for key, val in nutrition_data.quantities.items():
            # water should be calculated separately
            new_quantities[key] = val
            if key == self.gfm_factory.water_term.uid:
                new_quantities[key] = ReferencelessQuantityProp.unvalidated_construct(
                    value=(1.0 - max_evaporation) * water_g_per_100g * nutrient_upscale_ratio,
                    unit_term_uid=new_quantities[key].unit_term_uid,
                )
                continue

            if not val.value:
                new_quantities[key] = ReferencelessQuantityProp.unvalidated_construct(
                    value=0.0, unit_term_uid=new_quantities[key].unit_term_uid
                )

            new_quantities[key] = ReferencelessQuantityProp.unvalidated_construct(
                value=new_quantities[key].value * nutrient_upscale_ratio,
                unit_term_uid=new_quantities[key].unit_term_uid,
            )

        nutrition_data.quantities = new_quantities
        nutrition_data.source = QuantityPackageSourceEnum.model

        return nutrition_data, nutrient_upscale_ratio


class NutrientSubdivisionGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: "PostgresDb", service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.dried_state_term: Term | None = None
        self.glossary_service: GlossaryService = service_provider.glossary_service
        self.subdivision_access_group_uid: uuid.UUID | None = None
        self.glossary_link_service: GlossaryLinkService = service_provider.glossary_link_service
        self.water_term = Term(data={}, name="", sub_class_of=None)
        self.gram_term = Term(data={}, name="", sub_class_of=None)
        self.kilogram_term = Term(data={}, name="", sub_class_of=None)
        self.dimensionless_unit_term = Term(data={}, name="", sub_class_of=None)
        self.root_transport_term = Term(data={}, name="", sub_class_of=None)
        self.fresh_term: Term | None = None
        self.linked_nutrient_term_for_dried_tagged: set[uuid.UUID] | None = None
        self.linked_nutrient_terms: dict[frozenset[uuid.UUID], uuid.UUID] = {}

    async def init_cache(self) -> None:
        # caching is done at the glossary service level. But we can init subdivision_access_group_uid here.

        # used to retrieve the subdivisions of a term
        # and storing dried product state term
        self.subdivision_access_group_uid = (
            await self.service_provider.namespace_service.find_default_access_group_by_ns_xid("subdivision")
        )
        foodex2_access_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )

        self.dried_state_term: Term = self.glossary_service.terms_by_xid_ag_uid[("J0116", foodex2_access_group_uid)]
        assert self.dried_state_term, "could not find Term for dried product state"

        self.fresh_term: Term = self.glossary_service.terms_by_xid_ag_uid[("P0120", foodex2_access_group_uid)]
        assert self.fresh_term, "Could not find 'fresh' term."

        root_nutrients_term = self.service_provider.glossary_service.root_subterms.get("EOS_nutrient_names")
        self.water_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_water", str(root_nutrients_term.access_group_uid)
        )
        self.root_transport_term = self.service_provider.glossary_service.root_subterms.get("EOS_Transportation")

        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.gram_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_gram", str(root_unit_term.access_group_uid)
        )
        self.kilogram_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "EOS_kilogram", str(root_unit_term.access_group_uid)
        )
        self.dimensionless_unit_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                "EOS_dimensionless", str(root_unit_term.access_group_uid)
            )
        )

        # We need to know whether the linked brightway activity or nutrient term is coming from the set of terms
        # including the dried term or not. If it is, then we do not have to nutrient subdivide.
        self.linked_nutrient_term_for_dried_tagged = await get_set_of_nutrients_coming_from_dried_tagged(
            self.postgres_db.get_pg_glossary_link_mgr(), self.dried_state_term
        )

        nutrient_glossary_links = await self.service_provider.glossary_link_service.get_glossary_links_by_gfm(
            gap_filling_module="Nutrients"
        )
        for nutrient_glossary_link in nutrient_glossary_links:
            self.linked_nutrient_terms[
                frozenset(nutrient_glossary_link.term_uids)
            ] = nutrient_glossary_link.linked_term_uid

    def spawn_worker(self, node: Node) -> NutrientSubdivisionGapFillingWorker:
        return NutrientSubdivisionGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = NutrientSubdivisionGapFillingFactory
