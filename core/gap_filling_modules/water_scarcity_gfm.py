"""WaterScarcity gap filling module."""

import uuid

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.location_util.countries import iso_3166_map_3_to_2_letter
from gap_filling_modules.origin_gfm import OriginGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from structlog import get_logger

from core.domain.nodes import (
    ElementaryResourceEmissionNode,
    FlowNode,
    FoodProcessingActivityNode,
    FoodProductFlowNode,
    ModeledActivityNode,
)
from core.domain.nodes.node import Node
from core.domain.props import GlossaryTermProp, NamesProp, QuantityProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.utils import flow_location_is_known
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb
from database.postgres.settings import PgSettings

logger = get_logger()
SCARCE_WATER_CONSUMPTION_XID = "EOS_Scarce_Water_Consumption"


class WaterScarcityGapFillingWorker(AbstractGapFillingWorker):
    """Water scarcity gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Whether to schedule water scarcity gap filling worker."""
        # Schedule on Brightway nodes with a parent that has a product_name
        if (
            isinstance(self.node, ModeledActivityNode)
            and self.node.get_parent_nodes()
            and any(
                isinstance(parent_node, FoodProductFlowNode) and parent_node.product_name
                for parent_node in self.node.get_parent_nodes()
            )
        ):
            return True
        else:
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Whether water scarcity gap filling worker can be executed."""
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(OriginGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[WaterScarcity] wait for OriginGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # The following block exists to avoid adding water scarcity activity to two Brightway nodes, before
        # and after transport. In transport mode decision GFM, the Brightway Node sometimes gets Duplicated.
        # We do not want to run WaterScarcityGFM on both Brightway nodes, running on just one of them is sufficient.
        if any(isinstance(sub_node, FoodProductFlowNode) for sub_node in self.node.get_sub_nodes()):
            logger.debug(
                "[WaterScarcity] should be scheduled only on the leaf Brightway node (at the origin) --> cancel."
            )
            return GapFillingWorkerStatusEnum.cancel

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        """Execute water scarcity gap filling worker."""
        parent_flows = self.node.get_parent_nodes()
        is_scarce_water_node_in_graph = bool(
            calc_graph.get_node_by_uid(self.gfm_factory.scarce_water_consumption_node.uid)
        )
        for parent_flow in parent_flows:
            if isinstance(parent_flow, FoodProductFlowNode) and parent_flow.product_name:
                parent_flow_product_name_terms = [prop.get_term() for prop in parent_flow.product_name.terms]
                transport_term = False
                for product_name_term in parent_flow_product_name_terms:
                    if product_name_term.sub_class_of == self.gfm_factory.root_transport_term.uid:
                        transport_term = True  # TODO, change transport flow nodes into TransportFlowNode.
                if transport_term:
                    continue

                locations, _ = parent_flow.get_prop_by_inheritance(
                    "flow_location", ignore_if=(lambda prop: not flow_location_is_known(prop))
                )
                if locations:
                    flow_country_codes = [location.country_code for location in locations if location.country_code]
                else:
                    flow_country_codes = []

                if not flow_country_codes:
                    warning_msg = (
                        f"No location found for product {parent_flow_product_name_terms}."
                        f"Using a median value of all available countries as a fallback."
                    )
                    logger.debug(warning_msg)
                    flow_country_code = "median"
                elif len(flow_country_codes) > 1:
                    warning_msg = (
                        f"Multiple locations {flow_country_codes} for product {parent_flow_product_name_terms}."
                        f"Using a median value of all available countries as a fallback."
                    )
                    logger.debug(warning_msg)
                    flow_country_code = "median"
                else:
                    flow_country_code = flow_country_codes[0]
                    if len(flow_country_code) == 3:
                        flow_country_code = iso_3166_map_3_to_2_letter[flow_country_code]

                if any(not term for term in parent_flow_product_name_terms):
                    logger.debug(
                        "Not all FoodEx2 Terms are well-defined for this node.",
                        node_uid=self.node.uid,
                    )
                    continue
                else:
                    water_scarcity_term_uid = self.gfm_factory.linked_water_scarcity_terms.get(
                        frozenset([term.uid for term in parent_flow_product_name_terms])
                    )
                    if water_scarcity_term_uid:
                        water_scarcity_term = await self.gfm_factory.service_provider.glossary_service.get_term_by_uid(
                            water_scarcity_term_uid
                        )
                    else:
                        water_scarcity_term = None

                    if not water_scarcity_term:
                        warning_msg = (
                            f"Water scarcity index is unavailable for product {parent_flow_product_name_terms},"
                            f" skipping this product in water scarcity calculation."
                        )
                        logger.debug(warning_msg)
                        continue

                    if flow_country_code in water_scarcity_term.data:
                        scarce_water_consumption_m3_per_kg = water_scarcity_term.data[flow_country_code]
                    else:
                        warning_msg = (
                            f"Water scarcity index is unavailable for product {parent_flow_product_name_terms} in "
                            f"country {flow_country_code}. Using median of data for all available countries."
                        )
                        logger.debug(warning_msg)
                        scarce_water_consumption_m3_per_kg = water_scarcity_term.data["median"]

                    # Add scarce water consumption activity

                    # 1) remove edge between the Brightway node and parent flow node.
                    remove_edge_mutation = RemoveEdgeMutation(
                        created_by_module=self.__class__.__name__,
                        from_node_uid=parent_flow.uid,
                        to_node_uid=self.node.uid,
                    )
                    calc_graph.apply_mutation(remove_edge_mutation)

                    # 2) create scarce water consumption activity node
                    water_consumption_activity = FoodProcessingActivityNode.model_construct(
                        uid=uuid.uuid4(),
                        production_amount=self.node.production_amount.duplicate(),
                    )

                    # 3) re-establish edges between parent flow node and scarce water consumption activity node.
                    add_child_node_mutation = AddNodeMutation(
                        created_by_module=self.__class__.__name__,
                        parent_node_uid=parent_flow.uid,
                        new_node=water_consumption_activity,
                        copy=False,
                    )
                    calc_graph.apply_mutation(add_child_node_mutation)

                    # 4) add flow between Brightway node and scarce water consumption activity node
                    food_flow_uid = uuid.uuid4()
                    calc_graph.apply_mutation(
                        DuplicateNodeMutation(
                            created_by_module=self.__class__.__name__,
                            new_node_uid=food_flow_uid,
                            source_node_uid=parent_flow.uid,
                            parent_node_uid=water_consumption_activity.uid,
                            gfms_to_not_schedule=(
                                "OriginGapFillingWorker",
                                "NutrientSubdivisionGapFillingWorker",
                                "TransportModeDistanceGapFillingWorker",
                                "TransportDecisionGapFillingWorker",
                            ),
                            duplicate_child_nodes=False,
                        )
                    )

                    # Overwrite amount_in_original_source_unit and amount:
                    calc_graph.apply_mutation(
                        PropMutation(
                            created_by_module=self.__class__.__name__,
                            node_uid=food_flow_uid,
                            prop_name="amount_in_original_source_unit",
                            prop=QuantityProp.unvalidated_construct(
                                value=self.node.production_amount.value,
                                unit_term_uid=self.node.production_amount.unit_term_uid,
                                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                            ),
                        )
                    )
                    calc_graph.apply_mutation(
                        PropMutation(
                            created_by_module=self.__class__.__name__,
                            node_uid=food_flow_uid,
                            prop_name="amount",
                            prop=QuantityProp.unvalidated_construct(
                                value=self.node.production_amount.value,
                                unit_term_uid=self.node.production_amount.unit_term_uid,
                                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                            ),
                        )
                    )

                    add_edge_mutation = AddEdgeMutation(
                        created_by_module=self.__class__.__name__,
                        from_node_uid=food_flow_uid,
                        to_node_uid=self.node.uid,
                    )
                    calc_graph.apply_mutation(add_edge_mutation)

                    # 5) add scarce water consumption flow
                    production_amount_unit: Term = self.node.production_amount.get_unit_term()
                    assert (
                        production_amount_unit.sub_class_of == self.gfm_factory.kilogram_term.sub_class_of
                    ), f"Food product Brightway node {self.node} has production amount in non weight units."
                    production_amount_in_kg = UnitWeightConverter.convert_between_same_unit_types(
                        self.node.production_amount.value,
                        production_amount_unit,
                        self.gfm_factory.kilogram_term,
                        "mass-in-g",
                    )

                    scarce_water_consumption_flow = FlowNode.model_construct(
                        uid=uuid.uuid4(),
                        product_name=NamesProp.unvalidated_construct(
                            terms=[
                                GlossaryTermProp.unvalidated_construct(
                                    term_uid=self.gfm_factory.scarce_water_consumption_term.uid
                                )
                            ]
                        ),
                        amount_in_original_source_unit=QuantityProp.unvalidated_construct(
                            # Negative sign for consumption.
                            value=-production_amount_in_kg * scarce_water_consumption_m3_per_kg,
                            unit_term_uid=self.gfm_factory.cubic_meter_term.uid,
                            for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                        ),
                    )

                    add_child_node_mutation = AddNodeMutation(
                        created_by_module=self.__class__.__name__,
                        parent_node_uid=water_consumption_activity.uid,
                        new_node=scarce_water_consumption_flow,
                        copy=False,
                    )
                    calc_graph.apply_mutation(add_child_node_mutation)

                    if not is_scarce_water_node_in_graph:
                        node_to_add = self.gfm_factory.scarce_water_consumption_node.model_copy(deep=True)
                        node_to_add.remove_private_attributes()
                        add_child_node_mutation = AddNodeMutation(
                            created_by_module=self.__class__.__name__,
                            parent_node_uid=scarce_water_consumption_flow.uid,
                            new_node=node_to_add,
                            copy=False,
                        )
                        calc_graph.apply_mutation(add_child_node_mutation)
                        is_scarce_water_node_in_graph = True
                    else:
                        add_edge_mutation = AddEdgeMutation(
                            created_by_module=self.__class__.__name__,
                            from_node_uid=scarce_water_consumption_flow.uid,
                            to_node_uid=self.gfm_factory.scarce_water_consumption_node.uid,
                        )
                        calc_graph.apply_mutation(add_edge_mutation)


class WaterScarcityGapFillingFactory(AbstractGapFillingFactory):
    """Water scarcity gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.root_transport_term = self.service_provider.glossary_service.root_subterms.get("EOS_Transportation")

        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.cubic_meter_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_cubic-meter", root_unit_term.access_group_uid)
        ]
        self.liter_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_liter", root_unit_term.access_group_uid)
        ]
        self.kilogram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilogram", root_unit_term.access_group_uid)
        ]
        self.scarce_water_consumption_term = self.service_provider.glossary_service.root_subterms.get(
            "EOS_Scarce_Water_Consumption"
        )
        self.scarce_water_consumption_node: ElementaryResourceEmissionNode | None = None
        self.scarce_water_consumption_node_uid: uuid.UUID | None = None
        self.linked_water_scarcity_terms: dict[frozenset[uuid.UUID], uuid.UUID] = {}

    async def init_cache(self) -> None:
        """Initialize cache required for water scarcity gap filling factory."""
        settings = PgSettings()
        self.scarce_water_consumption_node_uid = (
            await self.service_provider.postgres_db.product_mgr.find_or_create_uid_by_xid(
                settings.EATERNITY_NAMESPACE_UUID, SCARCE_WATER_CONSUMPTION_XID
            )
        )

        self.scarce_water_consumption_node = await self.service_provider.node_service.find_by_uid(
            self.scarce_water_consumption_node_uid
        )

        water_scarcity_glossary_links = await self.service_provider.glossary_link_service.get_glossary_links_by_gfm(
            gap_filling_module="WaterScarcity"
        )
        for water_scarcity_glossary_link in water_scarcity_glossary_links:
            self.linked_water_scarcity_terms[
                frozenset(water_scarcity_glossary_link.term_uids)
            ] = water_scarcity_glossary_link.linked_term_uid

        if not self.scarce_water_consumption_node:
            self.scarce_water_consumption_node = ElementaryResourceEmissionNode(
                production_amount=QuantityProp.unvalidated_construct(
                    value=1.0, unit_term_uid=self.liter_term.uid, for_reference=ReferenceAmountEnum.self_reference
                ),
                uid=self.scarce_water_consumption_node_uid,
            )
            await self.service_provider.node_service.upsert_node_by_uid(self.scarce_water_consumption_node)
            await self.service_provider.product_service.update_all_mappings()

    def spawn_worker(self, node: Node) -> WaterScarcityGapFillingWorker:
        """Spawn water scarcity gap filling worker."""
        return WaterScarcityGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = WaterScarcityGapFillingFactory
