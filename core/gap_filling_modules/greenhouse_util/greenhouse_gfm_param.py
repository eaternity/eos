import calendar
from datetime import datetime, timedelta
from math import floor

"""Model parameters for the greenhouse gap filling module."""

FRACTION_OF_GLASS_IN_BUILDING_MATERIAL = 0.604
FRACTION_OF_PLASTIC_IN_BUILDING_MATERIAL = 0.396
AREA_OF_THE_WINDOWS_EXPOSED_TO_THE_SUN = 46_800  # m2 (assumed to be equal to the ground area of the greenhouse)

BW_XID_FOR_GLASS = "ecoinvent 3.6 cutoff_0ce659c3cfd443a38761058ee62e3f10"
BW_XID_FOR_PLASTIC = "ecoinvent 3.6 cutoff_fddfe51c6959f41ac044089c3a892af7"
HEATING_MIX_BW_ID = {
    "CH": "EDB_4220cbaabca343c09b19415d5ab4079f_copy1",
    "DE": "EDB_4220cbaabca343c09b19415d5ab4079f_copy2",
    "NL": "EDB_4220cbaabca343c09b19415d5ab4079f",
    "EU other": "EDB_4220cbaabca343c09b19415d5ab4079f_copy3",
}


def count_leap_years(start_year: int, end_year: int) -> int:
    leap_years = sum(1 for year in range(start_year, end_year + 1) if calendar.isleap(year))
    return leap_years


def datetime_now_wrapper() -> datetime:
    return datetime.now()


def days_in_each_month(growth_duration: int, harvest_date: datetime) -> list[int]:
    """Return the number of days in each month between the start and end date."""
    # Define the last day of each month (assuming February has 28 days)

    last_day_of_month_dict = {
        1: 31,  # January
        2: 28,  # February
        3: 31,  # March
        4: 30,  # April
        5: 31,  # May
        6: 30,  # June
        7: 31,  # July
        8: 31,  # August
        9: 30,  # September
        10: 31,  # October
        11: 30,  # November
        12: 31,  # December
    }

    number_of_full_years = floor(growth_duration / 365)

    start_date = harvest_date - timedelta(days=growth_duration)
    end_date = harvest_date

    start_month = start_date.month
    start_day = start_date.day
    end_month = end_date.month
    end_day = end_date.day

    # Compute number of leap years:
    start_year = start_date.year
    end_year = end_date.year

    if start_month > 2:
        # start_year does not count towards the total number of leap years:
        start_year += 1

    if end_month <= 2:
        # end_year does not count towards the total number of leap years:
        end_year -= 1

    number_of_leap_years = count_leap_years(start_year, end_year)

    if start_month > end_month:
        end_month += 12  # Add 12 for the loop below.

    days_in_month = [number_of_full_years * last_day_of_month_dict[i + 1] for i in range(12)]

    for i in range(int(start_month), int(end_month) + 1):
        month_number = i % 12
        if month_number == 0:
            month_number = 12

        if i == int(start_month):
            days_in_month[month_number - 1] += last_day_of_month_dict[month_number] - int(start_day) + 1
        elif i == int(end_month):
            days_in_month[month_number - 1] += int(end_day) - 1
        else:
            days_in_month[month_number - 1] += last_day_of_month_dict[month_number]

    days_in_month[1] += number_of_leap_years
    return days_in_month


# if vegetable is matched to one of these product names, we want to run the greenhouse gap filling module
# the vegetable shouldn't be matched to a combination of different vegetables (for combined products we
# don't match to all the ingredients, but just attach the ingredients.)
PRODUCT_NAMES_WITH_GREENHOUSE_MODEL = {
    "B1458": "eggplant",  # eggplant
    "A00JD": "eggplant",  # aubergines
    "A00JM": "cucumber",  # cucumbers
    "A00JR": "cucumber",  # courgettes
    "A00KY": "lettuce",  # lettuce
    "A00MJ": "lettuce",  # spinach
    "B4946": "lettuce",  # batavia lettuce
    "A00KX": "lettuce",  # lettces generic
    "A1563": "lettuce",  # iceberg lettuce
    "A0DLB": "lettuce",  # lettuces and similar
    "A1612": "lettuce",  # oakleaf lettuce
    "A00LB": "lettuce",  # lollo rosso
    "A00JA": "bell pepper",  # sweet peppers
    "A00QV": "radish",  # radishes
    "A00LM": "radish",  # roman rocket and similar
    "B2474": "radish",  # rocket
    "A0DMX": "tomato",  # tomatoes
    "A00HY": "vine tomato",  # cherry tomatoes
}

NUMBER_OF_GROWING_DAYS = {
    "eggplant": 50,
    "cucumber": 32,
    "lettuce": 60,
    "bell pepper": 41,
    "radish": 51,
    "vine tomato": 127,
    "tomato": 127,
}


REQUIRED_INSIDE_TEMPERATURE = {
    "eggplant": 291.15,
    "cucumber": 291.15,
    "lettuce": 281.15,
    "bell pepper": 293.15,
    "radish": 279.15,
    "tomato": 291.15,
    "vine tomato": 291.15,
}

AVG_YIELD_PER_M2_PER_MONTH = {
    "eggplant": 3.15,
    "cucumber": 4.36,
    "lettuce": 1.74,
    "bell pepper": 1.97,
    "radish": 1.36,
    "tomato": 4.66,
    "vine tomato": 4.72,
    # TODO Add other tomato variants?
}

REQUIRED_ELECTRICITY_KWH_PER_KG = {
    "eggplant": 0.5492,
    "cucumber": 0.1982,
    "lettuce": 0.4636,
    "bell pepper": 0.5746,
    "radish": 0.33798,
    "tomato": 0.2207,
    "vine tomato": 0.2099,
}
U_VALUE = 3.4
CLADDING_AREA = 54_978.2

Q_TRANS_FACTOR = U_VALUE * CLADDING_AREA

AIR_EXCHANGE_NUMBER = 0.24
BUILDING_VOLUME = 259_506.0
SPEC_VOLUMETRIC_ENERGY_CONSTANT_FOR_AIR = 0.32

Q_AIR_FACTOR = AIR_EXCHANGE_NUMBER * BUILDING_VOLUME * SPEC_VOLUMETRIC_ENERGY_CONSTANT_FOR_AIR

Q_SOLAR_FACTOR = 0.609 * 46_800.0 * 0.99 * 0.9 * 0.7
