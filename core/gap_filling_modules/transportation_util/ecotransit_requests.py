import json
from dataclasses import dataclass
from datetime import date, datetime
from decimal import Decimal
from typing import Iterable, Optional, cast

from gap_filling_modules.transportation_util.enum import CoolingTypeEnum, ServiceTransportModeEnum
from gap_filling_modules.transportation_util.settings import TransportModeDistanceGfmSettings
from zeep import Client, Transport, helpers
from zeep.cache import InMemoryCache

settings = TransportModeDistanceGfmSettings()


@dataclass
class RoutingClientLocation(object):
    longitude: float
    latitude: float

    def __hash__(self):
        return hash((self.longitude, self.latitude))

    def __eq__(self, other: object) -> bool:
        if type(other) is not type(self):
            return False
        other = cast(type(self), other)
        return (self.longitude, self.latitude) == (other.longitude, other.latitude)


@dataclass
class RoutingRequest(object):
    # TODO: for now, let's just use coordinates as query
    origin_location: RoutingClientLocation
    target_location: RoutingClientLocation
    transport_mode: ServiceTransportModeEnum
    cooling_type: CoolingTypeEnum

    def __hash__(self):
        return hash((self.origin_location, self.target_location, self.transport_mode, self.cooling_type))

    def __eq__(self, other: object):
        if type(other) is not type(self):
            return False
        other = cast(type(self), other)
        return (self.origin_location, self.target_location, self.transport_mode, self.cooling_type) == (
            other.origin_location,
            other.target_location,
            other.transport_mode,
            other.cooling_type,
        )


@dataclass
class RoutingTransportSection(object):
    start_location: RoutingClientLocation
    end_location: RoutingClientLocation
    # could be only co2 equivalents or also distances
    distance: float
    co2_equivalent_emissions: float
    parameter_info: str
    transport_mode: ServiceTransportModeEnum


def request_generator() -> tuple:
    cache = InMemoryCache()
    transport = Transport(cache=cache)
    client = Client(settings.ECOTRANSIT_WSDL_URL, transport=transport)
    client.settings(strict=False, raw_response=True)

    Cargo = client.get_type("ns0:Cargo")
    Date = client.get_type("ns0:Date")
    AccountingVariantParameters = client.get_type("ns0:AccountingVariantParameters")
    AccountingVariant = client.get_type("ns0:AccountingVariant")
    WeightUnit = client.get_type("ns0:WeightUnit")
    # Rail
    UicCode = client.get_type("ns0:UicCode")
    Wgs84Coordinates = client.get_type("ns0:Wgs84Coordinates")
    # Truck
    ZipCodeType = client.get_type("ns0:ZipCodeType")
    CityName = client.get_type("ns0:CityName")

    # Air (stored as string type, not a separate one)
    # IataCode = client.get_type("ns0:IataCode")
    # Sea (stored as string type, not a separate one)
    # unLocode = client.get_type("ns0:unLocode")

    LocationRef = client.get_type("ns0:LocationRef")
    RequestStation = client.get_type("ns0:RequestStation")
    CarriageParameters = client.get_type("ns0:CarriageParameters")
    RoadParameters = client.get_type("ns0:RoadParameters")
    RailParameters = client.get_type("ns0:RailParameters")
    AirParameters = client.get_type("ns0:AirParameters")
    SeaShipParameters = client.get_type("ns0:SeaShipParameters")
    InlandshipParameters = client.get_type("ns0:InlandshipParameters")
    LogisticSiteParameters = client.get_type("ns0:LogisticSiteParameters")
    PreOrPostCarriageParameters = client.get_type("ns0:PreOrPostCarriageParameters")
    Emission = client.get_type("ns0:Emission")
    Output = client.get_type("ns0:Output")
    OutputSplit = client.get_type("ns0:OutputSplit")

    UserSpecificEmissionData = client.get_type("ns0:UserSpecificEmissionData")
    Biofuels = client.get_type("ns0:Biofuels")
    VolumeWeight = client.get_type("ns0:VolumeWeight")

    def make_weight_unit(unit: str) -> WeightUnit:
        assert unit in ["ton", "teu", "cargo", "feu"]
        return WeightUnit(unit)

    def make_cargo(
        weight: float = None,
        unit: WeightUnit = None,
        volume_weight: VolumeWeight = None,
        tons_per_teu: float = None,
        tons_per_feu: float = None,
        tons_per_pallet: float = None,
        container_empty_weight: float = None,
    ) -> Cargo:
        return Cargo(weight, unit, volume_weight, tons_per_teu, tons_per_feu, tons_per_pallet, container_empty_weight)

    def make_date(day: int, month: int, year: int) -> Date:
        return Date(day, month, year)

    def make_accounting_variant(accounting_variant: str) -> AccountingVariant:
        assert accounting_variant in ["None", "ccwg-tradelane", "ccwg-tradelane-reefer"]
        return AccountingVariant(accounting_variant)

    def make_accounting_variant_parameters(variant: AccountingVariant, year: int) -> AccountingVariantParameters:
        return AccountingVariantParameters(variant, year)

    def make_uicCode(country: int, station: int) -> UicCode:
        return UicCode(country, station)

    def make_zip_code_type(country: str, code: str) -> ZipCodeType:
        return ZipCodeType(country, code)

    def make_wgs84_coordinates(longitude: float, latitude: float) -> Wgs84Coordinates:
        return Wgs84Coordinates(longitude, latitude)

    def make_city_name(name: str, country: str) -> CityName:
        return CityName(name, country)

    def make_location_ref(id: int) -> LocationRef:
        return LocationRef(id)

    def make_request_station(
        uicCode: UicCode = None,
        wgs84: Wgs84Coordinates = None,
        zipCode: ZipCodeType = None,
        iataCode: str = None,
        unLocode: str = None,
        cityName: CityName = None,
        location: LocationRef = None,
        longName: str = None,
        shortName: str = None,
        directConnection: bool = False,
        max_rfs_distance: float = None,
    ) -> RequestStation:
        return RequestStation(
            uicCode,
            wgs84,
            zipCode,
            iataCode,
            unLocode,
            cityName,
            location,
            longName,
            shortName,
            directConnection,
            max_rfs_distance,
        )

    def make_road_parameters() -> RoadParameters:
        return RoadParameters()

    def make_rail_parameters() -> RailParameters:
        return RailParameters()

    def make_air_parameters() -> AirParameters:
        return AirParameters()

    def make_sea_ship_parameters() -> SeaShipParameters:
        return SeaShipParameters()

    def make_inland_ship_parameters() -> InlandshipParameters:
        return InlandshipParameters()

    def make_logistic_site_parameters() -> LogisticSiteParameters:
        return LogisticSiteParameters

    def make_carriage_parameters(
        road: Optional[RoadParameters] = None,
        rail: Optional[RailParameters] = None,
        air: Optional[AirParameters] = None,
        sea: Optional[SeaShipParameters] = None,
        inlandwaterways: Optional[InlandshipParameters] = None,
        logisticsite: Optional[LogisticSiteParameters] = None,
        user_specific: Optional[UserSpecificEmissionData] = None,
        biofuels: Optional[Biofuels] = None,
        virtual_distance: Optional[float] = None,
        cooled_transport: Optional[str] = None,
        bio_fuel_share: Optional[float] = None,
        fuel_consumption: Optional[float] = None,
    ) -> CarriageParameters:
        return CarriageParameters(
            road,
            rail,
            air,
            sea,
            inlandwaterways,
            logisticsite,
            user_specific,
            biofuels,
            virtual_distance,
            cooled_transport,
            bio_fuel_share,
            fuel_consumption,
        )

    def make_pre_or_post_carriage_parameters(
        road: Optional[RoadParameters] = None,
        rail: Optional[RailParameters] = None,
        air: Optional[AirParameters] = None,
        sea: Optional[SeaShipParameters] = None,
        user_specific: Optional[UserSpecificEmissionData] = None,
        biofuels: Optional[Biofuels] = None,
        virtual_distance: Optional[float] = None,
        cooled_transport: Optional[str] = None,
        bio_fuel_share: Optional[float] = None,
        fuel_consumption: Optional[float] = None,
    ) -> PreOrPostCarriageParameters:
        return PreOrPostCarriageParameters(
            road,
            rail,
            air,
            sea,
            user_specific,
            biofuels,
            virtual_distance,
            cooled_transport,
            bio_fuel_share,
            fuel_consumption,
        )

    def make_output_split(frontiers: bool = False, railTraction: bool = False, seaArea: bool = False) -> OutputSplit:
        return OutputSplit(frontiers, railTraction, seaArea)

    def make_output(split: OutputSplit = None, createKml: bool = False, infoLevel: str = None) -> Output:
        _ = infoLevel
        return Output(split, createKml)

    def make_request(
        departure_longitude: float,
        departure_latitude: float,
        destination_longitude: float,
        destination_latitude: float,
        transport_mode: ServiceTransportModeEnum,
    ) -> dict:
        # create a main_carriage based on the requested transport mode
        if transport_mode == transport_mode.AIR:
            ready_main_carriage = make_carriage_parameters(air=make_air_parameters())
        elif transport_mode == transport_mode.SEA:
            ready_main_carriage = make_carriage_parameters(sea=make_sea_ship_parameters())
        elif transport_mode == transport_mode.GROUND:
            ready_main_carriage = make_carriage_parameters(road=make_road_parameters())
        elif transport_mode == transport_mode.TRAIN:
            ready_main_carriage = make_carriage_parameters(rail=make_rail_parameters())
        else:
            AssertionError(f"Can't make ecotransit request for transport mode {transport_mode.name}.")

        # prepare pre-/post-carriage parameters
        ready_pre_and_post_carriage = make_pre_or_post_carriage_parameters(road=make_road_parameters())

        complex_transport_request = {
            "cargo": make_cargo(weight=1, unit=make_weight_unit("ton")),
            "transportDate": None,  # provided transport dates must be exact or they cause unwanted swings
            "accountingVariant": make_accounting_variant_parameters(
                make_accounting_variant(settings.ECOTRANSIT_CALCULATION_METHOD),
                settings.ECOTRANSIT_CALCULATION_YEAR,
            ),
            "section": {
                "route": {
                    "departure": make_request_station(
                        wgs84=make_wgs84_coordinates(
                            departure_longitude,
                            departure_latitude,
                        )
                    ),
                    "destination": make_request_station(
                        wgs84=make_wgs84_coordinates(
                            destination_longitude,
                            destination_latitude,
                        )
                    ),
                },
                "carriage": {
                    "preCarriage": ready_pre_and_post_carriage,
                    "mainCarriage": ready_main_carriage,
                    "postCarriage": ready_pre_and_post_carriage,
                },
                "information": "",
            },
            "output": make_output(make_output_split()),
            "customer": settings.ECOTRANSIT_CUSTOMER,
            "password": settings.ECOTRANSIT_PASSWORD,
            "transportID": "",
        }
        return complex_transport_request

    def send_request(request: dict) -> dict:
        return client.service.calculateComplexTransport(**request)

    def extract_distance(distances: Emission) -> float:
        assert distances.unit == "Kilometers"
        result = 0
        if distances.rail:
            result += distances.rail.tankToWheel
        if distances.sea:
            result += distances.sea.tankToWheel
        if distances.air:
            result += distances.air.tankToWheel
        if distances.inlandWaterways:
            result += distances.inlandWaterways.tankToWheel
        if distances.road:
            result += distances.road.tankToWheel
        return result

    def extract_co2_equivalent(co2_equivalents: Emission) -> float:
        assert co2_equivalents.unit == "Tonne"
        result = 0
        if co2_equivalents.rail:
            result += co2_equivalents.rail.tankToWheel
        if co2_equivalents.sea:
            result += co2_equivalents.sea.tankToWheel
        if co2_equivalents.air:
            result += co2_equivalents.air.tankToWheel
        if co2_equivalents.inlandWaterways:
            result += co2_equivalents.inlandWaterways.tankToWheel
        if co2_equivalents.road:
            result += co2_equivalents.road.tankToWheel
        return result

    def extract_service_transport_mode(transport_mode: str) -> ServiceTransportModeEnum:
        if transport_mode == "Road":
            return ServiceTransportModeEnum.GROUND
        elif transport_mode == "Sea":
            return ServiceTransportModeEnum.SEA
        elif transport_mode == "Air":
            return ServiceTransportModeEnum.AIR
        elif transport_mode == "Rail":
            return ServiceTransportModeEnum.TRAIN

    # expected parameter: response.result.section[0].preCarriage[0]
    def extract_inner_sections(carriage: Iterable) -> list:
        extracted_sections_list = []
        for inner_section in carriage:
            extracted_sections_list.append(
                RoutingTransportSection(
                    RoutingClientLocation(
                        inner_section.route.startLocation.wgs84.longitude,
                        inner_section.route.startLocation.wgs84.latitude,
                    ),
                    RoutingClientLocation(
                        inner_section.route.endLocation.wgs84.longitude, inner_section.route.endLocation.wgs84.latitude
                    ),
                    extract_distance(inner_section.emissions.distances),
                    extract_co2_equivalent(inner_section.emissions.co2Equivalents),
                    inner_section.parameterInfo,
                    extract_service_transport_mode(inner_section.transportMode),
                )
            )
        return extracted_sections_list

    # TODO figure out the type for api_response. The function is not used currently.
    # ruff prefix does not work for no quality assurance tag.
    def extract_sections(api_response) -> list:  # noqa: ANN001
        extracted_sections_list = []

        if api_response.error:
            return api_response.error

        for result_section in api_response.result.section:
            try:
                extracted_sections_list.append(*extract_inner_sections(result_section.preCarriage))
            # happens if we try to append an empty list
            except TypeError:
                pass

            try:
                extracted_sections_list.append(*extract_inner_sections(result_section.mainhaul))
            except TypeError:
                pass

            try:
                extracted_sections_list.append(*extract_inner_sections(result_section.postCarriage))
            except TypeError:
                pass

        return extracted_sections_list

    return send_request, make_request, extract_sections


class EcotransitRoutingClient(object):
    def run_calculation(self, request: RoutingRequest) -> dict:
        send_request, make_request, extract_sections = request_generator()
        api_params = (
            request.origin_location.longitude,
            request.origin_location.latitude,
            request.target_location.longitude,
            request.target_location.latitude,
            request.transport_mode,
        )
        api_request = make_request(*api_params)
        api_response = send_request(api_request)

        client_response_converted = json.dumps(
            helpers.serialize_object(api_response, target_cls=dict),
            cls=CustomerEncoder,
            indent=2,
        )

        return json.loads(client_response_converted)


# zeep returns non-serializeable objects, so here is a customer encoder.
class CustomerEncoder(json.JSONEncoder):
    def default(self, o: object) -> float | str:
        if isinstance(o, Decimal):
            return float(o)
        if isinstance(o, date):
            return str(o)
        else:
            return super(CustomerEncoder, self).default(o)


# script for trying out this module's code independently of Transportation Mode & Distance GFM
if __name__ == "__main__":
    send_request_fun, make_request_fun, extract_sections_fun = request_generator()
    transport_modes = (
        ServiceTransportModeEnum.SEA,
        ServiceTransportModeEnum.GROUND,
        ServiceTransportModeEnum.AIR,
    )

    for tp in transport_modes:
        # lisboa -> zurich
        params = [-9.095108812466838, 38.768537285012684, 8.541693687438965, 47.376888275146484, tp]

        request = make_request_fun(*params)
        response = send_request_fun(*params)
        sections = extract_sections_fun(response)

        json_response = json.dumps(
            {
                "request": helpers.serialize_object(request),
                "response": helpers.serialize_object(response, target_cls=dict),
            },
            cls=CustomerEncoder,
            indent=2,
        )

        # return json_response
        with open("dump/{}_{}.json".format(datetime.now(), tp.name), "w") as f:
            f.write(json_response)
            f.close()
