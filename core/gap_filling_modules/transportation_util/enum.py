from enum import Enum


# enum for modes user specifies in Eaternity API
class TransportModeDistanceEnum(str, Enum):
    air = "air"
    ground = "ground"
    # TODO: keep this?
    rail = "rail"
    sea = "sea"


# enum for modes specified in EcoTransit responses
class ServiceTransportModeEnum(str, Enum):
    GROUND = "road"
    AIR = "air"
    SEA = "sea"
    TRAIN = "rail"

    UNKNOWN = "undefined"
    REGIONAL = "regional"


# TODO: unused for now
USER_TRANSPORT_MODE_TO_SERVICE_MODE_MAPPING = {
    TransportModeDistanceEnum.air: ServiceTransportModeEnum.AIR,
    TransportModeDistanceEnum.ground: ServiceTransportModeEnum.GROUND,
    TransportModeDistanceEnum.sea: ServiceTransportModeEnum.SEA,
    TransportModeDistanceEnum.rail: ServiceTransportModeEnum.TRAIN,
}


class CoolingTypeEnum(int, Enum):
    NONE, COOLED, FROZEN = range(3)
