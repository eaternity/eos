"""Inventory connector gap filling module."""
import uuid
from uuid import UUID

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.ingredient_amount_estimator_gfm import IngredientAmountEstimatorGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from structlog import get_logger

from core.domain.nodes import ModeledActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.environmental_flows_prop import EnvironmentalFlowsProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class InventoryConnectorGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "InventoryConnectorGapFillingFactory"):
        """Load child nodes of processes.

        If it finds a system node, it will load the environmental flows from the aggregated_cache, not from the graph.
        This is our way to speed up the graph loading process, which can be heavy when loading 10k's of nodes
        from e.g. Ecoinvent.
        """
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        if isinstance(self.node, ModeledActivityNode):
            logger.debug("found process or emission --> scheduled.")
            return True
        elif isinstance(self.node, FlowNode):
            if self.node.get_parent_nodes() and isinstance(self.node.get_parent_nodes()[0], ModeledActivityNode):
                logger.debug("flow from modeled activity --> scheduled.")
                return True
            else:
                return False
        else:
            logger.debug("not on root ingredient or (process, emission, flow) node --> not scheduled.")
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        if len(self.node.get_sub_nodes()) > 0:
            logger.debug("Node already has child nodes --> canceling inventory connector.")
            return GapFillingWorkerStatusEnum.cancel

        global_gfm_state = self.get_global_gfm_state()

        if (
            global_gfm_state.get(IngredientAmountEstimatorGapFillingWorker.__name__, 0)
            == NodeGfmStateEnum.scheduled.value
        ):
            logger.debug(
                "recipe root-node does not yet have ingredients amount estimation calculated" " --> not can_run_now."
            )
            return GapFillingWorkerStatusEnum.reschedule

        logger.debug("--> can_run_now.")

        return GapFillingWorkerStatusEnum.ready

    async def add_node_mutation(
        self,
        calc_graph: CalcGraph,
        node_uid: UUID,
        parent_uid: UUID,
    ) -> None:
        """Helper function to add a node mutation to the calc_graph."""
        # check if node already exists in this calc-graph:
        existing_node_in_graph = calc_graph.get_node_by_uid(node_uid)

        if existing_node_in_graph is None:
            node_from_cache = await calc_graph.service_provider.node_service.find_by_uid(node_uid)
            node_to_add = node_from_cache.model_copy(deep=True)
            node_to_add.remove_private_attributes()
            logger.debug(f"create AddNodeMutation for {node_to_add}...")

            if isinstance(node_to_add, FlowNode):
                node_amount = node_to_add.amount_in_original_source_unit
            else:
                node_amount = node_to_add.production_amount

            if isinstance(node_amount, QuantityProp):
                unit_term_uid = node_amount.unit_term_uid
            else:
                unit_term_uid = UnitWeightConverter.unit_term_from_unit_name(node_amount.unit).uid

            if isinstance(node_to_add, ActivityNode):
                node_to_add.production_amount = QuantityProp.unvalidated_construct(
                    value=node_to_add.production_amount.value if node_to_add.production_amount else 1.0,
                    unit_term_uid=unit_term_uid,
                    for_reference=ReferenceAmountEnum.self_reference,
                    source_data_raw=node_to_add.production_amount.model_dump()
                    if node_to_add.production_amount
                    else None,
                )
            else:
                node_to_add.amount_in_original_source_unit = QuantityProp.unvalidated_construct(
                    value=node_to_add.amount_in_original_source_unit.value
                    if node_to_add.amount_in_original_source_unit
                    else 1.0,
                    unit_term_uid=unit_term_uid,
                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    source_data_raw=node_to_add.amount_in_original_source_unit.model_dump()
                    if node_to_add.amount
                    else None,
                )

            add_child_node_mutation = AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=parent_uid,
                new_node=node_to_add,
                copy=True,
            )
            calc_graph.apply_mutation(add_child_node_mutation)

        else:
            logger.debug(
                f"do not add child node because it already exists in the graph ({existing_node_in_graph}); "
                f"only add edge."
            )
            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=parent_uid,
                to_node_uid=existing_node_in_graph.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

    async def run(self, calc_graph: CalcGraph) -> None:
        logger.debug(f"start running gap-filling-module InventoryConnectorGfm on {self.node}")

        requested_impact_assessments = calc_graph.get_requested_impact_assessments()
        if not calc_graph.required_environmental_flows:
            for requested_impact_assessment in requested_impact_assessments:
                calc_graph.required_environmental_flows.update(
                    self.gfm_factory.required_environmental_flows_by_impact_assessment[requested_impact_assessment]
                )

        # check if we in general want to use system processes in this calculation
        calculation = calc_graph.calculation
        if calculation and calculation.use_system_processes:
            # check if this node has aggregated_cache
            if isinstance(self.node, ActivityNode) and self.node.aggregated_cache is not None:
                # check for which GFMs we need to load all relevant nodes
                gfms_for_which_to_load_all_relevant_nodes = calculation.gfms_for_which_to_load_all_relevant_nodes
                has_sub_nodes_that_can_change_dynamically_for_gfms = (
                    self.node.aggregated_cache.has_sub_nodes_that_can_change_dynamically_for_gfms
                )
                if all(
                    [
                        # check that each gfm in gfms_for_which_to_load_all_relevant_nodes is contained in
                        # the dictionary has_sub_nodes_that_can_change_dynamically_for_gfms
                        gfm in has_sub_nodes_that_can_change_dynamically_for_gfms
                        # and that it has no sub nodes that can change dynamically for this gfm
                        and not has_sub_nodes_that_can_change_dynamically_for_gfms[gfm]
                        for gfm in gfms_for_which_to_load_all_relevant_nodes
                    ]
                ):
                    logger.debug("found (cached) environmental_flows in aggregated_cache.")
                    if calculation.explicitly_attach_cached_elementary_resource_emission:
                        for biosphere_node_uid, value in self.node.aggregated_cache.flow_quantities.items():
                            if biosphere_node_uid in calc_graph.required_environmental_flows:
                                # now we create a flow node (with the flow amount) btw the self.node and the
                                # biosphere_node Construct model without validation for faster performance.
                                # Moreover, assume that the emission unit matches the elementary flow amount
                                # by writing into the "amount" field, with a generic emission unit.
                                flow_node = FlowNode.model_construct(
                                    uid=uuid.uuid4(),
                                    amount=QuantityProp.unvalidated_construct(
                                        value=value,
                                        unit_term_uid=self.gfm_factory.emission_unit_term.uid,
                                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                                    ),
                                )
                                calc_graph.apply_mutation(
                                    AddNodeMutation(
                                        created_by_module=self.__class__.__name__,
                                        parent_node_uid=self.node.uid,
                                        new_node=flow_node,
                                        copy=False,
                                    )
                                )
                                # finally, we connect the new flow node to the biosphere node
                                await self.add_node_mutation(
                                    calc_graph,
                                    biosphere_node_uid,
                                    flow_node.uid,
                                )
                    else:
                        aggregated_cache: EnvironmentalFlowsProp = self.node.aggregated_cache.duplicate()
                        aggregated_cache.implicitly_added_to_calc_graph = True
                        calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=self.node.uid,
                                prop_name="aggregated_cache",
                                prop=aggregated_cache,
                            )
                        )

                    return

        # if unit node: add its child nodes
        # we need to set max_depth to 1 for now,
        # otherwise we cannot use the system process cache if they are in intermediate depths
        max_depth = 1

        edges = await calc_graph.service_provider.node_service.get_sub_graph_by_uid(self.node.uid, max_depth=max_depth)

        for edge in edges:
            await self.add_node_mutation(calc_graph, edge.child_uid, edge.parent_uid)


class InventoryConnectorGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.unit_term_access_group_uid = root_unit_term.access_group_uid

        # For efficiency, we do not retrieve from the database specific units of the emissions. We simply
        # assume that the flow amount unit matches the units of the emissions.
        # To mark this assumption, we simply put a generic Emission units term.
        self.emission_unit_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_units_emission", self.unit_term_access_group_uid)
        ]
        self.required_environmental_flows_by_impact_assessment: dict[str, set] = {}

    async def init_cache(self) -> None:
        cached_impact_assessment_factors = await self.postgres_db.get_gfm_cache_mgr().get_prefill_cache_entries(
            "ImpactAssessmentGapFillingWorker",
            5000,
        )

        impact_assessment_term_ag = self.service_provider.glossary_service.root_subterms[
            "Root_Impact_Assessments"
        ].access_group_uid

        for cached_impact_assessment in cached_impact_assessment_factors:
            try:
                data_dict: dict = cached_impact_assessment["cache_data"]["data"]
            except KeyError:
                data_dict: dict = cached_impact_assessment["cache_data"]
            required_env_flows = set(
                self.service_provider.product_service.xid_to_uid_mappings.get(f"biosphere3_{key}")
                for key in data_dict.keys()
                if self.service_provider.product_service.xid_to_uid_mappings.get(f"biosphere3_{key}")
            )

            self.required_environmental_flows_by_impact_assessment[
                self.service_provider.glossary_service.terms_by_xid_ag_uid[
                    (cached_impact_assessment["cache_key"], impact_assessment_term_ag)
                ].name
            ] = required_env_flows

    def spawn_worker(self, node: Node) -> InventoryConnectorGapFillingWorker:
        return InventoryConnectorGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = InventoryConnectorGapFillingFactory
