"Merge Linked Nodes gap filling module."
import uuid
from typing import List

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from structlog import get_logger

from core.domain.nodes import FlowNode
from core.domain.nodes.activity.linking_activity_node import LinkingActivityNode
from core.domain.nodes.node import Node
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.legacy_api_information_prop import LegacyAPIInformationProp, LegacyIngredientTypeEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class MergeLinkedNodesGapFillingWorker(AbstractGapFillingWorker):
    "Merge linked Flow Nodes into one Flow Node."

    def __init__(self, node: Node):
        """Load child nodes of recipes, sub-recipes and ingredients."""
        super().__init__(node)

    def should_be_scheduled(self) -> bool:
        "Schedule on all FlowNodes that have a LinkingActivityNode as parent."
        if (
            isinstance(self.node, FlowNode)
            and len(self.node.get_parent_nodes()) == 1
            and isinstance(self.node.get_parent_nodes()[0], LinkingActivityNode)
        ):
            return True

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        "Make sure that we only run on the bottom LinkingActivityNode."
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(AddClientNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[MergeClient] wait for AddClientNodesGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule
        elif len(self.node.get_sub_nodes()) == 1 and isinstance(self.node.get_sub_nodes()[0], LinkingActivityNode):
            return GapFillingWorkerStatusEnum.cancel
        return GapFillingWorkerStatusEnum.ready

    def get_canceled_gfm_states(self, node: Node) -> dict[str, NodeGfmStateEnum]:
        old_gfm_states = node.gfm_state.worker_states
        new_gfm_states: dict[str, NodeGfmStateEnum] = {}
        for gfm_name, gfm_state in old_gfm_states.items():
            if gfm_state == NodeGfmStateEnum.scheduled and not (
                self.node.uid == node.uid and gfm_name == self.__class__.__name__
            ):
                new_gfm_states[gfm_name] = NodeGfmStateEnum.canceled.value
            else:  # keep the current state
                new_gfm_states[gfm_name] = gfm_state
        return new_gfm_states

    async def run(self, calc_graph: CalcGraph) -> None:
        "Run the worker."
        logger.debug(f"start running gap-filling-module MergeClientNodesGFM on {self.node}")

        # get all linked FoodProductFlowNodes:
        current_flow = self.node
        linked_flow_nodes: List[FlowNode] = [current_flow]
        nodes_to_remove: List[Node] = [current_flow]
        while len(current_flow.get_parent_nodes()) == 1 and isinstance(
            (parent_activity := current_flow.get_parent_nodes()[0]), LinkingActivityNode
        ):
            parent_flow = parent_activity.get_parent_nodes()[0]
            assert isinstance(parent_flow, type(current_flow)), "Linked FlowNodes must have the same type."
            linked_flow_nodes.append(parent_flow)
            nodes_to_remove.append(parent_activity)
            nodes_to_remove.append(parent_flow)
            current_flow = parent_flow

        # we store all properties of the new node in this dict
        all_props_of_new_node = {}

        # we store the original properties of the last linked_flow_node to be able to store the original values
        # in props_modified_by_merge if they have been overwritten by the merge
        original_props = linked_flow_nodes[-1].shallow_model_dump()
        props_modified_by_merge = {}

        # the default merging-strategy is that we try to take the properties from the parent-node,
        # Two exceptions to this rule are
        # 1) properties that we try to take from the child-node or
        # 2) properties that we enforce to always take from the most parent node (and if None we set them to None).
        properties_to_try_to_take_from_child_flow = ["original_node_uid"]
        properties_to_always_take_from_most_parent_flow = ["amount", "amount_in_original_source_unit"]

        # We want to take the identification (i.e. product_name and / or ingredients_declaration)
        # from the most child node
        most_child_flow_that_has_identification = None

        # for the following properties we have custom logic or we don't to set them at all
        properties_to_leave_unset = ["uid", "xid", "access_group_uid", "access_group_xid", "gfm_state"]
        for current_flow in linked_flow_nodes[::-1]:
            serialized_properies = current_flow.shallow_model_dump()
            for key, value in serialized_properies.items():
                if (
                    key not in properties_to_leave_unset
                    and (
                        key not in properties_to_always_take_from_most_parent_flow
                        or current_flow.uid == linked_flow_nodes[-1].uid
                    )
                    and (key not in all_props_of_new_node or key in properties_to_try_to_take_from_child_flow)
                ):
                    if key not in ("product_name", "ingredients_declaration"):
                        all_props_of_new_node[key] = value
                    elif value is not None:
                        most_child_flow_that_has_identification = current_flow

        # Take the identification (i.e. product_name and / or ingredients_declaration)
        # from the most child node that has at least one of the two set.
        # Also we make sure to only take the nutrient_values from exactly this node.
        if most_child_flow_that_has_identification:
            serialized_most_child_flow_that_has_identification = (
                most_child_flow_that_has_identification.shallow_model_dump()
            )
            all_props_of_new_node["product_name"] = serialized_most_child_flow_that_has_identification.get(
                "product_name"
            )
            all_props_of_new_node["ingredients_declaration"] = serialized_most_child_flow_that_has_identification.get(
                "ingredients_declaration"
            )
            all_props_of_new_node["nutrient_values"] = serialized_most_child_flow_that_has_identification.get(
                "nutrient_values"
            )

        # force the link_to_sub_node to be taken from the last linked_flow_node
        all_props_of_new_node["link_to_sub_node"] = linked_flow_nodes[0].link_to_sub_node

        # check which original values need to be stored the original in the legacyAPIInformation-field
        # because they have been overwritten by the merge
        for new_key, new_value in all_props_of_new_node.items():
            if new_key in original_props and new_value != original_props[new_key]:
                props_modified_by_merge[new_key] = original_props[new_key]

        merged_flow = current_flow.__class__.model_construct(**all_props_of_new_node)
        merged_flow.uid = uuid.uuid4()
        merged_flow.remove_private_attributes()
        merged_flow.set_owner_node_for_props()

        # get parent of the last FlowNode in the chain:
        parent_flow = linked_flow_nodes[-1]
        assert len(parent_activities := parent_flow.get_parent_nodes()) == 1
        parent_activity = parent_activities[0]

        # get the child of the first FlowNode in the chain:
        child_flow = linked_flow_nodes[0]
        child_activity = None
        if len(child_activities := child_flow.get_sub_nodes()) == 1:
            child_activity = child_activities[0]

        # 1) remove edge between the parent_activity and the parent_flow
        remove_edge_mutation = RemoveEdgeMutation(
            created_by_module=self.__class__.__name__,
            from_node_uid=parent_activity.uid,
            to_node_uid=parent_flow.uid,
        )
        calc_graph.apply_mutation(remove_edge_mutation)

        # 2) remove edge between the child_flow and the child_activity
        if child_activity:
            remove_edge_mutation = RemoveEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=child_flow.uid,
                to_node_uid=child_activity.uid,
            )
            calc_graph.apply_mutation(remove_edge_mutation)

        # 3) cancel all gfms
        for node in nodes_to_remove:
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=node.uid,
                    prop_name="gfm_state",
                    prop=GfmStateProp(
                        worker_states=self.get_canceled_gfm_states(node),
                    ),
                )
            )

        # 4) Add the new merged_flow
        add_child_node_mutation = AddNodeMutation(
            created_by_module=self.__class__.__name__,
            parent_node_uid=parent_activity.uid,
            new_node=merged_flow,
            copy=False,
        )
        calc_graph.apply_mutation(add_child_node_mutation)

        old_legacy_api_information = merged_flow.legacy_API_information
        if isinstance(old_legacy_api_information, LegacyAPIInformationProp):
            updated_legacy_api_information = old_legacy_api_information.duplicate()
            assert isinstance(updated_legacy_api_information, LegacyAPIInformationProp)
            if not updated_legacy_api_information.properties_pre_gfms:
                updated_legacy_api_information.properties_pre_gfms = props_modified_by_merge
            else:
                update_properties_pre_gfms_dict = dict(updated_legacy_api_information.properties_pre_gfms)
                # TODO: I am not sure if this merging strategy is always the correct one
                update_properties_pre_gfms_dict.update(props_modified_by_merge)
                updated_legacy_api_information.properties_pre_gfms = update_properties_pre_gfms_dict
        else:
            updated_legacy_api_information = LegacyAPIInformationProp(properties_pre_gfms=props_modified_by_merge)

        prop_mutation = PropMutation(
            created_by_module=self.__class__.__name__,
            node_uid=merged_flow.uid,
            prop_name="legacy_API_information",
            prop=updated_legacy_api_information,
        )
        calc_graph.apply_mutation(prop_mutation)

        # 5) Add the edge between the merged_flow and the child_activity
        if child_activity:
            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=merged_flow.uid,
                to_node_uid=child_activity.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

            # 5a) Modify the type of child_activity to conceptual-ingredients:
            # If it has been linked through LinkingActivityNode, then we must have received
            # it as a conceptual-ingredient:
            if (
                child_activity.legacy_API_information
                and child_activity.legacy_API_information.type == LegacyIngredientTypeEnum.recipes
            ):
                child_activity_legacy_API_information = child_activity.legacy_API_information.duplicate()
                child_activity_legacy_API_information.type = LegacyIngredientTypeEnum.conceptual_ingredients
                calc_graph.apply_mutation(
                    PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=child_activity.uid,
                        prop_name="legacy_API_information",
                        prop=child_activity_legacy_API_information,
                    )
                )


class MergeLinkedNodesGapFillingFactory(AbstractGapFillingFactory):
    "Factory."

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)

    async def init_cache(self) -> None:
        "Do nothing."
        pass

    def spawn_worker(self, node: Node) -> MergeLinkedNodesGapFillingWorker:
        "Spawn worker."
        return MergeLinkedNodesGapFillingWorker(node)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = MergeLinkedNodesGapFillingFactory
