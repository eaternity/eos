"""Using the glossary_tags property to infer storage time."""

import copy

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.location_gfm import LocationGapFillingWorker
from gap_filling_modules.origin_gfm import OriginGapFillingWorker
from gap_filling_modules.perishability_gfm import PerishabilityGapFillingWorker
from structlog import get_logger

from core.domain.deep_mapping_view import DeepListView
from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.glossary_term_prop import GlossaryTermProp, SourceEnum
from core.domain.props.location_prop import LocationProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.domain.props.utils import flow_location_is_known
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class ConservationGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Whether to schedule ConservationGFM."""
        if not isinstance(self.node, FoodProductFlowNode):
            return False
        else:
            return True

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Whether ConservationGFM can be executed."""
        if self.node.is_subdivision:
            logger.debug("[Conservation] Skipping on subdivision nodes as they only exist for ingredient estimation.")
            return GapFillingWorkerStatusEnum.cancel

        global_gfm_state = self.get_global_gfm_state()
        if global_gfm_state.get(LocationGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for Location GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(OriginGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for Origin GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(PerishabilityGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for Perishability GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        """Execute conservation gap filling to add storage time."""
        perishability_type_term_xids = self.get_perishability_tag_xids(self.node)

        if len(perishability_type_term_xids) > 0:
            # take the first perishability, as we currently only support one perishability type per product
            perishability_type_term_xid = perishability_type_term_xids[0]

            storage_time = await self.add_missing_conservation_and_get_storage_time(
                perishability_type_term_xid,
                calc_graph,
            )

            prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="storage_time",
                prop=QuantityProp(
                    value=storage_time,
                    unit_term_uid=self.gfm_factory.hour_term.uid,
                    # set reference as self reference so that value is not scaled by the amount of product.
                    for_reference=ReferenceAmountEnum.self_reference,
                ),
            )
            calc_graph.apply_mutation(prop_mutation)

    def get_conservation_tag_xids(self, food_product: FoodProductFlowNode) -> list[str]:
        """Get the conservation method of the node."""
        tag_term_xids = food_product.tag_term_xids
        conservation_tags = []
        for xid in tag_term_xids:
            if xid in self.gfm_factory.storage_xid_to_term.keys():
                conservation_tags.append(xid)
        return conservation_tags

    def get_perishability_tag_xids(self, food_product: FoodProductFlowNode) -> list[str]:
        """Get the perishability type of the node."""
        tag_term_xids = food_product.tag_term_xids
        perishability_tags = []
        for xid in tag_term_xids:
            if xid in self.gfm_factory.perishability_term_xids:
                perishability_tags.append(xid)
        return perishability_tags

    def is_convenience_product(self, combined_product: FoodProductFlowNode) -> bool:
        """Checks if a food product flow node is a convenience product.

        We assume that a a combined product is a convenience product if it is not produced in the kitchen location.
        """

        def has_different_location_above_in_tree(current_node: Node, origin_location_prop: LocationProp) -> bool:
            parents_of_current_node = current_node.get_parent_nodes()
            if len(parents_of_current_node) == 0:
                return None
            if len(parents_of_current_node) > 1:
                raise TypeError("inheritance is not allowed for a node that is not in a tree.")
            parent_of_current_node = parents_of_current_node[0]
            assert isinstance(parent_of_current_node, Node)
            next_location, node_with_next_location = parent_of_current_node.get_prop_by_inheritance(
                "flow_location", ignore_if=(lambda prop: not flow_location_is_known(prop))
            )
            if not next_location:
                return False
            if not next_location or not isinstance(next_location, (list, DeepListView)) or len(next_location) != 1:
                return has_different_location_above_in_tree(node_with_next_location, origin_location_prop)
            next_location_prop = next_location[0]
            if origin_location_prop.exact_and_identical(next_location_prop):
                return has_different_location_above_in_tree(node_with_next_location, origin_location_prop)
            return True

        origin_location = combined_product.get_origin_flow_location()
        if not origin_location or not isinstance(origin_location, (list, DeepListView)) or len(origin_location) != 1:
            return False
        origin_location_prop = origin_location[0]
        if has_different_location_above_in_tree(combined_product, origin_location_prop):
            return True
        else:
            return False

    def is_sub_product_of_convenience_product(self, food_product: FoodProductFlowNode) -> bool:
        """Checks if a food product flow node is a sub(-sub(...))) product of a convenience product.

        We assume that a food-product is a (sub-) sub- etc. product of a convenience product if this product is not
        directly shipped to the kitchen-location of the root-node but there is at least one location in between
        that is different.
        """
        current_flow = food_product
        while True:
            parent_activities = current_flow.get_parent_nodes()
            if len(parent_activities) != 1:
                return False
            parent_activity = parent_activities[0]
            parent_flows = parent_activity.get_parent_nodes()
            if len(parent_flows) != 1:
                return False
            parent_flow = parent_flows[0]
            if not isinstance(parent_flow, FoodProductFlowNode):
                return False
            conservation_tag_xids = self.get_conservation_tag_xids(parent_flow)
            if self.gfm_factory.string_to_storage_term["not_conserved"].xid in conservation_tag_xids:
                return False
            if self.gfm_factory.string_to_storage_term["generic_conserved"].xid in conservation_tag_xids:
                return True
            if parent_flow.is_combined_product():
                # it could be that the parent is a convenience product, but it was not yet set to generic conserved
                if self.is_convenience_product(parent_flow):
                    return True
                else:
                    return False
            current_flow = parent_flow

    async def add_missing_conservation_and_get_storage_time(
        self, perishability_type_term_xid: str, calc_graph: CalcGraph
    ) -> int:
        """Define the conservation method if not yet defined and compute storage time."""
        # check whether this product is a convenience product
        assert isinstance(self.node, FoodProductFlowNode)

        storage_regime_term_xids = []

        already_specified_storage_regime_term_xids = self.get_conservation_tag_xids(self.node)
        storage_regime_term_xids = copy.deepcopy(already_specified_storage_regime_term_xids)
        storage_time_options = self.get_storage_time_options(perishability_type_term_xid)

        # Fish without specified origin are assumed to be transported roughly 14_000 km by sea.
        # This implies that the fish has to be frozen or set to another conservation method
        # that has even longer storage time (e.g. dried or canned). This is a temporary solution
        # until we have an improved origin-model for fish.
        if self.node.origin_data and (fao_code := self.node.origin_data.fao_code):
            fao_id = fao_code.get_term().xid
            if fao_id == self.gfm_factory.unknown_fish_production_fao_code_term.xid:
                storage_time = max(
                    [
                        storage_time_options.get(storage_regime_term_xid)
                        for storage_regime_term_xid in storage_regime_term_xids
                    ],
                    default=0,
                )
                if storage_time < storage_time_options.get(self.gfm_factory.string_to_storage_term["frozen"].xid):
                    storage_regime_term_xids.append(self.gfm_factory.string_to_storage_term["frozen"].xid)

        if not storage_regime_term_xids and self.node.is_dried():
            storage_regime_term_xids.append(self.gfm_factory.string_to_storage_term["generic_conserved"].xid)

        # if the product has no conservation specified and is either a convenience product or a (sub-(sub-...)) product
        # of a convenience product, we presume it is generic conserved
        if not storage_regime_term_xids:
            if self.node.is_combined_product():
                if self.is_convenience_product(self.node):
                    # it is a convenience product and therefore we assume it is generic conserved
                    storage_regime_term_xids.append(self.gfm_factory.string_to_storage_term["generic_conserved"].xid)
            elif self.is_sub_product_of_convenience_product(self.node):
                # it is a sub-product of a convenience product and therefore we assume it is generic conserved
                storage_regime_term_xids.append(self.gfm_factory.string_to_storage_term["generic_conserved"].xid)

        # If the product is (highly-) perishable, add cooling if the client did not specify any conservation
        # or specified the product to be "not conserved"
        if (
            (
                not storage_regime_term_xids
                or storage_regime_term_xids == [self.gfm_factory.string_to_storage_term["not_conserved"].xid]
            )
            and perishability_type_term_xid != self.gfm_factory.shelf_stable_term.xid
            and len(self.node.get_parent_nodes()) > 0  # do not add default conservation to the root node
        ):
            storage_regime_term_xids.append(self.gfm_factory.string_to_storage_term["cooled"].xid)

        if not storage_regime_term_xids:
            storage_regime_term_xids.append(self.gfm_factory.string_to_storage_term["not_conserved"].xid)

        # add the conservation tag to the node
        for storage_regime_term_xid in storage_regime_term_xids:
            if storage_regime_term_xid not in already_specified_storage_regime_term_xids:
                storage_term = self.gfm_factory.storage_xid_to_term[storage_regime_term_xid]
                prop_mutation = PropListMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=self.node.uid,
                    prop_name="glossary_tags",
                    props=[
                        GlossaryTermProp.unvalidated_construct(
                            term_uid=storage_term.uid,
                            source=SourceEnum.eos_assumed,
                        )
                    ],
                    append=True,
                )
                calc_graph.apply_mutation(prop_mutation)

        # get the maximum storage time of the specified storage regimes
        storage_time = max(
            [storage_time_options.get(storage_regime_term_xid) for storage_regime_term_xid in storage_regime_term_xids]
        )

        return storage_time

    def get_storage_time_options(self, perishability_type: str) -> dict[str, int]:
        """Hard coded storage time options according to the perishability type."""
        # TODO: (not urgent) load this data from a side source
        storage_time_options = {
            self.gfm_factory.shelf_stable_term.xid: {
                self.gfm_factory.string_to_storage_term["generic_conserved"].xid: 24 * 365,
                self.gfm_factory.string_to_storage_term["not_conserved"].xid: 24 * 365,
                self.gfm_factory.string_to_storage_term["cooled"].xid: 24 * 365,
                self.gfm_factory.string_to_storage_term["frozen"].xid: 24 * 365,
                self.gfm_factory.string_to_storage_term["canned"].xid: 24 * 365 * 4,
                self.gfm_factory.string_to_storage_term["dried"].xid: 24 * 365,
            },
            self.gfm_factory.perishable_term.xid: {
                self.gfm_factory.string_to_storage_term["generic_conserved"].xid: 24 * 365,
                self.gfm_factory.string_to_storage_term["not_conserved"].xid: 24 * 60,
                self.gfm_factory.string_to_storage_term["cooled"].xid: 24 * 60,
                self.gfm_factory.string_to_storage_term["frozen"].xid: 24 * 365,
                self.gfm_factory.string_to_storage_term["canned"].xid: 24 * 365 * 4,
                self.gfm_factory.string_to_storage_term["dried"].xid: 24 * 365,
            },
            self.gfm_factory.high_perishable_term.xid: {
                self.gfm_factory.string_to_storage_term["generic_conserved"].xid: 24 * 365,
                self.gfm_factory.string_to_storage_term["not_conserved"].xid: 24 * 3,
                self.gfm_factory.string_to_storage_term["cooled"].xid: 24 * 7,
                self.gfm_factory.string_to_storage_term["frozen"].xid: 24 * 60,
                self.gfm_factory.string_to_storage_term["canned"].xid: 24 * 365 * 4,
                self.gfm_factory.string_to_storage_term["dried"].xid: 24 * 365,
            },
        }

        return storage_time_options.get(perishability_type)


class ConservationGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)

        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", root_unit_term.access_group_uid)
        ]
        self.hour_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_hour", root_unit_term.access_group_uid)
        ]

        root_perishability_term = self.service_provider.glossary_service.root_subterms.get("EOS_Perishability")
        root_fao_term = self.service_provider.glossary_service.root_subterms.get("Root_FAO")
        self.shelf_stable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_STABLE", root_perishability_term.access_group_uid)
        ]
        assert self.shelf_stable_term, "Could not find 'shelf-stable' perishability term."

        self.perishable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_PERISHABLE", root_perishability_term.access_group_uid)
        ]
        assert self.perishable_term, "Could not find 'perishable' perishability term."

        self.high_perishable_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_HIGH-PERISHABLE", root_perishability_term.access_group_uid)
        ]
        assert self.high_perishable_term, "Could not find 'perishable' perishability term."

        self.unknown_fish_production_fao_code_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("300000", root_fao_term.access_group_uid)
        ]

        self.string_to_storage_term: dict[str, Term] = {}
        self.storage_xid_to_term: dict[str, Term] = {}
        self.perishability_terms = [self.shelf_stable_term, self.perishable_term, self.high_perishable_term]
        self.perishability_term_xids = set(term.xid for term in self.perishability_terms)

        root_combined_product_mono_product_term = self.service_provider.glossary_service.root_subterms.get(
            "EOS_Combined_Product_Mono_Product"
        )
        self.combined_product_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_COMBINED_PRODUCT", root_combined_product_mono_product_term.access_group_uid)
        ]
        assert self.combined_product_term, "Could not find 'combined product' term."
        self.mono_product_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_MONO_PRODUCT", root_combined_product_mono_product_term.access_group_uid)
        ]
        assert self.mono_product_term, "Could not find 'mono product' term."

    async def init_cache(self) -> None:
        """Initialize cache for conservation gap filling factory."""
        foodex2_access_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )

        gs = self.service_provider.glossary_service
        self.string_to_storage_term = {
            "generic_conserved": gs.terms_by_xid_ag_uid[("J0001", foodex2_access_group_uid)],
            "not_conserved": gs.terms_by_xid_ag_uid[("J0003", foodex2_access_group_uid)],
            "cooled": gs.terms_by_xid_ag_uid[("J0131", foodex2_access_group_uid)],
            "frozen": gs.terms_by_xid_ag_uid[("J0136", foodex2_access_group_uid)],
            "canned": gs.terms_by_xid_ag_uid[("J0111", foodex2_access_group_uid)],
            "dried": gs.terms_by_xid_ag_uid[("J0116", foodex2_access_group_uid)],
        }
        self.storage_xid_to_term = {term.xid: term for term in self.string_to_storage_term.values()}

    def spawn_worker(self, node: Node) -> ConservationGapFillingWorker:
        """Spawn conservation gap filling worker."""
        return ConservationGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = ConservationGapFillingFactory
