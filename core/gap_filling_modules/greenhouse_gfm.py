"""Greenhouse gap filling module."""
import copy
import os
import uuid
from datetime import datetime, timedelta
from io import FileIO
from typing import Tuple

import google
import numpy as np
import pandas as pd
from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingWorker
from gap_filling_modules.conservation_gfm import ConservationGapFillingWorker
from gap_filling_modules.greenhouse_util.greenhouse_gfm_param import (
    AREA_OF_THE_WINDOWS_EXPOSED_TO_THE_SUN,
    AVG_YIELD_PER_M2_PER_MONTH,
    BW_XID_FOR_GLASS,
    BW_XID_FOR_PLASTIC,
    FRACTION_OF_GLASS_IN_BUILDING_MATERIAL,
    FRACTION_OF_PLASTIC_IN_BUILDING_MATERIAL,
    HEATING_MIX_BW_ID,
    NUMBER_OF_GROWING_DAYS,
    PRODUCT_NAMES_WITH_GREENHOUSE_MODEL,
    Q_AIR_FACTOR,
    Q_SOLAR_FACTOR,
    Q_TRANS_FACTOR,
    REQUIRED_ELECTRICITY_KWH_PER_KG,
    REQUIRED_INSIDE_TEMPERATURE,
    days_in_each_month,
)
from gap_filling_modules.location_util.countries import get_inherited_country_code, iso_3166_map_3_to_2_letter
from gap_filling_modules.origin_gfm import OriginGapFillingWorker
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from googleapiclient.discovery import Resource, build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from pydantic_settings import SettingsConfigDict
from structlog import get_logger

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode, ModeledActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props import NamesProp, QuantityProp
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.utils import has_one_of_predefined_tags
from core.domain.term import Term
from core.envprops import EnvProps
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb
from database.postgres.settings import PgSettings

logger = get_logger()


class GreenhouseGfmSettings(EnvProps):
    """Settings for greenhouse gap filling module."""

    TEMP_DIR: str = "./temp_data"
    METEONORM_TEMPERATURE_FILE_NAME: str = "temperatures.csv"
    METEONORM_SOLAR_RADIATION_FILE_NAME: str = "solar_radiations.csv"

    GDRIVE_SERVICE_ACCOUNT_FILE: str = "./secrets/service_account.json"
    GDRIVE_METEONORM_FOLDER_ID: str = "1fiHIQImNuN2pd0TXRbncfTsaQgo-LCs7"

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


class GreenhouseGapFillingWorker(AbstractGapFillingWorker):
    """Greenhouse gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "GreenhouseGapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Whether to schedule greenhouse gap filling worker."""
        # Schedule on Brightway nodes with a parent that has a product_name
        if (
            isinstance(self.node, ModeledActivityNode)
            and self.node.get_parent_nodes()
            and any(
                isinstance(parent_node, FoodProductFlowNode) and parent_node.product_name
                for parent_node in self.node.get_parent_nodes()
            )
        ):
            return True
        else:
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Whether greenhouse gap filling worker can be executed."""
        # Not None product_names need to be matched
        if not all(
            isinstance(parent_node.product_name, NamesProp) or parent_node.product_name is None
            for parent_node in self.node.get_parent_nodes()
        ):
            logger.debug(f"[Greenhouse] Not all parent nodes of {self.node} are matched --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        # The OriginGFM has to be finished
        if self.get_global_gfm_state().get(OriginGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Greenhouse] wait for OriginGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # The following block exists to avoid adding greenhouse activity to two Brightway nodes, before
        # and after transport. In transport mode decision GFM, the Brightway Node sometimes gets Duplicated.
        if any(isinstance(sub_node, FoodProductFlowNode) for sub_node in self.node.get_sub_nodes()):
            logger.debug("[Greenhouse] should be scheduled only on the leaf Brightway node (at the origin) --> cancel.")
            return GapFillingWorkerStatusEnum.cancel

        if (
            self.get_global_gfm_state().get(AttachFoodTagsGapFillingWorker.__name__, 0)
            == NodeGfmStateEnum.scheduled.value
        ):
            logger.debug("[Greenhouse] wait for AttachFoodTagsGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if (
            self.get_global_gfm_state().get(ConservationGapFillingWorker.__name__, 0)
            == NodeGfmStateEnum.scheduled.value
        ):
            logger.debug("[Greenhouse] wait for ConservationGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # check that any parent is matched to any xid that we want to run the greenhouse model on
        if any(
            parent_node.product_name
            and any(
                term.get_term().xid in PRODUCT_NAMES_WITH_GREENHOUSE_MODEL for term in parent_node.product_name.terms
            )
            for parent_node in self.node.get_parent_nodes()
        ):
            return GapFillingWorkerStatusEnum.ready
        else:
            return GapFillingWorkerStatusEnum.cancel

    @staticmethod
    def modeled_or_emission_node_from_cache(node_from_cache: Node) -> Node:
        return node_from_cache.__class__.model_construct(
            uid=node_from_cache.uid,
            access_group_uid=node_from_cache.access_group_uid,
            production_amount=copy.copy(node_from_cache.production_amount),
            activity_location=node_from_cache.activity_location,
            aggregated_cache=node_from_cache.aggregated_cache
            if getattr(node_from_cache, "aggregated_cache", None)
            else None,
        )

    async def account_for_electrical_energy(
        self,
        calc_graph: CalcGraph,
        greenhouse_activity: FoodProcessingActivityNode,
        country_code: str,
        vegetable_name: str,
    ) -> None:
        """Account for electrical energy consumption of greenhouse."""
        energy_needed_per_kg = REQUIRED_ELECTRICITY_KWH_PER_KG[vegetable_name]
        electricity_consumption_flow = FlowNode(
            uid=uuid.uuid4(),
            product_name=NamesProp.unvalidated_construct(
                terms=[
                    GlossaryTermProp.unvalidated_construct(
                        term_uid=self.gfm_factory.string_to_production_term["greenhouse"].uid
                    )
                ]
            ),  # Needed for sheet converter to output Greenhouse as FlowNode name.
            amount=QuantityProp(
                value=energy_needed_per_kg * self.node.production_amount.value,
                unit_term_uid=self.gfm_factory.kWh_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
        add_child_node_mutation = AddNodeMutation(
            created_by_module=self.__class__.__name__,
            parent_node_uid=greenhouse_activity.uid,
            new_node=electricity_consumption_flow,
            copy=False,
        )
        calc_graph.apply_mutation(add_child_node_mutation)
        electrical_energy_node_from_cache = self.gfm_factory.electricity_markets_cache.get(country_code)
        if not electrical_energy_node_from_cache:
            raise KeyError(f"Electricity market mix for {country_code} is unavailable.")

        is_electrical_energy_node_in_graph = bool(calc_graph.get_node_by_uid(electrical_energy_node_from_cache.uid))

        if not is_electrical_energy_node_in_graph:
            electrical_energy_node = self.modeled_or_emission_node_from_cache(electrical_energy_node_from_cache)
            electrical_energy_node.production_amount = QuantityProp(
                value=(
                    electrical_energy_node.production_amount.value
                    if electrical_energy_node.production_amount.value
                    else 1.0
                ),
                unit_term_uid=self.gfm_factory.kWh_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            )
            add_child_node_mutation = AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=electricity_consumption_flow.uid,
                new_node=electrical_energy_node,
                copy=False,
            )
            calc_graph.apply_mutation(add_child_node_mutation)
        else:
            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=electricity_consumption_flow.uid,
                to_node_uid=electrical_energy_node_from_cache.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

    async def account_for_infrastructure(
        self, calc_graph: CalcGraph, greenhouse_activity: FoodProcessingActivityNode, vegetable_name: str
    ) -> None:
        """Account for infrastructure of greenhouse."""
        # The Brightway node will tell us how much kgCO2eq are emitted per m2 of building material per year.
        # Therefore, the flow_amount will be the amount of building material per m2 per year necessary for the
        # given production_amount of the crop.
        size_of_greenhouse_in_m2 = AREA_OF_THE_WINDOWS_EXPOSED_TO_THE_SUN
        average_yield_in_kg_per_m2_and_month = AVG_YIELD_PER_M2_PER_MONTH[vegetable_name]
        total_yield_per_year = average_yield_in_kg_per_m2_and_month * 12 * size_of_greenhouse_in_m2

        area_and_time_of_greenhouse_building_material_necessary_to_produce_given_amount_of_crop = (
            size_of_greenhouse_in_m2 / total_yield_per_year * self.node.production_amount.value
        )
        area_and_time_of_greenhouse_glass_building_material_necessary_to_produce_given_amount_of_crop = (
            area_and_time_of_greenhouse_building_material_necessary_to_produce_given_amount_of_crop
            * FRACTION_OF_GLASS_IN_BUILDING_MATERIAL
        )
        area_and_time_of_greenhouse_plastic_building_material_necessary_to_produce_given_amount_of_crop = (
            area_and_time_of_greenhouse_building_material_necessary_to_produce_given_amount_of_crop
            * FRACTION_OF_PLASTIC_IN_BUILDING_MATERIAL
        )

        # add Flow to the brightway nodes for the building materials
        is_greenhouse_glass_node_in_graph = bool(calc_graph.get_node_by_uid(self.gfm_factory.greenhouse_glass_node.uid))
        is_greenhouse_plastic_node_in_graph = bool(
            calc_graph.get_node_by_uid(self.gfm_factory.greenhouse_plastic_node.uid)
        )

        # 1) Glass
        glass_consumption_flow = FlowNode(
            uid=uuid.uuid4(),
            product_name=NamesProp.unvalidated_construct(
                terms=[
                    GlossaryTermProp.unvalidated_construct(
                        term_uid=self.gfm_factory.string_to_production_term["greenhouse"].uid
                    )
                ]
            ),  # Needed for sheet converter to output Greenhouse as FlowNode name.
            amount=QuantityProp(
                value=area_and_time_of_greenhouse_glass_building_material_necessary_to_produce_given_amount_of_crop,
                unit_term_uid=self.gfm_factory.m2_year_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
        add_child_node_mutation = AddNodeMutation(
            created_by_module=self.__class__.__name__,
            parent_node_uid=greenhouse_activity.uid,
            new_node=glass_consumption_flow,
            copy=False,
        )
        calc_graph.apply_mutation(add_child_node_mutation)

        if not is_greenhouse_glass_node_in_graph:
            add_child_node_mutation = AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=glass_consumption_flow.uid,
                new_node=self.modeled_or_emission_node_from_cache(self.gfm_factory.greenhouse_glass_node),
                copy=False,
            )
            calc_graph.apply_mutation(add_child_node_mutation)
        else:
            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=glass_consumption_flow.uid,
                to_node_uid=self.gfm_factory.greenhouse_glass_node.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

        # 2) Plastic
        plastic_consumption_flow = FlowNode(
            uid=uuid.uuid4(),
            product_name=NamesProp.unvalidated_construct(
                terms=[
                    GlossaryTermProp.unvalidated_construct(
                        term_uid=self.gfm_factory.string_to_production_term["greenhouse"].uid
                    )
                ]
            ),  # Needed for sheet converter to output Greenhouse as FlowNode name.
            amount=QuantityProp(
                value=area_and_time_of_greenhouse_plastic_building_material_necessary_to_produce_given_amount_of_crop,
                unit_term_uid=self.gfm_factory.m2_year_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
        add_child_node_mutation = AddNodeMutation(
            created_by_module=self.__class__.__name__,
            parent_node_uid=greenhouse_activity.uid,
            new_node=plastic_consumption_flow,
            copy=False,
        )
        calc_graph.apply_mutation(add_child_node_mutation)

        if not is_greenhouse_plastic_node_in_graph:
            add_child_node_mutation = AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=plastic_consumption_flow.uid,
                new_node=self.modeled_or_emission_node_from_cache(self.gfm_factory.greenhouse_plastic_node),
                copy=False,
            )
            calc_graph.apply_mutation(add_child_node_mutation)
        else:
            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=plastic_consumption_flow.uid,
                to_node_uid=self.gfm_factory.greenhouse_plastic_node.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

    def compute_required_heating(
        self,
        flow_country_code: str,
        flow_processing_date: datetime,
        vegetable_name: str,
    ) -> float:
        """Compute the necessary amount of heating."""
        days_between_harvest_and_production_date = 3

        harvest_date = flow_processing_date - timedelta(days=days_between_harvest_and_production_date)

        monthly_growing_days = np.array(days_in_each_month(NUMBER_OF_GROWING_DAYS[vegetable_name], harvest_date))
        monthly_avg_outside_temp = np.array(
            self.gfm_factory.aggregated_meteonorm_data[flow_country_code]["average_temperature"]
        )
        monthly_avg_solar_radiation = np.array(
            self.gfm_factory.aggregated_meteonorm_data[flow_country_code]["average_solar_radiation"]
        )

        monthly_temp_difference = REQUIRED_INSIDE_TEMPERATURE[vegetable_name] - monthly_avg_outside_temp
        monthly_required_heating_power_watts = (
            Q_TRANS_FACTOR + Q_AIR_FACTOR
        ) * monthly_temp_difference - Q_SOLAR_FACTOR * monthly_avg_solar_radiation
        monthly_required_heating_power_watts = np.array(
            [val if val >= 0.0 else 0.0 for val in monthly_required_heating_power_watts]
        )

        monthly_required_heating_energy_wh = monthly_required_heating_power_watts * monthly_growing_days * 24
        monthly_required_heating_energy_kwh = monthly_required_heating_energy_wh / 1000

        average_yield_kg_per_day = (
            AVG_YIELD_PER_M2_PER_MONTH[vegetable_name] * AREA_OF_THE_WINDOWS_EXPOSED_TO_THE_SUN * 12 / 365
        )

        assert sum(monthly_growing_days) == NUMBER_OF_GROWING_DAYS[vegetable_name]
        yield_in_growing_days_kg = average_yield_kg_per_day * sum(monthly_growing_days)

        mj_in_kwh = 3.6
        monthly_required_heating_energy_mj = monthly_required_heating_energy_kwh * mj_in_kwh

        required_heating_energy_mj_for_kg_of_vegetable = (
            np.sum(monthly_required_heating_energy_mj) / yield_in_growing_days_kg
        )

        return required_heating_energy_mj_for_kg_of_vegetable

    async def account_for_heating(
        self,
        calc_graph: CalcGraph,
        greenhouse_activity: FoodProcessingActivityNode,
        flow_country_code: str,
        required_heating_energy_mj_for_kg_of_vegetable: float,
    ) -> None:
        """Account for heating of greenhouse."""
        production_amount_in_kg = UnitWeightConverter.convert_between_same_unit_types(
            greenhouse_activity.production_amount.value,
            greenhouse_activity.production_amount.get_unit_term(),
            self.gfm_factory.kilogram_term,
            "mass-in-g",
        )
        required_heating_energy_mj_for_production_amount_of_vegetable = (
            production_amount_in_kg * required_heating_energy_mj_for_kg_of_vegetable
        )
        if flow_country_code in HEATING_MIX_BW_ID:
            heating_mix_country_code = flow_country_code
        else:
            heating_mix_country_code = "EU other"

        if heating_mix_country_code not in self.gfm_factory.heating_mix_node:
            raise KeyError(f"Heating mix for {heating_mix_country_code} is unavailable.")

        is_heating_node_in_graph = bool(
            calc_graph.get_node_by_uid(self.gfm_factory.heating_mix_node[heating_mix_country_code].uid)
        )
        heating_flow_uid = uuid.uuid4()
        calc_graph.apply_mutation(
            AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=greenhouse_activity.uid,
                new_node=FlowNode(
                    uid=heating_flow_uid,
                    product_name=NamesProp.unvalidated_construct(
                        terms=[
                            GlossaryTermProp.unvalidated_construct(
                                term_uid=self.gfm_factory.string_to_production_term["greenhouse"].uid
                            )
                        ]
                    ),  # Needed for sheet converter to output Greenhouse as FlowNode name.
                    amount=QuantityProp(
                        value=required_heating_energy_mj_for_production_amount_of_vegetable,
                        unit_term_uid=self.gfm_factory.mj_term.uid,
                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    ),
                ),
                copy=False,
            )
        )

        if not is_heating_node_in_graph:
            calc_graph.apply_mutation(
                AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=heating_flow_uid,
                    new_node=self.modeled_or_emission_node_from_cache(
                        self.gfm_factory.heating_mix_node[heating_mix_country_code]
                    ),
                    copy=False,
                )
            )
        else:
            calc_graph.apply_mutation(
                AddEdgeMutation(
                    created_by_module=self.__class__.__name__,
                    from_node_uid=heating_flow_uid,
                    to_node_uid=self.gfm_factory.heating_mix_node[heating_mix_country_code].uid,
                )
            )

    async def run(self, calc_graph: CalcGraph) -> None:
        """Execute greenhouse gap filling worker."""
        parent_flows = self.node.get_parent_nodes()

        for parent_flow in parent_flows:
            if not isinstance(parent_flow, FoodProductFlowNode):
                logger.debug(f"[Greenhouse] Parent node {parent_flow} is not a FoodProductFlowNode --> continue.")
                continue
            if not parent_flow.product_name:
                logger.debug(f"[Greenhouse] Parent node {parent_flow} has no product_name --> continue.")
                continue

            non_subdivision_node = parent_flow
            if parent_flow.is_subdivision:
                while (
                    non_subdivision_node.is_subdivision
                    and non_subdivision_node.get_parent_nodes()
                    and non_subdivision_node.get_parent_nodes()[0].get_parent_nodes()
                ):
                    non_subdivision_node = non_subdivision_node.get_parent_nodes()[0].get_parent_nodes()[0]

            all_conservation_terms = self.gfm_factory.all_conservation_term_xids
            (
                conservation_terms,
                conservation_term_containing_node,
            ) = non_subdivision_node.get_prop_by_inheritance(
                "tag_term_xids",
                ignore_if=(
                    lambda prop_name, tag_term_xids_to_search=all_conservation_terms: not has_one_of_predefined_tags(
                        prop_name, tag_term_xids_to_search
                    )
                ),
            )

            # Find if blacklisted terms exist in one of the parent nodes which is the same food product:
            # i.e., the same product_name.
            if conservation_terms:
                intersection_with_blacklist = self.gfm_factory.greenhouse_gfm_blacklist_term_xids.intersection(
                    conservation_terms
                )
            else:
                intersection_with_blacklist = None

            if (
                intersection_with_blacklist
                and conservation_term_containing_node.product_name == non_subdivision_node.product_name
            ):
                logger.debug(
                    f"[Greenhouse] Parent node {parent_flow} has" f" {intersection_with_blacklist} tag --> continue."
                )
                continue

            matched_names = set(
                [
                    PRODUCT_NAMES_WITH_GREENHOUSE_MODEL[term.get_term().xid]
                    for term in parent_flow.product_name.terms
                    if term.get_term().xid in PRODUCT_NAMES_WITH_GREENHOUSE_MODEL
                ]
            )
            if len(matched_names) == 0:
                continue
            if len(matched_names) > 1:
                warning_msg = (
                    f"Multiple product names matched to names for which the greenhouse model should run: "
                    f"{matched_names}. Only one should be matched."
                )
                logger.warn(warning_msg)
                calc_graph.append_data_errors_log_entry(warning_msg)
                continue
            vegetable_name = list(matched_names)[0]

            parent_flow_product_name_terms = [prop.get_term() for prop in parent_flow.product_name.terms]
            transport_term = False
            for product_name_term in parent_flow_product_name_terms:
                if product_name_term.sub_class_of == self.gfm_factory.root_transport_term.uid:
                    transport_term = True  # TODO, change transport flow nodes into TransportFlowNode.
            if transport_term:
                continue
            flow_country_code = get_inherited_country_code(parent_flow)
            if flow_country_code and len(flow_country_code) == 3:
                flow_country_code = iso_3166_map_3_to_2_letter[flow_country_code]

            if not flow_country_code:
                warning_msg = (
                    f"No location found for product {parent_flow_product_name_terms}." f"Cannot run greenhouse gfm."
                )
                logger.warn(warning_msg)
                calc_graph.append_data_errors_log_entry(warning_msg)
                continue

            if flow_country_code not in self.gfm_factory.aggregated_meteonorm_data:
                warning_msg = (
                    f"No meteo data available for {flow_country_code} "
                    f"(origin of {parent_flow_product_name_terms}). Cannot run greenhouse gfm."
                )
                logger.debug(warning_msg)
                continue

            # Add greenhouse activity
            # First compute the required amount of heating
            activity_date, _ = parent_flow.get_prop_by_inheritance("activity_date")
            if activity_date:
                try:
                    activity_date = datetime.strptime(activity_date, "%Y-%m-%d")
                except ValueError:
                    logger.error(f"Non-valid date {activity_date}. Use '%Y-%m-%d'. Using {datetime.now()} instead.")
                    activity_date = None

            if not activity_date:
                activity_date = datetime.now()
            required_heating_energy_mj_for_kg_of_vegetable = self.compute_required_heating(
                flow_country_code, activity_date, vegetable_name
            )

            # if no heating is required, we assume that the vegetable was not grown in a greenhouse
            if required_heating_energy_mj_for_kg_of_vegetable <= 0.0:
                logger.debug(f"[Greenhouse] No heating required for {vegetable_name} in {flow_country_code}.")
                continue

            # 1) remove edge between the Brightway node and parent flow node.
            remove_edge_mutation = RemoveEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=parent_flow.uid,
                to_node_uid=self.node.uid,
            )
            calc_graph.apply_mutation(remove_edge_mutation)

            # 2) greenhouse activity node
            greenhouse_activity = FoodProcessingActivityNode(
                uid=uuid.uuid4(),
                production_amount=self.node.production_amount,
            )

            # 3) re-establish edges between parent flow node and greenhouse activity node.
            add_child_node_mutation = AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=parent_flow.uid,
                new_node=greenhouse_activity,
                copy=False,
            )
            calc_graph.apply_mutation(add_child_node_mutation)

            # 4) add flow between Brightway node and greenhouse activity node
            food_flow_uid = uuid.uuid4()
            calc_graph.apply_mutation(
                DuplicateNodeMutation(
                    created_by_module=self.__class__.__name__,
                    new_node_uid=food_flow_uid,
                    source_node_uid=parent_flow.uid,
                    parent_node_uid=greenhouse_activity.uid,
                    gfms_to_not_schedule=(
                        "OriginGapFillingWorker",
                        "NutrientSubdivisionGapFillingWorker",
                        "TransportModeDistanceGapFillingWorker",
                    ),
                    duplicate_child_nodes=False,
                )
            )

            # Overwrite amount_in_original_source_unit and amount:
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=food_flow_uid,
                    prop_name="amount_in_original_source_unit",
                    prop=QuantityProp(
                        value=self.node.production_amount.value,
                        unit_term_uid=self.node.production_amount.unit_term_uid,
                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    ),
                )
            )
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=food_flow_uid,
                    prop_name="amount",
                    prop=QuantityProp(
                        value=self.node.production_amount.value,
                        unit_term_uid=self.node.production_amount.unit_term_uid,
                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    ),
                )
            )

            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=food_flow_uid,
                to_node_uid=self.node.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

            # 5) Attach the correct subgraphs to the greenhouse activity node.
            # All input flow amounts are for producing greenhouse_activity.production_amount
            # of agricultural produce.

            # 5.1) Heating Consumption
            await self.account_for_heating(
                calc_graph, greenhouse_activity, flow_country_code, required_heating_energy_mj_for_kg_of_vegetable
            )
            # 5.2) Electricity Consumption
            await self.account_for_electrical_energy(calc_graph, greenhouse_activity, flow_country_code, vegetable_name)
            # 5.3) Infrastructure
            await self.account_for_infrastructure(calc_graph, greenhouse_activity, vegetable_name)


class GreenhouseGapFillingFactory(AbstractGapFillingFactory):
    """Greenhouse gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.electricity_markets_cache: dict[str, Node] = {}
        self.aggregated_meteonorm_data: dict[str, dict[str, list[float]]] = {}
        self.root_transport_term = self.service_provider.glossary_service.root_subterms.get("EOS_Transportation")
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.root_transport_term = self.service_provider.glossary_service.root_subterms.get("EOS_Transportation")
        self.m2_year_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_square-meter-year", root_unit_term.access_group_uid)
        ]
        self.kWh_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilowatt-hour", root_unit_term.access_group_uid)
        ]
        self.mj_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_megajoule", root_unit_term.access_group_uid)
        ]
        self.kilogram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_kilogram", root_unit_term.access_group_uid)
        ]
        self.greenhouse_glass_node: ModeledActivityNode | None = None
        self.greenhouse_plastic_node: ModeledActivityNode | None = None
        self.heating_mix_node: dict[str, ModeledActivityNode] = {}
        self.string_to_production_term: dict[str, Term] = {}

        self.all_conservation_term_xids = frozenset(
            {
                "J0001",  # Generic Conserved
                "J0003",  # Not Conserved
                "J0131",  # Cooled
                "J0136",  # Frozen
                "J0111",  # Canned
                "J0116",  # Dried
            }
        )

        self.greenhouse_gfm_blacklist_term_xids = frozenset(
            {
                "J0001",  # Generic Conserved
                "J0136",  # Frozen
                "J0111",  # Canned
                "J0116",  # Dried
            }
        )

        self.greenhouse_gfm_settings = GreenhouseGfmSettings()
        self.gfm_dir = os.path.join(self.greenhouse_gfm_settings.TEMP_DIR, "greenhouse_gfm")
        os.makedirs(self.gfm_dir, exist_ok=True)

    async def init_cache(self) -> None:
        """Initialize cache required for greenhouse gap filling factory."""
        settings = PgSettings()

        electricity_market_low_voltage_cache = (
            await self.service_provider.postgres_db.gfm_cache_mgr.get_prefill_cache_entries(
                gfm_name="ElectricityMarketLowVoltage",
                limit=1000,  # Currently we have 172 electricity markets
            )
        )
        for cache_entry in electricity_market_low_voltage_cache:
            country_code = cache_entry["cache_key"]
            self.electricity_markets_cache[country_code] = await self.service_provider.node_service.find_by_uid(
                uuid.UUID(cache_entry["cache_data"])
            )
        logger.info(f"Loaded {len(self.electricity_markets_cache)} electricity markets from cache.")

        greenhouse_model_cache = await self.service_provider.postgres_db.gfm_cache_mgr.get_prefill_cache_entries(
            gfm_name="GreenhouseGapFillingWorker",
            limit=1000,  # Currently we 2*24 meteonorm countries
        )
        for identifier in ["average_temperature", "average_solar_radiation"]:
            meteonorm_identifier = f"meteonorm_{identifier}_"
            for cache_entry in greenhouse_model_cache:
                if cache_entry["cache_key"].startswith(meteonorm_identifier):
                    country_code = cache_entry["cache_key"].lstrip(meteonorm_identifier)
                    self.aggregated_meteonorm_data.setdefault(country_code, {})
                    self.aggregated_meteonorm_data[country_code][identifier] = cache_entry["cache_data"]
        logger.info(f"Loaded {len(self.aggregated_meteonorm_data)} meteonorm data points from cache.")

        greenhouse_glass_node_uid = await self.service_provider.postgres_db.product_mgr.find_uid_by_xid(
            xid=BW_XID_FOR_GLASS,
            namespace_uid=settings.EATERNITY_NAMESPACE_UUID,
        )
        self.greenhouse_glass_node = await self.service_provider.node_service.find_by_uid(greenhouse_glass_node_uid)
        if self.greenhouse_glass_node:
            self.greenhouse_glass_node.production_amount = QuantityProp(
                value=(
                    self.greenhouse_glass_node.production_amount.value
                    if self.greenhouse_glass_node.production_amount.value
                    else 1.0
                ),
                unit_term_uid=self.m2_year_term.uid,
                for_reference=ReferenceAmountEnum.self_reference,
            )
        greenhouse_plastic_node_uid = await self.service_provider.postgres_db.product_mgr.find_uid_by_xid(
            xid=BW_XID_FOR_PLASTIC,
            namespace_uid=settings.EATERNITY_NAMESPACE_UUID,
        )
        self.greenhouse_plastic_node = await self.service_provider.node_service.find_by_uid(greenhouse_plastic_node_uid)
        if self.greenhouse_plastic_node:
            self.greenhouse_plastic_node.production_amount = QuantityProp(
                value=(
                    self.greenhouse_plastic_node.production_amount.value
                    if self.greenhouse_plastic_node.production_amount.value
                    else 1.0
                ),
                unit_term_uid=self.m2_year_term.uid,
                for_reference=ReferenceAmountEnum.self_reference,
            )
        for key, val in HEATING_MIX_BW_ID.items():
            node_uid = await self.service_provider.postgres_db.product_mgr.find_uid_by_xid(
                xid=val, namespace_uid=settings.EATERNITY_NAMESPACE_UUID
            )
            if node_uid:
                heating_node = await self.service_provider.node_service.find_by_uid(node_uid)
                if heating_node:
                    heating_node.production_amount = QuantityProp(
                        value=heating_node.production_amount.value if heating_node.production_amount.value else 1.0,
                        unit_term_uid=self.mj_term.uid,
                        for_reference=ReferenceAmountEnum.self_reference,
                    )

                    assert isinstance(heating_node, ModeledActivityNode)
                    self.heating_mix_node[key] = heating_node

        eaternity_term_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            settings.EATERNITY_NAMESPACE_XID
        )
        foodex2_access_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )

        self.string_to_production_term = {
            "standard": self.service_provider.glossary_service.terms_by_xid_ag_uid[
                ("EOS_prod_standard", eaternity_term_group_uid)  # standard means without greenhouse
            ],
            "greenhouse": self.service_provider.glossary_service.terms_by_xid_ag_uid[
                ("EOS_prod_greenhouse", eaternity_term_group_uid)  # the default is to use the a greenhouse model
            ],
            "organic": self.service_provider.glossary_service.terms_by_xid_ag_uid[
                ("P0128", foodex2_access_group_uid)
            ],  # ATM we don't take into account differences between organic and conventional in the greenhouse GFM
        }

    async def import_data(self) -> None:
        """Import data for greenhouse gap filling factory."""
        greenhouse_model_cache = await self.service_provider.postgres_db.gfm_cache_mgr.get_prefill_cache_entries(
            gfm_name="GreenhouseGapFillingWorker",
            limit=1000,  # Currently we have 172 electricity markets and 2*24 meteonorm countries
        )
        existing_greenhouse_gfm_cache = {
            cache_entry["cache_key"]: cache_entry["cache_data"] for cache_entry in greenhouse_model_cache
        }
        for filename, identifier in zip(
            [
                self.greenhouse_gfm_settings.METEONORM_TEMPERATURE_FILE_NAME,
                self.greenhouse_gfm_settings.METEONORM_SOLAR_RADIATION_FILE_NAME,
            ],
            ["average_temperature", "average_solar_radiation"],
        ):
            file_path = os.path.join(self.gfm_dir, filename)

            if not os.path.exists(file_path):
                drive_service, drive_file_ids_by_name = self.get_drive_file_ids_by_name()
                if drive_file_ids_by_name:
                    self.download_file_from_google_drive(
                        drive_service,
                        filename,
                        drive_file_ids_by_name[filename],
                    )
                else:
                    continue
            df = pd.read_csv(file_path, index_col=0)
            meteonorm_dict = df.apply(lambda row: row.tolist(), axis=1).to_dict()
            for country_code, new_cache_value in meteonorm_dict.items():
                new_cache_key = f"meteonorm_{identifier}_{country_code}"
                if (
                    new_cache_key not in existing_greenhouse_gfm_cache
                    or new_cache_value != existing_greenhouse_gfm_cache[new_cache_key]
                ):
                    await self.service_provider.postgres_db.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
                        gfm_name="GreenhouseGapFillingWorker",
                        cache_key=new_cache_key,
                        cache_data=new_cache_value,
                        load_on_boot=True,
                    )
                    logger.info(f"imported meteonorm {identifier} for country code {country_code} into cache.")

    def download_file_from_google_drive(self, drive_service: Resource, filename: str, file_id: str) -> None:
        logger.info("download file from gdrive", filename=filename, id=file_id)

        save_file_path = os.path.join(self.gfm_dir, filename)

        request = drive_service.files().get_media(fileId=file_id)
        fh = FileIO(save_file_path, "wb")
        downloader = MediaIoBaseDownload(fh, request)

        done = False

        while not done:
            try:
                status, done = downloader.next_chunk()
            except Exception as e:
                fh.close()
                os.remove(save_file_path)
                raise Exception("Error downloading file from Google Drive!") from e

        logger.info("Download finished.", filename=filename, id=file_id)
        fh.close()
        request.http.close()

    def get_drive_file_ids_by_name(self) -> Tuple[Resource, dict]:
        """Helper function to get file ids from Google Drive."""
        if not os.path.isfile(self.greenhouse_gfm_settings.GDRIVE_SERVICE_ACCOUNT_FILE):
            logger.warning("Google Drive service account file not found. Falling back to local FAO files.")
            return None, None

        credentials, _ = google.auth.load_credentials_from_file(
            filename=self.greenhouse_gfm_settings.GDRIVE_SERVICE_ACCOUNT_FILE,
            scopes=[
                "https://www.googleapis.com/auth/devstorage.read_write",
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/drive",
            ],
        )

        try:
            drive_service = build("drive", "v3", credentials=credentials)
        except HttpError as error:
            logger.error(f"An error occurred: {error}")
            return None, None

        query = (
            f"'{self.greenhouse_gfm_settings.GDRIVE_METEONORM_FOLDER_ID}' in parents "
            f"and name = '{self.greenhouse_gfm_settings.METEONORM_TEMPERATURE_FILE_NAME}'"
            f"or name = '{self.greenhouse_gfm_settings.METEONORM_SOLAR_RADIATION_FILE_NAME}'"
        )
        drive_request_obj = drive_service.files().list(q=query, pageSize=4)
        results = drive_request_obj.execute()
        items = results.get("files", [])
        drive_file_ids_by_name = {item["name"]: item["id"] for item in items}

        # FIXME: not sure why this is necessary, but otherwise unit tests fail with warnings of unclosed SSL sessions:
        drive_request_obj.http.close()

        return drive_service, drive_file_ids_by_name

    def spawn_worker(self, node: Node) -> GreenhouseGapFillingWorker:
        """Spawn greenhouse gap filling worker."""
        return GreenhouseGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = GreenhouseGapFillingFactory
