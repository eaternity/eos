"""Processing with non-unit raw_material flow."""

import uuid

from gap_filling_modules.processing_models.process_with_bw_nodes import ProcessWithBWNode
from structlog import get_logger

from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum

logger = get_logger()


class ProcessWithNonUnitRawMaterial(ProcessWithBWNode):
    """Processing with non-unit raw_material flow."""

    process_name: str

    async def model_specific_formula(
        self,
        production_amount: QuantityProp,
        bw_nodes_and_amounts: dict[uuid.UUID, float],
        *_args: ...,
    ) -> tuple[QuantityProp, dict[uuid.UUID, QuantityProp]] | tuple[None, None]:
        """Formula for processing with non-unit raw_material flow."""
        return_dict = {}

        raw_material_amount = None
        for bw_node_uid, amount in bw_nodes_and_amounts.items():
            if bw_node_uid == self.raw_materials_uid:
                raw_material_amount = amount * production_amount.value
            else:
                unit_term = await self.get_unit_term_from_bw_node_uid(bw_node_uid)
                return_dict[bw_node_uid] = QuantityProp.unvalidated_construct(
                    value=amount * production_amount.value,
                    unit_term_uid=unit_term.uid,
                    for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                )

        assert raw_material_amount, f"{self.process_name} model requires a raw_material_amount."

        return (
            QuantityProp(
                value=raw_material_amount,
                unit_term_uid=production_amount.unit_term_uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
            return_dict,
        )
