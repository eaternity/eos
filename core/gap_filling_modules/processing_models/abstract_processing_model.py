"""Abstract class for implementing processing model."""

import uuid
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional, Union

from gap_filling_modules.location_util.countries import (
    get_inherited_country_code,
    iso_3166_map_2_to_3_letter,
    iso_3166_map_3_to_2_letter,
    regional_term_xid_to_region_gadm_codes_map,
)
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from structlog import get_logger

from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.props.quantity_prop import QuantityProp
from core.domain.term import Term
from core.service.service_provider import ServiceProvider

logger = get_logger()


@dataclass
class ElectricityNodeByCountry:
    """Stores node uid of Brightway electricity by country."""

    low_voltage: dict[str, uuid.UUID]
    medium_voltage: dict[str, uuid.UUID]


@dataclass
class ProcessingTagsAndXid:
    """Stores trigger tags for processing models, brightway node xid, and location."""

    tags: set[str]
    xid: str
    location: Optional[str]

    def model_dump(self) -> dict:
        """Serialize the model."""
        return {
            "tags": list(self.tags),
            "xid": self.xid,
            "location": self.location,
        }

    @classmethod
    def from_dict(
        cls: "ProcessingTagsAndXid", trigger_dict: dict[str, Union[str, set[str], Optional[str]]]
    ) -> "ProcessingTagsAndXid":
        """Deserialize the model."""
        return cls(
            tags=set(trigger_dict["tags"]),
            xid=trigger_dict["xid"],
            location=trigger_dict["location"],
        )


@dataclass
class ProcessingTagsAndElectricityAmount:
    """Stores trigger tags for processing models, electricity_amount, and location."""

    tags: set[str]
    electricity_amount: float
    location: Optional[str]

    def model_dump(self) -> dict:
        """Serialize the model."""
        return {
            "tags": list(self.tags),
            "electricity_amount": self.electricity_amount,
            "location": self.location,
        }

    @classmethod
    def from_dict(
        cls: "ProcessingTagsAndElectricityAmount", trigger_dict: dict[str, Union[str, set[str], Optional[str]]]
    ) -> "ProcessingTagsAndElectricityAmount":
        """Deserialize the model."""
        return cls(
            tags=set(trigger_dict["tags"]),
            electricity_amount=trigger_dict["electricity_amount"],
            location=trigger_dict["location"],
        )


class AbstractProcessingModel(ABC):
    """General implementation of processing model.

    Only the model specific formula and the logic to obtain brightway node and subgraph changes.
    """

    def __init__(
        self,
        process_name: str,
        process_specific_trigger_tags: list[Union[ProcessingTagsAndXid, ProcessingTagsAndElectricityAmount]],
        electricity_node_by_country: ElectricityNodeByCountry,
        raw_materials_uid: uuid.UUID,
        foodex2_access_group_uid: uuid.UUID,
        service_provider: ServiceProvider,
    ):
        self.process_name: str = process_name
        self.process_specific_trigger_tags = process_specific_trigger_tags
        self.electricity_node_by_country = electricity_node_by_country
        self.service_provider = service_provider
        self.raw_materials_uid = raw_materials_uid
        self.foodex2_access_group_uid = foodex2_access_group_uid

        self.processing_by_tag: dict[
            frozenset[str],
            dict[str, Union[ProcessingTagsAndXid, ProcessingTagsAndElectricityAmount]],
        ] = {}

    def is_triggered_by_tag_xids(self, tag_xids: set[str]) -> bool:
        """Check whether the model is trigger by a set of tag_xids."""
        if frozenset(tag_xids) in self.processing_by_tag:
            return True

        viable_processing = []
        for processing_xid_and_tags in self.process_specific_trigger_tags:
            if processing_xid_and_tags.tags.issubset(tag_xids):
                if not viable_processing:
                    viable_processing.append(processing_xid_and_tags)
                elif len(processing_xid_and_tags.tags) > len(viable_processing[0].tags):
                    # The processing with a higher number of tags is prioritized as they are more
                    # specific for this particular product.
                    viable_processing = [processing_xid_and_tags]
                elif len(processing_xid_and_tags.tags) == len(viable_processing[0].tags):
                    viable_processing.append(processing_xid_and_tags)
        if not viable_processing:
            return False
        else:
            self.processing_by_tag[frozenset(tag_xids)] = {elem.location: elem for elem in viable_processing}
            return True

    @staticmethod
    def get_country_and_reqional_code(parent_flow: FoodProductFlowNode) -> tuple[Optional[str], Optional[str]]:
        """Helper method to obtain the country and regional code from the parent flow."""
        country_code = get_inherited_country_code(parent_flow)
        if not country_code:
            regional_code = "Global"
        else:
            if len(country_code) == 2:
                country_code_2_letter = country_code
                country_code_3_letter = iso_3166_map_2_to_3_letter[country_code]
            elif len(country_code) == 3:
                country_code_3_letter = country_code
                country_code_2_letter = iso_3166_map_3_to_2_letter[country_code]
            else:
                raise ValueError(f"We cannot handle {country_code} since it is neither 2 nor 3 letters long.")
            regional_code = "Global"
            for key, val in regional_term_xid_to_region_gadm_codes_map.items():
                if country_code_3_letter in val:
                    regional_code = key
                    break
            # Standardize country code as the two letter version
            country_code = country_code_2_letter
        return country_code, regional_code

    async def compute_flow_amounts(
        self, tag_xids: set[str], parent_flow: FoodProductFlowNode, production_amount: QuantityProp
    ) -> tuple[QuantityProp, dict[uuid.UUID, QuantityProp], set[str]] | tuple[None, None, None]:
        """Skeleton function for computing the flow amounts and nodes to add."""
        processing_info_candidates: dict[
            str, Union[ProcessingTagsAndXid, ProcessingTagsAndElectricityAmount]
        ] = self.processing_by_tag[frozenset(tag_xids)]

        country_code, regional_code = self.get_country_and_reqional_code(parent_flow)

        processing_info: Union[
            ProcessingTagsAndXid, ProcessingTagsAndElectricityAmount
        ] = processing_info_candidates.get(regional_code, processing_info_candidates.get("Global"))
        if processing_info is None:
            try:
                processing_info = processing_info_candidates["unspecified"]
            except KeyError as e:
                f"Neither Global nor unspecified exists as the location of the Brightway processing activity {e}"

        bw_nodes_and_amounts: dict[uuid.UUID, float] = await self.load_brightway_node_and_subgraph(
            processing_info, country_code
        )
        if bw_nodes_and_amounts:
            result = await self.model_specific_formula(production_amount, bw_nodes_and_amounts, parent_flow)
            return result + (processing_info.tags,)
        else:
            return None, None, None

    @abstractmethod
    async def load_brightway_node_and_subgraph(
        self, processing_info: ProcessingTagsAndElectricityAmount, country_code: str | None
    ) -> Optional[dict[uuid.UUID, float]]:
        """Loading processing Brightway node and its direct subgraph."""
        pass

    @abstractmethod
    async def model_specific_formula(
        self,
        production_amount: QuantityProp,
        bw_nodes_and_amounts: dict[uuid.UUID, float],
        *args: ...,
    ) -> tuple[QuantityProp, dict[uuid.UUID, QuantityProp]]:
        """Implementation of model specific formula."""
        pass

    async def get_unit_term_from_bw_node_uid(self, bw_node_uid: uuid.UUID) -> Term:
        """Given the Brightway node uid, obtain the unit term."""
        bw_node = await self.service_provider.node_service.find_by_uid(bw_node_uid)
        return UnitWeightConverter.unit_term_from_unit_name(bw_node.production_amount.unit)
