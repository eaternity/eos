"""Freezing processing model."""

import uuid

from gap_filling_modules.ingredicalc.helpers import get_nutrient_amount_in_gram_per_100g, get_water_gram_per_100g
from gap_filling_modules.processing_models.process_with_electricity_node import ProcessWithElectricityNode
from structlog import get_logger

from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.props.quantity_package_prop import QuantityPackageProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum

logger = get_logger()


class FreezingProcessingModel(ProcessWithElectricityNode):
    """Freezing processing model."""

    async def model_specific_formula(
        self, production_amount: QuantityProp, bw_nodes_and_amounts: dict[uuid.UUID, float], *args: FoodProductFlowNode
    ) -> tuple[QuantityProp, dict[uuid.UUID, QuantityProp]] | tuple[None, None]:
        """Freezing specific formula computing flow amounts."""
        return await self.model_specific_formula_for_freezing(production_amount, bw_nodes_and_amounts, args[0])

    async def model_specific_formula_for_freezing(
        self,
        production_amount: QuantityProp,
        bw_nodes_and_amounts: dict[uuid.UUID, float],
        parent_flow: FoodProductFlowNode,
    ) -> tuple[QuantityProp, dict[uuid.UUID, QuantityProp]] | tuple[None, None]:
        """Freezing specific formula computing flow amounts."""
        return_dict = {}

        nutrients = parent_flow.nutrient_values
        electricity_need_computed_from_nutrients = None
        if isinstance(nutrients, QuantityPackageProp):
            water_fraction = get_water_gram_per_100g(nutrients) / 100.0
            fat_fraction = get_nutrient_amount_in_gram_per_100g(nutrients, "fat") / 100.0
            electricity_need_computed_from_nutrients = self.compute_freezing_energy(water_fraction, fat_fraction)

        for bw_node_uid, default_electricity_need in bw_nodes_and_amounts.items():
            unit_term = await self.get_unit_term_from_bw_node_uid(bw_node_uid)
            if electricity_need_computed_from_nutrients is not None:
                amount = electricity_need_computed_from_nutrients
            else:
                amount = default_electricity_need
            return_dict[bw_node_uid] = QuantityProp.unvalidated_construct(
                value=amount * production_amount.value,
                unit_term_uid=unit_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            )

        return (
            QuantityProp(
                value=production_amount.value,
                unit_term_uid=production_amount.unit_term_uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
            return_dict,
        )

    @staticmethod
    def compute_freezing_energy(x_w: float, x_f: float) -> float:
        """Computing energy required for freezing."""
        # can be considered as default variables

        # Temperatures in degrees Celsius

        # frozen food temperature [°C]
        T = -23
        # frozen food temperature at the center [°C](2)
        T_c = -15
        # initial temperature [°C]
        T_i = 20
        # initial freezing temperature product [°C] - value for potatoes(1)
        T_if = -1.7
        # cooling medium temperature [°C] (1)
        T_f = -32
        # outlet mass average temperature between T_c and T_f, temp gradient from center to surface of product (1)
        T_out = -18

        # shortest distance from thermal center to product surface [m] - considering 6 mm thickness of potatoes (1)
        R = 0.005
        # surface heat transfer coefficient [W/m2 K] [2]
        h = 13

        # Flow rate W_pr/T_pr set at 1 kg/s
        # amount of product resident in the freezer at any time [kg]
        W_pr = 1
        # product residence time [s] Can be used in place of calculating t_f. If not known put 0 or a negative value (1)
        t_pr = 1

        # Coefficient of performance refrigerator - relationship between heat load and compressor energy use (1)
        COP = 2.75

        ################################################
        # CONSTANTS
        # densities [kg/m3] (1)
        p_w = 1000
        # ice
        # p_i = 917
        p_f = 930
        # p_c = 1550
        # p_p = 1380
        # p_m = 2165
        # solids
        p_s = 1450

        # specific heat [J/kg K] (1)
        c_w = 4180
        c_i = 2110
        c_f = 1900
        # c_c = 1500
        # c_p = 1900
        # c_m = 1100
        c_s = 1600

        # thermal conductivity [W/m K] (1)
        k_w = 0.56
        # k_i = 0.18
        k_f = 0.18
        # k_c = 0.245
        # k_p = 0.2
        # k_m = 0.26
        k_s = 0.22

        # default bound water fraction
        b = 0.25

        # Freezing time parameters, assuming slab shape (infinite cylinder shape and sphere also available) (2)
        m = 1.04
        n = 0.09
        c = 0.18
        A1 = -1.08125
        B = 62.9375

        # latent heat of water [kJ/kg]
        L1 = 334

        # freezers heat load percentages per freezer type (2)
        # batch_air_blast = {"prod": 0.65, "fans": 0.25, "pulldown": 0.05, "defrost": 0.025, "other": 0.025}
        continuous_air_blast = {"prod": 0.6, "fans": 0.2, "pulldown": 0.0, "defrost": 0.15, "other": 0.5}
        # plate = {"prod": 0.85, "fans": 0.075, "pulldown": 0.025, "defrost": 0.025, "other": 0.025}
        # cryogenic = {"prod": 0.85, "fans": 0.06, "pulldown": 0.03, "defrost": 0.0, "other": 0.06}

        # unit conversion kWh - J
        kWh_J = 3600000
        # 0°C - 273.15 K
        # K_C = 273.15

        #######################################
        # CALCULATIONS

        # solid and bound water fractions (2)
        x_s = 1 - x_w - x_f
        # manual change on 2024-09-20: The bound water fraction can never be larger than the water fraction
        x_b = min(b * x_s, x_w)

        # ice and liquid water component water fraction (2)
        x_i = (x_w - x_b) * (1 - T_if / T)
        # x_lw = x_w - x_i - x_b

        # latent heat of freezing [kJ/kg]
        L = L1 * x_i

        # density of food (2)
        p = 1 / (x_w / p_w + x_s / p_s + x_f / p_f)
        # frozen
        # pf = 1 / (x_i / p_i + x_lw / p_w + x_b / p_w + x_s / p_s + x_f / p_f)

        # conductivity unfrozen food - function of food volume fractions [J/(kg K)]
        k_u = p * (x_s * k_s / p_s + x_w * k_w / p_w + x_f * k_f / p_f)
        # k = pf * (x_s * k_s / p_s + x_w * k_w / p_w + x_f * k_f / p_f)

        # unfrozen specific heat [J/ kgK]
        c_u = c_w * x_w + c_s * x_s + c_f * x_f
        # frozen specific heat
        c_fr = c_u - (x_w - x_b) * (L1 * T_f / T**2 + (c_w - c_i))

        # thermal diffusivity of fresh food [m2/s] (2)
        a = k_u / (p * c_u)

        # Biot number
        Bi = h * R / k_u

        if t_pr <= 0:
            # calculate freezing time if not known
            # freezing time - t_f<= t_pr [s]
            t_f = (
                (A1 * T_c + B)
                * (1 / Bi + c)
                * ((T_if - T_i) / T_if) ** n
                * ((T_f - T_if) / T_if) ** -m
                * a**-1
                * R**2
            )
            t_pr = t_f

        # Energy needed to freeze the product [J/s]
        Q_pr = W_pr / t_pr * (c_u * (T_i - T_if) + L * 1000 + c_fr * (T_if - T_out))

        # Energy for heat load components asst_pr/36-uming continuous air blast
        Q_fans = Q_pr / continuous_air_blast["prod"] * continuous_air_blast["fans"]
        Q_pulldown = Q_pr / continuous_air_blast["prod"] * continuous_air_blast["pulldown"]
        Q_defrost = Q_pr / continuous_air_blast["prod"] * continuous_air_blast["defrost"]
        Q_other = Q_pr / continuous_air_blast["prod"] * continuous_air_blast["other"]

        # Q total heat load [J/s]
        Q_heat_tot = Q_pr + Q_fans + Q_pulldown + Q_defrost + Q_other

        # Energy compressor [J/s]
        Q_compressor = Q_heat_tot / COP

        # Energy ancillary equipment [J/s] - usually 15/20% of compressor (2)
        Q_ancillary = Q_compressor * 0.175

        # Energy refrigerator
        Q_refr = Q_compressor + Q_ancillary

        # TOTAL ENERGY CONSUMPTION [J/s]
        Q_tot = Q_heat_tot + Q_refr

        # TOTAL ENERGY CONSUMPTION kWh PER KG [kWh/kg]
        Q_tot_kg = Q_tot * t_pr / W_pr / kWh_J
        return Q_tot_kg
