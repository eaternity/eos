"""Drying processing model."""

import uuid

from gap_filling_modules.processing_models.process_with_bw_nodes import ProcessWithBWNode
from structlog import get_logger

from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.domain.term import Term

logger = get_logger()


class DryingProcessingModel(ProcessWithBWNode):
    """Drying processing model."""

    async def model_specific_formula(
        self,
        production_amount: QuantityProp,
        bw_nodes_and_amounts: dict[uuid.UUID, float],
        *args: FoodProductFlowNode | dict[frozenset[str], Term],
    ) -> tuple[QuantityProp, dict[uuid.UUID, QuantityProp]] | tuple[None, None]:
        """Drying specific formula computing flow amounts."""
        return await self.model_specific_formula_for_drying(production_amount, bw_nodes_and_amounts, args[0])

    async def model_specific_formula_for_drying(
        self,
        production_amount: QuantityProp,
        bw_nodes_and_amounts: dict[uuid.UUID, float],
        parent_flow: FoodProductFlowNode,
    ) -> tuple[QuantityProp, dict[uuid.UUID, QuantityProp]] | tuple[None, None]:
        """Drying specific formula computing flow amounts."""
        upscale_ratio = parent_flow.nutrient_upscale_ratio.value
        water_to_evaporate = upscale_ratio - 1.0

        return_dict = {}

        for bw_node_uid, amount in bw_nodes_and_amounts.items():
            unit_term = await self.get_unit_term_from_bw_node_uid(bw_node_uid)
            return_dict[bw_node_uid] = QuantityProp.unvalidated_construct(
                value=amount * production_amount.value * water_to_evaporate,
                unit_term_uid=unit_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            )

        return (
            QuantityProp(
                value=upscale_ratio * production_amount.value,
                unit_term_uid=production_amount.unit_term_uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
            return_dict,
        )
