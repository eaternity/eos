"""Aggregation gap filling module."""

from gap_filling_modules.abstract_aggregation_gfm import (
    AbstractAggregationGapFillingFactory,
    AbstractAggregationGapFillingWorker,
    AggregatedDataEnum,
)
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.matrix_calculation_gfm import MatrixCalculationGapFillingWorker
from structlog import get_logger

from core.domain.nodes.node import Node
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class AggregationGapFillingWorker(AbstractAggregationGapFillingWorker):
    """Aggregation gap filling worker."""

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """We want to perform aggregation after all the flow amounts for the ingredients have been calculated."""
        return MatrixCalculationGapFillingWorker.graph_building_gfm_workers_finished(self)


class AggregationGapFillingFactory(AbstractAggregationGapFillingFactory):
    """Aggregation gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.aggregated_props: list[AggregatedDataEnum] = [
            AggregatedDataEnum.nutrient_values,
            AggregatedDataEnum.amount_per_category_in_flow,
            AggregatedDataEnum.rainforest_critical_products,
            AggregatedDataEnum.animal_products,
        ]

    def spawn_worker(self, node: Node) -> AggregationGapFillingWorker:
        """Spawn aggregation gap filling worker."""
        return AggregationGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = AggregationGapFillingFactory
