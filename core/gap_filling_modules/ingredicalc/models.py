# TODO large portions of this code is not used ATM; either remove it or use/test it

from gap_filling_modules.ingredicalc.helpers import get_nutrient_term, get_unit_term
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter

from core.domain.props.quantity_package_prop import QuantityPackageProp
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp

NA_PERCENT_IN_NACL = 39.34
CL_PERCENT_IN_NACL = 60.66
FAT_CALORIES_PER_GRAM = 9.0
PROTEIN_CALORIES_PER_GRAM = 4.0


def adjust_incoming_for_fermentation(incoming_nutrients: QuantityPackageProp) -> QuantityPackageProp:
    """Created in 2019-2020 by Jan Machacek, Kristin Schlaepfer and Jens Hinkelmann."""
    incoming_nutrients = incoming_nutrients.duplicate()
    alc = incoming_nutrients.quantities.get(get_nutrient_term("alcohol").uid)
    # cannot adjust if no alc content provided
    if alc is None or alc.value <= 0:
        return incoming_nutrients

    sugar = incoming_nutrients.quantities.get(get_nutrient_term("sucrose").uid)
    carbs = incoming_nutrients.quantities.get(get_nutrient_term("carbohydrates").uid)
    energy = incoming_nutrients.quantities.get(get_nutrient_term("energy").uid)

    if carbs is None and sugar is None and energy is None:
        return incoming_nutrients

    # and yes, I'm taking data from
    # "Quora": https://www.quora.com/How-much-alcohol-can-be-expected-to-be-fermented-from-1-kg-of-sugar
    # There are some slight differences in achievable alcohol concentration based on the yeast strain used but for our
    # purposes we will use the most commonly accepted conversion rate of 55% to get the answer.
    # One kilogram is 1000 grams, so, fermentation would produce 550 grams of alcohol.
    # alc_as_sugar = alc / 0.55  # from quora
    alc_as_sugar = (
        alc.value / 0.47
    )  # from judith: The Gay-Lussac Equation C6H12O6 2CH3CH2OH + 2CO2 180g Sugar is converted to 92g Ethanol
    # and 88g Carbon Dioxide BUT, this can only be achieved if there is no yeast growth and ethanol is not lost
    # as vapour. In practice, ethanol yields observed are 90 to 95% of theoretical.
    kcals_of_alc = alc.value * 7.0
    kcals_of_sugar = alc_as_sugar * 4.0
    if sugar is not None:
        sugar.value += alc_as_sugar
        incoming_nutrients.quantities[get_nutrient_term("sucrose").uid].value = sugar.value
    if carbs is not None:
        carbs.value += alc_as_sugar
        incoming_nutrients.quantities[get_nutrient_term("carbohydrates").uid].value = carbs.value
    if energy is not None:
        assert energy.unit_term_uid == get_unit_term("kilocalorie").uid
        energy.value += kcals_of_sugar
        energy.value -= kcals_of_alc
        energy.value = max(energy.value, 0.0)
        incoming_nutrients.quantities[get_nutrient_term("energy").uid].value = energy.value

    # now set alcohol to 0, as we are "pre-fermentation" now
    incoming_nutrients.quantities = {
        k: v for k, v in incoming_nutrients.quantities.items() if k != get_nutrient_term("alcohol").uid
    }

    return incoming_nutrients


def ignore_small_values(incoming_nutrients: dict) -> dict:
    incoming_nutrients = incoming_nutrients.copy()

    for k, v in incoming_nutrients.items():
        if v is not None and v < 1.0:
            incoming_nutrients[k] = None

    return incoming_nutrients


def split_sodium_chloride(incoming_nutrients: QuantityPackageProp) -> QuantityPackageProp:
    sodium_chloride = incoming_nutrients.quantities[get_nutrient_term("sodium_chloride").uid]
    if sodium_chloride is None:
        return incoming_nutrients.duplicate()

    sodium_chloride_unit = sodium_chloride.get_unit_term()

    old_chlorine = incoming_nutrients.quantities.get(get_nutrient_term("chlorine").uid, None)
    old_sodium = incoming_nutrients.quantities.get(get_nutrient_term("sodium").uid, None)

    if old_sodium is None:
        old_sodium = 0
    else:
        if old_sodium.get_unit_term().sub_class_of != sodium_chloride_unit.sub_class_of:
            raise ValueError(f"{old_sodium.get_unit_term()} should be convertible to {sodium_chloride_unit}")

        old_sodium = UnitWeightConverter.convert_between_same_unit_types(
            old_sodium.value, old_sodium.get_unit_term(), sodium_chloride_unit, "mass-in-g"
        )

    if old_chlorine is None:
        old_chlorine = 0
    else:
        if old_chlorine.get_unit_term().sub_class_of != sodium_chloride_unit.sub_class_of:
            raise ValueError(f"{old_chlorine.get_unit_term()} should be convertible to {sodium_chloride_unit}")

        old_chlorine = UnitWeightConverter.convert_between_same_unit_types(
            old_chlorine.value, old_chlorine.get_unit_term(), sodium_chloride_unit, "mass-in-g"
        )

    # 100 g NaCl is 39.34 g Na and 60.66 g Cl
    new_sodium = old_sodium + (sodium_chloride.value * NA_PERCENT_IN_NACL) / 100
    new_chlorine = old_chlorine + (sodium_chloride.value * CL_PERCENT_IN_NACL) / 100

    new_nutrients = dict(incoming_nutrients.quantities)

    new_nutrients[get_nutrient_term("sodium").uid] = ReferencelessQuantityProp(
        value=new_sodium, unit_term_uid=sodium_chloride.unit_term_uid
    )
    new_nutrients[get_nutrient_term("chlorine").uid] = ReferencelessQuantityProp(
        value=new_chlorine, unit_term_uid=sodium_chloride.unit_term_uid
    )

    # Need to reset sodium_chloride_gram to None, because otherwise it's still seen as a valid nutrient.
    # Each nutrient which is not in the EDB (flat nutrients), is considered to be zero rather than None.
    if get_nutrient_term("sodium_chloride").uid in new_nutrients:
        del new_nutrients[get_nutrient_term("sodium_chloride").uid]

    return QuantityPackageProp.unvalidated_construct(
        quantities=new_nutrients,
        for_reference=incoming_nutrients.for_reference,
        prop_term_uid=incoming_nutrients.prop_term_uid,
    )
