import json
import os
import uuid
from typing import Optional

import cvxpy as cp
import numpy as np
from cvxpy import SolverError
from gap_filling_modules.ingredicalc.helpers import (
    ACCEPTED_NUTRIENTS,
    get_nutrient_term,
    make_constraint_matrix,
    make_equality_matrix_and_vector,
    make_nutrient_matrix,
    make_percentages_constraint,
)
from gap_filling_modules.ingredicalc.models import adjust_incoming_for_fermentation, split_sodium_chloride
from structlog import get_logger

from core.domain.props.quantity_package_prop import QuantityPackageProp, ReferencelessQuantityProp

# TODO this should probably come from the glossary, so that Science can edit it there
with open(os.path.dirname(__file__) + "/nutr_stats.json", "r") as nutr_stats_file:
    nutr_stats = json.load(nutr_stats_file)

    nutr_stats_by_name = {}
    for key, val in nutr_stats.items():
        nutrient_name = "_".join(key.split("_")[:-1])
        nutrient_unit = key.split("_")[-1]
        if nutrient_name not in nutr_stats_by_name:
            if nutrient_unit in ("gram", "kcal"):
                nutr_stats_by_name[nutrient_name] = val
            elif nutrient_unit == "milligram":
                nutr_stats_by_name[nutrient_name] = {k: v * 1e-3 for k, v in val.items()}
            elif nutrient_unit == "microgram":
                nutr_stats_by_name[nutrient_name] = {k: v * 1e-6 for k, v in val.items()}

logger = get_logger()


def _make_norm_vector(param: list[uuid.UUID]) -> list[float]:
    from core.service.service_provider import ServiceLocator

    service_locator = ServiceLocator()
    service_provider = service_locator.service_provider
    glossary_service = service_provider.glossary_service

    return [nutr_stats_by_name[glossary_service.terms_by_uid[nutr].name.lower()]["STD"] for nutr in param]


def run(
    incoming_nutrients: QuantityPackageProp,
    flat_nutrients: dict[tuple, QuantityPackageProp],
    fixed_percentages: dict[tuple, Optional[float]],
) -> tuple[dict, dict, dict, str]:
    """Run the optimisation to calculate the ingredient amounts.

    Created in 2019-2020 by Jan Machacek, Kristin Schlaepfer and Jens Hinkelmann.
    General Outline of how ingredicalc currently works (from Manuel Memory): We collect the information:
    - List of ingredients (in order), percentage values, nutrients of the ingredients (including water) from EDB,
     the actual declared nutrients
    - We set the conditions for the equation solver to estimate the amounts by:
      - the list of ingredients needs to be decreasing in amounts
      - Percentage value are taken as fix values.
      - exceptions to both rules above: Each ingredient can scale beyond its quantities by the amount of water it
        contains (e.g. Apple 100g, contains 86g of water, so the amount can be up to 714g of apple for a product
        that contains 100% apple and weighs 100g.
      - → the sum of nutrients needs to be as close as possible to the actual declared nutrients.
    → fermentation (conversion of nutrients to alcohol)


    :param incoming_nutrients: a dict, key: nutrient-name_unit (e.g. energy_kcal), value: amount
    :param flat_nutrients: a dict whose keys create ids, composed of a tuple of (level, namespace, product_id,
        is_subdivision).
        I don't think we need namespace or is_subdivision. Values have the euroFIR format.
        The level informs the optimiser about the hierarchy of ingredients, e.g.:
                    # "ingredients_declaration": 'Main0 (Sub00, Sub01), Main1 (Sub10, Sub11)',
                    # (0, 0) means Sub00
                    # (0, 1) means Sub01
                    # (1, 0) means Sub10
                    # (1, 1) means Sub11
    :param fixed_percentages:
    :return: a tuple of (solutions, error_squares); solutions is a dict, key: same as flat_nutrients' keys, value: the
     optimized amount, as found by the algorithm
    """
    log = logger.bind(nr_products_to_adjust=len(flat_nutrients), method="nutrient calculation")

    try:
        # remove water from incoming to make sure that we don't optimize it directly (only used for evaporation):
        incoming_nutrients.quantities = {
            k: v for k, v in incoming_nutrients.quantities.items() if k != get_nutrient_term("water").uid
        }

        if get_nutrient_term("sodium_chloride").uid in incoming_nutrients.quantities:
            incoming_nutrients = split_sodium_chloride(incoming_nutrients)

        # adjust incoming nutrients for fermentation
        incoming_nutrients = adjust_incoming_for_fermentation(incoming_nutrients)
        # incoming_nutrients = ignore_small_values(incoming_nutrients)

        log.debug("Starting optimisation")
        # M nutrients
        # nutrient_order = list(incoming_nutrients.keys())
        accepted_nutrient_uids = {get_nutrient_term(v).uid for v in ACCEPTED_NUTRIENTS}

        # Filter to only use accepted nutrients for optimization. The filter is required since not all nutrients are
        # complete in the eaternity database.
        nutrient_order = list(
            key
            for key, val in incoming_nutrients.quantities.items()
            if val is not None and val.value > 0 and key in accepted_nutrient_uids
        )

        M = len(nutrient_order)
        # might be using energy twice (kCal and kJ)

        # N products
        product_order = sorted(list(flat_nutrients.keys()))
        N = len(product_order)

        log.debug("Problem size: {}x{}".format(M, N))

        # M x N ingredient-product matrix
        A = make_nutrient_matrix(flat_nutrients, product_order, nutrient_order, M, N)

        # M x 1 array, incoming nutrient array
        b = np.asarray([incoming_nutrients.quantities[nutrient].value for nutrient in nutrient_order])
        norm_vector = _make_norm_vector([nutrient for nutrient in nutrient_order])
        # normalized the individual nutrients to each nutrient standard deviation over the whole set
        Anorm = (A.T / norm_vector).T
        bnorm = b / norm_vector

        # N x N constraint matrix
        C = make_constraint_matrix(product_order, N)

        # N x 1 constraint vector
        d = np.zeros(N)

        # (nr_recipes) x N matrix F, (nr_recipes) x 1 vector g
        # Sum to 1, also on sublevels
        F, g = make_equality_matrix_and_vector(product_order, N)

        # https://www.cvxpy.org/examples/basic/quadratic_program.html
        # Disciplined Convex Programming - https://www.cvxpy.org/tutorial/dcp/index.html
        # finding x in Ax=b, with constraints matrix Cx<d and Fx==g, and x being positive
        # --> turned into minimization problem
        # min (0.5 (Ax-b)T (Ax-b)), Cx<=d, x>=e,

        x = cp.Variable(N)

        objective = cp.Minimize(cp.norm(Anorm @ x - bnorm))
        weight_constraint_base = F[0:1, :] @ x == g[0:1]
        constraints = [C @ x <= d, weight_constraint_base]
        if len(g) > 1:
            weight_constraint_subprods = F[1:, :] @ x == g[1:]
            constraints.append(weight_constraint_subprods)

        if fixed_percentages:
            constraints_perc = make_percentages_constraint(
                product_order,
                fixed_percentages,
                x,
            )
            constraints.extend(constraints_perc)

        problem = cp.Problem(objective, constraints)

        log.debug("Problem definition", problem=str(problem))
        try:
            problem.solve(solver="ECOS")
        except ValueError as error:
            print(str(problem))
            raise error
        except SolverError as error:
            print(str(error))
            log.debug("solver error...")
            raise error

        if problem.status in ("infeasible", "unbounded"):
            log.warning(
                "Optimisation failed.",
                percentage_constraints={
                    str(k): v for k, v in fixed_percentages.items()
                },  # make it explicitly serializable
                status=problem.status,
                solution=str(problem.solution),
            )
            raise ValueError(
                f"The ingredient amount estimator solution is {problem.status}! Please check if "
                f"percentages in ingredients declaration and their order are correct."
            )

        # x100, because we're in "fraction of 1" space, but need to get to grams, and nutrients are normalized to 100g
        try:
            solution_vector = x.value * 100.0
        except TypeError as err:
            log.error(
                "Invalid solution! Please check if percentages in ingredients declaration and their order are correct.",
                percentage_constraints=fixed_percentages,
            )
            raise ValueError(
                "Invalid solution! Please check if percentages in ingredients declaration "
                "and their order are correct."
            ) from err

        if any(np.isnan(sol) for sol in solution_vector):
            raise ValueError("Solution includes NaN values.")

        A0 = make_nutrient_matrix(flat_nutrients, product_order, nutrient_order, M, N)
        b0 = np.asarray([incoming_nutrients.quantities[nutr].value for nutr in nutrient_order])
        res = A0 @ x.value
        error_squares = {}

        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        glossary_service = service_provider.glossary_service
        dimensionless_term = glossary_service.terms_by_xid_ag_uid[
            ("EOS_dimensionless", glossary_service.root_subterms["EOS_units"].access_group_uid)
        ]

        for i in range(res.__len__()):
            try:
                nutr_name = glossary_service.terms_by_uid[nutrient_order[i]].name.lower()
                error_squares[nutrient_order[i]] = ReferencelessQuantityProp(
                    value=((res[i] - b0[i]) / nutr_stats_by_name[nutr_name]["STD"]) ** 2,
                    unit_term_uid=dimensionless_term.uid,
                )
            except KeyError as e:
                log.debug("unknown nutrient", nutrient_order=nutrient_order, i=i, error=e)
        log.debug("Finished optimisation")
        result = {k: v for k, v in zip(product_order, solution_vector)}
        return result, error_squares, flat_nutrients, problem.solution.status
    finally:
        log.unbind("nr_products_to_adjust", "method")
