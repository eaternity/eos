# TODO large portion of this code is not used ATM; either remove it or use/test it
"""Code created in 2019-2020 by Jan Machacek, Kristin Schlaepfer and Jens Hinkelmann."""
import re
import uuid
from typing import Optional

import cvxpy as cp
import numpy as np
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from structlog import get_logger

from core.domain.props import QuantityPackageProp, ReferencelessQuantityProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.service.service_provider import ServiceLocator

logger = get_logger()

MAP_EUROFIR_UNITS_TO_GRAM = {
    "g": 1e0,
    "MSE": 1e0,  # FIXME check if this unit should really be assumed to be gram!
    "mg": 1e-3,
    "ug": 1e-6,
}

# FIXME add more
# Mapping between EOS API schema's definition of nutrients and EUROFIR codes.
MAP_EUROFIR_EOS = {
    "ENERC": "energy_kcal",
    "FAT": "fat_gram",
    "FASAT": "saturated_fat_gram",
    "CHO": "carbohydrates_gram",
    "WATER": "water_gram",
    "SUCS": "sucrose_gram",
    "SUGAR": "sucrose_gram",
    "FIBT": "fibers_gram",
    "PROT": "protein_gram",
    "NA": "sodium_milligram",
    "CLD": "chlorine_milligram",
}
# Filter to only use accepted nutrients for optimization. The filter is required since not all nutrients are
# complete in the eaternity database.
ACCEPTED_NUTRIENTS = {
    "energy",
    "fat",
    "saturated_fat",
    "carbohydrates",
    "water",
    "sucrose",
    "protein",
    "sodium",
    "chlorine",
    "fibers",
}


def get_nutrient_amount_in_gram_per_100g(nutrients: QuantityPackageProp, nutrient_str: str) -> Optional[float]:
    """Method to extract a given nutrient in gram from the nutrient_values prop of a node."""
    service_provider = ServiceLocator().service_provider
    nutrition_access_group_uid = service_provider.glossary_service.root_subterms["EOS_nutrient_names"].access_group_uid
    nutrient_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (f"EOS_{nutrient_str}", nutrition_access_group_uid)
    ]
    if nutrient_term.uid not in nutrients.quantities:
        return None
    unit_access_group: uuid.UUID = service_provider.glossary_service.root_subterms["EOS_units"].access_group_uid
    gram_term: Term = service_provider.glossary_service.terms_by_xid_ag_uid[("EOS_gram", unit_access_group)]
    quantity = nutrients.amount_for_100g().quantities[nutrient_term.uid]
    quantity_in_gram = UnitWeightConverter.convert_between_same_unit_types(
        quantity.value, quantity.get_unit_term(), gram_term, "mass-in-g"
    )
    return quantity_in_gram


def get_water_gram_per_100g(nutrients: QuantityPackageProp) -> float:
    """Method to extract the water content from the nutrient_values prop of a node."""
    water_in_gram_directly_from_nutrients = get_nutrient_amount_in_gram_per_100g(nutrients, "water")
    if water_in_gram_directly_from_nutrients is not None:
        return water_in_gram_directly_from_nutrients
    else:
        other_massive_nutrients = ["fibers", "fat", "sodium", "chlorine", "protein", "carbohydrates"]
        water_content_from_other_nutrients = 100.0 - sum(
            (
                get_nutrient_amount_in_gram_per_100g(nutrients, other_massive_nutrient)
                if get_nutrient_amount_in_gram_per_100g(nutrients, other_massive_nutrient) is not None
                else 0.0
            )
            for other_massive_nutrient in other_massive_nutrients
        )
        return water_content_from_other_nutrients


def transform_eurofir(eurofir_data: list[dict[str, float | str]], data_source: str | None = None) -> dict[str, float]:
    """Transforms the eurofir nutrition data into a "eos" format.

    calc_graph is required if one wants to set_data_errors_log_entry.
    """
    if eurofir_data is None:
        return None
    else:
        nutr = {n["component-id"]: n for n in eurofir_data}
        nutr_kale = {}
        for eurofir_name, name in MAP_EUROFIR_EOS.items():
            if eurofir_name in nutr:
                amount, unit = nutr[eurofir_name]["value"], nutr[eurofir_name]["unit"]

                missing_data = nutr[eurofir_name].get("missing_data", "false").lower() == "true"

                if missing_data and data_source:
                    warning_msg = f"{eurofir_name} is missing in {data_source}."
                    logger.debug(warning_msg)

                if isinstance(amount, str):
                    logger.debug(
                        "type(amount) is string, but should be numeric!",
                        edb_name=eurofir_name,
                        amount=amount,
                        unit=unit,
                        eurofir_data=eurofir_data,
                    )
                    if amount == "" and data_source:
                        warning_msg = f"{eurofir_name} has empty amount in {data_source}."
                        logger.warning(warning_msg)
                        amount = 0.0
                    else:
                        amount = float(amount)

                    # FIXME should be fixed already in importer... Instead, it would be better to raise here:
                    # raise ValueError("eurofir nutrient file contains string amounts instead of integers. "
                    #                  "This better should be fixed in the source nutrient data.")

                if eurofir_name == "ENERC":
                    if unit == "kcal":
                        amount = amount
                    elif unit == "kJ" or unit == "KJ":
                        amount = amount * 0.239  # convert kJ to kcal
                    elif amount == 0:
                        pass  # unit does not matter anyway
                    else:
                        raise ValueError(f"nutrient {eurofir_name} contains unhandled unit {unit}")

                elif name.endswith("_gram"):
                    # we need to convert to gram:
                    if unit in MAP_EUROFIR_UNITS_TO_GRAM:
                        amount = amount * MAP_EUROFIR_UNITS_TO_GRAM[unit]
                    elif amount == 0:
                        pass  # unit does not matter anyway
                    else:
                        raise ValueError(f"nutrient {eurofir_name} contains unhandled unit {unit}")

                elif name.endswith("_milligram"):
                    # we need to convert to milligram:
                    if unit in MAP_EUROFIR_UNITS_TO_GRAM:
                        amount_in_gram = amount * MAP_EUROFIR_UNITS_TO_GRAM[unit]
                        amount = amount_in_gram * 1e3
                    elif amount == 0:
                        pass  # unit does not matter anyway
                    else:
                        raise ValueError(f"nutrient {eurofir_name} contains unhandled unit {unit}")

                else:
                    raise ValueError(f"nutrient {eurofir_name} has no specified target unit to convert to.")

                nutr_kale[name] = amount

        return nutr_kale


def nutrients_dict_to_prop(raw_nutrients: dict) -> QuantityPackageProp:
    """Convert nutrient dictionary with keys {nutrient_name}_{unit} and values {amount} QuantityPackageProp.

    :param raw_nutrients: dictionary of nutrients with unit in the dict keys.
    :return: standardized nutrient format.
    """
    from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter

    from core.service.service_provider import ServiceLocator

    service_locator = ServiceLocator()
    service_provider = service_locator.service_provider
    glossary_service = service_provider.glossary_service

    root_unit_term: Term = glossary_service.root_subterms["EOS_units"]
    units = ["gram", "milligram", "microgram", "kilocalorie", "kilojoule", "volume-percent"]

    unit_terms = {
        unit: glossary_service.terms_by_xid_ag_uid[(f"EOS_{unit}", root_unit_term.access_group_uid)] for unit in units
    }

    standardized_nutrients: dict[uuid.UUID, ReferencelessQuantityProp] = {}
    for key, val in raw_nutrients.items():
        name = key
        if val is not None:
            if name.endswith("_gram"):
                new_name = name.replace("_gram", "")
                unit = unit_terms["gram"]
                new_val = val
            elif name.endswith("_milligram"):
                new_name = name.replace("_milligram", "")
                unit = unit_terms["gram"]
                new_val = UnitWeightConverter.convert_between_same_unit_types(
                    val, unit_terms["milligram"], unit_terms["gram"], "mass-in-g"
                )
            elif name.endswith("_microgram"):
                new_name = name.replace("_microgram", "")
                unit = unit_terms["gram"]
                new_val = UnitWeightConverter.convert_between_same_unit_types(
                    val, unit_terms["microgram"], unit_terms["gram"], "mass-in-g"
                )
            elif name.endswith("_volume_percent"):
                new_name = name.replace("_volume_percent", "")
                unit = unit_terms["volume-percent"]
                new_val = val
            elif name.endswith("_kcal"):
                new_name = name.replace("_kcal", "")
                unit = unit_terms["kilocalorie"]
                new_val = val
            elif name.endswith("_kilocalorie"):
                new_name = name.replace("_kilocalorie", "")
                unit = unit_terms["kilocalorie"]
                new_val = val
            elif name.endswith("_kj"):
                new_name = name.replace("_kj", "")
                unit = unit_terms["kilocalorie"]
                new_val = UnitWeightConverter.convert_between_same_unit_types(
                    val, unit_terms["kilojoule"], unit_terms["kilocalorie"], "energy-in-kj"
                )
            elif name.endswith("_kjoule"):
                new_name = name.replace("_kjoule", "")
                unit = unit_terms["kilocalorie"]
                new_val = UnitWeightConverter.convert_between_same_unit_types(
                    val, unit_terms["kilojoule"], unit_terms["kilocalorie"], "energy-in-kj"
                )
            else:
                raise ValueError(f"Unrecognized nutrient unit suffix: {name}.")

            nutrients_root_term: Term = glossary_service.root_subterms.get("EOS_nutrient_names")
            try:
                nutr_term = glossary_service.terms_by_xid_ag_uid[
                    (f"EOS_{re.sub(' ', '_', new_name.lower())}", nutrients_root_term.access_group_uid)
                ]

                if nutr_term.uid in standardized_nutrients:
                    standardized_nutrients[nutr_term.uid].value += new_val
                else:
                    standardized_nutrients[nutr_term.uid] = ReferencelessQuantityProp(
                        value=new_val, unit_term_uid=unit.uid
                    )
            except KeyError:
                logger.debug(f"Nutrient name '{new_name}' is not present in the glossary.")

    return QuantityPackageProp.unvalidated_construct(
        quantities=standardized_nutrients, for_reference=ReferenceAmountEnum.amount_for_100g
    )


def get_nutrient_term(nutrient_name: str) -> Term:
    from core.service.service_provider import ServiceLocator

    service_locator = ServiceLocator()
    service_provider = service_locator.service_provider
    glossary_service = service_provider.glossary_service

    nutrients_root_term = glossary_service.root_subterms.get("EOS_nutrient_names")
    return glossary_service.terms_by_xid_ag_uid[
        (f"EOS_{re.sub(' ', '_', nutrient_name.lower())}", nutrients_root_term.access_group_uid)
    ]


def get_unit_term(unit: str) -> Term:
    from core.service.service_provider import ServiceLocator

    service_locator = ServiceLocator()
    service_provider = service_locator.service_provider
    glossary_service = service_provider.glossary_service

    unit_root_term = glossary_service.root_subterms.get("EOS_units")
    return glossary_service.terms_by_xid_ag_uid[
        (f"EOS_{re.sub(' ', '_', unit.lower())}", unit_root_term.access_group_uid)
    ]


def transform_eurofir_to_qty_pkg(
    eurofir_data: list[dict[str, float | str]], data_source: str | None = None
) -> QuantityPackageProp:
    nutr_dict = transform_eurofir(eurofir_data, data_source=data_source)
    return nutrients_dict_to_prop(nutr_dict)


def make_nutrient_matrix(
    flat_nutrients: dict[tuple, QuantityPackageProp], product_order: list[tuple], nutrient_order: list, M: int, N: int
) -> np.ndarray:
    matrix = np.zeros((M, N))
    for iy, product_key in enumerate(product_order):
        if flat_nutrients[product_key] is None:
            continue  # leave this column at zero
        for ix, nutrient_key in enumerate(nutrient_order):
            if nutrient_key in flat_nutrients[product_key].quantities:
                matrix[ix, iy] = flat_nutrients[product_key].quantities[nutrient_key].value

    # replace nans with 0
    matrix[np.isnan(matrix)] = 0.0

    return matrix


def make_constraint_matrix(product_order: list[tuple], N: int) -> np.ndarray:
    matrix = np.zeros((N, N))
    matrix -= np.identity(N)

    # iterate over columns of matrix. For each column, a maximum of one "1" get added to the upper triangle
    # of the matrix. It is placed in the row corresponding to the item the current item needs to be smaller than.
    # Deals with nested recipes
    # Dark magic, looks like this:
    # input_keys:
    # [(0,), (1,), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2,), (2, 0), (2, 1), (2, 2), (2, 3), (2, 3, 0), (2, 3, 1),
    # (2, 3, 2), (3,)]
    # output_matrix
    # [[-1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, -1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 1],
    #  [0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1]]
    #
    # or with above input and assuming that (1, 0), (1, 1), (1, 2), (1, 3), (1, 4)
    #  are subdivisions into eaternity namespace:
    # output_matrix
    # [[-1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, -1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 1],
    #  [0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1]]
    #

    depth_rows = {}
    for col, (
        level,
        _,
        _,
        is_subdivision,
        _,
    ) in enumerate(
        product_order
    ):  # iterate over columns
        depth = len(level)
        last_row = depth_rows.get(depth, None)
        if last_row is not None and not is_subdivision:
            matrix[last_row, col] = 1.0

        depth_rows[depth] = col
        for key in depth_rows:
            if key > depth:
                depth_rows[key] = None

    return matrix


def make_equality_matrix_and_vector(product_order: list[tuple], N: int) -> tuple[np.ndarray, np.ndarray]:
    matrix = np.zeros((1, N))
    vector = np.ones((1,))

    # more black magic - sets up the equality constraints between different recipe levels
    # example - input:
    # [(0,), (1,), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2,), (2, 0), (2, 1), (2, 2), (2, 3), (2, 3, 0), (2, 3, 1),
    # (2, 3, 2), (3,)]
    # example - output:
    # [[1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    #  [0, -1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, -1, 1, 1, 1, 1, 0, 0, 0, 0],
    #  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 1, 1, 1, 0]]

    previous_depth = 1
    highest_row = 0
    depth_rows = {1: 0}
    for col, (level, *_) in enumerate(product_order):  # iterate over columns
        depth = len(level)
        row = depth_rows.get(depth, highest_row + 1)
        if row > highest_row:
            matrix = np.pad(matrix, ((0, 1), (0, 0)), mode="constant", constant_values=0)  # add new row to matrix
            vector = np.pad(vector, ((0, 1)), mode="constant", constant_values=0)  # add zero to vector
            highest_row += 1

        matrix[row, col] = 1.0
        if previous_depth < depth:
            matrix[row, col - 1] = -1.0
        elif previous_depth > depth:
            for key in list(depth_rows.keys()):
                if key > depth:
                    del depth_rows[key]

        depth_rows[depth] = row
        previous_depth = depth

    return matrix, vector


def make_fixed_precentages_matrix_and_vector(
    product_order: tuple, fixed_precentages: dict[tuple, Optional[float]], N: int
) -> tuple[np.ndarray, np.ndarray]:
    K = len(fixed_precentages)
    matrix = np.zeros((K, N))
    vector = np.zeros((K,))

    # even more black magic - set the fixed percentage vs parents
    # example - input:
    # [(0,), (1,), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2,), (2, 0), (2, 1), (2, 2), (2, 3), (2, 3, 0), (2, 3, 1),
    # (2, 3, 2), (3,)]
    # {((1, 2), ...): 0.2889827971664697, ((2,), ...): 0.09364470814237003, ((2, 3), ...): 0.060264094734110096,
    # ((2, 3, 0), ...): 0.6203118691470788}
    # example - output matrix:
    # [[0., -0.2889828, 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
    #  [0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0.],
    #  [0., 0., 0., 0., 0., 0., 0., -0.06026409, 0., 0., 0., 1., 0., 0., 0., 0.],
    #  [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., -0.6203119, 1., 0., 0., 0.]]
    # and output vector:
    # array([0.        , 0.09364471, 0.        , 0.        ])

    previous_depth = 1
    highest_row = 0
    depth_parent_cols = {1: None}
    for col, key in enumerate(product_order):  # iterate over columns
        level = key[0]
        depth = len(level)

        if previous_depth < depth:
            depth_parent_cols[depth] = col - 1
        previous_depth = depth

        if key not in fixed_precentages:
            continue

        parent_col = depth_parent_cols[depth]
        if parent_col is None:
            matrix[highest_row, col] = 1.0
            vector[highest_row] = fixed_precentages[key]
        else:
            matrix[highest_row, col] = 1.0
            matrix[highest_row, parent_col] = -fixed_precentages[key]

        highest_row += 1

    return matrix, vector


class ProductTreeNode:
    def __init__(self):
        self.levels_key = None
        self.col_idx = None
        self.child_tree_nodes = []

        # these are relative to parent node:
        self.specified_percentage = None  # is the specified (input) percentage value or otherwise None


def make_product_tree(product_order: list[tuple], fixed_precentages: dict[tuple, Optional[float]]) -> ProductTreeNode:
    tree_nodes_by_key = dict()
    root_node = ProductTreeNode()
    tree_nodes_by_key[()] = root_node

    for col_idx, product_key in enumerate(product_order):
        levels_key = product_key[0]

        # create a new tree node:
        node = ProductTreeNode()
        node.col_idx = col_idx
        node.levels_key = levels_key
        tree_nodes_by_key[levels_key] = node

        if product_key in fixed_precentages:
            node.specified_percentage = fixed_precentages[product_key]

        parent_levels_key = levels_key[:-1]

        # add this node as child to parent:
        tree_nodes_by_key[parent_levels_key].child_tree_nodes.append(node)

    return tree_nodes_by_key


def make_percentages_constraint(
    product_order: list[tuple], fixed_precentages: dict[tuple, Optional[float]], x: cp.Variable
) -> list:
    # set percentage vs parents
    #
    # example - input:
    # product_order:      [(0,), (1,), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2,), (2, 0), (2, 1), (2, 2), (2, 3),
    # (2, 3, 0), (2, 3, 1), (2, 3, 2), (3,)]
    # fixed_precentages:  {((1, 2), ...): 0.2889827971664697, ((2,), ...): 0.09364470814237003, ((2, 3), ...):
    # 0.060264094734110096, ((2, 3, 0), ...): 0.6203118691470788}

    # for the upper bound of sub-recipes with specified own percentages relative to parent, we use
    # the specified own percentage value divided by the maximum rel_dry_mass of it's own sub-ingredients (recursively)

    # first create a tree of the products (at least for now, for simpler code structure):
    tree_nodes_by_key = make_product_tree(product_order, fixed_precentages)
    num_products = len(product_order)
    num_equalities = len([k for (k, v) in tree_nodes_by_key.items() if v.specified_percentage is not None])

    eq_mat = np.zeros((num_equalities, num_products))
    eq_vec = np.zeros((num_equalities,))

    # create equations:
    eq_counter = 0
    for col_idx, product_key in enumerate(product_order):
        levels_key = product_key[0]
        node = tree_nodes_by_key[levels_key]

        parent_levels_key = levels_key[:-1]
        parent_col_idx = tree_nodes_by_key[parent_levels_key].col_idx

        if node.specified_percentage is not None:
            eq_mat[eq_counter, col_idx] = 1
            if parent_col_idx is None:
                # this node is a direct ingredient of the root recipe, so it's percentage is specified directly:
                eq_vec[eq_counter] = node.specified_percentage
            else:
                # this node is an ingredient of a sub-recipe, so it's percentage is specified relative to its parent:
                eq_mat[eq_counter, parent_col_idx] = -1 * node.specified_percentage

            eq_counter += 1

    constraints = []
    if num_equalities > 0:
        constraints.append(eq_mat @ x == eq_vec)

    return constraints


def make_expected_geometric_series(product_order: tuple, N: int) -> np.ndarray:
    vector = np.zeros((N,))

    # example - input:
    # [(0,), (1,), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2,), (2, 0), (2, 1), (2, 2), (2, 3), (2, 3, 0), (2, 3, 1),
    # (2, 3, 2), (3,)]
    # output:
    # [0.5, 0.25, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.125, 0.5, 0.25, 0.125, 0.0625, 0.5, 0.25, 0.125, 0.0625]

    depth_item_nr = {}
    for row, (level, *_) in enumerate(product_order):  # iterate over columns
        depth = len(level)
        item_nr = depth_item_nr.get(depth, 0) + 1
        vector[row] = 0.5**item_nr

        depth_item_nr[depth] = item_nr

        for key in depth_item_nr:
            if key > depth:
                depth_item_nr[key] = 0

    return vector
