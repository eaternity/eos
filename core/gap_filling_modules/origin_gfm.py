"""Origin gap filling module."""

import csv
import io
import os
import shutil
import sqlite3
import time
import uuid
from io import FileIO
from typing import Dict, Optional, Tuple
from zipfile import ZipFile

import aiofiles
import google
import httpx
import msgpack
import pandas as pd
from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.ingredient_amount_estimator_gfm import IngredientAmountEstimatorGapFillingWorker
from gap_filling_modules.inventory_connector_gfm import InventoryConnectorGapFillingWorker
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingWorker
from gap_filling_modules.location_gfm import CHINA_SPECIAL_ADMINISTRATIVE_REGIONS, LocationGapFillingWorker
from gap_filling_modules.location_util.countries import (
    get_inherited_country_code,
    iso_3166_map_2_to_3_letter,
    iso_3166_map_3_to_2_letter,
    iso_to_m49_mapping,
    location_to_regional_term_xid_map,
    m49_to_iso_mapping,
    regional_term_xid_to_region_gadm_codes_map,
)
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from googleapiclient.discovery import Resource, build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload
from pydantic_settings import SettingsConfigDict
from structlog import get_logger

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.node import Node
from core.domain.props import (
    AnimalProductsProp,
    GfmStateProp,
    GlossaryTermProp,
    NamesProp,
    OriginDataProp,
    QuantityPackageProp,
    QuantityProp,
)
from core.domain.props.glossary_term_prop import SourceEnum
from core.domain.props.location_prop import LocationProp, LocationQualifierEnum, LocationSourceEnum
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.envprops import EnvProps
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.glossary_link_service import GlossaryLinkService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class OriginGfmSettings(EnvProps):
    TEMP_DIR: str = "./temp_data"

    # used to be https://fenixservices.fao.org/faostat/static/bulkdownloads/Trade_DetailedTradeMatrix_E_All_Data.zip
    FAO_IMPORT_EXPORT_ZIP_URL: str = "https://bulks-faostat.fao.org/production/Trade_DetailedTradeMatrix_E_All_Data.zip"
    FAO_DOMESTIC_ZIP_URL: str = "https://bulks-faostat.fao.org/production/Production_Crops_Livestock_E_All_Data.zip"

    GDRIVE_SERVICE_ACCOUNT_FILE: str = "./secrets/service_account.json"
    GDRIVE_FAO_FOLDER_ID: str = "1ynyd6dMKAS-EO81lKn03up2s6PHWarsn"

    # temp_file_names for data extraction from the source files
    DOMESTIC_SOURCE_FILE_NAME: str = "Production_Crops_Livestock_E_All_Data_NOFLAG.csv"
    IMPORT_EXPORT_SOURCE_FILE_NAME: str = "Trade_DetailedTradeMatrix_E_All_Data_NOFLAG.csv"
    ITEMCODES_SOURCE_FILE_NAME: str = "Trade_DetailedTradeMatrix_E_ItemCodes.csv"

    FAO_SNAPSHOT_SUFFIX: str = ""

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


class OriginGapFillingWorker(AbstractGapFillingWorker):
    def __init__(self, node: Node, gfm_factory: "GapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        if isinstance(self.node, ActivityNode):
            return False

        if isinstance(self.node, FoodProductFlowNode) and self.node.is_subdivision:
            logger.debug(f"{self.node} is a subdivision --> can't run Origin GFM.")
            return False

        if isinstance(self.node, FoodProductFlowNode):
            return True

        return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(LocationGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for LocationGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # Wait, such that is_combining_ingredients check can be properly performed.
        if global_gfm_state.get(AddClientNodesGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for AddClientNodesGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # Wait, such that all inventory are properly linked before origin splitting.
        if global_gfm_state.get(InventoryConnectorGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for InventoryConnectorGapFillingFactory to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        # Wait, such that product_name is already matched.
        if global_gfm_state.get(MatchProductNameGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for MatchProductNameGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if global_gfm_state.get(LinkTermToActivityNodeGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Origin] wait for LinkTermToActivityNodeGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if (
            global_gfm_state.get(
                IngredientAmountEstimatorGapFillingWorker.__name__,
                0,
            )
            == NodeGfmStateEnum.scheduled.value
        ):
            logger.debug("[Origin] wait for IngredientAmountEstimatorGapFillingWorker to finish --> not can_run_now.")

            return GapFillingWorkerStatusEnum.reschedule

        if self.node.get_sub_nodes():
            for sub_activity in self.node.get_sub_nodes():
                if not isinstance(sub_activity.production_amount, QuantityProp):
                    logger.debug("[Origin] waiting for sub activity production amount --> not can_run_now.")
                    return GapFillingWorkerStatusEnum.reschedule

        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        if isinstance(self.node, FoodProductFlowNode) and (
            self.node.is_combining_ingredients() or self.node.link_to_sub_node is not None
        ):
            await self.combined_product_origin_processing(calc_graph)
        elif isinstance(self.node, FoodProductFlowNode):
            await self.monoproduct_origin_processing(calc_graph)
        else:
            logger.debug(
                f"{self.node} does not fit Origin GFM requirements -- is neither a monoproduct nor a combined product."
            )

    async def monoproduct_origin_processing(self, calc_graph: "CalcGraph") -> None:
        origins_list = await self.parse_current_origins()

        if len(origins_list) == 1:
            logger.debug(f"{self.node} already has an origin specified.")
            origin: LocationProp = origins_list[0]
            if origin.location_qualifier == LocationQualifierEnum.unknown:
                logger.debug("location_qualifier is unknown --> setting unknown origin.")
                await self.set_unknown_origin(calc_graph)
            return

        country_total_percentage_share = {}
        fao_code_term = None
        is_origin_from_fao: bool = False

        kitchen_country_code = get_inherited_country_code(self.node)
        kitchen_country_m49_code = iso_to_m49_mapping.get(kitchen_country_code)

        if len(origins_list) == 0:
            if self.node.get_sub_nodes() and any(
                isinstance(sub_flow, FoodProductFlowNode) and not sub_flow.is_subdivision
                for sub_flow in self.node.get_sub_nodes()[0].get_sub_nodes()
            ):
                logger.debug(
                    f"{self.node} has sub-food-product --> skipping, because monoproduct-origin-processing should "
                    "happen on the child-food-products."
                )
                return
            if not kitchen_country_m49_code:
                await self.set_unknown_origin(calc_graph)

                return

            # calculating origin mix depending on the kitchen

            # current foodex2 terms -> glossary link -> fao code -> data from the table
            if (
                hasattr(self.node, "product_name")
                and self.node.product_name
                and isinstance(self.node.product_name, NamesProp)
            ):
                foodex2_terms: list[Term] = [term.get_term() for term in self.node.product_name.terms]
                if any(term.uid in self.gfm_factory.non_food_term_uids for term in foodex2_terms):
                    logger.debug(f"[MatrixGFM]: Skipping non-food flow {self.node}")
                    return
            else:
                logger.warning(
                    f"No FoodEx2 term present in one of 'product_name' props of {self.node}! Setting unknown origin..."
                )
                await self.set_unknown_origin(calc_graph)

                calc_graph.append_data_errors_log_entry(
                    f"No FoodEx2 term present in one of 'product_name' props of {self.node}! Setting unknown origin..."
                )

                return

            fao_code_term_uid = self.gfm_factory.linked_fao_code_terms.get(
                frozenset([foodex2_term.uid for foodex2_term in foodex2_terms])
            )
            if fao_code_term_uid:
                fao_code_term = await self.gfm_factory.service_provider.glossary_service.get_term_by_uid(
                    fao_code_term_uid
                )
            else:
                fao_code_term = None

            if not fao_code_term:
                logger.debug(
                    f"No FAO code term present for FoodEx2 terms {foodex2_terms}! Using averaged import statistics..."
                )
                fao_code_term = self.gfm_factory.default_production_fao_code_term

            # handling special FAO codes for local/unknown products
            if fao_code_term.xid in (
                self.gfm_factory.local_production_fao_code_term.xid,
                self.gfm_factory.unknown_fish_production_fao_code_term.xid,
                self.gfm_factory.failed_production_fao_code_term.xid,
            ):
                logger.debug(
                    f"FAO code {fao_code_term.xid} has no trade data by default. Setting unknown origin...",
                    terms=foodex2_terms,
                    ingredient=str(self.node),
                )
                await self.set_unknown_origin(calc_graph, fao_code_term)

                return

            fao_code = int(fao_code_term.xid)

            stats_data_year_column = self.gfm_factory.primary_year

            domestic_production_row = self.gfm_factory.domestic_df[
                (self.gfm_factory.domestic_df["Area Code (M49)"] == kitchen_country_m49_code)
                & (self.gfm_factory.domestic_df["Item Code"] == fao_code)
            ]

            domestic_production_value = domestic_production_row[stats_data_year_column]

            # if there is no domestic production entry at all, we substitute it with zero,
            # which pretty much always leads to 'unknown origin' assumption...
            if not len(domestic_production_value):
                domestic_production_value = 0
            else:
                # if there is no domestic production entry for the primary year, we try to use the secondary one
                if pd.isnull(domestic_production_value.values[0]):
                    stats_data_year_column = self.gfm_factory.secondary_year
                    domestic_production_value = domestic_production_row[stats_data_year_column]

                domestic_production_value = domestic_production_value.values[0]

            self.gfm_factory.init_import_export_cache()

            # filtering this country's filtered import & export data
            try:
                import_value, export_value = self.gfm_factory.import_export_agg_cache.import_export_value(
                    kitchen_country_m49_code, fao_code, stats_data_year_column
                )
            except KeyError:
                import_value = 0
                export_value = 0

                # handling the case when all 3 values equal zero
                if domestic_production_value == 0:
                    logger.warning(
                        f"FAO code {fao_code} has neither trade data nor domestic production data for "
                        f"country {kitchen_country_code}. Setting unknown origin...",
                        foodex2_terms=foodex2_terms,
                        ingredient=str(self.node),
                    )
                    await self.set_unknown_origin(calc_graph)

                    calc_graph.append_data_errors_log_entry(
                        f"FAO code {fao_code} has neither trade data nor domestic production data for "
                        f"country {kitchen_country_code}. Setting unknown origin... "
                        f"(node: {self.node}) (terms: {foodex2_terms})"
                    )

                    return

            domestic_import_export_total = domestic_production_value + import_value - export_value

            # TODO: implement points 3/4 (more advanced ones) in inconsistencies description
            # TODO: keep '=' sign here?
            if export_value >= domestic_production_value + import_value:
                logger.warning(
                    f"FAO code {fao_code} export value is bigger than or equal to the sum of domestic "
                    f"and import values of country {kitchen_country_code}! Setting unknown origin...",
                    foodex2_terms=foodex2_terms,
                    ingredient=str(self.node),
                )
                await self.set_unknown_origin(calc_graph)

                calc_graph.append_data_errors_log_entry(
                    f"FAO code {fao_code} export value is bigger than or equal to the sum of domestic "
                    f"and import values of country {kitchen_country_code}! Setting unknown origin... "
                    f"(node: {self.node}) (terms: {foodex2_terms})"
                )

                return

            domestic_origin_share = (domestic_production_value - export_value) / domestic_import_export_total

            if domestic_origin_share > 0:
                foreign_origin_share = 1 - domestic_origin_share
            else:
                foreign_origin_share = 1

            if import_value:
                m49_country_share = self.gfm_factory.import_export_agg_cache.import_countries_top90percent(
                    kitchen_country_m49_code, fao_code, stats_data_year_column
                )
                country_share = {m49_to_iso_mapping[country]: value for country, value in m49_country_share["share"]}
                if m49_country_share.get("coverage"):
                    logger.debug(
                        f"To avoid too many origins for FAO code {fao_code_term.xid}, only the top 10 countries "
                        f"are taken into account for origin split. They make up for "
                        f"{m49_country_share['coverage']}% of the total import value.",
                        ingredient=str(self.node),
                    )

                for country, value in country_share.items():
                    country_total_percentage_share[country] = foreign_origin_share * value

            if domestic_origin_share > 0:
                country_total_percentage_share[kitchen_country_code] = domestic_origin_share
            is_origin_from_fao = True

        else:
            stats_data_year_column = None

            for country in origins_list:
                country_total_percentage_share[country.country_code] = 1 / len(origins_list)

        # now that we know shares of each country, we can create mutations
        if not self.node.get_sub_nodes():
            logger.warning(f"No sub activity node of {self.node} available. Skipping origin splitting.")
            return

        await self.apply_origin_split_to_graph(
            calc_graph,
            country_total_percentage_share,
            is_origin_from_fao=is_origin_from_fao,
            fao_code_term=fao_code_term,
            stats_data_year_column=stats_data_year_column,
        )

    # TODO: if we don't have FAO code specified in a monoproduct,
    #  then it'd be better to get it from parent "layers" etc.
    # TODO: destination, ??

    async def combined_product_origin_processing(self, calc_graph: "CalcGraph") -> None:
        origins_list = await self.parse_current_origins()

        if len(origins_list) == 1:
            logger.debug(f"{self.node} already has an origin specified.")

            return

        if len(origins_list) == 0:
            logger.debug(f"{self.node} already has no origin specified.")

            return

        else:
            country_total_percentage_share = {origin.country_code: 1 / len(origins_list) for origin in origins_list}

            # now that we know shares of each country, we can create mutations
            if not self.node.get_sub_nodes():
                logger.warning(f"No sub activity node of {self.node} available. Skipping origin splitting.")
                return

            await self.apply_origin_split_to_graph(calc_graph, country_total_percentage_share, is_combined_product=True)

    async def apply_origin_split_to_graph(
        self,
        calc_graph: "CalcGraph",
        country_total_percentage_share: dict,
        is_origin_from_fao: bool = False,
        fao_code_term: Optional[Term] = None,
        stats_data_year_column: Optional[str] = None,
        is_combined_product: bool = False,
    ) -> None:
        # 1) delete the edge between self.node and its sub-activity node
        old_food_product_node = self.node.get_sub_nodes()[0]

        if is_combined_product:
            current_node_subs = self.node.get_sub_nodes()[0].get_sub_nodes()
            for sub_node in current_node_subs:
                remove_edge_mutation = RemoveEdgeMutation(
                    created_by_module=self.__class__.__name__,
                    from_node_uid=old_food_product_node.uid,
                    to_node_uid=sub_node.uid,
                )
                calc_graph.apply_mutation(remove_edge_mutation)

        remove_edge_mutation = RemoveEdgeMutation(
            created_by_module=self.__class__.__name__,
            from_node_uid=self.node.uid,
            to_node_uid=old_food_product_node.uid,
        )
        calc_graph.apply_mutation(remove_edge_mutation)

        # 2) Add a new Origin-Split-Activity

        activity_node_uid = uuid.uuid4()
        activity_node = FoodProcessingActivityNode.model_construct(
            uid=activity_node_uid,
            access_group_uid=old_food_product_node.access_group_uid,
            access_group_xid=old_food_product_node.access_group_xid,
            gfm_state=GfmStateProp.unvalidated_construct(
                worker_states={
                    # No more origin split necessary:
                    "OriginGapFillingWorker": NodeGfmStateEnum.canceled,
                    # Ingredient amount estimation should be already finished:
                    "IngredientAmountEstimatorGapFillingWorker": NodeGfmStateEnum.canceled,
                }
            ),
            production_amount=(
                old_food_product_node.production_amount.duplicate() if old_food_product_node.production_amount else None
            ),
        )

        calc_graph.apply_mutation(
            AddNodeMutation(
                created_by_module=self.__class__.__name__,
                new_node=activity_node,
                parent_node_uid=self.node.uid,
                copy=False,
            )
        )

        # create origin_splits

        for index, (
            country_iso_code,
            percentage,
        ) in enumerate(country_total_percentage_share.items()):
            origin_split_flow_node_uid = uuid.uuid4()

            calc_graph.apply_mutation(
                DuplicateNodeMutation(
                    created_by_module=self.__class__.__name__,
                    new_node_uid=origin_split_flow_node_uid,
                    source_node_uid=self.node.uid,
                    parent_node_uid=activity_node_uid,
                    gfms_to_not_schedule=("OriginGapFillingWorker", "MatrixCalculationGapFillingWorker"),
                    duplicate_child_nodes=False,
                )
            )
            origin_split_flow_node = calc_graph.get_node_by_uid(origin_split_flow_node_uid)

            # we need to overwrite the "amount"
            # such that it corresponds to a production_amount of the parent.
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=origin_split_flow_node.uid,
                    prop_name="amount",
                    prop=QuantityProp.unvalidated_construct(
                        value=percentage * activity_node.production_amount.value,
                        unit_term_uid=activity_node.production_amount.unit_term_uid,
                        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
                    ),
                )
            )

            # We still want to run conservation GFM on origin split nodes.
            old_gfm_state = dict(origin_split_flow_node.gfm_state.worker_states).copy()
            if old_gfm_state.get("ConservationGapFillingWorker"):
                if old_gfm_state["ConservationGapFillingWorker"] == NodeGfmStateEnum.finished:
                    old_gfm_state["ConservationGapFillingWorker"] = NodeGfmStateEnum.scheduled
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=origin_split_flow_node.uid,
                    prop_name="gfm_state",
                    prop=GfmStateProp.unvalidated_construct(worker_states=old_gfm_state),
                )
            )

            # Erase glossary tags if source is eos_assumed
            if origin_split_flow_node.glossary_tags:
                new_glossary_tags = []
                for tag in origin_split_flow_node.glossary_tags:
                    if tag.source != SourceEnum.eos_assumed:
                        new_glossary_tags.append(tag.duplicate())

                calc_graph.apply_mutation(
                    PropListMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=origin_split_flow_node.uid,
                        prop_name="glossary_tags",
                        props=new_glossary_tags,
                        append=False,
                    )
                )

            # we need to overwrite the "amount_per_category_in_flow" and "animal products"
            # to avoid aggregating the food categories from each origin.
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=origin_split_flow_node.uid,
                    prop_name="amount_per_category_in_flow",
                    prop=QuantityPackageProp.unvalidated_construct(
                        quantities={}, for_reference=ReferenceAmountEnum.amount_for_100g
                    ),
                )
            )

            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=origin_split_flow_node.uid,
                    prop_name="animal_products",
                    prop=AnimalProductsProp.unvalidated_construct(
                        quantities={}, for_reference=ReferenceAmountEnum.amount_for_100g
                    ),
                )
            )

            country_code_3_letter = iso_3166_map_2_to_3_letter.get(country_iso_code)
            if country_code_3_letter in CHINA_SPECIAL_ADMINISTRATIVE_REGIONS:
                # there is no Hong Kong or Macau in GADM v4.1, so we use China instead:
                country_code_3_letter = "CHN"

            gadm_term = await self.gfm_factory.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid=country_code_3_letter,
                access_group_uid=str(self.gfm_factory.root_gadm_term.access_group_uid),
            )

            origin_location = LocationProp.unvalidated_construct(
                address=gadm_term.name,
                latitude=gadm_term.data.get("centroid_lat"),
                longitude=gadm_term.data.get("centroid_lon"),
                country_code=country_iso_code,
                term_uid=gadm_term.uid,
                location_qualifier=LocationQualifierEnum.known,
                source=LocationSourceEnum.fao_stat if is_origin_from_fao else LocationSourceEnum.gadm,
            )

            await self.set_origin_prop(
                origin_split_flow_node,
                origin_location,
                calc_graph,
                fao_code_term,
                stats_data_year_column,
            )

            # 3) add a new edge between origin_split1 and old food_product

            if index == len(country_total_percentage_share.items()) - 1:
                add_edge_mutation = AddEdgeMutation(
                    created_by_module=self.__class__.__name__,
                    from_node_uid=origin_split_flow_node.uid,
                    to_node_uid=old_food_product_node.uid,
                )
                calc_graph.apply_mutation(add_edge_mutation)

                food_product_node = old_food_product_node
            else:
                new_food_product_node_uid = uuid.uuid4()

                calc_graph.apply_mutation(
                    DuplicateNodeMutation(
                        created_by_module=self.__class__.__name__,
                        new_node_uid=new_food_product_node_uid,
                        source_node_uid=old_food_product_node.uid,
                        parent_node_uid=origin_split_flow_node.uid,
                        duplicate_child_nodes=True,
                        gfms_to_not_schedule=("OriginGapFillingWorker",),
                    )
                )
                food_product_node = calc_graph.get_node_by_uid(new_food_product_node_uid)

            if is_combined_product:
                for sub_node in current_node_subs:
                    if index == len(country_total_percentage_share.items()) - 1:
                        add_edge_mutation = AddEdgeMutation(
                            created_by_module=self.__class__.__name__,
                            from_node_uid=food_product_node.uid,
                            to_node_uid=sub_node.uid,
                        )
                        calc_graph.apply_mutation(add_edge_mutation)
                    else:
                        new_sub_node_uid = uuid.uuid4()

                        calc_graph.apply_mutation(
                            DuplicateNodeMutation(
                                created_by_module=self.__class__.__name__,
                                new_node_uid=new_sub_node_uid,
                                source_node_uid=sub_node.uid,
                                parent_node_uid=food_product_node.uid,
                                duplicate_child_nodes=True,
                            )
                        )

            # making sure that this parent product node also has origin specified
            # fao_code_term and stats_data_year_column are not added to the sub-activity nodes as these properties
            # only belong to the flow nodes (the information is already available from the parent flow node).
            await self.set_origin_prop(
                food_product_node,
                origin_location,
                calc_graph,
            )

        # Need to clear self.node.location property, because it was now already split, so that the transport GFM will
        # run from the actual origin to the destination given by the parent of this node.
        prop_mutation = PropListMutation(
            created_by_module=self.__class__.__name__,
            node_uid=self.node.uid,
            prop_name="flow_location",
            props=[],
            append=False,
        )
        calc_graph.apply_mutation(prop_mutation)

    async def parse_current_origins(self) -> list[LocationProp]:
        location_props = self.node.flow_location

        if location_props:
            locations = []

            for location_prop in location_props:
                location_qualifier = location_prop.location_qualifier

                if location_term := location_prop.get_term():
                    term_xid = location_term.xid
                else:
                    term_xid = ""

                # handle regional terms
                if term_xid in location_to_regional_term_xid_map.values():
                    # blacklisting already listed countries
                    already_listed_regions = [l_prop.get_term().xid for l_prop in location_props]

                    for region in regional_term_xid_to_region_gadm_codes_map.get(term_xid, []):
                        if region not in already_listed_regions:
                            country_code = iso_3166_map_3_to_2_letter.get(region.split(".")[0])
                            glossary_service = self.gfm_factory.service_provider.glossary_service
                            gadm_term = await glossary_service.get_term_by_xid_and_access_group_uid(
                                term_xid=region,
                                access_group_uid=str(self.gfm_factory.root_gadm_term.access_group_uid),
                            )
                            locations.append(
                                LocationProp.unvalidated_construct(
                                    address=gadm_term.name,
                                    latitude=gadm_term.data.get("centroid_lat"),
                                    longitude=gadm_term.data.get("centroid_lon"),
                                    country_code=country_code,
                                    term_uid=gadm_term.uid,
                                    location_qualifier=location_qualifier,
                                    source=LocationSourceEnum.gadm,
                                )
                            )

                    continue

                locations.append(
                    LocationProp.unvalidated_construct(
                        address=location_prop.address,
                        latitude=location_prop.latitude,
                        longitude=location_prop.longitude,
                        country_code=location_prop.country_code,
                        term_uid=location_prop.term_uid,
                        location_qualifier=location_qualifier,
                        source=LocationSourceEnum.google,
                    )
                )

            return locations
        else:
            return []

    async def set_unknown_origin(self, calc_graph: CalcGraph, fao_code_term: Term = None) -> None:
        unknown_location = LocationProp.unvalidated_construct(
            address="",
            latitude=0.0,
            longitude=0.0,
            country_code=None,
            term_uid=None,
            location_qualifier=LocationQualifierEnum.unknown,
            source=LocationSourceEnum.unknown,
        )

        await self.set_origin_prop(
            self.node,
            unknown_location,
            calc_graph,
            fao_code_term=self.gfm_factory.failed_production_fao_code_term if fao_code_term is None else fao_code_term,
        )

    async def set_origin_prop(
        self,
        node: Node,
        location: LocationProp,
        calc_graph: "CalcGraph",
        fao_code_term: Term = None,
        stats_data_year_column: str = None,
    ) -> None:
        if isinstance(node, ActivityNode):
            prop_name = "activity_location"
        else:
            prop_name = "flow_location"

        prop_mutation = PropListMutation(
            created_by_module=self.__class__.__name__,
            node_uid=node.uid,
            prop_name=prop_name,
            props=[location.duplicate()],
            append=False,
        )

        calc_graph.apply_mutation(prop_mutation)

        # using a separate property for storing FAO term
        origin_data_dict = {}

        if fao_code_term:
            origin_data_dict["fao_code"] = GlossaryTermProp.unvalidated_construct(term_uid=fao_code_term.uid)

        if stats_data_year_column:
            origin_data_dict["stats_data_year_column"] = stats_data_year_column[1:]

        if origin_data_dict:
            # Seems not to be tested.
            prop_mutation = PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=node.uid,
                prop_name="origin_data",
                prop=OriginDataProp.unvalidated_construct(**origin_data_dict),
            )

            calc_graph.apply_mutation(prop_mutation)


class ImportExportCache:
    def __init__(self, filename: str):
        self.import_export_data = {}
        self.filename = filename

    def import_export_value(self, m49_code: int, fao_code: int, year_column: str) -> tuple[int, int]:
        "Returns import and export sums from cache."
        return self.import_export_data[(m49_code, fao_code)][year_column]

    def import_countries_top90percent(self, m49_code: int, fao_code: int, year_column: str) -> dict:
        "Returns import relative share between countries covering the top 90 percent."
        return self.import_export_data["top90"][(m49_code, fao_code)][year_column]

    def serialize(self) -> None:
        with open(self.filename, "wb") as f:
            f.write(msgpack.packb(self.import_export_data))

    def deserialize(self) -> "ImportExportCache":
        with open(self.filename, "rb") as f:
            o = ImportExportCache(self.filename)
            o.import_export_data = msgpack.unpackb(f.read(), use_list=False, strict_map_key=False)
            return o


class OriginGapFillingFactory(AbstractGapFillingFactory):
    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.glossary_link_service: GlossaryLinkService = service_provider.glossary_link_service
        self.domestic_df: pd.DataFrame = None  # noqa
        self.import_export_cache: dict[tuple, pd.DataFrame] = None  # noqa
        self.import_export_agg_cache: ImportExportCache = None  # noqa
        self.linked_fao_code_terms: dict[frozenset[uuid.UUID], uuid.UUID] = {}
        self.non_food_term_uids: frozenset[uuid.UUID] = frozenset()

        self.unknown_region_term: Term = None  # noqa
        self.root_gadm_term = self.service_provider.glossary_service.root_subterms.get("Root_GADM")
        assert self.root_gadm_term, "No root GADM term found."

        self.local_production_fao_code_term: Term = None  # noqa
        self.default_production_fao_code_term: Term = None  # noqa
        self.unknown_fish_production_fao_code_term: Term = None  # noqa
        self.failed_production_fao_code_term: Term = None  # noqa
        self.root_fao_term = self.service_provider.glossary_service.root_subterms.get("Root_FAO")
        assert self.root_fao_term, "No root FAO term found."

        self.origin_gfm_settings = OriginGfmSettings()

        self.gfm_dir = os.path.join(self.origin_gfm_settings.TEMP_DIR, "origin_gfm")
        os.makedirs(self.gfm_dir, exist_ok=True)

        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", self.service_provider.glossary_service.root_subterms["EOS_units"].access_group_uid)
        ]

        self.db_fao_snapshot_suffix = self.root_fao_term.data.get("fao_snapshot_suffix")
        if not self.db_fao_snapshot_suffix:
            logger.warning(
                "No FAO snapshot suffix found in the database, using the one provided in the "
                "settings to update the database."
            )
            self.fao_snapshot_suffix = self.origin_gfm_settings.FAO_SNAPSHOT_SUFFIX
        else:
            logger.info(f"FAO snapshot suffix found in the database: {self.db_fao_snapshot_suffix}")
            self.fao_snapshot_suffix = self.db_fao_snapshot_suffix
        self.initialize_file_names(self.fao_snapshot_suffix)

    def initialize_file_names(self, fao_snapshot_suffix: str) -> None:
        (
            self.primary_year,
            self.secondary_year,
            version_identifier,
        ) = fao_snapshot_suffix.split("_")

        # source file-names
        self.import_export_zip_source_file_name: str = f"import_export_{version_identifier}.zip"
        self.domestic_zip_source_file_name: str = f"domestic_{version_identifier}.zip"

        # cache file-names
        self.domestic_cache_file_name = f"domestic_df_{fao_snapshot_suffix}.hdf5"
        self.import_export_cache_file_name = f"import_export_cache_{fao_snapshot_suffix}.msgpack"
        self.itemcodes_cache_file_name = f"item_codes_{fao_snapshot_suffix}.csv"

        # cache paths
        self.import_export_cache_path: str = os.path.join(self.gfm_dir, self.import_export_cache_file_name)
        self.domestic_cache_path: str = os.path.join(self.gfm_dir, self.domestic_cache_file_name)

    async def init_cache(self) -> None:
        root_access_group_uid = self.service_provider.glossary_service.root_term.access_group_uid
        foodex2_access_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )
        self.non_food_term_uids = frozenset(
            [
                self.service_provider.glossary_service.terms_by_xid_ag_uid[("EAT-0002", foodex2_access_group_uid)].uid,
                self.service_provider.glossary_service.terms_by_xid_ag_uid[
                    ("EOS_data_error", root_access_group_uid)
                ].uid,
            ]
        )

        self.unknown_region_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("R0001", foodex2_access_group_uid)
        ]
        assert self.unknown_region_term, "No 'unknown region' term found."

        # loading custom FAO codes
        self.local_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="100000",
                access_group_uid=str(self.root_fao_term.access_group_uid),
            )
        )
        assert self.local_production_fao_code_term, "No 'local production' term found."

        self.default_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="200000",
                access_group_uid=str(self.root_fao_term.access_group_uid),
            )
        )
        assert self.default_production_fao_code_term, "No 'default production' term found."

        self.unknown_fish_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="300000",
                access_group_uid=str(self.root_fao_term.access_group_uid),
            )
        )
        assert self.unknown_fish_production_fao_code_term, "No 'unknown fish production' term found."

        self.failed_production_fao_code_term = (
            await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                term_xid="400000",
                access_group_uid=str(self.root_fao_term.access_group_uid),
            )
        )
        assert self.failed_production_fao_code_term, "No 'failed production' term found."

        await self.make_sure_cache_files_exist()
        fao_terms = self.service_provider.glossary_service.get_sub_terms_by_uid(self.root_fao_term.uid, depth=10)
        if self.db_fao_snapshot_suffix != self.fao_snapshot_suffix or len(fao_terms) < 500:
            # we need to update the fao-terms
            await self.process_fao_codes_data_from_fao(self.root_fao_term.uid, self.root_fao_term.access_group_uid)
            if self.db_fao_snapshot_suffix != self.fao_snapshot_suffix:
                # we need to update root-fao-term
                await self.update_root_fao_term(self.fao_snapshot_suffix)
            else:
                logger.error("Terms needed to be inserted even though the snapshot suffix was up to date.")

        # loading the lightest 2 files from local storage
        domestic_df = pd.read_hdf(self.domestic_cache_path)
        aggregated_for_groups = ["Area Code (M49)"]
        self.domestic_df = self.add_aggregated_columns(domestic_df, aggregated_for_groups)

        fao_glossary_links = await self.service_provider.glossary_link_service.get_glossary_links_by_gfm(
            gap_filling_module="FAO"
        )
        for fao_glossary_link in fao_glossary_links:
            self.linked_fao_code_terms[frozenset(fao_glossary_link.term_uids)] = fao_glossary_link.linked_term_uid

        logger.info("Starting to cache import/export statistics...")
        start = time.time()
        self.init_import_export_cache()
        duration = time.time() - start
        logger.info("import/export statistics cache loading finished.", duration=duration)

    def add_aggregated_columns(self, input_df: pd.DataFrame, aggregated_for_groups: list[str]) -> pd.DataFrame:
        """Aggregate over all products and add the aggregated columns to the DataFrame."""
        # Group by the relevant country codes.
        grouped = input_df.groupby(aggregated_for_groups)
        # Apply custom aggregation
        agg_rules = {col: "first" for col in input_df.columns}
        agg_rules[self.secondary_year] = "sum"
        agg_rules[self.primary_year] = "sum"
        aggregated_df = grouped.agg(agg_rules)
        # Set 'Item Code' and 'Item' for the summed entries
        aggregated_df["Item Code"] = int(self.default_production_fao_code_term.xid)
        aggregated_df["Item"] = "default_fao_statistics"

        # Concatenate the totals DataFrame with the original DataFrame.
        output_df = pd.concat([input_df, aggregated_df], ignore_index=True)

        return output_df

    def init_import_export_cache(self) -> None:
        if self.import_export_agg_cache is not None:
            return

        try:
            # try loading existing precomputed cache
            self.import_export_agg_cache = ImportExportCache(self.import_export_cache_path).deserialize()
            return
        except Exception as e:
            logger.error(f"loading precomputed cache failed {e}")
            raise e

    def sqlite_to_cache(self, trade_data: object, write_to_local_file: bool = True) -> None:
        "Write the aggregated data into the msgpack cache file."
        self.import_export_agg_cache = ImportExportCache(self.import_export_cache_path)
        cursor = trade_data.cursor()
        # replacing self.init_import_export_cache()
        # replacing self.add_aggregated_columns() call:
        #
        # group by "Reporter Country Code (M49)", "Partner Country Code (M49)",
        # separate by content of "Element" (to know if import/export)
        # later groups by "Reporter Country Code (M49)" and "Item Code" and appends these rows to cache,
        export_item_country = cursor.execute(
            f"""
            SELECT "Reporter Country Code (M49)",
                "Item Code",
                SUM(CASE WHEN "Element" = "Import Quantity"
                    THEN COALESCE({self.secondary_year}, 0)
                    ELSE 0
                    END
                    ) as "I_{self.secondary_year}",
                SUM(CASE WHEN "Element" = "Import Quantity"
                    THEN COALESCE({self.primary_year}, 0)
                    ELSE 0
                    END) as "I_{self.primary_year}",
                SUM(CASE WHEN "Element" = "Export Quantity"
                    THEN COALESCE({self.secondary_year}, 0)
                    ELSE 0
                    END
                    ) as "E_{self.secondary_year}",
                SUM(CASE WHEN "Element" = "Export Quantity"
                    THEN COALESCE({self.primary_year}, 0)
                    ELSE 0
                    END) as "E_{self.primary_year}"
            FROM trade_data
            GROUP BY "Reporter Country Code (M49)",
                "Item Code"
                """
        )

        self.import_export_agg_cache.import_export_data = {
            (r[0], r[1]): {
                self.secondary_year: (r[2], r[4]),
                self.primary_year: (r[3], r[5]),
            }
            for r in export_item_country
        }

        country2country = cursor.execute(
            f"""
            SELECT "Reporter Country Code (M49)",
                COUNT("Partner Country Code (M49)"),
                SUM("I_{self.secondary_year}") as "I_{self.secondary_year}",
                SUM("I_{self.primary_year}") as "I_{self.primary_year}",
                SUM("E_{self.secondary_year}") as "E_{self.secondary_year}",
                SUM("E_{self.primary_year}") as "E_{self.primary_year}"
            FROM (
            SELECT "Reporter Country Code (M49)",
                "Partner Country Code (M49)",
                SUM(CASE WHEN "Element" = "Import Quantity"
                    THEN COALESCE({self.secondary_year}, 0)
                    ELSE 0
                    END
                    ) as "I_{self.secondary_year}",
                SUM(CASE WHEN "Element" = "Import Quantity"
                    THEN COALESCE({self.primary_year}, 0)
                    ELSE 0
                    END) as "I_{self.primary_year}",
                SUM(CASE WHEN "Element" = "Export Quantity"
                    THEN COALESCE({self.secondary_year}, 0)
                    ELSE 0
                    END
                    ) as "E_{self.secondary_year}",
                SUM(CASE WHEN "Element" = "Export Quantity"
                    THEN COALESCE({self.primary_year}, 0)
                    ELSE 0
                    END) as "E_{self.primary_year}"
            FROM trade_data
            GROUP BY "Reporter Country Code (M49)",
                "Partner Country Code (M49)"
                ) as c2c
            GROUP BY "Reporter Country Code (M49)"
                """
        )
        for r in country2country:
            self.import_export_agg_cache.import_export_data[(r[0], int(self.default_production_fao_code_term.xid))] = {
                self.secondary_year: (r[2], r[4]),
                self.primary_year: (r[3], r[5]),
            }

        # build cache for sum() of 90% of import Quantity per
        # (kitchen_country_m49_code, fao_code, stats_data_year_column) tuple
        # goal is to find the list of countries that make up 90% of the import

        self.import_export_agg_cache.import_export_data["top90"] = dict()
        top90_percent = self.import_export_agg_cache.import_export_data["top90"]
        for amount_column in (self.secondary_year, self.primary_year):
            partner_kitchen_top_90_percent = f"""
    SELECT
        a."Reporter Country Code (M49)",
        a."Partner Country Code (M49)",
        a."Item Code",
        a.year_import,
        a.cumulative,
        b.proz90
    FROM
    (
        -- Cumulative sum of descending order of trade import amounts
        SELECT
            "Reporter Country Code (M49)",
            "Partner Country Code (M49)",
            "Item Code",
            COALESCE("{amount_column}", 0) as year_import,
            SUM(COALESCE("{amount_column}", 0))
                OVER (
                    PARTITION BY "Reporter Country Code (M49)", "Item Code"
                    ORDER BY "{amount_column}" DESC
                ) as cumulative
        FROM trade_data
        WHERE "Element" = "Import Quantity"
    ) as a
    JOIN
    (
        -- for performance reasons, compute 90% of total trade imports per (country, item)
        SELECT
            "Reporter Country Code (M49)",
            "Item Code",
            SUM(COALESCE("{amount_column}", 0)) * 0.9 as proz90
        FROM trade_data
        WHERE "Element" = "Import Quantity"
        GROUP BY "Reporter Country Code (M49)",
            "Item Code"
    ) as b
    ON a."Reporter Country Code (M49)" = b."Reporter Country Code (M49)"
        AND a."Item Code" = b."Item Code"
    ORDER BY a."Reporter Country Code (M49)", a."Item Code", a.cumulative ASC, a.year_import DESC
            """
            current_country_item_partners = []
            # since this is sorted by cumulative import amounts we can stop considering rows after we reached
            # 90% of total imports (precomputed in sub-query b above)
            for row in cursor.execute(partner_kitchen_top_90_percent):
                m49reporter, m49partner, item_code, year_import, cumulative, row_90_perc = row
                current_row = (m49reporter, item_code)
                assert (
                    cumulative * 0.99 <= row_90_perc / 0.9
                ), f"cumulative {cumulative} is bigger than total import {row_90_perc}"
                if row_90_perc == 0:
                    logger.debug(f"total import is zero {current_row}")
                    continue
                if current_row not in top90_percent or amount_column not in top90_percent[current_row]:
                    current_country_item_partners.append((m49partner, year_import))
                    if cumulative > row_90_perc:
                        total_import = sum((v for _, v in current_country_item_partners))
                        assert total_import > 0, (
                            f"total import is not bigger than zero {current_row}. "
                            "This shouldn't happen since we checked before for row_90_perc == 0."
                        )
                        partner_share = {
                            "share": tuple(
                                (partner, partner_import / total_import)
                                for partner, partner_import in current_country_item_partners
                            )
                        }
                        if current_row not in top90_percent:
                            top90_percent[current_row] = {}
                        top90_percent[current_row][amount_column] = partner_share
                        current_country_item_partners = []

        # for default FAO XID take the top 10 countries (to limit the node depth)
        # additionally compute how many percent of the total trade they make up, needed for logmessage
        for amount_column in (self.secondary_year, self.primary_year):
            cursor.execute(
                f"""
            SELECT "Reporter Country Code (M49)",
                "Partner Country Code (M49)",
                SUM(COALESCE("{amount_column}", 0)) as "total_import"
            FROM trade_data
            WHERE "Element" = "Import Quantity"
            GROUP BY "Reporter Country Code (M49)",
                "Partner Country Code (M49)"
            ORDER BY "Reporter Country Code (M49)",
                "total_import" DESC
                """
            )
            import_country2country = cursor.fetchall()
            total_rows = len(import_country2country)

            current_country = 0
            current_country_partners = []

            for index, r in enumerate(import_country2country):
                m49reporter, m49partner, country_import = r
                if current_country != m49reporter or index == total_rows - 1:
                    if current_country:
                        top10_total_import = sum((v for _, v in current_country_partners[:10]))
                        total_import = sum((v for _, v in current_country_partners))
                        if top10_total_import == 0:
                            logger.debug(f"total import is zero {m49reporter}, {m49partner}")
                        else:
                            # only take the top 10 countries to avoid too many origins in this case
                            partner_share = {
                                "share": tuple(
                                    (partner, partner_import / top10_total_import)
                                    for partner, partner_import in current_country_partners[:10]
                                )
                            }
                            # if there are too many countries, provide the coverage for warning message
                            if len(current_country_partners) > 10:
                                partner_share["coverage"] = round(100 * top10_total_import / total_import, 2)
                            try:
                                self.import_export_agg_cache.import_export_data["top90"][
                                    (current_country, int(self.default_production_fao_code_term.xid))
                                ][amount_column] = partner_share
                            except KeyError:
                                self.import_export_agg_cache.import_export_data["top90"][
                                    (current_country, int(self.default_production_fao_code_term.xid))
                                ] = {amount_column: partner_share}
                    current_country_partners = []
                    current_country = m49reporter
                current_country_partners.append((m49partner, country_import))

        # export to local cache file
        if write_to_local_file:
            self.import_export_agg_cache.serialize()

    def create_drive_service(self) -> Optional[Resource]:
        if not os.path.isfile(self.origin_gfm_settings.GDRIVE_SERVICE_ACCOUNT_FILE):
            logger.error("Google Drive service account file not found.")
            return None

        credentials, _ = google.auth.load_credentials_from_file(
            filename=self.origin_gfm_settings.GDRIVE_SERVICE_ACCOUNT_FILE,
            scopes=[
                "https://www.googleapis.com/auth/devstorage.read_write",
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/drive",
            ],
        )

        try:
            drive_service = build("drive", "v3", credentials=credentials)
        except HttpError as error:
            logger.error(f"An error occurred: {error}")
            return None
        return drive_service

    def get_drive_file_info_by_name(
        self, drive_service: Resource
    ) -> Tuple[Optional[Resource], Optional[dict[str, dict[str, str]]]]:
        query = (
            f"'{self.origin_gfm_settings.GDRIVE_FAO_FOLDER_ID}' in parents "
            "and ("
            f"name = '{self.domestic_cache_file_name}'"
            f"or name = '{self.itemcodes_cache_file_name}'"
            f"or name = '{self.import_export_cache_file_name}'"
            f"or name = '{self.import_export_zip_source_file_name}'"
            f"or name = '{self.domestic_zip_source_file_name}'"
            ")"
        )

        # Create the request object
        drive_request_obj = drive_service.files().list(q=query, pageSize=30, fields="files(id, name, md5Checksum)")

        # Execute the request
        results = drive_request_obj.execute()
        items = results.get("files", [])

        # Construct the dictionary with filename as the key, and id and hash as values
        drive_file_info_by_name = {
            item["name"]: {"id": item["id"], "hash": item.get("md5Checksum", "")} for item in items
        }

        # Close the HTTP session to avoid SSL warnings (this is a known issue with Google Drive API)
        drive_request_obj.http.close()

        return drive_file_info_by_name

    async def get_file_from_third_party_source(self, request_url: str, filename: str) -> None:
        save_file_path = os.path.join(self.gfm_dir, filename)

        async with httpx.AsyncClient(timeout=60 * 30) as session:
            resp = await session.get(request_url)
            if resp.status_code == 200:
                try:
                    async with aiofiles.open(save_file_path, mode="wb") as f:
                        await f.write(resp.read())
                except Exception as e:
                    os.remove(save_file_path)
                    logger.error("Error downloading third-party file", error=e)
                    raise e
            else:
                logger.error("Error downloading third-party file", status=resp.status_code)
                raise Exception("Error downloading third-party file")

    def download_file_from_google_drive(self, drive_service: Resource, filename: str, file_id: str) -> None:
        logger.info("download file from gdrive", filename=filename, id=file_id)

        save_file_path = os.path.join(self.gfm_dir, filename)

        request = drive_service.files().get_media(fileId=file_id)
        fh = FileIO(save_file_path, "wb")
        downloader = MediaIoBaseDownload(fh, request)

        done = False

        while not done:
            try:
                _, done = downloader.next_chunk()
            except Exception as e:
                fh.close()
                os.remove(save_file_path)
                raise Exception from f"Exception {e} downloading file from Google Drive!"

        logger.info("Download finished.", filename=filename, id=file_id)
        fh.close()
        request.http.close()

    def upload_cache_file_to_google_drive(self, drive_service: Resource, filename: str) -> None:
        logger.debug("upload origin GFM cache file to gdrive", filename=filename)

        file_path = os.path.join(self.gfm_dir, filename)
        file_metadata = {
            "name": filename,
            "parents": [
                self.origin_gfm_settings.GDRIVE_FAO_FOLDER_ID,
            ],
        }
        media = MediaFileUpload(file_path, resumable=True)
        # FIXME: this uploads the file with the same filename multiple times to Google Drive
        # to avoid this, use `files().update()` with the respective file-id and change this function
        # to become create-or-upload
        request = drive_service.files().create(body=file_metadata, media_body=media, fields="id")
        result = request.execute()

        logger.debug("origin GFM cache file uploaded", filename=filename, result=result)
        request.http.close()

    async def download_fao_source_files(self) -> None:
        await self.get_file_from_third_party_source(
            request_url=self.origin_gfm_settings.FAO_DOMESTIC_ZIP_URL,
            filename=self.domestic_zip_source_file_name,
        )
        await self.get_file_from_third_party_source(
            request_url=self.origin_gfm_settings.FAO_IMPORT_EXPORT_ZIP_URL,
            filename=self.import_export_zip_source_file_name,
        )

    async def rebuild_fao_cache_files(self) -> None:
        await self.process_import_export_data_from_fao()
        await self.process_domestic_production_data_from_fao()
        self.write_fao_codes_to_cache_file()

    async def make_sure_cache_files_exist(self) -> None:
        """Validate that the files exist locally, if not try to download them from google drive."""
        all_cache_file_names = (
            self.import_export_cache_file_name,
            self.domestic_cache_file_name,
            self.itemcodes_cache_file_name,
        )
        all_cache_file_paths = [os.path.join(self.gfm_dir, fn) for fn in all_cache_file_names]
        all_source_file_names = (
            self.import_export_zip_source_file_name,
            self.domestic_zip_source_file_name,
        )
        all_source_file_paths = [os.path.join(self.gfm_dir, fn) for fn in all_source_file_names]

        # 1. check if all cache files exist locally
        if all([os.path.exists(path) for path in all_cache_file_paths]):
            return

        # 2. check if all cache file exist on google drive
        logger.warning("not all local origin GFM cache files exist, trying google drive download.")
        drive_service = self.create_drive_service()
        drive_file_infos_by_name = self.get_drive_file_info_by_name(drive_service)
        if drive_file_infos_by_name is not None and all(
            [filename in drive_file_infos_by_name.keys() for filename in all_cache_file_names]
        ):
            logger.info("all origin GFM cache files found on google drive, downloading...")
            for filename in all_cache_file_names:
                self.download_file_from_google_drive(drive_service, filename, drive_file_infos_by_name[filename]["id"])
            return

        # 3. rebuild the cache files ourselves
        # 3.1. check if all source files exist locally
        logger.warning("not all origin GFM cache files found on google drive, trying to regenerate them.")
        if all([os.path.exists(path) for path in all_source_file_paths]):
            logger.info("all origin GFM source files found locally, rebuilding cache files...")
            await self.rebuild_fao_cache_files()
            return

        # 3.2. check if all source files exist on google drive
        if drive_file_infos_by_name is not None and all(
            [filename in drive_file_infos_by_name.keys() for filename in all_source_file_names]
        ):
            logger.info("all origin GFM source files found on google drive, downloading...")
            for filename in all_source_file_names:
                self.download_file_from_google_drive(drive_service, filename, drive_file_infos_by_name[filename]["id"])
            logger.info("all origin GFM source files downloaded, rebuilding cache files...")
            await self.rebuild_fao_cache_files()
            return

        # 3.3. download source files from third-party source
        logger.warning(
            "not all origin GFM source files found on google drive, trying to download from third-party source."
        )
        await self.download_fao_source_files()
        logger.info("all origin GFM source files downloaded, rebuilding cache files...")
        await self.rebuild_fao_cache_files()

    async def process_import_export_data_from_fao(self, write_to_local_file: bool = True) -> None:
        "Loads FAO data directly from ZIP file and writes the msgpack format cache."
        assert self.default_production_fao_code_term, "No 'default production' term found."

        trade_data = sqlite3.connect(":memory:")
        cursor = trade_data.cursor()
        create_table = f"""CREATE TABLE IF NOT EXISTS trade_data (
            "index" BIGINT,
            "Reporter Country Code" BIGINT,
            "Reporter Country Code (M49)" BIGINT,
            "Reporter Countries" TEXT,
            "Partner Country Code" BIGINT,
            "Partner Country Code (M49)" BIGINT,
            "Partner Countries" TEXT,
            "Item Code" BIGINT,
            "Item Code (CPC)" TEXT,
            "Item" TEXT,
            "Element Code" BIGINT,
            "Element" TEXT,
            "Unit" TEXT,
            {self.secondary_year} FLOAT,
            {self.primary_year} FLOAT
        );"""
        try:
            cursor.execute(create_table)
            cursor.execute('CREATE INDEX ix_trade_data_index ON trade_data ("index")')
        except sqlite3.OperationalError:
            return  # index existing is okay means we don't have to load
        convert_cols = {
            "Reporter Country Code": int,
            "Partner Country Code": int,
            "Item Code": int,
            "Element Code": int,
            self.secondary_year: float,
            self.primary_year: float,
        }

        sql = None

        def archive_reader() -> dict:
            "A generator producing rows of the CSV found inside the ZIP file."
            with ZipFile(os.path.join(self.gfm_dir, self.import_export_zip_source_file_name), "r") as zip_file:
                with zip_file.open(self.origin_gfm_settings.IMPORT_EXPORT_SOURCE_FILE_NAME) as f:
                    csv_header = None
                    imported = 0
                    year_cols = []
                    for line_no, row in enumerate(csv.reader(io.TextIOWrapper(f, encoding="latin-1"))):
                        if line_no == 0:
                            csv_header = row[0:12]
                            year_cols = [
                                i for i, col in enumerate(row) if self.primary_year in col or self.secondary_year in col
                            ]
                            csv_header += [row[i] for i in year_cols]
                            continue
                        dict_row = {k: v for k, v in zip(csv_header, row[0:12] + [row[i] for i in year_cols])}
                        # In the newer files, unit is "t" not "tonnes."
                        # We want to drop all rows that don't have tonnes as unit,
                        # other units are "Head", "1000 Head", "No" TODO implement those?
                        # if dict_row["Unit"] != "tonnes":
                        if dict_row["Unit"] not in ("tonnes", "t"):
                            continue
                        # we only care about Import/Export Quantity
                        if dict_row["Element"] in ("Export Value", "Import Value"):
                            continue
                        # skip when both years columns are invalid
                        if not dict_row[self.secondary_year] and not dict_row[self.primary_year]:
                            continue
                        dict_row["Reporter Country Code (M49)"] = int(
                            dict_row["Reporter Country Code (M49)"].replace("'", "")
                        )
                        dict_row["Partner Country Code (M49)"] = int(
                            dict_row["Partner Country Code (M49)"].replace("'", "")
                        )
                        imported += 1
                        for k, f in convert_cols.items():
                            try:
                                dict_row[k] = f(dict_row[k])
                            except ValueError:
                                dict_row[k] = None
                        dict_row["index"] = imported
                        # rename columns for placeholders
                        uscore_row = {k.replace(" ", "_"): v for k, v in dict_row.items()}
                        yield uscore_row

        row_gen = archive_reader()
        first_row = next(row_gen)
        columns = ", ".join((f'"{k.replace("_", " ")}"' for k in first_row.keys()))
        placeholders = ":" + ", :".join((k for k in first_row.keys()))
        sql = f"INSERT INTO trade_data ({columns}) VALUES ({placeholders})"
        trade_data.execute(sql, first_row)
        trade_data.executemany(sql, row_gen)
        trade_data.commit()

        self.sqlite_to_cache(trade_data, write_to_local_file=write_to_local_file)

    async def check_fao_codes_terms_presence(self) -> None:
        fao_terms = self.service_provider.glossary_service.get_sub_terms_by_uid(self.root_fao_term.uid, depth=10)
        if len(fao_terms) < 500:
            await self.process_fao_codes_data_from_fao(self.root_fao_term.uid, self.root_fao_term.access_group_uid)

    def write_fao_codes_to_cache_file(self) -> None:
        if not os.path.exists(os.path.join(self.gfm_dir, self.itemcodes_cache_file_name)):
            assert os.path.exists(os.path.join(self.gfm_dir, self.import_export_zip_source_file_name))

            # unzip the archive
            with ZipFile(os.path.join(self.gfm_dir, self.import_export_zip_source_file_name), "r") as zip_file:
                temp_extracted_file_path = os.path.join(
                    self.gfm_dir, self.origin_gfm_settings.ITEMCODES_SOURCE_FILE_NAME
                )
                zip_file.extract(
                    member=self.origin_gfm_settings.ITEMCODES_SOURCE_FILE_NAME,
                    path=self.gfm_dir,
                )
                output_filename = os.path.join(self.gfm_dir, self.itemcodes_cache_file_name)
                shutil.move(temp_extracted_file_path, output_filename)

    async def process_fao_codes_data_from_fao(
        self, fao_root_term_uid: uuid.UUID, fao_access_group_uid: uuid.UUID
    ) -> None:
        # load import/export data from FAO

        # open source CSV file
        fao_codes_data = pd.read_csv(
            os.path.join(self.gfm_dir, self.itemcodes_cache_file_name),
            encoding="latin-1",
        )

        terms_to_insert = []

        for _, row in fao_codes_data.iterrows():
            fao_id = str(row.get("Item Code"))
            fao_description = row.get("Item")

            # check if a term with such FAO code exists, so that there won't be any upsert conflict
            existing_term = await self.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
                fao_id,
                str(fao_access_group_uid),
            )

            if existing_term:
                term_uid = existing_term.uid
            else:
                term_uid = uuid.uuid4()

            new_term = Term(
                uid=term_uid,
                xid=fao_id,
                name=fao_description,
                sub_class_of=fao_root_term_uid,
                data={},
                access_group_uid=fao_access_group_uid,
            )
            terms_to_insert.append(new_term)

        logger.debug("inserting terms into glossary", num_terms=len(terms_to_insert))

        await self.service_provider.glossary_service.put_many_terms(terms_to_insert)

    async def update_root_fao_term(
        self, fao_snapshot_suffix: str, file_hashes: Optional[Dict[str, str]] = None
    ) -> None:
        fao_root_term = await self.service_provider.glossary_service.get_term_by_uid(self.root_fao_term.uid)
        fao_root_term_data = fao_root_term.data
        fao_root_term_data["fao_snapshot_suffix"] = fao_snapshot_suffix
        fao_root_term_data["file_hashes"] = file_hashes

        await self.service_provider.glossary_service.put_term_by_uid(
            Term(
                uid=fao_root_term.uid,
                xid=fao_root_term.xid,
                name=fao_root_term.name,
                sub_class_of=fao_root_term.sub_class_of,
                data=fao_root_term_data,
                access_group_uid=fao_root_term.access_group_uid,
            )
        )
        self.db_fao_snapshot_suffix = fao_snapshot_suffix

    async def process_domestic_production_data_from_fao(self) -> None:
        # unzip the archive
        with ZipFile(os.path.join(self.gfm_dir, self.domestic_zip_source_file_name), "r") as zip_file:
            with zip_file.open(self.origin_gfm_settings.DOMESTIC_SOURCE_FILE_NAME) as f:
                domestic_data = pd.read_csv(f, encoding="latin-1")

        # In the newer files, unit is "t" not "tonnes."
        # domestic_data.drop(domestic_data[(domestic_data["Unit"] != "tonnes")].index, inplace=True)
        domestic_data.drop(domestic_data[~domestic_data["Unit"].isin(["tonnes", "t"])].index, inplace=True)

        # drop rows where the primary and the secondary year are zero
        domestic_data.drop(
            domestic_data[
                (
                    ((domestic_data[self.primary_year].isnull()) | (domestic_data[self.primary_year] == 0))
                    & ((domestic_data[self.secondary_year].isnull()) | (domestic_data[self.secondary_year] == 0))
                )
            ].index,
            inplace=True,
        )

        # additional data type conversion
        domestic_data["Area Code (M49)"].replace(regex=r"'", value="", inplace=True)
        domestic_data["Area Code (M49)"] = domestic_data["Area Code (M49)"].astype(int)

        # save as .hdf5
        domestic_data.to_hdf(
            os.path.join(self.gfm_dir, self.domestic_cache_file_name),
            mode="w",
            key="df",
        )

    def spawn_worker(self, node: Node) -> OriginGapFillingWorker:
        return OriginGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = OriginGapFillingFactory
