"""Link food categories gap filling module."""

import uuid

from gap_filling_modules.abstract_gfm import GFM_STATE_PROP_NAME, AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingWorker
from gap_filling_modules.nutrient_subdivision_gfm import NutrientSubdivisionGapFillingWorker
from structlog import get_logger

from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props import AnimalProductsProp, NamesProp, QuantityPackageProp, ReferencelessQuantityProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.glossary_link_service import GlossaryLinkService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class LinkFoodCategoriesGapFillingWorker(AbstractGapFillingWorker):
    """Link food category tags to food products according to FoodEx2 terms."""

    def __init__(self, node: Node, gfm_factory: "LinkFoodCategoriesGapFillingFactory"):
        """Link the matched food Terms from the glossary, e.g., ("A1791", "J0116") to food categories."""
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Determine whether to schedule link food category gap filling worker.

        similar to MatchProductNameGapFillingWorker and NutritionSubdivisionGapFillingWorker;
        do not schedule if name is missing as matching product name to terms is impossible.
        """
        if not isinstance(self.node, FoodProductFlowNode):
            logger.debug("[LinkFoodCategories] node is not a food product --> not scheduled.")
            return False
        elif self.node.is_subdivision:
            logger.debug("[LinkFoodCategories] node is on a subdivision --> not scheduled.")
            return False
        else:
            if self.node.product_name:
                if self.node.get_parent_nodes() and self.node.get_parent_nodes()[0].get_parent_nodes():
                    parent_flow_node = self.node.get_parent_nodes()[0].get_parent_nodes()[0]
                    if parent_flow_node.product_name and parent_flow_node.product_name == self.node.product_name:
                        logger.debug("[LinkFoodCategories] node is on a origin split node --> not scheduled.")
                        return False

                logger.debug("[LinkFoodCategories] found product_name in node --> scheduled.")
                return True
            else:
                logger.debug("[LinkFoodCategories] node does not have product_name --> not scheduled.")
                return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Determine whether to run food category gap filling worker.

        We want to link food categories *after* performing nutrient subdivision; else we will not have a chance
        to link subdivided nodes.
        """
        global_gfm_state = self.get_global_gfm_state()

        if global_gfm_state.get(NutrientSubdivisionGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[LinkFoodCategories] waiting for NutrientSubdivision GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule
        else:
            product_name = self.node.product_name
            if product_name and isinstance(product_name, NamesProp):
                if getattr(self.node, GFM_STATE_PROP_NAME) and (
                    gfm_status := getattr(self.node, GFM_STATE_PROP_NAME).worker_states.get(
                        AttachFoodTagsGapFillingWorker.__name__
                    )
                ):
                    if gfm_status == NodeGfmStateEnum.scheduled:
                        logger.debug(
                            "[LinkFoodCategories] waiting for AttachFoodTagsGapFillingFactory GFM to"
                            "run --> reschedule."
                        )
                        return GapFillingWorkerStatusEnum.reschedule

                logger.debug("[LinkFoodCategories] --> ready.")
                return GapFillingWorkerStatusEnum.ready
            else:
                logger.debug(
                    "[LinkFoodCategories] cancel GFM worker because food category linking is impossible"
                    " without the product name."
                )
                return GapFillingWorkerStatusEnum.cancel

    async def run(self, calc_graph: CalcGraph) -> None:
        """Run food category gap filling worker."""
        logger.debug("start running gap-filling-module LinkFoodCategories...")

        product_name_terms = [prop.get_term() for prop in self.node.product_name.terms]

        if any(not term for term in product_name_terms):
            logger.warn(
                "Not all FoodEx2 Terms are well-defined for this node.",
                node_uid=self.node.uid,
            )
            calc_graph.append_data_errors_log_entry(
                f"Could not match product with terms {product_name_terms} in node {self.node} to food categories"
            )
        else:
            food_category_term_uids = self.gfm_factory.linked_food_category_terms.get(
                frozenset([term.uid for term in product_name_terms])
            )
            if food_category_term_uids:
                food_category_terms = [
                    await self.gfm_factory.service_provider.glossary_service.get_term_by_uid(food_category_term_uid)
                    for food_category_term_uid in food_category_term_uids
                ]
            else:
                food_category_terms = []

            logger.debug(
                "create PropMutation to add amount_per_category_in_flow",
                node=self.node,
                food_category_terms=food_category_terms,
            )

            if food_category_terms:
                prop_data: dict[uuid.UUID, ReferencelessQuantityProp] = {}
                for food_category_term in food_category_terms:
                    prop_data[food_category_term.uid] = ReferencelessQuantityProp.unvalidated_construct(
                        value=100, unit_term_uid=self.gfm_factory.gram_term.uid
                    )

                    if food_category_term == self.gfm_factory.animal_product_term:
                        qty_prop = ReferencelessQuantityProp.unvalidated_construct(
                            value=100,
                            unit_term_uid=self.gfm_factory.gram_term.uid,
                        )
                        animal_welfare_certified = False
                        if self.node.glossary_tags:
                            for label in self.node.glossary_tags:
                                if label.get_term().uid == self.gfm_factory.animal_welfare_certified_term.uid:
                                    animal_welfare_certified = True

                        if animal_welfare_certified:
                            animal_product_term_uid = self.gfm_factory.animal_welfare_certified_term.uid
                        else:
                            animal_product_term_uid = self.gfm_factory.not_certified_for_animal_welfare_term.uid

                        calc_graph.apply_mutation(
                            PropMutation(
                                created_by_module=self.__class__.__name__,
                                node_uid=self.node.uid,
                                prop_name="animal_products",
                                prop=AnimalProductsProp.unvalidated_construct(
                                    quantities={animal_product_term_uid: qty_prop},
                                    for_reference=ReferenceAmountEnum.amount_for_100g,
                                ),
                            )
                        )

                calc_graph.apply_mutation(
                    # When the base product is tagged, the entire amount (100g out of 100g) corresponds to the tag.
                    # For example, strawberry base product tagged as a fruit is entirely fruit (100g).
                    PropMutation(
                        created_by_module=self.__class__.__name__,
                        node_uid=self.node.uid,
                        prop_name="amount_per_category_in_flow",
                        prop=QuantityPackageProp.unvalidated_construct(
                            quantities=prop_data, for_reference=ReferenceAmountEnum.amount_for_100g
                        ),
                    )
                )

                if not self.node.animal_products:
                    not_certified_for_animal_welfare_term_uid = (
                        self.gfm_factory.not_certified_for_animal_welfare_term.uid
                    )
                    zero_gram_qty = ReferencelessQuantityProp.unvalidated_construct(
                        value=0,
                        unit_term_uid=self.gfm_factory.gram_term.uid,
                    )
                    calc_graph.apply_mutation(
                        PropMutation(
                            created_by_module=self.__class__.__name__,
                            node_uid=self.node.uid,
                            prop_name="animal_products",
                            prop=AnimalProductsProp.unvalidated_construct(
                                quantities={not_certified_for_animal_welfare_term_uid: zero_gram_qty},
                                for_reference=ReferenceAmountEnum.amount_for_100g,
                            ),
                        )
                    )
            else:
                term_set = frozenset({t.xid for t in product_name_terms})
                if term_set not in self.gfm_factory.warnings_generated:
                    self.gfm_factory.warnings_generated.add(term_set)
                    product_names = [{"name": t.name, "xid": t.xid} for t in product_name_terms]
                    if any(t.uid in self.gfm_factory.non_food_term_uids for t in product_name_terms):
                        # exclude non-food-items from warnings
                        logger.debug(
                            (
                                "[LinkFoodCategoriesGFM]: Food category tagging is missing for the product with "
                                "these terms"
                            ),
                            product_names=product_names,
                        )
                    else:
                        logger.warn(
                            "Food category tagging is missing for the product with these terms",
                            product_names=product_names,
                        )
                        calc_graph.append_data_errors_log_entry(
                            f"Could not match product with terms {product_names} in node {self.node} to food categories"
                        )


class LinkFoodCategoriesGapFillingFactory(AbstractGapFillingFactory):
    """Link food categories gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.glossary_link_service: GlossaryLinkService = service_provider.glossary_link_service
        self.warnings_generated: set[frozenset] = set()  # set of terms for which we already generated warnings
        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        root_food_categories_term = self.service_provider.glossary_service.root_subterms.get("Root_Food_Categories")
        self.root_transport_term = self.service_provider.glossary_service.root_subterms.get("EOS_Transportation")
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", root_unit_term.access_group_uid)
        ]
        self.animal_product_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_Animal-products", root_food_categories_term.access_group_uid)
        ]
        self.animal_product_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_Animal-products", root_food_categories_term.access_group_uid)
        ]
        self.animal_welfare_certified_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_animal_welfare_certified", self.service_provider.glossary_service.root_term.access_group_uid)
        ]
        self.not_certified_for_animal_welfare_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            ("EOS_not_certified_for_animal_welfare", self.service_provider.glossary_service.root_term.access_group_uid)
        ]
        self.linked_food_category_terms: dict[frozenset[uuid.UUID], list[uuid.UUID]] = {}
        self.non_food_term_uids: frozenset[uuid.UUID] = frozenset()

    async def init_cache(self) -> None:
        """Initialize cache for link food categories gap filling factory (currently empty)."""
        food_category_glossary_links = await self.service_provider.glossary_link_service.get_glossary_links_by_gfm(
            gap_filling_module="FoodCategories"
        )
        for food_category_glossary_link in food_category_glossary_links:
            if frozenset(food_category_glossary_link.term_uids) in self.linked_food_category_terms:
                self.linked_food_category_terms[frozenset(food_category_glossary_link.term_uids)].append(
                    food_category_glossary_link.linked_term_uid
                )
            else:
                self.linked_food_category_terms[frozenset(food_category_glossary_link.term_uids)] = [
                    food_category_glossary_link.linked_term_uid
                ]
        root_access_group_uid = self.service_provider.glossary_service.root_term.access_group_uid
        foodex2_access_group_uid = await self.service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )
        self.non_food_term_uids = frozenset(
            [
                self.service_provider.glossary_service.terms_by_xid_ag_uid[("EAT-0002", foodex2_access_group_uid)].uid,
                self.service_provider.glossary_service.terms_by_xid_ag_uid[
                    ("EOS_data_error", root_access_group_uid)
                ].uid,
            ]
        )

    def spawn_worker(self, node: Node) -> LinkFoodCategoriesGapFillingWorker:
        """Spawn link food categories gap filling worker."""
        return LinkFoodCategoriesGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = LinkFoodCategoriesGapFillingFactory
