"""Processing gap filling module."""

import os
import re
import uuid
from typing import Optional, Type, Union

import pandas as pd
from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util import get_dataframe_from_gdrive
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingWorker
from gap_filling_modules.conservation_gfm import ConservationGapFillingWorker
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingWorker
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingWorker
from gap_filling_modules.origin_gfm import OriginGapFillingWorker
from gap_filling_modules.processing_models import (
    AbstractProcessingModel,
    DryingProcessingModel,
    ElectricityNodeByCountry,
    FreezingProcessingModel,
    ProcessingTagsAndElectricityAmount,
    ProcessingTagsAndXid,
    ProcessWithFixedElectricityAmount,
    ProcessWithNonUnitRawMaterial,
    ProcessWithUnitRawMaterial,
)
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConverter
from pydantic_settings import SettingsConfigDict
from structlog import get_logger

from core.domain.matching_item import MatchingItem
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode, ModeledActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow.practice_flow_node import PracticeFlowNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props import QuantityProp
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.names_prop import NamesProp
from core.domain.props.utils import has_one_of_predefined_tags
from core.envprops import EnvProps
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_edge_mutation import AddEdgeMutation
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.graph_manager.mutations.remove_edge_mutation import RemoveEdgeMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class ProcessingSettings(EnvProps):
    """Settings for greenhouse gap filling module."""

    GDRIVE_SERVICE_ACCOUNT_FILE: str = "./secrets/service_account.json"
    PROCESSING_GSHEET_ID: str = "17k91sRZlutS8KzFyD4E6x1WjLasvATXelaQryxcVjVE"
    PROCESSING_GSHEET_NAME: str = "Processing GFM"
    GFM_INVOLVED_COL: str = "GFM involved"
    GLOSSARY_TERM_COL: str = "Glossary term"
    BRIGHTWAY_ID_COL: str = "Brightway ID or Electricity Amount"
    COMMENTS_COL: str = "Comments"
    RAW_NAME_COL: str = "Raw Name"

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


class ProcessingGapFillingWorker(AbstractGapFillingWorker):
    """Processing gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "ProcessingGapFillingFactory"):
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Whether to schedule Processing gap filling worker."""
        # Schedule on Brightway nodes with a parent that has a product_name and on Activity nodes with a parent that is
        # a combined product.
        if (
            isinstance(self.node, ModeledActivityNode)
            and self.node.get_parent_nodes()
            and any(
                isinstance(parent_node, FoodProductFlowNode) and parent_node.product_name
                for parent_node in self.node.get_parent_nodes()
            )
        ) or (
            isinstance(self.node, FoodProcessingActivityNode)  # in this case we only want to run on combined products
            # but we can not check if it is a combined product here, because add_client_nodes-gfm might not yet have
            # added the sub-flows. Instead we will check it in can_run_now.
        ):
            return True
        else:
            return False

    @staticmethod
    def is_match_product_name_scheduled(parent_node: Node) -> bool:
        if (
            parent_node.gfm_state
            and parent_node.gfm_state.worker_states.get(MatchProductNameGapFillingWorker.__name__)
            == NodeGfmStateEnum.scheduled.value
        ):
            return True
        else:
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """Whether Processing gap filling worker can be executed."""
        if (
            self.get_global_gfm_state().get(AddClientNodesGapFillingWorker.__name__, 0)
            == NodeGfmStateEnum.scheduled.value
        ):
            logger.debug("[Processing] wait for AddClientNodesGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule
        if isinstance(self.node, FoodProcessingActivityNode):
            if not any(
                isinstance(parent_node, FoodProductFlowNode) and parent_node.is_combining_ingredients()
                for parent_node in self.node.get_parent_nodes()
            ):
                logger.debug(
                    "[Processing] should not run on a FoodProcessingActivityNode that is not combining products"
                )
                return GapFillingWorkerStatusEnum.cancel

        # Not None product_names need to be matched
        if any(self.is_match_product_name_scheduled(parent_node) for parent_node in self.node.get_parent_nodes()):
            logger.debug("[Processing] Not all parent nodes are matched --> reschedule.", node_uid=self.node.uid)
            return GapFillingWorkerStatusEnum.reschedule

        # The OriginGFM has to be finished
        if self.get_global_gfm_state().get(OriginGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("[Processing] wait for OriginGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if (
            self.get_global_gfm_state().get(ConservationGapFillingWorker.__name__, 0)
            == NodeGfmStateEnum.scheduled.value
        ):
            # Wait for conservation gap filling such that cooling tag is added.
            logger.debug("[Processing] wait for ConservationGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        if isinstance(self.node, ModeledActivityNode):
            # The following block exists to avoid adding Processing activity to two Brightway nodes, before
            # and after transport. In transport mode decision GFM, the Brightway Node sometimes gets Duplicated.
            if any(isinstance(sub_node, FoodProductFlowNode) for sub_node in self.node.get_sub_nodes()):
                logger.debug(
                    "[Processing] should be scheduled only on the leaf Brightway node (at the origin) --> cancel."
                )
                return GapFillingWorkerStatusEnum.cancel

        if (
            self.get_global_gfm_state().get(AttachFoodTagsGapFillingWorker.__name__, 0)
            == NodeGfmStateEnum.scheduled.value
        ):
            logger.debug("[Processing] wait for CheckProductionGapFillingWorker to finish --> not can_run_now.")
            return GapFillingWorkerStatusEnum.reschedule

        return GapFillingWorkerStatusEnum.ready

    async def add_processing_nodes(
        self,
        raw_material_flow_amount: QuantityProp,
        bw_nodes_and_flow_amounts: dict[uuid.UUID, QuantityProp],
        parent_flow: FoodProductFlowNode,
        calc_graph: CalcGraph,
        trigger_term_xids: set[str],
        process_name: str,
        processing_activity: Optional[ActivityNode] = None,
    ) -> None:
        """Add all processing nodes of a single processing model to the graph."""
        assert isinstance(self.node, ActivityNode)
        if not processing_activity:
            # 1) remove edge between the Brightway node and parent flow node.
            remove_edge_mutation = RemoveEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=parent_flow.uid,
                to_node_uid=self.node.uid,
            )
            calc_graph.apply_mutation(remove_edge_mutation)

            # 2) Processing activity node
            legacy_api_information = None
            if self.node.legacy_API_information:
                # we need to preserve the legacy API information so that ingredient type is preserved:
                legacy_api_information = self.node.legacy_API_information.duplicate()
            processing_activity = FoodProcessingActivityNode(
                uid=uuid.uuid4(),
                production_amount=self.node.production_amount.duplicate(),
                gfm_state=GfmStateProp(worker_states={self.__class__.__name__: NodeGfmStateEnum.finished.value}),
                legacy_API_information=legacy_api_information,
            )

            # 3) re-establish edges between parent flow node and Processing activity node.
            add_child_node_mutation = AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=parent_flow.uid,
                new_node=processing_activity,
                copy=False,
            )
            calc_graph.apply_mutation(add_child_node_mutation)

            # 4) add flow between Brightway node and Processing activity node
            food_flow_uid = uuid.uuid4()
            calc_graph.apply_mutation(
                DuplicateNodeMutation(
                    created_by_module=self.__class__.__name__,
                    new_node_uid=food_flow_uid,
                    source_node_uid=parent_flow.uid,
                    parent_node_uid=processing_activity.uid,
                    gfms_to_not_schedule=(
                        "OriginGapFillingWorker",
                        "NutrientSubdivisionGapFillingWorker",
                        "TransportModeDistanceGapFillingWorker",
                        "TransportDecisionGapFillingWorker",
                        "MatrixCalculationGapFillingWorker",
                    ),
                    duplicate_child_nodes=False,
                )
            )

            # Overwrite amount_in_original_source_unit and amount:
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=food_flow_uid,
                    prop_name="amount_in_original_source_unit",
                    prop=raw_material_flow_amount,
                )
            )
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=food_flow_uid,
                    prop_name="amount",
                    prop=raw_material_flow_amount.duplicate(),
                )
            )

            add_edge_mutation = AddEdgeMutation(
                created_by_module=self.__class__.__name__,
                from_node_uid=food_flow_uid,
                to_node_uid=self.node.uid,
            )
            calc_graph.apply_mutation(add_edge_mutation)

        trigger_term_uids: list[uuid.UUID] = []
        for term_xid in trigger_term_xids:
            terms_by_xid_ag_uid = self.gfm_factory.service_provider.glossary_service.terms_by_xid_ag_uid
            if (term_xid, self.gfm_factory.foodex2_access_group_uid) in terms_by_xid_ag_uid:
                trigger_term_uids.append(terms_by_xid_ag_uid[(term_xid, self.gfm_factory.foodex2_access_group_uid)].uid)
            elif (term_xid, self.gfm_factory.default_eaternity_access_group_uid) in terms_by_xid_ag_uid:
                trigger_term_uids.append(
                    terms_by_xid_ag_uid[(term_xid, self.gfm_factory.default_eaternity_access_group_uid)].uid
                )
            elif (term_xid, self.gfm_factory.transportation_access_group_uid) in terms_by_xid_ag_uid:
                trigger_term_uids.append(
                    terms_by_xid_ag_uid[(term_xid, self.gfm_factory.transportation_access_group_uid)].uid
                )

        # 5) Attach the correct subgraphs to the Processing activity node.
        for bw_uid, amount in bw_nodes_and_flow_amounts.items():
            processing_flow = PracticeFlowNode(
                uid=uuid.uuid4(),
                amount=amount,
                product_name=NamesProp.unvalidated_construct(
                    terms=[GlossaryTermProp.unvalidated_construct(term_uid=term_uid) for term_uid in trigger_term_uids],
                    source_data_raw=[{"value": f"{process_name} ProcessingGFM", "language": "en"}],
                ),
            )
            add_child_node_mutation = AddNodeMutation(
                created_by_module=self.__class__.__name__,
                parent_node_uid=processing_activity.uid,
                new_node=processing_flow,
                copy=False,
            )
            calc_graph.apply_mutation(add_child_node_mutation)

            is_processing_method_node_in_graph = bool(calc_graph.get_node_by_uid(bw_uid))
            if not is_processing_method_node_in_graph:
                processing_method_node = (
                    await LinkTermToActivityNodeGapFillingWorker.modeled_or_emission_node_from_cache_by_uid(bw_uid)
                )
                add_child_node_mutation = AddNodeMutation(
                    created_by_module=self.__class__.__name__,
                    parent_node_uid=processing_flow.uid,
                    new_node=processing_method_node,
                    copy=False,
                )
                calc_graph.apply_mutation(add_child_node_mutation)
            else:
                add_edge_mutation = AddEdgeMutation(
                    created_by_module=self.__class__.__name__,
                    from_node_uid=processing_flow.uid,
                    to_node_uid=bw_uid,
                )
                calc_graph.apply_mutation(add_edge_mutation)

    @staticmethod
    def get_non_subdivision_node(node: FlowNode) -> FlowNode:
        non_subdivision_node = node
        if node.is_subdivision:
            while (
                non_subdivision_node.is_subdivision
                and non_subdivision_node.get_parent_nodes()
                and non_subdivision_node.get_parent_nodes()[0].get_parent_nodes()
            ):
                non_subdivision_node = non_subdivision_node.get_parent_nodes()[0].get_parent_nodes()[0]
        return non_subdivision_node

    @staticmethod
    def get_upscaling_activity_node(node: FlowNode, upscaling_amount: float) -> FlowNode:
        current_node = node
        while current_node.get_parent_nodes() and current_node.get_parent_nodes()[0].get_parent_nodes():
            production_amount = current_node.get_parent_nodes()[0].production_amount
            amount = current_node.amount
            amount_in_production_amount_unit = UnitWeightConverter.convert_between_same_unit_types(
                amount.value, amount.get_unit_term(), production_amount.get_unit_term(), "mass-in-g"
            )
            if abs(amount_in_production_amount_unit / production_amount.value - upscaling_amount) < 1e-3:
                return current_node.get_parent_nodes()[0]

            current_node = current_node.get_parent_nodes()[0].get_parent_nodes()[0]

    async def run(self, calc_graph: CalcGraph) -> None:
        """Execute Processing gap filling worker."""
        assert isinstance(self.node, ActivityNode)

        # Local cache to avoid loading nutrients multiple times.
        parent_flows_filtered_tag_term_xids = []
        parent_flows = self.node.get_parent_nodes()

        for parent_flow in parent_flows:
            if not isinstance(parent_flow, FoodProductFlowNode):
                logger.debug(f"[Processing] Parent node {parent_flow} is not a FoodProductFlowNode --> continue.")
                parent_flows_filtered_tag_term_xids.append({})
                continue

            non_subdivision_node = self.get_non_subdivision_node(parent_flow)
            tag_terms_xids = set(non_subdivision_node.tag_term_xids)
            # also check if one of the parent is dried, in that case we need to add the drying process.
            if self.gfm_factory.dried_term_xid not in parent_flows_filtered_tag_term_xids:
                (
                    dried_term,
                    _,
                ) = non_subdivision_node.get_prop_by_inheritance(
                    "tag_term_xids",
                    ignore_if=(
                        lambda prop_name, tag_term_xids_to_search=frozenset(
                            {self.gfm_factory.dried_term_xid}
                        ): not has_one_of_predefined_tags(prop_name, tag_term_xids_to_search)
                    ),
                )
                if dried_term:
                    tag_terms_xids.add(self.gfm_factory.dried_term_xid)
            filtered_tag_term_xids: set = tag_terms_xids.intersection(self.gfm_factory.all_trigger_tags)
            parent_flows_filtered_tag_term_xids.append(filtered_tag_term_xids)

        for processing_method in self.gfm_factory.all_models:
            parent_flows = self.node.get_parent_nodes()
            for parent_flow, filtered_tag_term_xids in zip(parent_flows, parent_flows_filtered_tag_term_xids):
                if processing_method.is_triggered_by_tag_xids(filtered_tag_term_xids):
                    if processing_method.process_name == "Drying":
                        if not parent_flow.nutrient_upscale_ratio:
                            # Skip this node because drying process can only be added for cases where the dried amount
                            # has been estimated using the nutrient subdivision GFM. The nutrient upscale ratio is used
                            # to compute the amount of water to evaporate.
                            continue
                        # For drying process, the nutrient subdivision gfm has already created upscaling node.
                        # We simply have to attach the average drying inventory node as a sibling flow to the
                        # upscaling node. The relevant production amount is the parent node's production amount
                        # in the compuation of water to evaporate.
                        processing_activity = self.get_upscaling_activity_node(
                            parent_flow, parent_flow.nutrient_upscale_ratio.value
                        )
                        post_processing_production_amount = processing_activity.production_amount
                    else:
                        post_processing_production_amount = self.node.production_amount
                        processing_activity = None  # processing_activity is constructed inside add_processing_node.

                    (
                        raw_material_flow_amount,
                        bw_nodes_and_flow_amounts,
                        trigger_term_xids,
                    ) = await processing_method.compute_flow_amounts(
                        filtered_tag_term_xids, parent_flow, post_processing_production_amount
                    )
                    if raw_material_flow_amount and bw_nodes_and_flow_amounts:
                        # Add transport term to filtered_tag_term_xids such that transport model is executed.
                        # Z0001 signifies that the new inventory is created from an existing inventory (and transport
                        # is needed to the processing facility).
                        if "Z0001" in filtered_tag_term_xids:
                            filtered_tag_term_xids.add("EOS_Transportation")
                        await self.add_processing_nodes(
                            raw_material_flow_amount,
                            bw_nodes_and_flow_amounts,
                            parent_flow,
                            calc_graph,
                            trigger_term_xids,
                            processing_method.process_name,
                            processing_activity=processing_activity,
                        )


class ProcessingGapFillingFactory(AbstractGapFillingFactory):
    """Processing gap filling factory."""

    settings = ProcessingSettings()

    model_by_name: dict[str, Type[AbstractProcessingModel]] = {
        "Dried Fruit Grinding": ProcessWithUnitRawMaterial,
        "Grinding": ProcessWithFixedElectricityAmount,
        "Carbonating": ProcessWithUnitRawMaterial,
        "Cutting": ProcessWithFixedElectricityAmount,
        "Shredding": ProcessWithFixedElectricityAmount,
        "Fermenting": ProcessWithFixedElectricityAmount,
        "Smoking": ProcessWithUnitRawMaterial,
        "Puffing": ProcessWithFixedElectricityAmount,
        "Freeze-drying": ProcessWithFixedElectricityAmount,
        "Cooling": ProcessWithFixedElectricityAmount,
        "Baking": ProcessWithNonUnitRawMaterial,
        "Jam production": ProcessWithNonUnitRawMaterial,
        "Juice production": ProcessWithNonUnitRawMaterial,
        "Yoghurt production": ProcessWithNonUnitRawMaterial,
        "Roasting": ProcessWithUnitRawMaterial,
        "Chopping": ProcessWithUnitRawMaterial,
        "Heating": ProcessWithUnitRawMaterial,
        "Drying": DryingProcessingModel,  # Should be after Grinding.
        "Freezing": FreezingProcessingModel,
        "Mixing": ProcessWithFixedElectricityAmount,
        "Transportation": ProcessWithUnitRawMaterial,  # Should be last.
    }

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.dried_term_xid = "J0116"  # needed to filter out dried-term-xids from NutrientSubdivisionNodes
        self.trigger_tags_by_process: dict[
            str, list[Union[ProcessingTagsAndXid, ProcessingTagsAndElectricityAmount]]
        ] = {}
        self.all_trigger_tags: set[str] = set()
        self.all_models: list[AbstractProcessingModel] = []
        self.foodex2_access_group_uid: Optional[uuid.UUID] = None
        self.default_eaternity_access_group_uid: Optional[uuid.UUID] = None
        self.transportation_access_group_uid: Optional[uuid.UUID] = None

    @staticmethod
    def get_dataframe_from_local_folder(file_path: str) -> Optional[pd.DataFrame]:
        """Return dataframe from local folder. If not available, return None."""
        file_path = os.path.join(file_path)
        if not os.path.exists(file_path):
            return None
        dataframe = pd.read_csv(file_path)
        return dataframe

    @staticmethod
    def get_dataframe_from_gdrive(settings: ProcessingSettings) -> Optional[pd.DataFrame]:
        """Return dataframe from google drive. If not available, return None."""
        return get_dataframe_from_gdrive(
            settings.GDRIVE_SERVICE_ACCOUNT_FILE, settings.PROCESSING_GSHEET_ID, settings.PROCESSING_GSHEET_NAME
        )

    @staticmethod
    async def get_all_process_trigger_tags_and_matched_strings_from_dataframe(
        settings: ProcessingSettings, service_provider: ServiceProvider, dataframe: Optional[pd.DataFrame] = None
    ) -> tuple[dict[str, list[Union[ProcessingTagsAndXid, ProcessingTagsAndElectricityAmount]]], dict[str, list[str]]]:
        """Method to get all process trigger tags from local folder or from google drive."""
        trigger_tags_by_process: dict[str, list[Union[ProcessingTagsAndXid, ProcessingTagsAndElectricityAmount]]] = {}
        matched_raw_strings: dict[str, list[str]] = {}

        if dataframe is None:
            trigger_tags_by_process = {
                "drying": [
                    ProcessingTagsAndXid(
                        tags={"J0116"}, xid="EDB_c3fbdd847ef44171875321e552c3ba32", location="unspecified"
                    )
                ]
            }

        else:

            def extract_strings(text: str) -> Optional[str]:
                matches = re.findall(r"\[([^\[\]]+)]", text)
                if len(matches) == 1:
                    return matches[0]
                else:
                    if not matches:
                        return None
                    else:
                        logger.warning(f"There should be only one term in {text}")
                        return None

            foodex2_access_group_uid = await service_provider.namespace_service.find_default_access_group_by_ns_xid(
                "foodex2"
            )

            for _, row in dataframe.iterrows():
                terms = set(
                    [
                        extract_strings(x)
                        for x in row.filter(like=settings.GLOSSARY_TERM_COL).values
                        if x and isinstance(x, str) and extract_strings(x)
                    ]
                )
                comments: str = row[settings.COMMENTS_COL] if isinstance(row[settings.COMMENTS_COL], str) else ""
                if comments.startswith("GLO"):
                    location = "Global"
                elif comments.startswith("RER"):
                    location = "R0355"
                else:
                    location = "unspecified"

                if (raw_name := row[settings.RAW_NAME_COL]) and not pd.isna(raw_name):
                    keys = str(raw_name).split(",")
                    for key in keys:
                        matched_raw_strings[key.strip()] = [
                            str(
                                service_provider.glossary_service.terms_by_xid_ag_uid.get(
                                    (xid, foodex2_access_group_uid),
                                    service_provider.glossary_service.root_subterms.get(xid),
                                ).uid
                            )
                            for xid in terms
                        ]

                bw_id_or_electricity_amount = str(row[settings.BRIGHTWAY_ID_COL])

                def is_float(string: str) -> bool:
                    """Helper function to check if string is a float."""
                    try:
                        float(string)
                        return True
                    except ValueError:
                        return False

                if is_float(bw_id_or_electricity_amount):
                    process_to_add = ProcessingTagsAndElectricityAmount(
                        tags=terms,
                        electricity_amount=float(bw_id_or_electricity_amount),
                        location=location,
                    )
                else:
                    process_to_add = ProcessingTagsAndXid(
                        tags=terms,
                        xid="_".join(re.findall(r"\w+", row[settings.BRIGHTWAY_ID_COL])),
                        location=location,
                    )

                trigger_tags_by_process.setdefault(row[settings.GFM_INVOLVED_COL], []).append(process_to_add)
        return trigger_tags_by_process, matched_raw_strings

    @staticmethod
    async def set_process_trigger_tags_cache_table_and_add_matchings(
        dataframe: pd.DataFrame,
        processing_settings: ProcessingSettings,
        service_provider: ServiceProvider,
    ) -> None:
        """Helper method to set the process trigger tags cache table."""
        (
            trigger_tags_by_process,
            matched_raw_strings,
        ) = await ProcessingGapFillingFactory.get_all_process_trigger_tags_and_matched_strings_from_dataframe(
            processing_settings, service_provider, dataframe
        )
        gfm_name = "ProcessingTriggerTags"
        gfm_cache = await service_provider.postgres_db.gfm_cache_mgr.get_prefill_cache_entries(
            gfm_name=gfm_name,
            limit=1000,  # this number is arbitrary, but should be enough for now
        )
        existing_gfm_cache = {cache_entry["cache_key"]: cache_entry["cache_data"] for cache_entry in gfm_cache}
        for process_name, trigger_tags in trigger_tags_by_process.items():
            new_cache_value = [trigger_tag.model_dump() for trigger_tag in trigger_tags]
            if process_name not in existing_gfm_cache or new_cache_value != existing_gfm_cache[process_name]:
                await service_provider.postgres_db.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
                    gfm_name=gfm_name,
                    cache_key=process_name,
                    cache_data=new_cache_value,
                    load_on_boot=True,
                )

        for matched_raw_string, term_uids in matched_raw_strings.items():
            await service_provider.matching_service.put_matching_item(
                MatchingItem(
                    gap_filling_module="ProcessingGFM",
                    lang="en",
                    matching_string=matched_raw_string,
                    term_uids=term_uids,
                )
            )

    async def init_cache(self) -> None:
        """Initialize cache required for Processing gap filling factory."""
        trigger_tags_by_process_cache: list[
            dict
        ] = await self.service_provider.postgres_db.gfm_cache_mgr.get_prefill_cache_entries(
            gfm_name="ProcessingTriggerTags", limit=1000
        )
        for cache_item in trigger_tags_by_process_cache:
            process_name: str = cache_item["cache_key"]

            trigger_dicts: list[dict] = cache_item["cache_data"]
            triggers_to_add = []
            for trigger_dict in trigger_dicts:
                if "electricity_amount" in trigger_dict:
                    triggers_to_add.append(ProcessingTagsAndElectricityAmount.from_dict(trigger_dict))
                else:
                    triggers_to_add.append(ProcessingTagsAndXid.from_dict(trigger_dict))
            self.trigger_tags_by_process[process_name] = triggers_to_add

        self.all_trigger_tags: set[str] = set(
            tag
            for processing_tags in self.trigger_tags_by_process.values()
            for processing_tag in processing_tags
            for tag in processing_tag.tags
        )
        kwargs_dict = {}
        voltage_key_mapping = {"Medium": "medium_voltage", "Low": "low_voltage"}
        for voltage in ("Medium", "Low"):
            electricity_market_voltage_cache = (
                await self.service_provider.postgres_db.gfm_cache_mgr.get_prefill_cache_entries(
                    gfm_name=f"ElectricityMarket{voltage}Voltage",
                    limit=1000,  # Currently we have 172 electricity markets
                )
            )
            cache_entries: dict[str, uuid.UUID] = {}
            for cache_entry in electricity_market_voltage_cache:
                country_code = cache_entry["cache_key"]
                cache_entries[country_code] = uuid.UUID(cache_entry["cache_data"])
            kwargs_dict[voltage_key_mapping[voltage]] = cache_entries

        raw_materials_xid = "EDB_53a561ae842a44da824a49577075522d"
        raw_materials_uid = self.service_provider.product_service.xid_to_uid_mappings.get(raw_materials_xid)
        electricity_nodes_by_country = ElectricityNodeByCountry(**kwargs_dict)

        groups = await self.service_provider.postgres_db.get_term_mgr().find_all_term_access_groups()
        for group in groups:
            if "FoodEx2 Glossary Terms" in group.data.get("name"):
                foodex2_access_group_uid = group.uid
                if isinstance(foodex2_access_group_uid, str):
                    foodex2_access_group_uid = uuid.UUID(foodex2_access_group_uid)
                self.foodex2_access_group_uid = foodex2_access_group_uid
            if "Transportation Glossary Terms" in group.data.get("name"):
                transportation_access_group_uid = group.uid
                if isinstance(transportation_access_group_uid, str):
                    transportation_access_group_uid = uuid.UUID(transportation_access_group_uid)
                self.transportation_access_group_uid = transportation_access_group_uid

            if self.transportation_access_group_uid and self.foodex2_access_group_uid:
                break

        assert self.foodex2_access_group_uid
        assert self.transportation_access_group_uid

        self.default_eaternity_access_group_uid = uuid.UUID(
            (
                await self.service_provider.access_group_service.get_all_access_group_ids_by_namespace(
                    self.settings.EATERNITY_NAMESPACE_UUID
                )
            )[0].uid
        )

        for key, val in self.model_by_name.items():
            if process_specific_trigger_tags := self.trigger_tags_by_process.get(key):
                self.all_models.append(
                    val(
                        process_name=key,
                        process_specific_trigger_tags=process_specific_trigger_tags,
                        electricity_node_by_country=electricity_nodes_by_country,
                        raw_materials_uid=raw_materials_uid,
                        foodex2_access_group_uid=self.foodex2_access_group_uid,
                        service_provider=self.service_provider,
                    )
                )

    def spawn_worker(self, node: Node) -> ProcessingGapFillingWorker:
        """Spawn processing gap filling worker."""
        return ProcessingGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = ProcessingGapFillingFactory
