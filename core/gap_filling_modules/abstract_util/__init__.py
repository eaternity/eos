from typing import Optional

import google
import pandas as pd
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from structlog import get_logger

logger = get_logger()


def get_dataframe_from_gdrive(
    gdrive_service_account_file: str, gsheet_id: str, gsheet_name: str, column_row: int = 0
) -> Optional[pd.DataFrame]:
    """Return dataframe from google drive. If not available, return None."""
    credentials, _ = google.auth.load_credentials_from_file(
        filename=gdrive_service_account_file,
        scopes=[
            "https://www.googleapis.com/auth/devstorage.read_write",
            "https://www.googleapis.com/auth/spreadsheets",
            "https://www.googleapis.com/auth/drive",
        ],
    )

    try:
        service = build("sheets", "v4", credentials=credentials)
    except HttpError as e:
        logger.warning(f"Error occurred while connecting to google sheets: {e}")
        return

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=gsheet_id, range=gsheet_name).execute()
    values = result.get("values", [])
    if not values:
        logger.warning("No data found.")
        return None
    else:
        dataframe = pd.DataFrame(values[column_row:], columns=values[column_row])[1:]
    service.close()
    return dataframe
