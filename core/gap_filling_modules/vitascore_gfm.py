"""Compute Vitascore."""

import re
import uuid
from enum import Enum
from typing import Optional, Tuple

from gap_filling_modules.abstract_gfm import AbstractGapFillingFactory, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from gap_filling_modules.aggregation_gfm import AggregationGapFillingWorker
from gap_filling_modules.daily_food_unit_gfm import DailyFoodUnitGapFillingWorker
from gap_filling_modules.ingredicalc.models import (
    FAT_CALORIES_PER_GRAM,
    NA_PERCENT_IN_NACL,
    PROTEIN_CALORIES_PER_GRAM,
    split_sodium_chloride,
)
from structlog import get_logger

from core.domain.nodes import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props import QuantityPackageProp, QuantityProp, ReferencelessQuantityProp, VitascoreProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.pg_term_mgr import TmrelNormalizationEnum
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class VitascoreVersionEnum(str, Enum):
    """Enum for Vitascore version: legacy or current."""

    legacy = "legacy"
    current = "current"


class VitascoreGapFillingWorker(AbstractGapFillingWorker):
    """Vitascore gap filling worker."""

    def __init__(self, node: Node, gfm_factory: "VitascoreGapFillingFactory"):
        """Compute VitaScore from aggregated global burden of disease risk factors."""
        super().__init__(node)
        self.gfm_factory = gfm_factory

    def should_be_scheduled(self) -> bool:
        """Only run on FoodProductFlowNodes."""
        if isinstance(self.node, FoodProductFlowNode):
            return True
        else:
            logger.debug("[Vitascore] on a non-FoodProductFlowNode --> not scheduled.")
            return False

    def can_run_now(self) -> GapFillingWorkerStatusEnum:
        """We want to perform vitascore calculation after all aggregation is complete."""

        def dfu_scheduled() -> bool:
            """Determine if Daily food unit GFM is scheduled."""
            if gfm_status := self.node.gfm_state:
                return (
                    gfm_status.worker_states.get(DailyFoodUnitGapFillingWorker.__name__)
                    == NodeGfmStateEnum.scheduled.value
                )
            else:
                return False

        global_gfm_state = self.get_global_gfm_state()
        if global_gfm_state.get(AggregationGapFillingWorker.__name__, 0) == NodeGfmStateEnum.scheduled.value:
            logger.debug("Waiting for AggregationGapFillingWorker GFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if dfu_scheduled():
            logger.debug("Waiting for DailyFoodUnitGFM to run --> reschedule.")
            return GapFillingWorkerStatusEnum.reschedule

        if not self.node.daily_food_unit:
            logger.debug("[Vitascore] cancel GFM worker because daily food unit is not present.")
            return GapFillingWorkerStatusEnum.cancel

        if not self.node.amount_per_category_in_flow or not self.node.amount_per_category_in_flow.quantities:
            logger.debug("[Vitascore] cancel GFM worker because food category is not present.")
            return GapFillingWorkerStatusEnum.cancel

        logger.debug("[Vitascore] All required food categories aggregated --> can_run_now.")
        return GapFillingWorkerStatusEnum.ready

    async def run(self, calc_graph: CalcGraph) -> None:
        """Perform vitascore calculation."""
        assert isinstance(self.node, FoodProductFlowNode)
        amount_per_category_in_flow_prop = self.node.amount_per_category_in_flow
        dfu_prop = self.node.daily_food_unit

        # {vitascore version: {for_reference_amount: {dietary_risk_factor: ReferencelessQuantityProp}}}
        vitascore_data: dict[
            VitascoreVersionEnum, dict[ReferenceAmountEnum, dict[uuid.UUID, ReferencelessQuantityProp]]
        ] = {}

        # Add salt & nutritions before computing vitascore:
        salt_risk_term_xid = "EOS_Diet-high-in-sodium-(salt)"
        energy_risk_term_xid = "EOS_Diet-high-in-energy"
        high_fat_risk_term_xid = "EOS_Diet-high-in-fat"
        low_fat_risk_term_xid = "EOS_Diet-low-in-fat"
        high_protein_risk_term_xid = "EOS_Diet-high-in-protein"
        low_protein_risk_term_xid = "EOS_Diet-low-in-protein"
        (
            salt_qty,
            energy_qty,
            fat_qty,
            protein_qty,
        ) = self.retrieve_salt_and_nutrition_amounts_for_activity_production_amount()
        new_amount_per_category_in_flow_quantities = dict(
            amount_per_category_in_flow_prop.amount_for_activity_production_amount().quantities
        )
        if salt_qty is not None:
            new_amount_per_category_in_flow_quantities[
                self.gfm_factory.all_categories_xid_to_uid[salt_risk_term_xid]
            ] = salt_qty
        if energy_qty is not None:
            new_amount_per_category_in_flow_quantities[
                self.gfm_factory.all_categories_xid_to_uid[energy_risk_term_xid]
            ] = energy_qty
            if fat_qty is not None:
                new_amount_per_category_in_flow_quantities[
                    self.gfm_factory.all_categories_xid_to_uid[high_fat_risk_term_xid]
                ] = fat_qty
                new_amount_per_category_in_flow_quantities[
                    self.gfm_factory.all_categories_xid_to_uid[low_fat_risk_term_xid]
                ] = fat_qty
            if protein_qty is not None:
                new_amount_per_category_in_flow_quantities[
                    self.gfm_factory.all_categories_xid_to_uid[high_protein_risk_term_xid]
                ] = protein_qty
                new_amount_per_category_in_flow_quantities[
                    self.gfm_factory.all_categories_xid_to_uid[low_protein_risk_term_xid]
                ] = protein_qty
        new_amount_per_category_in_flow_prop = QuantityPackageProp(
            quantities=new_amount_per_category_in_flow_quantities,
            for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
        )
        calc_graph.apply_mutation(
            PropMutation(
                created_by_module=self.__class__.__name__,
                node_uid=self.node.uid,
                prop_name="amount_per_category_in_flow",
                prop=new_amount_per_category_in_flow_prop,
            )
        )
        amount_per_category_in_flow_prop = new_amount_per_category_in_flow_prop

        for version in (VitascoreVersionEnum.legacy, VitascoreVersionEnum.current):
            vitascore_for_local = self.compute_vitascore(
                amount_per_category_in_flow_prop,
                dfu_prop,
                version,
                ReferenceAmountEnum.amount_for_activity_production_amount,
            )

            if self.node.get_parent_nodes() and self.node.get_parent_nodes()[0].impact_assessment_supply_for_root:
                flow_vitascore_for_root = self.compute_vitascore(
                    amount_per_category_in_flow_prop, dfu_prop, version, ReferenceAmountEnum.amount_for_root_node
                )
            else:
                flow_vitascore_for_root = None

            flow_vitascore_data: dict[ReferenceAmountEnum, dict[uuid.UUID, ReferencelessQuantityProp]] = {}

            if vitascore_for_local is not None:
                flow_vitascore_data[ReferenceAmountEnum.amount_for_activity_production_amount] = vitascore_for_local
            if flow_vitascore_for_root is not None:
                flow_vitascore_data[ReferenceAmountEnum.amount_for_root_node] = flow_vitascore_for_root

            if (vitascore_for_local is not None) or (flow_vitascore_for_root is not None):
                vitascore_data[version] = flow_vitascore_data

        if vitascore_data:
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=self.node.uid,
                    prop_name="vitascore",
                    prop=VitascoreProp.unvalidated_construct(quantities=vitascore_data[VitascoreVersionEnum.current]),
                )
            )
            calc_graph.apply_mutation(
                PropMutation(
                    created_by_module=self.__class__.__name__,
                    node_uid=self.node.uid,
                    prop_name="vitascore_legacy",
                    prop=VitascoreProp.unvalidated_construct(quantities=vitascore_data[VitascoreVersionEnum.legacy]),
                )
            )

    def compute_vitascore(
        self,
        food_category: QuantityPackageProp,
        daily_food_unit: QuantityProp,
        legacy_or_current: str,
        for_reference_amount: ReferenceAmountEnum,
    ) -> dict[uuid.UUID, ReferencelessQuantityProp] | None:
        """Computes current vitascore: Scaling is by daily food unit.

        :param food_category: "amount_per_category_in_flow" property
        :param daily_food_unit: "daily_food_unit" property
        :param legacy_or_current: which disability adjusted life years (DALYs) values to use (legacy or current)
        :param for_reference_amount: whether the computed amount is for local or root node.
        :return: current_vitascore. {dietary_risk_factor: quantity{"value": value, "unit": unit}}.
        """
        if daily_food_unit:
            dfu = getattr(daily_food_unit, for_reference_amount)()
            if dfu:
                return self.vitascore_formula(food_category, dfu.value, legacy_or_current, for_reference_amount)

    def retrieve_salt_and_nutrition_amounts_for_activity_production_amount(
        self,
    ) -> Tuple[
        Optional[ReferencelessQuantityProp],
        Optional[ReferencelessQuantityProp],
        Optional[ReferencelessQuantityProp],
        Optional[ReferencelessQuantityProp],
    ]:
        nutrition_pkg: QuantityPackageProp | None = None
        salt_qty = None
        energy_kcal_qty = None
        fat_qty = None
        protein_qty = None
        if aggregated_nutrients := self.node.aggregated_nutrients:
            if (
                self.gfm_factory.sodium_term.uid
                in aggregated_nutrients.amount_for_activity_production_amount().quantities
                or self.gfm_factory.sodium_chloride_term.uid
                in aggregated_nutrients.amount_for_activity_production_amount().quantities
            ):
                nutrition_pkg = aggregated_nutrients.amount_for_activity_production_amount()

        elif nutrient_values := self.node.nutrient_values:
            if (
                self.gfm_factory.sodium_term.uid in nutrient_values.amount_for_activity_production_amount().quantities
                or self.gfm_factory.sodium_chloride_term.uid
                in nutrient_values.amount_for_activity_production_amount().quantities
            ):
                nutrition_pkg = nutrient_values.amount_for_activity_production_amount()

        if nutrition_pkg and (self.gfm_factory.sodium_chloride_term.uid in nutrition_pkg.quantities):
            nutrition_pkg_all_na_combined = split_sodium_chloride(nutrition_pkg)
        else:
            nutrition_pkg_all_na_combined = nutrition_pkg

        if (
            nutrition_pkg_all_na_combined
            and self.gfm_factory.sodium_term.uid in nutrition_pkg_all_na_combined.quantities
        ):
            # Converting sodium_quantity to salt_qty.
            salt_qty = (nutrition_pkg_all_na_combined.quantities[self.gfm_factory.sodium_term.uid]).duplicate()
            salt_qty.value *= 100 / NA_PERCENT_IN_NACL

        if nutrition_pkg:
            energy_kcal_qty = nutrition_pkg.quantities.get(self.gfm_factory.energy_term.uid, None)
            fat_in_mass_qty = nutrition_pkg.quantities.get(self.gfm_factory.fat_term.uid, None)
            if isinstance(fat_in_mass_qty, ReferencelessQuantityProp):  # convert to kcal
                fat_in_gram = ReferencelessQuantityProp.convert_between_same_unit_types(
                    fat_in_mass_qty.value, fat_in_mass_qty.get_unit_term(), self.gfm_factory.gram_term, "mass-in-g"
                )
                fat_qty = ReferencelessQuantityProp.unvalidated_construct(
                    value=fat_in_gram * FAT_CALORIES_PER_GRAM, unit_term_uid=self.gfm_factory.kcal_term.uid
                )
            protein_in_mass_qty = nutrition_pkg.quantities.get(self.gfm_factory.protein_term.uid, None)
            if isinstance(protein_in_mass_qty, ReferencelessQuantityProp):  # convert to kcal
                protein_in_gram = ReferencelessQuantityProp.convert_between_same_unit_types(
                    protein_in_mass_qty.value,
                    protein_in_mass_qty.get_unit_term(),
                    self.gfm_factory.gram_term,
                    "mass-in-g",
                )
                protein_qty = ReferencelessQuantityProp.unvalidated_construct(
                    value=protein_in_gram * PROTEIN_CALORIES_PER_GRAM, unit_term_uid=self.gfm_factory.kcal_term.uid
                )

        return salt_qty, energy_kcal_qty, fat_qty, protein_qty

    def vitascore_formula(
        self,
        food_category: QuantityPackageProp,
        dfu: float,
        legacy_or_current: str,
        for_reference_amount: ReferenceAmountEnum,
    ) -> dict[uuid.UUID, ReferencelessQuantityProp]:
        """Formula for computing vitascore.

        :param food_category: "amount_per_category_in_flow" property
        :param dfu: scale to account for daily intake of food.
        :param legacy_or_current: which disability adjusted life years (DALYs) values to use (legacy or current)
        :param for_reference_amount: whether the computed amount is for local or root node.
        :return: vitascore {dietary_risk_factor: quantity{"value": value, "unit": unit}}.
        """
        vitascore: dict[uuid.UUID, ReferencelessQuantityProp] = {
            self.gfm_factory.vitascore_term.uid: ReferencelessQuantityProp.unvalidated_construct(
                value=0.0, unit_term_uid=self.gfm_factory.daly_unit_term.uid
            )
        }
        if food_category:
            amount_per_category_in_flow_for_reference = getattr(food_category, for_reference_amount)()
            if amount_per_category_in_flow_for_reference:
                amount_per_category_in_flow_present = amount_per_category_in_flow_for_reference.quantities
            else:
                amount_per_category_in_flow_present = {}

            energy_kcal_for_reference = amount_per_category_in_flow_for_reference.quantities.get(
                self.gfm_factory.all_categories_xid_to_uid["EOS_Diet-high-in-energy"], None
            )

            for idx, term in enumerate(self.gfm_factory.positive_terms + self.gfm_factory.negative_terms):
                if term.xid in self.gfm_factory.used_categories_xid:
                    t_1: float = float(term.data.get("tmrel").get("lower"))
                    t_2: float = float(term.data.get("tmrel").get("upper"))
                    normalization: TmrelNormalizationEnum = TmrelNormalizationEnum(
                        term.data.get("tmrel").get("tmrel_normalization")
                    )
                    if term.uid in amount_per_category_in_flow_present:
                        x_r = float(amount_per_category_in_flow_present[term.uid].value)
                    else:
                        x_r = 0.0

                    daly_r = float(term.data.get(legacy_or_current).get("amount"))

                    if normalization == TmrelNormalizationEnum.per_dfu:
                        scaling = dfu
                    elif normalization == TmrelNormalizationEnum.per_total_energy:
                        scaling = getattr(energy_kcal_for_reference, "value", 0.0)
                    elif normalization == TmrelNormalizationEnum.per_self:
                        scaling = 1.0
                    else:
                        raise ValueError(f"Normalization {normalization} not supported.")

                    if scaling <= 0.0:
                        logger.debug(f"Scaling factor is zero or negative for {term.name} ({term.xid}).")
                        f_r = 0.0
                    else:
                        if idx < len(self.gfm_factory.positive_terms):
                            f_r = (t_2 / (t_2 - t_1)) - x_r / (scaling * (t_2 - t_1))
                        else:
                            f_r = x_r / (scaling * (t_2 - t_1)) - (t_1 / (t_2 - t_1))

                    if f_r >= 1:
                        f_r = 1

                    if f_r <= 0:
                        f_r = 0

                    vitascore[term.uid] = ReferencelessQuantityProp.unvalidated_construct(
                        value=daly_r * f_r, unit_term_uid=self.gfm_factory.daly_unit_term.uid
                    )
                    vitascore[self.gfm_factory.vitascore_term.uid].value += daly_r * f_r
            return vitascore


class VitascoreGapFillingFactory(AbstractGapFillingFactory):
    """Vitascore gap filling factory."""

    def __init__(self, postgres_db: PostgresDb, service_provider: ServiceProvider):
        super().__init__(postgres_db, service_provider)
        self.positive_terms: [Term] = []
        self.negative_terms: [Term] = []
        self.all_categories_xid_to_uid: {str: uuid.UUID} = {}
        self.used_categories_xid: [str] = []

        root_char_term = self.service_provider.glossary_service.root_subterms.get("Root_Impact_Assessments")
        self.vitascore_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_vita-score", root_char_term.access_group_uid)
        )

        root_unit_term = self.service_provider.glossary_service.root_subterms.get("EOS_units")
        self.daly_unit_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_dalys", root_unit_term.access_group_uid)
        )
        self.gram_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_gram", root_unit_term.access_group_uid)
        )
        self.kcal_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_kilocalorie", root_unit_term.access_group_uid)
        )

        root_nutr_term = self.service_provider.glossary_service.root_subterms.get("EOS_nutrient_names")
        self.sodium_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_sodium", root_nutr_term.access_group_uid)
        )
        self.sodium_chloride_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_sodium_chloride", root_nutr_term.access_group_uid)
        )
        self.energy_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_energy", root_nutr_term.access_group_uid)
        )
        self.fat_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_fat", root_nutr_term.access_group_uid)
        )
        self.protein_term = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_protein", root_nutr_term.access_group_uid)
        )

    async def init_cache(self) -> None:
        """Initialize cache for Vitascore gap filling factory."""
        root_food_categories_term = self.service_provider.glossary_service.root_subterms.get("Root_Food_Categories")

        ag_uid = root_food_categories_term.access_group_uid
        positive_term_uid = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_GBD-Positive", ag_uid)
        ).uid
        negative_term_uid = self.service_provider.glossary_service.terms_by_xid_ag_uid.get(
            ("EOS_GBD-Negative", ag_uid)
        ).uid

        self.positive_terms = self.service_provider.glossary_service.get_sub_terms_by_uid(positive_term_uid, depth=1)
        self.negative_terms = self.service_provider.glossary_service.get_sub_terms_by_uid(negative_term_uid, depth=1)

        used_categories = [
            "Diet low in fruits",
            "Diet low in whole grains",
            "Diet low in vegetables",
            "Diet low in nuts and seeds",
            "Diet low in milk",
            "Diet high in sodium (salt)",
            "Diet high in processed meat",
            "Diet high in red meat",
            "Diet high in energy",
            "Diet high in fat",
            "Diet low in fat",
            "Diet high in protein",
            "Diet low in protein",
        ]

        self.used_categories_xid = [f"EOS_{re.sub(' ','-', category)}" for category in used_categories]
        self.all_categories_xid_to_uid = {term.xid: term.uid for term in self.positive_terms + self.negative_terms}
        for used_category_xid in self.used_categories_xid:
            assert (  # Verify that the used categories are valid.
                used_category_xid in self.all_categories_xid_to_uid
            ), f"{used_category_xid} not in {self.all_categories_xid_to_uid}"

    def spawn_worker(self, node: Node) -> VitascoreGapFillingWorker:
        """Spawn Vitascore gap filling worker."""
        return VitascoreGapFillingWorker(node, self)


# export gap filling module as name "GapFillingModule" so that it is picked up automatically by the loader:
GapFillingFactory = VitascoreGapFillingFactory
