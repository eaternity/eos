import uuid
from typing import Tuple

import pytest

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import append_flow_node, is_close, prepend_flow_node
from core.tests.test_ingredient_amount_estimator_gfm import nutrients_mixer
from database.postgres.postgres_db import PostgresDb


async def _run_calculation(
    services: Tuple[PostgresDb, ServiceProvider],
    ingredient_declaration: str,
    nutrient_values: dict[str, float],
    production_amount: float = 250,
) -> Tuple[Node, PostgresDb, ServiceProvider]:
    postgres_db, service_provider = services
    glossary_service = service_provider.glossary_service
    graph_mgr = postgres_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            production_amount={"value": production_amount, "unit": "gram"},
            titles=[
                {
                    "language": "en",
                    "value": "A sample recipe with 2 ingredients, to test the ingredient estimator",
                },
            ],
        )
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid, nutrient_values=nutrient_values)
    ingredients_declaration = [
        {
            "language": "de",
            "value": ingredient_declaration,
        },
    ]
    _ = await append_flow_node(graph_mgr, recipe.uid, ingredients_declaration=ingredients_declaration)

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(
        service_provider,
        glossary_service,
        node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    return root_node, postgres_db, service_provider


async def _run_calculation_with_defined_ingredients(
    services: Tuple[PostgresDb, ServiceProvider],
    chicken_label: dict | None = None,
    egg_label: dict | None = None,
    chicken_amount: float = 0.125,
) -> Tuple[Node, PostgresDb, ServiceProvider]:
    postgres_db, service_provider = services
    glossary_service = service_provider.glossary_service
    graph_mgr = postgres_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            titles=[
                {
                    "language": "en",
                    "value": "A sample recipe with 2 ingredients, to test the ingredient estimator",
                },
            ],
        )
    )

    # create first ingredient
    chicken_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "de", "value": "Hähnchen"}],
            amount_in_original_source_unit={"value": chicken_amount, "unit": "kilogram"},
            raw_labels=chicken_label,
        )
    )

    # add edge recipe --> chicken_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=chicken_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # create second ingredient
    egg_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "de", "value": "Eier"}],
            amount_in_original_source_unit={"value": 0.125, "unit": "kilogram"},
            raw_labels=egg_label,
        )
    )

    # add edge recipe --> egg_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=egg_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # create third ingredient
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": 0.125, "unit": "kilogram"},
        )
    )

    # add edge recipe --> carrot_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=carrot_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(
        service_provider,
        glossary_service,
        node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, return_log=True)

    calculation, root_node = await calc_service.calculate(calculation)

    return root_node, postgres_db, service_provider


@pytest.mark.asyncio
async def test_animal_welfare_rating_E_rating(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    egg_nutrients = load_seeded_nutrients["egg"]
    chicken_nutrients = load_seeded_nutrients["chicken"]

    mix_50_50_egg_chicken = nutrients_mixer((egg_nutrients, chicken_nutrients), (0.5, 0.5))
    root_node, postgres_db, service_provider = await _run_calculation(
        setup_services,
        "Eier, Hähnchen",
        mix_50_50_egg_chicken,
    )
    access_group_uid = service_provider.glossary_service.root_term.access_group_uid
    not_certified_for_animal_welfare = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_not_certified_for_animal_welfare", access_group_uid)
    ]
    assert is_close(
        root_node.animal_products.amount_for_activity_production_amount()
        .quantities[not_certified_for_animal_welfare.uid]
        .value,
        250,
    )
    assert root_node.animal_products.get_animal_welfare_rating() == "E"


@pytest.mark.asyncio
async def test_animal_welfare_rating_B_rating(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    _ = load_seeded_nutrients
    root_node, postgres_db, service_provider = await _run_calculation_with_defined_ingredients(
        setup_services,
        egg_label={
            "value": "free-range",
            "language": "asdf",
        },  # Because free-range is in LabelsOptionsEnum, it should be correctly matched even with the wrong language.
    )
    access_group_uid = service_provider.glossary_service.root_term.access_group_uid
    not_certified_for_animal_welfare = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_not_certified_for_animal_welfare", access_group_uid)
    ]
    animal_welfare_certified = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_animal_welfare_certified", access_group_uid)
    ]
    assert is_close(
        root_node.animal_products.amount_for_activity_production_amount()
        .quantities[not_certified_for_animal_welfare.uid]
        .value,
        125,
    )
    assert is_close(
        root_node.animal_products.amount_for_activity_production_amount()
        .quantities[animal_welfare_certified.uid]
        .value,
        125,
    )
    assert root_node.animal_products.get_animal_welfare_rating() == "B"


@pytest.mark.asyncio
async def test_animal_welfare_rating_A_rating(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    _ = load_seeded_nutrients
    root_node, postgres_db, service_provider = await _run_calculation_with_defined_ingredients(
        setup_services,
        egg_label={"value": "free-range", "language": "en"},
        chicken_amount=0.002,
    )
    access_group_uid = service_provider.glossary_service.root_term.access_group_uid
    not_certified_for_animal_welfare = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_not_certified_for_animal_welfare", access_group_uid)
    ]
    animal_welfare_certified = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_animal_welfare_certified", access_group_uid)
    ]
    assert is_close(
        root_node.animal_products.amount_for_activity_production_amount()
        .quantities[not_certified_for_animal_welfare.uid]
        .value,
        2,
    )
    assert is_close(
        root_node.animal_products.amount_for_activity_production_amount()
        .quantities[animal_welfare_certified.uid]
        .value,
        125,
    )
    assert root_node.animal_products.get_animal_welfare_rating() == "A"
