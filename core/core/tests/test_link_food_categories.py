"Tests for linking food categories gap filling module."
# ruff: noqa: D103
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingFactory
from gap_filling_modules.link_food_categories_gfm import LinkFoodCategoriesGapFillingFactory
from gap_filling_modules.match_product_name_gfm import MATCH_PRODUCT_GFM_NAME, MatchProductNameGapFillingFactory

from core.domain.matching_item import MatchingItem
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeRType, TermAccessGroupIdsRType
from database.postgres.postgres_db import PostgresDb


@pytest.mark.asyncio
async def test_link_food_categories_gfm(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    # First run MatchProductNameGFM and AttachFoodTagsGapFillingFactory to obtain product name.
    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(carrot_ingredient)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    # Then, run LinkFoodCategoriesGFM.
    gap_filling_worker = LinkFoodCategoriesGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    amount_per_category_in_flow = carrot_ingredient.amount_per_category_in_flow
    assert len(amount_per_category_in_flow.amount_for_100g().quantities) == 3

    amount_per_category_in_flow_terms = [
        service_provider.glossary_service.terms_by_uid[food_category].xid
        for food_category in amount_per_category_in_flow.amount_for_100g().quantities
    ]
    for food_category_term in amount_per_category_in_flow_terms:
        assert isinstance(food_category_term, str)

    food_category_xids = frozenset(amount_per_category_in_flow_terms)
    expected_food_category_xids = frozenset(
        {"EOS_Vegetables", "EOS_Plant-based-products", "EOS_Diet-low-in-vegetables"}
    )
    assert food_category_xids == expected_food_category_xids


@pytest.mark.asyncio
async def test_link_food_categories_gfm_with_wrong_node_type(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    # default NodeType is 'recipe'
    child_of_root_node = FoodProcessingActivityNode(uid=node_uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(child_of_root_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = LinkFoodCategoriesGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(child_of_root_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_link_food_categories_gfm_with_no_prod_name(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    gap_filling_worker = LinkFoodCategoriesGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.cancel


@pytest.mark.asyncio
async def test_link_food_categories_gfm_with_composed_name(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Matching for dried potatoes (trockene Kartoffeln)."""
    postgres_db, service_provider = setup_services

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        product_name=[
            {
                "language": "de",
                "value": "getrockene kartoffeln",
            }
        ],
        type="conceptual-ingredients",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # First run MatchProductNameGFM to obtain product name.
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)

    # Then, run LinkFoodCategories.
    gap_filling_factory = LinkFoodCategoriesGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)

    amount_per_category_in_flow = [
        service_provider.glossary_service.terms_by_uid[term_uid].xid
        for term_uid in ingredient_node.amount_per_category_in_flow.amount_for_100g().quantities
    ]
    assert len(amount_per_category_in_flow) == 2

    for food_category_term in amount_per_category_in_flow:
        assert isinstance(food_category_term, str)

    food_category_xids = frozenset(amount_per_category_in_flow)
    expected_food_category_xids = frozenset({"EOS_Vegetables", "EOS_Plant-based-products"})
    assert food_category_xids == expected_food_category_xids


@pytest.mark.asyncio
async def test_link_amount_per_category_in_flow_with_unknown_tags(
    setup_services: Tuple[PostgresDb, ServiceProvider], get_term_access_group_uids: TermAccessGroupIdsRType
) -> None:
    """Test the nutrient subdivision gap filling module.

    - run nutrition_subdivision_GFM, nothing happens
    - add subdivision Terms for cocoa
    - run nutrition_subdivision_GFM, subdivision happens
    """
    postgres_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    # create one ingredient & add it to the graph
    cocoa_ingredient = await postgres_db.get_graph_mgr().upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "de", "value": "Kakaopulver"}],
            type="conceptual-ingredients",
        )
    )
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=cocoa_ingredient.uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(cocoa_ingredient)

    # attach Term to ingredient & matching
    cocoa_term: Term = await postgres_db.pg_term_mgr.get_term_by_xid_and_access_group_uid(
        "A03HG", foodex2_term_group_uid
    )
    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module=MATCH_PRODUCT_GFM_NAME,
            term_uids=[str(cocoa_term.uid)],
            matching_string="Kakaopulver",
            lang="de",
        )
    )

    # First run MatchProductNameGFM to obtain product name.
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(cocoa_ingredient)
    await gap_filling_worker.run(calc_graph)

    # Then, run LinkFoodCategories.
    gap_filling_factory = LinkFoodCategoriesGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(cocoa_ingredient)
    await gap_filling_worker.run(calc_graph)

    assert len(calc_graph.data_errors_log) > 0
    food_category_data_error = False
    for data_error in calc_graph.data_errors_log:
        if data_error.startswith("Could not match") and data_error.endswith("to food categories"):
            food_category_data_error = True

    assert food_category_data_error
