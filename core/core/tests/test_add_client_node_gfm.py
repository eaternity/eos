"Tests for add client node gap filling module."
import pytest
from gap_filling_modules.abstract_gfm import GapFillingWorkerStatusEnum
from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingFactory

from core.domain.calculation import Calculation
from core.graph_manager.calc_graph import CalcGraph
from core.tests.conftest import MinimumRecipeRType


@pytest.mark.asyncio
async def test_add_child_nodes_to_recipe(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe_node, carrot_ingredient, carrot_process, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    assert not recipe_node.get_sub_nodes(), f"recipe {recipe_node} should not have sub-nodes before running this GFM"

    # Needed to appropriately pass "should_be_scheduled" check.
    calculation = Calculation(root_node_uid=recipe_node.uid, child_of_root_node_uid=recipe_node.uid)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe_node.uid,
        glossary_service=glossary_service,
        calculation=calculation,
    )
    recipe_node.added_by = "Orchestrator"
    calc_graph.add_node(recipe_node)

    gap_filling_factory = AddClientNodesGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe_node)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)
    assert recipe_node.get_sub_nodes(), f"recipe {recipe_node} should have sub-nodes after running this GFM"
    assert (
        recipe_node.get_sub_nodes()[0].uid == carrot_ingredient.uid
    ), f"recipe {recipe_node} should have ingredient {carrot_ingredient} as child after running this GFM"

    assert not carrot_process.get_sub_nodes(), (
        f"{carrot_process}'s child (tractor process) has not been loaded yet."
        f"It will be loaded once LinkTermToActivityNode GFM runs."
    )


@pytest.mark.asyncio
async def test_add_child_nodes_gfm_with_parent_node(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe_node, carrot_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    assert not recipe_node.get_sub_nodes(), f"Recipe {recipe_node} should not have sub-nodes before running this test."

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=recipe_node.uid,
        glossary_service=glossary_service,
    )

    # emulating a situation where there is a node with a parent node
    calc_graph.add_node(recipe_node)
    calc_graph.add_node(carrot_ingredient)
    calc_graph.add_edge(recipe_node.uid, carrot_ingredient.uid)
    assert recipe_node.get_sub_nodes(), f"Recipe {recipe_node} should have sub-nodes after adding an edge."

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = AddClientNodesGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)

    assert not gap_filling_worker.should_be_scheduled()
