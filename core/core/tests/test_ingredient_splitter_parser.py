import pytest
from gap_filling_modules.ingredient_splitter_gfm import IngredientSplitterParser
from structlog import get_logger

logger = get_logger()


def validate_nested_parse_results(actual_tokens: list[dict], expected_tokens: list[dict]):
    for actual, expected in zip(actual_tokens, expected_tokens):
        assert (
            actual["name"] == expected["name"]
        ), f"names don't match: expected {expected['name']} but got {actual['name']}"
        assert (
            actual["percentage"] == expected["percentage"]
        ), f"percentages don't match: expected {expected['percentage']} but got {actual['percentage']}"

        if "subproducts" in expected:
            validate_nested_parse_results(actual["subproducts"], expected["subproducts"])


def validate_parse(ingredient_list: str, expected_parse: list[dict]):
    """Convenience function to validate the parse of the IngredientSplitterParser against an expected parse"""
    parse = IngredientSplitterParser().parse(ingredient_list)
    for ig in parse:
        logger.debug(f"('{ig['name']}', {ig['percentage']}),")
    assert len(parse) == len(expected_parse), (
        f"number of parsed ingredients ({len(parse)}) does not match with "
        f"expected parse length ({len(expected_parse)})"
    )

    validate_nested_parse_results(parse, expected_parse)


@pytest.mark.asyncio
async def test_ingredient_splitter_parser_bliss_ball_peanut():
    validate_parse(
        "Datteln* getrocknet (25%), Reissirup*, Reisproteinpulver*, ERDNUSSMUS* (15%), geröstete ERDNÜSSE* "
        "gehackt (8,0%), CASHEWKERNE*, Kakao* stark entölt (3,3%), Erdmandelmehl*, Meersalz. * aus "
        "kontrolliert biologischem Anbau. Kann Spuren von SELLERIE, SENF, SOJA, anderen SCHALENFRÜCHTEN, "
        "LUPINE, SESAM, EI und MILCH enthalten.",
        [
            {"name": "Datteln getrocknet", "percentage": 25.0},
            {"name": "Reissirup", "percentage": None},
            {"name": "Reisproteinpulver", "percentage": None},
            {"name": "ERDNUSSMUS", "percentage": 15.0},
            {"name": "geröstete ERDNÜSSE gehackt", "percentage": 8.0},
            {"name": "CASHEWKERNE", "percentage": None},
            {"name": "Kakao stark entölt", "percentage": 3.3},
            {"name": "Erdmandelmehl", "percentage": None},
            {"name": "Meersalz", "percentage": None},
        ],
    )


@pytest.mark.asyncio
@pytest.mark.skip("fails to parse the 79% Mandelerzeugnis ATM")
async def test_ingredient_splitter_parser_streichgenuss_paprika_simplyv():
    validate_parse(
        "79% Mandelerzeugnis (Trinkwasser, 21% Mandeln), Kokosöl, 5% Paprika, Lauch, Zwiebel, Knoblauch, "
        "Zitronensaftkonzentrat, Meersalz, Verdickungsmittel: Johannisbrotkernmehl.",
        [
            {"name": "Mandelerzeugnis", "percentage": 79.0},
            {"name": "Kokosöl", "percentage": None},
            {"name": "Paprika", "percentage": 5.0},
            {"name": "Lauch", "percentage": None},
            {"name": "Zwiebel", "percentage": None},
            {"name": "Knoblauch", "percentage": None},
            {"name": "Zitronensaftkonzentrat", "percentage": None},
            {"name": "Meersalz", "percentage": None},
            {"name": "Verdickungsmittel: Johannisbrotkernmehl", "percentage": None},
        ],
    )


@pytest.mark.asyncio
async def test_ingredient_splitter_parser_streichgenuss_paprika_simplyv():
    validate_parse(
        "Veganes Erbsenproteinerzeugnis nach Art eines Würstchens (38 %) (Wasser, Sonnenblumenöl, "
        "Erbsenprotein, Verdickungsmittel: Carrageen, Konjak, Methylcellulose; Salz, Rapsöl, Aroma, "
        "Dextrose, Gewürze, Maltodextrin, Raucharoma, Gewürzextrakt, Farbstoff: Eisenoxid und -hydroxide), "
        "WEIZENMEHL (26 %), Margarine (Palmöl, Wasser, Rapsöl, Kokosöl, Emulgator: Mono- und Diglyceride "
        "von Speisefettsäuren; Säuerungsmittel: Citronensäure), Wasser, Ketchup (4,0 %) (Tomatenpüree, "
        "Zucker, Essig, modifizierte Maisstärke, modifizierte Kartoffelstärke, Salz, Gewürze, Rapsöl, "
        "Verdickungsmittel: Xanthan; natürliches Aroma), Hefe, Salz, Bohnenmehl, Zucker, "
        "Erbsenproteinisolat, Dextrose, WEIZENPROTEIN, Erbsenprotein, Emulgatoren: Mono- und "
        "Diacetylweinsäureester von Mono- und Diglyceriden von Speisefettsäuren, Glukosesirup, Reismehl, "
        "WEIZENENZYM, Antioxidationsmittel: Ascorbinsäure. Kann Spuren von SELLERIE, SENF, SOJA, "
        "SCHALENFRÜCHTEN, SESAM, EI und MILCH enthalten.",
        [
            {"name": "Veganes Erbsenproteinerzeugnis nach Art eines Würstchens", "percentage": 38.0},
            {"name": "WEIZENMEHL", "percentage": 26.0},
            {"name": "Margarine", "percentage": None},
            {"name": "Wasser", "percentage": None},
            {"name": "Ketchup", "percentage": 4.0},
            {"name": "Hefe", "percentage": None},
            {"name": "Salz", "percentage": None},
            {"name": "Bohnenmehl", "percentage": None},
            {"name": "Zucker", "percentage": None},
            {"name": "Erbsenproteinisolat", "percentage": None},
            {"name": "Dextrose", "percentage": None},
            {"name": "WEIZENPROTEIN", "percentage": None},
            {"name": "Erbsenprotein", "percentage": None},
            {
                "name": "Emulgatoren: Mono- und Diacetylweinsäureester von Mono- und Diglyceriden von "
                "Speisefettsäuren",
                "percentage": None,
            },
            {"name": "Glukosesirup", "percentage": None},
            {"name": "Reismehl", "percentage": None},
            {"name": "WEIZENENZYM", "percentage": None},
            {"name": "Antioxidationsmittel", "percentage": None},
            # TODO: why not "Antioxidationsmittel: Ascorbinsäure"?
        ],
    )


@pytest.mark.asyncio
async def test_discard_string_ending_followed_by_parenthesis():
    parsed = IngredientSplitterParser().parse("aaaaa (enthält bbbb, cccccc), ddddddddd")
    assert len(parsed) == 2, "there should only be aaaaa and ddddddddd"
    assert parsed[1]["name"] == "ddddddddd"


@pytest.mark.asyncio
async def test_inner_percentage_bracketing_with_integers():
    validate_parse(
        "MainIngrOne (Karotten (64%), Kartoffeln (36%)), MainIngrTwo (80% Zwiebeln, 20% Tomaten)",
        [
            {
                "name": "MainIngrOne",
                "percentage": None,
                "subproducts": [
                    {"name": "Karotten", "percentage": 64.0},
                    {"name": "Kartoffeln", "percentage": 36.0},
                ],
            },
            {
                "name": "MainIngrTwo",
                "percentage": None,
                "subproducts": [
                    {"name": "Zwiebeln", "percentage": 80.0},
                    {"name": "Tomaten", "percentage": 20.0},
                ],
            },
        ],
    )


@pytest.mark.asyncio
async def test_inner_percentage_bracketing_with_decimals():
    validate_parse(
        "MainIngrOne (Karotten 37.7%, Kartoffeln 77.3%), MainIngrTwo (80,6% Zwiebeln, 20,6% Tomaten)",
        [
            {
                "name": "MainIngrOne",
                "percentage": None,
                "subproducts": [
                    {"name": "Karotten", "percentage": 37.7},
                    {"name": "Kartoffeln", "percentage": 77.3},
                ],
            },
            {
                "name": "MainIngrTwo",
                "percentage": None,
                "subproducts": [
                    {"name": "Zwiebeln", "percentage": 80.6},
                    {"name": "Tomaten", "percentage": 20.6},
                ],
            },
        ],
    )


@pytest.mark.asyncio
async def test_inner_percentages_already_bracketed():
    validate_parse(
        "MainIngrOne 53.01% (Karotten (92.7%), Kartoffeln (7.3%)), "
        "MainIngrTwo (47%) (Zwiebeln (80,6%), Tomaten (19,4%))",
        [
            {
                "name": "MainIngrOne",
                "percentage": 53.01,
                "subproducts": [
                    {"name": "Karotten", "percentage": 92.7},
                    {"name": "Kartoffeln", "percentage": 7.3},
                ],
            },
            {
                "name": "MainIngrTwo",
                "percentage": 47,
                "subproducts": [
                    {"name": "Zwiebeln", "percentage": 80.6},
                    {"name": "Tomaten", "percentage": 19.4},
                ],
            },
        ],
    )


@pytest.mark.asyncio
async def test_outside_percentages_at_end():
    validate_parse(
        "MainIngrOne (Karotten (92.7%), Kartoffeln (7.3%)) (53%), "
        "MainIngrTwo (Zwiebeln (80,6%), Tomaten (19,4%)) 10%",
        [
            {
                "name": "MainIngrOne",
                "percentage": 53,
                "subproducts": [
                    {"name": "Karotten", "percentage": 92.7},
                    {"name": "Kartoffeln", "percentage": 7.3},
                ],
            },
            {
                "name": "MainIngrTwo",
                "percentage": 10,
                "subproducts": [
                    {"name": "Zwiebeln", "percentage": 80.6},
                    {"name": "Tomaten", "percentage": 19.4},
                ],
            },
        ],
    )


@pytest.mark.asyncio
async def test_inner_percentages_in_front():
    validate_parse(
        "MainIngrOne (92.7% Karotten, 7.3% Kartoffeln), MainIngrTwo (80,6% Zwiebeln, 19,4% Tomaten)",
        [
            {
                "name": "MainIngrOne",
                "percentage": None,
                "subproducts": [
                    {"name": "Karotten", "percentage": 92.7},
                    {"name": "Kartoffeln", "percentage": 7.3},
                ],
            },
            {
                "name": "MainIngrTwo",
                "percentage": None,
                "subproducts": [
                    {"name": "Zwiebeln", "percentage": 80.6},
                    {"name": "Tomaten", "percentage": 19.4},
                ],
            },
        ],
    )


@pytest.mark.asyncio
async def test_discard_string_ending_till_the_very_end():
    parsed = IngredientSplitterParser().parse(
        "Erdmandelmehl*, Meersalz. * aus kontrolliert biologischem Anbau. Kann "
        "Spuren von SELLERIE, SENF, SOJA, anderen SCHALENFRÜCHTEN enthalten."
    )
    assert len(parsed) == 2, "there should only be Erdmandelmehl and Meersalz"
    assert parsed[1]["name"] == "Meersalz"


@pytest.mark.asyncio
async def test_validate_balanced_parenthesis():
    with pytest.raises(ValueError):
        IngredientSplitterParser().parse("this text's parenthesis (are not balanced")
    with pytest.raises(ValueError):
        IngredientSplitterParser().parse("this text's parenthesis are not ) balanced either")
    IngredientSplitterParser().parse("but this text is balanced")
