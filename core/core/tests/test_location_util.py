from gap_filling_modules.location_util import countries


def test_iso_3166_2to3_integrity() -> None:
    alpha2 = set(countries.iso_3166_official_2_letter_country_codes.keys())
    alpha3 = set(countries.iso_3166_official_3_letter_country_codes.keys())
    two2three = countries.iso_3166_map_2_to_3_letter

    assert alpha2.difference(set(two2three.keys())) == set()
    assert alpha3.difference(set(two2three.values())) == set()
    assert set(two2three.keys()).difference(alpha2) == set()
    assert set(two2three.values()).difference(alpha3) == set()
