"Tests for Glossary Service."
from typing import Any
from unittest.mock import patch

import httpx
import pytest
import structlog

from core.tests.conftest import MinimumRecipeRType

logger = structlog.get_logger()


@pytest.mark.asyncio
async def test_binary_cache_available(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    assert glossary_service.terms_by_uid and glossary_service.terms_by_xid_ag_uid, "Glossary service not initialized"

    def get_cache_mock(**kwargs: dict[str, Any]) -> None:
        return httpx.Response(status_code=200, content=glossary_service.terms_by_uid_binary)

    # Patch the cache endpoint
    with patch.object(httpx, "get", new=get_cache_mock):
        terms_from_cache = await glossary_service._get_all_terms_from_running_pods()
        terms_from_db = await postgres_db.pg_term_mgr.find_all()

        assert len(terms_from_cache) == len(terms_from_db), "terms from cache and DB do not match"
        for cached_term, db_term in zip(terms_from_cache, terms_from_db):
            assert cached_term == db_term, "terms from cache and DB do not match"


@pytest.mark.asyncio
async def test_binary_cache_unavailable(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    assert glossary_service.terms_by_uid and glossary_service.terms_by_xid_ag_uid, "Glossary service not initialized"

    def get_cache_mock(**kwargs: dict[str, Any]) -> None:
        return httpx.Response(status_code=408)

    def get_from_db_mock(**kwargs: dict[str, Any]) -> None:
        raise NotImplementedError

    # Patch the cache endpoint
    with patch.object(httpx, "get", new=get_cache_mock), patch.object(
        postgres_db.pg_term_mgr, "find_all", new=get_from_db_mock
    ):
        with pytest.raises(NotImplementedError):
            await glossary_service._get_all_terms_from_running_pods()
