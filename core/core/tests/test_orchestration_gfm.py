"Test for orchestration gap filling module."
import uuid
from typing import List, Optional

import pytest
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import ElementaryResourceEmissionNode, FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.orchestrator.orchestrator import Orchestrator
from core.service.service_provider import ServiceProvider
from core.tests.conftest import (
    EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_COOLING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_FREEZING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_FUEL_CONSUMPTION_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_INFRASTRUCTURE_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_DEFAULT_TRANSPORT,
    EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_TRANSPORT_FREEZING_CO2_AMOUNT_PER_KG_DEFAULT_TRANSPORT,
    ElaborateRecipe,
    MinimumRecipeForUnitWeightGFM,
    MinimumRecipeRType,
    MinimumRecipeWithDeclaration,
    MinimumRecipeWithTwoOrigins,
    MinimumSupplyRType,
    MinimumSupplyWithTomato,
    add_flow,
    is_close,
)
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID
from database.postgres.postgres_db import PostgresDb

logger = get_logger()

CH_INGREDIENT_ORIGIN_SPLIT = {
    "ES": 0.012833891449972747,
    "IT": 0.010832174449104889,
    "DK": 0.007651294744364455,
    "NL": 0.00277471359835878,
    "PT": 0.0022550694176551394,
    "CH": 0.963652856340544,
}

ES_INGREDIENT_ORIGIN_SPLIT = {
    "FR": 0.04982663949254379,
    "PT": 0.012275249153074458,
    "NL": 0.0043598929287616784,
    "DE": 0.0028313652535985685,
    "ES": 0.9307068531720215,
}


async def run_orchestrator_on_recipe(
    service_provider: ServiceProvider,
    pg_db: PostgresDb,
    root: Node,
    recipe: Node,
    return_log: bool = False,
    requested_quantity_references: Optional[List[ReferenceAmountEnum]] = None,
) -> tuple[CalcGraph, Calculation]:
    # create a new Calculation object
    calculation = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root.uid,
        child_of_root_node_uid=recipe.uid,
        return_log=return_log,
        requested_quantity_references=requested_quantity_references if requested_quantity_references else [],
    )

    # first persist this calculation to db (without mutations and results yet):
    await pg_db.get_calc_mgr().insert_calculation(calculation)

    # create the calculation graph:
    logger.debug("creating the calculation graph...")
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=calculation.root_node_uid,
        glossary_service=service_provider.glossary_service,
        calculation=calculation,
    )

    # initialize orchestrator with the calculation graph:
    logger.debug("creating the orchestrator...")
    gfm_loader = GapFillingModuleLoader()
    await gfm_loader.init(service_provider=service_provider)

    orchestrator = Orchestrator(
        calc_graph=calc_graph,
        glossary_service=service_provider.glossary_service,
        gap_filling_module_loader=gfm_loader,
        print_as_tree_between_gfm_runs=True,
    )

    # start running the orchestrator, which will internally call gap filling modules:
    await orchestrator.run()

    if calculation.return_log:
        calculation.mutations = calc_graph.get_mutation_log()

    await pg_db.get_calc_mgr().upsert_by_uid(calculation)

    logger.debug("mutation log:")
    for i, log in enumerate(calc_graph.get_mutation_log()):
        logger.debug(f"{i}.   {log}")

    return calc_graph, calculation


def recursive_collect_emission_nodes(node: Node, collect: list[Node]) -> list[Node]:
    if isinstance(node, ElementaryResourceEmissionNode):
        collect.append(node)
    for child in node.get_sub_nodes():
        recursive_collect_emission_nodes(child, collect)
    return collect


def recursive_collect_unique_emission_nodes(node: Node, collect: set[Node]) -> list[Node]:
    if isinstance(node, ElementaryResourceEmissionNode):
        collect.add(node)
    for child in node.get_sub_nodes():
        recursive_collect_unique_emission_nodes(child, collect)
    return collect


@pytest.mark.asyncio
async def test_orchestration_recipe(create_minimum_recipe: MinimumRecipeRType) -> None:
    await common_test_orchestration_recipe_or_supply(create_minimum_recipe)


@pytest.mark.asyncio
async def test_orchestration_supply(create_minimum_supply: MinimumSupplyRType) -> None:
    await common_test_orchestration_recipe_or_supply(create_minimum_supply)


async def common_test_orchestration_recipe_or_supply(
    create_minimum_recipe_or_supply: MinimumRecipeRType | MinimumSupplyRType,
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    pg_db, service_provider, recipe_or_supply, carrot_ingredient, _, root_flow = create_minimum_recipe_or_supply

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    root_water_scarcity = service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
    carrot_water_scarcity = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("WS_sample_1", root_water_scarcity.access_group_uid)
    ]

    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe_or_supply)

    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 14, (
        "should have 14 emission nodes"
        "(2 from dummy tractor LCA, 3x(1(for fuel)+2(for infrastructure) of transport),"
        " 2 for cooling the transport, 1 for water scarcity)"
    )
    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes"

    assert carrot_ingredient.amount_in_original_source_unit.unit == "kilogram", "should have correct unit"
    amount_g_first_ingredient = carrot_ingredient.amount_in_original_source_unit.value * 1000  # converting kg to gram
    # Factor of 2 in the expected co2 is from root flow having amount = 2 production_amount.
    expected_co2 = 2 * (
        (
            EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
            + EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
        )
        * amount_g_first_ingredient
        / 1000
    )
    expected_water_scarcity = 2 * (
        carrot_water_scarcity.data[
            calc_graph.child_of_root_node()
            .get_sub_nodes()[0]
            .get_sub_nodes()[0]
            .get_sub_nodes()[0]
            .flow_location[0]
            .country_code
        ]
        * amount_g_first_ingredient
    )
    assert is_close(
        calc_graph.get_root_node()
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        expected_co2,
    ), "root node should have correct CO2 amount"
    assert (
        calc_graph.get_root_node().rainforest_critical_products.get_rainforest_rating() == "B"
    ), "Rainforest rating should be equal to 'B'."

    assert is_close(
        calc_graph.get_root_node().scarce_water_consumption.amount_for_root_node().value,
        expected_water_scarcity,
    ), "root node should have correct scarce water consumption amount"

    assert is_close(
        calc_graph.get_root_node().scarce_water_consumption.amount_for_root_node().value,
        calc_graph.child_of_root_node().get_sub_nodes()[0].scarce_water_consumption.amount_for_root_node().value,
    ), "ingredient node should have correct scarce water consumption amount"


@pytest.mark.asyncio
async def test_orchestration_on_tomato(create_minimum_supply_with_tomato: MinimumSupplyWithTomato) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    pg_db, service_provider, supply, tomato_ingredient, _, root_flow = create_minimum_supply_with_tomato

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, supply)

    # currently 4 (1 from plastic, 1 from glass 1 from electricity and 1 form heating) from greenhouse + 1 from water
    # scarcity = 5 total  more emission nodes compared to the tomato recipe without greenhouse,
    # which has 9 emission nodes
    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 5, "should have 5 emission nodes"
    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 2
    ), "should have 2 emission nodes (1 for greenhouse and 1 for water scarcity)"

    # regression tests for the total CO2 amount
    expected_co2 = 0.0893266274422175
    calculated_co2 = (
        calc_graph.get_root_node().impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    assert is_close(calculated_co2, expected_co2), "root node should have correct CO2 amount"

    # compare to the excel sheet:
    calculated_co2_value_without_greenhouse = 0.0  # no transport or other CO2 emissions
    amount_of_tomato_in_kg = tomato_ingredient.amount_in_original_source_unit.value  # = 0.15 kg
    calculated_co2_for_heating_infrastructure_and_electricity_per_kg = (
        calculated_co2 - calculated_co2_value_without_greenhouse
    ) / amount_of_tomato_in_kg
    expected_co2_for_infrastructure_per_kg = 0.0629228460178026  # from excel
    expected_co2_for_electricity_per_kg = 0.174997081830983  # from excel
    expected_co2_for_heating_per_kg = 0.3575909218  # to get this value from excel:
    # hardcode k_j to 3,4 in sheet Berechnung 1 und 4. then
    # change in Eingabe 4 in sheet Übersicht Produkt to Tomaten and Herkunftsland and Production to IP to Niederlande to
    # get monthly "Heizbedarf in diesem Monat" and then scale these values with number of growing days
    # in each month "NUMBER_OF_GROWING_DAYS_IN_EACH_MONTH" divided by the average day per month. Sum of the scaled
    # value gives the total flow amount needed.
    expected_co2_for_heating_electricity_and_infrastructure_per_kg = (
        expected_co2_for_electricity_per_kg + expected_co2_for_infrastructure_per_kg + expected_co2_for_heating_per_kg
    )
    assert is_close(
        calculated_co2_for_heating_infrastructure_and_electricity_per_kg,
        expected_co2_for_heating_electricity_and_infrastructure_per_kg,
        0.001,
    )


@pytest.mark.asyncio
async def test_orchestration_with_ingredients_declaration(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    pg_db, service_provider, recipe, root_flow, _sub_flow = create_minimum_recipe_with_small_declaration

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    root_water_scarcity = service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
    carrot_water_scarcity = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("WS_sample_1", root_water_scarcity.access_group_uid)
    ]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    expected_water_scarcity = (
        sum([carrot_water_scarcity.data[key] * val for key, val in CH_INGREDIENT_ORIGIN_SPLIT.items()]) * 1000
    )

    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 66, (
        "should have 54 emission nodes (24 carrot nodes + (18 + 12 (from cooling)) transport nodes"
        " + 12 scarce water consumption nodes)"
    )
    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes (2 carrot nodes + 1 scarce water consumption node)"

    assert is_close(
        calc_graph.get_root_node()
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        (
            EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_COOLING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
        ),
    ), "root node should have correct CO2 amount"
    assert is_close(
        calc_graph.get_root_node().scarce_water_consumption.value,
        expected_water_scarcity,
    ), "root node should have correct scarce water consumption amount"


@pytest.mark.asyncio
async def test_orchestration_with_ingredients_declaration_and_no_transport_lci_nodes(
    create_minimum_recipe_with_small_declaration_and_no_transport_lci: MinimumRecipeWithDeclaration,
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    (
        pg_db,
        service_provider,
        recipe,
        root_flow,
        _sub_flow,
    ) = create_minimum_recipe_with_small_declaration_and_no_transport_lci

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 54, (
        "should have 30 emission nodes (24 carrot nodes + 6 transport fuel consumption nodes"
        " + 12 transport cooling nodes + 12 scarce water consumption)"
    )
    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes (2 carrot nodes + 1 scarce water consumption)"

    result_co2 = (
        calc_graph.get_root_node().impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    assert is_close(
        result_co2,
        (
            EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_FUEL_CONSUMPTION_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_COOLING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
        ),
    ), "root node should have correct CO2 amount"


@pytest.mark.asyncio
async def test_orchestration_with_ingredients_declaration_and_multiple_terms(
    create_minimum_recipe_with_small_declaration_and_dried_ingredient: MinimumRecipeWithDeclaration,
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    (
        pg_db,
        service_provider,
        recipe,
        root_flow,
        _sub_flow,
    ) = create_minimum_recipe_with_small_declaration_and_dried_ingredient

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    root_water_scarcity = service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
    carrot_water_scarcity = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("WS_sample_1", root_water_scarcity.access_group_uid)
    ]
    vitascore_char_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_vita-score", root_char_term.access_group_uid)
    ]

    root_unit_term = service_provider.glossary_service.root_subterms["EOS_units"]
    daly_term = service_provider.glossary_service.terms_by_xid_ag_uid[("EOS_dalys", root_unit_term.access_group_uid)]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    root_node = calc_graph.get_root_node()
    child_of_root_node = calc_graph.child_of_root_node()
    mixing_activity_node = child_of_root_node.get_sub_nodes()[0].get_sub_nodes()[0]

    root_daily_food_unit = root_node.daily_food_unit.amount_for_activity_production_amount().value
    sum_of_sub_node_daily_food_unit = 0.0

    for ingredient_node in mixing_activity_node.get_sub_nodes():
        assert len(ingredient_node.product_name.terms) == 2

        origin_splits = ingredient_node.get_sub_nodes()[0].get_sub_nodes()

        combined_flow_dfu = ingredient_node.daily_food_unit
        sum_of_sub_node_daily_food_unit += combined_flow_dfu.amount_for_root_node().value
        combined_flow_vitascore = ingredient_node.vitascore.quantities
        combined_flow_vitascore_legacy = ingredient_node.vitascore_legacy.quantities
        combined_flow_food_category = ingredient_node.amount_per_category_in_flow
        assert combined_flow_dfu
        assert combined_flow_vitascore
        assert combined_flow_food_category

        assert is_close(
            combined_flow_vitascore_legacy.get("amount_for_root_node").get(vitascore_char_term.uid).value,
            280.1195536371073,
        )
        assert is_close(
            combined_flow_vitascore_legacy.get("amount_for_activity_production_amount")
            .get(vitascore_char_term.uid)
            .value,
            280.1195536371073,
        )
        assert is_close(
            combined_flow_vitascore.get("amount_for_root_node").get(vitascore_char_term.uid).value,
            318.09955363710725,
        )
        assert is_close(
            combined_flow_vitascore.get("amount_for_activity_production_amount").get(vitascore_char_term.uid).value,
            318.09955363710725,
        )
        assert (
            combined_flow_vitascore.get("amount_for_activity_production_amount")
            .get(vitascore_char_term.uid)
            .unit_term_uid
            == daly_term.uid
        )

        for _idx, origin_split in enumerate(origin_splits):
            assert len(origin_split.product_name.terms) == 2

    assert (  # no cooling since the ingredient are "dried carrots -> not perishable"
        len(recursive_collect_emission_nodes(root_node, [])) == 36
    ), "should have 36 emission nodes (12 carrot nodes + 18 transport nodes + 6 scarce water consumption)"
    assert (
        len(recursive_collect_unique_emission_nodes(root_node, set())) == 3
    ), "should have 3 emission nodes (2 carrot nodes + 1 scarce water consumption)"

    assert is_close(root_daily_food_unit, sum_of_sub_node_daily_food_unit)

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    result_co2 = root_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    assert is_close(
        result_co2, EXPECTED_CO2_AMOUNT_PER_KG_CARROT + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
    ), "root node should have correct CO2 amount"

    expected_water_scarcity = (
        sum([carrot_water_scarcity.data[key] * val for key, val in CH_INGREDIENT_ORIGIN_SPLIT.items()]) * 1000
    )
    assert is_close(
        calc_graph.get_root_node().scarce_water_consumption.value,
        expected_water_scarcity,
    ), "root node should have correct scarce water consumption amount"


@pytest.mark.asyncio
async def test_orchestration_with_ingredients_declaration_and_frozen_ingredient(
    create_minimum_recipe_with_small_declaration_and_frozen_ingredient: MinimumRecipeWithDeclaration,
) -> None:
    "Orchestration of all GFMs over the minimum_recipe fixture with ingredient that has 'PRESERVED BY FREEZING' term."
    (
        pg_db,
        service_provider,
        recipe,
        root_flow,
        _sub_flow,
    ) = create_minimum_recipe_with_small_declaration_and_frozen_ingredient

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    root_node = calc_graph.get_root_node()
    child_of_root_node = calc_graph.child_of_root_node()
    mixing_activity_node = child_of_root_node.get_sub_nodes()[0].get_sub_nodes()[0]

    for ingredient_node in mixing_activity_node.get_sub_nodes():
        assert len(ingredient_node.product_name.terms) == 2

        origin_splits = ingredient_node.get_sub_nodes()[0].get_sub_nodes()

        for origin_split in origin_splits:
            assert len(origin_split.product_name.terms) == 2

            subdivision_splits = origin_split.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()

            for subdivision_split in subdivision_splits:
                assert len(subdivision_split.product_name.terms) == 2

    assert (
        len(recursive_collect_emission_nodes(root_node, [])) == 54
    ), "should have 54 emission nodes (24 carrot nodes + (18 + 12) transport nodes)"
    assert (
        len(recursive_collect_unique_emission_nodes(root_node, set())) == 2
    ), "should have 2 emission nodes (2 carrot nodes)"

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    result_co2 = (
        calc_graph.get_root_node().impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    assert is_close(
        result_co2,
        (
            EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_FREEZING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_FUEL_CONSUMPTION_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_INFRASTRUCTURE_CO2_AMOUNT_PER_KG_CARROT
        ),
    ), "root node should have correct CO2 amount"


@pytest.mark.asyncio
async def test_orchestration_with_ingredients_declaration_and_two_recipe_origins(
    create_minimum_recipe_with_small_declaration_and_two_origins: MinimumRecipeWithTwoOrigins,
) -> None:
    """Tests the whole orchestration of all GFMs.

    over the `create_minimum_recipe_with_small_declaration_and_two_origins` fixture to make sure that origin
    subdivisions for both combined products and monoproducts don't break anything.
    """
    pg_db, service_provider, recipe, _, root_flow = create_minimum_recipe_with_small_declaration_and_two_origins

    root_water_scarcity = service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
    carrot_water_scarcity = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("WS_sample_1", root_water_scarcity.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    # Check if the number of emission nodes is correct:
    # A) Emissions from carrot production (without transport):
    #    The sub-recipe containing carrots is produced in Spain and Switzerland and then transported to Zurich.
    #    CH has 6 origins and ES has 5 origins of carrots (according to origin model).
    #    --> In total there are 11 carrot origins (each with 2 subdivisions x 2 emission nodes each).
    #    --> In total 11*2*2 = 44 emission nodes from carrot production.
    # B) Emissions from transport:
    #    There are 6+5+2 = 13 transports (6 from CH, 5 from ES, 2 from sub-recipe to root-recipe).
    #    Each transport has 3 emission nodes (1 for fuel consumption + 2 for infrastructure).
    #    --> In total there are 13*3 = 39 emission nodes from transport.
    # Change on 06.02.2024: Cooled transport is now not present because the ingredients are generic-conserved
    # Change on 21.02.2024: The change from 06.02.2024 is reverted because with the new definitions, the recipe
    # is no loner a convenience product because it is not a combined product anymore (it does not combine more than
    # one different ingredient but is just an origin split).
    # Change on 12.04.204: Now the sub-recipe is again defined as a combined product, since we define no all
    # food-products that have an activity with ingredients-declaration as combined products.
    assert (
        len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 105
    ), "should have 105 emission nodes (44 carrot emission nodes + 39 transport emissions+ 22 scarce water consumption)"

    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes (2 carrot nodes + 1 scarce water consumption)"

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    result_co2 = (
        calc_graph.get_root_node().impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    # adding less CO2 here since in this test we calculate distances to Switzerland centroid,
    # not to canton of Zurich
    # excess_switzerland_transport_amount = -6.186969716053682 / 1000
    # transport_from_spain_co2_amount = 35.39558556481143 / 1000
    # TODO Add back detailed difference in CO2 amount. Rather than just a single number difference.
    # TODO Figure out how Alexei got the numbers. Now that the bugs in fuel consumption are fixed, the numbers changed.
    # transport_difference = 34.875660151131287 / 1000

    # TODO: I don't understand above calculation of expected transport_difference.
    # TODO: Also, there is now cooling added for some of the transports, which is not included in the above calculation.
    # So instead just using the current output as the expected value for now:
    # change on 20.02: with the new definition the recipe is no longer a combined product -> carrots don't get generic
    # conserved anymore and need cooling
    # change on 12.04. now the carrots don't need cooling again, because due to the ingredients-decaration the recipe is
    # a combined product
    expected_transport_total_co2_amount = 0.16831904118

    ch_expected_water_scarcity = (
        sum([carrot_water_scarcity.data[key] * val for key, val in CH_INGREDIENT_ORIGIN_SPLIT.items()]) * 1000
    )

    es_expected_water_scarcity = (
        sum(
            [
                carrot_water_scarcity.data.get(key, carrot_water_scarcity.data["median"]) * val
                for key, val in ES_INGREDIENT_ORIGIN_SPLIT.items()
            ]
        )
        * 1000
    )

    assert is_close(
        result_co2, EXPECTED_CO2_AMOUNT_PER_KG_CARROT + expected_transport_total_co2_amount
    ), "root node should have correct CO2 amount"

    assert is_close(
        calc_graph.get_root_node().scarce_water_consumption.amount_for_root_node().value,
        (ch_expected_water_scarcity + es_expected_water_scarcity) / 2,
    )


@pytest.mark.asyncio
async def test_orchestration_with_food_product_flow_and_declaration(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
) -> None:
    """Tests the whole orchestration of all GFMs over a flawed minimum_recipe fixture."""
    (
        pg_db,
        service_provider,
        recipe,
        _,
        _,
        _,
        _,
        root_flow,
    ) = create_minimum_recipe_for_unit_weight_gfm

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    found_missing_unit_error = False
    for error_log in calc_graph.data_errors_log:
        if error_log.startswith("No matching unit term found for"):
            found_missing_unit_error = True

    assert found_missing_unit_error


@pytest.mark.asyncio
async def test_orchestration_with_more_elaborate_recipe(create_more_elaborate_recipe: ElaborateRecipe) -> None:
    """Tests the whole orchestration of all GFMs over the create_more_elaborate_recipe fixture."""
    er = create_more_elaborate_recipe

    calc_graph, calculation = await run_orchestrator_on_recipe(er.service_provider, er.pg_db, er.root_flow, er.recipe)

    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 5, "should have 5 emission nodes"
    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes"

    flow_quantities: dict[uuid.UUID, float] = calc_graph.child_of_root_node().environmental_flows.flow_quantities
    assert is_close(flow_quantities[er.co2_process.uid], 120)
    assert (
        calc_graph.child_of_root_node()
        .environmental_flows.emission_node_uid_to_production_amount_unit(er.co2_process.uid, calc_graph=calc_graph)
        .name.lower()
        == "kilogram"
    )
    assert is_close(flow_quantities[er.sulfur_process.uid], 14)
    assert is_close(flow_quantities[er.crude_oil_process.uid], -100)

    root_char_term = er.service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = er.service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    assert is_close(
        calc_graph.get_root_node()
        .impact_assessment.amount_for_activity_production_amount()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        120.0,
    )
    assert is_close(
        calc_graph.get_root_node()
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        120.0,
    )

    def print_tree(_node: Node, depth: int) -> None:
        logger.debug("  " * depth + f"{_node}")
        for sub_node in _node.get_sub_nodes():
            if depth < 30:
                print_tree(sub_node, depth + 1)  # recursive

    print_tree(calc_graph.get_root_node(), 0)


@pytest.mark.asyncio
async def test_orchestration_with_more_elaborate_recipe_and_subrecipe(
    create_more_elaborate_recipe: ElaborateRecipe,
) -> None:
    """Tests that the calculation also works with subrecipes."""
    er = create_more_elaborate_recipe

    graph_mgr = er.pg_db.get_graph_mgr()
    top_recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            # activity_location="Zürich Schweiz", # now we have OriginGFM setting unknown origin for the subrecipe, but
            # for the reference value we don't have transport to the final location
            titles=[
                {
                    "language": "de",
                    "value": "a top recipe with another sub-recipe: more_elaborate_recipe (from the book)",
                }
            ],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )
    # create a new root-flow as the top_recipe is prepended also the flow needs to be a new one
    root_flow = await graph_mgr.upsert_node_by_uid(FoodProductFlowNode())

    # add edge Root flow --> activitiy node
    await graph_mgr.add_edge(
        Edge(parent_uid=root_flow.uid, child_uid=top_recipe.uid, edge_type=EdgeTypeEnum.root_to_recipe)
    )
    # recipe --> sub_recipe
    await add_flow(
        er.pg_db.get_graph_mgr(),
        top_recipe,
        er.recipe,
        1.0,
        activity_type=FoodProductFlowNode.__name__,
        make_link_to_sub_node=True,
    )

    calc_graph, calculation = await run_orchestrator_on_recipe(er.service_provider, er.pg_db, root_flow, top_recipe)

    # checking that impact_assessment GFM has run correctly
    root_char_term = er.service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = er.service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]
    assert is_close(
        calc_graph.get_root_node()
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        0.120,
    ), "calculation doesn't work with subrecipes"


@pytest.mark.asyncio
async def test_orchestration_with_recipe_and_declaration_subrecipe(
    create_minimum_recipe: MinimumRecipeRType,
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    """Tests that the calculation also works with subrecipes."""
    pg_db, service_provider, parent_recipe, carrot_ingredient, _, root_flow = create_minimum_recipe
    _, _, child_recipe, _root_flow, _sub_flow = create_minimum_recipe_with_small_declaration

    # recipe --> sub_recipe
    # we use 32g of the sub-recipe, and add it to the 150g of carrot that the parent recipe already has
    amount_g_of_second_subrecipe = 32.0
    await add_flow(
        pg_db.get_graph_mgr(),
        parent_recipe,
        child_recipe,
        amount_g_of_second_subrecipe,
        activity_type=FoodProductFlowNode.__name__,
        make_link_to_sub_node=True,
        nutrient_values=_root_flow.nutrient_values,
    )

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, parent_recipe)

    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 80, (
        "should have 67 emission nodes (26 carrot nodes"
        " + (18+12) transport emission nodes from minimum_recipe_with_small_declaration"
        " + (9+2) transport emission nodes from the air transport of the carrot in the root recipe "
        "+ 13 scarce water consumnption)"
    )
    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes (2 carrot nodes + 1 scarce water consumption)"

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    # checking that impact_assessment GFM has run correctly
    impact_assessment_amount = (
        calc_graph.get_root_node().impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    assert carrot_ingredient.amount_in_original_source_unit.unit == "kilogram", "should have correct unit"
    amount_g_first_ingredient = carrot_ingredient.amount_in_original_source_unit.value * 1000  # converting kg to gram
    expected_co2 = (
        (EXPECTED_CO2_AMOUNT_PER_KG_CARROT * (amount_g_first_ingredient + amount_g_of_second_subrecipe) / 1000)
        + (
            (EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT + EXPECTED_COOLING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT)
            * amount_g_of_second_subrecipe
            / 1000
        )
        + (
            (
                EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
                + EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
            )
            * amount_g_first_ingredient
            / 1000
        )
    )
    # Multiply by 2 for scaling for production amounts in create_minimum_recipe.
    assert is_close(impact_assessment_amount, expected_co2 * 2), "calculation doesn't work with subrecipes"

    subrecipe = calc_graph.child_of_root_node().get_sub_nodes()[0].get_sub_nodes()[0]

    char_subrecipe_for_root = (
        subrecipe.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    char_subrecipe_for_local = (
        subrecipe.impact_assessment.amount_for_activity_production_amount().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    # check if subrecipe co2eq per kg (as required for producing the root node) is correct:
    expected_co2_second_subrecipe_for_root = (
        (
            EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_COOLING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
        )
        * amount_g_of_second_subrecipe
        / 1000
    )
    # Multiply expected_co2_second_subrecipe_for_root by 2 because root_flow has amount of 2 production_amounts.
    assert is_close(
        char_subrecipe_for_root, expected_co2_second_subrecipe_for_root * 2
    ), "impact assessment of subrecipes for root_node production does not work."

    # check if co2eq per local unit production_amount is correct:
    expected_co2_second_subrecipe_for_local = (
        EXPECTED_CO2_AMOUNT_PER_KG_CARROT
        + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
        + EXPECTED_COOLING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT
    )
    assert is_close(
        char_subrecipe_for_local, expected_co2_second_subrecipe_for_local
    ), "impact assessment of subrecipes for local_node production does not work."


@pytest.mark.asyncio
async def test_on_fish_recipe_from_known_origin(
    create_minimum_fish_recipe_with_fresh_and_air_transport_from_spain: MinimumRecipeRType,
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    (
        pg_db,
        service_provider,
        recipe,
        tuna_ingredient,
        _,
        root_flow,
    ) = create_minimum_fish_recipe_with_fresh_and_air_transport_from_spain

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]

    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 11, (
        "should have 11 emission nodes"
        "3x(1(for fuel)+2(for infrastructure) of transport),"
        "2 for cooling the transport"
    )
    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 2
    ), "should have 2 emission nodes"

    assert tuna_ingredient.amount_in_original_source_unit.unit == "kilogram", "should have correct unit"
    amount_g_first_ingredient = tuna_ingredient.amount_in_original_source_unit.value * 1000  # converting kg to gram
    # Factor of 2 in the expected co2 is from root flow having amount = 2 production_amount.
    expected_co2 = 2 * (
        (
            EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
            + EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
        )
        * amount_g_first_ingredient
        / 1000
    )

    assert is_close(
        calc_graph.get_root_node()
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        expected_co2,
    ), "root node should have correct CO2 amount"


@pytest.mark.asyncio
async def test_on_fish_recipe_from_unknown_origin(
    create_minimum_fish_recipe_with_fresh_unknown_origin: MinimumRecipeRType,
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    (
        pg_db,
        service_provider,
        recipe,
        tuna_ingredient,
        _,
        root_flow,
    ) = create_minimum_fish_recipe_with_fresh_unknown_origin

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]

    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 11, (
        "should have 11 emission nodes"
        "3x(1(for fuel)+2(for infrastructure) of transport),"
        "2 for freezing the transport"
    )
    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 2
    ), "should have 2 emission nodes"

    assert tuna_ingredient.amount_in_original_source_unit.unit == "kilogram", "should have correct unit"
    amount_g_first_ingredient = tuna_ingredient.amount_in_original_source_unit.value * 1000  # converting kg to gram
    # Factor of 2 in the expected co2 is from root flow having amount = 2 production_amount.
    expected_co2 = 2 * (
        (
            EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_DEFAULT_TRANSPORT
            + EXPECTED_TRANSPORT_FREEZING_CO2_AMOUNT_PER_KG_DEFAULT_TRANSPORT
        )
        * amount_g_first_ingredient
        / 1000
    )
    assert is_close(
        calc_graph.get_root_node()
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value,
        expected_co2,
    ), "root node should have correct CO2 amount"
