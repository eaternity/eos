"Test Node Service."
import asyncio
import copy
import uuid
from typing import Any, Tuple
from unittest.mock import patch

import httpx
import pytest
import pytest_asyncio
import structlog

from core.domain.nodes.activity.food_processing_activity_node import FoodProcessingActivityNode
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.index_prop import IndexProp
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.service.messaging_service import MessagingService
from core.service.node_service import NodeService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeForUnitWeightGFM, MinimumRecipeRType, MinimumRecipeWithTwoOrigins, is_close
from database.postgres.postgres_db import PostgresDb

logger = structlog.get_logger()


@pytest_asyncio.fixture
async def setup_messaging_service(
    setup_services: Tuple[PostgresDb, ServiceProvider],
) -> None:
    """Fixture to get a clean (resetted) db for testing."""
    _, service_provider = setup_services

    # add messaging_service to service_provider:
    service_provider.messaging_service = MessagingService(service_provider)
    await service_provider.messaging_service.start()

    # reinit so that node service is using the messaging_service:
    await service_provider.node_service.init()

    await service_provider.messaging_service.start_worker_channel()

    # yield the messaging_service so that the test can use it:
    yield service_provider.messaging_service

    # teardown after test is complete:
    await service_provider.messaging_service.stop()
    service_provider.messaging_service = None


@pytest.mark.asyncio
async def test_number_lca_nodes_in_cache(
    create_minimum_recipe_with_small_declaration_and_two_origins: MinimumRecipeWithTwoOrigins,
) -> None:
    """Test node service synchronization."""
    (
        postgres_db,
        service_provider,
        _,
        lca_process,
        _root_flow,
    ) = create_minimum_recipe_with_small_declaration_and_two_origins

    # we need to initialize the node_service again so that it fills the cache after the minimum_recipe was created:
    modeled_activity_nodes = await postgres_db.get_graph_mgr().get_nodes_by_type(
        node_types=[ModeledActivityNode.__name__]
    )

    flow_nodes = await postgres_db.get_graph_mgr().get_nodes_by_type(node_types=[FlowNode.__name__])
    food_processing_activity_nodes = await postgres_db.get_graph_mgr().get_nodes_by_type(
        node_types=[FoodProcessingActivityNode.__name__]
    )

    cached_nodes = [str(n.uid) for n in modeled_activity_nodes[:2]]
    await postgres_db.get_gfm_cache_mgr().set_cache_entry_by_gfm_name_and_key(
        gfm_name="InventoryConnectorGapFillingFactory",
        cache_key="pruned_nodes_to_cache",
        cache_data={"uids": cached_nodes},
        load_on_boot=False,
    )

    # Add the first two nodes in the gfm cache table.

    service_provider.node_service.load_every_inventory_nodes = False  # Check proper loading of LCA nodes.
    service_provider.node_service.cache.clear()
    await service_provider.node_service.ensure_cache_is_loaded()

    # check if we can get the lca_process out of the node_service1 cache:
    assert len(service_provider.node_service.cache) == 4, (
        "1 emissions from tractor_dummy_lca, "
        "1 emissions from seed_glossary_links, "
        "2 Nodes due to pruned_nodes_to_cache entry."
    )
    assert len(service_provider.node_service.cache._cached_inventory_nodes) == 2

    for idx, n in enumerate(flow_nodes):
        await service_provider.node_service.find_by_uid(n.uid)
        if idx > 1:
            cached_nodes.append(str(n.uid))

    await postgres_db.get_gfm_cache_mgr().set_cache_entry_by_gfm_name_and_key(
        gfm_name="InventoryConnectorGapFillingFactory",
        cache_key="pruned_nodes_to_cache",
        cache_data={"uids": cached_nodes},
        load_on_boot=False,
    )

    # Still only 4 nodes as the caches are not updated.
    assert len(service_provider.node_service.cache) == 4, (
        "1 emissions from tractor_dummy_lca, "
        "1 emissions from seed_glossary_links, "
        "2 Nodes due to pruned_nodes_to_cache entry."
    )

    await service_provider.node_service.update_and_broadcast_inventory_invalidation()

    # 14 nodes as the caches are updated.
    assert len(service_provider.node_service.cache) == 14, (
        "1 emissions from tractor_dummy_lca, "
        "1 emissions from seed_glossary_links, "
        "2 Nodes due to pruned_nodes_to_cache entry."
        "10 FlowNodes."
    )

    for n in flow_nodes:
        await service_provider.node_service.find_by_uid(n.uid)
        cached_nodes.append(str(n.uid))

    # 14 nodes as the two other flow nodes should not be inserted into the cache.
    assert len(service_provider.node_service.cache) == 14, (
        "1 emissions from tractor_dummy_lca, "
        "1 emissions from seed_glossary_links, "
        "2 Nodes due to pruned_nodes_to_cache entry."
        "10 FlowNodes."
    )

    assert len(service_provider.node_service.cache._cached_inventory_nodes) == 12

    service_provider.node_service.load_every_inventory_nodes = True  # Check proper loading of LCA nodes.

    for n in modeled_activity_nodes:
        await service_provider.node_service.upsert_node_by_uid(n)

    for n in flow_nodes[:2]:
        await service_provider.node_service.upsert_node_by_uid(n)

    assert len(service_provider.node_service.cache) == 33, (
        "There should be 33 nodes in the cache. "
        "6 brightway_process from tractor_dummy_lca, "
        "1 emissions from tractor_dummy_lca, "
        "13 brightway_process from seed_glossary_links, "
        "1 emissions from seed_glossary_links, "
        "12 FlowNodes."
    )

    for n in food_processing_activity_nodes:
        await service_provider.node_service.upsert_node_by_uid(n)

    assert len(service_provider.node_service.cache) == 33, (
        "There should be 33 nodes in the cache. "
        "6 brightway_process from tractor_dummy_lca, "
        "1 emissions from tractor_dummy_lca, "
        "13 brightway_process from seed_glossary_links, "
        "1 emissions from seed_glossary_links, "
        "12 FlowNodes."
    )


@pytest.mark.asyncio
async def test_node_service_cache_invalidation(
    create_minimum_recipe: Tuple[PostgresDb, ServiceProvider, Node, Node, Node],
    setup_messaging_service: MessagingService,  # noqa: ARG001
) -> Node:
    """Test node service synchronization."""
    postgres_db, service_provider, _, _, lca_process, _root_node = create_minimum_recipe
    _msg_service = setup_messaging_service

    # create 2 instances of NodeService to test if caches are synchronized:
    node_service1 = NodeService(service_provider)
    node_service2 = NodeService(service_provider)
    await node_service1.init()
    await node_service2.init()

    # check if we can get the lca_process out of the node_service1 cache:
    retrieved_node = await node_service1.find_by_uid(lca_process.uid)
    assert retrieved_node is not None
    assert retrieved_node.uid == lca_process.uid

    root_unit_term = service_provider.glossary_service.root_subterms.get("EOS_units")

    gram_term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
        "EOS_gram",
        str(root_unit_term.access_group_uid),
    )

    # Changed to product_name from is_dried, which will be eventually removed.
    retrieved_node.production_amount = QuantityProp(
        value=789.0, unit_term_uid=gram_term.uid, for_reference=ReferenceAmountEnum.self_reference
    )

    await node_service1.upsert_node_by_uid(retrieved_node)

    # now we check if the node_service2 cache is synchronized by retrieving that updated property from a node from it:
    received_prop_data = None
    for i in range(100):
        await asyncio.sleep(0.1)

        retrieved_node2 = await node_service2.find_by_uid(lca_process.uid)
        if retrieved_node2 is None:
            continue

        if retrieved_node2.production_amount:
            logger.debug(f"we received a node that contains the new property after {i/1000} seconds.")
            received_prop_data = retrieved_node2.production_amount
            break

    # check if we received the same property value that we had set using node_service1:
    assert received_prop_data is not None
    assert received_prop_data.get_unit_term() == gram_term
    assert is_close(received_prop_data.value, 789.0)
    assert received_prop_data.for_reference == ReferenceAmountEnum.self_reference


@pytest.mark.asyncio
async def test_upsert_recipe_change_detection(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
) -> None:
    """Test if we can simultaneously upsert many recipes."""
    (
        pg_db,
        service_provider,
        recipe,
        apple_juice_ingredient,
        orange_juice_ingredient,
        abstract_orange_juice_ingredient,
        piece_ingredient,
        root_flow,
    ) = create_minimum_recipe_for_unit_weight_gfm

    # for ingredient in (
    #     apple_juice_ingredient,
    #     orange_juice_ingredient,
    #     abstract_orange_juice_ingredient,
    #     piece_ingredient,
    # ):
    #     ingredient_copied = copy.deepcopy(ingredient)
    #     ingredient_copied.link_to_sub_node = LinkToUidProp(uid=uuid.uuid4())
    #     recipe.add_sub_node(ingredient_copied)

    node_service = NodeService(service_provider)
    await node_service.init()

    skipped_upsert = await node_service.upsert_recipe_and_insert_edges(recipe)
    assert not skipped_upsert

    # upserting the same recipe again should not change anything:
    skipped_upsert = await node_service.upsert_recipe_and_insert_edges(recipe)
    assert skipped_upsert

    # deleting the recipe and then upserting it again should do a full upsert to reset the deleted_at:
    await node_service.mark_recipe_as_deleted(recipe_node=recipe)
    skipped_upsert = await node_service.upsert_recipe_and_insert_edges(recipe)
    assert not skipped_upsert


@pytest.mark.asyncio
async def test_simultaneous_upsert_mark_as_delete_and_read_of_recipe(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
) -> None:
    """Test if we can simultaneously upsert many recipes."""
    (
        pg_db,
        service_provider,
        recipe,
        apple_juice_ingredient,
        orange_juice_ingredient,
        abstract_orange_juice_ingredient,
        piece_ingredient,
        root_flow,
    ) = create_minimum_recipe_for_unit_weight_gfm

    for ingredient in (
        apple_juice_ingredient,
        orange_juice_ingredient,
        abstract_orange_juice_ingredient,
        piece_ingredient,
    ):
        ingredient_copied = copy.deepcopy(ingredient)
        ingredient_copied.link_to_sub_node = LinkToUidProp(uid=uuid.uuid4())
        recipe.add_sub_node(ingredient_copied)

    node_service = NodeService(service_provider)
    await node_service.init()

    logger.info("start tasks to check if simultaneous upserting of recipes would result in deadlock.")
    tasks = []
    for _task_idx in range(300):
        # We add alternately 2 tasks that upsert and read the number of sub nodes.
        # These will then execute simultaneously. This tests for two different types of issues:
        # 1) If there are deadlocks between simultaneous writes.
        # 2) If a simultaneous write & read of the same recipe could result in an incomplete recipe when reading.
        tasks.append(asyncio.create_task(upsert_by_recipe_id(node_service, recipe, 4)))
        tasks.append(asyncio.create_task(node_service.mark_recipe_as_deleted(recipe_node=recipe)))
        tasks.append(asyncio.create_task(check_number_of_sub_nodes(node_service, recipe, 4)))

    await asyncio.gather(*tasks)
    logger.info("finished all tasks to reproduce deadlock.")


@pytest.mark.asyncio
async def test_simultaneous_upsert_and_read_of_recipe_different_info(
    create_minimum_recipe_for_unit_weight_gfm: MinimumRecipeForUnitWeightGFM,
) -> None:
    """Test if we can simultaneously upsert many recipes."""
    (
        pg_db,
        service_provider,
        recipe,
        apple_juice_ingredient,
        orange_juice_ingredient,
        abstract_orange_juice_ingredient,
        piece_ingredient,
        root_flow,
    ) = create_minimum_recipe_for_unit_weight_gfm

    recipe.uid = uuid.uuid4()
    recipe2 = copy.deepcopy(recipe)
    recipe3 = copy.deepcopy(recipe)
    recipe4 = copy.deepcopy(recipe)

    for i, ingredient in enumerate(
        (apple_juice_ingredient, orange_juice_ingredient, abstract_orange_juice_ingredient, piece_ingredient)
    ):
        ingredient_copied = copy.deepcopy(ingredient)
        ingredient_copied.link_to_sub_node = LinkToUidProp(uid=uuid.uuid4())
        ingredient_copied.declaration_index = IndexProp(value=i)
        ingredient_copied.uid = None
        recipe.add_sub_node(ingredient_copied)

        ingredient_copied = copy.deepcopy(ingredient)
        ingredient_copied.link_to_sub_node = LinkToUidProp(uid=uuid.uuid4())
        ingredient_copied.declaration_index = IndexProp(value=(i + 1) % 4)
        ingredient_copied.uid = None
        recipe2.add_sub_node(ingredient_copied)
        recipe2.uid = uuid.uuid4()

        ingredient_copied = copy.deepcopy(ingredient)
        ingredient_copied.link_to_sub_node = LinkToUidProp(uid=uuid.uuid4())
        ingredient_copied.declaration_index = IndexProp(value=(i + 2) % 4)
        ingredient_copied.uid = uuid.uuid4()  # This should also work since the uids do not overlap with other recipes.
        recipe3.add_sub_node(ingredient_copied)

        ingredient_copied = copy.deepcopy(ingredient_copied)
        ingredient_copied.link_to_sub_node = LinkToUidProp(uid=uuid.uuid4())
        ingredient_copied.declaration_index = IndexProp(value=(i + 3) % 4)
        recipe4.add_sub_node(ingredient_copied)
        recipe4.uid = uuid.uuid4()

    node_service = NodeService(service_provider)
    await node_service.init()

    logger.info("start tasks to check if simultaneous upserting of recipes would result in deadlock.")
    tasks = []
    for _task_idx in range(500):
        # We add alternately 2 tasks that upsert and read the number of sub nodes.
        # These will then execute simultaneously. This tests for two different types of issues:
        # 1) If there are deadlocks between simultaneous writes.
        # 2) If a simultaneous write & read of the same recipe could result in an incomplete recipe when reading.
        tasks.append(asyncio.create_task(upsert_by_recipe_id(node_service, recipe, 4)))
        tasks.append(asyncio.create_task(upsert_by_recipe_id(node_service, recipe2, 4)))
        tasks.append(asyncio.create_task(upsert_by_recipe_id(node_service, recipe3, 4)))

        # This leads to a deadlock, and we should disallow two different recipes having flows with the
        # same uid on API level.
        # tasks.append(asyncio.create_task(upsert_by_recipe_id(node_service, recipe4, 4)))

    await asyncio.gather(*tasks)
    logger.info("finished all tasks to reproduce deadlock.")

    await asyncio.gather(*tasks)
    logger.info("finished all tasks to reproduce deadlock.")


async def check_number_of_sub_nodes(node_service: NodeService, recipe: Node, expected_num: int) -> None:
    edges = await node_service.get_sub_graph_by_uid(recipe.uid, max_depth=2)
    assert len(edges) == expected_num, f"There should be {expected_num} edge for the recipe node itself."


async def upsert_by_recipe_id(node_service: NodeService, recipe: Node, expected_num: int) -> None:
    edges_to_delete, edges_to_insert, _, _, not_changed, _ = await node_service.upsert_by_recipe_id(recipe)
    if not not_changed:
        assert len(edges_to_insert) == expected_num


@pytest.mark.asyncio
async def test_binary_cache_available(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    node_service = service_provider.node_service

    assert node_service.cache.loaded, "Node service not initialized"

    # stash away caches that were originally loaded
    original_binary_cache = copy.deepcopy(node_service.cache._binary_cache)
    original_node_cache = copy.deepcopy(node_service.cache._node_cache)
    original_node_child_edge_cache = copy.deepcopy(node_service.cache._node_child_edge_cache)
    original_node_parent_edge_cache = copy.deepcopy(node_service.cache._node_parent_edge_cache)

    # re-initialize caches from binary version
    node_service.cache.clear()
    assert len(node_service.cache._node_cache) == 0
    assert len(node_service.cache._node_child_edge_cache) == 0
    assert len(node_service.cache._node_parent_edge_cache) == 0

    def get_cache_successful_mock(**kwargs: dict[str, Any]) -> None:
        return httpx.Response(status_code=200, content=original_binary_cache)

    with patch.object(httpx, "get", new=get_cache_successful_mock):
        node_service.cache._initialize_from_running_pods()

    node_cache_from_binary = copy.deepcopy(node_service.cache._node_cache)
    node_child_edge_cache_from_binary = copy.deepcopy(node_service.cache._node_child_edge_cache)
    node_parent_edge_cache_from_binary = copy.deepcopy(node_service.cache._node_parent_edge_cache)

    assert node_cache_from_binary == original_node_cache, "nodes from binary cache and DB do not match"
    assert (
        node_child_edge_cache_from_binary == original_node_child_edge_cache
    ), "child edges from binary cache and DB do not match"
    assert (
        node_parent_edge_cache_from_binary == original_node_parent_edge_cache
    ), "parent edges from binary cache and DB do not match"


@pytest.mark.asyncio
async def test_binary_cache_unavailable(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    node_service = service_provider.node_service

    assert node_service.cache.loaded, "Node service not initialized"

    def get_cache_mock(**kwargs: dict[str, Any]) -> None:
        return httpx.Response(status_code=408)

    with patch.object(httpx, "get", new=get_cache_mock):
        with pytest.raises(Exception):  # noqa: B017
            node_service.cache._initialize_from_running_pods()
