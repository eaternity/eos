"Amount estimator gap filling module tests."
import json
import os
import uuid
from typing import Any, NamedTuple, Optional, Tuple
from unittest import mock

import pytest
from gap_filling_modules.ingredicalc.helpers import (
    nutrients_dict_to_prop,
    transform_eurofir,
    transform_eurofir_to_qty_pkg,
)
from gap_filling_modules.ingredicalc.optimiser import run
from gap_filling_modules.ingredient_amount_estimator_gfm import (
    IngredientAmountEstimatorGapFillingFactory,
    IngredientAmountEstimatorGapFillingWorker,
)
from numpy.testing import assert_allclose
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.data_error import DataError, ErrorClassification
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.raw_nutrient_values_per_100g import RawNutrientValuesPer100g
from core.graph_manager.calc_graph import CalcGraph
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import prepend_flow_node
from database.postgres.pg_graph_mgr import PostgresGraphMgr
from database.postgres.pg_term_mgr import PgTermMgr
from database.postgres.postgres_db import PostgresDb
from database.postgres.settings import PgSettings

from .conftest import append_flow_node, is_close

logger = get_logger()

FRESH_CARROT_UPSCALE_RATIO = 1.0466820180029306
FRESH_POTATO_UPSCALE_RATIO = 1.0409618487482435

with open(
    os.path.join(os.path.dirname(__file__), "..", "..", "gap_filling_modules", "ingredicalc", "nutr_stats.json")
) as nutr_stats_file:
    nutr_stats = json.load(nutr_stats_file)


class CalculationFailed(Exception):
    def __init__(self, data_errors: list[DataError]):
        self.data_errors = data_errors


def find_missing_matching_errors(data_errors: list[DataError]) -> list[DataError]:
    return [error for error in data_errors if error.error_classification == ErrorClassification.missing_matching]


@pytest.mark.asyncio
async def test_ingredient_estimator_library(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Test the ingredient estimator; this is taken from the original __main__ code in optimizer.py."""
    postgres_db, service_provider = setup_services
    incoming_nutrients = {
        k: v
        for k, v in [
            ("energy_kcal", 0),
            ("energy_kjoule", 120),
            ("fat_gram", 40),
            ("saturated_fat_gram", None),
            ("carbohydrates_gram", None),
            ("sucrose_gram", None),
            ("protein_gram", None),
            ("sodium_chloride_gram", None),
            ("monounsaturated_fat_milligram", None),
            ("polyunsaturated_fat_milligram", None),
            ("cholesterol_milligram", None),
            ("fibers_gram", None),
            ("water_gram", None),
            ("vitamine_a1_microgram", None),
            ("vitamine_b1_microgram", None),
            ("vitamine_b2_microgram", None),
            ("vitamine_b6_microgram", None),
            ("vitamine_b12_microgram", None),
            ("vitamine_c_milligram", None),
            ("vitamine_d_microgram", None),
            ("vitamine_e_microgram", None),
            ("vitamine_h_microgram", None),
            ("vitamine_k_microgram", None),
            ("beta_carotene_milligram", None),
            ("niacin_milligram", None),
            ("pantohen_milligram", None),
            ("folic_acid_microgram", None),
            ("sodium_milligram", None),
            ("potassium_milligram", None),
            ("chlorine_milligram", None),
            ("calcium_milligram", None),
            ("magnesium_milligram", None),
            ("phosphorus_milligram", None),
            ("iron_milligram", None),
            ("zinc_microgram", None),
            ("copper_microgram", None),
            ("manganese_microgram", None),
            ("flouride_microgram", None),
            ("iodine_microgram", None),
            ("purine_milligram", None),
            ("uric_acid_milligram", None),
            ("alcohol_volume_percent", None),
        ]
    }
    flat_nutrients = {
        (
            (0,),
            "c3idotC4hBpPLbSfJnyRwGu7UXTx2q1V",
            "IngredientStr76108469444498f75371c-6394-11ea-8469-ae471177f174",
            0,
            "TERM_DOESNT_EXIST",
        ): {
            "energy_kcal": 0.1,
            "energy_kjoule": 200,
            "fat_gram": 0,
            "saturated_fat_gram": 0,
            "carbohydrates_gram": 0,
            "sucrose_gram": 0,
            "protein_gram": 0,
            "sodium_chloride_gram": None,
            "monounsaturated_fat_milligram": 0,
            "polyunsaturated_fat_milligram": 0,
            "cholesterol_milligram": 0,
            "fibers_gram": 0,
            "water_gram": 76.2,
            "vitamine_a1_microgram": 0,
            "vitamine_b1_microgram": 0,
            "vitamine_b2_microgram": 0,
            "vitamine_b6_microgram": 0,
            "vitamine_b12_microgram": 0,
            "vitamine_c_milligram": 0,
            "vitamine_d_microgram": 0,
            "vitamine_e_microgram": 0,
            "vitamine_h_microgram": 0,
            "vitamine_k_microgram": 0,
            "beta_carotene_milligram": 0,
            "niacin_milligram": 0,
            "pantohen_milligram": 0,
            "folic_acid_microgram": 0,
            "sodium_milligram": 0,
            "potassium_milligram": 0,
            "chlorine_milligram": 0,
            "calcium_milligram": 0,
            "magnesium_milligram": 0,
            "phosphorus_milligram": 0,
            "iron_milligram": 0,
            "zinc_microgram": 0,
            "copper_microgram": 0,
            "manganese_microgram": 0,
            "flouride_microgram": 0,
            "iodine_microgram": 0,
            "purine_milligram": 0,
            "uric_acid_milligram": 0,
            "alcohol_volume_percent": 0,
        },
        (
            (1,),
            "c3idotC4hBpPLbSfJnyRwGu7UXTx2q1V",
            "IngredientStr76108469444498f75371c-6394-11ea-8469-ae471177f174",
            1,
            "TERM_DOESNT_EXIST_2",
        ): {
            "energy_kcal": 0,
            "energy_kjoule": 0,
            "fat_gram": 100,
            "saturated_fat_gram": 0,
            "carbohydrates_gram": 0,
            "sucrose_gram": 0,
            "protein_gram": 0,
            "sodium_chloride_gram": None,
            "monounsaturated_fat_milligram": 0,
            "polyunsaturated_fat_milligram": 0,
            "cholesterol_milligram": 0,
            "fibers_gram": 0,
            "water_gram": 0,
            "vitamine_a1_microgram": 0,
            "vitamine_b1_microgram": 0,
            "vitamine_b2_microgram": 0,
            "vitamine_b6_microgram": 0,
            "vitamine_b12_microgram": 0,
            "vitamine_c_milligram": 0,
            "vitamine_d_microgram": 0,
            "vitamine_e_microgram": 0,
            "vitamine_h_microgram": 0,
            "vitamine_k_microgram": 0,
            "beta_carotene_milligram": 0,
            "niacin_milligram": 0,
            "pantohen_milligram": 0,
            "folic_acid_microgram": 0,
            "sodium_milligram": 0,
            "potassium_milligram": 0,
            "chlorine_milligram": 0,
            "calcium_milligram": 0,
            "magnesium_milligram": 0,
            "phosphorus_milligram": 0,
            "iron_milligram": 0,
            "zinc_microgram": 0,
            "copper_microgram": 0,
            "manganese_microgram": 0,
            "flouride_microgram": 0,
            "iodine_microgram": 0,
            "purine_milligram": 0,
            "uric_acid_milligram": 0,
            "alcohol_volume_percent": 0,
        },
    }
    solution, error_squares, _, _ = run(
        nutrients_dict_to_prop(incoming_nutrients),
        {key: nutrients_dict_to_prop(val) for key, val in flat_nutrients.items()},
        {},
    )
    assert_allclose(
        solution[
            (
                (0,),
                "c3idotC4hBpPLbSfJnyRwGu7UXTx2q1V",
                "IngredientStr76108469444498f75371c-6394-11ea-8469-ae471177f174",
                0,
                "TERM_DOESNT_EXIST",
            )
        ],
        60,
        rtol=1e-5,
    )


@pytest.mark.asyncio
async def test_load_term_nutrients(load_seeded_nutrients: dict[str, dict[str, float]]) -> None:
    """Test that the nutrients for a term are loaded correctly from the database."""
    carrot_nutrients_kale = load_seeded_nutrients["carrot"]
    assert carrot_nutrients_kale["fat_gram"] == 0.3
    assert carrot_nutrients_kale["energy_kcal"] == 37.762, "energy gets converted to kcal"


@pytest.mark.asyncio
async def test_ingredient_estimator_with_terms(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    """Take 2 sample Terms (carrot, potato) and estimate the nutrients for a recipe with 2 different ratios."""
    postgres_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    term_mgr: PgTermMgr = postgres_db.get_term_mgr()

    # get terms and their corresponding nutrients (these were seeded in pg_term_mgr.py)

    carrot = await term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)
    carrot_nutrients_term = await service_provider.glossary_link_service.load_term_nutrients([carrot])
    carrot_nutrients: list[dict[str, Any]] = carrot_nutrients_term.data["nutr-vals"]
    carrot_id = (
        (0,),
        "",
        "A1791",
        False,
        carrot_nutrients_term.xid,
    )

    potato = await term_mgr.get_term_by_xid_and_access_group_uid("A00ZT", foodex2_term_group_uid)
    potato_nutrients_term = await service_provider.glossary_link_service.load_term_nutrients([potato])
    potato_nutrients = potato_nutrients_term.data["nutr-vals"]
    potato_id = (
        (1,),
        "",
        "A00ZT",
        False,
        potato_nutrients_term.xid,
    )

    flat_nutrients = {carrot_id: carrot_nutrients, potato_id: potato_nutrients}

    # create a mix recipe of the 2 ingredients with a 50/50 ratio
    carrot_nutrients_kale = transform_eurofir(carrot_nutrients)
    potato_nutrients_kale = transform_eurofir(potato_nutrients)
    incoming_nutrients = {}
    for nutrient in carrot_nutrients_kale.keys():
        incoming_nutrients[nutrient] = 0.5 * carrot_nutrients_kale[nutrient] + 0.5 * potato_nutrients_kale[nutrient]

    solution, error_squares, _, _ = run(
        nutrients_dict_to_prop(incoming_nutrients),
        {key: transform_eurofir_to_qty_pkg(val) for key, val in flat_nutrients.items()},
        {},
    )
    assert_allclose(solution[carrot_id], 50, rtol=1e-5, err_msg="carrot ratio should be 50%")
    assert_allclose(solution[potato_id], 50, rtol=1e-5, err_msg="potato ratio should be 50%")

    # create a mix recipe of the 2 ingredients with an 80/20 ratio
    incoming_nutrients = {}
    for nutrient in carrot_nutrients_kale.keys():
        incoming_nutrients[nutrient] = 0.8 * carrot_nutrients_kale[nutrient] + 0.2 * potato_nutrients_kale[nutrient]

    solution, error_squares, _, _ = run(
        nutrients_dict_to_prop(incoming_nutrients),
        {key: transform_eurofir_to_qty_pkg(val) for key, val in flat_nutrients.items()},
        {},
    )
    assert_allclose(solution[carrot_id], 80, rtol=1e-5, err_msg="carrot ratio should be 80%")
    assert_allclose(solution[potato_id], 20, rtol=1e-5, err_msg="potato ratio should be 20%")


def nutrients_mixer(ingredients_data: tuple, ratios: tuple) -> dict:
    """Auxiliary method for mixing nutrition data of selected ingredients, according to provided ratios."""
    result_dict = {}

    # all ingredients have the same nutrients listed,
    # so we can grab them from the very first ingredient
    for nutrient in ingredients_data[0].keys():
        total_nutrient_value = 0

        for ingredient, ratio in zip(ingredients_data, ratios):
            total_nutrient_value += ingredient.get(nutrient, 0.0) * ratio

        result_dict[nutrient] = total_nutrient_value

    return result_dict


def nutrients_upscaler(ingredients_data: dict, ratio: float) -> dict:
    """Auxiliary method for upscaling nutrition data according to the ratio."""
    result_dict = {}
    # all ingredients have the same nutrients listed,
    # so we can grab them from the very first ingredient
    for nutrient, value in ingredients_data.items():
        result_dict[nutrient] = value * ratio
    return result_dict


class NutritionalTuple(NamedTuple):
    nutrition: dict
    ratio: float


def recursive_nutrients_mixer(nutrient_values: list[NutritionalTuple] | list[list]) -> Tuple[dict, float]:
    total_nutrients = {key: 0.0 for key in nutrient_values[0].nutrition}
    normalization_factor = 0.0
    for nutr_val in nutrient_values:
        if isinstance(nutr_val, list):
            nested_total, normalization_factor = recursive_nutrients_mixer(nutr_val)
            total_nutrients = nutrients_mixer((total_nutrients, nested_total), (1.0, 1.0))
            normalization_factor += normalization_factor
        else:
            total_nutrients = nutrients_mixer((total_nutrients, nutr_val.nutrition), (1.0, nutr_val.ratio))
            normalization_factor += nutr_val.ratio

    return total_nutrients, normalization_factor


def nutrients_mixer_from_list(nutrient_values: list[NutritionalTuple] | list[list]) -> dict:
    total_nutrients, normalization_factor = recursive_nutrients_mixer(nutrient_values)
    # Delete fibers information from nutrient declaration to:
    # 1. Keep old test benchmarks.
    # 2. It is not required by law to provide fiber information on food items.
    return {key: val / normalization_factor for key, val in total_nutrients.items() if key != "fibers_gram"}


async def _prep_recipe_with_fresh_ingredients_from_declaration_dict(
    graph_mgr: PostgresGraphMgr,
    ingredient_declaration: list[str] | list[list],
    nutrient_values: list[NutritionalTuple] | list[list],
) -> Tuple[Node, Node]:
    "Prepare recipe with fresh ingredients from declaration dict."
    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            production_amount={"value": 100.0, "unit": "gram"},
            titles=[
                {
                    "language": "en",
                    "value": f"A sample recipe with {len(ingredient_declaration)} ingredients",
                },
            ],
        )
    )

    root_flow = await prepend_flow_node(
        graph_mgr, recipe.uid, nutrient_values=nutrients_mixer_from_list(nutrient_values)
    )

    async def add_edge(ingredients: list[str] | list[list], nutrition_values: list, recipe: Optional[int]) -> None:
        "Allow building nested tree from nested lists."
        for idx, (ing_str, nutr_val) in enumerate(zip(ingredients, nutrition_values)):
            if isinstance(ing_str, list):
                assert isinstance(nutr_val, list)
                existing_children = await graph_mgr.get_sub_graph_by_uid(recipe.uid, max_depth=1)
                sub_flow = await graph_mgr.upsert_node_by_uid(
                    FoodProductFlowNode(
                        raw_production={"value": "greenhouse", "language": "en"},
                        raw_transport="air",
                        flow_location="spain",
                        product_name=[{"language": "de", "value": ",".join(ing_str)}],
                        type="brightway_process",
                        processing="raw",
                        raw_conservation={"value": "fresh", "language": "en"},
                        packaging="plastic",
                        nutrient_values=nutrients_mixer_from_list(nutr_val),
                        declaration_index={"value": len(existing_children)},
                    )
                )
                sub_recipe = await graph_mgr.upsert_node_by_uid(
                    FoodProcessingActivityNode(
                        # production_amount={"value": 100.0, "unit": "gram"},
                        titles=[
                            {
                                "language": "en",
                                "value": f"A sample recipe with {len(ingredients)} ingredients",
                            },
                        ],
                    )
                )
                await graph_mgr.add_edge(
                    Edge(
                        parent_uid=recipe.uid,
                        child_uid=sub_flow.uid,
                        edge_type=EdgeTypeEnum.ingredient,
                    )
                )
                await graph_mgr.add_edge(
                    Edge(
                        parent_uid=sub_flow.uid,
                        child_uid=sub_recipe.uid,
                        edge_type=EdgeTypeEnum.ingredient,
                    )
                )
                await add_edge(ing_str, nutr_val, sub_recipe)
                continue
            ingredient = await graph_mgr.upsert_node_by_uid(
                FoodProductFlowNode(
                    product_name=[{"language": "de", "value": ing_str}],
                    raw_conservation={
                        "value": "fresh, canned",
                        "language": "en",
                    },  # canned just exists to prevent cooling the transport
                    flow_location="spain",
                    declaration_index={"value": idx},
                )
            )
            await graph_mgr.add_edge(
                Edge(
                    parent_uid=recipe.uid,
                    child_uid=ingredient.uid,
                    edge_type=EdgeTypeEnum.ingredient,
                )
            )

    await add_edge(ingredient_declaration, nutrient_values, recipe)
    return recipe, root_flow


async def _prep_recipe_from_declaration(
    graph_mgr: PostgresGraphMgr, ingredient_declaration: str, nutrient_values: dict[str, float]
) -> Tuple[Node, Node]:
    "Prepare recipe from declaration."
    # Delete fibers information from nutrient declaration to:
    # 1. Keep old test benchmarks.
    # 2. It is not required by law to provide fiber information on food items.
    if "fibers_gram" in nutrient_values:
        del nutrient_values["fibers_gram"]

    amount = {"value": 100.0, "unit": "gram"}
    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            production_amount=amount,
            titles=[
                {
                    "language": "en",
                    "value": "A sample recipe with an ingredient-declaration, to test the ingredient estimator",
                },
            ],
        )
    )
    root_flow = await prepend_flow_node(graph_mgr, recipe.uid)
    ingredients_declaration = [
        {
            "language": "de",
            "value": ingredient_declaration,
        },
    ]
    _ = await append_flow_node(
        graph_mgr=graph_mgr,
        recipe_uid=recipe.uid,
        ingredients_declaration=ingredients_declaration,
        nutrient_values=nutrient_values,
        amount_in_original_source_unit=amount,
    )
    return recipe, root_flow


async def _run_calculation(
    services: Tuple[PostgresDb, ServiceProvider],
    ingredient_declaration: str | list[str],
    nutrient_values: dict[str, float] | list,
    expected_amounts: Optional[dict[str, float]] = None,
    msg: str = "",
    max_allowed_residual: float | None = 1e-5,
    return_early: bool = False,
) -> Node | None:
    postgres_db, service_provider = services
    glossary_service = service_provider.glossary_service
    graph_mgr = postgres_db.get_graph_mgr()

    if isinstance(ingredient_declaration, list):
        assert isinstance(nutrient_values, list)
        recipe, root_flow = await _prep_recipe_with_fresh_ingredients_from_declaration_dict(
            graph_mgr, ingredient_declaration, nutrient_values
        )
    else:
        recipe, root_flow = await _prep_recipe_from_declaration(graph_mgr, ingredient_declaration, nutrient_values)

    # Add CO2 emission node:
    co2_emission = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
            name="Carbon dioxide, fossil",
            type="emission",
            categories=["air"],
        )
    )
    co2_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
        co2_emission.uid,
    )
    await service_provider.product_service.bulk_insert_xid_uid_mappings([co2_mapping])

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(
        service_provider,
        glossary_service,
        node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    if len(calculation.data_errors) > 0 and (
        not calculation.success or len(find_missing_matching_errors(calculation.data_errors)) > 0
    ):
        raise CalculationFailed(calculation.data_errors)

    assert len(calculation.mutations) > 5

    if return_early:
        return root_node

    matches: dict[str, float | None] = {k: None for k in expected_amounts}

    def find_my_products(node: Node) -> None:
        def old_node_representation(node: Node) -> str:
            name = node.name()
            if isinstance(node, FoodProductFlowNode):
                name = name.removesuffix(" (dried)")
                subdivision_str = " (subdivision)" if node.is_subdivision else ""
                dried_str = " (dried)" if node.is_dried() else ""
                normal_product_str = (
                    " (normal product required for dried product)" if node.nutrient_upscale_ratio else ""
                )
                return f"{name}{subdivision_str}{dried_str}{normal_product_str}"
            else:
                return name

        old_name_repr = old_node_representation(node)
        for name, _ in matches.items():
            if name == old_name_repr:
                if "normal product required for dried product" in old_name_repr:
                    amount = node.amount.value
                    if not matches.get(name):
                        matches[name] = amount
                else:
                    if isinstance(node, FlowNode):
                        qty = node.amount.amount_for_root_node()
                    else:
                        qty = node.get_parent_nodes()[0].amount.amount_for_root_node()
                    unit = qty.get_unit_term()
                    amount = qty.value * unit.data["mass-in-g"]

                    matches[name] = amount
        for child in node.get_sub_nodes():
            find_my_products(child)  # recursion

    if isinstance(ingredient_declaration, list):
        mixing_activity = root_node.get_sub_nodes()[0]
    else:
        # ingredient-splitter adds an extra layer in between
        mixing_activity = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
    find_my_products(mixing_activity)
    logger.info("estimated amounts: ", matches=matches)

    def assert_close_or_both_none(actual: Optional[list], expected: Optional[list]) -> None:
        if actual is None and expected is None:
            return  # all good
        elif actual is None or expected is None:
            raise AssertionError(f"one of the values is None, actual={actual}, expected={expected}, msg={msg}")
        else:
            assert_allclose(
                actual,
                expected,
                atol=0.1,
                err_msg=f"too much difference btw actual={actual} and expected={expected}, msg={msg}",
            )

    for key, val in expected_amounts.items():
        assert_close_or_both_none(matches[key], val)

    if max_allowed_residual is not None:
        estimated_nutrients = (
            mixing_activity.ingredient_amount_estimator_estimated_nutrients.amount_for_100g().quantities
        )
        nutrient_values_quantities = nutrients_dict_to_prop(nutrient_values).quantities

        for key in set(nutrient_values_quantities.keys()).intersection(estimated_nutrients.keys()):
            nutr_name = glossary_service.terms_by_uid[key].name.lower()
            nutr_unit = glossary_service.terms_by_uid[nutrient_values_quantities[key].unit_term_uid].name.lower()
            if nutr_unit == "kilocalorie":
                nutr_unit = "kcal"
            if nutr_name in ("chlorine", "sodium", "sodium_chloride") and nutr_unit == "gram":
                nutr_unit = "milligram"

            if glossary_service.terms_by_uid[key].name.lower() == "water":
                continue  # water is allowed to evaporate and is not optimized itself

            if nutrient_values_quantities[key] is None or not nutrient_values_quantities[key].value > 0:
                continue  # these are not optimized, so ignore them

            residual = abs(
                (estimated_nutrients[key].value - nutrient_values_quantities[key].value)
                / nutr_stats.get(f"{nutr_name}_{nutr_unit}").get("STD")
            )
            logger.info("residual: ", key=key, rel_diff=residual)
            assert residual <= max_allowed_residual, f"residual too high: {residual}"


@pytest.mark.asyncio
async def test_ingredient_estimator_with_one_fresh_ingredient(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    upscaled_fresh_carrot = nutrients_upscaler(carrot_nutrients, FRESH_CARROT_UPSCALE_RATIO)
    mixed_nutrients = [
        NutritionalTuple(
            nutrition=nutrients_mixer((carrot_nutrients, upscaled_fresh_carrot), (0.666, 0.334)), ratio=1.0
        )
    ]
    await _run_calculation(
        setup_services,
        ["Karotten"],
        mixed_nutrients,
        expected_amounts={
            "Karotten": 100,
            "Karotten (subdivision)": 66.6,
            "Karotten (subdivision) (dried)": 33.4,
            "Karotten (subdivision) (normal product required for dried product)": FRESH_CARROT_UPSCALE_RATIO,
            "Kartoffeln": None,
            "Zwiebeln": None,
            "Tomaten": None,
        },
        msg="One fresh Karotten ingredient. Should be 66.6% non dried 33.4% dried (5% dried)",
        max_allowed_residual=None,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_two_fresh_ingredient(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    upscaled_carrot = nutrients_upscaler(carrot_nutrients, FRESH_CARROT_UPSCALE_RATIO)
    upscaled_potato = nutrients_upscaler(potato_nutrients, FRESH_POTATO_UPSCALE_RATIO)
    mix_50_50_carrot_potato = [
        NutritionalTuple(nutrition=upscaled_potato, ratio=0.666),
        NutritionalTuple(nutrition=upscaled_carrot, ratio=0.334),
    ]
    await _run_calculation(
        setup_services,
        ["Karotten", "Kartoffeln"],
        mix_50_50_carrot_potato,
        expected_amounts={
            "Karotten": 50.0,
            "Karotten (subdivision)": 0.0,
            "Karotten (subdivision) (dried)": 50.0,
            "Karotten (subdivision) (normal product required for dried product)": FRESH_CARROT_UPSCALE_RATIO,
            "Kartoffeln": 50.0,
            "Kartoffeln (subdivision)": 0.0,
            "Kartoffeln (subdivision) (dried)": 50.0,
            "Kartoffeln (subdivision) (normal product required for dried product)": FRESH_POTATO_UPSCALE_RATIO,
            "Zwiebeln": None,
            "Tomaten": None,
        },
        msg="Fresh Karotten and fresh Kartoffeln ingredients. Should be 30% Carrots and 70% Potato",
        max_allowed_residual=None,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_one_ingredient(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mixed_nutrients = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    await _run_calculation(
        setup_services,
        "Karotten",
        mixed_nutrients,
        expected_amounts={
            "Karotten": 100,
            "Karotten (subdivision)": 97.84,
            "Karotten (subdivision) (dried)": 2.157,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": None,
            "Zwiebeln": None,
            "Tomaten": None,
        },
        msg="One Karotten ingredient. Should be 100% Karotten, rest None",
        max_allowed_residual=0.3,  # perfect fit not possible, because we give non-matching nutrients
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_one_ingredient2(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    onion_nutrients = load_seeded_nutrients["onion"]
    await _run_calculation(
        setup_services,
        "Zwiebeln",
        onion_nutrients,
        expected_amounts={
            "Zwiebeln": 100,
            "Zwiebeln (subdivision)": 100,
            "Zwiebeln (subdivision) (dried)": 0,
            "Zwiebeln (subdivision) (normal product required for dried product)": 5.025125628140706,
            "Karotten": None,
            "Kartoffeln": None,
            "Tomaten": None,
        },
        msg="One Onion ingredient. Should be 100% Onion, rest None",
        max_allowed_residual=1e-5,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_two_ingredients(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mix_50_50_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    await _run_calculation(
        setup_services,
        "Karotten, Kartoffeln",
        mix_50_50_carrot_potato,
        expected_amounts={
            "Karotten": 50.0,
            "Karotten (subdivision)": 50.0,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 50.0,
            "Kartoffeln (subdivision)": 50.0,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": None,
            "Tomaten": None,
        },
        max_allowed_residual=1e-5,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_fixed_percentages(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mix_50_50_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    await _run_calculation(
        setup_services,
        "60% Karotten, Kartoffeln",
        mix_50_50_carrot_potato,
        expected_amounts={
            "Karotten": 60.0,
            "Karotten (subdivision)": 60.0,
            "Karotten (subdivision) (dried)": 0.0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 40.0,
            "Kartoffeln (subdivision)": 37.808,
            "Kartoffeln (subdivision) (dried)": 2.191,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": None,
            "Tomaten": None,
        },
        msg="A sample recipe with fixed percentages. We expect 60% carrot & 40% potato.",
        max_allowed_residual=0.05,  # perfect fit not possible, because we give non-matching nutrients
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_fixed_percentages2(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mix_50_50_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    await _run_calculation(
        setup_services,
        "60% Karotten, 40% Kartoffeln",
        mix_50_50_carrot_potato,
        expected_amounts={
            "Karotten": 60.0,
            "Karotten (subdivision)": 60.0,
            "Karotten (subdivision) (dried)": 0.0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 40.0,
            "Kartoffeln (subdivision)": 37.808,
            "Kartoffeln (subdivision) (dried)": 2.191,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": None,
            "Tomaten": None,
        },
        max_allowed_residual=0.05,  # perfect fit not possible, because we give non-matching nutrients
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_four_ingredients(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """A recipe with more complex ingredient structure, to test the ingredient estimator.

    TREE-VIEW:
    A sample recipe with 4 ingredients, to test the ingredient estimator  [recipe]
      None  [food_product_flow]
        MainIngredientOne  [food_product]
          None  [food_product_flow]
            Karotten  [food_product]
                [food_product_to_inventory_link]
                market for carrot (w/o transport)  [brightway_process]
          None  [food_product_flow]
            Kartoffeln  [food_product]
      None  [food_product_flow]
        MainIngredientTwo  [food_product]
          None  [food_product_flow]
            Zwiebeln  [food_product]
                [food_product_to_inventory_link]
                market for onion (w/o transport)  [brightway_process]
                  None (-0.01 None)  [intermediary_flow]
                    Methane, fossil  [emission]
          None  [food_product_flow]
            Tomaten  [food_product]
                [food_product_to_inventory_link]
                market for tomato (w/o transport)  [brightway_process]


    flat_nutrients = {
        ((0,), '_', UUID(...), 1): None,
        ((0, 0), '_', UUID(...), 1): {'fat_gram': 0.3, 'energy_kcal': 158.0, ...
        ((0, 1), '_', UUID(...), 1): {'fat_gram': 0.1, 'energy_kcal': 76.48, ...
        ((1,), '_', UUID(...), 1): None,
        ((1, 0), '_', UUID(...), 1): {'fat_gram': 0.2, 'energy_kcal': 38.957, ...
        ((1, 1), '_', UUID(...), 1): {'fat_gram': 0.3, 'energy_kcal': 21.271, ...
    }
    """
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mix_50_50_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    await _run_calculation(
        setup_services,
        "MainIngrOne (Karotten, Kartoffeln), MainIngrTwo (Zwiebeln, Tomaten)",
        mix_50_50_carrot_potato,
        expected_amounts={
            "Karotten": 50.0,
            "Karotten (subdivision)": 50.0,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 50.0,
            "Kartoffeln (subdivision)": 50.0,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": 0.0,
            "Zwiebeln (subdivision)": 0.0,
            "Zwiebeln (subdivision) (dried)": 0,
            "Zwiebeln (subdivision) (normal product required for dried product)": 5.025125628140706,
            "Tomaten": 0.0,
            "Tomaten (subdivision)": 0.0,
            "Tomaten (subdivision) (dried)": 0,
            "Tomaten (subdivision) (normal product required for dried product)": 6.418485237483955,
        },
        max_allowed_residual=1e-5,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_four_ingredients2(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    onion_nutrients = load_seeded_nutrients["onion"]
    await _run_calculation(
        setup_services,
        "MainIngrOne (Karotten, Kartoffeln), MainIngrTwo (Zwiebeln, Tomaten)",
        onion_nutrients,
        expected_amounts={
            "Karotten": 48.642,
            "Karotten (subdivision)": 48.642,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 1.357,
            "Kartoffeln (subdivision)": 1.357,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": 50.0,
            "Zwiebeln (subdivision)": 48.767,
            "Zwiebeln (subdivision) (dried)": 1.232,
            "Zwiebeln (subdivision) (normal product required for dried product)": 5.025125628140706,
            "Tomaten": 0,
            "Tomaten (subdivision)": 0,
            "Tomaten (subdivision) (dried)": 0,
            "Tomaten (subdivision) (normal product required for dried product)": 6.418485237483955,
        },
        max_allowed_residual=0.05,  # perfect fit not possible, because we give non-matching nutrients
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_long_and_deep_ingredients(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """An example of a very weird edge case.

    It illustrates what happens if we have nutrition data
    only for an ingredient in an unusual (from the perspective of giving nutrition data) position.
    """
    onion_nutrients = load_seeded_nutrients["onion"]
    try:
        await _run_calculation(
            setup_services,
            "MainIngrOne (Bli, Bla, Karotten, Kartoffeln), "
            "MainIngrTwo (Zwiebeln, Tomaten, Blu, Blo), "
            "Another Ingredient(Bloh, Bluh)",
            onion_nutrients,
            expected_amounts={},
        )
    except CalculationFailed as e:
        assert len(find_missing_matching_errors(e.data_errors)) == 6, "Should have missing matching errors"


@pytest.mark.asyncio
async def test_ingredient_estimator_with_four_ingredients3(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    onion_nutrients = load_seeded_nutrients["onion"]
    await _run_calculation(
        setup_services,
        "MainIngrOne (Zwiebeln, Kartoffeln), MainIngrTwo (Karotten, Tomaten)",
        onion_nutrients,
        expected_amounts={
            "Zwiebeln": 100,
            "Zwiebeln (subdivision)": 100,
            "Zwiebeln (subdivision) (dried)": 0,
            "Zwiebeln (subdivision) (normal product required for dried product)": 5.025125628140706,
            "Kartoffeln": 0,
            "Kartoffeln (subdivision)": 0,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Karotten": 0,
            "Karotten (subdivision)": 0,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Tomaten": 0,
            "Tomaten (subdivision)": 0,
            "Tomaten (subdivision) (dried)": 0,
            "Tomaten (subdivision) (normal product required for dried product)": 6.418485237483955,
        },
        max_allowed_residual=1e-5,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_four_ingredients4(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """When GFM has nutrition data only of an ingredient that lies in an unusual position in the declaration.

    Example of weird behaviour in this case:
    Possibly, it also demonstrates an example of strange percentage distribution,
    when the GFM is forced to give potatoes at least equal weight (compared to onion) due to order constraint,
    so carrot and tomato have to be scaled up in order to even out potato weight (energy value).
    """
    onion_nutrients = load_seeded_nutrients["onion"]
    await _run_calculation(
        setup_services,
        "MainIngrOne (Kartoffeln, Zwiebeln), MainIngrTwo (Karotten, Tomaten)",
        onion_nutrients,
        expected_amounts={
            "Karotten": 25.0,
            "Karotten (subdivision)": 25.0,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 25.0,
            "Kartoffeln (subdivision)": 25.0,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": 25.0,
            "Zwiebeln (subdivision)": 21.776,
            "Zwiebeln (subdivision) (dried)": 3.223,
            "Zwiebeln (subdivision) (normal product required for dried product)": 5.025125628140706,
            "Tomaten": 25.0,
            "Tomaten (subdivision)": 25.0,
            "Tomaten (subdivision) (dried)": 0,
            "Tomaten (subdivision) (normal product required for dried product)": 6.418485237483955,
        },
        max_allowed_residual=0.2,  # perfect fit not possible, because we give non-matching nutrients
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_four_ingredients5(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    onion_nutrients = load_seeded_nutrients["onion"]
    tomato_nutrients = load_seeded_nutrients["tomato"]
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mixed_nutrients = nutrients_mixer(
        (carrot_nutrients, potato_nutrients, onion_nutrients, tomato_nutrients), (0.25, 0.25, 0.25, 0.25)
    )

    await _run_calculation(
        setup_services,
        "MainIngrOne (Karotten, Kartoffeln), MainIngrTwo (Zwiebeln, Tomaten)",
        mixed_nutrients,
        expected_amounts={
            "Karotten": 25.0,
            "Karotten (subdivision)": 25.0,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 25.0,
            "Kartoffeln (subdivision)": 25.0,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": 25.0,
            "Zwiebeln (subdivision)": 25.0,
            "Zwiebeln (subdivision) (dried)": 0,
            "Zwiebeln (subdivision) (normal product required for dried product)": 5.025125628140706,
            "Tomaten": 25.0,
            "Tomaten (subdivision)": 25.0,
            "Tomaten (subdivision) (dried)": 0,
            "Tomaten (subdivision) (normal product required for dried product)": 6.418485237483955,
        },
        max_allowed_residual=1e-5,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_four_ingredients6(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    onion_nutrients = load_seeded_nutrients["onion"]
    tomato_nutrients = load_seeded_nutrients["tomato"]
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mixed_nutrients = nutrients_mixer(
        (carrot_nutrients, potato_nutrients, onion_nutrients, tomato_nutrients), (0.25, 0.25, 0.4, 0.1)
    )

    await _run_calculation(
        setup_services,
        "MainIngrOne (Karotten, Kartoffeln), MainIngrTwo (Zwiebeln, Tomaten)",
        mixed_nutrients,
        expected_amounts={
            "Karotten": 25.0,
            "Karotten (subdivision)": 25.0,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 25.0,
            "Kartoffeln (subdivision)": 25.0,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": 40.0,
            "Zwiebeln (subdivision)": 40.0,
            "Zwiebeln (subdivision) (dried)": 0,
            "Zwiebeln (subdivision) (normal product required for dried product)": 5.025125628140706,
            "Tomaten": 10.0,
            "Tomaten (subdivision)": 10.0,
            "Tomaten (subdivision) (dried)": 0,
            "Tomaten (subdivision) (normal product required for dried product)": 6.418485237483955,
        },
        max_allowed_residual=1e-5,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_four_ingredients7(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    onion_nutrients = load_seeded_nutrients["onion"]
    tomato_nutrients = load_seeded_nutrients["tomato"]
    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mixed_nutrients = nutrients_mixer(
        (carrot_nutrients, potato_nutrients, onion_nutrients, tomato_nutrients), (0.25, 0.25, 0.4, 0.1)
    )

    await _run_calculation(
        setup_services,
        "MainIngrOne (Karotten, Kartoffeln), MainIngrTwo (80% Zwiebeln, 20% Tomaten)",
        mixed_nutrients,
        expected_amounts={
            "Karotten": 25.0,
            "Karotten (subdivision)": 25.0,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 25.0,
            "Kartoffeln (subdivision)": 25.0,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": 40.0,
            "Zwiebeln (subdivision)": 40.0,
            "Zwiebeln (subdivision) (dried)": 0.0,
            "Zwiebeln (subdivision) (normal product required for dried product)": 5.025125628140706,
            "Tomaten": 10.0,
            "Tomaten (subdivision)": 10.0,
            "Tomaten (subdivision) (dried)": 0.0,
            "Tomaten (subdivision) (normal product required for dried product)": 6.418485237483955,
        },
        max_allowed_residual=1e-5,
    )


def _run_gfm(
    node: Node,
    postgres_db: PostgresDb,
    service_provider: ServiceProvider,
    nutrient_values: Optional[RawNutrientValuesPer100g],
) -> IngredientAmountEstimatorGapFillingWorker:
    # create calculation graph

    root_flow = FoodProductFlowNode(uid=uuid.uuid4(), nutrient_values=nutrient_values)
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(node)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(parent_node_uid=root_flow.uid, child_node_uid=node.uid)

    # running the GFM's `should_be_scheduled` method
    gap_filling_factory = IngredientAmountEstimatorGapFillingFactory(postgres_db, service_provider)
    return gap_filling_factory.spawn_worker(node)


@pytest.mark.asyncio
async def test_ingredient_amount_estimator_gfm_with_no_raw_input(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    recipe = FoodProcessingActivityNode(uid=uuid.uuid4())

    gap_filling_worker = _run_gfm(recipe, postgres_db, service_provider, None)
    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_ingredient_amount_estimator_gfm_with_empty_ingredients_declaration(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    "If an empty ingredients declaration does not cause this GFM to be scheduled."
    postgres_db, service_provider = setup_services

    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mixed_nutrients = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    recipe = await postgres_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            ingredients_declaration=None,
            titles=[{"language": "en", "value": "foo"}],
        )
    )

    gap_filling_worker = _run_gfm(recipe, postgres_db, service_provider, mixed_nutrients)
    assert (
        gap_filling_worker.should_be_scheduled()
    ), "GFM has not been scheduled to run, but it should be, because there are nutrient_values"


@pytest.mark.asyncio
async def test_ingredient_amount_estimator_gfm_with_no_ingredients_declaration(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    "If absence of ingredients declaration does not cause this GFM to be scheduled."
    postgres_db, service_provider = setup_services

    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mixed_nutrients = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    recipe = await postgres_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            titles=[{"language": "en", "value": "foo"}],
        )
    )

    gap_filling_worker = _run_gfm(recipe, postgres_db, service_provider, mixed_nutrients)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should be."


@pytest.mark.asyncio
async def test_ingredient_amount_estimator_gfm_with_empty_nutrient_values(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    "If empty value of `nutrient_values` field does not cause this GFM to be scheduled."
    postgres_db, service_provider = setup_services

    recipe = await postgres_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            ingredients_declaration=[
                {
                    "language": "de",
                    "value": "Karotten, Kartoffeln",
                },
            ],
            titles=[{"language": "en", "value": "foo"}],
        )
    )

    gap_filling_worker = _run_gfm(recipe, postgres_db, service_provider, {})
    assert (
        not gap_filling_worker.should_be_scheduled()
    ), "GFM has been scheduled to run, but it should not, because the nutrient_values are empty."


@pytest.mark.asyncio
async def test_ingredient_amount_estimator_gfm_filter(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """Test to see if nutrient value filter works."""
    # Verified that this test fails without the filter on.
    postgres_db, service_provider = setup_services

    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    mixed_nutrients = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    mixed_nutrients["vitamine_a1_microgram"] = 100
    mixed_nutrients["flouride_microgram"] = 100
    await _run_calculation(
        setup_services,
        "Karotten, Kartoffeln",
        mixed_nutrients,
        expected_amounts={
            "Karotten": 50.0,
            "Karotten (subdivision)": 50.0,
            "Karotten (subdivision) (dried)": 0,
            "Karotten (subdivision) (normal product required for dried product)": 5.070993914807303,
            "Kartoffeln": 50.0,
            "Kartoffeln (subdivision)": 50.0,
            "Kartoffeln (subdivision) (dried)": 0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 3.4281796366129583,
            "Zwiebeln": None,
            "Tomaten": None,
        },
        max_allowed_residual=1e-5,
    )


@pytest.mark.asyncio
async def test_ingredient_amount_estimator_gfm_with_no_nutrient_values(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    "If absence of nutrient values does not cause this GFM to be scheduled."
    postgres_db, service_provider = setup_services

    recipe = await postgres_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            ingredients_declaration=[
                {
                    "language": "de",
                    "value": "Karotten, Kartoffeln",
                },
            ],
            titles=[{"language": "en", "value": "foo"}],
        )
    )

    gap_filling_worker = _run_gfm(recipe, postgres_db, service_provider, None)
    assert (
        not gap_filling_worker.should_be_scheduled()
    ), "GFM has been scheduled to run, although there are no nutrient_values."


@pytest.mark.asyncio
async def test_ingredient_estimator_first_on_sub_products_with_nutrients(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """Estimator should run first on sub-products with nutrient profiles and afterwards on root product.

    Inspired by the Valora "Ciabatta Gouda" and the "Gouda Brötchen" which both contain the "Backwerk Kräutercreme",
    where each time the creme has different values, even though it should not:

    - create two root product calculations each with a shared sub-product.
    - compare the sub-products calculation, should be equal.
    """
    onion_nutrients = load_seeded_nutrients["onion"]
    potato_nutrients = load_seeded_nutrients["potato"]
    tomato_nutrients = load_seeded_nutrients["tomato"]
    perturbed_onion_nutrients = nutrients_mixer((onion_nutrients, potato_nutrients), (0.95, 0.05))
    perturbed_potato_nutrients = nutrients_mixer((onion_nutrients, potato_nutrients), (0.05, 0.95))
    carrot_nutrients = load_seeded_nutrients["carrot"]
    mixed_nutrients = [
        NutritionalTuple(nutrition=perturbed_onion_nutrients, ratio=0.5),
        [
            NutritionalTuple(nutrition=tomato_nutrients, ratio=0.475),
            NutritionalTuple(nutrition=carrot_nutrients, ratio=0.025),
        ],
    ]

    await _run_calculation(
        setup_services,
        ["Zwiebeln", ["Tomaten", "Karotten"]],
        mixed_nutrients,
        expected_amounts={
            "Karotten": 2.5,
            "Karotten (subdivision)": 2.5,
            "Karotten (subdivision) (dried)": 0.0,
            "Karotten (subdivision) (normal product required for dried product)": 1.0466820180029306,
            "Zwiebeln": 50.0,
            "Zwiebeln (subdivision)": 45.29355721477526,
            "Zwiebeln (subdivision) (dried)": 4.706442805110537,
            "Zwiebeln (subdivision) (normal product required for dried product)": 1.025125628140706,
            "Tomaten": 47.5,
            "Tomaten (subdivision)": 47.5,
            "Tomaten (subdivision) (dried)": 0.0,
            "Tomaten (subdivision) (normal product required for dried product)": 1.0492078480747036,
        },
        max_allowed_residual=None,
    )

    mixed_nutrients = [
        NutritionalTuple(nutrition=perturbed_potato_nutrients, ratio=0.5),
        [
            NutritionalTuple(nutrition=tomato_nutrients, ratio=0.475),
            NutritionalTuple(nutrition=carrot_nutrients, ratio=0.025),
        ],
    ]
    await _run_calculation(
        setup_services,
        ["Kartoffeln", ["Tomaten", "Karotten"]],
        mixed_nutrients,
        expected_amounts={
            "Karotten": 2.5,
            "Karotten (subdivision)": 2.5,
            "Karotten (subdivision) (dried)": 0.0,
            "Karotten (subdivision) (normal product required for dried product)": 1.0466820180029306,
            "Kartoffeln": 50.0,
            "Kartoffeln (subdivision)": 50.0,
            "Kartoffeln (subdivision) (dried)": 0.0,
            "Kartoffeln (subdivision) (normal product required for dried product)": 1.025125628140706,
            "Tomaten": 47.5,
            "Tomaten (subdivision)": 47.5,
            "Tomaten (subdivision) (dried)": 0.0,
            "Tomaten (subdivision) (normal product required for dried product)": 1.0492078480747036,
        },
        max_allowed_residual=None,
    )


@pytest.mark.asyncio
async def test_ingredient_estimator_with_fixed_percentages_and_no_nutrient_files(
    setup_matchings_with_unknown_nutrients: Tuple[Calculation, CalcService]
) -> None:
    calculation, calc_service = setup_matchings_with_unknown_nutrients
    calculation, root_node = await calc_service.calculate(calculation)
    assert all(sub_node.amount for sub_node in root_node.get_sub_nodes()[0].get_sub_nodes())
    assert root_node.impact_assessment
    assert is_close(list(root_node.impact_assessment.quantities.values())[0].value, 1.2)


@pytest.mark.asyncio
@pytest.mark.parametrize("setup_recipe_with_all_amounts", [600, None], indirect=True)
async def test_ingredient_estimator_with_complete_percentages(
    setup_recipe_with_all_amounts: Tuple[Calculation, CalcService]
) -> None:
    """Tests situations where the ingredient amount estimator fails, but percentages are completely given.

    The first case (param = 600) corresponds to the case where all percentages are given. The second case (param=None)
    corresponds to all, except one, percentages are given (the final percentage can be worked out as 1 - [others' sum]).
    """
    calculation, calc_service = setup_recipe_with_all_amounts
    with mock.patch("gap_filling_modules.ingredient_amount_estimator_gfm.run", side_effect=ValueError()):
        calculation, root_node = await calc_service.calculate(calculation)
        assert not any(
            (data_error.error_classification == ErrorClassification.failed_amount_estimation)
            for data_error in calculation.data_errors
        )
        assert any(
            (data_error.error_classification == ErrorClassification.fallback_without_amount_estimation)
            for data_error in calculation.data_errors
        )
        expected_co2_value = 0.07425
        assert list(root_node.impact_assessment.quantities.values())[0].value == expected_co2_value


@pytest.mark.asyncio
async def test_ingredient_estimator_with_sub_recipe_production_amount(
    setup_minimal_sub_recipe_for_ingredient_declaration: Tuple[Calculation, CalcService]
) -> None:
    """Tests ingredient_amount_estimation for a recipe with subrecipe with production_amount != 1.0 kg."""
    calculation, calc_service = setup_minimal_sub_recipe_for_ingredient_declaration
    calculation, root_node = await calc_service.calculate(calculation)
    assert not any(
        (data_error.error_classification == ErrorClassification.failed_amount_estimation)
        for data_error in calculation.data_errors
    )
    assert not any(
        (data_error.error_classification == ErrorClassification.fallback_without_amount_estimation)
        for data_error in calculation.data_errors
    )
    expected_co2_value = 0.04455000000165069
    assert is_close(list(root_node.impact_assessment.quantities.values())[0].value, expected_co2_value)


@pytest.mark.asyncio
async def test_ingredient_estimator_nutrient_file_without_water(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    egg_nutrients = load_seeded_nutrients["egg"]
    carrot_nutrients = load_seeded_nutrients["carrot"]
    mix_50_50_carrot_egg = nutrients_mixer((carrot_nutrients, egg_nutrients), (0.5, 0.5))
    calculated_root_node = await _run_calculation(
        setup_services, "Dried egg, Karotten", mix_50_50_carrot_egg, return_early=True
    )

    def get_sub_node_from_path(root: Node, path: list[int]) -> Node:
        n = root
        for p in path:
            n = n.get_sub_nodes()[p]
        return n

    dried_egg_root_node = get_sub_node_from_path(calculated_root_node, [0, 0, 0, 0])
    dried_egg_20_percent_water = get_sub_node_from_path(calculated_root_node, [0, 0, 0, 0, 0, 0])
    dried_egg_20_percent_water_upscale = get_sub_node_from_path(calculated_root_node, [0, 0, 0, 0, 0, 0, 0, 0])
    carrot_root_node = get_sub_node_from_path(calculated_root_node, [0, 0, 0, 1])
    expected_amounts = [0.05, 1.0, 3.58, 0.05]

    for node, expected_amount in zip(
        [dried_egg_root_node, dried_egg_20_percent_water, dried_egg_20_percent_water_upscale, carrot_root_node],
        expected_amounts,
    ):
        assert round(node.amount.value, 2) == expected_amount
