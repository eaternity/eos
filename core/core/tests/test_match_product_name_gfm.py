"Tests for product name matching gap filling module."
import uuid
from typing import List, Optional, Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum

# Has to run AttachFoodTagsGapFillingWorker now since we attach nutrient values there (because nutrient values
# need some additional terms coming from glossary tags).
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingFactory
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingFactory

from core.domain.matching_item import MatchingItem
from core.domain.nodes import FoodProcessingActivityNode, LinkingActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.props.required_matching_prop import MatchingFailureReasonEnum
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeRType
from database.postgres.postgres_db import PostgresDb


@pytest.mark.asyncio
async def test_match_product_name_gfm(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)
    product_name_prop = carrot_ingredient.product_name
    assert len(product_name_prop.terms) == 1

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(carrot_ingredient)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    prop_term = product_name_prop.terms[0].get_term()
    assert isinstance(prop_term, Term)
    assert prop_term.xid == "A1791"  # Karotten

    nutrients_prop = carrot_ingredient.nutrient_values
    assert nutrients_prop is not None

    nutrients_term = nutrients_prop.get_prop_term()
    assert isinstance(nutrients_term, Term)
    assert nutrients_term.xid == "131"  # nutrients for Karotten


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_matching_to_node_uid(
    setup_matching_to_node_uid: Tuple[PostgresDb, ServiceProvider, MatchingItem],
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, matching_item = setup_matching_to_node_uid
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # change the minimum recipe's ingredient name to the matching string to test if the GFM will match it
    # to the convenience product:
    carrot_ingredient.product_name = [{"language": "en", "value": matching_item.matching_string}]
    carrot_ingredient = await postgres_db.get_graph_mgr().upsert_node_by_uid(carrot_ingredient)
    carrot_ingredient.original_node_uid = LinkToUidProp(uid=carrot_ingredient.uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # check if the child node is of type LinkingActivityNode:
    assert isinstance(calc_graph.child_of_root_node(), LinkingActivityNode)

    # check if the convenience product was inserted as a sub-node of the LinkingActivityNode:
    assert (
        str(calc_graph.child_of_root_node().get_sub_nodes()[0].original_node_uid.uid) == matching_item.matched_node_uid
    )


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_missing_matching(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    carrot_ingredient.product_name = [{"language": "en", "value": "some new name that has no matching"}]
    carrot_ingredient = await postgres_db.get_graph_mgr().upsert_node_by_uid(carrot_ingredient)
    carrot_ingredient.original_node_uid = LinkToUidProp(uid=carrot_ingredient.uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # make sure that error message starts with "Could not match product name"
    assert calc_graph.data_errors_log[0].message.startswith("Could not match product name")

    # now check the carrot in the database, if it was marked as missing matching:
    node_from_db = await postgres_db.get_graph_mgr().find_by_uid(carrot_ingredient.uid)
    assert node_from_db.required_matching[0].matching_failure_reason == MatchingFailureReasonEnum.no_matching_product


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_wrong_node_type(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    # default NodeType is 'recipe'
    child_of_root_node = FoodProcessingActivityNode(uid=node_uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(child_of_root_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(child_of_root_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_no_raw_input(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(uid=node_uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_no_names_field(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        raw_transport="air",
        flow_location="spain",
        amount_in_original_source_unit={"value": 150, "unit": "gram"},
        id="100100191",
        type="conceptual-ingredients",
        processing="raw",
        raw_conservation={"value": "fresh", "language": "en"},
        packaging="plastic",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_empty_names_field(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        raw_transport="air",
        flow_location="spain",
        product_name=[
            {
                "language": "de",
                "value": "",  # <-- empty names field
            }
        ],
        amount_in_original_source_unit={"value": 150, "unit": "gram"},
        id="100100191",
        type="conceptual-ingredients",
        processing="raw",
        raw_conservation={"value": "fresh", "language": "en"},
        packaging="plastic",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running the GFM
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)

    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)
    product_name_prop = ingredient_node.product_name
    assert len(product_name_prop.terms) == 1

    prop_term = product_name_prop.terms[0].get_term()
    assert isinstance(prop_term, Term)
    assert prop_term.xid == "EOS_data_error"

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient_node)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    assert len(calc_graph.data_errors_log) == 1
    assert calc_graph.data_errors_log[0].startswith("Empty name values!")

    assert ingredient_node.nutrient_values is None


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_lowercase_names_field(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        product_name=[
            {
                "language": "de",
                "value": "kartoffeln",
            }
        ],
        type="conceptual-ingredients",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running the GFM
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)

    product_name_prop = ingredient_node.product_name
    prop_term = product_name_prop.terms[0].get_term()
    assert prop_term.xid == "A00ZT"  # FoodEx2 xid for POTATO

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient_node)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    nutrients_prop = ingredient_node.nutrient_values
    assert nutrients_prop is not None

    nutrients_term = nutrients_prop.get_prop_term()
    assert isinstance(nutrients_term, Term)
    assert nutrients_term.xid == "508"  # nutrients for potato


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_composed_name(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Matching for dried potatoes (trockene Kartoffeln)."""
    postgres_db, service_provider = setup_services

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        product_name=[
            {
                "language": "de",
                "value": "getrockene kartoffeln",
            }
        ],
        type="conceptual-ingredients",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # running the GFM
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)

    product_name_props = ingredient_node.product_name
    assert len(product_name_props.terms) == 2, "Two terms should be matched: POTATO and DEHYDRATED OR DRIED"
    prop_term_xids = [p.get_term().xid for p in product_name_props.terms]
    assert prop_term_xids == ["A00ZT", "J0116"], "FoodEx2 xid for POTATO and DEHYDRATED OR DRIED"

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(ingredient_node)
    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    nutrients_prop = ingredient_node.nutrient_values
    assert nutrients_prop is not None

    nutrients_term = nutrients_prop.get_prop_term()
    assert isinstance(nutrients_term, Term)
    assert nutrients_term.xid == "508"  # nutrients for Karotten


@pytest.mark.asyncio
async def test_match_product_name_gfm_with_context_scoping(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    matching_service = service_provider.matching_service
    glossary_service = service_provider.glossary_service

    async def add_matching(supplier: Optional[str], producer: Optional[str], terms: List[Term]) -> None:
        # producer and supplier match
        data = dict()
        if supplier is not None:
            data["supplier"] = supplier
        if producer is not None:
            data["producer"] = producer
        await matching_service.put_matching_item(
            MatchingItem(
                gap_filling_module="MatchProductName",
                access_group_uid=None,
                lang="en",
                matching_string="apple",
                term_uids=[str(term.uid) for term in terms],
                data=data,
            )
        )

    async def run_matching_with_context(supplier: Optional[str], producer: Optional[str]) -> None:
        # Create a FoodProductFlowNode with context
        ingredient_node = FoodProductFlowNode(
            uid=uuid.uuid4(),
            product_name=[
                {
                    "language": "en",
                    "value": "apple",
                }
            ],
            type="conceptual-ingredients",
        )
        if supplier is not None:
            ingredient_node.supplier = supplier
        if producer is not None:
            ingredient_node.producer = producer

        # Create the calculation graph
        calc_graph = CalcGraph(
            service_provider,
            root_node_uid=ingredient_node.uid,
            glossary_service=service_provider.glossary_service,
        )
        calc_graph.add_node(ingredient_node)

        # Run the GFM
        gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
        await gap_filling_factory.init_cache()
        gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
        await gap_filling_worker.run(calc_graph)

        return ingredient_node

    # Get actual terms from the glossary service
    foodex2_ag_uid = service_provider.glossary_service.root_subterms["A0361"].access_group_uid
    apple: Term = glossary_service.terms_by_xid_ag_uid[("A01DJ", foodex2_ag_uid)]
    fresh: Term = glossary_service.terms_by_xid_ag_uid[("P0120", foodex2_ag_uid)]
    dried: Term = glossary_service.terms_by_xid_ag_uid[("J0116", foodex2_ag_uid)]
    frozen: Term = glossary_service.terms_by_xid_ag_uid[("J0136", foodex2_ag_uid)]

    await add_matching(supplier="S1", producer="P1", terms=[apple, fresh])
    await add_matching(supplier=None, producer="P1", terms=[apple, fresh, dried])
    await add_matching(supplier=None, producer=None, terms=[apple])
    await add_matching(supplier="S1", producer=None, terms=[apple, dried])
    await add_matching(supplier="S2", producer=None, terms=[apple, frozen])
    await add_matching(supplier=None, producer="P2", terms=[apple, frozen, dried])

    # Test with full context match
    ingredient_node = await run_matching_with_context(supplier="S1", producer="P1")
    assert {apple.uid, fresh.uid} == set([term.term_uid for term in ingredient_node.product_name.terms])

    # Test with partial context match
    ingredient_node = await run_matching_with_context(supplier="S1", producer="P3")
    assert {apple.uid, dried.uid} == set([term.term_uid for term in ingredient_node.product_name.terms])

    # Test with no context match
    ingredient_node = await run_matching_with_context(supplier="S3", producer="P3")
    assert {apple.uid} == set([term.term_uid for term in ingredient_node.product_name.terms])

    # Test with possible two matching different contexts (supplier should have precedence over producer)
    ingredient_node = await run_matching_with_context(supplier="S2", producer="P2")
    assert {apple.uid, frozen.uid} == set([term.term_uid for term in ingredient_node.product_name.terms])
