import copy

import pytest

from core.domain.props.quantity_prop import RawQuantity
from core.tests.conftest import MinimumRecipeWithDeclaration


@pytest.mark.asyncio
async def test_production_amount_equality_recursion_pitfall(
    create_minimum_recipe_with_declaration: MinimumRecipeWithDeclaration,
) -> None:
    _, _, recipe, _, _ = create_minimum_recipe_with_declaration

    # attach to recipe to check possible recursion problems
    raw_quantity1 = RawQuantity(prop_type="RawQuantity", value=17.0, unit="kg")
    raw_quantity1._owner_node = recipe
    recipe.production_amount = raw_quantity1

    raw_quantity2 = RawQuantity(prop_type="RawQuantity", value=42.42, unit="g")
    raw_quantity2._owner_node = recipe
    recipe.production_amount = raw_quantity2
    assert raw_quantity1 != raw_quantity2, "RawQuantity with different data should not be equal"

    raw_quantity3 = copy.deepcopy(raw_quantity1)
    assert raw_quantity1 == raw_quantity3, "RawQuantity with same data should be equal"
