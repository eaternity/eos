"Tests for system process caching."
import uuid
from typing import Tuple

import pytest
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode
from core.domain.nodes.node import Node
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import (
    AMOUNT_BRIGHTWAY_PROCESS_1,
    AMOUNT_BRIGHTWAY_PROCESS_2,
    EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
    ElaborateRecipe,
    is_close,
    prepend_flow_node,
)
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


async def seed_first_calculation(
    create_more_elaborate_recipe: ElaborateRecipe,
) -> tuple[Calculation, Node, CalcService]:
    er = create_more_elaborate_recipe
    glossary_service = er.service_provider.glossary_service

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(er.service_provider)
    calc_service = CalcService(
        er.service_provider,
        glossary_service,
        er.service_provider.node_service,
        gap_filling_module_loader,
    )
    root_flow = await prepend_flow_node(er.pg_db.get_graph_mgr(), recipe_uid=er.electricity_production_process.uid)

    # create a new Calculation to cache the electricity_production_process:
    calculation = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow.uid,
        child_of_root_node_uid=er.electricity_production_process.uid,
        return_log=False,
        save_as_system_process=True,
    )

    calculation, root_node = await calc_service.calculate(calculation)
    return calculation, root_node, calc_service


@pytest.mark.asyncio
async def test_system_process_caching(create_more_elaborate_recipe: ElaborateRecipe) -> None:
    """Tests if we can cache a process as a system process to be eused later."""
    er = create_more_elaborate_recipe
    calculation, root_node, calc_service = await seed_first_calculation(create_more_elaborate_recipe)

    root_char_term = er.service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = er.service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    result_co2 = (
        root_node.impact_assessment.amount_for_activity_production_amount().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    # check if calculated value of one unit of electricity was correctly calculated:
    assert is_close(result_co2, 1.2), (
        "In the book, the amount is 120 but that's for a production of 1000 kWh of electr. "
        "So 10 kWh (the node production_amount) should be 1.2 kg_co2_eq"
    )

    # check if a loaded electricity_process node from database contains the cached system process:
    reloaded_electricity_proc = await er.service_provider.node_service.get_root_flow_or_activity(
        er.electricity_production_process.uid
    )
    assert len(reloaded_electricity_proc.aggregated_cache.flow_quantities) > 2

    root_flow_above_recipe = await prepend_flow_node(er.pg_db.get_graph_mgr(), recipe_uid=er.recipe.uid)

    # check if a new calculation used the aggregated system process cache of the electricity_production_process:
    calc_using_cache = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow_above_recipe.uid,
        child_of_root_node_uid=er.recipe.uid,
        return_log=False,
        save_as_system_process=False,
        explicitly_attach_cached_elementary_resource_emission=True,
    )

    calc_using_cache, root_node = await calc_service.calculate(calc_using_cache)

    # get the process node and check if it is the electricity_production_process:
    sub_system_process: Node = root_node._sub_nodes[0]._sub_nodes[0]._sub_nodes[0]
    assert sub_system_process.uid == er.electricity_production_process.uid

    assert sub_system_process.get_sub_nodes()
    # check if the process node was actually using the system process cache:
    for sub_flow_node in sub_system_process.get_sub_nodes():
        assert isinstance(sub_flow_node.get_sub_nodes()[0], ElementaryResourceEmissionNode)

    result_co2 = (
        root_node.impact_assessment.amount_for_activity_production_amount().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    # check if calculated co2-value of a recipe using electricity was correctly calculated:
    assert is_close(result_co2, 120.0)


@pytest.mark.asyncio
async def test_system_process_caching_with_self_reference(create_more_elaborate_recipe: ElaborateRecipe) -> None:
    """Tests if we can cache a process as a system process to be reused later."""
    er = create_more_elaborate_recipe
    glossary_service = er.service_provider.glossary_service

    pg_graph_mgr = er.pg_db.get_graph_mgr()

    # add a self reference:
    flow = await pg_graph_mgr.upsert_node_by_uid(
        FlowNode(
            amount_in_original_source_unit={
                "value": 5.0,
                "unit": "kWh",
            },  # 5.0 kWh electricity is needed for a production_amount of 10 kWh electricity
            type="technosphere",
        )
    )
    await pg_graph_mgr.add_edge(
        Edge(
            parent_uid=er.electricity_production_process.uid,
            child_uid=flow.uid,
            edge_type=EdgeTypeEnum.lca_link,
        )
    )
    await pg_graph_mgr.add_edge(
        Edge(
            parent_uid=flow.uid,
            child_uid=er.electricity_production_process.uid,
            edge_type=EdgeTypeEnum.lca_link,
        )
    )
    # added a self consumption of 5 kWh electricity for a production of 10 kWh electricity -> expected amount of co2:
    expected_co2_with_elec_loss = 120.0 * 2

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(er.service_provider)
    calc_service = CalcService(
        er.service_provider,
        glossary_service,
        er.service_provider.node_service,
        gap_filling_module_loader,
    )

    # Step 1: calculate recipe co2 without caching:
    root_flow_above_recipe = await prepend_flow_node(er.pg_db.get_graph_mgr(), recipe_uid=er.recipe.uid)
    calc_without_cache = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow_above_recipe.uid,
        child_of_root_node_uid=er.recipe.uid,
        return_log=False,
        save_as_system_process=False,
    )

    calc_without_cache, root_recipe_node_without_cache = await calc_service.calculate(calc_without_cache)

    root_char_term = er.service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = er.service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    result_co2_wo_cache = (
        root_recipe_node_without_cache.impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value
    )

    assert is_close(result_co2_wo_cache, expected_co2_with_elec_loss)

    # Step 2: create a new Calculation to cache the electricity_production_process:
    root_flow = await prepend_flow_node(er.pg_db.get_graph_mgr(), recipe_uid=er.electricity_production_process.uid)
    calculation = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow.uid,
        child_of_root_node_uid=er.electricity_production_process.uid,
        return_log=False,
        save_as_system_process=True,
    )

    calculation, root_node = await calc_service.calculate(calculation)

    result_co2 = (
        root_node.impact_assessment.amount_for_activity_production_amount().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    # check if calculated value of one unit of electricity was correctly calculated:
    assert is_close(result_co2, 2.4), (
        "In the book, the amount is 120 but that's for a production of 1000 kWh of electr. "
        "But here, the electr. process needs itself 5 kWh to produce 10 kWh. "
        "So for 10 kWh (the node production_amount) we now need double the amount of co2, which is 2.4 kg_co2_eq"
    )

    # check if a loaded electricity_process node from database contains the cached system process:
    reloaded_electricity_proc = await er.service_provider.node_service.find_by_uid(
        er.electricity_production_process.uid
    )
    assert len(reloaded_electricity_proc.aggregated_cache.flow_quantities) > 2

    # Step 3: check if a new calculation used the aggregated system process cache of the electricity_production_process:
    calc_using_cache = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow_above_recipe.uid,
        child_of_root_node_uid=er.recipe.uid,
        return_log=False,
        save_as_system_process=False,
    )

    calc_using_cache, root_recipe_node = await calc_service.calculate(calc_using_cache)

    # get the process node and check if it is the electricity_production_process:
    sub_system_process: Node = root_recipe_node._sub_nodes[0]._sub_nodes[0]._sub_nodes[0]
    assert sub_system_process.uid == er.electricity_production_process.uid

    # check if the process node was actually using the system process cache:
    for sub_flow_node in sub_system_process.get_sub_nodes():
        assert isinstance(sub_flow_node.get_sub_nodes()[0], ElementaryResourceEmissionNode)

    # check if calculated co2-value of a recipe using cached electricity is the same as without cache:
    result_co2_w_cache = (
        root_recipe_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    assert is_close(result_co2_w_cache, expected_co2_with_elec_loss)

    # just in case also assert that the resulting co2 amount is the same with and without using cache:
    assert is_close(result_co2_w_cache, result_co2_wo_cache)


@pytest.mark.asyncio
async def test_system_process_caching_with_fixed_depth_for_calculating_supply(
    create_bw_graph_with_larger_depth: Tuple[PostgresDb, ServiceProvider, Node]
) -> None:
    """Tests if we can cache sub-processes as a system process and reuse their cache in a subsequent calculation."""
    postgres_db, service_provider, recipe = create_bw_graph_with_larger_depth
    glossary_service = service_provider.glossary_service

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service

    # step 1: perform a calculation with fixed_depth_for_calculating_supply to cache
    # the environmental flows of the brightway process which is a sub_sub_node of the top-recipe
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)
    root_flow_above_recipe = await prepend_flow_node(postgres_db.get_graph_mgr(), recipe_uid=recipe.uid)
    calculation = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow_above_recipe.uid,
        child_of_root_node_uid=recipe.uid,
        save_as_system_process=True,
        fixed_depth_for_calculating_supply=3,
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # check if the impact assessment of the root node is as expected:
    amount_for_bw_1 = AMOUNT_BRIGHTWAY_PROCESS_1
    amount_for_bw_2 = AMOUNT_BRIGHTWAY_PROCESS_2

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    char_root_node = root_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value

    expected_impact_assessment_of_bw_1 = amount_for_bw_1 * EXPECTED_CO2_AMOUNT_PER_KG_CARROT
    expected_impact_assessment_of_bw_2 = amount_for_bw_2 * EXPECTED_CO2_AMOUNT_PER_KG_CARROT

    expected_impact_assessment_of_root_node = expected_impact_assessment_of_bw_1 + expected_impact_assessment_of_bw_2
    assert is_close(char_root_node, expected_impact_assessment_of_root_node)

    # check if the impact_assessment of the bw_1 node is as expected:
    bw_1_node = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
    char_bw_1 = (
        bw_1_node.impact_assessment.amount_for_activity_production_amount().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    assert is_close(char_bw_1, expected_impact_assessment_of_bw_1)

    # check if the impact_assessment of the bw_2 node is as expected:
    bw_2_node = root_node.get_sub_nodes()[0].get_sub_nodes()[1].get_sub_nodes()[0]
    char_bw_2 = (
        bw_2_node.impact_assessment.amount_for_activity_production_amount().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    assert is_close(char_bw_2, expected_impact_assessment_of_bw_2)

    # check if the loaded bw-processes from the database contain the cached system process:
    reloaded_bw_1 = await service_provider.node_service.get_root_flow_or_activity(bw_1_node.uid)
    assert len(reloaded_bw_1.aggregated_cache.flow_quantities) > 0
    reloaded_bw_2 = await service_provider.node_service.get_root_flow_or_activity(bw_2_node.uid)
    assert len(reloaded_bw_2.aggregated_cache.flow_quantities) > 0

    # step 2: perform a new calculation and check if it uses the aggregated system process cache of the bw-processes
    root_flow = await prepend_flow_node(postgres_db.get_graph_mgr(), recipe_uid=bw_1_node.uid)
    calc_using_cache = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow.uid,
        child_of_root_node_uid=bw_1_node.uid,
        save_as_system_process=False,
    )

    calc_using_cache, root_recipe_node = await calc_service.calculate(calc_using_cache)

    # check if all the sub_sub_nodes are emission nodes
    for sub_flow_node in root_recipe_node._sub_nodes[0]._sub_nodes:
        assert isinstance(sub_flow_node._sub_nodes[0], ElementaryResourceEmissionNode)

    # check if the impact_assessment is still correct
    new_char_bw_1 = bw_1_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    assert is_close(new_char_bw_1, expected_impact_assessment_of_bw_1)

    new_char_root_recipe = (
        root_recipe_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )
    assert is_close(new_char_root_recipe, expected_impact_assessment_of_bw_1)


@pytest.mark.asyncio
async def test_system_process_caching_without_attaching_emissions(
    create_more_elaborate_recipe: ElaborateRecipe,
) -> None:
    """Tests if we can correctly calculate without adding emissions explicitly."""
    er = create_more_elaborate_recipe
    _, _, calc_service = await seed_first_calculation(create_more_elaborate_recipe)

    root_char_term = er.service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = er.service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    root_flow_above_recipe = await prepend_flow_node(er.pg_db.get_graph_mgr(), recipe_uid=er.recipe.uid)

    # check if a new calculation used the aggregated system process cache of the electricity_production_process:
    calc_using_cache = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=root_flow_above_recipe.uid,
        child_of_root_node_uid=er.recipe.uid,
        return_log=False,
        save_as_system_process=False,
        explicitly_attach_cached_elementary_resource_emission=False,
    )

    calc_using_cache, root_node = await calc_service.calculate(calc_using_cache)

    # get the process node and check if it is the electricity_production_process:
    sub_system_process: Node = root_node._sub_nodes[0]._sub_nodes[0]._sub_nodes[0]
    assert sub_system_process.uid == er.electricity_production_process.uid

    assert len(sub_system_process.get_sub_nodes()) == 0

    result_co2 = (
        root_node.impact_assessment.amount_for_activity_production_amount().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    # check if calculated co2-value of a recipe using electricity was correctly calculated:
    assert is_close(result_co2, 120.0)
