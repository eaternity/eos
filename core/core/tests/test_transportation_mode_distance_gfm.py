"Tests transportation mode distance gap filling module."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.transportation_mode_distance_gfm import TransportModeDistanceGapFillingFactory
from gap_filling_modules.unit_weight_conversion_gfm import NodeGfmStateEnum, UnitWeightConversionGapFillingWorker

from core.domain.calculation import Calculation
from core.domain.nodes.activity.food_processing_activity_node import FoodProcessingActivityNode
from core.domain.nodes.node import Node
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.location_prop import LocationProp, LocationQualifierEnum
from core.domain.props.origin_data_prop import OriginDataProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.domain.props.transport_modes_distances_prop import TransportModesDistancesProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_list_mutation import PropListMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceLocator, ServiceProvider
from core.tests.conftest import (
    AMOUNT_BRIGHTWAY_PROCESS_2,
    EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
    MinimumRecipeRType,
    MinimumRecipeWithDeclaration,
    MinimumRecipeWithTwoOrigins,
    assert_transport_allclose,
    is_close,
)
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID
from database.postgres.postgres_db import PostgresDb

DUMMY_LOCATION_PROP = LocationProp(
    country_code="XXX", latitude=0.0, longitude=0.0, address="Dummy", location_qualifier=LocationQualifierEnum.known
)

# The benchmark values were updated on 2023-12-05 according to the decision from the Science team.
MODES_AND_DISTANCES_FOR_ZURICH = {
    "CH": {
        "road": {
            "distances": {
                "main_carriage": 104.435052,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 104.435052,
            },
            "co2": {
                "main_carriage": 0.00866023448,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        }
    },
    "ES": {
        "road": {
            "distances": {
                "main_carriage": 1629.16742,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1629.16742,
            },
            "co2": {
                "main_carriage": 0.122126599,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 1145.62824,
                "post_carriage": 489.88509468894387,
                "pre_carriage": 422.4294985794444,  # From Area
                "total": 2057.9428332683883,
            },
            "co2": {
                "main_carriage": 0.0177100863,
                "post_carriage": 0.03779315617,
                "pre_carriage": 0.03147642385,
            },
        },
        "air": {
            "distances": {
                "main_carriage": 1334.543,
                "post_carriage": 12.810948636982577,
                "pre_carriage": 118.7592524227776,  # From Area
                "total": 1466.11320105976,
            },
            "co2": {
                "main_carriage": 1.28445241,
                "post_carriage": 0.0010457848169151132,
                "pre_carriage": 0.00950143836736783548,
            },
        },
    },
    "IT": {
        "road": {
            "distances": {
                "main_carriage": 744.706759,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 744.706759,
            },
            "co2": {
                "main_carriage": 0.0563098788,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 464.257222,
                "post_carriage": 482.68064016805823,
                "pre_carriage": 225.6756857559661,  # From Area
                "total": 1172.6135479240243,
            },
            "co2": {
                "main_carriage": 0.00717687919,
                "post_carriage": 0.037118404570467528,
                "pre_carriage": 0.01764099225618014410,
            },
        },
    },
    "DK": {
        "road": {
            "distances": {
                "main_carriage": 1171.10237,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1171.10237,
            },
            "co2": {
                "main_carriage": 0.0866346466,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 5710.14667,
                "post_carriage": 489.88509468894387,
                "pre_carriage": 85.47875765574823,  # From Area
                "total": 6285.510522344693,
            },
            "co2": {
                "main_carriage": 0.0889280212,
                "post_carriage": 0.037793156170643806,
                "pre_carriage": 0.007149315731446453,
            },
        },
    },
    "NL": {
        "road": {
            "distances": {
                "main_carriage": 743.567627,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 743.567627,
            },
            "co2": {
                "main_carriage": 0.0559465938,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 4752.53622,
                "post_carriage": 470.71780015537854,
                "pre_carriage": 128.124250751518,
                "total": 5351.378270906896,
            },
            "co2": {
                "main_carriage": 0.0737920445,
                "post_carriage": 0.036253757642176594,
                "pre_carriage": 0.009075528358051253,
            },
        },
    },
    "PT": {
        "road": {
            "distances": {
                "main_carriage": 2014.30979,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 2014.30979,
            },
            "co2": {
                "main_carriage": 0.151539758,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 2450.61755,
                "post_carriage": 470.71780015537854,
                "pre_carriage": 175.7143171940921,
                "total": 3097.0496673494704,
            },
            "co2": {
                "main_carriage": 0.0378837102,
                "post_carriage": 0.036253757642176594,
                "pre_carriage": 0.01331208795973501279,
            },
        },
        "air": {
            "distances": {
                "main_carriage": 1818.448,
                "post_carriage": 12.810948636982577,
                "pre_carriage": 149.94060061429968,
                "total": 1981.1995492512824,
            },
            "co2": {
                "main_carriage": 1.45987917,
                "post_carriage": 0.0010457848169151132,
                "pre_carriage": 0.01127052303475366,
            },
        },
    },
}

MODES_AND_DISTANCES_FOR_SWITZERLAND = {
    "CH": {
        "road": {
            "distances": {
                "main_carriage": 163.7402977783409,  # From Area
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 163.7402977783409,
            },
            "co2": {"main_carriage": 0.013427246535166712, "pre_carriage": 0, "post_carriage": 0},
        }
    },
    "ES": {
        "road": {
            "distances": {
                "main_carriage": 1598.69564,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1598.69564,
            },
            "co2": {
                "main_carriage": 0.119955051,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 1145.62824,
                "post_carriage": 399.7692292204276,
                "pre_carriage": 422.4294985794444,
                "total": 1967.8269677998721,
            },
            "co2": {
                "main_carriage": 0.0177100863,
                "post_carriage": 0.030642289129900684,
                "pre_carriage": 0.031476423854131956,
            },
        },
        "air": {
            "distances": {
                "main_carriage": 1334.543,
                "post_carriage": 109.01598152625796,
                "pre_carriage": 118.7592524227776,  # From Area
                "total": 1562.3182339490354,
            },
            "co2": {
                "main_carriage": 1.28445241,
                "post_carriage": 0.008739595658422581,
                "pre_carriage": 0.00950143836736783548,
            },
        },
    },
    "IT": {
        "road": {
            "distances": {
                "main_carriage": 708.321977,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 708.321977,
            },
            "co2": {
                "main_carriage": 0.0532901736,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 464.257222,
                "post_carriage": 393.0599291584942,
                "pre_carriage": 225.6756857559661,  # From Area
                "total": 1082.9928369144602,
            },
            "co2": {
                "main_carriage": 0.00717687919,
                "post_carriage": 0.030018572956367836,
                "pre_carriage": 0.01764099225618014,
            },
        },
    },
    "DK": {
        "road": {
            "distances": {
                "main_carriage": 1257.01659,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1257.01659,
            },
            "co2": {
                "main_carriage": 0.0925568316,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 5710.14667,
                "post_carriage": 399.7692292204276,
                "pre_carriage": 85.47875765574821,  # From Area
                "total": 6195.394656876176,
            },
            "co2": {
                "main_carriage": 0.0889280212,
                "post_carriage": 0.030642289129900684,
                "pre_carriage": 0.007149315731446452,
            },
        },
        "air": {
            "distances": {
                "main_carriage": 1016.873,
                "post_carriage": 109.01598152625796,
                "pre_carriage": 87.28117701861235,
                "total": 1213.1701585448704,
            },
            "co2": {
                "main_carriage": 1.22534556,
                "post_carriage": 0.008739595658422581,
                "pre_carriage": 0.006329112675206034,
            },
        },
    },
    "NL": {
        "road": {
            "distances": {
                "main_carriage": 803.511844,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 803.511844,
            },
            "co2": {
                "main_carriage": 0.0598233292,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 4752.53622,
                "post_carriage": 385.658628434049,
                "pre_carriage": 128.124250751518,
                "total": 5266.319099185567,
            },
            "co2": {
                "main_carriage": 0.0737920445,
                "post_carriage": 0.029487824127740394,
                "pre_carriage": 0.009075528358051253,
            },
        },
    },
    "PT": {
        "road": {
            "distances": {
                "main_carriage": 1983.83801,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1983.83801,
            },
            "co2": {
                "main_carriage": 0.149359738,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 2450.61755,
                "post_carriage": 385.658628434049,
                "pre_carriage": 175.7143171940921,
                "total": 3011.9904956281407,
            },
            "co2": {
                "main_carriage": 0.0378837102,
                "post_carriage": 0.029487824127740394,
                "pre_carriage": 0.013312087959735012,
            },
        },
        "air": {
            "distances": {
                "main_carriage": 1818.448,
                "post_carriage": 109.01598152625796,
                "pre_carriage": 149.94060061429968,
                "total": 2077.4045821405575,
            },
            "co2": {
                "main_carriage": 1.45987917,
                "post_carriage": 0.008739595658422581,
                "pre_carriage": 0.01127052303475366,
            },
        },
    },
}

MODES_AND_DISTANCES_FOR_SPAIN = {
    "ES": {
        "road": {
            "distances": {
                "main_carriage": 573.6071490336897,  # From Area
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 573.6071490336897,
            },
            "co2": {"main_carriage": 0.0451036013032305, "pre_carriage": 0, "post_carriage": 0},
        }
    },
    "FR": {
        "road": {
            "distances": {
                "main_carriage": 1148.32515,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1148.32515,
            },
            "co2": {
                "main_carriage": 0.0840715648,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 2942.53521,
                "post_carriage": 425.60856317486264,
                "pre_carriage": 521.2276863719535,
                "total": 3889.3714595468164,
            },
            "co2": {
                "main_carriage": 0.0454881879,
                "post_carriage": 0.03132665556009418,
                "pre_carriage": 0.038812674643978466,
            },
        },
    },
    "PT": {
        "road": {
            "distances": {
                "main_carriage": 577.345759,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 577.345759,
            },
            "co2": {
                "main_carriage": 0.0429978706,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 1768.15766,
                "post_carriage": 445.81520770768714,
                "pre_carriage": 124.73822800300255,
                "total": 2338.7110957106897,
            },
            "co2": {
                "main_carriage": 0.0273336705,
                "post_carriage": 0.03314654636185427,
                "pre_carriage": 0.009509699253900859,
            },
        },
    },
    "NL": {
        "road": {
            "distances": {
                "main_carriage": 1759.71659,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 1759.71659,
            },
            "co2": {
                "main_carriage": 0.130555201,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 1631.73067,
                "post_carriage": 496.29538047761315,
                "pre_carriage": 128.124250751518,
                "total": 2256.150301229131,
            },
            "co2": {
                "main_carriage": 0.0255480037,
                "post_carriage": 0.036680985784735144,
                "pre_carriage": 0.009075528358051253,
            },
        },
        "air": {
            "distances": {
                "main_carriage": 1555.527,
                "post_carriage": 118.7592524227776,  # From Area
                "pre_carriage": 78.59548500659491,
                "total": 1752.8817374293726,
            },
            "co2": {
                "main_carriage": 1.25370987,
                "post_carriage": 0.00993218404695775,
                "pre_carriage": 0.005554095959599183,
            },
        },
    },
    "DE": {
        "road": {
            "distances": {
                "main_carriage": 2026.05423,
                "pre_carriage": 0,
                "post_carriage": 0,
                "total": 2026.05423,
            },
            "co2": {
                "main_carriage": 0.150345435,
                "pre_carriage": 0,
                "post_carriage": 0,
            },
        },
        "sea": {
            "distances": {
                "main_carriage": 2143.28606,
                "post_carriage": 496.29538047761315,
                "pre_carriage": 356.8095875009643,
                "total": 2996.3910279785773,
            },
            "co2": {
                "main_carriage": 0.0336426805,
                "post_carriage": 0.036680985784735144,
                "pre_carriage": 0.027711870666082918,
            },
        },
        "air": {
            "distances": {
                "main_carriage": 1517.948,
                "post_carriage": 118.7592524227776,  # From Area
                "pre_carriage": 248.1760776877359,
                "total": 1884.8833301105135,
            },
            "co2": {
                "main_carriage": 1.36211477,
                "post_carriage": 0.00993218404695775,
                "pre_carriage": 0.018478870071787508,
            },
        },
    },
}

DEFAULT_UNKNOWN_PRODUCTION_MODES_AND_DISTANCES = {
    "sea": {
        "distances": {
            "main_carriage": 11428.6755,
            "post_carriage": 2569.737467,
            "pre_carriage": 666.024184,
            "total": 14664.437150999998,
        },
        "co2": {"main_carriage": 0.12019801, "post_carriage": 0.19076337, "pre_carriage": 0.0952182809},
    },
}


@pytest.mark.asyncio
async def test_transportation_mode_distance_with_small_declaration(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    """Calculate transport modes and their distances for ingredients where origin is calculated by Origin GFM."""
    postgres_db, service_provider, recipe, root_flow, _sub_flow = create_minimum_recipe_with_small_declaration
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    origin_split_nodes = (
        root_node.get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()
    )
    assert len(origin_split_nodes) == 6

    for node in origin_split_nodes:
        # asserting that everything's good with transportation choices prop
        prop_data = node.get_sub_nodes()[0].transport
        assert prop_data
        origin_location_prop_data = node.get_sub_nodes()[0].get_sub_nodes()[0].flow_location[0]
        assert_transport_allclose(prop_data, MODES_AND_DISTANCES_FOR_ZURICH.get(origin_location_prop_data.country_code))


@pytest.mark.asyncio
async def test_transportation_mode_distance_with_two_recipe_origins(
    create_minimum_recipe_with_small_declaration_and_two_origins: MinimumRecipeWithTwoOrigins,
) -> None:
    """Tests a case when 2 origins are specified for a combined product and no origins for monoproducts."""
    postgres_db, service_provider, recipe, _, root_flow = create_minimum_recipe_with_small_declaration_and_two_origins
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # recipe -> recipe origin split flow nodes
    origin_split_recipe_nodes = (
        root_node.get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()
    )
    assert len(origin_split_recipe_nodes) == 2

    for origin_split_recipe_node in origin_split_recipe_nodes:
        # recipe origin split flow nodes -> split recipe node -> main food_product_flow node at origin.
        node_at_origin = origin_split_recipe_node.get_sub_nodes()[0].get_sub_nodes()[0]
        location_prop = node_at_origin.get_sub_nodes()[0].activity_location[-1]
        recipe_origin_country_code = location_prop.country_code
        assert recipe_origin_country_code in (
            "ES",
            "CH",
        )

        # Chain of nested nodes:
        # -> main food_product_flow node
        #   -> food_product (in destination location)
        #     -> transported_food_product_flow
        #       -> food_product (in origin location)
        #         -> origin split ingredient nodes
        origin_split_nodes = node_at_origin.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()

        if recipe_origin_country_code == "CH":
            assert len(origin_split_nodes) == 6
        elif recipe_origin_country_code == "ES":
            assert len(origin_split_nodes) == 5
        else:
            raise ValueError(f"Unexpected recipe origin country code: {recipe_origin_country_code}")

        modes_and_distances_per_country = {
            "CH": MODES_AND_DISTANCES_FOR_SWITZERLAND,
            "ES": MODES_AND_DISTANCES_FOR_SPAIN,
        }

        for node in origin_split_nodes:
            origin_node = node.get_sub_nodes()[0].get_sub_nodes()[0]
            # asserting that everything's good with transportation choices prop
            prop_data = origin_node.get_parent_nodes()[0].transport
            assert prop_data
            origin_location_prop_data = origin_node.flow_location[0]
            assert_transport_allclose(
                prop_data,
                modes_and_distances_per_country.get(recipe_origin_country_code).get(
                    origin_location_prop_data.country_code
                ),
            )


@pytest.mark.asyncio
async def test_transportation_mode_distance_with_local_production_ingredient(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    """Tests a case when a product has ingredients with a local production FAO code defined."""
    postgres_db, service_provider, recipe, local_production_ingredient, _, _root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=local_production_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(local_production_ingredient)

    # adding auxiliary food product node)
    calc_graph.add_node(recipe)
    calc_graph.add_edge(recipe.uid, local_production_ingredient.uid)

    # adding custom FAO code to local_production_ingredient, using a mutation
    root_fao_term = service_provider.glossary_service.root_subterms.get("Root_FAO")
    assert root_fao_term, "No root FAO term found."

    local_production_term: Term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
        "100000",
        str(root_fao_term.access_group_uid),
    )
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=local_production_ingredient.uid,
        prop_name="origin_data",
        prop=OriginDataProp.unvalidated_construct(fao_code=GlossaryTermProp(term_uid=local_production_term.uid)),
    )
    calc_graph.apply_mutation(prop_mutation)

    # adding necessary data for this GFM to run
    prop_mutation = PropListMutation(
        created_by_module="test",
        node_uid=recipe.uid,
        prop_name="activity_location",
        props=[DUMMY_LOCATION_PROP.duplicate()],
        append=False,
    )
    calc_graph.apply_mutation(prop_mutation)

    prop_mutation = PropListMutation(
        created_by_module="test",
        node_uid=local_production_ingredient.uid,
        prop_name="flow_location",
        props=[DUMMY_LOCATION_PROP.duplicate()],
        append=False,
    )
    calc_graph.apply_mutation(prop_mutation)

    # running the GFM
    gap_filling_factory = TransportModeDistanceGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(local_production_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # Transportation of food node is added as a parent of the "root node."
    assert not isinstance(calc_graph.get_root_node().get_parent_nodes()[0].transport, TransportModesDistancesProp)


def get_child_food_activity_node() -> FoodProcessingActivityNode:
    service_provider = ServiceLocator().service_provider
    root_unit_term = service_provider.glossary_service.root_subterms.get("EOS_units")
    kilogram_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_kilogram", root_unit_term.access_group_uid)
    ]

    return FoodProcessingActivityNode(
        uid=uuid.uuid4(),
        production_amount=QuantityProp(
            value=1.0, unit_term_uid=kilogram_term.uid, for_reference=ReferenceAmountEnum.self_reference
        ),
        gfm_state=GfmStateProp(
            worker_states={UnitWeightConversionGapFillingWorker.__name__: NodeGfmStateEnum.finished}
        ),
    )


@pytest.mark.asyncio
async def test_transportation_mode_distance_with_unknown_production_ingredient(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    """Tests a case when a product has ingredients with an unknown production FAO code defined."""
    postgres_db, service_provider, recipe, unknown_production_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=unknown_production_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(unknown_production_ingredient)

    child_food_activity = get_child_food_activity_node()
    calc_graph.add_node(child_food_activity)

    # adding auxiliary food product node
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_node)
    calc_graph.add_edge(recipe.uid, unknown_production_ingredient.uid)
    calc_graph.add_edge(root_node.uid, recipe.uid)
    calc_graph.add_edge(unknown_production_ingredient.uid, child_food_activity.uid)

    # adding custom FAO code to unknown_production_ingredient, using a mutation
    root_fao_term = service_provider.glossary_service.root_subterms.get("Root_FAO")
    assert root_fao_term, "No root FAO term found."

    failed_production_term: Term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
        "400000",
        str(root_fao_term.access_group_uid),
    )
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=unknown_production_ingredient.uid,
        prop_name="origin_data",
        prop=OriginDataProp.unvalidated_construct(fao_code=GlossaryTermProp(term_uid=failed_production_term.uid)),
    )
    calc_graph.apply_mutation(prop_mutation)

    # adding necessary data for this GFM to run
    prop_mutation = PropListMutation(
        created_by_module="test",
        node_uid=root_node.uid,
        prop_name="flow_location",
        props=[DUMMY_LOCATION_PROP.duplicate()],
        append=False,
    )
    calc_graph.apply_mutation(prop_mutation)

    prop_mutation = PropListMutation(
        created_by_module="test",
        node_uid=unknown_production_ingredient.uid,
        prop_name="flow_location",
        props=[DUMMY_LOCATION_PROP.duplicate()],
        append=False,
    )
    calc_graph.apply_mutation(prop_mutation)

    # running the GFM
    gap_filling_factory = TransportModeDistanceGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(unknown_production_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert_transport_allclose(
        calc_graph.get_root_node().get_parent_nodes()[0].transport, DEFAULT_UNKNOWN_PRODUCTION_MODES_AND_DISTANCES
    )


# TODO: change values in this test after Science team decides on default distances for such ingredients
@pytest.mark.asyncio
async def test_transportation_mode_distance_with_unknown_fish_production_ingredient(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    """Tests a case when a product has ingredients with an unknown fish production FAO code defined."""
    postgres_db, service_provider, recipe, unknown_fish_production_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=unknown_fish_production_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(unknown_fish_production_ingredient)

    child_food_activity = get_child_food_activity_node()
    calc_graph.add_node(child_food_activity)

    # adding auxiliary food product node
    calc_graph.add_node(root_node)
    calc_graph.add_node(recipe)
    calc_graph.add_edge(recipe.uid, unknown_fish_production_ingredient.uid)
    calc_graph.add_edge(root_node.uid, recipe.uid)
    calc_graph.add_edge(unknown_fish_production_ingredient.uid, child_food_activity.uid)

    # adding custom FAO code to unknown_fish_production_ingredient, using a mutation
    root_fao_term = service_provider.glossary_service.root_subterms.get("Root_FAO")
    assert root_fao_term, "No root FAO term found."

    unknown_fish_production_term: Term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
        "300000",
        str(root_fao_term.access_group_uid),
    )
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=unknown_fish_production_ingredient.uid,
        prop_name="origin_data",
        prop=OriginDataProp.unvalidated_construct(fao_code=GlossaryTermProp(term_uid=unknown_fish_production_term.uid)),
    )
    calc_graph.apply_mutation(prop_mutation)

    # adding necessary data for this GFM to run
    prop_mutation = PropListMutation(
        created_by_module="test",
        node_uid=root_node.uid,
        prop_name="flow_location",
        props=[DUMMY_LOCATION_PROP.duplicate()],
        append=False,
    )
    calc_graph.apply_mutation(prop_mutation)

    prop_mutation = PropListMutation(
        created_by_module="test",
        node_uid=unknown_fish_production_ingredient.uid,
        prop_name="flow_location",
        props=[DUMMY_LOCATION_PROP.duplicate()],
        append=False,
    )
    calc_graph.apply_mutation(prop_mutation)

    # running the GFM
    gap_filling_factory = TransportModeDistanceGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(unknown_fish_production_ingredient)
    assert gap_filling_worker.should_be_scheduled(), "GFM has not been scheduled to run, while it should not be."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # TODO: change values here after Science team decides on default distances for such ingredients
    assert_transport_allclose(
        calc_graph.get_root_node().get_parent_nodes()[0].transport, DEFAULT_UNKNOWN_PRODUCTION_MODES_AND_DISTANCES
    )


@pytest.mark.asyncio
async def test_location_and_transport_for_sub_bw_nodes(
    create_recipe_with_two_layers_of_brightway_nodes_containing_location_information: Tuple[
        PostgresDb, ServiceProvider, Node, list[tuple[uuid.UUID, uuid.UUID]], Node
    ]
) -> None:
    """Tests if we can perform calculation using the location information stored in Brightway Nodes."""
    (
        postgres_db,
        service_provider,
        recipe,
        activity_flow_uid_tuples_below_bw_nodes_to_compute,
        root_flow,
    ) = create_recipe_with_two_layers_of_brightway_nodes_containing_location_information
    glossary_service = service_provider.glossary_service

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service

    # step 1: perform a calculation with not using the information from the brightway nodes
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)
    calculation = Calculation(uid=uuid.uuid4(), root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid)

    calculation, root_node = await calc_service.calculate(calculation)

    # check if the impact assessment of the root node is as expected:
    amount_for_bw_2 = AMOUNT_BRIGHTWAY_PROCESS_2

    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    result_co2 = root_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value

    expected_impact_assessment_of_bw_2 = amount_for_bw_2 * EXPECTED_CO2_AMOUNT_PER_KG_CARROT

    expected_impact_assessment_of_root_node = expected_impact_assessment_of_bw_2
    assert is_close(result_co2, expected_impact_assessment_of_root_node)

    # step 2: perform a calculation with using the information from the brightway nodes
    await service_provider.modeled_activity_node_service.init(activity_flow_uid_tuples_below_bw_nodes_to_compute)

    # the rest is the same as before
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)
    calculation = Calculation(uid=uuid.uuid4(), root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid)

    calculation, root_node = await calc_service.calculate(calculation)

    # check that transport has been calculated correctly
    bw_node = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
    flow_below_bw_node = bw_node.get_sub_nodes()[0]
    prop_data = flow_below_bw_node.get_sub_nodes()[0].transport
    assert_transport_allclose(prop_data, MODES_AND_DISTANCES_FOR_SWITZERLAND["NL"])

    result_co2 = root_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    assert result_co2 > expected_impact_assessment_of_root_node


@pytest.mark.asyncio
async def test_transportation_mode_distance_with_transport_of_sub_recipe(
    create_recipe_with_larger_depth: MinimumRecipeRType,
) -> None:
    """Calculate transport modes and their distances for ingredients where origin is calculated by Origin GFM."""
    (
        postgres_db,
        service_provider,
        top_recipe,
        sub_recipe,
        carrot_ingredient,
        carrot_process,
        root_flow,
    ) = create_recipe_with_larger_depth
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(top_recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, top_recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=top_recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # check transport of sub-recipe to recipe:
    transport_parent_recipe = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[1]
    assert transport_parent_recipe.product_name.source_data_raw[0]["value"] == "Main carriage"

    # check transport of sub-sub-ingredient into sub-recipe:
    transport_sub_ingredient = (
        root_node.get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[2]
    )
    assert transport_sub_ingredient.product_name.source_data_raw[0]["value"] == "Main carriage"
