"Configuration for all tests."
import asyncio
import json
import os
import uuid
from typing import NamedTuple, Optional, Tuple, Type, cast
from unittest import mock

import httpx
import numpy as np
import pytest
import pytest_asyncio
from _pytest.fixtures import SubRequest
from gap_filling_modules.ingredicalc.helpers import transform_eurofir
from gap_filling_modules.processing_gfm import ProcessingGapFillingFactory, ProcessingSettings
from lxml import etree
from pytest import FixtureRequest

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.glossary_link import GlossaryLink
from core.domain.matching_item import MatchingItem
from core.domain.nodes import (
    ElementaryResourceEmissionNode,
    FoodProcessingActivityNode,
    FoodProductFlowNode,
    ModeledActivityNode,
    SupplySheetActivityNode,
)
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.props.names_prop import RawName
from core.domain.props.quantity_prop import QuantityProp, RawQuantity
from core.domain.props.raw_nutrient_values_per_100g import RawNutrientValuesPer100g
from core.domain.props.transport_modes_distances_prop import TransportModesDistancesProp
from core.domain.term import Term
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceLocator, ServiceProvider
from database.postgres.pg_graph_mgr import PostgresGraphMgr
from database.postgres.pg_term_mgr import PgTermMgr
from database.postgres.postgres_db import PostgresDb
from database.postgres.settings import PgSettings


class MinimumRecipeRType(NamedTuple):
    "Elements for a minimal recipe."
    postgres_db: PostgresDb
    service_provider: ServiceProvider
    recipe: Node
    ingredient: Node
    process: Node
    root_flow: FlowNode


class MinimumSupplyRType(NamedTuple):
    "Elements for a minimal recipe."
    postgres_db: PostgresDb
    service_provider: ServiceProvider
    supply: Node
    ingredient: Node
    process: Node
    root_flow: FlowNode


class MinimumRecipeWithDeclaration(NamedTuple):
    "Elements for a minimal recipe."
    postgres_db: PostgresDb
    service_provider: ServiceProvider
    recipe: FoodProcessingActivityNode
    root_flow: FoodProductFlowNode
    sub_flow: FoodProductFlowNode


class MinimumRecipeWithTomato(NamedTuple):
    "Elements for a minimal recipe."
    postgres_db: PostgresDb
    service_provider: ServiceProvider
    recipe: FoodProcessingActivityNode
    tomato_ingredient: Node
    tomato_process: Node
    root_flow: FoodProductFlowNode


class MinimumSupplyWithTomato(NamedTuple):
    "Elements for a minimal recipe."
    postgres_db: PostgresDb
    service_provider: ServiceProvider
    supply: SupplySheetActivityNode
    tomato_ingredient: Node
    tomato_process: Node
    root_flow: FoodProductFlowNode


class MinimumRecipeWithTwoOrigins(NamedTuple):
    "Elements for a minimal recipe."
    postgres_db: PostgresDb
    service_provider: ServiceProvider
    recipe: FoodProcessingActivityNode
    lca_process: Node
    root_flow: FoodProductFlowNode


class MinimumRecipeForUnitWeightGFM(NamedTuple):
    "Elements for a minimal recipe."
    postgres_db: PostgresDb
    service_provider: ServiceProvider
    recipe: Node
    apple_juice_ingredient: Node
    orange_juice_ingredient: Node
    abstract_orange_juice_ingredient: Node
    piece_ingredient: Node
    root_flow: FoodProductFlowNode


class TermAccessGroupIdsRType(NamedTuple):
    "TermAccessGroupIds."
    eaternity_term_group_uid: str
    eurofir_term_group_uid: str
    foodex2_term_group_uid: str
    nutrient_subdivision_term_group_uid: str
    perishability_term_group_uid: str
    transport_term_group_uid: str
    eaternity_namespace_uid: str


class ElaborateRecipe(NamedTuple):
    "Elaborate Recipe Without LCA Process."
    pg_db: PostgresDb
    service_provider: ServiceProvider
    recipe: Node
    electricity_ingredient: Node
    electricity_production_process: Node
    fuel_production_process: Node
    co2_process: Node
    sulfur_process: Node
    crude_oil_process: Node
    root_flow: FlowNode


ECOTRANSIT_RESPONSES_FOLDER = "ecotransit_responses"

EXPECTED_CO2_AMOUNT_PER_KG_CARROT = 1.24884000013878
EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR = 1.3486367353445314
# 6.2.: co2 value for cooling changed from changed from 0.022206120329021733 due to updated travel speed
EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR = 0.025436355338679384
# 28.02.: what was the variable before did not actually represent the freezing transport from ES to CH
EXPECTED_TRANSPORT_FREEZING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR = 0.05087271067735877

# transport for the dummy tractor lca
EXPECTED_FUEL_CONSUMPTION_CO2_AMOUNT_PER_KG_CARROT = 0.011682579788516279
EXPECTED_INFRASTRUCTURE_CO2_AMOUNT_PER_KG_CARROT = 0.008086555661956733  # TODO: verify this comes from infrastructure
EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT = (
    EXPECTED_FUEL_CONSUMPTION_CO2_AMOUNT_PER_KG_CARROT + EXPECTED_INFRASTRUCTURE_CO2_AMOUNT_PER_KG_CARROT
)
# 6.2.: co2 value for cooling changed from changed from 0.007566869685290856 due to updated travel speed
EXPECTED_COOLING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT = 0.009002130220565174
# 6.2: co2 value for cooling changed from changed from 0.0074941559166499085 due to updated travel speed
# 28.02.: renamed this variable because it is actually not representing the frozen air transport from ES to CH
# 28.02.: doupled the attached flows to the emission nodes to get a difference to the cooled transport
EXPECTED_FREEZING_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT = 0.01800426044113035

EXPECTED_GWP_20_CO2_AMOUNT_PER_KG_CARROT = 3.2874
# 6.2.: co2 value for cooling changed from changed from 0.05727515185727524 due to updated travel speed
EXPECTED_GWP_20_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR = 0.0656067378331926
EXPECTED_GWP_20_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR = 1.4289797389013867

EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_DEFAULT_TRANSPORT = 0.9372719227106997
EXPECTED_TRANSPORT_FREEZING_CO2_AMOUNT_PER_KG_DEFAULT_TRANSPORT = 1.6351514925058546

AMOUNT_BRIGHTWAY_PROCESS_1 = 0.9
AMOUNT_BRIGHTWAY_PROCESS_2 = 0.7
SAMPLE_JSONS_DIR = os.path.join(
    os.path.dirname(__file__),
    "sample_jsons",
)


TOLERANCE = 1e-6


def is_close(a: float, b: float, tol: float = TOLERANCE) -> bool:
    "Compare values and see if they are within tolerance."
    return abs(a - b) < tol


def assert_transport_allclose(value: TransportModesDistancesProp, reference: dict) -> None:
    """Compares transport data dicts with np.allclose."""
    for k, v in value.distances.items():
        assert np.allclose(
            [getattr(v, key).value for key in sorted(v.__slots__) if getattr(v, key) is not None],
            [val for key, val in sorted(reference[k]["distances"].items())],
        )
    for k, v in value.co2.items():
        assert np.allclose(
            [getattr(v, key).value for key in sorted(v.__slots__) if getattr(v, key) is not None],
            [val for key, val in sorted(reference[k]["co2"].items())],
        )


async def prepend_flow_node(
    graph_mgr: PostgresGraphMgr,
    recipe_uid: uuid,
    nutrient_values: RawNutrientValuesPer100g = None,
    flow_type: Type[FoodProductFlowNode | FlowNode] = FoodProductFlowNode,
) -> FoodProductFlowNode:
    "Creates a minimal flow node to prepend as the new root node."
    if flow_type == FoodProductFlowNode:
        root_flow = await graph_mgr.upsert_node_by_uid(flow_type(nutrient_values=nutrient_values))
    else:
        root_flow = await graph_mgr.upsert_node_by_uid(flow_type())

    # add edge Root flow --> activitiy node
    await graph_mgr.add_edge(
        Edge(parent_uid=root_flow.uid, child_uid=recipe_uid, edge_type=EdgeTypeEnum.root_to_recipe)
    )
    return root_flow


async def append_flow_node(
    graph_mgr: PostgresGraphMgr,
    recipe_uid: uuid,
    nutrient_values: Optional[RawNutrientValuesPer100g] = None,
    ingredients_declaration: Optional[list[RawName]] = None,
    amount_in_original_source_unit: RawQuantity = None,
    flow_location: Optional[str] = None,
    flow_type: Type[FoodProductFlowNode | FlowNode] = FoodProductFlowNode,
) -> FoodProductFlowNode:
    "Creates a minimal flow node to append as the new root node."
    if flow_type == FoodProductFlowNode:
        appended_flow = await graph_mgr.upsert_node_by_uid(
            flow_type(
                nutrient_values=nutrient_values,
                ingredients_declaration=ingredients_declaration,
                amount_in_original_source_unit=amount_in_original_source_unit,
                flow_location=flow_location,
            )
        )
    else:
        appended_flow = await graph_mgr.upsert_node_by_uid(
            flow_type(
                amount_in_original_source_unit=amount_in_original_source_unit,
                flow_location=flow_location,
            )
        )

    # add edge activitiy node -> appended flow
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe_uid, child_uid=appended_flow.uid, edge_type=EdgeTypeEnum.link_to_subrecipe)
    )
    return appended_flow


@pytest.fixture(scope="package")
def event_loop() -> "asyncio.EventLoop":
    "Yield the asyncio event loop."
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture
async def setup_services() -> None:
    """Fixture to get a clean (resetted) db for testing."""
    service_locator = ServiceLocator()
    service_locator.recreate()
    service_provider = service_locator.service_provider

    postgres_db = service_provider.postgres_db

    await postgres_db.connect(schema="test_pg")
    await postgres_db.reset_db("test_pg")

    service_provider.messaging_service = None

    service_provider.node_service.load_every_inventory_nodes = True  # Load all inventory nodes for faster tests.
    await service_provider.init_caches()

    yield postgres_db, service_provider

    await service_provider.shutdown_services()


@pytest.fixture(scope="package", autouse=True)
def virtual_memory_lock() -> None:
    """Mocks virtual memory such that we do not accidentally exit tests because of lack of memory."""
    with mock.patch("psutil.virtual_memory") as mock_vm:
        mock_vm.return_value.percent = 30
        yield mock_vm


@pytest.fixture(scope="package", autouse=True)
def geolocation_api_mock(request: SubRequest) -> None:
    """Mocks the google geolocation API to return a fixed result for spain and switzerland."""

    def mocked_requests_get(url: str) -> httpx.Response:
        url_to_check = url.lower()

        if "z%c3%bcrich" in url_to_check or "zurich" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "zurich_google_maps_query_response.json",
            )
        elif "schweiz" in url_to_check or "switzerland" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "switzerland_google_maps_query_response.json",
            )
        elif "niederlande" in url_to_check or "netherlands" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "netherlands_google_maps_query_response.json",
            )
        elif "spain" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "spain_google_maps_query_response.json",
            )
        elif "china" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "china_google_maps_query_response.json",
            )
        else:
            raise NotImplementedError(f"Mocking not implemented for {url}")

        with open(json_sample_path) as response_json_sample_file:
            resp_json = json.load(response_json_sample_file)

        mock_response = httpx.Response(status_code=200, content=json.dumps(resp_json).encode("utf-8"))

        return mock_response

    print("Patching 'gap_filling_modules.location_gfm._google_api_lookup'")
    patched = mock.patch("gap_filling_modules.location_gfm._google_api_lookup", new=mocked_requests_get)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'gap_filling_modules.location_gfm._google_api_lookup'")

    request.addfinalizer(unpatch)


XML_FILES_MAPPING = {
    "wsdl": "wsdl.xml",
    "wsdl=1": "wsdl_1.xml",
    "xsd=1": "xsd_1.xml",
    "xsd=2": "xsd_2.xml",
}


@pytest.fixture(scope="package", autouse=True)
def ecotransit_wsdl_call_mock(request: SubRequest) -> None:
    """Mocks the EcoTransIT WSDL call to return a fixed XML schema file."""

    def mocked_wsdls_get(self: None, url: str) -> bytes:  # noqa `self` is for patching
        url_to_check = url.lower()
        schema_file_type = url_to_check.split("?")[-1]

        if schema_file_type not in XML_FILES_MAPPING:
            raise NotImplementedError(f"WSDL load mocking not implemented for {url}")

        xml_sample_path = os.path.join(
            SAMPLE_JSONS_DIR,
            XML_FILES_MAPPING[schema_file_type],
        )

        with open(xml_sample_path, "r") as response_xml_sample_file:
            mock_xml = bytes(response_xml_sample_file.read().encode("utf-8"))

        return mock_xml

    print("Patching 'zeep.transports.Transport._load_remote_data'")
    patched = mock.patch("zeep.transports.Transport._load_remote_data", new=mocked_wsdls_get)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'zeep.transports.Transport._load_remote_data'")

    request.addfinalizer(unpatch)


@pytest.fixture(scope="package", autouse=True)
def ecotransit_call_mock(request: SubRequest) -> None:
    """Mocks the EcoTransIT API call to return a fixed result in XML format."""

    def mocked_ecotransit_send_request(
        self: None, address: str, message: bytes, headers: dict  # noqa args needed for patching
    ) -> httpx.Response:
        departure = {}
        destination = {}
        mode = ""

        parsed_message = etree.fromstring(message)
        root = parsed_message.getroottree().getroot()

        for section in root.iter():
            if "wgs84" in section.tag:
                if section.prefix == "ns1":
                    # departure coords
                    departure = dict(section.attrib)
                elif section.prefix == "ns2":
                    # destination coords
                    destination = dict(section.attrib)

            elif section.prefix == "ns4":
                if "air" in section.tag:
                    mode = "air"
                elif "sea" in section.tag:
                    mode = "sea"
                elif "road" in section.tag:
                    mode = "road"

            if mode:
                break

        sample_response_xml_file = (
            f"{departure.get('longitude')}_{departure.get('latitude')}_"
            f"{destination.get('longitude')}_{destination.get('latitude')}_{mode}.xml"
        )
        xml_sample_path = os.path.join(
            SAMPLE_JSONS_DIR,
            ECOTRANSIT_RESPONSES_FOLDER,
            sample_response_xml_file,
        )

        if not os.path.isfile(xml_sample_path):
            print(f"WARNING: {xml_sample_path} does not exist. Return 404 status.")
            return httpx.Response(status_code=404)

        with open(xml_sample_path, "r") as response_xml_sample_file:
            mock_response = httpx.Response(
                status_code=200, content=bytes(response_xml_sample_file.read().encode("utf-8"))
            )
        return mock_response

    print("Patching 'zeep.transports.Transport.post'")
    patched = mock.patch("zeep.transports.Transport.post", new=mocked_ecotransit_send_request)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'zeep.transports.Transport.post'")

    request.addfinalizer(unpatch)


@pytest_asyncio.fixture
async def get_term_access_group_uids(
    setup_services: Tuple[PostgresDb, ServiceProvider],
) -> TermAccessGroupIdsRType:
    """Gets access group UUIDs of groups that contain glossary terms."""
    pg_db, service_provider = setup_services
    settings = PgSettings()
    term_mgr = pg_db.get_term_mgr()

    eaternity_term_group_uid = ""
    eurofir_term_group_uid = ""
    foodex2_term_group_uid = ""
    nutrient_subdivision_term_group_uid = ""
    perishability_term_group_uid = ""
    transport_term_group_uid = ""

    groups = await term_mgr.find_all_term_access_groups()

    for group in groups:
        if "namespace eaternity" in group.data.get("name"):
            eaternity_term_group_uid = group.uid
        if "EuroFIR" in group.data.get("name"):
            eurofir_term_group_uid = group.uid
        if "FoodEx2" in group.data.get("name"):
            foodex2_term_group_uid = group.uid
        if "Nutrients Subdivision" in group.data.get("name"):
            nutrient_subdivision_term_group_uid = group.uid
        if "Perishability" in group.data.get("name"):
            perishability_term_group_uid = group.uid
        if "Transport" in group.data.get("name"):
            transport_term_group_uid = group.uid

    return (
        eaternity_term_group_uid,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        perishability_term_group_uid,
        transport_term_group_uid,
        settings.EATERNITY_NAMESPACE_UUID,
    )


@pytest_asyncio.fixture
async def load_seeded_nutrients(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> dict[str, dict[str, float]]:
    "Load seeded nutrients."
    postgres_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    term_mgr: PgTermMgr = postgres_db.get_term_mgr()

    nutrients_to_load = {
        "onion": "A1480",
        "carrot": "A1791",
        "potato": "A00ZT",
        "tomato": "A0DMX",
        "egg": "A0725",
        "chicken": "A01SP",
    }

    nutrients_eos = {}
    for term_name, xid in nutrients_to_load.items():
        term: Term = await term_mgr.get_term_by_xid_and_access_group_uid(xid, foodex2_term_group_uid)
        nutrients_term: Term = await service_provider.glossary_link_service.load_term_nutrients([term])
        assert nutrients_term.data["nutr-vals"], "nutrients should be loaded"
        nutrients_eos[term_name] = transform_eurofir(nutrients_term.data["nutr-vals"])

    return nutrients_eos


@pytest_asyncio.fixture
async def mock_cooling_process(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> None:
    "Load seeded nutrients."
    postgres_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    graph_mgr = postgres_db.get_graph_mgr()

    # add the invented cooling process
    cooled_term = await postgres_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0131", foodex2_term_group_uid)

    cooling_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="GLO",
            production_amount={"value": 1, "unit": "kg*day"},
            id="('ecoinvent 3.6 cutoff', '001decfdb6f2319cd8578e05c91c247d')",
            key=[
                "ecoinvent 3.6 cutoff",
                "001decfdb6f2319cd8578e05c91c247d",
            ],
            flow=None,
            name="market for operation, reefer, cooling",
            type="process",
            database="ecoinvent 3.6 cutoff",
            filename=None,
            reference_product="market for operation, reefer, cooling",
        )
    )

    await postgres_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[cooled_term.uid],
            linked_node_uid=cooling_process.uid,
        )
    )
    methane_emission = await graph_mgr.find_by_uid(
        postgres_db.service_provider.product_service.xid_to_uid_mappings[
            "biosphere3_0795345f-c7ae-410c-ad25-1845784c75f5"
        ]
    )

    co2_emission = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
            name="Carbon dioxide, fossil",
            type="emission",
            categories=["air"],
        )
    )
    co2_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
        co2_emission.uid,
    )
    await service_provider.product_service.bulk_insert_xid_uid_mappings([co2_mapping])

    await add_flow(graph_mgr, cooling_process, methane_emission, 0.001, "biosphere")
    await add_flow(graph_mgr, cooling_process, co2_emission, 0.005, "biosphere")


async def add_flow(
    graph_mgr: PostgresGraphMgr,
    from_: Node,
    to: Node,
    amount: float,
    activity_type: str = "technosphere",
    make_link_to_sub_node: bool = False,
    name_of_flow: Optional[str] = None,
    nutrient_values: Optional[RawNutrientValuesPer100g] = None,
) -> FlowNode:
    """Helper to add a flow (as a Node) and 2 Edges between them.

    E.g. from carott_process to tractor_process.
    activity_type: one of biosphere, technosphere.
    """
    sphere_activity = activity_type in (
        "technosphere",
        "biosphere",
    )

    #  "unit": "gram" # default should be gram according to old api docs
    if isinstance(to, ActivityNode) and to.production_amount:
        if isinstance(to.production_amount, QuantityProp):
            destination_unit: str = to.production_amount.get_unit_term().name
        else:
            destination_unit: str = to.production_amount.unit if to.production_amount.unit else "gram"
    else:
        destination_unit: str = "gram"

    if activity_type == FoodProductFlowNode.__name__:
        node_class = FoodProductFlowNode
    else:
        node_class = FlowNode

    if make_link_to_sub_node:
        link_to_sub_node = LinkToUidProp(uid=to.uid)
    else:
        link_to_sub_node = None

    kwargs_dict = {
        "amount_in_original_source_unit": {"value": amount, "unit": destination_unit},
        "type": activity_type,
        "link_to_sub_node": link_to_sub_node,
        "product_name": name_of_flow,
    }
    if nutrient_values:
        kwargs_dict["nutrient_values"] = nutrient_values

    flow: FlowNode = cast(FlowNode, await graph_mgr.upsert_node_by_uid(node_class(**kwargs_dict)))

    await graph_mgr.add_edge(
        Edge(
            parent_uid=from_.uid,
            child_uid=flow.uid,
            edge_type=EdgeTypeEnum.lca_link if sphere_activity else EdgeTypeEnum.link_to_subrecipe,
        )
    )

    if link_to_sub_node:
        await graph_mgr.add_edge(
            Edge(
                parent_uid=flow.uid,
                child_uid=link_to_sub_node.uid,
                edge_type=EdgeTypeEnum.link_to_subrecipe,
            )
        )

    if not (node_class == FoodProductFlowNode and make_link_to_sub_node):
        await graph_mgr.add_edge(
            Edge(
                parent_uid=flow.uid,
                child_uid=to.uid,
                edge_type=EdgeTypeEnum.lca_link if sphere_activity else EdgeTypeEnum.link_to_subrecipe,
            )
        )
    return flow


@pytest_asyncio.fixture
async def create_minimum_recipe_with_declaration(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeWithDeclaration:
    """Creates a minimum recipe with lots of dummy ingredients in its ingredients declaration."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )
    root_flow = await prepend_flow_node(graph_mgr, recipe.uid)
    ingredients_declaration = [
        {
            "language": "de",
            "value": "Hähnchenbrustfilet (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, "
            "Speisesalz, Stärke, Maltodextrin, Milcheiweiß, Pflanzeneiweiß, Dextrose, "
            "Zucker, Gewürzextrakte, Geschmacksverstärker: E 620",
        },
    ]
    sub_flow = await append_flow_node(
        graph_mgr,
        recipe.uid,
        nutrient_values=RawNutrientValuesPer100g(carbohydrates_gram=100),
        ingredients_declaration=ingredients_declaration,
    )

    return MinimumRecipeWithDeclaration(pg_db, service_provider, recipe, root_flow, sub_flow)


@pytest_asyncio.fixture
async def create_minimum_recipe_with_small_declaration(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeWithDeclaration:
    """Creates a minimum recipe with just a carrot in its ingredients declaration."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # get the terms' nutrients (these were seeded in pg_term_mgr.py)
    term_mgr: PgTermMgr = pg_db.get_term_mgr()

    carrot = await term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)

    carrot_nutrients_term: Term = await service_provider.glossary_link_service.load_term_nutrients([carrot])
    assert carrot_nutrients_term.data["nutr-vals"], "nutrients should be loaded"
    carrot_nutrients_kale = transform_eurofir(carrot_nutrients_term.data["nutr-vals"])

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 1000},
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid, nutrient_values=carrot_nutrients_kale)

    ingredients_declaration = [
        {
            "language": "de",
            "value": "Karotten",
        },
    ]
    sub_flow = await append_flow_node(graph_mgr, recipe.uid, ingredients_declaration=ingredients_declaration)
    return MinimumRecipeWithDeclaration(pg_db, service_provider, recipe, root_flow, sub_flow)


@pytest_asyncio.fixture
async def create_minimum_recipe_with_small_declaration_and_dried_ingredient(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeWithDeclaration:
    """Creates a minimum recipe with just a carrot in its ingredients declaration."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # get the terms' nutrients (these were seeded in pg_term_mgr.py)
    term_mgr: PgTermMgr = pg_db.get_term_mgr()

    carrot = await term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)
    dried_term = await term_mgr.get_term_by_xid_and_access_group_uid("J0116", foodex2_term_group_uid)

    carrot_nutrients_term: Term = await service_provider.glossary_link_service.load_term_nutrients([carrot, dried_term])
    assert carrot_nutrients_term.data["nutr-vals"], "nutrients should be loaded"
    carrot_nutrients_kale = transform_eurofir(carrot_nutrients_term.data["nutr-vals"])

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 1000},
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
        add_dried_carrot=True,
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid, nutrient_values=carrot_nutrients_kale)

    ingredients_declaration = [
        {
            "language": "de",
            "value": "getrockene Karotten",
        },
    ]
    sub_flow = await append_flow_node(graph_mgr, recipe.uid, ingredients_declaration=ingredients_declaration)
    return MinimumRecipeWithDeclaration(pg_db, service_provider, recipe, root_flow, sub_flow)


@pytest_asyncio.fixture
async def create_minimum_recipe_with_small_declaration_and_frozen_ingredient(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeWithDeclaration:
    """Creates a minimum recipe with just a carrot in its ingredients declaration."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # get the terms' nutrients (these were seeded in pg_term_mgr.py)
    term_mgr: PgTermMgr = pg_db.get_term_mgr()

    carrot = await term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)
    frozen_term = await term_mgr.get_term_by_xid_and_access_group_uid("J0136", foodex2_term_group_uid)

    carrot_nutrients_term: Term = await service_provider.glossary_link_service.load_term_nutrients(
        [
            carrot,
            frozen_term,
        ],
    )
    assert carrot_nutrients_term.data["nutr-vals"], "nutrients should be loaded"
    carrot_nutrients_kale = transform_eurofir(carrot_nutrients_term.data["nutr-vals"])

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 1000},
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
        add_freezing_transport_lci=True,
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid, nutrient_values=carrot_nutrients_kale)

    ingredients_declaration = [
        {
            "language": "de",
            "value": "frozen Karotten",
        },
    ]
    sub_flow = await append_flow_node(graph_mgr, recipe.uid, ingredients_declaration=ingredients_declaration)
    return MinimumRecipeWithDeclaration(pg_db, service_provider, recipe, root_flow, sub_flow)


async def insert_processing_xid_to_uid_mappings(service_provider: ServiceProvider) -> None:
    async def replicate_brightway_node(node_uid: uuid.UUID) -> uuid.UUID:
        # TODO Write a recursive function.

        orig_node = await service_provider.node_service.find_by_uid(node_uid)
        replica_node = orig_node.model_copy(deep=True)
        edges = await service_provider.node_service.get_sub_graph_by_uid(replica_node.uid, max_depth=1)
        replica_node.uid = uuid.uuid4()
        await service_provider.node_service.upsert_node_by_uid(replica_node)
        child_uids = [edge.child_uid for edge in edges]
        for child_uid in child_uids:
            orig_child_node = await service_provider.node_service.find_by_uid(child_uid)
            replica_child_node = orig_child_node.model_copy(deep=True)
            child_edges = await service_provider.node_service.get_sub_graph_by_uid(replica_child_node.uid, max_depth=1)
            replica_child_node.uid = uuid.uuid4()
            await service_provider.node_service.upsert_node_by_uid(replica_child_node)
            await service_provider.node_service.add_edge(
                Edge(parent_uid=replica_node.uid, child_uid=replica_child_node.uid, edge_type=EdgeTypeEnum.lca_link)
            )
            grandchild_uids = [edge.child_uid for edge in child_edges]
            for grandchild_uid in grandchild_uids:
                await service_provider.node_service.add_edge(
                    Edge(parent_uid=replica_child_node.uid, child_uid=grandchild_uid, edge_type=EdgeTypeEnum.lca_link)
                )

        return replica_node.uid

    process_uids = {
        "region_unspecified": service_provider.product_service.xid_to_uid_mappings["EDB_region_unspecified_process"],
        "region_unspecified_with_raw": service_provider.product_service.xid_to_uid_mappings[
            "EDB_region_unspecified_with_raw_material"
        ],
        "region_specific": service_provider.product_service.xid_to_uid_mappings["EDB_region_specific_process"],
    }
    processing_gfm_factory = ProcessingGapFillingFactory(service_provider.postgres_db, service_provider)
    await processing_gfm_factory.init_cache()  # necessary to load the trigger tags
    process_xids = {"region_unspecified": [], "region_unspecified_with_raw": [], "region_specific": []}
    for process, process_infos in processing_gfm_factory.trigger_tags_by_process.items():
        # TODO Add more processes here:
        if process in ("Drying", "Dried Fruit Grinding", "Transportation", "Heating", "Carbonating", "Smoking"):
            process_xids["region_unspecified"].extend([process_info.xid for process_info in process_infos])
        elif process in ("Baking", "Jam production", "Juice production"):
            process_xids["region_unspecified_with_raw"].extend([process_info.xid for process_info in process_infos])
        elif process in ("Yoghurt production", "Roasting", "Chopping"):
            process_xids["region_specific"].extend([process_info.xid for process_info in process_infos])

    for process_type in ("region_specific", "region_unspecified", "region_unspecified_with_raw"):
        await service_provider.product_service.bulk_insert_xid_uid_mappings(
            [
                (
                    PgSettings().EATERNITY_NAMESPACE_UUID,
                    process_xid,
                    await replicate_brightway_node(process_uids[process_type]),
                )
                for process_xid in process_xids[process_type]
            ]
        )


@pytest_asyncio.fixture
async def create_minimum_recipe_without_amounts(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeRType:
    """Creates a minimum recipe without amounts."""
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        _,
        _,
        transport_term_group_uid,
        default_namespace_uid,
    ) = get_term_access_group_uids

    graph_mgr = pg_db.get_graph_mgr()

    term_mgr: PgTermMgr = pg_db.get_term_mgr()

    carrot = await term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)

    carrot_nutrients_term: Term = await service_provider.glossary_link_service.load_term_nutrients([carrot])
    dried_carrot_nutrients_term: Term = await term_mgr.get_term_by_xid_and_access_group_uid(
        "132", eurofir_term_group_uid
    )
    carrot_nutrients_kale = transform_eurofir(carrot_nutrients_term.data["nutr-vals"])
    dried_carrot_nutrients_kale = transform_eurofir(dried_carrot_nutrients_term.data["nutr-vals"])

    mixed_nutrients = {}
    for key in carrot_nutrients_kale:
        mixed_nutrients[key] = carrot_nutrients_kale[key] * 0.5 + dried_carrot_nutrients_kale[key] * 0.5

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 150, "unit": "gram"},
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    # Delete fibers information from nutrient declaration to:
    # 1. Keep old test benchmarks.
    # 2. It is not required by law to provide fiber information on food items.
    if "fibers_gram" in mixed_nutrients:
        del mixed_nutrients["fibers_gram"]

    root_flow = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            # the amount is bascially the demand
            amount_in_original_source_unit={"value": 2, "unit": "production amount"},
            nutrient_values=mixed_nutrients,
            # do no not use link, otherwise add-client-node-gfm will add the recipe a second time
            # link_to_sub_node=LinkToUidProp(uid=recipe.uid),
        )
    )

    # create one ingredient
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="ES",
            product_name=[{"language": "de", "value": "Karotten"}],
            type="conceptual-ingredients",
            raw_processing={"value": "freezing", "language": "en"},
            raw_labels={"value": "Migros Bio Ausland", "language": "de"},
            packaging="plastic",
        )
    )

    # add edge recipe --> carrot_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=carrot_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # add edge Root flow --> activitiy node
    await graph_mgr.add_edge(
        Edge(parent_uid=root_flow.uid, child_uid=recipe.uid, edge_type=EdgeTypeEnum.root_to_recipe)
    )

    carrot_process = await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
        default_namespace_uid,
        add_freezing_transport_lci=True,
    )

    return MinimumRecipeRType(pg_db, service_provider, recipe, carrot_ingredient, carrot_process, root_flow)


@pytest_asyncio.fixture
async def create_minimum_recipe(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeRType:
    """Creates a minimum recipe."""
    return await create_minimum_recipe_or_supply(setup_services, get_term_access_group_uids, FoodProcessingActivityNode)


@pytest_asyncio.fixture
async def create_minimum_supply(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumSupplyRType:
    """Creates a minimum supply."""
    return await create_minimum_recipe_or_supply(setup_services, get_term_access_group_uids, SupplySheetActivityNode)


async def create_minimum_recipe_or_supply(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
    child_of_root_node_type: Type[FoodProcessingActivityNode | SupplySheetActivityNode],
) -> MinimumRecipeRType | MinimumSupplyRType:
    """Creates a minimum recipe or supply."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids

    graph_mgr = pg_db.get_graph_mgr()

    recipe_or_supply = await graph_mgr.upsert_node_by_uid(
        child_of_root_node_type(
            activity_location="Zürich Schweiz",
            production_amount={"value": 150, "unit": "gram"},
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
            # do not put "nutrient_values" here, so that ingredient_amount_estimator_gfm is not scheduled
        )
    )

    # Add FoodProductFlowNode as root such that water scarcity and rainforest rating can be aggregated.
    root_flow = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            # the amount is bascially the demand
            amount_in_original_source_unit={"value": 2, "unit": "production amount"}
            # do no not use link, otherwise add-client-node-gfm will add the recipe_or_supply a second time
            # link_to_sub_node=LinkToUidProp(uid=recipe_or_supply.uid),
        )
    )

    # create one ingredient
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="ES",
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": 0.150, "unit": "kilogram"},
            type="conceptual-ingredients",
            raw_labels={"value": "Migros Bio Ausland", "language": "de"},
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    # add edge recipe_or_supply --> carrot_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe_or_supply.uid, child_uid=carrot_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # add edge Root flow --> activitiy node
    await graph_mgr.add_edge(
        Edge(parent_uid=root_flow.uid, child_uid=recipe_or_supply.uid, edge_type=EdgeTypeEnum.root_to_recipe)
    )

    carrot_process = await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
        default_namespace_uid,
        add_freezing_transport_lci=True,
    )

    return MinimumRecipeRType(pg_db, service_provider, recipe_or_supply, carrot_ingredient, carrot_process, root_flow)


@pytest_asyncio.fixture
async def create_minimum_fish_recipe_with_fresh_and_air_transport_from_spain(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumSupplyRType:
    """Creates a minimum fish recipe with known origin and fresh."""
    return await create_minimum_fish_recipe_with_given_configuration(
        setup_services, get_term_access_group_uids, "fresh", "air", "ES"
    )


@pytest_asyncio.fixture
async def create_minimum_fish_recipe_with_fresh_unknown_origin(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumSupplyRType:
    """Creates a minimum fish recipe with unknown origin and fresh."""
    return await create_minimum_fish_recipe_with_given_configuration(
        setup_services, get_term_access_group_uids, "fresh", "", ""
    )


async def create_minimum_fish_recipe_with_given_configuration(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
    conservation: str = "fresh",
    transport: str = "air",
    location: str = "ES",
) -> MinimumRecipeRType | MinimumSupplyRType:
    """Creates a minimum fish recipe."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids

    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 150, "unit": "gram"},
            titles=[{"language": "de", "value": "Ein Fish-Rezept"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
        )
    )

    # Add FoodProductFlowNode as root such that water scarcity and rainforest rating can be aggregated.
    root_flow = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            # the amount is bascially the demand
            amount_in_original_source_unit={"value": 2, "unit": "production amount"}
            # do no not use link, otherwise add-client-node-gfm will add the recipe_or_supply a second time
            # link_to_sub_node=LinkToUidProp(uid=recipe_or_supply.uid),
        )
    )

    # create one ingredient
    tuna_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport=transport,
            flow_location=location,
            product_name=[{"language": "de", "value": "Thunfisch"}],
            amount_in_original_source_unit={"value": 0.150, "unit": "kilogram"},
            type="conceptual-ingredients",
            raw_labels={"value": "Migros Bio Ausland", "language": "de"},
            processing="raw",
            raw_conservation={"value": conservation, "language": "en"},
            packaging="plastic",
        )
    )

    # add edge recipe --> tuna_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=tuna_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # add edge Root flow --> activitiy node
    await graph_mgr.add_edge(
        Edge(parent_uid=root_flow.uid, child_uid=recipe.uid, edge_type=EdgeTypeEnum.root_to_recipe)
    )

    tuna_process = await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
        default_namespace_uid,
        add_freezing_transport_lci=True,
    )

    return MinimumRecipeRType(pg_db, service_provider, recipe, tuna_ingredient, tuna_process, root_flow)


async def seed_processing_gfm_cache_table(service_provider: ServiceProvider) -> None:
    file_path = os.path.join(SAMPLE_JSONS_DIR, "processing_data.csv")
    processing_settings = ProcessingSettings()
    dataframe_for_processing = ProcessingGapFillingFactory.get_dataframe_from_local_folder(file_path)
    if dataframe_for_processing is None:
        print(f"WARNING: {file_path} does not exist. Trying to download it from google-drive.")
        dataframe_for_processing = ProcessingGapFillingFactory.get_dataframe_from_gdrive(processing_settings)
        if dataframe_for_processing is None:
            raise ValueError(
                f"Dataframe for processing not found in {file_path} and downloading it from gdrive did not succeed."
            )
        else:
            dataframe_for_processing.to_csv(file_path, index=False)
    await ProcessingGapFillingFactory.set_process_trigger_tags_cache_table_and_add_matchings(
        dataframe_for_processing, processing_settings, service_provider
    )


async def seed_processing(pg_db: PostgresDb, service_provider: ServiceProvider) -> None:
    await seed_processing_gfm_cache_table(service_provider)
    methane_emission_uid = service_provider.product_service.xid_to_uid_mappings[
        "biosphere3_0795345f-c7ae-410c-ad25-1845784c75f5"
    ]
    # seed sample processing data.
    country_codes_for_additional_low_voltage_electricity_processes = ["ES", "CH"]
    (
        region_unspecified_example_process,
        region_unspecified_with_raw_material_example_process,
        region_specific_example_process,
        raw_material_process,
        low_voltage_electricity_process,
        additional_electricity_mixes,
    ) = await pg_db.graph_mgr.seed_sample_processing_data(
        methane_emission_uid, country_codes_for_additional_low_voltage_electricity_processes
    )

    await service_provider.product_service.bulk_insert_xid_uid_mappings(
        [
            (
                PgSettings().EATERNITY_NAMESPACE_UUID,
                process_key,
                process_uid,
            )
            for process_key, process_uid in {
                "EDB_region_unspecified_process": region_unspecified_example_process.uid,
                "EDB_region_unspecified_with_raw_material": region_unspecified_with_raw_material_example_process.uid,
                "EDB_region_specific_process": region_specific_example_process.uid,
                "EDB_53a561ae842a44da824a49577075522d": raw_material_process.uid,
                "ecoinvent 3.6 cutoff_14e00ca0088efa00a0a436c070eb10bb": low_voltage_electricity_process.uid,
            }.items()
        ]
    )
    await insert_processing_xid_to_uid_mappings(service_provider)

    for electricity_mix in additional_electricity_mixes + [low_voltage_electricity_process]:
        await pg_db.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
            gfm_name="ElectricityMarketLowVoltage",
            cache_key=electricity_mix.activity_location,
            cache_data=str(electricity_mix.uid),
            load_on_boot=True,
        )


@pytest_asyncio.fixture
async def create_recipe_without_amounts_for_processing(
    create_minimum_recipe_without_amounts: MinimumRecipeRType,
) -> MinimumRecipeRType:
    """Creates a minimum recipe without amounts for processing GFM tests."""
    (
        pg_db,
        service_provider,
        recipe,
        carrot_ingredient,
        carrot_process,
        root_flow,
    ) = create_minimum_recipe_without_amounts
    await seed_processing(pg_db, service_provider)
    return MinimumRecipeRType(pg_db, service_provider, recipe, carrot_ingredient, carrot_process, root_flow)


@pytest_asyncio.fixture
async def create_minimum_recipe_for_processing(
    create_minimum_recipe: MinimumRecipeRType,
) -> MinimumRecipeRType:
    """Creates a minimum recipe for processing GFM tests."""
    (pg_db, service_provider, recipe, carrot_ingredient, carrot_process, root_flow) = create_minimum_recipe
    await seed_processing(pg_db, service_provider)
    return MinimumRecipeRType(pg_db, service_provider, recipe, carrot_ingredient, carrot_process, root_flow)


@pytest_asyncio.fixture
async def create_convenience_product_for_processing(
    create_convenience_product: Tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node],
) -> Tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node]:
    """Creates a recipe with a convenience product."""
    pg_db, service_provider, root_recipe, convenience_product, carrot_ingredient, root_flow = create_convenience_product
    await seed_processing(pg_db, service_provider)
    return pg_db, service_provider, root_recipe, convenience_product, carrot_ingredient, root_flow


@pytest_asyncio.fixture
async def seed_processing_for_tests(
    setup_services: Tuple[PostgresDb, ServiceProvider],
) -> Tuple[PostgresDb, ServiceProvider, str]:
    pg_db, service_provider = setup_services
    term_mgr = pg_db.get_term_mgr()

    foodex2_term_group_uid = ""

    groups = await term_mgr.find_all_term_access_groups()

    for group in groups:
        if "FoodEx2" in group.data.get("name"):
            foodex2_term_group_uid = group.uid
    await seed_processing(pg_db, service_provider)
    return pg_db, service_provider, foodex2_term_group_uid


async def create_minimum_recipe_or_supply_with_tomato(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
    child_of_root_node_type: Type[FoodProcessingActivityNode | SupplySheetActivityNode],
) -> MinimumRecipeWithTomato | MinimumSupplyWithTomato:
    """Creates a minimum recipe or supply with a tomato and tomato process."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    recipe_or_supply = await graph_mgr.upsert_node_by_uid(
        child_of_root_node_type(
            activity_location="Zürich Schweiz",
            production_amount={"value": 150},
            activity_date="2023-06-20",
            titles=[{"language": "de", "value": "Eine leckere Tomaten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Tomaten gut kochen."}],
            # do not put "nutrient_values" here, so that ingredient_amount_estimator_gfm is not scheduled
        )
    )

    # create one ingredient
    tomato_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_transport="air",
            flow_location="NL",
            product_name=[{"language": "de", "value": "Tomaten"}],
            amount_in_original_source_unit={"value": 0.150, "unit": "kilogram"},
            type="conceptual-ingredients",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    # add edge recipe_or_supply --> tomato_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe_or_supply.uid, child_uid=tomato_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    tomato_process = await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
        add_greenhouse_lci=True,
    )
    root_flow = await prepend_flow_node(graph_mgr, recipe_or_supply.uid, flow_type=FlowNode)
    return MinimumRecipeWithTomato(
        pg_db, service_provider, recipe_or_supply, tomato_ingredient, tomato_process, root_flow
    )


@pytest_asyncio.fixture
async def create_minimum_recipe_with_tomato(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeWithTomato:
    """Creates a minimum recipe with a tomato and tomato process."""
    return await create_minimum_recipe_or_supply_with_tomato(
        setup_services, get_term_access_group_uids, FoodProcessingActivityNode
    )


@pytest_asyncio.fixture
async def create_minimum_supply_with_tomato(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumSupplyWithTomato:
    """Creates a minimum supply with a tomato and tomato process."""
    return await create_minimum_recipe_or_supply_with_tomato(
        setup_services, get_term_access_group_uids, SupplySheetActivityNode
    )


@pytest_asyncio.fixture
async def create_recipe_with_larger_depth(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeRType:
    """Create a top recipe containing sub recipe with a carrot.

    The transport chain is as follows:
    Spain (ingredient-origin) -> Switzerland (sub-recipe) -> Spain (top-recipe)
    """
    pg_db, service_provider, sub_recipe, carrot_ingredient, carrot_process, root_flow = create_minimum_recipe
    _ = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    top_recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Netherlands",
            production_amount={"value": 150, "unit": "gram"},
            titles=[{"language": "de", "value": "A top recipe that is prepared in Netherlands"}],
            author="Eckart Witzigmann",
            date="2013-11-17",
            instructions=[{"language": "de", "value": "Repackage the sub-recipe in this product."}],
        )
    )
    await add_flow(
        graph_mgr,
        top_recipe,
        sub_recipe,
        amount=170.0,  # assume 20g production loss
        activity_type=FoodProductFlowNode.__name__,
        make_link_to_sub_node=True,
    )

    top_root_flow = await prepend_flow_node(graph_mgr, top_recipe.uid)

    return pg_db, service_provider, top_recipe, sub_recipe, carrot_ingredient, carrot_process, top_root_flow


@pytest_asyncio.fixture
async def create_convenience_product(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node]:
    """Create a top recipe containing a convenience product."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # Define a recipe that is prepared in the kitchen location
    root_recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 300, "unit": "gram"},
            titles=[{"language": "de", "value": "A recipe is prepared in the kitchen location"}],
            author="Eckart Witzigmann",
            date="2013-11-17",
        )
    )

    # this has as an ingredient a convenience product produced in the Netherlands
    convenience_product_recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Schweiz",
            production_amount={"value": 300, "unit": "gram"},
            titles=[{"language": "de", "value": "A convenience product that is prepared in Switzerland"}],
            author="Eckart Witzigmann",
            date="2013-11-17",
        )
    )
    convenience_product = await add_flow(
        graph_mgr,
        root_recipe,
        convenience_product_recipe,
        amount=300,
        activity_type=FoodProductFlowNode.__name__,
        make_link_to_sub_node=True,
        name_of_flow=[{"language": "de", "value": "Potato-carrot-combined-product"}],
    )

    # create two ingredients of the convenience product

    # ingredient 1: carrot
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="ES",
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": 0.150, "unit": "kilogram"},
            type="conceptual-ingredients",
            raw_labels={"value": "Migros Bio Ausland", "language": "de"},
            processing="raw",
            packaging="plastic",
        )
    )

    await graph_mgr.add_edge(
        Edge(
            parent_uid=convenience_product_recipe.uid,
            child_uid=carrot_ingredient.uid,
            edge_type=EdgeTypeEnum.ingredient,
        )
    )

    await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
    )

    # ingredient 2: potato
    potato_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_transport="air",
            flow_location="ES",
            product_name=[{"language": "de", "value": "Kartoffeln"}],
            amount_in_original_source_unit={"value": 0.150, "unit": "kilogram"},
            type="conceptual-ingredients",
            processing="raw",
            packaging="plastic",
        )
    )

    # add edge recipe --> potato_ingredient
    await graph_mgr.add_edge(
        Edge(
            parent_uid=convenience_product_recipe.uid,
            child_uid=potato_ingredient.uid,
            edge_type=EdgeTypeEnum.ingredient,
        )
    )

    root_flow = await prepend_flow_node(graph_mgr, root_recipe.uid)

    return pg_db, service_provider, root_recipe, convenience_product, carrot_ingredient, root_flow


@pytest_asyncio.fixture
async def create_convenience_product_with_root_flow(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node]:
    """Create a top recipe containing a convenience product."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # Define a recipe that is prepared in the kitchen location
    root_recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 300, "unit": "gram"},
            titles=[{"language": "de", "value": "A recipe is prepared in the kitchen location"}],
            author="Eckart Witzigmann",
            date="2013-11-17",
        )
    )

    root_flow_of_convenience_product = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            amount_in_original_source_unit={"value": 300, "unit": "gram"},
            type="technosphere",
            flow_location="Schweiz",
            titles=[{"language": "de", "value": "The root-flow of a convenience product"}],
        )
    )

    # this has as an ingredient a convenience product produced in th{"value": 150}e Netherlands
    convenience_product_recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Schweiz",
            production_amount={"value": 300, "unit": "gram"},
            titles=[{"language": "de", "value": "A convenience product that is prepared in Switzerland"}],
            author="Eckart Witzigmann",
            date="2013-11-17",
        )
    )

    await graph_mgr.add_edge(
        Edge(
            parent_uid=root_flow_of_convenience_product.uid,
            child_uid=convenience_product_recipe.uid,
            edge_type=EdgeTypeEnum.ingredient,
        )
    )

    convenience_product = await add_flow(
        graph_mgr,
        root_recipe,
        root_flow_of_convenience_product,
        amount=300,
        activity_type=FoodProductFlowNode.__name__,
        make_link_to_sub_node=True,
        name_of_flow=[{"language": "de", "value": "Potato-carrot-combined-product"}],
    )

    # create two ingredients of the convenience product

    # ingredient 1: carrot
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="ES",
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": 0.150, "unit": "kilogram"},
            type="conceptual-ingredients",
            raw_labels={"value": "Migros Bio Ausland", "language": "de"},
            processing="raw",
            packaging="plastic",
        )
    )

    await graph_mgr.add_edge(
        Edge(
            parent_uid=convenience_product_recipe.uid,
            child_uid=carrot_ingredient.uid,
            edge_type=EdgeTypeEnum.ingredient,
        )
    )

    await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
    )

    # ingredient 2: potato
    potato_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_transport="air",
            flow_location="ES",
            product_name=[{"language": "de", "value": "Kartoffeln"}],
            amount_in_original_source_unit={"value": 0.150, "unit": "kilogram"},
            type="conceptual-ingredients",
            processing="raw",
            packaging="plastic",
        )
    )

    # add edge recipe --> potato_ingredient
    await graph_mgr.add_edge(
        Edge(
            parent_uid=convenience_product_recipe.uid,
            child_uid=potato_ingredient.uid,
            edge_type=EdgeTypeEnum.ingredient,
        )
    )

    root_flow = await prepend_flow_node(graph_mgr, root_recipe.uid)

    return pg_db, service_provider, root_recipe, convenience_product, carrot_ingredient, root_flow


@pytest_asyncio.fixture
async def create_bw_graph_with_larger_depth(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> Tuple[PostgresDb, ServiceProvider, ModeledActivityNode]:
    """Create a recipe containing two brightway processes.

    These brightway processes link to the the carrot ingredient.
    """
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # create an auxillary root node
    top_recipe = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            titles=[{"value": "Top Db name"}],
            date="2013-10-14",
        )
    )

    # create two brightway processes
    bwp_1 = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            production_amount={"unit": "kilogram", "value": 1.0},
            titles=[{"value": "Db name"}],
            date="2013-10-14",
        )
    )

    await add_flow(graph_mgr, top_recipe, bwp_1, 1.0)

    bwp_2 = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            titles=[{"value": "Db name"}],
            date="2013-10-14",
        )
    )

    await add_flow(graph_mgr, top_recipe, bwp_2, 1.0)

    # create carrot ingredient and link it to the two brightway proccesses
    carrot_flow_1 = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="spain",
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": AMOUNT_BRIGHTWAY_PROCESS_1, "unit": "kilogram"},
            type="brightway_process",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    await graph_mgr.add_edge(Edge(parent_uid=bwp_1.uid, child_uid=carrot_flow_1.uid, edge_type=EdgeTypeEnum.lca_link))

    carrot_flow_2 = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="spain",
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": AMOUNT_BRIGHTWAY_PROCESS_2, "unit": "kilogram"},
            type="brightway_process",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    await graph_mgr.add_edge(Edge(parent_uid=bwp_2.uid, child_uid=carrot_flow_2.uid, edge_type=EdgeTypeEnum.lca_link))

    await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
    )

    return pg_db, service_provider, top_recipe


@pytest_asyncio.fixture
async def create_recipe_with_two_layers_of_brightway_nodes_containing_location_information(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> Tuple[PostgresDb, ServiceProvider, Node, list, Node]:
    """Creates a recipe containing two brightway processes with location information.

    These brightway processes link to the the carrot ingredient.
    """
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # create an auxillary root node
    top_recipe = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            production_amount={"value": 1, "unit": "kilogram"},
            titles=[{"value": "Top Db name"}],
            date="2013-10-14",
        )
    )

    # create two brightway processes
    bwp_1 = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="Schweiz",
            production_amount={"value": 1, "unit": "kilogram"},
            titles=[{"value": "BW node 1"}],
            date="2013-10-14",
        )
    )

    await add_flow(graph_mgr, top_recipe, bwp_1, 1.0)

    bwp_2 = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="Netherlands",
            production_amount={"value": 1, "unit": "kilogram"},
            titles=[{"value": "Db name"}],
            date="2013-10-14",
        )
    )

    await add_flow(graph_mgr, bwp_1, bwp_2, 1.0)

    # add the top bw_node_uid and its child-flow to the list of uids for sub-bw-nodes that we want to compute
    edges = await graph_mgr.get_sub_graph_by_uid(bwp_1.uid, max_depth=1)
    activity_flow_uid_tuples_below_bw_nodes_to_compute = [(bwp_1.uid, edges[0].child_uid)]

    carrot_flow_2 = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="spain",
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": AMOUNT_BRIGHTWAY_PROCESS_2, "unit": "kilogram"},
            type="brightway_process",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    await graph_mgr.add_edge(Edge(parent_uid=bwp_2.uid, child_uid=carrot_flow_2.uid, edge_type=EdgeTypeEnum.lca_link))

    await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
    )

    root_flow = await prepend_flow_node(pg_db.get_graph_mgr(), top_recipe.uid)

    return pg_db, service_provider, top_recipe, activity_flow_uid_tuples_below_bw_nodes_to_compute, root_flow


async def create_tractor_dummy_lca(
    foodex2_term_group_uid: str,
    graph_mgr: PostgresGraphMgr,
    pg_db: PostgresDb,
    transport_term_group_uid: str,
    add_dried_carrot: bool = False,
    add_transport_lci: bool = True,
    add_freezing_transport_lci: bool = False,
    add_greenhouse_lci: bool = False,
) -> Node:
    "Create LCA process, will be imported from EDB/Brightway."
    carrot_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for carrot (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="carrot",
        )
    )
    # The linking from carrot_ingredient to carrot[Term] is performed by MatchProductName GFM
    # The linking from carrot[Term] to the carrot_process[Node] is performed by LinkTermToActivityNode GFM
    # Here we just add/seed with a glossary-link btw Carrot Term (FoodEx2) and Carrot process
    carrot_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)

    if add_dried_carrot:
        dried_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0116", foodex2_term_group_uid)
        await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[carrot_term.uid, dried_term.uid],
                linked_node_uid=carrot_process.uid,
            )
        )
    elif add_freezing_transport_lci:
        frozen_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0136", foodex2_term_group_uid)
        await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[carrot_term.uid, frozen_term.uid],
                linked_node_uid=carrot_process.uid,
            )
        )
    await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[carrot_term.uid],
            linked_node_uid=carrot_process.uid,
        )
    )

    # create another process and link it to the first one (carrot_process)
    tractor_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="CH",
            production_amount={"value": 1, "unit": "ton kilometer"},
            key=["ecoinvent 3.6 cutoff", "74ec4b7d70b94280921496781b013580"],
            flow="ba4b7781-39f2-465b-8d98-0bc1c4c82e61",
            name="transport, tractor and trailer, agricultural",
            type="process",
            reference_product="transport, tractor and trailer, agricultural",
        )
    )
    await add_flow(graph_mgr, carrot_process, tractor_process, 0.12)

    # add 2 emissions (methane and CO2) to above tractor process
    methane_emission = await graph_mgr.find_by_uid(
        pg_db.service_provider.product_service.xid_to_uid_mappings["biosphere3_0795345f-c7ae-410c-ad25-1845784c75f5"]
    )
    await add_flow(graph_mgr, tractor_process, methane_emission, 0.31, "biosphere")

    co2_emission = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
            name="Carbon dioxide, fossil",
            type="emission",
            categories=["air"],
        )
    )
    co2_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
        co2_emission.uid,
    )
    service_provider = ServiceLocator().service_provider
    await service_provider.product_service.bulk_insert_xid_uid_mappings([co2_mapping])
    await add_flow(graph_mgr, tractor_process, co2_emission, 1.2, "biosphere")
    # necessary for transportation & orchestration unit tests

    if add_transport_lci:
        truck_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid(
            "EOS_GROUND",
            transport_term_group_uid,
        )

        truck_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                activity_location="GLO",
                production_amount={"value": 1, "unit": "tonkm"},
                id="('EDB', '59e1cf02ff66424392fe9b1e893771a2')",
                key=[
                    "EDB",
                    "59e1cf02ff66424392fe9b1e893771a2",
                ],
                flow=None,
                name="market for transport infrastructure, freight, lorry, unspecified",
                type="process",
                database="EDB",
                filename=None,
                reference_product="transport infrastructure, freight, lorry, unspecified",
            )
        )

        await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[truck_term.uid],
                linked_node_uid=truck_process.uid,
            )
        )

        await add_flow(graph_mgr, truck_process, methane_emission, 0.001, "biosphere")
        await add_flow(graph_mgr, truck_process, co2_emission, 0.026, "biosphere")

        sea_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("EOS_SEA", transport_term_group_uid)

        sea_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                activity_location="GLO",
                production_amount={"value": 1.0, "unit": "ton kilometer"},
                key=[
                    "EDB",
                    "b9bbf27802ab228a297bf10e06c93059_copy1",
                ],
                flow="2741cea8-327f-4e0f-9401-b10858dc68f8",
                name="transport, freight, sea, container ship infrastructure",
                type="process",
                database="EDB",
                filename="5687441a-90e8-4727-8e19-58013da8a0b9_2741cea8-327f-4e0f-9401-b10858dc68f8.spold",
                reference_product="transport, freight, sea, container ship infrastructure",
            )
        )

        await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[sea_term.uid],
                linked_node_uid=sea_process.uid,
            )
        )

        await add_flow(graph_mgr, sea_process, methane_emission, 0.001, "biosphere")
        await add_flow(graph_mgr, sea_process, co2_emission, 0.001, "biosphere")

        air_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("EOS_AIR", transport_term_group_uid)

        air_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                activity_location="GLO",
                production_amount={"value": 1.0, "unit": "ton kilometer"},
                key=[
                    "EDB",
                    "55cebdd5bf3d450d8ca06490f54af5f3",
                ],
                flow=None,
                name="transport, freight, aircraft infrastructure, unspecified distances",
                type="process",
                database="EDB",
                filename=None,
                reference_product="transport, freight, aircraft infrastructure",
            )
        )

        await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[air_term.uid],
                linked_node_uid=air_process.uid,
            )
        )

        await add_flow(graph_mgr, air_process, methane_emission, 0.001, "biosphere")
        await add_flow(graph_mgr, air_process, co2_emission, 0.005, "biosphere")

    # the cooling terms have to be added always because they might also be relevant if cooling
    # was not explicitly declared in the recipe (e.g. for perishable products)
    cooled_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0131", foodex2_term_group_uid)

    cooling_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="GLO",
            production_amount={"value": 1, "unit": "kg*day"},
            id="('ecoinvent 3.6 cutoff', '001decfdb6f2319cd8578e05c91c247d')",
            key=[
                "ecoinvent 3.6 cutoff",
                "001decfdb6f2319cd8578e05c91c247d",
            ],
            flow=None,
            name="market for operation, reefer, cooling",
            type="process",
            database="ecoinvent 3.6 cutoff",
            filename=None,
            reference_product="market for operation, reefer, cooling",
        )
    )

    await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[cooled_term.uid],
            linked_node_uid=cooling_process.uid,
        )
    )

    await add_flow(graph_mgr, cooling_process, methane_emission, 0.001, "biosphere")
    await add_flow(graph_mgr, cooling_process, co2_emission, 0.005, "biosphere")

    if add_freezing_transport_lci:
        frozen_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0136", foodex2_term_group_uid)

        freezing_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                activity_location="GLO",
                production_amount={"value": 1, "unit": "kg*day"},
                id="('ecoinvent 3.6 cutoff', '5f5d6e8a800eeacf9b004d85e57d06bc')",
                key=[
                    "ecoinvent 3.6 cutoff",
                    "5f5d6e8a800eeacf9b004d85e57d06bc",
                ],
                flow=None,
                name="market for operation, reefer, freezing",
                type="process",
                database="ecoinvent 3.6 cutoff",
                filename=None,
                reference_product="market for operation, reefer, freezing",
            )
        )

        await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[frozen_term.uid],
                linked_node_uid=freezing_process.uid,
            )
        )

        await add_flow(graph_mgr, freezing_process, methane_emission, 0.002, "biosphere")
        await add_flow(graph_mgr, freezing_process, co2_emission, 0.010, "biosphere")

    if add_greenhouse_lci:
        # add the nodes necessary to for the greenhouse gfm
        await add_greenhouse_mock_nodes(pg_db, graph_mgr, co2_emission, "NL")
        await service_provider.product_service.update_all_mappings()

    return carrot_process


async def add_greenhouse_mock_nodes(
    pg_db: PostgresDb, graph_mgr: PostgresGraphMgr, co2_emission: ElementaryResourceEmissionNode, country_code: str
) -> None:
    """Helper function to add greenhouse mock nodes."""
    # all numbers are taken from the excel sheet for Netherlands to be able to compare
    # in the test in core against the excel sheet.
    # in the core test we chose NL instead of FR, because for NL heating of the greenhouse is necessary
    # and we can test the heating mix as well
    temperature_data = [
        278.45,
        278.55,
        279.45,
        282.75,
        286.25,
        288.75,
        290.75,
        290.95,
        288.85,
        285.25,
        281.75,
        278.35,
    ]
    solar_radiation_data = [19, 33, 69, 119, 149, 149, 149, 118, 83, 51, 22, 14]
    for data, identifier in zip(
        [temperature_data, solar_radiation_data], ["average_temperature", "average_solar_radiation"]
    ):
        await pg_db.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
            gfm_name="GreenhouseGapFillingWorker",
            cache_key=f"meteonorm_{identifier}_{country_code}",
            cache_data=data,
            load_on_boot=True,
        )

        # add the nodes necessary to for the greenhouse gfm
        greenhouse_glass_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                activity_location="GLO",
                production_amount={"value": 1.0, "unit": "square meter-year"},
                key=["ecoinvent 3.6 cutoff", "0ce659c3cfd443a38761058ee62e3f10"],
                flow=None,
                name="market for greenhouse, glass walls and roof",
                filename=None,
                reference_product="greenhouse, glass walls and roof",
            )
        )
        greenhouse_plastic_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                activity_location="GLO",
                production_amount={"value": 1.0, "unit": "square meter-year"},
                key=["ecoinvent 3.6 cutoff", "fddfe51c6959f41ac044089c3a892af7"],
                flow=None,
                name="market for greenhouse, plastic walls and roof",
                filename=None,
                reference_product="greenhouse, plastic walls and roof",
            )
        )

        uid_of_electricity = uuid.uuid4()
        electricity_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                uid=uid_of_electricity,
                activity_location=country_code,
                production_amount={"value": 1.0, "unit": "kilowatt hour"},
                key=["EDB", "fddfe51c6959f41ac044089c3a892af7"],
                flow=None,
                name="market for electricity, low voltage",
                filename=None,
                reference_product="electricity, low voltage",
            )
        )
        await pg_db.gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
            gfm_name="ElectricityMarketLowVoltage",
            cache_key=f"{country_code}",
            cache_data=str(uid_of_electricity),
            load_on_boot=True,
        )

        greenhouse_glass_process_mapping = (
            PgSettings().EATERNITY_NAMESPACE_UUID,
            "ecoinvent 3.6 cutoff_0ce659c3cfd443a38761058ee62e3f10",
            greenhouse_glass_process.uid,
        )

        greenhouse_plastic_process_mapping = (
            PgSettings().EATERNITY_NAMESPACE_UUID,
            "ecoinvent 3.6 cutoff_fddfe51c6959f41ac044089c3a892af7",
            greenhouse_plastic_process.uid,
        )

        # from excel sheet, GWP of glass per m2 and year
        await add_flow(graph_mgr, greenhouse_glass_process, co2_emission, 4.04, "biosphere")

        # from excel sheet, GWP of plastic per m2 and year
        await add_flow(graph_mgr, greenhouse_plastic_process, co2_emission, 2.72, "biosphere")

        # from excel sheet, GWP of electricity (for NL), GWP of total_electricity / total_electricity
        await add_flow(graph_mgr, electricity_process, co2_emission, 0.792940417365036, "biosphere")

        await pg_db.product_mgr.bulk_insert_xid_uid_mappings(
            [greenhouse_glass_process_mapping, greenhouse_plastic_process_mapping]
        )

        all_mappings = []

        for heating_xid_suffix in ("_copy1", "_copy2", "", "_copy3"):
            heating_process = await graph_mgr.upsert_node_by_uid(
                (
                    ModeledActivityNode(
                        activity_location="GLO",
                        production_amount={"value": 1.0, "unit": "megajoule"},
                        key=["EDB", f"4220cbaabca343c09b19415d5ab4079f{heating_xid_suffix}"],
                        name="market for heating mix",
                    )
                )
            )
            heating_process_mapping = (
                PgSettings().EATERNITY_NAMESPACE_UUID,
                f"EDB_4220cbaabca343c09b19415d5ab4079f{heating_xid_suffix}",
                heating_process.uid,
            )
            all_mappings.append(heating_process_mapping)
            await add_flow(
                graph_mgr, heating_process, co2_emission, 0.05, "biosphere"
            )  # cutoff value taken from javaland

        await pg_db.product_mgr.bulk_insert_xid_uid_mappings(all_mappings)


@pytest_asyncio.fixture
async def create_minimum_recipe_for_unit_weight_gfm(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> MinimumRecipeForUnitWeightGFM:
    """Create a minimum recipe with liquid ingredients and ingredients that have abstract unit declared ("piece").

    To test out Unit Weight Conversion GFM.
    """
    pg_db, service_provider = setup_services
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    # create an ingredient that has density declared in its Term
    apple_juice_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            product_name=[{"language": "de", "value": "Apfelsaft"}],
            amount_in_original_source_unit={"value": 293, "unit": "milliliter"},
            type="conceptual-ingredients",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    # add edge recipe --> apple_juice_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=apple_juice_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # create an ingredient that has no density declared in its Term
    orange_juice_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            product_name=[{"language": "de", "value": "Orangensaft"}],
            amount_in_original_source_unit={"value": 854, "unit": "ml"},
            type="conceptual-ingredients",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    # add edge recipe --> orange_juice_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=orange_juice_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # create an ingredient that has an invalid declared unit
    abstract_orange_juice_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            product_name=[{"language": "de", "value": "Orangensaft"}],
            amount_in_original_source_unit={"value": 666, "unit": "hells"},
            ype="conceptual-ingredients",
            rocessing="raw",
            onservation="fresh",
            ackaging="plastic",
        )
    )

    # add edge recipe --> abstract_orange_juice_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=abstract_orange_juice_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    # create one ingredient
    piece_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            product_name=[{"language": "de", "value": "Karotten"}],
            amount_in_original_source_unit={"value": 7, "unit": "pieces"},
            type="conceptual-ingredients",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    # add edge recipe --> piece_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=piece_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid)

    return MinimumRecipeForUnitWeightGFM(
        pg_db,
        service_provider,
        recipe,
        apple_juice_ingredient,
        orange_juice_ingredient,
        abstract_orange_juice_ingredient,
        piece_ingredient,
        root_flow,
    )


@pytest_asyncio.fixture
async def create_more_elaborate_recipe(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> ElaborateRecipe:
    """Creates a more elaborate recipe.

    based on the example in
    <https://link.springer.com/chapter/10.1007/978-94-015-9900-9_2>
    """
    pg_db, service_provider = setup_services
    eaternity_term_group_uid, _, _, _, _, _, _ = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            titles=[{"language": "en", "value": "A recipe to test LCA of electricity production"}],
            recipe_portions=10,
            production_portions=4,
        )
    )

    root_flow = await graph_mgr.upsert_node_by_uid(FoodProductFlowNode())

    # add edge Root flow --> activitiy node
    await graph_mgr.add_edge(
        Edge(parent_uid=root_flow.uid, child_uid=recipe.uid, edge_type=EdgeTypeEnum.root_to_recipe)
    )

    # create one ingredient
    electricity_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "en", "value": "electricity"}],
            amount_in_original_source_unit={
                "value": 1000,
                "unit": "kWh",
            },  # this becomes our reference flow / functional unit, used by matrix_calculation module
            type="conceptual-ingredients",
        )
    )

    # edge recipe --> ingredient
    await graph_mgr.add_edge(
        Edge(
            parent_uid=recipe.uid,
            child_uid=electricity_ingredient.uid,
            edge_type=EdgeTypeEnum.ingredient,
        )
    )

    electricity_production_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            production_amount={"value": 10, "unit": "kWh"},
            key=["test", "1"],
            name="production of electricity",
            type="process",
        )
    )

    electricity_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid(
        "EOS_electricity",
        eaternity_term_group_uid,
    )
    assert electricity_term
    await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[electricity_term.uid],
            linked_node_uid=electricity_production_process.uid,
        )
    )

    fuel_production_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            production_amount={"value": 100, "unit": "l"},
            key=["test", "2"],
            name="production of fuel",
            type="process",
        )
    )
    await add_flow(graph_mgr, electricity_production_process, fuel_production_process, 2)

    co2_process = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
            name="Carbon dioxide, fossil",
            type="emission",
            categories=["air"],
        )
    )
    service_provider.node_service.cache.add_node(co2_process)
    co2_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
        co2_process.uid,
    )
    await add_flow(graph_mgr, electricity_production_process, co2_process, 1, "biosphere")
    await add_flow(graph_mgr, fuel_production_process, co2_process, 10, "biosphere")

    sulfur_process = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "fd7aa71c-508c-480d-81a6-8052aad92646"],
            name="Sulfur dioxide",
            type="emission",
            categories=["air"],
        )
    )
    service_provider.node_service.cache.add_node(sulfur_process)
    sulfur_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_fd7aa71c-508c-480d-81a6-8052aad92646",
        sulfur_process.uid,
    )
    await add_flow(graph_mgr, electricity_production_process, sulfur_process, 0.1, "biosphere")
    await add_flow(graph_mgr, fuel_production_process, sulfur_process, 2, "biosphere")

    crude_oil_process = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "3"],
            name="crude oil",
            type="natural ressource",
            categories=["natural ressource"],
        )
    )
    service_provider.node_service.cache.add_node(crude_oil_process)
    crude_oil_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_3",
        crude_oil_process.uid,
    )
    await add_flow(graph_mgr, fuel_production_process, crude_oil_process, -50, "biosphere")
    await service_provider.product_service.bulk_insert_xid_uid_mappings(
        [crude_oil_mapping, co2_mapping, sulfur_mapping]
    )

    return ElaborateRecipe(
        pg_db,
        service_provider,
        recipe,
        electricity_ingredient,
        electricity_production_process,
        fuel_production_process,
        co2_process,
        sulfur_process,
        crude_oil_process,
        root_flow,
    )


@pytest_asyncio.fixture
async def create_more_elaborate_recipe_without_lca_process(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> ElaborateRecipe:
    """Creates a more elaborate recipe.

    based on the example in
    <https://link.springer.com/chapter/10.1007/978-94-015-9900-9_2>
    """
    pg_db, service_provider = setup_services
    eaternity_term_group_uid, _, _, _, _, _, _ = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            titles=[{"language": "en", "value": "A recipe to test LCA of electricity production"}],
            recipe_portions=10,
            production_portions=4,
        )
    )

    root_flow = await graph_mgr.upsert_node_by_uid(FoodProductFlowNode())

    # add edge Root flow --> activitiy node
    await graph_mgr.add_edge(
        Edge(parent_uid=root_flow.uid, child_uid=recipe.uid, edge_type=EdgeTypeEnum.root_to_recipe)
    )

    # create one ingredient
    electricity_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "en", "value": "electricity"}],
            amount_in_original_source_unit={
                "value": 1000,
                "unit": "kWh",
            },  # this becomes our reference flow / functional unit, used by matrix_calculation module
            type="conceptual-ingredients",
        )
    )

    # edge recipe --> ingredient
    await graph_mgr.add_edge(
        Edge(
            parent_uid=recipe.uid,
            child_uid=electricity_ingredient.uid,
            edge_type=EdgeTypeEnum.ingredient,
        )
    )

    electricity_production_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            production_amount={"value": 10, "unit": "kWh"},
            key=["test", "1"],
            name="production of electricity",
            type="process",
        )
    )

    electricity_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid(
        "EOS_electricity",
        eaternity_term_group_uid,
    )
    assert electricity_term

    fuel_production_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            production_amount={"value": 100, "unit": "l"},
            key=["test", "2"],
            name="production of fuel",
            type="process",
        )
    )
    await add_flow(graph_mgr, electricity_production_process, fuel_production_process, 2)

    co2_process = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
            name="Carbon dioxide, fossil",
            type="emission",
            categories=["air"],
        )
    )
    co2_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
        co2_process.uid,
    )
    await add_flow(graph_mgr, electricity_production_process, co2_process, 1, "biosphere")
    await add_flow(graph_mgr, fuel_production_process, co2_process, 10, "biosphere")

    sulfur_process = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "fd7aa71c-508c-480d-81a6-8052aad92646"],
            name="Sulfur dioxide",
            type="emission",
            categories=["air"],
        )
    )
    sulfur_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_fd7aa71c-508c-480d-81a6-8052aad92646",
        sulfur_process.uid,
    )
    await add_flow(graph_mgr, electricity_production_process, sulfur_process, 0.1, "biosphere")
    await add_flow(graph_mgr, fuel_production_process, sulfur_process, 2, "biosphere")

    crude_oil_process = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "3"],
            name="crude oil",
            type="natural ressource",
            categories=["natural ressource"],
        )
    )
    crude_oil_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_3",
        crude_oil_process.uid,
    )
    await add_flow(graph_mgr, fuel_production_process, crude_oil_process, -50, "biosphere")
    await service_provider.product_service.bulk_insert_xid_uid_mappings(
        [crude_oil_mapping, co2_mapping, sulfur_mapping]
    )

    return ElaborateRecipe(
        pg_db,
        service_provider,
        recipe,
        electricity_ingredient,
        electricity_production_process,
        fuel_production_process,
        co2_process,
        sulfur_process,
        crude_oil_process,
        root_flow,
    )


@pytest_asyncio.fixture
async def create_minimum_recipe_with_wrong_ingredients_declaration(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> MinimumRecipeWithDeclaration:
    """Creates a minimum recipe with wrong ingredients declaration for testing declaration mapping table."""
    pg_db, service_provider = setup_services
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid)
    ingredients_declaration = [
        {
            "language": "de",
            "value": "Hähnchenbrustfilet. (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), Käse (7 %). "
            "Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, Speisesalz, "
            "Stärke, Maltodextrin, Milcheiweiß. Pflanzeneiweiß, Dextrose, Zucker, "
            "Gewürzextrakte. Geschmacksverstärker: E 620",
        },
    ]
    sub_flow = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            nutrient_values=RawNutrientValuesPer100g(carbohydrates_gram=100),
            ingredients_declaration=ingredients_declaration,
        )
    )
    return MinimumRecipeWithDeclaration(pg_db, service_provider, recipe, root_flow, sub_flow)


@pytest_asyncio.fixture
async def create_minimum_recipe_with_empty_fields(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> Tuple[PostgresDb, ServiceProvider, Node, Node, Node]:
    """Creates a minimum recipe with empty ingredient declaration, amount and unit fields."""
    pg_db, service_provider = setup_services
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            ingredients_declaration=None,
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    # create one ingredient
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="spain",
            product_name=[{"language": "de", "value": "Karotten"}],
            ingredients_declaration=None,
            type="conceptual-ingredients",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    # add edge recipe --> carrot_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=carrot_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid)
    return pg_db, service_provider, recipe, carrot_ingredient, root_flow


@pytest_asyncio.fixture
async def create_minimum_recipe_with_no_fields(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> Tuple[PostgresDb, ServiceProvider, Node, Node]:
    """Creates a minimum recipe with no ingredient declaration, amount and unit fields."""
    pg_db, service_provider = setup_services
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    # create one ingredient
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            raw_production={"value": "greenhouse", "language": "en"},
            raw_transport="air",
            flow_location="spain",
            product_name=[{"language": "de", "value": "Karotten"}],
            type="conceptual-ingredients",
            processing="raw",
            raw_conservation={"value": "fresh", "language": "en"},
            packaging="plastic",
        )
    )

    # add edge recipe --> carrot_ingredient
    await graph_mgr.add_edge(
        Edge(parent_uid=recipe.uid, child_uid=carrot_ingredient.uid, edge_type=EdgeTypeEnum.ingredient)
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid)
    return pg_db, service_provider, recipe, carrot_ingredient, root_flow


@pytest_asyncio.fixture
async def create_minimum_recipe_with_foreign_and_german_ingredients_declarations(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> MinimumRecipeWithDeclaration:
    """Create a minimum recipe with multiple ingredients declarations.

    1 in German, 1 in English and 1 with undefined language.
    """
    pg_db, service_provider = setup_services
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid)
    ingredients_declaration = [
        {
            "language": "en",
            "value": "WRONG, DECLARATION",
        },
        {
            "language": "",
            "value": "ANOTHER, WRONG, DECLARATION",
        },
        {
            "language": "de",
            "value": "Hähnchenbrustfilet (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, "
            "Speisesalz, Stärke, Maltodextrin, Milcheiweiß, Pflanzeneiweiß, Dextrose, "
            "Zucker, Gewürzextrakte, Geschmacksverstärker: E 620",
        },
    ]
    sub_flow = await append_flow_node(
        graph_mgr,
        recipe.uid,
        nutrient_values=RawNutrientValuesPer100g(carbohydrates_gram=100),
        ingredients_declaration=ingredients_declaration,
    )

    return MinimumRecipeWithDeclaration(pg_db, service_provider, recipe, root_flow, sub_flow)


@pytest_asyncio.fixture
async def create_minimum_recipe_with_foreign_and_undefined_ingredients_declarations(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> Tuple[PostgresDb, ServiceProvider, Node, FoodProductFlowNode]:
    """Creates a minimum recipe with multiple ingredients declarations.

    1 in English and 1 with undefined language, to test out that IngredientSplitterGFM
    always picks the first declaration if there is no declaration in German.
    """
    pg_db, service_provider = setup_services
    graph_mgr = pg_db.get_graph_mgr()

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid)

    ingredients_declaration = [
        {
            "language": "",
            "value": "Hähnchenbrustfilet (53 %), Panade (28%) "
            "(Weizenmehl, Wasser, modifizierte Weizen-stärke, "
            "Weizenstärke, Speisesalz, Gewürze, Hefe), "
            "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) "
            "(Putenkeulenfleisch, Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), "
            "Maltodextrin, Dextrose, Gewürzextrakte, Stabilisator: E450), "
            "Käse (7 %), Kartoffelstärke, Stabilisator: modifizierte Stärke, Salz, "
            "Speisesalz, Stärke, Maltodextrin, Milcheiweiß, Pflanzeneiweiß, Dextrose, "
            "Zucker, Gewürzextrakte, Geschmacksverstärker: E 620",
        },
        {
            "language": "en",
            "value": "WRONG, DECLARATION",
        },
    ]
    sub_flow = await append_flow_node(
        graph_mgr,
        recipe.uid,
        nutrient_values=RawNutrientValuesPer100g(carbohydrates_gram=100),
        ingredients_declaration=ingredients_declaration,
    )

    return pg_db, service_provider, recipe, root_flow, sub_flow


@pytest_asyncio.fixture
async def create_minimum_recipe_with_small_declaration_and_two_origins(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeWithTwoOrigins:
    """Creates a minimum recipe with just a carrot in its ingredients declaration."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # get the terms' nutrients (these were seeded in pg_term_mgr.py)
    term_mgr: PgTermMgr = pg_db.get_term_mgr()

    carrot = await term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)

    carrot_nutrients_term: Term = await service_provider.glossary_link_service.load_term_nutrients([carrot])
    assert carrot_nutrients_term.data["nutr-vals"], "nutrients should be loaded"
    carrot_nutrients_kale = transform_eurofir(carrot_nutrients_term.data["nutr-vals"])

    top_recipe = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 1000},
            titles=[
                {
                    "language": "de",
                    "value": "a top recipe with another sub-recipe: more_elaborate_recipe (from the book)",
                }
            ],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    # create one ingredient
    linking_node = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            amount_in_original_source_unit={"value": 1000},
            nutrient_values=carrot_nutrients_kale,
            link_to_sub_node=LinkToUidProp(uid=recipe.uid),
        ),
    )

    await graph_mgr.add_edge(
        Edge(
            parent_uid=top_recipe.uid,
            child_uid=linking_node.uid,
            edge_type=EdgeTypeEnum.link_to_subrecipe,
        )
    )

    # Add an edge to reflect the link_to_sub_node.
    await graph_mgr.add_edge(
        Edge(
            parent_uid=linking_node.uid,
            child_uid=recipe.uid,
            edge_type=EdgeTypeEnum.link_to_subrecipe,
        )
    )

    carrot_process = await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
    )

    root_flow = await prepend_flow_node(graph_mgr, top_recipe.uid, nutrient_values=carrot_nutrients_kale)
    ingredients_declaration = [
        {
            "language": "de",
            "value": "Karotten",
        },
    ]
    # Notes regarding "origin":
    # 1) This is interpreted as the production location of this Activity.
    # 2) According to legacy apiary documentation, the "origin" field in a Product (i.e. a node that contains
    # ingredients_declaration) is interpreted as "Production location: postal address or country".
    # 3) In this case the comma separated list is interpreted as 50% of this sub-recipe is produced in Spain
    # and 50% is produced in Switzerland. The origin model will run individually on the sub-recipe in Spain and
    # Switzerland, such that the actual carrot ingredient are sourced from a lot of third countries (eg FR, PT,
    # IT, etc.) based on FAO import statistics of Spain and Switzerland.
    flow_location = "ES, CH"
    _ = await append_flow_node(
        graph_mgr, recipe.uid, ingredients_declaration=ingredients_declaration, flow_location=flow_location
    )

    return MinimumRecipeWithTwoOrigins(pg_db, service_provider, top_recipe, carrot_process, root_flow)


@pytest_asyncio.fixture
async def create_minimum_recipe_with_small_declaration_and_no_transport_lci(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> MinimumRecipeWithDeclaration:
    """Creates a minimum recipe with just a carrot in its ingredients declaration."""
    pg_db, service_provider = setup_services
    _, _, foodex2_term_group_uid, _, _, transport_term_group_uid, default_namespace_uid = get_term_access_group_uids
    graph_mgr = pg_db.get_graph_mgr()

    # get the terms' nutrients (these were seeded in pg_term_mgr.py)
    term_mgr: PgTermMgr = pg_db.get_term_mgr()

    carrot = await term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)

    carrot_nutrients_term: Term = await service_provider.glossary_link_service.load_term_nutrients([carrot])
    assert carrot_nutrients_term.data["nutr-vals"], "nutrients should be loaded"
    carrot_nutrients_kale = transform_eurofir(carrot_nutrients_term.data["nutr-vals"])

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            activity_location="Zürich Schweiz",
            production_amount={"value": 1000},
            titles=[{"language": "de", "value": "Eine leckere Karotten Rezeptur"}],
            author="Eckart Witzigmann",
            date="2013-10-14",
            instructions=[{"language": "de", "value": "Die Karotten gut kochen."}],
        )
    )

    await create_tractor_dummy_lca(
        foodex2_term_group_uid,
        graph_mgr,
        pg_db,
        transport_term_group_uid,
        add_transport_lci=False,
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid, nutrient_values=carrot_nutrients_kale)
    ingredients_declaration = [
        {
            "language": "de",
            "value": "Karotten",
        },
    ]
    sub_flow = await append_flow_node(graph_mgr, recipe.uid, ingredients_declaration=ingredients_declaration)
    return MinimumRecipeWithDeclaration(pg_db, service_provider, recipe, root_flow, sub_flow)


@pytest_asyncio.fixture
async def setup_matching_to_node_uid(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    create_convenience_product: Tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node],
) -> Tuple[PostgresDb, ServiceProvider, MatchingItem]:
    postgres_db, service_provider = setup_services
    pg_db, service_provider, root_recipe, convenience_product, carrot_ingredient, root_flow = create_convenience_product
    await seed_processing(pg_db, service_provider)

    # create a matching item that will match to the convenience product
    matching_item = await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module="MatchProductName",
            access_group_uid=None,
            lang="en",
            matching_string="a very special convenience product",
            matched_node_uid=str(convenience_product.uid),
            term_uids=[],
        )
    )

    return pg_db, service_provider, matching_item


@pytest_asyncio.fixture
async def setup_matchings_with_unknown_nutrients(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> Tuple[Calculation, CalcService]:
    postgres_db, service_provider = setup_services
    foodex2_ag_uid = service_provider.glossary_service.root_subterms["A0361"].access_group_uid

    a01bs = service_provider.glossary_service.terms_by_xid_ag_uid[("A01BS", foodex2_ag_uid)]
    j0116 = service_provider.glossary_service.terms_by_xid_ag_uid[("J0116", foodex2_ag_uid)]
    j0136 = service_provider.glossary_service.terms_by_xid_ag_uid[("J0136", foodex2_ag_uid)]

    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module="MatchProductName",
            access_group_uid=None,
            lang="en",
            matching_string="test item 1",
            term_uids=[str(a01bs.uid), str(j0116.uid)],
        )
    )

    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module="MatchProductName",
            access_group_uid=None,
            lang="en",
            matching_string="test item 2",
            term_uids=[str(a01bs.uid), str(j0136.uid)],
        )
    )

    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module="MatchProductName",
            access_group_uid=None,
            lang="en",
            matching_string="test item 3",
            term_uids=[str(a01bs.uid)],
        )
    )

    random_carrot_process = ModeledActivityNode(
        uid=uuid.uuid4(),
        activity_location="GLO",
        production_amount={"value": 1.0, "unit": "kilogram"},
        id="('EDB', '437443338484ca565ae5a832d81fcc52_copy1')",
        key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
        flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
        name="market for carrot (w/o transport)",
        type="process",
        database="EDB",
        filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
        reference_product="carrot",
    )
    await postgres_db.get_graph_mgr().upsert_node_by_uid(random_carrot_process)

    co2_emission = await postgres_db.get_graph_mgr().upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
            name="Carbon dioxide, fossil",
            type="emission",
            categories=["air"],
        )
    )
    co2_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
        co2_emission.uid,
    )
    service_provider = ServiceLocator().service_provider
    await service_provider.product_service.bulk_insert_xid_uid_mappings([co2_mapping])
    await add_flow(postgres_db.get_graph_mgr(), random_carrot_process, co2_emission, 1.2, "biosphere")

    for term_tuple in (("A01BS", "J0116"), ("A01BS", "J0136"), ("A01BS",)):
        terms: list[Term] = [(await postgres_db.get_term_mgr().get_terms_by_xid(xid))[0] for xid in term_tuple]

        await postgres_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[t.uid for t in terms],
                linked_node_uid=random_carrot_process.uid,
            )
        )

    graph_mgr = postgres_db.get_graph_mgr()
    root_flow = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(
            amount_in_original_source_unit={"value": 1.0, "unit": "kg"}, nutrient_values=load_seeded_nutrients["carrot"]
        )
    )
    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            production_amount={"value": 1.0, "unit": "kg"},
        )
    )

    await graph_mgr.add_edge(Edge(parent_uid=root_flow.uid, child_uid=recipe.uid))

    ingredients = []
    for name, percent in {"test item 1": 60, "test item 2": 25, "test item 3": 15}.items():
        ingredients.append(
            await graph_mgr.upsert_node_by_uid(
                FoodProductFlowNode(
                    product_name=[{"language": "en", "value": name}],
                    amount_in_original_source_unit={"value": percent, "unit": "%"},
                )
            )
        )
        await graph_mgr.add_edge(Edge(parent_uid=recipe.uid, child_uid=ingredients[-1].uid))

    await service_provider.gap_filling_module_loader.init(service_provider)

    calculation = Calculation(uid=uuid.uuid4(), root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid)
    return calculation, postgres_db.service_provider.calc_service


@pytest_asyncio.fixture
async def setup_recipe_with_all_amounts(
    request: FixtureRequest,
    setup_services: Tuple[PostgresDb, ServiceProvider],
    load_seeded_nutrients: dict[str, dict[str, float]],
) -> Tuple[Calculation, CalcService]:
    postgres_db, service_provider = setup_services

    graph_mgr = postgres_db.get_graph_mgr()

    root_flow = await graph_mgr.upsert_node_by_uid(FoodProductFlowNode(nutrient_values=load_seeded_nutrients["carrot"]))
    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(production_amount={"value": 1, "unit": "kg"})
    )

    await graph_mgr.add_edge(Edge(parent_uid=root_flow.uid, child_uid=recipe.uid))

    ingredients = []
    for name, weight in {"Karotten": request.param, "Zwiebeln": 250, "Kartoffeln": 150}.items():
        ingredients.append(
            await graph_mgr.upsert_node_by_uid(
                FoodProductFlowNode(
                    product_name=[{"language": "de", "value": name}],
                    amount_in_original_source_unit={"value": weight, "unit": "gram"} if weight else None,
                )
            )
        )
        await graph_mgr.add_edge(Edge(parent_uid=recipe.uid, child_uid=ingredients[-1].uid))

    await service_provider.gap_filling_module_loader.init(service_provider)

    calculation = Calculation(uid=uuid.uuid4(), root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid)
    return calculation, postgres_db.service_provider.calc_service


@pytest_asyncio.fixture
async def setup_minimal_sub_recipe_for_ingredient_declaration(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    load_seeded_nutrients: dict[str, dict[str, float]],
) -> Tuple[Calculation, CalcService]:
    postgres_db, service_provider = setup_services

    graph_mgr = postgres_db.get_graph_mgr()

    root_flow = await graph_mgr.upsert_node_by_uid(FoodProductFlowNode(nutrient_values=load_seeded_nutrients["onion"]))
    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(production_amount={"value": 0.150, "unit": "kg"})
    )

    await graph_mgr.add_edge(Edge(parent_uid=root_flow.uid, child_uid=recipe.uid))

    flow_to_sub_recipe = await graph_mgr.upsert_node_by_uid(
        FoodProductFlowNode(amount_in_original_source_unit={"value": 0.150, "unit": "kg"})
    )

    await graph_mgr.add_edge(Edge(parent_uid=recipe.uid, child_uid=flow_to_sub_recipe.uid))

    sub_recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(production_amount={"value": 0.00350, "unit": "kg"})
    )

    await graph_mgr.add_edge(Edge(parent_uid=flow_to_sub_recipe.uid, child_uid=sub_recipe.uid))

    ingredients = []
    for name, weight in {"Zwiebeln": 3.5}.items():
        ingredients.append(
            await graph_mgr.upsert_node_by_uid(
                FoodProductFlowNode(
                    product_name=[{"language": "de", "value": name}],
                    amount_in_original_source_unit={"value": weight, "unit": "gram"} if weight else None,
                )
            )
        )
        await graph_mgr.add_edge(Edge(parent_uid=sub_recipe.uid, child_uid=ingredients[-1].uid))

    await service_provider.gap_filling_module_loader.init(service_provider)

    calculation = Calculation(uid=uuid.uuid4(), root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid)
    return calculation, postgres_db.service_provider.calc_service
