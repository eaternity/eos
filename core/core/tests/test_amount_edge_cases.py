"Tests amount edge cases."
import uuid

import pytest

from core.domain.calculation import Calculation
from core.graph_manager.calc_graph import CalcGraph
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.tests.conftest import MinimumRecipeRType


@pytest.mark.asyncio
async def test_amount_of_sub_recipe_without_amount(
    create_recipe_with_larger_depth: MinimumRecipeRType,
) -> None:
    (
        postgres_db,
        service_provider,
        top_recipe,
        sub_recipe,
        carrot_ingredient,
        carrot_process,
        root_flow,
    ) = create_recipe_with_larger_depth
    glossary_service = service_provider.glossary_service
    graph_mgr = service_provider.postgres_db.graph_mgr

    # update the sub_recipe such that it does not have a production amount:
    sub_recipe.production_amount = None
    sub_recipe = await graph_mgr.upsert_node_by_uid(sub_recipe)

    # update the carrot_ingredient within the sub_recipe to have 0 amount:
    carrot_ingredient.amount_in_original_source_unit = {"value": 0, "unit": "kilogram"}
    carrot_ingredient = await graph_mgr.upsert_node_by_uid(carrot_ingredient)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(top_recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, top_recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=top_recipe.uid, return_log=True
    )
    calculation, root_node = await calc_service.calculate(calculation)
    result_co2 = root_node.impact_assessment.amount_for_root_node().model_dump().get("amount_for_root_node").values()
    co2eq_value = list(result_co2)[0].get("quantity").get("value")
    assert 0.03 > co2eq_value > 0.01
