"""Tests for processing GFM."""

import uuid
from functools import partial
from typing import Optional, Tuple

import pytest
import structlog
from gap_filling_modules.impact_assessment_gfm import ImpactAssessmentGapFillingWorker
from gap_filling_modules.ingredicalc.helpers import transform_eurofir, transform_eurofir_to_qty_pkg
from gap_filling_modules.processing_gfm import ProcessingGapFillingFactory
from gap_filling_modules.processing_models import AbstractProcessingModel, ProcessWithFixedElectricityAmount

from core.domain.calculation import Calculation
from core.domain.glossary_link import GlossaryLink
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.location_prop import LocationProp, LocationSourceEnum
from core.domain.props.names_prop import NamesProp
from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.glossary_service import GlossaryService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import (
    EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_TRANSPORT_FREEZING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    is_close,
)
from core.tests.test_conservation_gfm import EXPECTED_CO2_VALUE_FOR_TEST_CONSERVATION_FOR_CONVENIENCE_PRODUCT_PER_KG
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID
from database.postgres.postgres_db import PostgresDb

from .conftest import MinimumRecipeRType

logger = structlog.get_logger()

EXPECTED_UPSCALE = 8.333333333

RATIO_80_PERCENT_DRIED = 0.3989032529778154
RATIO_100_PERCENT_DRIED = 1 - RATIO_80_PERCENT_DRIED
EXPECTED_UPSCALE_WITH_AMOUNT_ESTIMATION = 7.407407407 * RATIO_80_PERCENT_DRIED + 9.259259259259261 * (
    RATIO_100_PERCENT_DRIED
)
EXPECTED_WATER_EVAPORATED_WITH_AMOUNT_ESTIMATION = EXPECTED_UPSCALE_WITH_AMOUNT_ESTIMATION - 1.0
EXPECTED_WATER_EVAPORATED = EXPECTED_UPSCALE - 1.0
METHANE_EMISSION_UNIT_PROCESSING = 0.0123
METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_GLO = 0.0234
METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC = 0.0432
METHANE_GWP_100 = 29.7
RAW_MATERIAL_AMOUNT = 0.789
REQUIRED_ENERGY_FOR_COOLING = 0.02074074074
DEFAULT_ENERGY_NEEDED_TO_FREEZE_PER_KG = 0.005078911111  # from google sheet
EXPECTED_ENERGY_NEEDED_TO_MIX_PER_KG = 0.03935185185  # from google sheet
EXPECTED_ENERGY_NEEDED_TO_FREEZE_CARROT_PER_KG = 0.3709348762349024

EXPECTED_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY = 0.0456
METHANE_EMISSION_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY = (
    EXPECTED_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY * METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC
)
EXPECTED_OTHER_AMOUNT = 0.0345
METHANE_EMISSION_OTHERS_EXAMPLE_WITH_ELECTRICITY = METHANE_EMISSION_UNIT_PROCESSING * EXPECTED_OTHER_AMOUNT
RAW_MATERIAL_AMOUNT_EXAMPLE_WITH_ELECTRICITY = 0.0111


async def find_country_specific_electricity_node(
    service_provider: ServiceProvider, country_code: str, voltage: Optional[str] = "Low"
) -> uuid.UUID:
    """Method to find the electricity node for a specific country."""
    electricity_market_node_uid = await service_provider.postgres_db.gfm_cache_mgr.get_cache_entry_by_gfm_name_and_key(
        gfm_name=f"ElectricityMarket{voltage}Voltage", cache_key=country_code
    )
    assert electricity_market_node_uid, f"Electricity market node for {country_code} not found."
    return uuid.UUID(electricity_market_node_uid)


async def compare_processing_with_only_electricity(
    expected_raw_material: float,
    country_code: str,
    expected_electricity: float,
    service_provider: ServiceProvider,
    model: AbstractProcessingModel,
    tag_xids: frozenset,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
) -> None:
    """Compare result values with benchmark for processes with only electricity."""
    assert model.is_triggered_by_tag_xids(
        set(tag_xids)
    ), f"The tag xids {tag_xids} should trigger {model.__class__.__name__} model."
    raw_material_flow_amount, bw_nodes_and_flow_amounts, _ = await model.compute_flow_amounts(
        set(tag_xids), flow_node, production_amount
    )
    assert is_close(raw_material_flow_amount.value, expected_raw_material)
    assert len(bw_nodes_and_flow_amounts) == 1
    electricity_market_node_uid = await find_country_specific_electricity_node(service_provider, country_code)
    assert is_close(bw_nodes_and_flow_amounts[electricity_market_node_uid].value, expected_electricity)


async def compare_region_unspecified_or_glo_processes(
    expected_raw_material: float,
    expected_bw: float,
    service_provider: ServiceProvider,
    model: AbstractProcessingModel,
    tag_xids: frozenset,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
    region: str = "unspecified",
) -> None:
    """Compare result values with benchmark for processes with non-specific regions."""
    assert model.is_triggered_by_tag_xids(
        set(tag_xids)
    ), f"The tag xids {tag_xids} should trigger {model.__class__.__name__} model."
    raw_material_flow_amount, bw_nodes_and_flow_amounts, _ = await model.compute_flow_amounts(
        set(tag_xids), flow_node, production_amount
    )
    assert is_close(raw_material_flow_amount.value, expected_raw_material)
    if region == "unspecified":
        assert len(bw_nodes_and_flow_amounts) == 1
    else:
        assert len(bw_nodes_and_flow_amounts) > 1

    bw_xid = model.processing_by_tag[tag_xids][region].xid
    bw_uid = service_provider.product_service.xid_to_uid_mappings[bw_xid]
    assert is_close(bw_nodes_and_flow_amounts[bw_uid].value, expected_bw)


async def compare_processing_region_specific(
    expected_raw_material: float,
    country_code: str,
    service_provider: ServiceProvider,
    model: AbstractProcessingModel,
    tag_xids: frozenset,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
) -> None:
    """Compare result values with benchmark for processes with specific location."""
    assert model.is_triggered_by_tag_xids(
        set(tag_xids)
    ), f"The tag xids {tag_xids} should trigger {model.__class__.__name__} model."
    raw_material_flow_amount, bw_nodes_and_flow_amounts, _ = await model.compute_flow_amounts(
        set(tag_xids), flow_node, production_amount
    )
    assert is_close(raw_material_flow_amount.value, expected_raw_material)
    assert len(bw_nodes_and_flow_amounts) > 1
    electricity_market_node_uid = await find_country_specific_electricity_node(service_provider, country_code)
    assert is_close(
        bw_nodes_and_flow_amounts[electricity_market_node_uid].value,
        EXPECTED_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY * production_amount.value,
    )


async def freezing_test(
    pg_db: PostgresDb,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: str,
    model: AbstractProcessingModel,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
) -> None:
    """Test for freezing process."""
    tag_xids = ["A1791", "J0136"]
    await add_glossary_tags(
        pg_db,
        service_provider.glossary_service,
        foodex2_term_group_uid,
        flow_node,
        tag_xids=[],
        product_name_xids=tag_xids,
    )
    # first we test the case where there are no nutrients attached to the carrot
    flow_node.flow_location = None  # Test for GLO.
    flow_node.nutrient_values = None
    await compare_processing_with_only_electricity(
        expected_raw_material=production_amount.value,
        country_code="GLO",
        expected_electricity=DEFAULT_ENERGY_NEEDED_TO_FREEZE_PER_KG * production_amount.value,
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
    )

    # then the case where there are nutrients attached to the carrot
    nutrient_terms = [term.get_term() for term in flow_node.product_name.terms]
    nutrient_values = await service_provider.glossary_link_service.load_term_nutrients(nutrient_terms)
    flow_node.nutrient_values = transform_eurofir_to_qty_pkg(nutrient_values.data["nutr-vals"])
    await compare_processing_with_only_electricity(
        expected_raw_material=production_amount.value,
        country_code="GLO",
        expected_electricity=EXPECTED_ENERGY_NEEDED_TO_FREEZE_CARROT_PER_KG * production_amount.value,
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
    )


async def fixed_electricity_test(
    pg_db: PostgresDb,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: str,
    model: AbstractProcessingModel,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
    tag_xids: list[str] | None = None,
    model_electricity: float = 0.0,
) -> None:
    """Test for freezing process."""
    assert model_electricity, "Model electricity must be non-zero."
    await add_glossary_tags(
        pg_db,
        service_provider.glossary_service,
        foodex2_term_group_uid,
        flow_node,
        tag_xids=[],
        product_name_xids=tag_xids,
    )
    flow_node.flow_location = None  # Test for GLO.
    await compare_processing_with_only_electricity(
        expected_raw_material=production_amount.value,
        country_code="GLO",
        expected_electricity=model_electricity * production_amount.value,
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
    )


async def region_specific_test(
    pg_db: PostgresDb,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: str,
    model: AbstractProcessingModel,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
    tag_xids: list[str] | None = None,
) -> None:
    """Test for processes that depend on the location."""
    flow_node.flow_location = [
        LocationProp.unvalidated_construct(
            address="Switzerland", location_qualifier=LocationSourceEnum.foodex, country_code="CH"
        )
    ]  # GLO is not interesting as it is not treated as region specific so use CH.
    await add_glossary_tags(
        pg_db,
        service_provider.glossary_service,
        foodex2_term_group_uid,
        flow_node,
        tag_xids=tag_xids,
    )
    await compare_processing_region_specific(
        expected_raw_material=production_amount.value * RAW_MATERIAL_AMOUNT_EXAMPLE_WITH_ELECTRICITY,
        country_code="CH",
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
    )


async def process_with_rer_and_glo_test(
    pg_db: PostgresDb,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: str,
    model: AbstractProcessingModel,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
    tag_xids: list[str] | None = None,
) -> None:
    """Test for processes that have two different versions (one for RER and another for GLO)."""
    assert tag_xids, "tag_xids must be provided"
    flow_node.flow_location = None  # First no location should lead to GLO.
    await add_glossary_tags(
        pg_db,
        service_provider.glossary_service,
        foodex2_term_group_uid,
        flow_node,
        tag_xids=tag_xids,
    )
    await compare_region_unspecified_or_glo_processes(
        expected_raw_material=production_amount.value,
        expected_bw=production_amount.value,
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
        region="Global",
    )

    # Switzerland should lead to RER.
    flow_node.flow_location = [
        LocationProp.unvalidated_construct(
            address="Spain", location_qualifier=LocationSourceEnum.foodex, country_code="ES"
        )
    ]
    await compare_processing_region_specific(
        expected_raw_material=production_amount.value,
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
        country_code="ES",
    )


async def process_with_non_unit_raw_material_test(
    pg_db: PostgresDb,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: str,
    model: AbstractProcessingModel,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
    tag_xids: list[str] | None = None,
) -> None:
    """Test for processes that have non-unit raw material."""
    assert tag_xids, "tag_xids must be provided"
    await add_glossary_tags(
        pg_db,
        service_provider.glossary_service,
        foodex2_term_group_uid,
        flow_node,
        tag_xids=[tag_xids[1]],
        product_name_xids=[tag_xids[0]],
    )
    await compare_region_unspecified_or_glo_processes(
        expected_raw_material=0.789 * production_amount.value,
        expected_bw=production_amount.value,
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
    )


async def process_with_unit_raw_material_test(
    pg_db: PostgresDb,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: str,
    model: AbstractProcessingModel,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
    tag_xids: list[str] | None = None,
) -> None:
    """Test for processes that have unit raw material."""
    tag_xids_without_product_name = []
    product_name_xids = []

    for tag_xid in tag_xids:
        if tag_xid in ("A1791", "A01BS"):
            product_name_xids.append(tag_xid)
        else:
            tag_xids_without_product_name.append(tag_xid)

    await add_glossary_tags(
        pg_db,
        service_provider.glossary_service,
        foodex2_term_group_uid,
        flow_node,
        tag_xids=tag_xids_without_product_name,
        product_name_xids=product_name_xids,
    )
    await compare_region_unspecified_or_glo_processes(
        expected_raw_material=production_amount.value,
        expected_bw=production_amount.value,
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
    )


async def drying_test(
    pg_db: PostgresDb,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: str,
    model: AbstractProcessingModel,
    flow_node: FoodProductFlowNode,
    production_amount: QuantityProp,
) -> None:
    """Test for drying process."""
    expected_upscale_adjusted_for_production = EXPECTED_UPSCALE * production_amount.value
    expected_water_evaporated_adjusted_for_production = EXPECTED_WATER_EVAPORATED * production_amount.value

    tag_xids = ["A1791", "A01BS", "J0116"]
    await add_glossary_tags(
        pg_db,
        service_provider.glossary_service,
        foodex2_term_group_uid,
        flow_node,
        tag_xids=[],
        product_name_xids=tag_xids,
    )

    flow_node.amount = QuantityProp(
        value=expected_upscale_adjusted_for_production,
        unit_term_uid=production_amount.unit_term_uid,
        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
    )
    flow_node.nutrient_upscale_ratio = QuantityProp(
        value=EXPECTED_UPSCALE,
        unit_term_uid=production_amount.unit_term_uid,
        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
    )
    await compare_region_unspecified_or_glo_processes(
        expected_raw_material=expected_upscale_adjusted_for_production,
        expected_bw=expected_water_evaporated_adjusted_for_production,
        service_provider=service_provider,
        model=model,
        tag_xids=frozenset(tag_xids),
        flow_node=flow_node,
        production_amount=production_amount,
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "tag_xid_to_attach_co2_value_difference",
    [
        (
            ["A01BS", "J0116"],
            METHANE_GWP_100 * METHANE_EMISSION_UNIT_PROCESSING * EXPECTED_WATER_EVAPORATED
            + (EXPECTED_UPSCALE - 1) * EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
            False,
        ),
        (
            ["A01BS", "J0116"],
            METHANE_GWP_100 * METHANE_EMISSION_UNIT_PROCESSING * (EXPECTED_WATER_EVAPORATED_WITH_AMOUNT_ESTIMATION)
            + (EXPECTED_UPSCALE_WITH_AMOUNT_ESTIMATION - 1) * EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
            True,
        ),
        (
            ["A01BS", "A004V"],
            (RAW_MATERIAL_AMOUNT - 1.0) * EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            # 1.0 for baking,
            + METHANE_EMISSION_UNIT_PROCESSING * METHANE_GWP_100 * 1.0
            + (
                METHANE_GWP_100
                * METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC
                * REQUIRED_ENERGY_FOR_COOLING
            ),  # For cooling.
            False,
        ),
        (
            ["J0136"],
            (
                EXPECTED_ENERGY_NEEDED_TO_FREEZE_CARROT_PER_KG
                * METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC
            )  # No transport is added.
            * METHANE_GWP_100,
            False,
        ),
        (
            ["A01BS", "A02NH"],
            (RAW_MATERIAL_AMOUNT_EXAMPLE_WITH_ELECTRICITY - 1.0) * EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            + (
                METHANE_GWP_100
                * METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC
                * REQUIRED_ENERGY_FOR_COOLING
            )  # For cooling.
            + (
                METHANE_EMISSION_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY
                + METHANE_EMISSION_OTHERS_EXAMPLE_WITH_ELECTRICITY
                + METHANE_EMISSION_UNIT_PROCESSING * RAW_MATERIAL_AMOUNT_EXAMPLE_WITH_ELECTRICITY  # For transport.
            )
            * METHANE_GWP_100,
            False,
        ),
    ],
)
async def test_processing_gfm(
    create_minimum_recipe_for_processing: MinimumRecipeRType,
    get_term_access_group_uids: dict[str, uuid.UUID],
    tag_xid_to_attach_co2_value_difference: tuple[list, float],
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    (
        postgres_db,
        service_provider,
        recipe,
        carrot_ingredient,
        carrot_process,
        root_flow,
    ) = create_minimum_recipe_for_processing
    glossary_service = service_provider.glossary_service
    _, eurofir_term_group_uid, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids

    # here the carrot ingredient can be modified
    await add_glossary_tags(
        postgres_db,
        glossary_service,
        foodex2_term_group_uid,
        carrot_ingredient,
        tag_xid_to_attach_co2_value_difference[0],
        process_node=carrot_process,
    )

    if tag_xid_to_attach_co2_value_difference[2]:
        dried_carrot_nutrients_term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
            "244", eurofir_term_group_uid
        )
        root_flow.nutrient_values = transform_eurofir(dried_carrot_nutrients_term.data["nutr-vals"])
        await postgres_db.graph_mgr.upsert_node_by_uid(root_flow)

    await postgres_db.graph_mgr.upsert_node_by_uid(carrot_ingredient)

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert len(calculation.mutations) > 5
    assert ImpactAssessmentGapFillingWorker.__name__ in [
        mutation.created_by_module for mutation in calculation.mutations
    ]

    root_impact_assessments_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_impact_assessments_term.access_group_uid)
    ]

    result_co2 = (
        root_node.get_sub_nodes()[0]
        .impact_assessment.amount_for_activity_production_amount()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value
    )

    assert carrot_ingredient.amount_in_original_source_unit.unit == "kilogram", "should have correct unit"
    amount_carrot_in_kg = carrot_ingredient.amount_in_original_source_unit.value

    if "J0116" in tag_xid_to_attach_co2_value_difference[0]:
        cooling_co2 = 0.0  # If dried, no cooling is added.
    elif "J0136" in tag_xid_to_attach_co2_value_difference[0]:
        # in this case, the carrot is frozen and transported by air
        cooling_co2 = EXPECTED_TRANSPORT_FREEZING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
    else:
        cooling_co2 = EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR

    expected_co2 = (
        EXPECTED_CO2_AMOUNT_PER_KG_CARROT
        + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
        + cooling_co2
        + tag_xid_to_attach_co2_value_difference[1]
    ) * amount_carrot_in_kg
    assert is_close(result_co2, expected_co2), "root node should have correct CO2 amount"


@pytest.mark.asyncio
async def test_processing_gfm_with_subdivision(
    create_recipe_without_amounts_for_processing: MinimumRecipeRType,
    get_term_access_group_uids: dict[str, uuid.UUID],
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    (
        postgres_db,
        service_provider,
        recipe,
        carrot_ingredient,
        carrot_process,
        root_flow,
    ) = create_recipe_without_amounts_for_processing
    glossary_service = service_provider.glossary_service
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids

    # here the carrot ingredient can be modified
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    carrot_amount_in_kg = 0.15
    non_dried_portion = 0.6941534636156644
    dried_portion = 1.0 - non_dried_portion
    upscaling = 5.070993914807302

    total_carrot_amount_in_kg = carrot_amount_in_kg * (non_dried_portion + dried_portion * upscaling)

    assert len(calculation.mutations) > 5
    assert ImpactAssessmentGapFillingWorker.__name__ in [
        mutation.created_by_module for mutation in calculation.mutations
    ]

    root_impact_assessments_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_impact_assessments_term.access_group_uid)
    ]

    result_co2 = (
        root_node.get_sub_nodes()[0]
        .impact_assessment.amount_for_activity_production_amount()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value
    )

    cooling_co2 = EXPECTED_TRANSPORT_FREEZING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR

    expected_additional_co2 = (
        EXPECTED_ENERGY_NEEDED_TO_FREEZE_CARROT_PER_KG * METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC
    ) * METHANE_GWP_100  # No transport is added.

    expected_co2 = (EXPECTED_CO2_AMOUNT_PER_KG_CARROT + expected_additional_co2) * total_carrot_amount_in_kg + (
        EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR + cooling_co2
    ) * carrot_amount_in_kg
    assert is_close(result_co2, expected_co2), "root node should have correct CO2 amount"


async def add_glossary_tags(
    pg_db: PostgresDb,
    glossary_service: GlossaryService,
    foodex2_term_group_uid: str,
    flow_node: Node,
    tag_xids: list[str],
    process_node: Node | None = None,
    product_name_xids: list[str] | None = None,
) -> None:
    """Add glossary tags to a flow_node."""
    terms_to_attach = [
        (
            await glossary_service.get_term_by_xid_and_access_group_uid(
                tag_xid,
                foodex2_term_group_uid,
            )
            if not tag_xid.startswith("EOS")
            else glossary_service.root_subterms.get("EOS_Transportation")
        )
        for tag_xid in tag_xids
    ]

    if tag_xids == ["A01BS", "J0116"]:
        flow_node.product_name = [{"language": "de", "value": "neue getrocknete Karotten"}]
        carrot_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)
        fruit_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A01BS", foodex2_term_group_uid)
        if process_node:
            terms_to_attach = [terms_to_attach[-1]]
            await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                    term_uids=[carrot_term.uid, fruit_term.uid],
                    linked_node_uid=process_node.uid,
                )
            )
    elif tag_xids == ["J0136"]:
        flow_node.product_name = [{"language": "de", "value": "frozen Karotten"}]
        carrot_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A1791", foodex2_term_group_uid)
        frozen_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0136", foodex2_term_group_uid)
        if process_node:
            await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                    term_uids=[carrot_term.uid, frozen_term.uid],
                    linked_node_uid=process_node.uid,
                )
            )
        terms_to_attach = []

    if tag_xids == ["A01BS", "A02NH"]:
        flow_node.raw_processing = {"value": "fruit yoghurt production (cow milk)", "language": "en"}
    else:
        flow_node.glossary_tags = [
            GlossaryTermProp.unvalidated_construct(term_uid=term_to_attach.uid) for term_to_attach in terms_to_attach
        ]

    if product_name_xids:
        product_names_to_attach = [
            GlossaryTermProp.unvalidated_construct(
                term_uid=(
                    await glossary_service.get_term_by_xid_and_access_group_uid(
                        name_xid,
                        foodex2_term_group_uid,
                    )
                ).uid
            )
            for name_xid in product_name_xids
        ]
        flow_node.product_name = NamesProp.unvalidated_construct(terms=product_names_to_attach)


@pytest.mark.asyncio
async def test_process_specific_formula(
    seed_processing_for_tests: tuple[PostgresDb, ServiceProvider, str],
) -> None:
    """Simple tests for processing formulae."""
    pg_db, service_provider, foodex2_term_group_uid = seed_processing_for_tests
    gfm_factory = ProcessingGapFillingFactory(pg_db, service_provider)
    await gfm_factory.init_cache()

    test_function_by_model = {
        "Dried Fruit Grinding": partial(
            process_with_unit_raw_material_test, tag_xids=["A1791", "E0106", "J0116", "A01BS"]
        ),
        "Grinding": partial(fixed_electricity_test, tag_xids=["A1791", "E0106"]),
        "Roasting": partial(process_with_rer_and_glo_test, tag_xids=["A1791", "H0391", "A014C"]),
        "Chopping": partial(process_with_rer_and_glo_test, tag_xids=["A1791", "E0117", "A014C"]),
        "Carbonating": partial(process_with_unit_raw_material_test, tag_xids=["A1791", "H0109"]),
        "Smoking": partial(process_with_unit_raw_material_test, tag_xids=["A1791", "H0172"]),
        "Heating": partial(process_with_unit_raw_material_test, tag_xids=["A1791", "G0002"]),
        "Drying": drying_test,
        "Freezing": freezing_test,
        "Baking": partial(process_with_non_unit_raw_material_test, tag_xids=["A1791", "A004V"]),
        "Jam production": partial(process_with_non_unit_raw_material_test, tag_xids=["A1791", "A01MM", "A01BS"]),
        "Juice production": partial(process_with_non_unit_raw_material_test, tag_xids=["A1791", "C0300", "A01BS"]),
        "Transportation": partial(
            process_with_unit_raw_material_test, tag_xids=["A1791", "EOS_Transportation", "A01BS"]
        ),
        "Yoghurt production": partial(region_specific_test, tag_xids=["A01BS", "A02NH"]),
        "Mixing": partial(fixed_electricity_test, tag_xids=["A1791", "Z0074"]),
        "Cutting": partial(fixed_electricity_test, tag_xids=["A1791", "E0137"]),
        "Shredding": partial(fixed_electricity_test, tag_xids=["A1791", "E0111"]),
        "Fermenting": partial(fixed_electricity_test, tag_xids=["A1791", "H0230"]),
        "Puffing": partial(fixed_electricity_test, tag_xids=["A1791", "H0268"]),
        "Freeze-drying": partial(fixed_electricity_test, tag_xids=["A1791", "J0130"]),
        "Cooling": partial(fixed_electricity_test, tag_xids=["A1791", "J0131"]),
    }

    flow_node = FoodProductFlowNode()

    kilogram_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_kilogram", service_provider.glossary_service.root_subterms["EOS_units"].access_group_uid)
    ]
    production_amount = QuantityProp.unvalidated_construct(
        value=0.9,
        unit_term_uid=kilogram_term.uid,
        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
    )

    assert gfm_factory.all_models, f"all_models field should not be empty. It is {gfm_factory.all_models}."

    for model in gfm_factory.all_models:
        if model.process_name in test_function_by_model:
            if isinstance(model, ProcessWithFixedElectricityAmount):
                kwargs = {"model_electricity": model.process_specific_trigger_tags[0].electricity_amount}
            else:
                kwargs = {}

            test_function = test_function_by_model[model.process_name]
            await test_function(
                pg_db,
                service_provider,
                foodex2_term_group_uid,
                model,
                flow_node,
                production_amount,
                **kwargs,
            )
        else:
            raise AssertionError(f"Implement model specific test for {model.process_name}.")


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "tag_xid_to_attach_co2_value_difference",
    [
        (
            ["Z0074", "Z0001"],
            (
                EXPECTED_ENERGY_NEEDED_TO_MIX_PER_KG * METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC
                + 1.0 * METHANE_EMISSION_UNIT_PROCESSING
            )  # 1.0 is for transport
            * METHANE_GWP_100,
        ),
    ],
)
async def test_processing_on_combined_product_gfm(
    create_convenience_product_for_processing: Tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node],
    get_term_access_group_uids: dict[str, uuid.UUID],
    tag_xid_to_attach_co2_value_difference: tuple[list, float],
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    postgres_db, service_provider, recipe, convenience_product, _, root_flow = create_convenience_product_for_processing
    glossary_service = service_provider.glossary_service
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids

    # here the carrot ingredient can be modified
    await add_glossary_tags(
        postgres_db,
        glossary_service,
        foodex2_term_group_uid,
        convenience_product,
        tag_xids=[],
        product_name_xids=tag_xid_to_attach_co2_value_difference[0],
    )

    await postgres_db.graph_mgr.upsert_node_by_uid(convenience_product)

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert len(calculation.mutations) > 5
    assert ImpactAssessmentGapFillingWorker.__name__ in [
        mutation.created_by_module for mutation in calculation.mutations
    ]

    root_impact_assessments_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_impact_assessments_term.access_group_uid)
    ]

    result_co2 = (
        root_node.get_sub_nodes()[0]
        .impact_assessment.amount_for_activity_production_amount()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value
    )

    assert convenience_product.amount_in_original_source_unit.unit == "gram", "should have correct unit"
    amount_carrot_in_kg = convenience_product.amount_in_original_source_unit.value / 1000.0

    expected_co2 = (
        EXPECTED_CO2_VALUE_FOR_TEST_CONSERVATION_FOR_CONVENIENCE_PRODUCT_PER_KG
        + tag_xid_to_attach_co2_value_difference[1]
    ) * amount_carrot_in_kg
    assert is_close(result_co2, expected_co2), "root node should have correct CO2 amount"


@pytest.mark.asyncio
async def test_processing_on_dried_combined_product(
    create_convenience_product_for_processing: Tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node],
    get_term_access_group_uids: dict[str, uuid.UUID],
) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    postgres_db, service_provider, recipe, convenience_product, _, root_flow = create_convenience_product_for_processing
    glossary_service = service_provider.glossary_service
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids

    convenience_product.raw_conservation = {"value": "dried", "language": "en"}

    # when we set the convenience product to be dried, nutrient-subdivision-gfm will run on the potato- and carrot
    # ingredient. For the potato it will infer that the nutrient-values already come from a dried product and will
    # add the dried tag to the potato ingredient. Since we normally don't have a mocked linking for (potato, dried),
    # we add the mock here:
    potato_term = await postgres_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A00ZT", foodex2_term_group_uid)
    dried_term = await postgres_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0116", foodex2_term_group_uid)
    potato_glossary_link = await postgres_db.get_pg_glossary_link_mgr().get_glossary_links(
        [potato_term.uid], "LinkTermToActivityNode"
    )
    await postgres_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[potato_term.uid, dried_term.uid],
            linked_node_uid=potato_glossary_link[0].linked_node_uid,
        )
    )

    await postgres_db.graph_mgr.upsert_node_by_uid(convenience_product)

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert len(calculation.mutations) > 5
    assert ImpactAssessmentGapFillingWorker.__name__ in [
        mutation.created_by_module for mutation in calculation.mutations
    ]

    root_impact_assessments_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_impact_assessments_term.access_group_uid)
    ]

    result_co2 = (
        root_node.get_sub_nodes()[0]
        .impact_assessment.amount_for_activity_production_amount()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value
    )

    assert convenience_product.amount_in_original_source_unit.unit == "gram", "should have correct unit"

    # calculated with running the same calculation without the dried tag on the convenience product
    co2_value_without_drying = 0.6001759300599454
    # calculated by adding the dried tag but turning off the processing-gfm
    co2_value_because_of_carrot_upscaling = 1.3737240000000002
    # calculated by adding the dried tag and running the processing-gfm
    # Note that processing-gfm will only be executed on the carrot ingredient, since the potato ingredient
    # has no nutrient-upscaling ration (as the nutrient-profile already comes from a dried product).
    co2_value_because_of_drying_process = 0.40184100000000034

    expected_co2 = (
        co2_value_without_drying + co2_value_because_of_carrot_upscaling + co2_value_because_of_drying_process
    )
    assert is_close(result_co2, expected_co2), "root node should have correct CO2 amount"
