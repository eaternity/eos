import asyncio
from typing import Tuple

import pytest
import structlog

from core.service.messaging_service import MessagingService
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = structlog.get_logger()


@pytest.mark.asyncio
async def test_messaging_service(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Tests basic send/receive operations of MessagingService."""
    postgres_db, service_provider = setup_services

    service_provider.messaging_service = MessagingService(service_provider)
    await service_provider.messaging_service.start()

    obj_identifier = b"TESTING"
    received_identifier = None

    async def on_cache_invalidation_msg(msg_bytes: bytes) -> None:
        nonlocal received_identifier
        print("received on_cache_invalidation_msg")
        received_identifier = msg_bytes

    await service_provider.messaging_service.subscribe_cache_invalidation("term", on_cache_invalidation_msg)

    # send an invalidation message:
    await service_provider.messaging_service.emit_invalidate("term", obj_identifier)

    # wait maximum of 10 seconds
    for i in range(100):
        await asyncio.sleep(0.1)

        if received_identifier is not None:
            logger.debug(f"received message after {i/10} seconds.")
            break

    # check if we received the message:
    assert received_identifier == obj_identifier

    await service_provider.messaging_service.stop()
    service_provider.messaging_service = None
