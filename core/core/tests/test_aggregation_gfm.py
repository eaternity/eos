"""Test Aggregation gap filling module."""

import uuid
from typing import Tuple

import pytest
from gap_filling_modules.aggregation_gfm import AggregationGapFillingFactory
from gap_filling_modules.ingredicalc.helpers import nutrients_dict_to_prop

from core.domain.calculation import Calculation
from core.domain.nodes import FoodProcessingActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.term import Term
from core.graph_manager.sheet_converter import OutputGraphToTableConverter
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.access_group_service import AccessGroupService
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeWithDeclaration, append_flow_node, is_close, prepend_flow_node
from core.tests.test_ingredient_amount_estimator_gfm import nutrients_mixer
from core.tests.test_orchestration_gfm import run_orchestrator_on_recipe
from database.postgres.postgres_db import PostgresDb


async def _run_calculation(
    services: Tuple[PostgresDb, ServiceProvider],
    ingredient_declaration: str,
    nutrient_values: dict[str, float],
    production_amount: float = 100,
) -> Tuple[Node, PostgresDb, ServiceProvider, Calculation]:
    postgres_db, service_provider = services
    glossary_service = service_provider.glossary_service
    graph_mgr = postgres_db.get_graph_mgr()

    amount = {"value": production_amount, "unit": "gram"}

    # Delete fibers information from nutrient declaration to:
    # 1. Keep old test benchmarks.
    # 2. It is not required by law to provide fiber information on food items.
    if "fibers_gram" in nutrient_values:
        del nutrient_values["fibers_gram"]

    recipe = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            production_amount=amount,
            raw_input={
                "titles": [
                    {
                        "language": "en",
                        "value": "A sample recipe with 2 ingredients, to test the ingredient estimator",
                    },
                ]
            },
        )
    )

    root_flow = await prepend_flow_node(graph_mgr, recipe.uid, nutrient_values=nutrient_values)
    ingredients_declaration = [
        {
            "language": "de",
            "value": ingredient_declaration,
        },
    ]
    _ = await append_flow_node(
        graph_mgr,
        recipe.uid,
        ingredients_declaration=ingredients_declaration,
    )

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(
        service_provider,
        glossary_service,
        node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid,
        root_node_uid=root_flow.uid,
        child_of_root_node_uid=recipe.uid,
        return_log=True,
        return_data_as_table=True,
        requested_quantity_references=[ReferenceAmountEnum.amount_for_root_node],
    )

    calculation, root_node = await calc_service.calculate(calculation)

    return root_node, postgres_db, service_provider, calculation


def setup_nutrients_units_food_categories_terms(
    service_provider: ServiceProvider,
) -> Tuple[dict, dict, Term, Term, Term]:
    root_nutr_term = service_provider.glossary_service.root_subterms["EOS_nutrient_names"]
    root_unit_term = service_provider.glossary_service.root_subterms["EOS_units"]
    root_food_categories_term = service_provider.glossary_service.root_subterms["Root_Food_Categories"]

    nutrs = ["fat", "sodium", "energy", "protein", "carbohydrates", "water", "sodium_chloride"]
    nutr_name_to_term = {
        nutr: service_provider.glossary_service.terms_by_xid_ag_uid[
            (f"EOS_{nutr}", root_nutr_term.access_group_uid)
        ].uid
        for nutr in nutrs
    }

    gram_term = service_provider.glossary_service.terms_by_xid_ag_uid[("EOS_gram", root_unit_term.access_group_uid)]
    kcal_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_kilocalorie", root_unit_term.access_group_uid)
    ]
    production_amount_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_production_amount", root_unit_term.access_group_uid)
    ]

    amount_per_category_in_flow = [
        "EOS_Diet-low-in-vegetables",
        "EOS_Diet-high-in-sodium-(salt)",
        "EOS_Plant-based-products",
        "EOS_Vegetables",
        "EOS_Diet-low-in-milk",
    ]
    food_cat_to_term = {
        food_cat: service_provider.glossary_service.terms_by_xid_ag_uid[
            (food_cat, root_food_categories_term.access_group_uid)
        ].uid
        for food_cat in amount_per_category_in_flow
    }
    return nutr_name_to_term, food_cat_to_term, gram_term, kcal_term, production_amount_term


async def check_amounts_per_glossary_link(
    root_node: FoodProductFlowNode, expected_amounts_by_term_xid: dict[frozenset[str], float], postgres_db: PostgresDb
) -> dict[uuid.UUID, float]:
    pg_glossary_link_mgr = postgres_db.get_pg_glossary_link_mgr()
    term_mgr = postgres_db.get_term_mgr()
    all_glossary_links = await pg_glossary_link_mgr.get_data_of_gfm("LinkTermToActivityNode")
    expected_amount_per_glossary_link_required_for_flow: dict[uuid.UUID, float] = {}
    for xids, amount in expected_amounts_by_term_xid.items():
        term_uids = []
        for xid in xids:
            terms = await term_mgr.get_terms_by_xid(xid)
            assert len(terms) == 1, f"Could not find term with xid {xid}"
            term_uids.append(terms[0].uid)
        glossary_links = [gl for gl in all_glossary_links if gl.term_uids == term_uids]
        assert len(glossary_links) == 1, f"Could not find glossary link with term_uids {term_uids}"
        glossary_link = glossary_links[0]
        expected_amount_per_glossary_link_required_for_flow[glossary_link.uid] = amount
    assert root_node.amount_per_glossary_link_required_for_flow is not None
    assert len(root_node.amount_per_glossary_link_required_for_flow.quantities) == len(
        expected_amount_per_glossary_link_required_for_flow
    )
    for term_uid, amount in expected_amount_per_glossary_link_required_for_flow.items():
        assert is_close(
            root_node.amount_per_glossary_link_required_for_flow.amount_for_activity_production_amount()
            .quantities[term_uid]
            .value,
            amount,
        )


@pytest.mark.asyncio
async def test_aggregation_gfm(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """Test two things.

    1) AggregationGFM is only scheduled on the root node
    2) nutrients and food categories are computed correctly (no matrix calculation is done here).
    """
    known_root_diet_low_in_vege = 50
    known_carrot_diet_low_in_vege = 100

    known_root_plant_based = 100
    known_carrot_plant_based = 100
    known_potato_plant_based = 100

    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]

    known_carrot_prot = carrot_nutrients["protein_gram"]
    known_potato_prot = potato_nutrients["protein_gram"]

    known_carrot_fat = carrot_nutrients["fat_gram"]
    known_potato_fat = potato_nutrients["fat_gram"]
    known_root_fat = (known_potato_fat + known_carrot_fat) / 2.0

    known_carrot_salt = carrot_nutrients["sodium_milligram"]
    known_potato_salt = potato_nutrients["sodium_milligram"]
    known_root_salt = (known_potato_salt + known_carrot_salt) / 2.0

    mix_50_50_carrot_potato = nutrients_mixer((carrot_nutrients, potato_nutrients), (0.5, 0.5))
    root_node, postgres_db, service_provider, calculation = await _run_calculation(
        setup_services,
        "Karotten, Kartoffeln",
        mix_50_50_carrot_potato,
    )

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    gap_filling_factory = AggregationGapFillingFactory(postgres_db, service_provider)

    await gap_filling_factory.init_cache()

    mixing_activity = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
    gap_filling_worker = gap_filling_factory.spawn_worker(root_node)
    assert (
        gap_filling_worker.should_be_scheduled()
    ), "GFM has not been scheduled to run, but it should always be scheduled for a root node."

    gap_filling_worker = gap_filling_factory.spawn_worker(mixing_activity.get_sub_nodes()[0].get_sub_nodes()[0])
    assert (
        not gap_filling_worker.should_be_scheduled()
    ), "GFM has been scheduled to run, but it should not be scheduled for a sub-node."

    # set-up the expected dict for the aggregated amounts per category
    # these values were obtained by looking at the calc_graph, finding the corresponding ModeledActivityNodes and
    # summing up the amount.amount_for_root_node() of all parent flows
    expected_amounts_by_term_xid = {
        frozenset(["A1791"]): 50.00000000466878,  # carrot
        frozenset(["A00ZT"]): 49.99999999865705,  # potato
    }

    base_dfu_per_100g = {  # obtained from the calc_graph
        frozenset(["A1791"]): 0.019288297842424244,  # carrot
        frozenset(["A00ZT"]): 0.0311087743030303,  # potato
    }
    # check the results
    await check_amounts_per_glossary_link(root_node, expected_amounts_by_term_xid, postgres_db)

    assert isinstance(root_node, FoodProductFlowNode)

    sum_all_base_products = sum(expected_amounts_by_term_xid.values())
    assert all(
        [
            is_close(qty.value, sum_all_base_products)
            for qty in root_node.amount_per_category_required_for_flow.quantities.values()
        ]
    )
    sum_all_dfus = sum(
        expected_amounts_by_term_xid[k] * base_dfu_per_100g[k] / 100.0 for k in expected_amounts_by_term_xid
    )
    assert all(
        [is_close(qty.value, sum_all_dfus) for qty in root_node.dfu_per_category_required_for_flow.quantities.values()]
    )

    nutrient_values = root_node.nutrient_values.amount_for_activity_production_amount().quantities

    assert nutrient_values[nutr_name_to_term["fat"]].unit_term_uid == gram_term.uid
    assert is_close(nutrient_values[nutr_name_to_term["fat"]].value, known_root_fat)

    assert nutrient_values[nutr_name_to_term["sodium"]].unit_term_uid == gram_term.uid
    assert is_close(nutrient_values[nutr_name_to_term["sodium"]].value, known_root_salt / 1000)

    assert nutrient_values[nutr_name_to_term["energy"]].unit_term_uid == kcal_term.uid

    amount_per_category_in_flow = (
        root_node.amount_per_category_in_flow.amount_for_activity_production_amount().quantities
    )
    assert amount_per_category_in_flow[food_cat_to_term["EOS_Diet-low-in-vegetables"]].unit_term_uid == gram_term.uid
    assert is_close(
        amount_per_category_in_flow[food_cat_to_term["EOS_Diet-low-in-vegetables"]].value, known_root_diet_low_in_vege
    )
    assert amount_per_category_in_flow[food_cat_to_term["EOS_Plant-based-products"]].unit_term_uid == gram_term.uid
    assert is_close(
        amount_per_category_in_flow[food_cat_to_term["EOS_Plant-based-products"]].value, known_root_plant_based
    )

    sub_flow_nutrients = [sn.aggregated_nutrients for sn in mixing_activity.get_sub_nodes()]
    sub_activity_prod_amount = [sn.get_sub_nodes()[0].production_amount.value for sn in mixing_activity.get_sub_nodes()]

    # get nutrient_values per activity_production_amount (assumed to be in kg), divide by the actual production amount
    # and multiply by 0.1 (to convert to 100g)
    assert is_close(
        sub_flow_nutrients[0].amount_for_activity_production_amount().quantities[nutr_name_to_term["protein"]].value
        / mixing_activity.production_amount.value
        * 0.1,
        known_carrot_prot * 0.5,
    )
    assert is_close(
        sub_flow_nutrients[1].amount_for_activity_production_amount().quantities[nutr_name_to_term["protein"]].value
        / mixing_activity.production_amount.value
        * 0.1,
        known_potato_prot * 0.5,
    )
    assert is_close(
        sub_flow_nutrients[0].amount_for_100g().quantities[nutr_name_to_term["protein"]].value
        * 10
        * sub_activity_prod_amount[0],
        known_carrot_prot * 10,
    )
    assert is_close(
        sub_flow_nutrients[1].amount_for_100g().quantities[nutr_name_to_term["protein"]].value
        * 10
        * sub_activity_prod_amount[1],
        known_potato_prot * 10,
    )

    sub_flow_amount_per_category_in_flow = [sn.amount_per_category_in_flow for sn in mixing_activity.get_sub_nodes()]
    assert is_close(
        sub_flow_amount_per_category_in_flow[0]
        .amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Diet-low-in-vegetables"]]
        .value
        / mixing_activity.production_amount.value
        * 0.1,
        known_carrot_diet_low_in_vege * 0.5,
    )
    assert is_close(
        sub_flow_amount_per_category_in_flow[0]
        .amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Plant-based-products"]]
        .value
        / mixing_activity.production_amount.value
        * 0.1,
        known_carrot_plant_based * 0.5,
    )
    assert is_close(
        sub_flow_amount_per_category_in_flow[1]
        .amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Plant-based-products"]]
        .value
        / mixing_activity.production_amount.value
        * 0.1,
        known_potato_plant_based * 0.5,
    )

    assert is_close(
        sub_flow_amount_per_category_in_flow[0]
        .amount_for_100g()
        .quantities[food_cat_to_term["EOS_Diet-low-in-vegetables"]]
        .value
        * 10
        * sub_activity_prod_amount[0],
        known_carrot_diet_low_in_vege * 10,
    )
    assert is_close(
        sub_flow_amount_per_category_in_flow[0]
        .amount_for_100g()
        .quantities[food_cat_to_term["EOS_Plant-based-products"]]
        .value
        * 10
        * sub_activity_prod_amount[0],
        known_carrot_plant_based * 10,
    )
    assert is_close(
        sub_flow_amount_per_category_in_flow[1]
        .amount_for_100g()
        .quantities[food_cat_to_term["EOS_Plant-based-products"]]
        .value
        * 10
        * sub_activity_prod_amount[1],
        known_potato_plant_based * 10,
    )


@pytest.mark.asyncio
async def test_aggregation_missing_water_gfm(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Aggregation occurs correctly by comparing it with ingredient amount estimated version."""
    nutrient_values = {
        "energy_kcal": 100,
        "fat_gram": 0,
        "saturated_fat_gram": 0,
        "carbohydrates_gram": 25,
        "sucrose_gram": 18,
        "protein_gram": 0,
        "sodium_chloride_gram": 0,
    }

    root_node, postgres_db, service_provider, calculation = await _run_calculation(
        setup_services, "Karotten, Kartoffeln", nutrient_values, 90.0
    )

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    # set-up the expected dict for the aggregated amounts per category
    # these values were obtained by looking at the calc_graph, finding the corresponding ModeledActivityNodes and
    # summing up the amount.amount_for_root_node() of all parent flows
    expected_amounts_by_term_xid = {
        frozenset(["A1791"]): 247.1940045510884,  # carrot
        frozenset(["A00ZT"]): 25.318056950739465,  # potato
    }
    base_dfu_per_100g = {  # obtained from the calc_graph
        frozenset(["A1791"]): 0.019288297842424244,  # carrot
        frozenset(["A00ZT"]): 0.0311087743030303,  # potato
    }
    # check the results
    await check_amounts_per_glossary_link(root_node, expected_amounts_by_term_xid, postgres_db)

    assert isinstance(root_node, FoodProductFlowNode)

    assert set(root_node.impact_assessment_per_category.quantities.keys()) == set(
        [
            food_cat_to_term["EOS_Plant-based-products"],
            food_cat_to_term["EOS_Vegetables"],
        ]
    )

    sum_all_base_products = sum(expected_amounts_by_term_xid.values())
    assert all(
        [
            is_close(qty.value, sum_all_base_products)
            for qty in root_node.amount_per_category_required_for_flow.quantities.values()
        ]
    )
    sum_all_dfus = sum(
        expected_amounts_by_term_xid[k] * base_dfu_per_100g[k] / 100.0 for k in expected_amounts_by_term_xid
    )
    assert all(
        [is_close(qty.value, sum_all_dfus) for qty in root_node.dfu_per_category_required_for_flow.quantities.values()]
    )

    mixing_activity = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
    child_of_root_node = root_node.get_sub_nodes()[0]

    for nutr in nutr_name_to_term:
        if nutr == "sodium_chloride":
            continue

        # Check that the production amount is indeed 90g
        activity_amount = sum(
            sn.aggregated_nutrients.amount_for_activity_production_amount().quantities[nutr_name_to_term[nutr]].value
            * child_of_root_node.production_amount.value  # normalize by the total production amount
            for sn in mixing_activity.get_sub_nodes()
        )

        assert is_close(
            root_node.aggregated_nutrients.amount_for_100g().quantities[nutr_name_to_term[nutr]].value * 0.9,
            activity_amount,
        )
        # Aggregated and declared nutrient values are not the same.
        if nutr not in ("water", "sodium", "chlorine"):
            assert not is_close(
                root_node.aggregated_nutrients.amount_for_100g().quantities[nutr_name_to_term[nutr]].value,
                root_node.nutrient_values.amount_for_100g().quantities[nutr_name_to_term[nutr]].value,
            )
        assert is_close(
            root_node.aggregated_nutrients.amount_for_100g().quantities[nutr_name_to_term[nutr]].value,
            child_of_root_node.ingredient_amount_estimator_estimated_nutrients.quantities[
                nutr_name_to_term[nutr]
            ].value,
        )


@pytest.mark.asyncio
async def test_aggregation_gfm_with_combined_ingredients(
    setup_services: Tuple[PostgresDb, ServiceProvider], load_seeded_nutrients: dict[str, dict[str, float]]
) -> None:
    """If nutrients can be correctly aggregated for combined food products."""
    # these reference values are directly taken from the calc_graph
    carrot_amount = 48.642385243883375 / 100
    potato_amount = 1.357614773213417 / 100
    onion_amount = (50 - 2.3072096063692278e-07) / 100
    tomato_amount = 2.3072096063692278e-07 / 100
    raw_onion_ratio = (48.7671549761338 / 100) / onion_amount
    dried_onion_ratio = 1 - raw_onion_ratio
    upscale_ratio = 5.025125628140706

    total_rainforest_critical_product_amount = (
        onion_amount * (raw_onion_ratio * 40 + upscale_ratio * dried_onion_ratio * 40) + carrot_amount * 35
    )

    carrot_nutrients = load_seeded_nutrients["carrot"]
    potato_nutrients = load_seeded_nutrients["potato"]
    onion_nutrients = load_seeded_nutrients["onion"]
    tomato_nutrients = load_seeded_nutrients["tomato"]
    root_node, postgres_db, service_provider, calculation = await _run_calculation(
        setup_services,
        "MainIngrOne (Karotten, Kartoffeln), MainIngrTwo (Zwiebeln, Tomaten)",
        onion_nutrients,
    )

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    # set-up the expected dict for the aggregated amounts per category
    expected_amounts_by_term_xid = {
        frozenset(["A1791"]): carrot_amount * 100,  # carrot
        frozenset(["A00ZT"]): potato_amount * 100,  # potato
        frozenset(["A1480"]): onion_amount * (raw_onion_ratio + upscale_ratio * dried_onion_ratio) * 100,  # onion
        frozenset(["A0DMX"]): tomato_amount * 100,  # tomato
    }
    base_dfu_per_100g = {  # obtained from the calc_graph
        frozenset(["A1791"]): 0.019288297842424244,  # carrot
        frozenset(["A00ZT"]): 0.0311087743030303,  # potato
        frozenset(["A1480"]): 0.021042596872727273,  # onion
        frozenset(["A0DMX"]): 0.01582301970909091,  # tomato
    }
    # check the results
    await check_amounts_per_glossary_link(root_node, expected_amounts_by_term_xid, postgres_db)

    final_graph_table = calculation.final_graph_table
    kg_co2eq_only_base_per_root_node_in_final_graph = final_graph_table[0]["kg_co2eq_only_base_per_root_node"]
    assert kg_co2eq_only_base_per_root_node_in_final_graph > 0.0

    assert isinstance(root_node, FoodProductFlowNode)
    sum_impact_assessment_per_glossary_link = sum(
        qty.value for qty in root_node.impact_assessment_per_glossary_link.amount_for_root_node().quantities.values()
    )
    assert is_close(kg_co2eq_only_base_per_root_node_in_final_graph, sum_impact_assessment_per_glossary_link)

    assert set(root_node.impact_assessment_per_category.quantities.keys()) == set(
        [
            food_cat_to_term["EOS_Plant-based-products"],
            food_cat_to_term["EOS_Vegetables"],
        ]
    )

    assert all(
        is_close(qty.value, kg_co2eq_only_base_per_root_node_in_final_graph)
        for qty in root_node.impact_assessment_per_category.quantities.values()
    )

    sum_all_base_products = sum(expected_amounts_by_term_xid.values())
    assert all(
        [
            is_close(qty.value, sum_all_base_products)
            for qty in root_node.amount_per_category_required_for_flow.quantities.values()
        ]
    )
    sum_all_dfus = sum(
        expected_amounts_by_term_xid[k] * base_dfu_per_100g[k] / 100.0 for k in expected_amounts_by_term_xid
    )
    assert all(
        [is_close(qty.value, sum_all_dfus) for qty in root_node.dfu_per_category_required_for_flow.quantities.values()]
    )

    mixing_activity = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]

    known_main_ingr1_fat = carrot_amount * carrot_nutrients["fat_gram"] + potato_amount * potato_nutrients["fat_gram"]
    known_main_ingr2_fat = (
        onion_amount * dried_onion_ratio * upscale_ratio * onion_nutrients["fat_gram"]
        + onion_amount * raw_onion_ratio * onion_nutrients["fat_gram"]
        + tomato_amount * tomato_nutrients["fat_gram"]
    )

    known_root_diet_low_in_vege = (carrot_amount + onion_amount + tomato_amount) * 100

    main_ingr1_flow_nutrients = mixing_activity.get_sub_nodes()[0].aggregated_nutrients
    main_ingr2_flow_nutrients = mixing_activity.get_sub_nodes()[1].aggregated_nutrients
    main_ingr1_node_nutrients = [
        sn.aggregated_nutrients for sn in mixing_activity.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()
    ]
    main_ingr2_node_nutrients = [
        sn.aggregated_nutrients for sn in mixing_activity.get_sub_nodes()[1].get_sub_nodes()[0].get_sub_nodes()
    ]

    root_node_amount_per_category_in_flow = root_node.amount_per_category_in_flow
    access_group_uid = service_provider.glossary_service.root_term.access_group_uid
    not_certified_for_rainforest_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("EOS_not_certified_for_rainforest", access_group_uid)
    ]

    assert is_close(
        root_node.rainforest_critical_products.quantities[not_certified_for_rainforest_term.uid].value,
        total_rainforest_critical_product_amount,
    )

    assert (
        main_ingr1_node_nutrients[0]
        .amount_for_activity_production_amount()
        .quantities[nutr_name_to_term["fat"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        sum(
            nutr.amount_for_activity_production_amount().quantities[nutr_name_to_term["fat"]].value
            for nutr in main_ingr1_node_nutrients
        ),
        known_main_ingr1_fat * 20,
    )

    assert (
        main_ingr2_node_nutrients[0]
        .amount_for_activity_production_amount()
        .quantities[nutr_name_to_term["fat"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        sum(
            nutr.amount_for_activity_production_amount().quantities[nutr_name_to_term["fat"]].value
            for nutr in main_ingr2_node_nutrients
        ),
        known_main_ingr2_fat * 20,
    )

    assert (
        main_ingr1_flow_nutrients.amount_for_activity_production_amount()
        .quantities[nutr_name_to_term["fat"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        main_ingr1_flow_nutrients.amount_for_activity_production_amount().quantities[nutr_name_to_term["fat"]].value
        * 0.1,
        known_main_ingr1_fat,
    )

    assert (
        main_ingr2_flow_nutrients.amount_for_activity_production_amount()
        .quantities[nutr_name_to_term["fat"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        main_ingr2_flow_nutrients.amount_for_activity_production_amount().quantities[nutr_name_to_term["fat"]].value
        * 0.1,
        known_main_ingr2_fat,
    )

    assert (
        root_node_amount_per_category_in_flow.amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Diet-low-in-vegetables"]]
        .unit_term_uid
        == gram_term.uid
    )
    assert is_close(
        root_node_amount_per_category_in_flow.amount_for_activity_production_amount()
        .quantities[food_cat_to_term["EOS_Diet-low-in-vegetables"]]
        .value,
        known_root_diet_low_in_vege,
    )

    dried_carrot_node = mixing_activity
    for _ in range(4):
        dried_carrot_node = dried_carrot_node.get_sub_nodes()[0]
    dried_carrot_node = dried_carrot_node.get_sub_nodes()[1].get_sub_nodes()[0]

    carrot_node = mixing_activity
    for _ in range(4):
        carrot_node = carrot_node.get_sub_nodes()[0]

    dried_carrot_node_nutrients = dried_carrot_node.get_parent_nodes()[0].nutrient_values
    dried_carrot_flow = dried_carrot_node.get_parent_nodes()[0]

    dried_carrot_nutrient_values = dried_carrot_flow.nutrient_values
    dried_carrot_fat = dried_carrot_nutrient_values.quantities[nutr_name_to_term["fat"]]

    assert (
        dried_carrot_node_nutrients.amount_for_100g().quantities[nutr_name_to_term["fat"]].unit_term_uid
        == gram_term.uid
    )

    assert is_close(
        dried_carrot_node_nutrients.amount_for_100g().quantities[nutr_name_to_term["fat"]].value,
        dried_carrot_fat.value,
    )

    carrot_bw_activity = mixing_activity
    for _ in range(6):
        carrot_bw_activity = carrot_bw_activity.get_sub_nodes()[0]

    bw_parent_flow = carrot_bw_activity.get_parent_nodes()[0]

    assert not bw_parent_flow.is_dried()

    carrot_bw_flow_nutrients = bw_parent_flow.nutrient_values.amount_for_activity_production_amount().quantities
    carrot_node_amount_per_category_in_flow = (
        carrot_node.get_parent_nodes()[0].amount_per_category_in_flow.amount_for_100g().quantities
    )

    carrot_amount_per_category_in_flow = {
        food_cat_to_term["EOS_Plant-based-products"]: {"value": 100, "unit": "g"},
        food_cat_to_term["EOS_Vegetables"]: {"value": 100, "unit": "g"},
        food_cat_to_term["EOS_Diet-low-in-vegetables"]: {"value": 100, "unit": "g"},
    }

    gap_filling_factory = AggregationGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    diff_between_amount_per_category_in_flow = 0
    for key, val in carrot_amount_per_category_in_flow.items():
        diff_between_amount_per_category_in_flow += abs(
            val["value"] - carrot_node_amount_per_category_in_flow[key].value
        )
    assert is_close(diff_between_amount_per_category_in_flow, 0.0)

    diff_between_nutrients = 0
    for key, val in nutrients_dict_to_prop(carrot_nutrients).quantities.items():
        # Factor of 10 comes in because it is nutrients for 1 kg and 100 g.
        diff_between_nutrients += abs(val.value - carrot_bw_flow_nutrients[key].value / 10)
    assert is_close(diff_between_nutrients, 0.0)


@pytest.mark.asyncio
async def test_aggregation_gfm_with_minimum_recipe(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    pg_db, service_provider, recipe, root_flow, _sub_flow = create_minimum_recipe_with_small_declaration
    service_provider.access_group_service = AccessGroupService(service_provider)
    await service_provider.access_group_service.init()

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    calc_graph, _ = await run_orchestrator_on_recipe(
        service_provider,
        pg_db,
        root_flow,
        recipe,
        return_log=True,
        requested_quantity_references=[ReferenceAmountEnum.amount_for_root_node.value],
    )

    final_graph_table = OutputGraphToTableConverter().run(
        calc_graph,
        references=[ReferenceAmountEnum.amount_for_root_node.value],
    )

    root_node = calc_graph.get_root_node()

    kg_co2eq_only_base_per_root_node_in_final_graph = final_graph_table[0]["kg_co2eq_only_base_per_root_node"]
    assert kg_co2eq_only_base_per_root_node_in_final_graph > 0.0

    assert isinstance(root_node, FoodProductFlowNode)
    sum_impact_assessment_per_glossary_link = sum(
        qty.value for qty in root_node.impact_assessment_per_glossary_link.amount_for_root_node().quantities.values()
    )
    assert is_close(kg_co2eq_only_base_per_root_node_in_final_graph, sum_impact_assessment_per_glossary_link)

    assert set(root_node.impact_assessment_per_category.quantities.keys()) == set(
        [
            food_cat_to_term["EOS_Plant-based-products"],
            food_cat_to_term["EOS_Vegetables"],
        ]
    )

    assert all(
        is_close(qty.value, kg_co2eq_only_base_per_root_node_in_final_graph)
        for qty in root_node.impact_assessment_per_category.quantities.values()
    )

    child_of_root_node = calc_graph.child_of_root_node()
    mixing_activity = child_of_root_node.get_sub_nodes()[0].get_sub_nodes()[0]

    nutrients_from_aggregation_gfm = root_node.nutrient_values.amount_for_activity_production_amount()

    assert len(calc_graph.data_errors_log) == 0

    assert nutrients_from_aggregation_gfm

    nutrients_from_ingredient_amount_estimator_gfm = (
        child_of_root_node.ingredient_amount_estimator_estimated_nutrients.amount_for_100g().quantities
    )

    nutrients_quantities = nutrients_from_aggregation_gfm.quantities

    gap_filling_factory = AggregationGapFillingFactory(pg_db, service_provider)
    await gap_filling_factory.init_cache()

    diff_between_nutrients = 0
    for key, val in nutrients_from_ingredient_amount_estimator_gfm.items():
        # Factor of 10 comes in because it is nutrients for 1 kg and 100 g.
        diff_between_nutrients += abs(val.value - nutrients_quantities[key].value / 10)

    assert is_close(
        diff_between_nutrients, 0.0
    ), "nutrients estimated by ingredient amount estimator differs from that of daily food unit gap-filling module."

    flow = mixing_activity.get_sub_nodes()[0]
    flow_nutrients_declared = flow.nutrient_values.quantities
    flow_nutrients_from_aggregation = flow.aggregated_nutrients.amount_for_activity_production_amount().quantities

    diff_between_nutrients = 0
    for key, val in flow_nutrients_declared.items():
        # Factor of 10 comes in because it is nutrients for 1 kg and 100 g.
        diff_between_nutrients += abs(val.value - flow_nutrients_from_aggregation[key].value / 10)

    assert is_close(
        diff_between_nutrients, 0.0
    ), "nutrients from eurofir differs from that of daily food unit gap-filling module."

    recipe_sub_nodes = mixing_activity.get_sub_nodes()

    sub_flow_lcl_plant = [
        sn.amount_per_category_in_flow.amount_for_activity_production_amount()
        .quantities.get(food_cat_to_term["EOS_Plant-based-products"])
        .value
        for sn in recipe_sub_nodes
    ]
    sub_flow_root_plant = [
        sn.amount_per_category_in_flow.amount_for_root_node()
        .quantities.get(food_cat_to_term["EOS_Plant-based-products"])
        .value
        for sn in recipe_sub_nodes
    ]

    parent_supply_for_root = (
        child_of_root_node.get_sub_nodes()[0].get_sub_nodes()[0].impact_assessment_supply_for_root.value
    )

    assert is_close(
        sum([abs(r - lp * parent_supply_for_root) for (r, lp) in zip(sub_flow_root_plant, sub_flow_lcl_plant)]), 0.0
    ), "Plant based products not scaled correctly for root."
