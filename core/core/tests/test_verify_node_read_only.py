import uuid
from typing import Tuple

import pytest

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.props import QuantityPackageProp, QuantityProp, ReferencelessQuantityProp
from core.domain.props.quantity_prop import RawQuantity, ReferenceAmountEnum
from core.envprops import EnvProps
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.duplicate_node_mutation import DuplicateNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb


@pytest.mark.asyncio
async def test_verify_node_read_only(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    assert EnvProps().VERIFY_NODE_READ_ONLY, "For development, you must set VERIFY_NODE_READ_ONLY=true in .env"

    pg_db, service_provider = setup_services

    unit_ag = service_provider.glossary_service.root_subterms["EOS_units"].access_group_uid
    gram_unit_term = service_provider.glossary_service.terms_by_xid_ag_uid[("EOS_gram", unit_ag)]

    graph_mgr = pg_db.get_graph_mgr()
    root_process = await graph_mgr.upsert_node_by_uid(
        FoodProcessingActivityNode(
            production_amount=RawQuantity(value=1.0, unit="kilogram"),
        )
    )
    service_provider.node_service.cache.add_node(root_process)
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_process.uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.apply_mutation(AddNodeMutation(new_node=root_process, copy=True))
    child_flow = FoodProductFlowNode(
        uid=uuid.uuid4(),
    )
    calc_graph.apply_mutation(AddNodeMutation(parent_node_uid=root_process.uid, new_node=child_flow, copy=True))
    amount = QuantityProp(
        value=1.0,
        unit_term_uid=gram_unit_term.uid,
        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
    )
    random_quantity_uid = uuid.uuid4()
    random_quantity_package = QuantityPackageProp(
        quantities={
            random_quantity_uid: ReferencelessQuantityProp.unvalidated_construct(
                value=0.0, unit_term_uid=gram_unit_term.uid
            )
        },
        for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
    )
    calc_graph.apply_mutation(PropMutation(node_uid=child_flow.uid, prop_name="amount", prop=amount))
    calc_graph.apply_mutation(
        PropMutation(node_uid=child_flow.uid, prop_name="nutrient_values", prop=random_quantity_package)
    )
    child_flow_duplicate_uid = uuid.uuid4()
    calc_graph.apply_mutation(
        DuplicateNodeMutation(
            duplicate_child_nodes=False,
            parent_node_uid=root_process.uid,
            source_node_uid=child_flow.uid,
            new_node_uid=child_flow_duplicate_uid,
        )
    )

    # This modification should not be allowed since it will modify child_flow.amount
    with pytest.raises(AttributeError):
        amount.value = 5.0

    child_flow_duplicated = calc_graph.get_node_by_uid(child_flow_duplicate_uid)

    # This modification should not be allowed since it will modify child_flow.amount
    with pytest.raises(AttributeError):
        child_flow_duplicated.nutrient_values.quantities[random_quantity_uid].value = 7.0

    # This modification should not be allowed since it will modify cached production_amount
    with pytest.raises(AttributeError):
        calc_graph.get_root_node().production_amount.value = 3.0

    # This modification should not be allowed since it will modify production_amount in calc graph.
    with pytest.raises(AttributeError):
        root_process.production_amount.unit = "gram"
