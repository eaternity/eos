"Test Calculation queue."
import asyncio
import uuid
from multiprocessing import Process, Queue, Value
from typing import Any
from unittest.mock import patch

import pytest
import structlog
from structlog.contextvars import bind_contextvars, clear_contextvars

from core.domain.calculation import Calculation
from core.service.calc_service import OrchestratorAtLimitException
from core.service.messaging_service import MessagingService, Priority
from core.service.service_provider import ServiceLocator, ServiceProvider
from core.tests.conftest import MinimumRecipeRType

logger = structlog.get_logger()


@pytest.mark.asyncio
@pytest.mark.parametrize("return_log", [True, False])
async def test_calculation_queue(create_minimum_recipe: MinimumRecipeRType, return_log: bool) -> None:
    """Tests basic queue of calculations."""
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_flow = create_minimum_recipe

    service_provider.messaging_service = MessagingService(service_provider)
    await service_provider.messaging_service.start()
    await service_provider.gap_filling_module_loader.init(service_provider)
    await service_provider.messaging_service.start_worker_channel()

    request_id = str(uuid.uuid4())
    bind_contextvars(request_id=request_id)

    # create a calculation
    calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=return_log
    )
    await service_provider.postgres_db.pg_calc_mgr.insert_calculation(calculation)

    # now enqueue the calculation for processing
    calculation = await service_provider.messaging_service.enqueue_calculation(calculation)

    if return_log:
        assert len(calculation.mutations) > 5
    else:
        assert len(calculation.mutations) == 0

    await service_provider.messaging_service.stop()
    service_provider.messaging_service = None

    clear_contextvars()


@pytest.mark.asyncio
@pytest.mark.parametrize("failure_mode", ["OOMKill", "OrchestratorAtLimitException", "OtherException"])
async def test_calculation_queue_retries(create_minimum_recipe: MinimumRecipeRType, failure_mode: str) -> None:
    """Tests that calculations aren't requeued indefinitely when OOM occurs."""
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_flow = create_minimum_recipe

    max_retries_on_oomkill = 2
    simulated_high_watermarks = 4

    service_provider.messaging_service = MessagingService(service_provider)
    await service_provider.messaging_service.start()

    # disable consuming worker messages in this instance,
    # because we only want to submit work from this main process:
    service_provider.messaging_service.settings.RABBITMQ_WORKER_PREFETCH_COUNT = 0
    service_provider.messaging_service.settings.RABBITMQ_WORKER_MAX_RETRIES = max_retries_on_oomkill

    await service_provider.messaging_service.start_worker_channel()

    request_id = str(uuid.uuid4())
    bind_contextvars(request_id=request_id)

    # Create a calculation
    calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=False
    )
    await service_provider.postgres_db.pg_calc_mgr.insert_calculation(calculation)

    calc_count = Value("i", 0)
    ready_queue = Queue()

    def run_worker_process(calc_uid: uuid.UUID, calc_count: Value, ready_queue: Queue) -> None:
        """Run the worker process that will handle calculations."""
        import asyncio

        async def worker_main() -> None:
            # Create new service provider for this process
            service_locator = ServiceLocator()
            service_locator.recreate()
            worker_service_provider: ServiceProvider = service_locator.service_provider
            await worker_service_provider.postgres_db.connect(schema="test_pg")

            async def mock_calculate(*args: Any, **kwargs: Any) -> None:  # noqa: ANN401
                with calc_count.get_lock():
                    calc_count.value += 1
                    count = calc_count.value

                if count < simulated_high_watermarks:
                    if failure_mode == "OOMKill":
                        # Simulate process death
                        import os

                        os._exit(1)
                    elif failure_mode == "OrchestratorAtLimitException":
                        raise OrchestratorAtLimitException("Simulated OOM")
                    elif failure_mode == "OtherException":
                        raise Exception("Simulated other exception")

            # Patch the calculate method
            with patch.object(worker_service_provider.calc_service, "calculate", side_effect=mock_calculate):
                try:
                    worker_service_provider.messaging_service.settings.RABBITMQ_WORKER_PREFETCH_COUNT = 2
                    worker_service_provider.messaging_service.settings.RABBITMQ_WORKER_MAX_RETRIES = (
                        max_retries_on_oomkill
                    )
                    await worker_service_provider.messaging_service.start()
                    await worker_service_provider.messaging_service.start_worker_channel()

                    # Signal that this worker is ready
                    ready_queue.put(True)

                    # Keep the worker running until the process is terminated
                    while True:
                        await asyncio.sleep(0.1)
                finally:
                    await worker_service_provider.messaging_service.stop()

        asyncio.run(worker_main())

    # Start worker processes
    processes = []
    num_workers = max_retries_on_oomkill + 1
    for _ in range(num_workers):  # Start worker processes
        p = Process(target=run_worker_process, args=(calculation_uid, calc_count, ready_queue))
        p.start()
        processes.append(p)

    # Wait for all workers to be ready
    for _ in range(num_workers):
        ready_queue.get()

    # Now that all workers are ready, enqueue the calculation
    calculation_result = await service_provider.messaging_service.enqueue_calculation(calculation)
    assert calculation_result.success is False, "The calculation should have failed."

    # Wait for processes to finish
    for p in processes:
        p.join(timeout=1)
        if p.is_alive():
            p.terminate()

    # Check how many times the calculation was attempted:
    final_count = calc_count.value
    if failure_mode == "OOMKill":
        assert (
            final_count == max_retries_on_oomkill
        ), f"There should have been {max_retries_on_oomkill} attempts before failing"
    elif failure_mode == "OrchestratorAtLimitException":
        assert (
            final_count == simulated_high_watermarks
        ), f"There should have been {simulated_high_watermarks} attempts before failing"
    elif failure_mode == "OtherException":
        assert (
            final_count == 1
        ), "There should have been only 1 calculation attempt when there is another uncaught exception"

    await service_provider.messaging_service.stop()
    service_provider.messaging_service = None
    clear_contextvars()


class CalculationConfig:
    def __init__(self, uuid: uuid.UUID, priority: Priority) -> None:
        self.uuid = uuid
        self.priority = priority


async def run_prioritized_calculations(
    calculation_configs: list[CalculationConfig], create_minimum_recipe: MinimumRecipeRType, num_workers: int = 2
) -> list[uuid.UUID]:
    """Helper to test priorization of calculations.

    return: list of calculation_uids in the order they were started
    """
    long_running_delay = 10
    short_running_delay = 0.1
    enqueue_delay = 1

    _, service_provider, recipe, _, _, root_flow = create_minimum_recipe

    service_provider.messaging_service = MessagingService(service_provider)
    await service_provider.messaging_service.start()

    # disable consuming worker messages in this instance,
    # because we only want to submit work from this main process:
    service_provider.messaging_service.settings.RABBITMQ_WORKER_PREFETCH_COUNT = 0

    await service_provider.messaging_service.start_worker_channel()

    # bind request_id
    request_id = str(uuid.uuid4())
    bind_contextvars(request_id=request_id)

    # Create calculations
    calculation_long_running = {}
    calculations = {}
    for calc_config in calculation_configs:
        calculations[calc_config.uuid] = Calculation(
            uid=calc_config.uuid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=False
        )

        # set low prio calculations as long running by default
        calculation_long_running[calc_config.uuid] = calc_config.priority == Priority.LOW
        await service_provider.postgres_db.pg_calc_mgr.insert_calculation(calculations[calc_config.uuid])

    ready_queue = Queue()
    calculation_started_queue = Queue()

    def run_worker_process(ready_queue: Queue, calculation_uid_queue: Queue) -> None:
        """Run the worker process that will handle calculations."""
        import asyncio

        async def worker_main() -> None:
            # Create new service provider for this process
            service_locator = ServiceLocator()
            service_locator.recreate()
            worker_service_provider: ServiceProvider = service_locator.service_provider
            await worker_service_provider.postgres_db.connect(schema="test_pg")

            async def mock_calculate(*args: Any, **kwargs: Any) -> None:  # noqa: ANN401
                calculation_uid = args[0].uid
                long_running = calculation_long_running[calculation_uid]
                logger.info("mock_calculate start", calculation_uid=calculation_uid, long_running=long_running)
                calculation_uid_queue.put(calculation_uid)
                if long_running:
                    await asyncio.sleep(long_running_delay)
                else:
                    asyncio.sleep(short_running_delay)
                logger.info("mock_calculate done", calculation_uid=calculation_uid)
                return args[0], None

            # Replace the calculate method
            with patch.object(worker_service_provider.calc_service, "calculate", mock_calculate):
                try:
                    worker_service_provider.messaging_service.settings.RABBITMQ_RETURN_CALCULATION = True
                    worker_service_provider.messaging_service.settings.RABBITMQ_WORKER_PREFETCH_COUNT = 1
                    await worker_service_provider.messaging_service.start()
                    await worker_service_provider.messaging_service.start_worker_channel()

                    # Signal that this worker is ready
                    ready_queue.put(True)

                    # Keep the worker running until the process is terminated
                    while True:
                        await asyncio.sleep(0.1)
                finally:
                    await worker_service_provider.messaging_service.stop()

        asyncio.run(worker_main())

    # Start worker processes
    processes = []
    for _ in range(num_workers):  # Start worker processes
        p = Process(target=run_worker_process, args=[ready_queue, calculation_started_queue])
        p.start()
        processes.append(p)

    # Wait for all workers to be ready
    for _ in range(num_workers):
        ready_queue.get()

    # Enqueue calculations in the specified order
    enqueued_calculations = Queue()

    async def enqueue_and_register(calc_config: CalculationConfig) -> None:
        enqueued_calculations.put(calc_config.uuid)
        await service_provider.messaging_service.enqueue_calculation(
            calculations[calc_config.uuid], calc_config.priority
        )

    async with asyncio.TaskGroup() as tg:
        for calc_config in calculation_configs:
            tg.create_task(enqueue_and_register(calc_config))
            # give a little headroom to make sure the queue input is ordered as expected
            await asyncio.sleep(enqueue_delay)

    # Enqueueing completed, wait for processes to finish
    upper_duration_limit = sum(
        short_running_delay if calc_config.priority == Priority.HIGH else long_running_delay
        for calc_config in calculation_configs
    )
    for p in processes:
        p.join(timeout=upper_duration_limit)
        if p.is_alive():
            p.terminate()

    # ensure correct test setup
    # 1. enqueue order was as expected
    enqueued_uids = []
    while not enqueued_calculations.empty():
        enqueued_uids.append(enqueued_calculations.get_nowait())
    assert len(enqueued_uids) == len(calculation_configs), "unexpected number of calculations were enqueued"
    for calc_config, enqueued_uid in zip(calculation_configs, enqueued_uids):
        assert calc_config.uuid == enqueued_uid, "calculation was not enqueued at the expected time"

    # 2. all calculations were started
    started_uids = []
    while not calculation_started_queue.empty():
        started_uids.append(calculation_started_queue.get_nowait())
    assert len(started_uids) == len(calculation_configs), "unexpected number of calculations were started"

    return started_uids


@pytest.mark.asyncio
async def test_high_priority_calculation_preferred(create_minimum_recipe: MinimumRecipeRType) -> None:
    """High prio calculation has to be preferred once a worker becomes available."""
    priorities = [Priority.LOW, Priority.LOW, Priority.LOW, Priority.LOW, Priority.HIGH]
    calculation_configs = [CalculationConfig(uuid.uuid4(), p) for p in priorities]
    started_calculation_uids = await run_prioritized_calculations(
        calculation_configs, create_minimum_recipe, num_workers=2
    )

    # first two calculations should be done first, then workers are busy while more calculations are enqueued
    # and should prefer the high prio one as soon as they become ready again
    assert started_calculation_uids[0] == calculation_configs[0].uuid
    assert started_calculation_uids[1] == calculation_configs[1].uuid
    assert started_calculation_uids[2] == calculation_configs[4].uuid, "high prio calculation was not preferred"
    assert started_calculation_uids[3] == calculation_configs[2].uuid
    assert started_calculation_uids[4] == calculation_configs[3].uuid


@pytest.mark.asyncio
async def test_high_priority_calculation_will_wait(create_minimum_recipe: MinimumRecipeRType) -> None:
    """High prio calculation still needs to wait if low prio ones were here before."""
    priorities = [Priority.LOW, Priority.LOW, Priority.HIGH]
    calculation_configs = [CalculationConfig(uuid.uuid4(), p) for p in priorities]
    started_calculation_uids = await run_prioritized_calculations(
        calculation_configs, create_minimum_recipe, num_workers=2
    )

    # first two calculations should be done first, then workers are busy while more calculations are enqueued
    # and will work on the high prio one only after the low prio calculations complete
    assert started_calculation_uids[0] == calculation_configs[0].uuid
    assert started_calculation_uids[1] == calculation_configs[1].uuid
    assert started_calculation_uids[2] == calculation_configs[2].uuid, "high prio calculation did not have to wait"


@pytest.mark.asyncio
async def test_low_priority_calculation_will_starve(create_minimum_recipe: MinimumRecipeRType) -> None:
    """Low prio calculation will starve if queue is busy with high prio calculations."""
    priorities = [
        Priority.LOW,
        Priority.LOW,
        Priority.LOW,
        Priority.HIGH,
        Priority.HIGH,
        Priority.HIGH,
        Priority.HIGH,
        Priority.HIGH,
        Priority.HIGH,
    ]
    calculation_configs = [CalculationConfig(uuid.uuid4(), p) for p in priorities]
    started_calculation_uids = await run_prioritized_calculations(
        calculation_configs, create_minimum_recipe, num_workers=2
    )

    # first two calculations should be done first, then workers are busy while more calculations are enqueued
    # and will only work on the low prio one once no high prio ones are available any more
    assert started_calculation_uids[0] == calculation_configs[0].uuid
    assert started_calculation_uids[1] == calculation_configs[1].uuid
    assert started_calculation_uids[-1] == calculation_configs[2].uuid, "low prio calculation was not started last"
