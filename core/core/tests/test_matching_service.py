"Tests for Matching Service."
import copy
from typing import Any
from unittest.mock import patch

import httpx
import pytest
import structlog

from core.domain.matching_item import MatchingItem
from core.tests.conftest import MinimumRecipeRType

logger = structlog.get_logger()


@pytest.mark.asyncio
async def test_binary_cache_available(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    matching_service = service_provider.matching_service

    assert matching_service._terms_by_matching_str, "Matching service not initialized"

    # stash away terms that are originally loaded
    original_binary_cache = copy.deepcopy(matching_service._terms_by_matching_str_binary)

    def get_cache_mock(**kwargs: dict[str, Any]) -> None:
        return httpx.Response(status_code=200, content=original_binary_cache)

    with patch.object(httpx, "get", new=get_cache_mock):
        # initialize terms via binary cache
        matching_service.clear_cache()
        matching_service._get_terms_by_matching_str_from_running_pods()
        terms_from_cache = copy.deepcopy(matching_service._terms_by_matching_str)

        # initialize terms via DB
        matching_service.clear_cache()
        all_items: list[MatchingItem] = await postgres_db.pg_matching_mgr.get_matching_items()
        for item in all_items:
            matching_service._update_matching_item_in_cache(item)
        terms_from_db = copy.deepcopy(matching_service._terms_by_matching_str)

        assert terms_from_cache == terms_from_db, "terms from cache and DB do not match"


@pytest.mark.asyncio
async def test_binary_cache_unavailable(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    matching_service = service_provider.matching_service

    assert matching_service._terms_by_matching_str, "Matching service not initialized"

    def get_cache_mock(**kwargs: dict[str, Any]) -> None:
        return httpx.Response(status_code=408)

    # Patch the cache endpoint
    with patch.object(httpx, "get", new=get_cache_mock):
        with pytest.raises(Exception):  # noqa: B017
            matching_service._get_terms_by_matching_str_from_running_pods()
