"Test Water scarcity gap filling module."
from typing import List

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingFactory
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingFactory
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingFactory
from gap_filling_modules.water_scarcity_gfm import WaterScarcityGapFillingFactory

from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.props import GfmStateProp, LocationProp
from core.graph_manager.calc_graph import CalcGraph
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeRType
from database.postgres.postgres_db import PostgresDb

from .conftest import is_close


async def setup_and_run_water_scarcity_gfm(
    postgres_db: PostgresDb,
    service_provider: ServiceProvider,
    carrot_ingredient: FoodProductFlowNode,
    location: List[LocationProp] | None = None,
) -> FlowNode:
    carrot_ingredient.gfm_state = GfmStateProp(worker_states={})
    if location:
        carrot_ingredient.flow_location = location
    else:
        carrot_ingredient.flow_location = None

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=service_provider.glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    gap_filling_worker = WaterScarcityGapFillingFactory(postgres_db, service_provider).spawn_worker(carrot_ingredient)
    assert (
        not gap_filling_worker.should_be_scheduled()
    ), "WaterScarcityGFM should not be scheduled for FoodProductFlowNode."

    # First run MatchProductNameGFM to obtain product name.
    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # Then, run LinkTermToActivityGFM.
    gap_filling_worker = LinkTermToActivityNodeGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    await gap_filling_worker.gfm_factory.init_cache()
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # Run WaterScarcityGFM on the ModeledActivityNode.
    brightway_node = calc_graph.get_root_node().get_sub_nodes()[0]
    gap_filling_worker = WaterScarcityGapFillingFactory(postgres_db, service_provider).spawn_worker(brightway_node)
    await gap_filling_worker.gfm_factory.init_cache()
    assert gap_filling_worker.should_be_scheduled(), "WaterScarcityGFM should be scheduled for ModeledActivityNode."
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    scarce_water_consumption_node = calc_graph.get_root_node().get_sub_nodes()[0]
    assert type(scarce_water_consumption_node) is FoodProcessingActivityNode

    elementary_flow_node = None
    for sub_flow in scarce_water_consumption_node.get_sub_nodes():
        if type(sub_flow) is FlowNode and type(sub_flow.get_sub_nodes()[0]) is ElementaryResourceEmissionNode:
            elementary_flow_node = sub_flow

    gap_filling_worker = UnitWeightConversionGapFillingFactory(postgres_db, service_provider).spawn_worker(
        elementary_flow_node
    )
    await gap_filling_worker.gfm_factory.init_cache()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert (
        elementary_flow_node is not None
    ), "One of the subnodes of scarce water consumption node should be an elementary flow."
    return elementary_flow_node


@pytest.mark.asyncio
async def test_water_scarcity_gfm_without_location(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_flow = create_minimum_recipe
    elementary_flow_node = await setup_and_run_water_scarcity_gfm(postgres_db, service_provider, carrot_ingredient)

    water_scarcity_root_term = service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
    carrot_water_scarcity = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("WS_sample_1", water_scarcity_root_term.access_group_uid)
    ]

    # Must match consumption (negative sign since the flow is consumption not emission).
    assert is_close(elementary_flow_node.amount.value, -carrot_water_scarcity.data["median"] * 1000)


@pytest.mark.asyncio
async def test_water_scarcity_gfm_with_one_known_location(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_flow = create_minimum_recipe
    elementary_flow_node = await setup_and_run_water_scarcity_gfm(
        postgres_db,
        service_provider,
        carrot_ingredient,
        location=[LocationProp(address="Switzerland", country_code="CH", location_qualifier="KNOWN")],
    )

    water_scarcity_root_term = service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
    carrot_water_scarcity = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("WS_sample_1", water_scarcity_root_term.access_group_uid)
    ]

    # Must match consumption (negative sign since the flow is consumption not emission).
    assert is_close(elementary_flow_node.amount.value, -carrot_water_scarcity.data["CH"] * 1000)


@pytest.mark.asyncio
async def test_water_scarcity_gfm_with_one_known_location_three_letter_code(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_flow = create_minimum_recipe
    elementary_flow_node = await setup_and_run_water_scarcity_gfm(
        postgres_db,
        service_provider,
        carrot_ingredient,
        location=[LocationProp(address="Switzerland", country_code="CHE", location_qualifier="KNOWN")],
    )

    water_scarcity_root_term = service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
    carrot_water_scarcity = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("WS_sample_1", water_scarcity_root_term.access_group_uid)
    ]

    # Must match consumption (negative sign since the flow is consumption not emission).
    assert is_close(elementary_flow_node.amount.value, -carrot_water_scarcity.data["CH"] * 1000)


@pytest.mark.asyncio
async def test_water_scarcity_gfm_with_an_unknown_location(create_minimum_recipe: MinimumRecipeRType) -> None:
    """Tests water scarcity consumption for a country for which the water scarcity index data is unavailable."""
    postgres_db, service_provider, _, carrot_ingredient, _, _root_flow = create_minimum_recipe
    elementary_flow_node = await setup_and_run_water_scarcity_gfm(
        postgres_db,
        service_provider,
        carrot_ingredient,
        location=[
            LocationProp(address="Hungary", country_code="HU", location_qualifier="KNOWN"),
        ],
    )

    water_scarcity_root_term = service_provider.glossary_service.root_subterms["Root_Water_Scarcity"]
    carrot_water_scarcity = service_provider.glossary_service.terms_by_xid_ag_uid[
        ("WS_sample_1", water_scarcity_root_term.access_group_uid)
    ]

    # Must match consumption (negative sign since the flow is consumption not emission).
    # Median is used as a fallback if more than one location is available.
    assert is_close(elementary_flow_node.amount.value, -carrot_water_scarcity.data["median"] * 1000)
