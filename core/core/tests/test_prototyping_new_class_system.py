import uuid
from typing import List, Optional

import pytest
from pydantic import BaseModel, ConfigDict, Field, ValidationError
from structlog import get_logger

from core.domain.term import Term

logger = get_logger()


# some dummy term to use inside node properties:
mg_term = Term(uid=uuid.uuid4(), name="milligram", xid="mg", data={}, sub_class_of=None)

mg_term_serialized = {"data": {}, "name": "milligram", "uid": mg_term.uid, "xid": "mg"}


# Mock CalcGraph class (just as placeholder for testing):
class CalcGraph:
    def __init__(self):
        pass


###############################
# Property classes:
###############################


class NodeProp(BaseModel):
    def __repr__(self):
        return "NodeProp()"

    model_config = ConfigDict(extra="forbid")


class QuantityProp(NodeProp):
    value: float = Field()
    unit_term: Term = Field()
    model_config = ConfigDict(extra="forbid")


class LocationProp(NodeProp):
    country_code: str = Field()
    latitude: Optional[float] = Field(None)
    longitude: Optional[float] = Field(None)
    model_config = ConfigDict(extra="forbid")


class JsonProp(NodeProp):
    model_config = ConfigDict(extra="allow")


###############################
# Node classes:
###############################


class Node(BaseModel):
    __slots__ = ("_calc_graph", "_parent_nodes", "_sub_nodes")
    api_version: int = Field(default=1)
    xid: Optional[str] = Field(None)
    uid: Optional[str] = Field(None)
    raw_input: Optional[JsonProp] = Field(None)
    over_production: Optional[QuantityProp] = Field(None, json_schema_extra={"api_version": 5})

    def __init__(self, **kwargs):
        self._parent_nodes: [Node] = []
        self._sub_nodes: [Node] = []
        self._calc_graph: Optional[CalcGraph] = None

        raw_input = kwargs.get("raw_input", {})
        kwargs_keys_to_delete = []  # delete after loop to avoid potential side effects
        for kwargs_key, kwargs_val in kwargs.items():
            if kwargs_key not in self.model_fields:
                logger.warning(f"Field {kwargs_key} is not defined in this model, " f"moving it into raw_input field.")
                raw_input[kwargs_key] = kwargs[kwargs_key]
                kwargs_keys_to_delete.append(kwargs_key)
            elif "api_version" in kwargs:
                model_field = self.model_fields[kwargs_key]
                field_api_version = 1
                if model_field.json_schema_extra is not None:
                    print(model_field.json_schema_extra)
                    field_api_version = model_field.json_schema_extra.get("api_version", 1)
                if kwargs["api_version"] < field_api_version:
                    # this means that the data was created for an older API version, where this field did not exist:
                    logger.warning(
                        f"Moving field {kwargs_key} into raw_input field, "
                        f"because data was defined for a previous api version."
                    )
                    raw_input[kwargs_key] = kwargs[kwargs_key]
                    kwargs_keys_to_delete.append(kwargs_key)

        for kwargs_key in kwargs_keys_to_delete:
            del kwargs[kwargs_key]

        if len(raw_input) > 0:
            kwargs["raw_input"] = raw_input

        super().__init__(**kwargs)

    @property
    def extra_fields(self) -> set[str]:
        return set(self.__dict__) - set(self.model_fields)

    def __apply_mutation__(self, name: str, value: NodeProp):
        """
        This method is used to apply a mutation to a node, which is used by the PropMutation class.
        This method should never be called directly, but only by the PropMutation class.
        :param name: str
        :param value: NodeProp
        :return:
        """

        if isinstance(value, list):
            if not all(isinstance(x, NodeProp) for x in value):
                raise ValueError(f"Value {value} is list not a NodeProp")
        else:
            if not isinstance(value, NodeProp):
                raise ValueError(f"Value {value} is not a NodeProp")

        if name not in self.model_fields:
            logger.warning(f"Setting extra field {name} on node")

        super().__setattr__(name, value)

    def __setattr__(self, name, value):
        if name in {"_calc_graph", "_parent_nodes", "_sub_nodes", "xid", "uid"}:
            object.__setattr__(self, name, value)
            return

        if self._calc_graph is not None:
            raise ValueError(
                "Cannot modify node after it has been added to a calculation graph. " "Use PropMutation instead."
            )

        self.__apply_mutation__(name, value)

    def __getattribute__(self, name):
        attr = super().__getattribute__(name)
        if object.__getattribute__(self, "_calc_graph") is not None:
            # if this node is integrated in a calculation graph, we need to return a copy of the attribute to prevent
            # mutation of the original property:
            if isinstance(attr, NodeProp):
                return attr.model_copy(deep=True)
            if isinstance(attr, list) and all(isinstance(x, NodeProp) for x in attr):
                return [a.model_copy(deep=True) for a in attr]
        return attr

    def set_calc_graph(self, calc_graph: CalcGraph):
        self._calc_graph = calc_graph

    model_config = ConfigDict(populate_by_name=True, extra="forbid", validate_assignment=True)


class ActivityNode(Node):
    production_amount: Optional[QuantityProp] = None  # <----- API input can go here directly
    fe2: Optional[JsonProp] = None
    locations: Optional[List[LocationProp]] = Field(None)

    def __str__(self):
        return "production_amount: " + str(self.production_amount.value)


class FlowNode(Node):
    amount: Optional[QuantityProp] = None
    fe2: Optional[JsonProp] = None

    def __str__(self):
        return "amount: " + str(self.amount.value)


@pytest.mark.asyncio
async def test_separate_instance_props():
    my_node = ActivityNode(uid="my_uid")
    my_node.xid = "my_xid"
    my_node.production_amount = QuantityProp(value=5, unit_term=mg_term)
    my_node.fe2 = JsonProp(json_data={"a": "b"}, term=None)

    assert my_node.xid == "my_xid"
    assert my_node.production_amount.value == 5

    my_node2 = ActivityNode()
    my_node2.production_amount = QuantityProp(value=9, unit_term=mg_term)
    my_node2.production_amount.value = 33
    assert my_node2.production_amount.value == 33

    # old value is still there:
    assert my_node.production_amount.value == 5


@pytest.mark.asyncio
async def test_read_only_attr():
    my_node2 = ActivityNode()
    my_node2.production_amount = QuantityProp(value=9, unit_term=mg_term)

    calc_graph = CalcGraph()
    my_node2.set_calc_graph(calc_graph)
    my_node2.production_amount.value = 44

    with pytest.raises(ValueError):
        my_node2.production_amount = QuantityProp(value=12, unit_term=mg_term)

    # old value should still be there, because direct mutation is not allowed:
    assert my_node2.production_amount.value == 9

    my_node2.__apply_mutation__("production_amount", QuantityProp(value=44, unit_term=mg_term))

    # now it should be successfully mutated:
    assert my_node2.production_amount.value == 44


@pytest.mark.asyncio
async def test_extra_attr():
    my_node2 = ActivityNode()
    my_node2.production_amount = QuantityProp(value=9, unit_term=mg_term)

    # check if we can dynamically add a new property that was not defined in the class:
    my_node2 = ActivityNode(
        production_amount=QuantityProp(value=9, unit_term=mg_term),
        new_prop_name=QuantityProp(value=4, unit_term=mg_term),
    )

    result = my_node2.model_dump(exclude_none=True)
    assert result == {
        "api_version": 1,
        "raw_input": {
            "new_prop_name": {"value": 4.0, "unit_term": mg_term_serialized},
        },
        "production_amount": {"value": 9.0, "unit_term": mg_term_serialized},
    }


@pytest.mark.asyncio
async def test_only_node_prop_extra_allowed():
    my_node2 = ActivityNode(production_amount=QuantityProp(value=9, unit_term=mg_term))

    # check if we could dynamically add a new property that was not defined in the class:
    with pytest.raises(ValueError):
        my_node2.new_prop_name = "some_string"


@pytest.mark.asyncio
async def test_with_wrong_prop_type():
    # this should raise, because production_amount should be of type QuantityProp:
    with pytest.raises(ValidationError):
        ActivityNode(production_amount=JsonProp(json_data={"a": [1, 2]}))

    # it should also raise if we assign a prop with wrong type:
    my_node2 = ActivityNode(production_amount=QuantityProp(value=9, unit_term=mg_term))
    with pytest.raises(ValidationError):
        my_node2.production_amount = JsonProp(json_data={"b": [1, 2]})

    # try setting it as a new property should work:
    my_node3 = ActivityNode(
        production_amount=QuantityProp(value=9, unit_term=mg_term),
        other_customer_data=JsonProp(json_data={"a": [1, 2]}),  # this field is not defined in the class
    )
    result = my_node3.model_dump(exclude_none=True)
    assert result == {
        "api_version": 1,
        "production_amount": {"value": 9, "unit_term": mg_term_serialized},
        "raw_input": {"other_customer_data": {"json_data": {"a": [1, 2]}}},
    }


@pytest.mark.asyncio
async def test_field_versioning_old_data_on_new_model():
    # check old api data that would be valid for newer api version field:
    api_input_v1_a = {
        "api_version": 1,  # The 'over_production' field was introduced in api_version 5, but data is api_version 1
        "over_production": {  # Therefore, it should be deserialized into 'raw_input' property.
            "value": 60,
            "unit_term": mg_term,
        },
    }
    node_v1_a = ActivityNode(**api_input_v1_a)
    result = node_v1_a.model_dump(exclude_none=True)
    assert result == {
        "api_version": 1,
        "raw_input": {
            "over_production": {
                "value": 60,
                "unit_term": {
                    "access_group_uid": None,
                    "sub_class_of": None,
                    "data": {},
                    "name": "milligram",
                    "uid": mg_term.uid,
                    "xid": "mg",
                },
            },  # should be inside 'raw_input' field
        },
    }

    # check old api version:
    api_input_v1_b = {
        "api_version": 1,  # The 'over_production' field was introduced in api_version 5, but data is api_version 1
        "over_production": 60,  # Therefore, it should be deserialized into 'raw_input' property.
    }
    node_v1_b = ActivityNode(**api_input_v1_b)
    result = node_v1_b.model_dump(exclude_none=True)
    assert result == {"api_version": 1, "raw_input": {"over_production": 60}}  # should be inside 'raw_input' field


@pytest.mark.asyncio
async def test_field_versioning_new_data_on_new_model():
    # check new api version 5 with the new valid over_production field:
    api_input_v5 = {
        "api_version": 5,  # The 'over_production' field was introduced in api_version 5, and data is api_version 5.
        "over_production": {  # Therefore, it should be deserialized directly into the new 'over_production' property.
            "value": 60,
            "unit_term": mg_term,
        },
    }
    node_v5 = ActivityNode(**api_input_v5)
    result = node_v5.model_dump(exclude_none=True)
    assert result == {
        "api_version": 5,
        "over_production": {
            "value": 60.0,
            "unit_term": mg_term_serialized,
        },  # check if it was directly inserted as a node prop
    }

    # test if using the newer api version with a wrongly typed field raises a ValidationError:
    api_input_v5_wrong_type = {"api_version": 5, "over_production": 60}  # not valid according to api_version 5
    with pytest.raises(ValidationError):
        Node(**api_input_v5_wrong_type)


@pytest.mark.asyncio
async def test_list_of_props():
    my_node = ActivityNode(locations=[LocationProp(country_code="CH")])

    # it should not be possible to assign a LocationProp directly that is not in a list:
    with pytest.raises(ValidationError):
        my_node.locations = LocationProp(country_code="NL")

    my_node.locations = [
        LocationProp(country_code="NL"),
        LocationProp(country_code="DE"),
    ]


@pytest.mark.asyncio
async def test_list_with_wrong_prop_type():
    my_node = ActivityNode(locations=[LocationProp(country_code="CH")])
    with pytest.raises(ValidationError):
        my_node.locations = [
            LocationProp(country_code="NL"),
            JsonProp(json_data={"a": [1, 2]}),
        ]

    with pytest.raises(ValidationError):
        ActivityNode(locations=[LocationProp(country_code="CH"), JsonProp(json_data={"a": [1, 2]})])
