"Tests for inventory connector gap filling module."
import uuid

import pytest
from gap_filling_modules.abstract_gfm import GapFillingWorkerStatusEnum
from gap_filling_modules.inventory_connector_gfm import InventoryConnectorGapFillingFactory
from gap_filling_modules.link_term_to_activity_node_gfm import LinkTermToActivityNodeGapFillingFactory

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode
from core.domain.nodes.node import Node
from core.domain.props.environmental_flows_prop import EnvironmentalFlowsProp
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.names_prop import NamesProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.tests.conftest import ElaborateRecipe, MinimumRecipeRType
from database.postgres.settings import PgSettings

from .conftest import is_close


@pytest.mark.asyncio
async def test_add_child_processes_to_carrot_process(create_minimum_recipe: MinimumRecipeRType) -> None:
    "Same as test_add_child_nodes_to_recipe but to test that this GFM also will load sub-processes of an (LCA) process."
    postgres_db, service_provider, _, _, carrot_process, _root_flow = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    assert not carrot_process.get_sub_nodes(), f"process {carrot_process} should not have sub-nodes before running  GFM"

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_process.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_process)

    gap_filling_factory = InventoryConnectorGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_process)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)
    assert carrot_process.get_sub_nodes(), f"process {carrot_process} should have sub-nodes after running this GFM"
    flow: Node = carrot_process.get_sub_nodes()[0]
    assert isinstance(flow, FlowNode), f"process {carrot_process} should have a flow sub-node"
    # FIXME fix when caching is implemented:
    # assert flow.get_sub_nodes()[0].node_type == NodeTypeEnum.process, 'process "tractor..." has been loaded'


@pytest.mark.asyncio
async def test_load_process_child_nodes_gfm_after_linking_term_to_activity_node(
    create_minimum_recipe: MinimumRecipeRType,
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, root_flow = create_minimum_recipe
    _, _, foodex2_term_group_uid, _, _, _, _ = get_term_access_group_uids
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_ingredient)

    # add carrot[Term] to carrot_ingredient, using a mutation
    carrot_term: Term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
        "A1791",
        foodex2_term_group_uid,
    )
    prop_mutation = PropMutation(
        created_by_module="test",
        node_uid=carrot_ingredient.uid,
        prop_name="product_name",
        prop=NamesProp(terms=[GlossaryTermProp(term_uid=carrot_term.uid)]),
    )
    calc_graph.apply_mutation(prop_mutation)

    # now run LinkTermToActivityNode GFM to add a process
    gap_filling_factory = LinkTermToActivityNodeGapFillingFactory(postgres_db, service_provider=service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    # assert that this GFM has correctly liked carrot[Term] to carrot_process[Node]
    await gap_filling_worker.run(calc_graph)
    subs = carrot_ingredient.get_sub_nodes()
    added_carrot_process: Node = subs[0]

    # running the target unit test GFM
    gap_filling_factory = InventoryConnectorGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(added_carrot_process)

    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert added_carrot_process.get_sub_nodes(), "added_carrot_process should have sub-proc. attached to it"
    assert len(added_carrot_process.get_sub_nodes()) == 1


@pytest.mark.asyncio
async def test_inventory_connector_zero_flow(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, _, carrot_process, _root_flow = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    flow_node = await postgres_db.get_graph_mgr().upsert_node_by_uid(
        FlowNode(amount_in_original_source_unit={"value": 0.0})
    )
    await postgres_db.get_graph_mgr().add_edge(
        edge=Edge(parent_uid=carrot_process.uid, child_uid=flow_node.uid, edge_type=EdgeTypeEnum.lca_link)
    )

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_process.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(carrot_process)

    gap_filling_factory = InventoryConnectorGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_process)

    await gap_filling_worker.run(calc_graph)

    zero_flow_present = False
    for flow in carrot_process.get_sub_nodes():
        if is_close(flow.amount_in_original_source_unit.value, 0.0):
            zero_flow_present = True

    assert zero_flow_present, "At least one of the flows should have 0 quantity."


@pytest.mark.asyncio
async def test_load_cached_system_process(create_more_elaborate_recipe: ElaborateRecipe) -> None:
    """Check that the AddChildNodes GFM is able to load a cached system process."""
    # start with recipe from book
    er = create_more_elaborate_recipe
    glossary_service = er.service_provider.glossary_service

    # modify electricity_production_proc to be a system process, insert it into the DB with new values
    methane = await er.pg_db.get_graph_mgr().upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            production_amount={"value": 1.0, "unit": "square meter-year"},  # Not the actual unit, just for tests.
            id="('biosphere3', 'baf58fc9-573c-419c-8c16-831ac03203b9')",
            key=["biosphere3", "baf58fc9-573c-419c-8c16-831ac03203b9"],
            name="Methane fossil",
            type="emission",
            categories=["air"],
        )
    )
    er.service_provider.node_service.cache.add_node(methane)
    methane_mapping = (
        PgSettings().EATERNITY_NAMESPACE_UUID,
        "biosphere3_baf58fc9-573c-419c-8c16-831ac03203b9",
        methane.uid,
    )
    # square_meter_year_term = glossary_service.terms_by_xid_ag_uid[
    #     ("EOS_square-meter-year", glossary_service.root_subterms["EOS_units"].access_group_uid)
    # ]
    await er.service_provider.product_service.bulk_insert_xid_uid_mappings([methane_mapping])

    aggregated_cache = EnvironmentalFlowsProp(flow_quantities={methane.uid: 123})
    await er.pg_db.get_graph_mgr().update_node_prop(
        er.electricity_production_process.uid, "aggregated_cache", aggregated_cache
    )

    # run calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=er.service_provider)
    calc_service = CalcService(
        er.service_provider,
        glossary_service,
        er.service_provider.node_service,
        gap_filling_module_loader,
    )
    calculation = Calculation(
        uid=uuid.uuid4(),
        root_node_uid=er.electricity_production_process.uid,
        explicitly_attach_cached_elementary_resource_emission=True,
    )
    calculation = Calculation(
        uid=uuid.uuid4(), root_node_uid=er.root_flow.uid, explicitly_attach_cached_elementary_resource_emission=True
    )
    _, result_root_node = await calc_service.calculate(calculation)

    # assert that InventoryConnectorGFM has correctly loaded the cached system processes
    result_elec_proc = result_root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
    result_methane_node = result_elec_proc.get_sub_nodes()[0].get_sub_nodes()[0]
    assert result_methane_node.uid == methane.uid, (
        "the graph should look like this: "
        "electricity_production_proc -> flow -> methane. no CO2 or sulfur "
        "should be added, because we picked up the (cached) system process."
    )
