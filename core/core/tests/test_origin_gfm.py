"Tests for origin gap filling module."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.origin_gfm import OriginGapFillingFactory, OriginGfmSettings
from gap_filling_modules.unit_weight_conversion_gfm import UnitWeightConversionGapFillingFactory

from core.domain.calculation import Calculation
from core.domain.glossary_link import GlossaryLink
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.props.location_prop import LocationProp, LocationQualifierEnum
from core.domain.props.quantity_prop import RawQuantity
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeRType, MinimumRecipeWithDeclaration, MinimumRecipeWithTwoOrigins
from database.postgres.postgres_db import PostgresDb


@pytest.mark.asyncio
async def test_origin_check_correct_fao_file_is_used() -> None:
    assert (
        OriginGfmSettings().FAO_SNAPSHOT_SUFFIX == "Y2021_Y2020_snapshot-2024-09-11"
    ), "For development, you must set FAO_SNAPSHOT_SUFFIX='Y2021_Y2020_snapshot-2024-09-11' in .env"


@pytest.mark.asyncio
async def test_generation_of_top90(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Sanity check for the generation of the top 90 countries."""
    postgres_db, service_provider = setup_services
    gap_filling_factory = OriginGapFillingFactory(postgres_db, service_provider)

    # download the import-export data from gdrive
    drive_service = gap_filling_factory.create_drive_service()
    drive_file_info_by_name = gap_filling_factory.get_drive_file_info_by_name(drive_service)
    filename = gap_filling_factory.import_export_zip_source_file_name
    assert filename in drive_file_info_by_name

    gap_filling_factory.download_file_from_google_drive(
        drive_service,
        filename,
        drive_file_info_by_name[filename]["id"],
    )

    gap_filling_factory.default_production_fao_code_term = (
        await gap_filling_factory.service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            term_xid="200000",
            access_group_uid=str(gap_filling_factory.root_fao_term.access_group_uid),
        )
    )
    await gap_filling_factory.process_import_export_data_from_fao(write_to_local_file=False)
    import_export_dict = gap_filling_factory.import_export_agg_cache.import_export_data
    assert isinstance(import_export_dict, dict)
    top90_dict = gap_filling_factory.import_export_agg_cache.import_export_data["top90"]
    assert isinstance(top90_dict, dict)

    # check that all top90 countries have values for 2020 and 2021 if they have import-statistics != 0
    incomplete_values = []
    for key, value in import_export_dict.items():
        assert isinstance(value, dict)
        percentage_1 = None
        percentage_2 = None
        if key != "top90" and (
            (
                value.get("Y2020")
                and value.get("Y2020")[0] > 0
                and not (
                    key in top90_dict
                    and isinstance(top90_dict[key], dict)
                    and isinstance(top90_dict[key].get("Y2020"), dict)
                    and (percentage_1 := sum(share[1] for share in top90_dict[key].get("Y2020").get("share"))) > 0.9
                )
            )
            or (
                value.get("Y2021")
                and value.get("Y2021")[0] > 0
                and not (
                    key in top90_dict
                    and isinstance(top90_dict[key], dict)
                    and top90_dict[key].get("Y2021")
                    and (percentage_2 := sum(share[1] for share in top90_dict[key].get("Y2021").get("share"))) > 0.9
                )
            )
        ):
            incomplete_values.append((key, value, top90_dict.get(key), percentage_1, percentage_2))

    assert len(incomplete_values) == 0, [
        (
            f"Incomplete value in top90 for {key}: "
            f"(import_export_value: {import_export_value}, top90_value: {top90_value}, "
            f"percentage_1: {percentage_1}, percentage_2: {percentage_2})"
        )
        for (key, import_export_value, top90_value, percentage_1, percentage_2) in incomplete_values
    ]

    # for development we can also compare to the existing local file
    # import os

    # from deepdiff import DeepDiff
    # from gap_filling_modules.origin_gfm import ImportExportCache

    # old_cache_path = os.path.join(gap_filling_factory.gfm_dir, "import_export_cache.msgpack")
    # old_import_export_agg_cache = ImportExportCache(old_cache_path).deserialize()
    # old_import_export_data = old_import_export_agg_cache.import_export_data

    # old_top90_dict = old_import_export_data.pop("top90")
    # new_top90_dict = import_export_dict.pop("top90")
    # # compare the two dicts with DeepDiff
    # dict_diff_top_90 = DeepDiff(
    #     old_top90_dict,
    #     new_top90_dict,
    #     ignore_order=True,
    #     significant_digits=6,
    # )
    # print(dict_diff_top_90)
    # dict_diff = DeepDiff(
    #     old_import_export_data,
    #     import_export_dict,
    #     ignore_order=True,
    #     significant_digits=6,
    # )
    # assert dict_diff == {}


@pytest.mark.asyncio
async def test_origin_gfm_ingredient_with_no_origin(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
) -> None:
    """Tests a case when no origin is specified for both combined product and monoproduct."""
    postgres_db, service_provider, recipe, root_flow, _sub_flow = create_minimum_recipe_with_small_declaration
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    origin_split_nodes = (
        root_node.get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()
    )
    assert len(origin_split_nodes) == 6

    ingredient_amount_per_country = {
        "CH": 96.3653,
        "ES": 1.2834,
        "IT": 1.0832,
        "DK": 0.7651,
        "NL": 0.2775,
        "PT": 0.2255,
    }

    for node in origin_split_nodes:
        origin_node = node.get_sub_nodes()[0].get_sub_nodes()[0]
        assert isinstance(origin_node, FlowNode)

        assert origin_node.origin_data
        assert origin_node.origin_data.stats_data_year_column
        assert origin_node.origin_data.stats_data_year_column == str(2021)

        # asserting that everything's good with origin prop
        assert len(origin_node.flow_location) == 1
        prop_data = origin_node.flow_location[0]
        assert prop_data
        assert prop_data.country_code in ingredient_amount_per_country.keys()
        assert prop_data.location_qualifier == LocationQualifierEnum.known

        # asserting that subdivision nodes are all present
        subdivision_nodes = origin_node.get_sub_nodes()[0].get_sub_nodes()
        assert len(subdivision_nodes) == 2

        for subdivision_node in subdivision_nodes:
            # asserting that ingredient amount have all been converted
            if not subdivision_node.is_dried():
                unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
                assert (
                    round(
                        subdivision_node.amount.amount_for_root_node().value * unit.data["mass-in-g"] / 10.0,
                        4,
                    )
                    == ingredient_amount_per_country[prop_data.country_code]
                )

                assert round(node.amount.value, 6) == round(
                    ingredient_amount_per_country[prop_data.country_code] / 100, 6
                )

            # asserting that brightway_process nodes are present
            assert len(subdivision_node.get_sub_nodes()) == 1


@pytest.mark.asyncio
@pytest.mark.parametrize("mode", ["delete_fao_code_link", "replace_fao_code_link_with_default_fao_code"])
async def test_origin_gfm_ingredient_with_no_unknown_origin_and_no_fao_code(
    create_minimum_recipe_with_small_declaration: MinimumRecipeWithDeclaration,
    mode: str,
) -> None:
    """Tests a case when no origin is specified for both combined product and monoproduct."""
    postgres_db, service_provider, recipe, root_flow, _sub_flow = create_minimum_recipe_with_small_declaration
    glossary_service = service_provider.glossary_service
    matching_service = service_provider.matching_service
    glossary_link_service = service_provider.glossary_link_service

    # delete FAO glossary links for the test
    matchings = matching_service.get_terms_by_matching_string("Karotten", "MatchProductName", filter_lang="de")
    matching_terms = matchings[0]
    fao_term = await glossary_link_service.load_fao_code_term(matching_terms)
    root_fao_term_access_group_uid = fao_term.access_group_uid
    assert isinstance(fao_term, Term)

    existing_fao_glossary_links = await glossary_link_service.get_glossary_links_by_gfm("FAO")

    await glossary_link_service.delete_glossary_links_by_uids([link.uid for link in existing_fao_glossary_links])
    fao_term = await glossary_link_service.load_fao_code_term(matching_terms)
    assert fao_term is None

    if mode == "replace_fao_code_link_with_default_fao_code":
        # add the default fao-code as a new glossary link
        default_production_fao_code_term = await glossary_service.get_term_by_xid_and_access_group_uid(
            term_xid="200000",
            access_group_uid=str(root_fao_term_access_group_uid),
        )
        new_glossary_link = GlossaryLink(
            gap_filling_module="FAO",
            term_uids=[matching.uid for matching in matching_terms],
            linked_term_uid=default_production_fao_code_term.uid,
        )
        await glossary_link_service.insert_glossary_link(new_glossary_link)
        fao_term = await glossary_link_service.load_fao_code_term(matching_terms)
        assert isinstance(fao_term, Term)

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)

    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    origin_split_nodes = (
        root_node.get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()
    )
    assert len(origin_split_nodes) == 11

    ingredient_amount_per_country = {
        "CH": 64.5391,
        "FR": 8.5542,
        "DE": 11.2385,
        "IT": 6.2306,
        "AT": 1.9603,
        "ES": 2.7951,
        "BR": 1.6148,
        "NL": 1.3263,
        "BE": 0.5454,
        "CO": 0.5145,
        "PL": 0.6812,
    }

    countries_which_have_mocked_ecotransit_responses = [
        "CH",
        "ES",
        "IT",
        "NL",
    ]  # we don't yet have mocked ecotransit responses for all countries

    for node in origin_split_nodes:
        if node.get_sub_nodes() and node.get_sub_nodes()[0].transport:
            origin_node = node.get_sub_nodes()[0].get_sub_nodes()[0]
        else:
            origin_node = node  # no transport nodes have been added in between
            assert origin_node.flow_location[0].country_code not in countries_which_have_mocked_ecotransit_responses

        assert isinstance(origin_node, FlowNode)

        assert origin_node.origin_data
        assert origin_node.origin_data.stats_data_year_column
        assert origin_node.origin_data.stats_data_year_column == str(2021)

        # asserting that everything's good with origin prop
        assert len(origin_node.flow_location) == 1
        prop_data = origin_node.flow_location[0]
        assert prop_data
        assert prop_data.country_code in ingredient_amount_per_country.keys()
        assert prop_data.location_qualifier == LocationQualifierEnum.known

        # asserting that subdivision nodes are all present
        subdivision_nodes = origin_node.get_sub_nodes()[0].get_sub_nodes()
        assert len(subdivision_nodes) == 2

        for subdivision_node in subdivision_nodes:
            # asserting that ingredient amount have all been converted
            if not subdivision_node.is_dried():
                unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
                assert round(
                    subdivision_node.amount.amount_for_root_node().value * unit.data["mass-in-g"] / 10.0,
                    4,
                ) == round(ingredient_amount_per_country[prop_data.country_code], 4)

                assert round(node.amount.value, 6) == round(
                    ingredient_amount_per_country[prop_data.country_code] / 100, 6
                )

            # asserting that brightway_process nodes are present
            assert len(subdivision_node.get_sub_nodes()) == 1


@pytest.mark.asyncio
async def test_origin_gfm_ingredient_with_1_origin(
    create_minimum_recipe: MinimumRecipeRType,
) -> None:
    "Test with one origin."
    postgres_db, service_provider, recipe, carrot_ingredient, carrot_process, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_node.uid,
        glossary_service=glossary_service,
    )
    carrot_ingredient.flow_location = [LocationProp(address="Spain", location_qualifier=LocationQualifierEnum.known)]
    calc_graph.add_node(carrot_ingredient)
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_node)
    calc_graph.add_edge(root_node.uid, recipe.uid)
    calc_graph.add_edge(recipe.uid, carrot_ingredient.uid)
    modeled_activity_node = ModeledActivityNode(uid=uuid.uuid4(), production_amount=RawQuantity(value=1.0, unit="kg"))
    calc_graph.add_node(modeled_activity_node)
    calc_graph.add_edge(carrot_ingredient.uid, modeled_activity_node.uid)

    # running the GFM
    gap_filling_factory = OriginGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.reschedule
    # first convert the unit of the subactivity
    unit_weight_conversion_gap_filling_factory = UnitWeightConversionGapFillingFactory(postgres_db, service_provider)
    await unit_weight_conversion_gap_filling_factory.init_cache()
    unit_weight_conversion_gap_filling_worker = unit_weight_conversion_gap_filling_factory.spawn_worker(
        modeled_activity_node
    )
    assert unit_weight_conversion_gap_filling_worker.should_be_scheduled()
    assert unit_weight_conversion_gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
    await unit_weight_conversion_gap_filling_worker.run(calc_graph)
    # then run the origin GFM
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready
    await gap_filling_worker.run(calc_graph)

    # checking if all ingredients have been added
    mutation_log = calc_graph.get_mutation_log()
    assert len(mutation_log) == 1  # only the mutation coming from the unit-weight-conversion


@pytest.mark.asyncio
async def test_origin_gfm_ingredient_with_two_recipe_origins(
    create_minimum_recipe_with_small_declaration_and_two_origins: MinimumRecipeWithTwoOrigins,
) -> None:
    """Tests a case when 2 origins are specified for a combined product and no origins for monoproducts."""
    postgres_db, service_provider, recipe, _, root_flow = create_minimum_recipe_with_small_declaration_and_two_origins
    glossary_service = service_provider.glossary_service

    # create calculation graph
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=root_flow.uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe)
    calc_graph.add_node(root_flow)
    calc_graph.add_edge(root_flow.uid, recipe.uid)

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, child_of_root_node_uid=recipe.uid, root_node_uid=root_flow.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # recipe -> recipe origin split flow nodes
    origin_split_recipe_nodes = (
        root_node.get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()
    )
    assert len(origin_split_recipe_nodes) == 2

    for origin_split_recipe_node in origin_split_recipe_nodes:
        node_at_origin = origin_split_recipe_node.get_sub_nodes()[0].get_sub_nodes()[0]
        assert len(node_at_origin.get_sub_nodes()[0].activity_location) == 1
        location_prop = node_at_origin.get_sub_nodes()[0].activity_location[0]
        recipe_origin_country_code = location_prop.country_code
        assert recipe_origin_country_code in (
            "ES",
            "CH",
        )

        # recipe origin split flow nodes -> split recipe node -> main food_product_flow node
        unit = node_at_origin.amount.amount_for_root_node().get_unit_term()
        assert round(node_at_origin.amount.amount_for_root_node().value * unit.data["mass-in-g"], 4) == 500.0

        # TODO: !!! should this be checked & edited at all?
        # is_origin_counted is not a part of amount and the following was never checked.
        # if converted_amount_props.get("is_origin_counted"):
        assert round(origin_split_recipe_node.amount.value, 6) == 0.5

        # Chain of nested nodes:
        # -> main food_product_flow node
        #   -> food_product (in destination location)
        #     -> transported_food_product_flow
        #       -> food_product (in origin location)
        #         -> origin split ingredient nodes
        origin_split_nodes = node_at_origin.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()

        if recipe_origin_country_code == "CH":
            assert len(origin_split_nodes) == 6
        elif recipe_origin_country_code == "ES":
            assert len(origin_split_nodes) == 5
        else:
            raise ValueError(f"Unexpected recipe origin country code: {recipe_origin_country_code}")

        ingredient_amount_per_country = {
            "CH": {
                "CH": 96.3653 / 2,
                "ES": 1.2834 / 2,
                "IT": 1.0832 / 2,
                "DK": 0.7651 / 2,
                "NL": 0.2775 / 2,
                "PT": 0.2255 / 2,
            },
            "ES": {
                "ES": 93.0707 / 2,
                "FR": 4.9827 / 2,
                "PT": 1.2275 / 2,
                "NL": 0.4360 / 2,
                "DE": 0.2831 / 2,
            },
        }

        country_codes = {
            "CH": ingredient_amount_per_country["CH"].keys(),
            "ES": ingredient_amount_per_country["ES"].keys(),
        }

        for node in origin_split_nodes:
            origin_node = node.get_sub_nodes()[0].get_sub_nodes()[0]
            assert isinstance(origin_node, FlowNode)

            # asserting that everything's good with origin prop
            prop_data = origin_node.flow_location[0]
            assert prop_data
            assert prop_data.country_code in country_codes[recipe_origin_country_code]

            # asserting that subdivision nodes are all present
            subdivision_nodes = origin_node.get_sub_nodes()[0].get_sub_nodes()
            assert len(subdivision_nodes) == 2

            for subdivision_node in subdivision_nodes:
                # asserting that ingredient amount have all been converted
                if not subdivision_node.is_dried():
                    unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
                    node_value = round(
                        subdivision_node.amount.amount_for_root_node().value * unit.data["mass-in-g"] / 10.0,
                        4,
                    )
                    test_value = round(
                        ingredient_amount_per_country[recipe_origin_country_code][prop_data.country_code],
                        4,
                    )
                    assert round(abs(node_value - test_value), 4) <= 0.0001

                    node_value = round(subdivision_node.amount.value, 6)
                    # the value of the not dried subdivision should be 1 kg, as that number is counting relative to
                    # the production_amount of the direct parent activity: 1 kg
                    test_value = 1.0
                    assert round(abs(node_value - test_value), 4) <= 0.00001

                # asserting that brightway_process nodes are present
                assert len(subdivision_node.get_sub_nodes()) == 1
