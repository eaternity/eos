"Test conservation gap filling module."
from typing import Tuple

import pytest

from api.tests.conftest import is_close
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.service.service_provider import ServiceProvider
from core.tests.test_orchestration_gfm import (
    recursive_collect_emission_nodes,
    recursive_collect_unique_emission_nodes,
    run_orchestrator_on_recipe,
)
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID
from database.postgres.postgres_db import PostgresDb

# TODO: try to split up this CO2 value into its different contributions
EXPECTED_CO2_VALUE_FOR_TEST_CONSERVATION_FOR_CONVENIENCE_PRODUCT_PER_KG = 2.000586433533152
EXPECTED_CO2_AMOUNT_CONVENIENCE_PRODUCT = 0.6001759300599454


@pytest.mark.asyncio
async def test_conservation_for_convenience_product(
    create_convenience_product: Tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node],
) -> None:
    """Tests the whole orchestration of all GFMs over the create_convenience_product fixture.

    In this case we expect that the that the ingredients get recognized as ingredients of a convenience product and
    therefore are generically conserved. The convenience product should also be highly perishable.
    """
    pg_db, service_provider, recipe, convenience_product, _, root_flow = create_convenience_product
    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes"
    # carrot and potato are both flown in (as specified in the recipe => Pre, Main, and Post carriage), the convenience
    # is transported by truck => Main carriage
    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 25, "should have 25 emission nodes"
    "(2 from dummy tractor LCA + 2 from water scarcity) +"
    "(3x Main carriage + 2x Pre-carriage + 2x Post-carriage)x(1(for fuel)+2(for infrastructure) of transport))"

    result_co2 = (
        calc_graph.get_root_node().impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    assert convenience_product.amount_in_original_source_unit.unit == "gram", "should have correct unit"
    amount_convenience_product_in_kg = convenience_product.amount_in_original_source_unit.value / 1000.0

    expected_co2 = (
        EXPECTED_CO2_VALUE_FOR_TEST_CONSERVATION_FOR_CONVENIENCE_PRODUCT_PER_KG
    ) * amount_convenience_product_in_kg
    assert is_close(result_co2, expected_co2), "root node should have correct CO2 amount"

    def recursive_collect_unique_food_flow_nodes(
        node: Node, collect: set[FoodProductFlowNode]
    ) -> set[FoodProductFlowNode]:
        if isinstance(node, FoodProductFlowNode):
            collect.add(node)
        for child in node.get_sub_nodes():
            recursive_collect_unique_food_flow_nodes(child, collect)
        return collect

    all_food_product_flow_nodes = recursive_collect_unique_food_flow_nodes(calc_graph.child_of_root_node(), set())
    for food_product in all_food_product_flow_nodes:
        if food_product.name() == "Kartoffeln":
            # potato should be generic conserved (it is a convenience product ingredient) and it is perishable
            assert food_product.tag_term_xids == frozenset({"J0001", "EOS_PERISHABLE", "A00ZT", "EOS_MONO_PRODUCT"})
        elif food_product.name() == "Karotten":
            # carrot should be generic conserved (it is a convenience product ingredient) and it is highly perishable
            assert food_product.tag_term_xids == frozenset(
                {
                    "J0001",
                    "EOS_HIGH-PERISHABLE",
                    "A1791",
                    "EOS_certified_rainforest_not_specified",
                    "EOS_MONO_PRODUCT",
                    "EOS_prod_greenhouse",
                }
            )
        elif food_product.name() == "Potato-carrot-combined-product":
            # the convenience product should be generic conserved and it is highly perishable because it has
            # a highly perishable ingredient
            assert food_product.tag_term_xids == frozenset({"J0001", "EOS_HIGH-PERISHABLE", "EOS_COMBINED_PRODUCT"})
        else:
            raise ValueError(f"unexpected food product: {food_product.name()}")


@pytest.mark.asyncio
async def test_conservation_for_convenience_product_with_root_flow(
    create_convenience_product_with_root_flow: Tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node],
) -> None:
    """Tests the whole orchestration of all GFMs over the create_convenience_product fixture.

    In this case we expect that the that the ingredients get recognized as ingredients of a convenience product and
    therefore are generically conserved. The convenience product should also be highly perishable.
    """
    pg_db, service_provider, recipe, _, _, root_flow = create_convenience_product_with_root_flow
    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes"
    # carrot and potato are both flown in (as specified in the recipe => Pre, Main, and Post carriage), the convenience
    # is transported by truck => Main carriage
    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 25, "should have 25 emission nodes"
    "(2 from dummy tractor LCA + 2 from water scarcity) +"
    "(3x Main carriage + 2x Pre-carriage + 2x Post-carriage)x(1(for fuel)+2(for infrastructure) of transport))"

    result_co2 = (
        calc_graph.get_root_node().impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    assert is_close(result_co2, EXPECTED_CO2_AMOUNT_CONVENIENCE_PRODUCT), "Failed to compute the impact assessment."

    def recursive_collect_unique_food_flow_nodes(
        node: Node, collect: set[FoodProductFlowNode]
    ) -> set[FoodProductFlowNode]:
        if isinstance(node, FoodProductFlowNode):
            collect.add(node)
        for child in node.get_sub_nodes():
            recursive_collect_unique_food_flow_nodes(child, collect)
        return collect

    all_food_product_flow_nodes = recursive_collect_unique_food_flow_nodes(calc_graph.child_of_root_node(), set())
    for food_product in all_food_product_flow_nodes:
        if food_product.name() == "Kartoffeln":
            # potato should be generic conserved (it is a convenience product ingredient) and it is perishable
            assert food_product.tag_term_xids == frozenset({"J0001", "EOS_PERISHABLE", "A00ZT", "EOS_MONO_PRODUCT"})
        elif food_product.name() == "Karotten":
            # carrot should be generic conserved (it is a convenience product ingredient) and it is highly perishable
            assert food_product.tag_term_xids == frozenset(
                {
                    "J0001",
                    "EOS_HIGH-PERISHABLE",
                    "A1791",
                    "EOS_certified_rainforest_not_specified",
                    "EOS_MONO_PRODUCT",
                    "EOS_prod_greenhouse",
                }
            )
        elif food_product.name() == "The root-flow of a convenience product":
            # the convenience product should be generic conserved and it is highly perishable because it has
            # a highly perishable ingredient
            assert food_product.tag_term_xids == frozenset({"J0001", "EOS_HIGH-PERISHABLE", "EOS_COMBINED_PRODUCT"})
        elif food_product.name() == "Potato-carrot-combined-product":
            # the convenience product should be generic conserved and it is highly perishable because it has
            # a highly perishable ingredient
            assert food_product.tag_term_xids == frozenset({"J0001", "EOS_HIGH-PERISHABLE", "EOS_COMBINED_PRODUCT"})
        else:
            raise ValueError(f"unexpected food product: {food_product.name()}")


@pytest.mark.asyncio
async def test_conservation_for_fresh_product(
    create_convenience_product: Tuple[PostgresDb, ServiceProvider, Node, Node, Node, Node],
) -> None:
    """Tests the whole orchestration of all GFMs over the create_convenience_product fixture.

    In this case we expect that the that the ingredients get recognized as ingredients of a convenience product and
    therefore are generically conserved. The convenience product should also be highly perishable.
    """
    pg_db, service_provider, recipe, convenience_product, _, root_flow = create_convenience_product
    root_char_term = service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = service_provider.glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    assert isinstance(convenience_product, FoodProductFlowNode)
    # set the conservation of the convenience product to "not conserved" -> fresh recipe
    convenience_product.raw_conservation = {"value": "not conserved", "language": "en"}
    await pg_db.get_graph_mgr().upsert_node_by_uid(convenience_product)

    calc_graph, calculation = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)

    assert (
        len(recursive_collect_unique_emission_nodes(calc_graph.get_root_node(), set())) == 3
    ), "should have 3 emission nodes"
    # carrot and potato are both flown in (as specified in the recipe => Pre, Main, and Post carriage)
    # the convenience is transported by truck => Main carriage
    # each step is additionally cooled because they are fresh, perishable products
    assert len(recursive_collect_emission_nodes(calc_graph.get_root_node(), [])) == 31, "should have 31 emission nodes"
    "(2 from dummy tractor LCA + 2 from water scarcity) + "
    "(3x Main carriage + 2x Pre-carriage + 2x Post-carriage)x(1(for fuel)+2(for infrastructure) of transport)) + "
    "(3x2 cooling for the transport)"

    result_co2 = (
        calc_graph.get_root_node().impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
    )

    assert result_co2 > 0.0, "Failed to compute the impact assessment."

    def recursive_collect_unique_food_flow_nodes(
        node: Node, collect: set[FoodProductFlowNode]
    ) -> set[FoodProductFlowNode]:
        if isinstance(node, FoodProductFlowNode):
            collect.add(node)
        for child in node.get_sub_nodes():
            recursive_collect_unique_food_flow_nodes(child, collect)
        return collect

    all_food_product_flow_nodes = recursive_collect_unique_food_flow_nodes(calc_graph.child_of_root_node(), set())
    for food_product in all_food_product_flow_nodes:
        if food_product.name() == "Kartoffeln":
            # potato needs to be cooled, since it is perishable
            assert food_product.tag_term_xids == frozenset({"J0131", "EOS_PERISHABLE", "A00ZT", "EOS_MONO_PRODUCT"})
        elif food_product.name() == "Karotten":
            # carrot should be cooled (it is an ingredient of a fresh recipe) and it is highly perishable
            assert food_product.tag_term_xids == frozenset(
                {
                    "J0131",
                    "EOS_HIGH-PERISHABLE",
                    "A1791",
                    "EOS_certified_rainforest_not_specified",
                    "EOS_MONO_PRODUCT",
                    "EOS_prod_greenhouse",
                }
            )
        elif food_product.name() == "Potato-carrot-combined-product":
            # the sub-recipe should be not-conserved (as specified by the client) and cooled because it is
            # highly perishable because it has a highly perishable ingredient
            assert food_product.tag_term_xids == frozenset(
                {"J0003", "J0131", "EOS_HIGH-PERISHABLE", "EOS_COMBINED_PRODUCT"}
            )
        else:
            raise ValueError(f"unexpected food product: {food_product.name()}")
