import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.daily_food_unit_gfm import DailyFoodUnitGapFillingFactory

from core.domain.calculation import Calculation
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.props import QuantityPackageProp, QuantityProp, ReferencelessQuantityProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.service.service_provider import ServiceProvider
from core.tests.conftest import is_close
from core.tests.test_aggregation_gfm import setup_nutrients_units_food_categories_terms
from database.postgres.postgres_db import PostgresDb


@pytest.mark.asyncio
async def test_daily_food_unit_gfm(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: tuple[str, str, str, str, str, str, str],
) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    (
        nutr_name_to_term,
        food_cat_to_term,
        gram_term,
        kcal_term,
        production_amount_term,
    ) = setup_nutrients_units_food_categories_terms(service_provider)

    eaternity_term_group_uid, _, _, _, _, _, _ = get_term_access_group_uids

    node_uid = uuid.uuid4()
    child_of_root_node = FoodProcessingActivityNode(uid=node_uid)

    flow_node_uid = uuid.uuid4()
    parent_flow_node = FoodProductFlowNode(uid=flow_node_uid)

    root_node_uid = uuid.uuid4()
    root_node = FoodProcessingActivityNode(uid=root_node_uid)

    calculation = Calculation(
        requested_quantity_references=[
            ReferenceAmountEnum.amount_for_100g,
            ReferenceAmountEnum.amount_for_root_node,
            ReferenceAmountEnum.amount_for_activity_production_amount,
        ]
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider, root_node_uid=node_uid, glossary_service=glossary_service, calculation=calculation
    )
    calc_graph.add_node(child_of_root_node)
    calc_graph.add_node(root_node)
    calc_graph.add_node(parent_flow_node)

    calc_graph.add_edge(root_node.uid, parent_flow_node.uid)
    calc_graph.add_edge(parent_flow_node.uid, child_of_root_node.uid)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = DailyFoodUnitGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()

    gap_filling_worker_flow = gap_filling_factory.spawn_worker(parent_flow_node)

    assert gap_filling_worker_flow.should_be_scheduled()
    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.cancel

    nutr_data: dict = {}
    for nutr, nutr_val in {
        nutr_name_to_term["water"]: 30,
        nutr_name_to_term["protein"]: 20,
        nutr_name_to_term["fat"]: 10,
    }.items():
        nutr_data[nutr] = ReferencelessQuantityProp(value=nutr_val, unit_term_uid=gram_term.uid)

    calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="aggregated_nutrients",
            prop=QuantityPackageProp(
                quantities=nutr_data, for_reference=ReferenceAmountEnum.amount_for_activity_production_amount
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.cancel

    nutr_data[nutr_name_to_term["energy"]] = ReferencelessQuantityProp(value=550, unit_term_uid=kcal_term.uid)

    calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="aggregated_nutrients",
            prop=QuantityPackageProp(
                quantities=nutr_data, for_reference=ReferenceAmountEnum.amount_for_activity_production_amount
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.ready

    calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="amount",
            prop=QuantityProp(
                value=500,
                unit_term_uid=gram_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_activity_production_amount,
            ),
        )
    )

    calc_graph.apply_mutation(
        PropMutation(
            node_uid=root_node_uid,
            prop_name="impact_assessment_supply_for_root",
            prop=QuantityProp(
                value=0.1,
                unit_term_uid=production_amount_term.uid,
                for_reference=ReferenceAmountEnum.amount_for_root_node,
            ),
        )
    )

    nutr_data[nutr_name_to_term["energy"]] = ReferencelessQuantityProp(unit_term_uid=kcal_term.uid, value=0.5 * 550)

    for nutr, nutr_val in {
        nutr_name_to_term["water"]: 30,
        nutr_name_to_term["protein"]: 20,
        nutr_name_to_term["fat"]: 10,
    }.items():
        nutr_data[nutr] = ReferencelessQuantityProp(unit_term_uid=gram_term.uid, value=0.5 * nutr_val)

    calc_graph.apply_mutation(
        PropMutation(
            node_uid=flow_node_uid,
            prop_name="aggregated_nutrients",
            prop=QuantityPackageProp(
                quantities=nutr_data, for_reference=ReferenceAmountEnum.amount_for_activity_production_amount
            ),
        )
    )

    assert gap_filling_worker_flow.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker_flow.run(calc_graph)

    # Obtained from an independent calculation.
    known_dfu_local_flow = 0.24453818181818185
    known_dfu_flow_for_root = 0.02445381818181818

    assert is_close(
        parent_flow_node.daily_food_unit.amount_for_activity_production_amount().value,
        known_dfu_local_flow,
    )

    assert is_close(
        parent_flow_node.daily_food_unit.amount_for_root_node().value,
        known_dfu_flow_for_root,
    )
    # Serializaion / Deserialization
    flow_nutrients_serialized = parent_flow_node.aggregated_nutrients.model_dump()

    parent_flow_amount_for_root = parent_flow_node.aggregated_nutrients.amount_for_root_node().quantities

    for key, val in parent_flow_amount_for_root.items():
        assert is_close(
            flow_nutrients_serialized[ReferenceAmountEnum.amount_for_root_node][str(key)]["quantity"]["value"],
            val.value,
        )

    assert (
        parent_flow_node.aggregated_nutrients.for_reference == ReferenceAmountEnum.amount_for_activity_production_amount
    )
    deserialized_nutrients = QuantityPackageProp(**flow_nutrients_serialized)
    assert deserialized_nutrients.for_reference == ReferenceAmountEnum.amount_for_100g

    for key, val in parent_flow_node.aggregated_nutrients.amount_for_100g().quantities.items():
        assert is_close(
            deserialized_nutrients.quantities[key].value,
            val.value,
        )

    flow_dfu_serialized = parent_flow_node.daily_food_unit.model_dump()

    parent_flow_amount_for_root = parent_flow_node.daily_food_unit.amount_for_root_node()

    assert is_close(
        flow_dfu_serialized[ReferenceAmountEnum.amount_for_root_node],
        parent_flow_amount_for_root.value,
    )

    assert parent_flow_node.daily_food_unit.for_reference == ReferenceAmountEnum.amount_for_activity_production_amount
    deserialized_dfu = QuantityProp(**flow_dfu_serialized)
    assert deserialized_dfu.for_reference == ReferenceAmountEnum.amount_for_100g

    assert is_close(deserialized_dfu.value, parent_flow_node.daily_food_unit.amount_for_100g().value)
