"Test for impact assessments."
import uuid

import pytest
import structlog

from core.domain.calculation import Calculation
from core.domain.props.utils import get_daily_food_unit
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.tests.conftest import (
    EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_GWP_20_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_GWP_20_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_GWP_20_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    MinimumRecipeRType,
    is_close,
)
from database.postgres.pg_term_mgr import (
    IPCC_2013_GWP_20,
    IPCC_2013_GWP_20_XID,
    IPCC_2013_GWP_100,
    IPCC_2013_GWP_100_XID,
)

logger = structlog.get_logger()


@pytest.mark.asyncio
async def test_gwp_20(create_minimum_recipe: MinimumRecipeRType) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)
    root_char_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    root_unit_term = glossary_service.root_subterms["EOS_units"]
    ipcc_2013_gwp_20_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_20_XID, root_char_term.access_group_uid)
    ]
    ipcc_2013_gwp_100_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]
    kg_co2_eq_term = glossary_service.terms_by_xid_ag_uid[("EOS_kg-co2-eq", root_unit_term.access_group_uid)]

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid,
        root_node_uid=root_node.uid,
        child_of_root_node_uid=recipe.uid,
        return_log=True,
        requested_impact_assessments=[IPCC_2013_GWP_20, IPCC_2013_GWP_100],
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert len(calculation.mutations) > 5

    assert carrot_ingredient.amount_in_original_source_unit.unit == "kilogram", "should have correct unit"
    amount_carrot_in_kg = carrot_ingredient.amount_in_original_source_unit.value

    expected_gwp_20_co2 = (
        EXPECTED_GWP_20_CO2_AMOUNT_PER_KG_CARROT
        + EXPECTED_GWP_20_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
        + EXPECTED_GWP_20_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
    ) * amount_carrot_in_kg

    assert root_node.impact_assessment.amount_for_root_node().quantities.get(
        ipcc_2013_gwp_20_term.uid
    ), f"GWP 20a expected as method but instead got {root_node.impact_assessment.data}"
    assert is_close(
        root_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_20_term.uid].value,
        expected_gwp_20_co2 * 2,  # x 2 because of the root flow amount.
    ), "root node should have correct GWP 20 CO2 amount"

    expected_gwp_100_co2 = (
        EXPECTED_CO2_AMOUNT_PER_KG_CARROT
        + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
        + EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
    ) * amount_carrot_in_kg

    assert is_close(
        root_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value,
        expected_gwp_100_co2 * 2,  # x 2 because of the root flow amount.
    ), "root node should have correct GWP 100 CO2 amount"
    assert (
        root_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].unit_term_uid
        == kg_co2_eq_term.uid
    ), "Impact assessment unit is incorrect"

    co2_value_in_kg = root_node.impact_assessment.get_co2_value_in_kg()
    dfu_value = get_daily_food_unit(root_node.impact_assessment.get_owner_node())
    fu_value = dfu_value * 5
    co2_rating = root_node.impact_assessment.get_co2_rating(co2_value_in_kg, fu_value)
    assert co2_rating == "D"
