"Tests for Calculation Service."
import asyncio
import copy
import gc
import uuid
from typing import Generator
from unittest import mock
from unittest.mock import MagicMock

import pytest
import structlog
from gap_filling_modules.impact_assessment_gfm import ImpactAssessmentGapFillingWorker

from core.domain.calculation import Calculation
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceLocator
from core.tests.conftest import (
    EXPECTED_CO2_AMOUNT_PER_KG_CARROT,
    EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR,
    MinimumRecipeRType,
    is_close,
)
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID

logger = structlog.get_logger()


async def run_calculation(
    calc_service: CalcService, root_node_uid: uuid.UUID, child_of_root_node_uid: uuid.UUID
) -> None:
    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid,
        root_node_uid=root_node_uid,
        child_of_root_node_uid=child_of_root_node_uid,
        return_log=True,
    )
    calculation, result_node = await calc_service.calculate(calculation)
    assert len(calculation.mutations) > 5


@pytest.mark.asyncio
async def test_memory_leak(create_minimum_recipe: MinimumRecipeRType) -> None:
    """Tests if calculation is correctly clean up from memory."""
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)

    root_node_uid = root_node.uid
    child_of_root_node_uid = recipe.uid

    gc.collect()

    def find_calculation_objects_in_mem() -> Generator[Calculation, None, None]:
        for obj in gc.get_objects():
            if hasattr(obj, "__class__") and obj.__class__ == Calculation:
                yield obj
            else:
                continue

    all_calc_obj = [obj for obj in find_calculation_objects_in_mem()]
    assert len(all_calc_obj) == 0

    await run_calculation(calc_service, root_node_uid, child_of_root_node_uid)

    gc.collect()
    all_calc_obj = [obj for obj in find_calculation_objects_in_mem()]
    assert len(all_calc_obj) == 0, "should not have any calculation objects left in memory"
    # In case there is a memory leak, this will show the objects that are keeping the calculation objects in memory
    # import objgraph  # needs to be installed using poetry add objgraph
    # objgraph.show_backrefs(all_calc_obj, max_depth=10)


@pytest.mark.asyncio
async def test_calc_service(create_minimum_recipe: MinimumRecipeRType) -> None:
    """Tests the whole orchestration of all GFMs over the minimum_recipe fixture."""
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_node.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    assert len(calculation.mutations) > 5
    # FIXME: somehow many mutations are skipped
    # FIXME: improve assertation to validate all or many more worker module names?
    for i, dm in enumerate(calculation.mutations):
        print(i, dm.created_by_module)
    # -6 when aggregation GFM is active
    assert ImpactAssessmentGapFillingWorker.__name__ in [
        mutation.created_by_module for mutation in calculation.mutations
    ]

    root_char_term = glossary_service.root_subterms["Root_Impact_Assessments"]
    ipcc_2013_gwp_100_term = glossary_service.terms_by_xid_ag_uid[
        (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
    ]

    # FIXME: root_node should have the impact_assessment
    result_co2 = (
        root_node.get_sub_nodes()[0]
        .impact_assessment.amount_for_root_node()
        .quantities[ipcc_2013_gwp_100_term.uid]
        .value
    )

    assert carrot_ingredient.amount_in_original_source_unit.unit == "kilogram", "should have correct unit"
    amount_carrot_in_kg = carrot_ingredient.amount_in_original_source_unit.value
    expected_co2 = (
        2
        * (  # factor 2 because the root-flow-node that we appended has twice the amount of the original recipe
            EXPECTED_CO2_AMOUNT_PER_KG_CARROT
            + EXPECTED_TRANSPORT_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
            + EXPECTED_TRANSPORT_COOLING_CO2_AMOUNT_PER_KG_CARROT_ES_TO_CH_BY_AIR
        )
        * amount_carrot_in_kg
    )
    assert is_close(result_co2, expected_co2), "root node should have correct CO2 amount"


class DummyOrchestrator:
    def __init__(self, counter: dict, mock_vm: MagicMock | None, run_time_sec: int = 60):
        self.counter = counter
        self.mock_vm = mock_vm
        self.run_time_sec = run_time_sec

    async def run(self) -> None:
        self.counter["running"] += 1
        if self.mock_vm:
            self.mock_vm.rss += 500 * 1024 * 1024
        await asyncio.sleep(self.run_time_sec)
        self.counter["running"] -= 1


@pytest.mark.asyncio
async def test_calc_service_low_memory() -> None:
    """Start at 3000 MB memory usage, making all three run."""
    service_provider = ServiceLocator().service_provider
    gap_filling_module_loader = GapFillingModuleLoader()

    counter = {"running": 0}
    with mock.patch("psutil.Process") as MockProcess:
        mock_vm = mock.Mock()
        mock_vm.rss = 3000 * 1024 * 1024
        MockProcess.return_value.memory_info.return_value = mock_vm
        orchestrator = DummyOrchestrator(counter=counter, mock_vm=mock_vm, run_time_sec=2)
        with mock.patch(
            "core.service.calc_service.CalcService.create_calc_graph_and_orchestrator",
            return_value=(None, copy.copy(orchestrator)),
        ):
            calc_service = CalcService(
                service_provider,
                service_provider.glossary_service,
                service_provider.node_service,
                gap_filling_module_loader,
            )

            async def run_orchestrators() -> None:
                await asyncio.gather(
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                    calc_service.calculate(  # the fourth one should be put on hold.
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                )

            orchestrator_task = asyncio.create_task(run_orchestrators())

            # Allow some time for the orchestrators to potentially run
            await asyncio.sleep(1)

            # Assert that three orchestrators were running at the same time
            assert counter["running"] == 3, "Not all orchestrators ran concurrently."

            # Allow some time for the orchestrators to finish
            await asyncio.sleep(2)

            # Assert that three orchestrators were running at the same time
            assert counter["running"] == 2, "Not all orchestrators ran concurrently."

            orchestrator_task.cancel()

            # Ensure all tasks are properly cancelled
            await asyncio.gather(orchestrator_task, return_exceptions=True)


@pytest.mark.asyncio
async def test_calc_service_medium_memory() -> None:
    """Start at 7600 MB memory usage, making only 2 run."""
    service_provider = ServiceLocator().service_provider
    gap_filling_module_loader = GapFillingModuleLoader()

    counter = {"running": 0}
    with mock.patch("psutil.Process") as MockProcess:
        mock_vm = mock.Mock()
        mock_vm.rss = 7600 * 1024 * 1024
        MockProcess.return_value.memory_info.return_value = mock_vm
        orchestrator = DummyOrchestrator(counter=counter, mock_vm=mock_vm, run_time_sec=3)
        with mock.patch(
            "core.service.calc_service.CalcService.create_calc_graph_and_orchestrator",
            return_value=(None, copy.copy(orchestrator)),
        ):
            calc_service = CalcService(
                service_provider,
                service_provider.glossary_service,
                service_provider.node_service,
                gap_filling_module_loader,
            )

            async def run_orchestrators() -> None:
                await asyncio.gather(
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                )

            orchestrator_task_1 = asyncio.create_task(run_orchestrators())

            # Allow some time for the orchestrators to potentially run
            await asyncio.sleep(1)

            # Starting at 85 percent both processes should run.
            assert counter["running"] == 1, "only 1 orchestrator should run."

            orchestrator_task_2 = asyncio.create_task(
                calc_service.calculate(Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4()))
            )

            # Allow some time for the orchestrators to potentially run
            await asyncio.sleep(1)

            # Assert that the third task is rejected.
            assert counter["running"] == 1, "only 1 orchestrators can run concurrently."

            orchestrator_task_1.cancel()
            orchestrator_task_2.cancel()

            # Ensure all tasks are properly cancelled
            await asyncio.gather(orchestrator_task_1, return_exceptions=True)
            await asyncio.gather(orchestrator_task_2, return_exceptions=True)


@pytest.mark.asyncio
async def test_calc_service_high_memory() -> None:
    """Start at 8100 MB memory usage, so that only 1 should run concurrently."""
    service_provider = ServiceLocator().service_provider
    gap_filling_module_loader = GapFillingModuleLoader()

    counter = {"running": 0}
    with mock.patch("psutil.Process") as MockProcess:
        mock_vm = mock.Mock()
        mock_vm.rss = 8600 * 1024 * 1024
        MockProcess.return_value.memory_info.return_value = mock_vm
        orchestrator = DummyOrchestrator(counter=counter, mock_vm=mock_vm, run_time_sec=2)
        with mock.patch(
            "core.service.calc_service.CalcService.create_calc_graph_and_orchestrator",
            return_value=(None, copy.copy(orchestrator)),
        ):
            calc_service = CalcService(
                service_provider,
                service_provider.glossary_service,
                service_provider.node_service,
                gap_filling_module_loader,
            )

            async def run_orchestrators() -> None:
                await asyncio.gather(
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                    calc_service.calculate(
                        Calculation(child_of_root_node_uid=uuid.uuid4(), root_node_uid=uuid.uuid4())
                    ),
                )

            orchestrator_task = asyncio.create_task(run_orchestrators())

            await asyncio.sleep(2)
            assert counter["running"] == 1
            await asyncio.sleep(2)
            assert counter["running"] == 1

            orchestrator_task.cancel()

            # Ensure all tasks are properly cancelled
            await asyncio.gather(orchestrator_task, return_exceptions=True)
