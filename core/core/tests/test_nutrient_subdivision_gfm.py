"Test Nutrient Subdivision Gap filling module."
import uuid
from typing import Optional, Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.ingredicalc.helpers import nutrients_dict_to_prop
from gap_filling_modules.match_product_name_gfm import MATCH_PRODUCT_GFM_NAME, MatchProductNameGapFillingWorker
from gap_filling_modules.nutrient_subdivision_gfm import NutrientSubdivisionGapFillingFactory

from core.domain.calculation import Calculation
from core.domain.glossary_link import GlossaryLink
from core.domain.matching_item import MatchingItem
from core.domain.nodes import FoodProcessingActivityNode, ModeledActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.node import Node
from core.domain.props import GlossaryTermProp, NamesProp
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.quantity_package_prop import QuantityPackageProp
from core.domain.term import Term
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceProvider
from core.tests.conftest import TermAccessGroupIdsRType, append_flow_node, is_close, prepend_flow_node
from core.tests.test_ingredient_amount_estimator_gfm import nutrients_mixer
from core.tests.test_orchestration_gfm import run_orchestrator_on_recipe
from database.postgres.postgres_db import PostgresDb

# this data comes from the nutrition files
LOW_FAT_COCOA = 10.9  # g/100g
HIGH_FAT_COCOA = 20.6
MED_FAT_COCOA = (LOW_FAT_COCOA + HIGH_FAT_COCOA) / 2


async def create_dummy_activity_with_ingredient(
    ingredient_name: str,
    ingredient_xid: str,
    foodex2_term_group_uid: uuid.UUID,
    eurofir_term_group_uid: uuid.UUID,
    service_provider: ServiceProvider,
    pg_db: PostgresDb,
    nutrient_values: dict,
    schedule_parent_ingredient_amount_estimation: bool = False,
    dried_ingredient: bool = False,
    add_dummy_nutrient_term: bool = False,
) -> CalcGraph:
    dummy_activity = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            uid=uuid.uuid4(),
            gfm_state=(
                GfmStateProp.unvalidated_construct(worker_states={"IngredientAmountEstimatorGapFillingWorker": "S"})
                if schedule_parent_ingredient_amount_estimation
                else None
            ),
        )
    )

    dried_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0116", foodex2_term_group_uid)
    root_nutrient_term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid(
        "Root_Nutrients", eurofir_term_group_uid
    )

    nutrient_values_qty_package_prop = nutrients_dict_to_prop(nutrient_values)

    # create one ingredient & add it to the graph
    ingredient_node = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProductFlowNode(
            product_name=[{"language": "de", "value": ingredient_name}],
            nutrient_values=QuantityPackageProp.unvalidated_construct(
                quantities=nutrient_values_qty_package_prop.quantities,
                for_reference=nutrient_values_qty_package_prop.for_reference,
                prop_term_uid=root_nutrient_term.uid if add_dummy_nutrient_term else None,
            ),
            type="recipe-ingredients",
            glossary_tags=(
                [GlossaryTermProp.unvalidated_construct(term_uid=dried_term.uid)] if dried_ingredient else None
            ),
        )
    )
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=dummy_activity.uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(ingredient_node)
    calc_graph.add_node(dummy_activity)
    calc_graph.add_edge(dummy_activity.uid, ingredient_node.uid)

    # attach Term to ingredient & matching
    ingredient_term: Term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid(
        ingredient_xid, foodex2_term_group_uid
    )

    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module=MATCH_PRODUCT_GFM_NAME,
            term_uids=[str(ingredient_term.uid)],
            matching_string=ingredient_name,
            lang="de",
        )
    )
    calc_graph.apply_mutation(
        PropMutation(
            created_by_module=MatchProductNameGapFillingWorker.__class__.__name__,
            node_uid=ingredient_node.uid,
            prop_name="product_name",
            prop=NamesProp(
                terms=[GlossaryTermProp(term_uid=ingredient_term.uid)],
                source_data_raw=[{"language": "de", "value": ingredient_name}],
            ),
        )
    )
    return calc_graph, ingredient_node, ingredient_term


@pytest.mark.asyncio
async def test_nutrient_subdivision(
    setup_services: Tuple[PostgresDb, ServiceProvider], get_term_access_group_uids: TermAccessGroupIdsRType
) -> None:
    """Test the nutrient subdivision gap filling module.

    - run nutrition_subdivision_GFM, nothing happens
    - add subdivision Terms for cocoa
    - run nutrition_subdivision_GFM, subdivision happens
    """
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    calc_graph, cocoa_ingredient, cocoa_term = await create_dummy_activity_with_ingredient(
        "Kakaopulver",
        "A03HG",
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        service_provider,
        pg_db,
        {"fat_gram": MED_FAT_COCOA},
        schedule_parent_ingredient_amount_estimation=True,
    )

    # now run the NutrientSubdivision GFM
    gfm_factory = NutrientSubdivisionGapFillingFactory(pg_db, service_provider)
    await gfm_factory.init_cache()
    gap_filling_worker = gfm_factory.spawn_worker(cocoa_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    product_name_prop = cocoa_ingredient.product_name
    assert len(product_name_prop.terms) == 1

    prop_term = product_name_prop.terms[0].get_term()
    assert isinstance(prop_term, Term)
    assert prop_term.xid == "A03HG"

    assert cocoa_ingredient.is_subdivision is False, "is_subdivision should not be set yet"
    assert len(cocoa_ingredient.get_sub_nodes()) == 0, "At this stage, the ingredient should not have any sub-divisions"

    # This is the 2nd part of the test: add the subdivision data and run the GFM again

    await add_subdivision_data(
        cocoa_term,
        service_provider,
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        nutrient_subdivision_term_group_uid,
    )

    # now run the NutrientSubdivision GFM again
    gfm_factory = NutrientSubdivisionGapFillingFactory(pg_db, service_provider)
    await gfm_factory.init_cache()
    gap_filling_worker = gfm_factory.spawn_worker(cocoa_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert len(cocoa_ingredient.get_sub_nodes()) == 1, "At this stage, cocoa should have sub-divisions"

    splitted_nodes = cocoa_ingredient.get_sub_nodes()[0].get_sub_nodes()
    # partially defatted (732), partially defatted (732) (dried), low-fat (733)
    assert len(splitted_nodes) == 3, "At this stage, cocoa should have 3 sub-divisions"

    for node in splitted_nodes:
        node_name = node.product_name.source_data_raw[0]["value"]

        assert node_name == "Kakaopulver" or "Kakaopulver (dried)"
        assert node.is_subdivision, "subdivided ingredient should be marked as such"

        if "(dried)" in node_name:
            assert node.is_dried(), "No `is_dried` property attached!"
            assert "J0116" in frozenset(
                tag.get_term().xid for tag in node.glossary_tags
            ), "No dried state Term attached!"


async def add_subdivision_data(
    cocoa_term: Term,
    service_provider: ServiceProvider,
    foodex2_term_group_uid: uuid,
    eurofir_term_group_uid: uuid,
    nutrient_subdivision_term_group_uid: uuid,
    use_nutrition_term: bool = True,
    cocoa_process: Optional[Node] = None,
) -> list[Term]:
    "Create two more ingredient for the subdivision & add them to the graph."
    cocoa_ingredients_subdivided: list[Term] = []

    for name, nutrition_id in [("Kakaopulver schwach entölt", 732), ("Kakaopulver stark entölt", 733)]:
        nutrition_term: Term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            str(nutrition_id), eurofir_term_group_uid
        )

        assert nutrition_term, f"Could not find nutrition term with xid {nutrition_id} for {name}"

        t = Term(
            uid=uuid.uuid4(),
            name=name,
            xid="dummy_" + name,
            access_group_uid=foodex2_term_group_uid,
            data={},
            sub_class_of=cocoa_term.sub_class_of,
        )
        cocoa_ingredients_subdivided.append(await service_provider.glossary_service.put_term_by_uid(t))

        if cocoa_process:
            await service_provider.glossary_link_service.insert_glossary_link(
                GlossaryLink(
                    gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                    term_uids=[t.uid],
                    linked_node_uid=cocoa_process.uid,
                )
            )

        # link nutrition term to the ingredient
        if use_nutrition_term:
            await service_provider.glossary_link_service.insert_glossary_link(
                GlossaryLink(
                    term_uids=[t.uid],
                    gap_filling_module="Nutrients",
                    linked_term_uid=nutrition_term.uid,
                )
            )

    # add the subdivision term to the Glossary
    subdivision_term = Term(
        name="Kakaopulver_subdivision",
        sub_class_of=None,
        data={
            "subproduct_terms_uuids": [
                [str(cocoa_ingredients_subdivided[0].uid)],
                [str(cocoa_ingredients_subdivided[1].uid)],
            ],
        },
        access_group_uid=uuid.UUID(nutrient_subdivision_term_group_uid),
        uid=uuid.uuid4(),
        xid=str(cocoa_term.uid),
    )
    await service_provider.glossary_service.put_term_by_uid(subdivision_term)

    return cocoa_ingredients_subdivided


@pytest.mark.asyncio
async def test_drying_of_water(
    setup_services: Tuple[PostgresDb, ServiceProvider], get_term_access_group_uids: TermAccessGroupIdsRType
) -> None:
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    calc_graph, water_ingredient, water_term = await create_dummy_activity_with_ingredient(
        "wasser",
        "A03DL",
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        service_provider,
        pg_db,
        {"water_gram": 99.998, "sodium_milligram": 1, "chlorine_milligram": 1},
        dried_ingredient=True,
        add_dummy_nutrient_term=True,
    )

    gfm_factory = NutrientSubdivisionGapFillingFactory(pg_db, service_provider)
    await gfm_factory.init_cache()
    gap_filling_worker = gfm_factory.spawn_worker(water_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # Water ingredient has no max_evaporation defined yet. The upscale ratio should be extremely high.
    assert is_close(water_ingredient.get_sub_nodes()[0].get_sub_nodes()[0].nutrient_upscale_ratio.value, 45000)

    # add the subdivision term to the Glossary
    manual_subdivision_term = Term(
        name="wasser_subdivision",
        sub_class_of=None,
        data={
            # In reality, it should be 0, but for tests, use non-zero value to see
            # that we can dry according to max_evaporation.
            "max_evaporation": 0.1,
        },
        access_group_uid=uuid.UUID(nutrient_subdivision_term_group_uid),
        uid=uuid.uuid4(),
        xid=str(water_term.uid),
    )
    await service_provider.glossary_service.put_term_by_uid(manual_subdivision_term)

    calc_graph, water_ingredient, water_term = await create_dummy_activity_with_ingredient(
        "wasser",
        "A03DL",
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        service_provider,
        pg_db,
        {"water_gram": 99.998, "sodium_milligram": 1, "chlorine_milligram": 1},
        dried_ingredient=True,
        add_dummy_nutrient_term=True,
    )

    gfm_factory = NutrientSubdivisionGapFillingFactory(pg_db, service_provider)
    await gfm_factory.init_cache()
    gap_filling_worker = gfm_factory.spawn_worker(water_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # Water ingredient has max_evaporation defined now. The upscale ratio should be much lower.
    assert is_close(
        water_ingredient.get_sub_nodes()[0].get_sub_nodes()[0].nutrient_upscale_ratio.value, 1.1111086419807956
    )


@pytest.mark.asyncio
async def test_nutrient_subdivision_with_zero_nutrition_term(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
) -> None:
    "In this case we expect no sub-division (fallback in ingredient-amount-estimation)."
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    # attach Term to ingredient & matching
    cocoa_term: Term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A03HG", foodex2_term_group_uid)
    assert cocoa_term, "Could not find cocoa term"
    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module=MATCH_PRODUCT_GFM_NAME,
            term_uids=[str(cocoa_term.uid)],
            matching_string="Kakaopulver",
            lang="de",
        )
    )

    recipe = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            title=[
                {
                    "language": "de",
                    "value": "Kakaopulver recipe",
                },
            ],
        )
    )
    ingredients_declaration = [
        {
            "language": "de",
            "value": "Kakaopulver",
        },
    ]
    _ = await append_flow_node(pg_db.get_graph_mgr(), recipe.uid, ingredients_declaration=ingredients_declaration)
    root_flow = await prepend_flow_node(pg_db.get_graph_mgr(), recipe.uid, nutrient_values={"fat_gram": 0.0})

    await add_subdivision_data(
        cocoa_term,
        service_provider,
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        nutrient_subdivision_term_group_uid,
        use_nutrition_term=False,
    )

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    calculation = Calculation(
        uid=uuid.uuid4(), root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    # we expect that the ingredient-splitter attaches the cocoa-node,
    # realizes that all fixed percentages are given (because it is only one ingredient in the ingredient-declaration)
    # and cancels the NutrientSubdivisionGapFillingWorker because there are no positve nutrient-values
    cocoa_ingredient = root_node.get_sub_nodes()[0].get_sub_nodes()[0]
    # first check that this is really the expected cocoa-ingredient
    assert isinstance(cocoa_ingredient, FoodProductFlowNode)
    assert cocoa_ingredient.added_by == "AddClientNodesGapFillingWorker"
    assert cocoa_ingredient.ingredients_declaration == [{"language": "de", "value": "Kakaopulver"}]

    # then check that the Ingredient-Splitter behaved like expected. Note t
    assert len(cocoa_ingredient.get_sub_nodes()) == 1
    assert cocoa_ingredient.get_sub_nodes()[0].added_by == "IngredientSplitterGapFillingWorker"
    assert len(cocoa_ingredient.get_sub_nodes()[0].get_sub_nodes()) == 1
    added_sub_ingredient = cocoa_ingredient.get_sub_nodes()[0].get_sub_nodes()[0]
    assert isinstance(added_sub_ingredient, FoodProductFlowNode)
    assert added_sub_ingredient.added_by == "IngredientSplitterGapFillingWorker"
    assert added_sub_ingredient.product_name.source_data_raw[0]["value"] == "Kakaopulver"


@pytest.mark.parametrize(
    "cocoa_fat",
    [
        LOW_FAT_COCOA,
        MED_FAT_COCOA,
        HIGH_FAT_COCOA,
    ],
)
@pytest.mark.asyncio
async def test_nutrient_subdivision_with_recipe(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
    cocoa_fat: float,
) -> None:
    """Run 3x with different cocoa fat in the recipe.

    Test that the subdivision is working both ways
    (= that the weight decrease constraint is deactivated).
    """
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    # attach Term to ingredient & matching
    cocoa_term: Term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A03HG", foodex2_term_group_uid)
    assert cocoa_term, "Could not find cocoa term"
    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module=MATCH_PRODUCT_GFM_NAME,
            term_uids=[str(cocoa_term.uid)],
            matching_string="Kakaopulver",
            lang="de",
        )
    )

    recipe = await pg_db.get_graph_mgr().upsert_node_by_uid(FoodProcessingActivityNode())

    ingredients_declaration = [
        {
            "language": "de",
            "value": "Kakaopulver",
        },
    ]
    _ = await append_flow_node(pg_db.get_graph_mgr(), recipe.uid, ingredients_declaration=ingredients_declaration)

    root_flow = await prepend_flow_node(
        pg_db.get_graph_mgr(),
        recipe.uid,
        nutrient_values={
            "fat_gram": cocoa_fat,
        },
    )

    cocoa_process = await pg_db.get_graph_mgr().upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["EDB", "437443338484ca565ae5a832d81fcc52_copy1"],
            flow="ed50040b-8630-4560-9b12-dd6cb2c9d741",
            name="market for cacao (w/o transport)",
            type="process",
            database="EDB",
            filename="d1c3bee4-e074-4da9-a8f9-96742233be71_ed50040b-8630-4560-9b12-dd6cb2c9d741.spold",
            reference_product="cacao",
        )
    )
    await pg_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[cocoa_term.uid],
            linked_node_uid=cocoa_process.uid,
        )
    )

    await add_subdivision_data(
        cocoa_term,
        service_provider,
        foodex2_term_group_uid,
        eurofir_term_group_uid,
        nutrient_subdivision_term_group_uid,
        cocoa_process=cocoa_process,
    )

    # run the calculation
    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider=service_provider)
    calc_service = CalcService(
        service_provider,
        service_provider.glossary_service,
        service_provider.node_service,
        gap_filling_module_loader,
    )

    new_calculation_uid = uuid.uuid4()

    calculation = Calculation(
        uid=new_calculation_uid, root_node_uid=root_flow.uid, child_of_root_node_uid=recipe.uid, return_log=True
    )

    calculation, root_node = await calc_service.calculate(calculation)

    sub_division_nodes = (
        root_node.get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()
    )

    assert sub_division_nodes

    for subdivision_node in sub_division_nodes:
        unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
        recipe_node = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
        recipe_node_unit = recipe_node.production_amount.get_unit_term()

        recipe_node_production_amount_gram = recipe_node.production_amount.value * recipe_node_unit.data["mass-in-g"]

        ingredient_amount_gram = round(subdivision_node.amount.amount_for_root_node().value, 3) * unit.data["mass-in-g"]

        # Scale the ingredient_amount such that it is for 100g of the recipe_node production.
        ingredient_amount = ingredient_amount_gram / recipe_node_production_amount_gram * 100

        name = service_provider.glossary_service.terms_by_uid[subdivision_node.product_name.terms[0].term_uid].name

        if cocoa_fat == LOW_FAT_COCOA:
            # we set the recipe's fat to equal the LOW fat cocoa; the ingredient amount estimator should choose
            # 0g of the HIGH fat cocoa and 100g of the LOW fat cocoa
            if name == "Kakaopulver schwach entölt" and subdivision_node.is_dried():  # HIGH fat cocoa (dried)
                assert 0 <= ingredient_amount < 0.1, "should not use dried high fat cocoa"
            elif name == "Kakaopulver schwach entölt":  # HIGH fat cocoa
                assert 0 <= ingredient_amount < 0.1, "should not use high fat cocoa"
            elif name == "Kakaopulver stark entölt":  # LOW fat cocoa
                assert 99 < ingredient_amount <= 100, "should use much of low fat cocoa"
            else:
                raise ValueError(f"name {name} not matched for asserts")

        elif cocoa_fat == MED_FAT_COCOA:
            # we set the recipe's fat to equal the MED fat cocoa; the ingredient amount estimator should choose
            # 50g of the HIGH fat cocoa and 50g of the LOW fat cocoa
            if name == "Kakaopulver schwach entölt" and subdivision_node.is_dried():  # HIGH fat cocoa (dried)
                assert 22.5 <= ingredient_amount < 23.5, "should use around 23g of dried high fat cocoa"
            elif name == "Kakaopulver schwach entölt":  # HIGH fat cocoa
                assert 24.0 < ingredient_amount < 25.0, "should use around 25g of high fat cocoa"
            elif name == "Kakaopulver stark entölt":  # LOW fat cocoa
                assert 51.5 < ingredient_amount < 52.5, "should use around 52g of low fat cocoa"
            else:
                raise ValueError(f"name {name} not matched for asserts")

        elif cocoa_fat == HIGH_FAT_COCOA:
            # we set the recipe's fat to equal the HIGH fat cocoa; the ingredient amount estimator should choose
            # 100g of the HIGH fat cocoa and 0g of the LOW fat cocoa
            if name == "Kakaopulver schwach entölt" and subdivision_node.is_dried():  # HIGH fat cocoa (dried)
                assert 62.0 < ingredient_amount < 63.0, "should use around 63g of dried high fat cocoa"
            elif name == "Kakaopulver schwach entölt":  # HIGH fat cocoa
                assert 32.0 < ingredient_amount < 32.5, "should use around 32g of high fat cocoa"
            elif name == "Kakaopulver stark entölt":  # LOW fat cocoa
                assert 5.0 < ingredient_amount < 5.5, "should use only 5g low fat cocoa"
            else:
                raise ValueError(f"name {name} not matched for asserts")

        else:
            raise ValueError("invalid cocoa fat")


def calculate_dried_product_nutrients(
    non_dried_product_nutrients: dict[str, float], max_evaporation: float
) -> dict[str, float]:
    dried_product_nutrients = {}

    water_g_per_100g = non_dried_product_nutrients["water_gram"]
    nutrient_upscale_ratio: float = 100 / (100 - max_evaporation * water_g_per_100g)
    for key, value in non_dried_product_nutrients.items():
        if key == "water_gram":
            dried_product_nutrients[key] = (1.0 - max_evaporation) * water_g_per_100g * nutrient_upscale_ratio
            continue
        dried_product_nutrients[key] = value * nutrient_upscale_ratio
    return dried_product_nutrients


@pytest.mark.parametrize(
    "dried_onion_ratio",
    [0.0, 0.4, 0.6, 1.0],
)
@pytest.mark.asyncio
async def test_automatic_nutrient_subdivision_on_onion(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    load_seeded_nutrients: dict[str, dict[str, float]],
    dried_onion_ratio: float,
) -> None:
    "Test the automatic nutrient-subdivision for onions."
    pg_db, service_provider = setup_services

    onion_nutrients = load_seeded_nutrients["onion"]

    # get the dried carrot nutrients
    dried_onion_nutrients = calculate_dried_product_nutrients(onion_nutrients, max_evaporation=0.9)

    # mix the nutrients
    mixed_nutrients = nutrients_mixer(
        (onion_nutrients, dried_onion_nutrients), (1 - dried_onion_ratio, dried_onion_ratio)
    )

    # set up the recipe
    recipe = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            title=[
                {
                    "language": "de",
                    "value": "Onion recipe",
                },
            ],
        )
    )
    ingredients_declaration = [
        {
            "language": "de",
            "value": "Zwiebeln",
        },
    ]
    _ = await append_flow_node(pg_db.get_graph_mgr(), recipe.uid, ingredients_declaration=ingredients_declaration)
    root_flow = await prepend_flow_node(pg_db.get_graph_mgr(), recipe.uid, nutrient_values=mixed_nutrients)

    # run the calculation
    calc_graph, _ = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)
    root_node = calc_graph.get_root_node()

    # There should only be one unrelated data error
    assert len(calc_graph.data_errors_log) == 3
    assert "No location found for product" in calc_graph.data_errors_log[0].message
    assert "No location found for product" in calc_graph.data_errors_log[1].message
    assert "Unknown location found in origin location prop of node" in calc_graph.data_errors_log[2].message

    sub_division_nodes = (
        root_node.get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()[0]
        .get_sub_nodes()
    )

    assert sub_division_nodes

    for subdivision_node in sub_division_nodes:
        unit = subdivision_node.amount.amount_for_root_node().get_unit_term()
        recipe_node = root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
        recipe_node_unit = recipe_node.production_amount.get_unit_term()

        recipe_node_production_amount_gram = recipe_node.production_amount.value * recipe_node_unit.data["mass-in-g"]

        ingredient_amount_gram = round(subdivision_node.amount.amount_for_root_node().value, 3) * unit.data["mass-in-g"]

        # Scale the ingredient_amount such that it is the ration of the recipe_node production.
        ingredient_amount = ingredient_amount_gram / recipe_node_production_amount_gram

        name = subdivision_node.name()

        if name == "Zwiebeln":
            assert is_close(ingredient_amount, 1 - dried_onion_ratio)
        elif name == "Zwiebeln (dried)":
            assert is_close(ingredient_amount, dried_onion_ratio)
        else:
            raise ValueError(f"name {name} not matched for asserts")


@pytest.mark.asyncio
async def test_nutrient_sub_division_dried_product_that_has_no_water_to_evaporate_and_is_linked_to_none_dried_nutrients(
    setup_services: Tuple[PostgresDb, ServiceProvider],
    get_term_access_group_uids: TermAccessGroupIdsRType,
    load_seeded_nutrients: dict[str, dict[str, float]],
) -> None:
    """Tests an edge case in automatic nutrient-subdivision.

    We are considering a for a product where the nutrient-values are not officially coming from a dried product,
    but it is still not possible to generate a dried version of that product (here because water_in_gram = 0).
    In this case we want that nutrient-subdivision-gfm just takes the flow-node on which it was scheduled as
    the 'dried' product and not append any activity below it.
    """
    pg_db, service_provider = setup_services
    (
        _,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        _,
        _,
        _,
        _,
    ) = get_term_access_group_uids

    # we just take any nutrients, here it doesn't matter which since the calculation will anyway fail because no
    # brightway nodes are mocked for emulgator.
    onion_nutrients = load_seeded_nutrients["onion"]

    # insert matching for "getrockneter Emulgator" and the corresponding nutrient-file. The latter without the dried
    # term such that nutrient-subdivision-gfm assumes that the nutrient-profile is from a non-dried product.
    emulgator_term: Term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("A047V", foodex2_term_group_uid)
    dried_term: Term = await pg_db.pg_term_mgr.get_term_by_xid_and_access_group_uid("J0116", foodex2_term_group_uid)
    nutrition_term: Term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
        "528", eurofir_term_group_uid
    )
    assert nutrition_term, "Could not find nutrition term with xid 528"
    await service_provider.matching_service.put_matching_item(
        MatchingItem(
            gap_filling_module=MATCH_PRODUCT_GFM_NAME,
            term_uids=[str(emulgator_term.uid), str(dried_term.uid)],
            matching_string="getrockneter Emulgator",
            lang="de",
        )
    )
    await service_provider.glossary_link_service.insert_glossary_link(
        GlossaryLink(
            term_uids=[emulgator_term.uid],
            gap_filling_module="Nutrients",
            linked_term_uid=nutrition_term.uid,
        )
    )

    # create the recipe
    recipe = await pg_db.get_graph_mgr().upsert_node_by_uid(
        FoodProcessingActivityNode(
            title=[
                {
                    "language": "de",
                    "value": "Emulgator recipe",
                },
            ],
        )
    )
    ingredients_declaration = [
        {
            "language": "de",
            "value": "getrockneter Emulgator",
        },
    ]
    _ = await append_flow_node(pg_db.get_graph_mgr(), recipe.uid, ingredients_declaration=ingredients_declaration)
    root_flow = await prepend_flow_node(pg_db.get_graph_mgr(), recipe.uid, nutrient_values=onion_nutrients)

    # run the calculation
    calc_graph, _ = await run_orchestrator_on_recipe(service_provider, pg_db, root_flow, recipe)
    root_node = calc_graph.get_root_node()

    # There should be no gfm-errors whatsoever for nutrient-subdivision-gfm
    assert len(calc_graph.data_errors_log) == 3
    assert "Could not find Brightway mapping for FoodEx2 terms" in calc_graph.data_errors_log[1].message
    assert "Could not match product with terms" in calc_graph.data_errors_log[0].message
    assert "Unknown location found in origin location prop of node" in calc_graph.data_errors_log[2].message

    # check that nutrient-subdivision-gfm was scheduled on the correct node and that it didn't append any activity
    node_on_which_nutrient_sub_division_was_scheduled = (
        root_node.get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0].get_sub_nodes()[0]
    )
    assert (
        node_on_which_nutrient_sub_division_was_scheduled.gfm_state.worker_states.get(
            "NutrientSubdivisionGapFillingWorker"
        )
        == "F"
    )
    assert len(node_on_which_nutrient_sub_division_was_scheduled.get_sub_nodes()) == 0
