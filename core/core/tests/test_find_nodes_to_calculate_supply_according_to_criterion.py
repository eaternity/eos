import random
from typing import Tuple
from uuid import UUID

import networkx as nx
import numpy
import pytest
from gap_filling_modules.matrix_calculation_gfm import MatrixBuilder

from core.domain.calculation import Calculation
from core.domain.nodes import FlowNode, ModeledActivityNode
from core.graph_manager.calc_graph import CalcGraph
from core.service.glossary_service import GlossaryService
from core.service.service_provider import ServiceProvider
from database.postgres.pg_graph_mgr import PostgresGraphMgr
from database.postgres.postgres_db import PostgresDb

random.seed(0)
numpy.random.seed(0)


async def add_all_nodes_to_db(g: nx.DiGraph, graph_mgr: PostgresGraphMgr) -> tuple[dict, dict]:
    """For all nodes and edges in the networkx graph add a node to the database.

    :param g: networkx graph
    :param graph_mgr: graph manager
    :return id_to_uid: dictionary mapping networkx node ids to activity node uids
    :return flow_id_to_uid: dictionary mapping networkx edge ids to flow node uids
    """
    # add all activities to the database
    id_to_uid = {}
    for node_id in list(g.nodes):
        # in the future we might want to add other types of activities
        activity_node = ModeledActivityNode()
        await graph_mgr.upsert_node_by_uid(activity_node)
        id_to_uid[node_id] = activity_node.uid

    # add all flows to the database
    flow_id_to_uid = {}
    for edge in list(g.edges):
        # in the future we might want to add other types of flows
        amount = 1.0
        activity_type = "technosphere"
        flow_node = FlowNode(amount_in_original_source_unit={"value": amount}, type=activity_type)
        await graph_mgr.upsert_node_by_uid(flow_node)
        # create a unique id for the flow and map it to the uid
        flow_id = f"{edge[0]}_{edge[1]}"
        flow_id_to_uid[flow_id] = flow_node.uid

    return id_to_uid, flow_id_to_uid


async def create_calc_graph_from_nx_graph(
    g: nx.DiGraph,
    service_provider: ServiceProvider,
    graph_mgr: PostgresGraphMgr,
    save_as_system_process: bool,
    fixed_depth_for_calculating_supply: int,
    gfms_for_which_to_save_most_shallow_nodes: list,
    id_to_uid: dict,
    flow_id_to_uid: dict,
) -> CalcGraph:
    """Generate a CalcGraph from a networkx graph.

    :param g: networkx graph
    :param service_provider: service provider
    :param graph_mgr: graph manager
    :param save_as_system_process: whether to save as system process
    :param fixed_depth_for_calculating_supply: fixed depth for calculating supply
    :param gfms_for_which_to_save_most_shallow_nodes: list of gfms which might change impact assessments dynamically
    :param id_to_uid: dictionary mapping networkx node ids to activity node uids
    :param flow_id_to_uid: dictionary mapping networkx edge ids to flow node uids
    :return calc_graph: CalcGraph
    """
    # create the Calculation
    calculation = Calculation(
        save_as_system_process=save_as_system_process,
        fixed_depth_for_calculating_supply=fixed_depth_for_calculating_supply,
        gfms_for_which_to_save_most_shallow_nodes=gfms_for_which_to_save_most_shallow_nodes,
    )

    # create the CalcGraph
    glossary_service: GlossaryService = service_provider.glossary_service
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=id_to_uid[0],
        glossary_service=glossary_service,
        calculation=calculation,
    )

    # for each node in the networkx graph add an activity node in the CalcGraph
    for node_id in list(g.nodes):
        # load activity node from database
        activity_node = await graph_mgr.find_by_uid(id_to_uid[node_id])
        # add node to the calc graph
        calc_graph.add_node(activity_node)

    # for each edge in the nx graph add a flow node in the CalcGraph
    for edge in list(g.edges):
        # load flow node from database
        flow_node = await graph_mgr.find_by_uid(flow_id_to_uid[f"{edge[0]}_{edge[1]}"])
        # load parent and child nodes from the CalcGraph
        parent_node = calc_graph.get_node_by_uid(id_to_uid[edge[0]])
        child_node = calc_graph.get_node_by_uid(id_to_uid[edge[1]])
        # add flow to the CalcGraph
        calc_graph.add_node(flow_node)
        calc_graph.add_edge(parent_node_uid=parent_node.uid, child_node_uid=flow_node.uid)
        calc_graph.add_edge(parent_node_uid=flow_node.uid, child_node_uid=child_node.uid)

    return calc_graph


def check_that_all_nodes_below_cannot_change_dynamically(
    calc_graph: CalcGraph, most_shallow_node_uids: dict[UUID, bool], node_uids_that_can_change: set[UUID]
) -> None:
    """Check that all nodes below the most shallow nodes cannot change dynamically.

    :param calc_graph: calculation graph
    :param most_shallow_node_uids: most shallow node uids (result of calculation)
    :param node_uids_that_can_change: node uids that can change (as initialized in the test))
    """
    for most_shallow_node_uid in most_shallow_node_uids:
        nodes_visited = set()
        most_shallow_node = calc_graph.get_node_by_uid(most_shallow_node_uid)
        nodes_to_visit = [most_shallow_node]
        while nodes_to_visit:
            node = nodes_to_visit.pop()
            nodes_visited.add(node)
            assert (
                node.uid not in node_uids_that_can_change
            ), f"Most shallow node {most_shallow_node} has child that can change dynamically {node}"
            for child in node.get_sub_nodes():
                if child not in nodes_visited:
                    nodes_to_visit.append(child)


@pytest.mark.asyncio
async def test_find_most_shallow_node_for_each_gfm_and_add_them_to_supply_list_on_balanced_tree(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    """Tests for a balanced tree that has nodes tagged by two different GFMs.

    :param setup_services: setup services
    """
    # load services
    pg_db, service_provider = setup_services
    graph_mgr: PostgresGraphMgr = pg_db.get_graph_mgr()

    # generate a balanced tree
    branching_factor: int = 2
    height_of_tree: int = 2
    g: nx.DiGraph = nx.generators.balanced_tree(branching_factor, height_of_tree)

    # add all nodes to the database
    id_to_uid, flow_id_to_uid = await add_all_nodes_to_db(g, graph_mgr)

    # set the calculation parameters
    save_as_system_process: bool = False
    fixed_depth_for_calculating_supply: int = 0
    gfms_for_which_to_save_most_shallow_nodes = ["GFM_a", "GFM_b"]

    # generate the calculation_graph
    calc_graph: CalcGraph = await create_calc_graph_from_nx_graph(
        g,
        service_provider,
        graph_mgr,
        save_as_system_process,
        fixed_depth_for_calculating_supply,
        gfms_for_which_to_save_most_shallow_nodes,
        id_to_uid,
        flow_id_to_uid,
    )

    # tag one node with as "can be modified by GFM a" and the other as "can be modified by GFM b"
    calc_graph.nodes_that_can_change_dynamically_for_each_gfm["GFM_a"] = set([id_to_uid[0]])
    calc_graph.nodes_that_can_change_dynamically_for_each_gfm["GFM_b"] = set([id_to_uid[1]])

    # call function that we want to test
    matrix_builder = MatrixBuilder()
    matrix_builder.find_most_shallow_node_for_each_gfm_and_add_them_to_supply_list(calc_graph)

    # get the result of the function that we want to test
    nodes_to_calculate_supply: set = calc_graph.nodes_to_calculate_supply
    meta_information_for_most_shallow_nodes: dict = calc_graph.meta_data_for_most_shallow_nodes

    # reference solution
    nodes_to_calculate_supply_reference: set = set({id_to_uid[1], id_to_uid[2], id_to_uid[3], id_to_uid[4]})
    assert nodes_to_calculate_supply == nodes_to_calculate_supply_reference
    assert meta_information_for_most_shallow_nodes.keys() == nodes_to_calculate_supply_reference
    assert meta_information_for_most_shallow_nodes[id_to_uid[1]].is_most_shallow_node_for_gfms == ["GFM_a"]
    assert meta_information_for_most_shallow_nodes[id_to_uid[1]].has_sub_nodes_that_can_change_dynamically_for_gfms == {
        "GFM_a": False,
        "GFM_b": True,
    }

    assert meta_information_for_most_shallow_nodes[id_to_uid[2]].is_most_shallow_node_for_gfms == ["GFM_a", "GFM_b"]
    assert meta_information_for_most_shallow_nodes[id_to_uid[2]].has_sub_nodes_that_can_change_dynamically_for_gfms == {
        "GFM_a": False,
        "GFM_b": False,
    }

    assert meta_information_for_most_shallow_nodes[id_to_uid[3]].is_most_shallow_node_for_gfms == ["GFM_b"]
    assert meta_information_for_most_shallow_nodes[id_to_uid[3]].has_sub_nodes_that_can_change_dynamically_for_gfms == {
        "GFM_a": False,
        "GFM_b": False,
    }

    assert meta_information_for_most_shallow_nodes[id_to_uid[4]].is_most_shallow_node_for_gfms == ["GFM_b"]
    assert meta_information_for_most_shallow_nodes[id_to_uid[4]].has_sub_nodes_that_can_change_dynamically_for_gfms == {
        "GFM_a": False,
        "GFM_b": False,
    }


@pytest.mark.asyncio
@pytest.mark.parametrize("degree_of_graph", [4, 6])
async def test_find_most_shallow_node_for_each_gfm_and_add_them_to_supply_list_on_binomial_tree(
    setup_services: Tuple[PostgresDb, ServiceProvider], degree_of_graph: int
) -> None:
    """Test on a binomial tree.

    :param setup_services: setup services
    :param degree_of_graph: degree of the graph
    """
    # load services
    pg_db, service_provider = setup_services
    graph_mgr: PostgresGraphMgr = pg_db.get_graph_mgr()

    # generate a binomial tree
    g: nx.DiGraph = nx.generators.binomial_tree(degree_of_graph)

    # add all nodes to the database
    id_to_uid, flow_id_to_uid = await add_all_nodes_to_db(g, graph_mgr)

    # number of dirty nodes that we want to test
    number_of_nodes_that_can_change_dynamically_list: list[int] = [1, 2, 3, 4, 5]

    # iterate over the different number of dirty nodes
    for number_of_nodes_that_can_change_dynamically in number_of_nodes_that_can_change_dynamically_list:
        # set the simulation parameters
        save_as_system_process: bool = False
        fixed_depth_for_calculating_supply: int = 0
        gfms_for_which_to_save_most_shallow_nodes = ["GFM_a"]

        # generate the calculation_graph
        calc_graph: CalcGraph = await create_calc_graph_from_nx_graph(
            g,
            service_provider,
            graph_mgr,
            save_as_system_process,
            fixed_depth_for_calculating_supply,
            gfms_for_which_to_save_most_shallow_nodes,
            id_to_uid,
            flow_id_to_uid,
        )

        # tag a random number of nodes
        nodes_that_can_change_dynamically: list[int] = random.sample(
            list(g.nodes), number_of_nodes_that_can_change_dynamically
        )
        node_uids_that_can_change_dynamically: set[UUID] = set()
        for node_id in nodes_that_can_change_dynamically:
            node_uids_that_can_change_dynamically.add(id_to_uid[node_id])

        # call function that we want to test
        matrix_builder = MatrixBuilder()
        calc_graph.nodes_that_can_change_dynamically_for_each_gfm["GFM_a"] = node_uids_that_can_change_dynamically
        matrix_builder.find_most_shallow_node_for_each_gfm_and_add_them_to_supply_list(calc_graph)

        # get the result of the function that we want to test
        most_shallow_nodes_uids: set = calc_graph.meta_data_for_most_shallow_nodes.keys()
        nodes_to_calculate_supply: set = calc_graph.nodes_to_calculate_supply
        assert most_shallow_nodes_uids == nodes_to_calculate_supply

        # test if the result is correct
        check_that_all_nodes_below_cannot_change_dynamically(calc_graph, nodes_to_calculate_supply, set())


@pytest.mark.asyncio
@pytest.mark.parametrize("number_of_nodes", [15])
@pytest.mark.parametrize("density_of_edges", [0.8, 1.2, 1.6])
async def test_find_most_shallow_node_for_each_gfm_and_add_them_to_supply_list_on_directed_cyclic_graph(
    setup_services: Tuple[PostgresDb, ServiceProvider], number_of_nodes: int, density_of_edges: float
) -> None:
    """Test on a random directed cyclic graph.

    :param setup_services: setup services
    :param number_of_nodes: number of nodes in the networkx graph
    :param density_of_edges: density of edges in the networkx graph
    """
    # calculate the probability for edges: density of edges is a made-up quantity to allow more sensible parametrization
    probability_for_edges: float = density_of_edges / number_of_nodes

    # load services
    pg_db, service_provider = setup_services
    graph_mgr: PostgresGraphMgr = pg_db.get_graph_mgr()

    # generate a random graph
    g: nx.DiGraph = nx.gnp_random_graph(number_of_nodes, probability_for_edges, directed=True, seed=numpy.random)

    # add all nodes to the database
    id_to_uid, flow_id_to_uid = await add_all_nodes_to_db(g, graph_mgr)

    # number of dirty nodes that we want to test
    number_of_nodes_that_can_change_dynamically_list: list[int] = [1, 2, 3, 4, 5]

    # iterate over the different number of dirty nodes
    for number_of_nodes_that_can_change_dynamically in number_of_nodes_that_can_change_dynamically_list:
        # set the simulation parameters
        save_as_system_process: bool = False
        fixed_depth_for_calculating_supply: int = 0
        gfms_for_which_to_save_most_shallow_nodes = ["GFM_a"]

        # generate the calculation_graph
        calc_graph: CalcGraph = await create_calc_graph_from_nx_graph(
            g,
            service_provider,
            graph_mgr,
            save_as_system_process,
            fixed_depth_for_calculating_supply,
            gfms_for_which_to_save_most_shallow_nodes,
            id_to_uid,
            flow_id_to_uid,
        )

        # tag a random number of nodes
        nodes_that_can_change_dynamically: list[int] = random.sample(
            list(g.nodes), number_of_nodes_that_can_change_dynamically
        )
        node_uids_that_can_change_dynamically: set[UUID] = set()
        for node_id in nodes_that_can_change_dynamically:
            node_uids_that_can_change_dynamically.add(id_to_uid[node_id])

        # call function that we want to test
        matrix_builder = MatrixBuilder()
        calc_graph.nodes_that_can_change_dynamically_for_each_gfm["GFM_a"] = node_uids_that_can_change_dynamically
        matrix_builder.find_most_shallow_node_for_each_gfm_and_add_them_to_supply_list(calc_graph)

        # get the result of the function that we want to test
        most_shallow_nodes_uids: set = calc_graph.meta_data_for_most_shallow_nodes.keys()
        nodes_to_calculate_supply: set = calc_graph.nodes_to_calculate_supply
        assert most_shallow_nodes_uids == nodes_to_calculate_supply

        # test if the result is correct
        check_that_all_nodes_below_cannot_change_dynamically(calc_graph, nodes_to_calculate_supply, set())


@pytest.mark.asyncio
@pytest.mark.parametrize("number_of_nodes", [20, 40, 80])
@pytest.mark.parametrize("density_of_edges", [1.0, 2.0, 4.0])
async def test_find_nodes_that_are_more_shallow_than_fixed_depth_and_add_them_to_supply_list_on_directed_cyclic_graph(
    setup_services: Tuple[PostgresDb, ServiceProvider], number_of_nodes: int, density_of_edges: float
) -> None:
    """Test on a random directed cyclic graph.

    :param setup_services: setup services
    :param number_of_nodes: number of nodes in the networkx graph
    :param density_of_edges: density of edges in the networkx graph
    """
    # calculate the probability for edges: density of edges is a made-up quantity to allow more sensible parametrization
    probability_for_edges: float = density_of_edges / number_of_nodes

    # load services
    pg_db, service_provider = setup_services
    graph_mgr: PostgresGraphMgr = pg_db.get_graph_mgr()

    # generate a random graph
    g: nx.DiGraph = nx.gnp_random_graph(number_of_nodes, probability_for_edges, directed=True, seed=numpy.random)

    # get the reference solution for the shortest path lengths to the root node
    paths = nx.single_source_shortest_path(g, 0)

    # add all nodes to the database
    id_to_uid, flow_id_to_uid = await add_all_nodes_to_db(g, graph_mgr)

    # cutoff values that we want to test
    cutoff_values: list[int] = [0, 1, 2, 4, 8]

    # iterate over all cutoff values
    for cutoff in cutoff_values:
        # get the reference solution
        nodes_to_calculate_supply_reference: set = set(
            [id_to_uid[k] for k in paths.keys() if ((len(paths[k]) - 1) <= cutoff)]
        )

        # set the simulation parameters
        save_as_system_process: bool = True
        # edges are converted to flows, so we need to double the cutoff
        fixed_depth_for_calculating_supply: float = cutoff * 2
        gfms_for_which_to_save_most_shallow_nodes = []

        # generate the calculation_graph
        calc_graph: CalcGraph = await create_calc_graph_from_nx_graph(
            g,
            service_provider,
            graph_mgr,
            save_as_system_process,
            fixed_depth_for_calculating_supply,
            gfms_for_which_to_save_most_shallow_nodes,
            id_to_uid,
            flow_id_to_uid,
        )

        # call function that we want to test
        matrix_builder = MatrixBuilder()
        matrix_builder.find_nodes_that_are_more_shallow_than_fixed_depth_and_add_them_to_supply_list(calc_graph)

        # get the result of the function that we want to test
        nodes_to_calculate_supply: set = calc_graph.nodes_to_calculate_supply

        # test if the result is correct
        assert len(nodes_to_calculate_supply) == len(nodes_to_calculate_supply_reference)
        assert nodes_to_calculate_supply == nodes_to_calculate_supply_reference
