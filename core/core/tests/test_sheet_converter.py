import uuid
from typing import Optional

import pandas as pd
import pytest
import structlog

from core.domain.calculation import Calculation
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.calc_service import CalcService
from core.tests.conftest import MinimumRecipeRType

logger = structlog.get_logger()


@pytest.mark.asyncio
@pytest.mark.parametrize("max_depth", [None, 1])
async def test_calc_service(create_minimum_recipe: MinimumRecipeRType, max_depth: Optional[int]) -> None:
    """Tests the sheet converter on the minimum_recipe fixture."""
    postgres_db, service_provider, recipe, carrot_ingredient, _, root_node = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    gap_filling_module_loader = GapFillingModuleLoader()
    await gap_filling_module_loader.init(service_provider)
    node_service = service_provider.node_service
    calc_service = CalcService(service_provider, glossary_service, node_service, gap_filling_module_loader)

    new_calculation_uid = uuid.uuid4()
    calculation = Calculation(
        uid=new_calculation_uid,
        root_node_uid=root_node.uid,
        child_of_root_node_uid=recipe.uid,
        return_data_as_table=True,
        max_depth_of_returned_table=max_depth,
    )

    calculation, root_node = await calc_service.calculate(calculation)

    row_names = [row["name"].strip() for row in calculation.final_graph_table]
    assert "Karotten" in row_names
    if max_depth is None:
        assert "Scarce Water Consumption" in row_names
        assert "Pre-carriage" in row_names
        assert "Fuel consumption of Truck transport" in row_names

    final_table = pd.DataFrame(calculation.final_graph_table)
    logger.debug(
        "final_table", final_table=final_table[["name", "tree_index", "flow_amount_per_root_node", "flow_unit"]]
    )
