"Test attach food tags gap filling module."
import uuid
from typing import Tuple

import pytest
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum
from gap_filling_modules.attach_food_tags_gfm import AttachFoodTagsGapFillingFactory
from gap_filling_modules.match_product_name_gfm import MatchProductNameGapFillingFactory

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.graph_manager.calc_graph import CalcGraph
from core.service.service_provider import ServiceProvider
from core.tests.conftest import MinimumRecipeRType
from database.postgres.postgres_db import PostgresDb

RAINFOREST_CONSERVATION_CERTIFIED = "EOS_rainforest_conservation_certified"
CERTIFIED_RAINFOREST_NOT_SPECIFIED = "EOS_certified_rainforest_not_specified"
RAINFOREST_CERTIFIED_XIDS = (RAINFOREST_CONSERVATION_CERTIFIED, CERTIFIED_RAINFOREST_NOT_SPECIFIED)


@pytest.mark.asyncio
async def test_attach_food_tags_gfm_with_conservation_field(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_flow = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    carrot_ingredient.raw_conservation = {
        "value": "fresh, cooled, conserved",
        "language": "asdf",
    }  # Because fresh, cooled, and conserved are in ConservationOptionsEnum, it should be correctly matched.

    calc_graph.add_node(carrot_ingredient)

    # First run MatchProductNameGFM to obtain product name.
    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # Then, run AttachFoodTagsGFM.
    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    assert type(carrot_ingredient) is FoodProductFlowNode
    assert all(type(glossary_tag) is GlossaryTermProp for glossary_tag in carrot_ingredient.glossary_tags)
    food_glossary_term_xids = frozenset(glossary_tag.get_term().xid for glossary_tag in carrot_ingredient.glossary_tags)
    food_glossary_raw_tags = frozenset(
        food_glossary_tag.source_data_raw
        for food_glossary_tag in carrot_ingredient.glossary_tags
        if food_glossary_tag.source_data_raw
    )

    assert "EOS_HIGH-PERISHABLE" in food_glossary_term_xids
    assert "P0120" in food_glossary_term_xids
    assert "J0131" in food_glossary_term_xids
    assert "J0001" in food_glossary_term_xids
    assert "fresh" in food_glossary_raw_tags
    assert "cooled" in food_glossary_raw_tags
    assert "conserved" in food_glossary_raw_tags


@pytest.mark.asyncio
async def test_attach_food_tags_gfm_with_production_field(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_flow = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )
    assert isinstance(carrot_ingredient, FoodProductFlowNode)
    carrot_ingredient.raw_production = {
        "value": "standard",
        "language": "asdf",
    }  # Because standard is in ProductionOptionsEnum, it should be correctly matched even with the wrong language.

    calc_graph.add_node(carrot_ingredient)

    # First run MatchProductNameGFM to obtain product name.
    gap_filling_worker = MatchProductNameGapFillingFactory(postgres_db, service_provider).spawn_worker(
        carrot_ingredient
    )
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    # Then, run AttachFoodTagsGFM.
    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.gfm_factory.init_cache()
    await gap_filling_worker.run(calc_graph)

    assert all(type(glossary_tag) is GlossaryTermProp for glossary_tag in carrot_ingredient.glossary_tags)
    food_glossary_term_xids = frozenset(glossary_tag.get_term().xid for glossary_tag in carrot_ingredient.glossary_tags)
    food_glossary_raw_tags = frozenset(
        food_glossary_tag.source_data_raw
        for food_glossary_tag in carrot_ingredient.glossary_tags
        if food_glossary_tag.source_data_raw
    )
    assert "EOS_prod_standard" in food_glossary_term_xids
    assert carrot_ingredient.raw_production.get("value") in food_glossary_raw_tags


@pytest.mark.asyncio
async def test_attach_food_tags_gfm_with_wrong_node_type(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    postgres_db, service_provider = setup_services
    glossary_service = service_provider.glossary_service
    assert len(glossary_service.terms_by_uid) > 0

    node_uid = uuid.uuid4()
    # default NodeType is 'recipe'
    recipe_node = FoodProcessingActivityNode(uid=node_uid)

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=glossary_service,
    )
    calc_graph.add_node(recipe_node)

    # running GFM's `should_be_scheduled` method
    gap_filling_factory = AttachFoodTagsGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(recipe_node)

    assert not gap_filling_worker.should_be_scheduled()


@pytest.mark.asyncio
async def test_attach_food_tags_gfm_with_no_prod_name(create_minimum_recipe: MinimumRecipeRType) -> None:
    postgres_db, service_provider, _, carrot_ingredient, _, _root_flow = create_minimum_recipe
    glossary_service = service_provider.glossary_service

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=carrot_ingredient.uid,
        glossary_service=glossary_service,
    )

    calc_graph.add_node(carrot_ingredient)

    gap_filling_worker = AttachFoodTagsGapFillingFactory(postgres_db, service_provider).spawn_worker(carrot_ingredient)
    assert gap_filling_worker.should_be_scheduled()
    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready


@pytest.mark.asyncio
async def test_attach_food_tags_gfm_with_composed_name(setup_services: Tuple[PostgresDb, ServiceProvider]) -> None:
    """Matching for dried potatoes (trockene Kartoffeln)."""
    postgres_db, service_provider = setup_services

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        product_name=[
            {
                "language": "de",
                "value": "getrockene kartoffeln",
            }
        ],
        type="conceptual-ingredients",
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=service_provider.glossary_service,
    )
    calc_graph.add_node(ingredient_node)

    # First run MatchProductNameGFM to obtain product name.
    gap_filling_factory = MatchProductNameGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)

    # Then, run AttachFoodTagsGFM.
    gap_filling_factory = AttachFoodTagsGapFillingFactory(postgres_db, service_provider)
    await gap_filling_factory.init_cache()
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)

    food_glossary_term_xids = frozenset(
        glossary_tag.get_term().xid
        for glossary_tag in ingredient_node.glossary_tags + ingredient_node.product_name.terms
    )
    food_glossary_raw_tags = frozenset(
        food_glossary_tag.source_data_raw
        for food_glossary_tag in ingredient_node.glossary_tags
        if food_glossary_tag.source_data_raw
    )

    assert "EOS_STABLE" in food_glossary_term_xids
    assert "J0116" in food_glossary_term_xids  # Dried term extracted from name.
    assert not food_glossary_raw_tags


@pytest.mark.asyncio
async def test_check_labels_gfm_rainforest_conservation_certified(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        raw_labels={"value": "Rain Forest Alliance certified", "language": "en"},
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=service_provider.glossary_service,
    )

    calc_graph.add_node(ingredient_node)

    gap_filling_factory = AttachFoodTagsGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)

    assert gap_filling_worker.should_be_scheduled()

    assert gap_filling_worker.can_run_now() == GapFillingWorkerStatusEnum.ready

    await gap_filling_worker.run(calc_graph)

    assert all(isinstance(label, GlossaryTermProp) for label in ingredient_node.glossary_tags)

    terms = [label.get_term() for label in ingredient_node.glossary_tags]

    rainforest_certified = None
    for term in terms:
        if term.xid in RAINFOREST_CERTIFIED_XIDS:
            rainforest_certified = term.xid

    assert rainforest_certified == RAINFOREST_CONSERVATION_CERTIFIED


@pytest.mark.asyncio
async def test_check_labels_gfm_certified_rainforest_not_specified(
    setup_services: Tuple[PostgresDb, ServiceProvider]
) -> None:
    postgres_db, service_provider = setup_services

    node_uid = uuid.uuid4()
    ingredient_node = FoodProductFlowNode(
        uid=node_uid,
        raw_labels={"value": "USDA Organic", "language": "en"},
    )

    # create the calculation graph:
    calc_graph = CalcGraph(
        service_provider,
        root_node_uid=node_uid,
        glossary_service=service_provider.glossary_service,
    )

    calc_graph.add_node(ingredient_node)

    gap_filling_factory = AttachFoodTagsGapFillingFactory(postgres_db, service_provider)
    gap_filling_worker = gap_filling_factory.spawn_worker(ingredient_node)
    await gap_filling_worker.run(calc_graph)
    terms = [label.get_term() for label in ingredient_node.glossary_tags]
    rainforest_certified = None
    for term in terms:
        if term.xid in RAINFOREST_CERTIFIED_XIDS:
            rainforest_certified = term.xid

    assert rainforest_certified == CERTIFIED_RAINFOREST_NOT_SPECIFIED
