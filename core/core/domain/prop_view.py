import copy
from typing import Any, Optional

from core.domain.deep_mapping_view import DeepListView, DeepMappingView
from core.domain.term import Term


class PropView:
    def __init__(self, prop=None):
        self._prop = prop

    def get_data(self) -> Optional[DeepMappingView | DeepListView | Any]:
        if type(self._prop.data) is dict:
            return DeepMappingView(self._prop.data)
        elif type(self._prop.data) is list:
            return DeepListView(self._prop.data)
        else:
            return self._prop.data

    async def as_dict(self, glossary_service):
        return copy.deepcopy(await self._prop.as_dict(glossary_service))

    def get_term(self) -> Optional[Term]:
        return self._prop.term

    def __repr__(self):
        return f"PropView(${self._prop.data})"
