"Calculation data class."
import gzip
import json
from dataclasses import dataclass, field
from enum import Enum
from typing import List, Optional, Union
from uuid import UUID

from pydantic import BaseModel, ConfigDict, Field

from api.dto.v2.access_group_dto import LegacyAPIResponseBehavior
from core.domain.data_error import DataError
from core.domain.final_graph_criterion import FinalGraphTableCriterion
from core.domain.nodes.root_with_subflows_dto import ExistingRootDto, RootWithSubFlowsDto
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.quantity_prop import RawQuantity, ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop import ConvertibleUnits
from core.graph_manager.mutations import mutation_from_dict
from core.graph_manager.mutations.abstract_mutation import AbstractMutation

CALCULATION_DATA_INPUT_FIELDS = [
    "request_id",
    "namespace_uid",
    "skip_calc",
    "save_result",
    "input_root",
    "save_as_system_process",
    "fixed_depth_for_calculating_supply",
    "explicitly_attach_cached_elementary_resource_emission",
    "gfms_for_which_to_save_most_shallow_nodes",
    "gfms_to_skip",
    "use_system_processes",
    "gfms_for_which_to_load_all_relevant_nodes",
    "exclude_node_props",
    "transient",
    "return_log",
    "return_data_as_table",
    "return_final_graph",
    "return_final_root",
    "legacyAPI_response_behavior",
    "max_depth_of_returned_graph",
    "max_depth_of_returned_table",
    "final_graph_table_criterion",
    "requested_quantity_references",
    "requested_units",
    "requested_impact_assessments",
    "cache_final_root",
    "use_cached_final_root",
]
CALCULATION_DATA_RESULT_FIELDS = [
    "global_gfm_state",
    "success",
    "statuscode",
    "message",
    "data_errors",
    "final_graph",
    "final_graph_table",
    "final_root",
]


@dataclass
class LinkedSubNodeInfo:
    uid: UUID
    flow_amount: RawQuantity


class RequestedMassUnitEnum(str, Enum):
    microgram = "microgram"
    milligram = "milligram"
    gram = "gram"
    kilogram = "kilogram"
    ton = "ton"


class RequestedTimeUnitEnum(str, Enum):
    year = "year"
    day = "day"
    hour = "hour"
    minute = "minute"
    second = "second"


class RequestedElectricCurrentUnitEnum(str, Enum):
    watt_hour = "watt-hour"
    kilowatt_hour = "kilowatt-hour"


class RequestedLengthUnitEnum(str, Enum):
    millimeter = "millimeter"
    centimeter = "centimeter"
    decimeter = "decimeter"
    meter = "meter"
    kilometer = "kilometer"


class RequestedEnergyUnitEnum(str, Enum):
    kilojoule = "kilojoule"
    kilocalorie = "kilocalorie"


class Calculation(BaseModel):
    """Calculation data class.

    Attributes:
        uid (Optional[UUID]): The uid of the calculation.
        root_node_uid (Optional[UUID]): The uid of the root node of the calculation.
        linked_sub_node_infos (Optional[List[LinkedSubNodeInfo]]):
            The set of uid and flow amount for which to run calculation.
            If defined, a dummy root flow and a dummy child-of-root activity are added as parents.
        skip_calc (Optional[bool]): Whether to skip the calculation.
        save_result (Optional[bool]): Whether to save the result of the calculation in the database.
        input_root (Optional[Union[RootWithSubFlowsDto, ExistingRootDto]]): The root activity of the
            calculation.
        save_as_system_process (bool): Whether to save nodes as system processes.
        fixed_depth_for_calculating_supply (int): The depth for which the supply is calculated.
            If save_as_system_process, this will be the depth for which nodes are stored as system processes.
        gfms_for_which_to_save_most_shallow_nodes (list[str]): The GFMs for which to save the most shallow nodes.
        gfms_to_skip (list[str]): The GFMs to skip.
        use_system_processes (bool): Whether to use system processes.
        gfms_for_which_to_load_all_relevant_nodes (list[str]): Only use system-processes for nodes
            where no node below can get modified by GFMs contained in this list.
        exclude_node_props (list[str]): The node props to exclude.
        return_log (bool): Whether to return the mutation log.
        return_data_as_table (bool): Whether to return the data as a table.
        return_final_graph (bool): Whether to return the final graph.
        return_final_root (bool): Whether to return the final root activity with subflows.
        cache_final_root (bool): Whether to cache the final root flow and activity post-calculation.
        use_cached_final_root (bool): Whether to use the cached final root in add_client_nodes
            (It is currently not used for sub-recipes as we do not have correct cache invalidation).
        legacyAPI_response_behavior (Optional[LegacyAPIResponseBehavior]): The behavior of the legacy API response.
        max_depth_of_returned_graph (int): Max-depth of the returned final graph.
        max_depth_of_returned_table (int): Max-depth of the returned tree in table format.
        final_graph_table_criterion (Optional[FinalGraphTableCriterion]): The criterion to determine which rows to keep
            in the final table.
        explicitly_attach_cached_elementary_resource_emission (bool): Whether to explicitly attach
            ElementaryResourceEmissionNode in aggregated_cache in inventory_connector.
        requested_quantity_references (Optional[list[ReferenceAmountEnum]]): List holding quantity references
            (e.g., amount_for_100g, amount_for_activity_production_amount) requested by the client.
        requested_units (Optional[str]): If given, quantities will be deserialized according to these units.
        requested_impact_assessments (Optional[list[str]]): List holding impact_assessments (e.g., IPCC 2013 GWP 100a)
        requested by the client.
        mutations (Optional[list[AbstractMutation]]): Can contain the mutation log of the calculation.
        success (bool): Whether the calculation was successful.
        statuscode (int): Statuscode of the calculation.
        message (str): Message of the calculation.
        data_errors (Optional[list[DataError]]): Contains data_errors that occurred during the calculation.
        final_graph: Optional[List[dict]] = Final graph of the calculation.
        final_graph_table: Optional[List[dict]] = Final graph of the calculation as a table.
    """

    uid: Optional[UUID] = None
    child_of_root_node_uid: Optional[UUID] = None
    linked_sub_node_infos: Optional[List[LinkedSubNodeInfo]] = None
    root_node_uid: Optional[UUID] = None
    request_id: Optional[int] = None
    namespace_uid: Optional[str] = None
    transient: bool = Field(default=False)

    # global GFM state (to give access to this property to all nodes).
    global_gfm_state: Optional[GfmStateProp] = None

    # data_input fields:
    skip_calc: Optional[bool] = False
    save_result: Optional[bool] = False
    input_root: Optional[Union[RootWithSubFlowsDto, ExistingRootDto]] = Field(default=None)
    # parameters for saving system processes:
    save_as_system_process: bool = False
    fixed_depth_for_calculating_supply: int = 1  # Increased from 0 to 1 so that the root flow node is accounted for.
    gfms_for_which_to_save_most_shallow_nodes: list[str] = field(default_factory=lambda: [])
    gfms_to_skip: list[str] = field(default_factory=lambda: [])
    # parameters for using system processes:
    use_system_processes: bool = True
    gfms_for_which_to_load_all_relevant_nodes: list[str] = field(default_factory=lambda: [])
    exclude_node_props: list[str] = field(default_factory=lambda: ["environmental_flows"])
    explicitly_attach_cached_elementary_resource_emission: bool = Field(default=False)
    # parameter for caching post-calculation nodes:
    cache_final_root: bool = False
    # parameter for using cached post-calculation nodes:
    use_cached_final_root: bool = False
    # parameter for configuring:
    legacyAPI_response_behavior: Optional[LegacyAPIResponseBehavior] = Field(default=None)
    # parameters for returning data:
    return_log: bool = False
    return_data_as_table: bool = False
    return_final_graph: bool = False
    return_final_root: bool = True
    max_depth_of_returned_graph: Optional[int] = None
    max_depth_of_returned_table: Optional[int] = None
    final_graph_table_criterion: Optional[FinalGraphTableCriterion] = None
    requested_quantity_references: Optional[list[ReferenceAmountEnum]] = field(default_factory=lambda: [])
    requested_units: Optional[
        dict[
            ConvertibleUnits,
            RequestedMassUnitEnum
            | RequestedTimeUnitEnum
            | RequestedLengthUnitEnum
            | RequestedElectricCurrentUnitEnum
            | RequestedEnergyUnitEnum,
        ]
    ] = field(default=None)
    requested_impact_assessments: Optional[list[str]] = field(default_factory=lambda: [])

    # data_mutations, only contains the mutation log:
    mutations: Optional[list[AbstractMutation]] = field(default_factory=lambda: [])

    # data_result fields:
    success: bool = True
    statuscode: Optional[int] = Field(default=None)
    message: Optional[str] = Field(default=None)
    data_errors: Optional[list[DataError]] = field(default_factory=lambda: [])
    final_graph: Optional[list[dict]] = Field(None, alias="final_graph")
    final_graph_table: Optional[list[dict]] = Field(None, alias="final_graph_table")
    final_root: Optional[RootWithSubFlowsDto] = Field(default=None)

    retry_count: int = Field(default=0)

    model_config = ConfigDict(extra="forbid", validate_assignment=True)

    def get_compressed_input(self) -> bytes:
        return self.get_compressed_fieldset(CALCULATION_DATA_INPUT_FIELDS)

    def get_compressed_result(self) -> bytes:
        return self.get_compressed_fieldset(CALCULATION_DATA_RESULT_FIELDS)

    def get_compressed_mutations(self) -> bytes:
        return self.get_compressed_fieldset(["mutations"])

    def get_compressed_fieldset(self, fields: list[str]) -> bytes:
        """Get the compressed mutation log of the calculation."""
        json_data = self.model_dump_json(by_alias=False, include=fields)
        compressed_data = gzip.compress(json_data.encode("utf-8"), compresslevel=3)
        return compressed_data

    def set_fields_from_compressed_data(self, compressed_data: bytes) -> None:
        """Load the compressed input of the calculation."""
        json_str = gzip.decompress(compressed_data).decode("utf-8")
        json_data = json.loads(json_str)
        for key, value in json_data.items():
            setattr(self, key, value)

    def set_mutations_from_compressed_data(self, compressed_data: bytes) -> None:
        json_str = gzip.decompress(compressed_data).decode("utf-8")
        json_data = json.loads(json_str)
        if "mutations" in json_data:
            self.mutations = [mutation_from_dict(mutation) for mutation in json_data["mutations"]]
        else:
            self.mutations = []
