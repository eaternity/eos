from dataclasses import dataclass

from pydantic import BaseModel


@dataclass
class Token:
    name: str
    token: str
    user_id: str


class TokenDto(BaseModel):
    name: str


class TokenOutputDto(BaseModel):
    name: str
    token: str


class TokenListingOutputDto(BaseModel):
    tokens: list[TokenOutputDto]
