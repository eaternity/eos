from dataclasses import dataclass
from enum import Enum
from typing import Optional


# FIXME: The right-hand-side values of the enum are currently inserted as json keys into the database. We should
#  change these values to be more database-friendly or change how permissions are serialized.
class UserPermissionsEnum(str, Enum):
    add_users_to_group = "add_users_to_group"
    create_access_group = "create_access_group"
    # "admin actions" == "create" and "delete"
    child_access_group_delete = "child_access_group_delete"
    child_access_group_post = "child_access_group_post"
    child_access_group_read = "child_access_group_read"
    child_access_group_update = "child_access_group_update"
    delete = "delete"
    delete_access_group = "delete_access_group"
    delete_users_from_group = "delete_users_from_group"
    give_permissions_in_access_group = "give_permissions_in_access_group"
    give_permissions_in_child_access_group = "give_permissions_in_child_access_group"
    is_group_admin = "is_group_admin"
    post = "post"
    read = "read"
    update = "update"
    view_access_group_members = "view_access_group_members"
    create_terms = "create_terms"
    edit_terms = "edit_terms"

    def __str__(self):
        string_repr_dict = {
            UserPermissionsEnum.add_users_to_group: "Add users to an access group",
            UserPermissionsEnum.create_access_group: "Create a new access group",
            # "admin actions" == "create" and "delete"
            UserPermissionsEnum.child_access_group_delete: "Delete nodes inside any child access group",
            UserPermissionsEnum.child_access_group_post: "Post nodes inside any child access group",
            UserPermissionsEnum.child_access_group_read: "Read nodes inside any child access group",
            UserPermissionsEnum.child_access_group_update: "Update nodes inside any child access group",
            UserPermissionsEnum.delete: "Delete group's nodes",
            UserPermissionsEnum.delete_access_group: "Delete an access group",
            UserPermissionsEnum.delete_users_from_group: "Delete users from an access group",
            UserPermissionsEnum.give_permissions_in_access_group: "Give permissions to users in an access group",
            UserPermissionsEnum.give_permissions_in_child_access_group: "Give permissions to users"
            " in a child access group",
            UserPermissionsEnum.is_group_admin: "Perform admin actions inside an access group",
            UserPermissionsEnum.post: "Create new nodes",
            UserPermissionsEnum.read: "Read group's nodes",
            UserPermissionsEnum.update: "Update group's nodes",
            UserPermissionsEnum.view_access_group_members: "View a list of access group members",
            UserPermissionsEnum.create_terms: "Create glossary terms",
            UserPermissionsEnum.edit_terms: "Edit glossary terms",
        }
        return string_repr_dict[self]

    def __repr__(self):
        return self.value


UPE = UserPermissionsEnum
PERMISSION_INHERITANCE_MAP = {
    UPE.child_access_group_delete.value: UPE.delete.value,
    UPE.child_access_group_post.value: UPE.post.value,
    UPE.child_access_group_read.value: UPE.read.value,
    UPE.child_access_group_update.value: UPE.update.value,
    UPE.give_permissions_in_child_access_group.value: UPE.give_permissions_in_access_group.value,
    UPE.is_group_admin.value: UPE.is_group_admin.value,
}


class UserGroupRoleEnum(list[UserPermissionsEnum], Enum):
    # TODO: add more roles in the future
    ADMIN = [
        UserPermissionsEnum.add_users_to_group,
        UserPermissionsEnum.child_access_group_delete,
        UserPermissionsEnum.child_access_group_post,
        UserPermissionsEnum.child_access_group_read,
        UserPermissionsEnum.child_access_group_update,
        UserPermissionsEnum.create_access_group,
        UserPermissionsEnum.delete,
        UserPermissionsEnum.delete_access_group,
        UserPermissionsEnum.delete_users_from_group,
        UserPermissionsEnum.give_permissions_in_access_group,
        UserPermissionsEnum.give_permissions_in_child_access_group,
        UserPermissionsEnum.is_group_admin,
        UserPermissionsEnum.post,
        UserPermissionsEnum.read,
        UserPermissionsEnum.update,
        UserPermissionsEnum.view_access_group_members,
    ]
    BASIC_USER = []
    SUPERUSER = [permission for permission in UserPermissionsEnum]


@dataclass
class User:
    email: Optional[str]
    # TODO: remove this field once we have authentication via Cognito implemented
    password: Optional[str]
    is_superuser: bool = False
    is_service_account: bool = False
    user_id: Optional[str] = None
    legacy_api_default_access_group_uid: Optional[str] = None


@dataclass
class UserPermissions:
    user_id: str
    permissions: dict[UserPermissionsEnum, int]
