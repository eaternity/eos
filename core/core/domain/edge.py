import uuid
from dataclasses import dataclass, field
from enum import Enum
from typing import Optional


class EdgeTypeEnum(str, Enum):
    root_to_recipe = "root_to_recipe"
    ingredient = "ingredient"
    link_to_subrecipe = "link_to_subrecipe"
    lca_link = "lca_link"
    """ A link between LCA activities and flows (nota: Brightway flows/exchanges are modelled as EOS Nodes) """


@dataclass(slots=True)
class Edge:
    parent_uid: uuid
    child_uid: uuid
    edge_type: EdgeTypeEnum = field(default=EdgeTypeEnum.ingredient)

    def __post_init__(self) -> None:
        if self.child_uid == self.parent_uid:
            raise ValueError("edge child and parent are the same. this is not allowed.")

    def __repr__(self) -> str:
        return f"Edge(parent_uid=${self.parent_uid}, child_uid=${self.child_uid}, edge_type=${self.edge_type})"

    def serialize(self) -> dict:
        return {
            "cls": self.__class__.__name__,
            "p": str(self.parent_uid),
            "c": str(self.child_uid),
            "e": self.edge_type.value,
        }

    @classmethod
    def deserialize(cls: type["Edge"], obj: object) -> object:
        if "cls" in obj and obj["cls"] == cls.__name__:
            return Edge(
                parent_uid=uuid.UUID(obj["p"]),
                child_uid=uuid.UUID(obj["c"]),
                edge_type=EdgeTypeEnum(obj["e"]),
            )
        else:
            return obj


@dataclass(frozen=True)
class XidAccessGroupUid:
    xid: str
    access_group_uid: uuid.UUID


@dataclass
class EdgeToUpsert:
    parent_uid: uuid.UUID
    child_uid: Optional[uuid.UUID] = None
    child_xid_access_group_uid: Optional[XidAccessGroupUid] = None
