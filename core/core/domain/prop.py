import dataclasses
from typing import TYPE_CHECKING, Any, Dict, Literal, Optional, Set, Type, Union

from pydantic import ConfigDict, Field, model_serializer
from pydantic.dataclasses import dataclass
from pydantic.fields import FieldInfo
from pydantic_core import PydanticUndefined

from core.domain.deep_mapping_view import DeepListView, DeepMappingView, DeepSetView
from core.envprops import EnvProps

if TYPE_CHECKING:
    from .nodes.node import Node


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True)
class Prop:
    prop_type: Literal["Prop"] = Field(default="Prop")

    # If the request payload contains raw_data, it is stored in source_data_raw.
    source_data_raw: Optional[Union[str, dict]] = Field(default=None)
    _owner_node: Optional[Any] = dataclasses.field(
        default=None,
        repr=False,
        init=False,
        compare=False,
    )  # Type checking cannot be activated due to circular dependence.

    if EnvProps().VERIFY_NODE_READ_ONLY:
        # Mark Prop as read only to prevent writing directly into prop. Writing into a prop is prohibited if
        # either _owner_node is set or _read_only is set to true. _read_only flag is useful to mark
        # Props that are nested in another prop as being read-only.
        _read_only: bool = Field(default=False)

    @model_serializer(mode="plain")
    def wrapper_ser_model(self) -> Dict[str, Any]:
        return self.model_dump()

    def model_dump(self, exclude: Optional[set] = None) -> dict:
        dump_dict = self.model_dump_base(exclude=exclude)
        # Warning: private attributes of Deep*View should never be accessed, but for efficiency,
        # we access it only during serialization.
        for key, val in dump_dict.items():
            if isinstance(val, DeepMappingView):
                dump_dict[key] = val._data
            elif isinstance(val, DeepListView):
                dump_dict[key] = val._data
            elif isinstance(val, DeepSetView):
                dump_dict[key] = val._set
        return dump_dict

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        if hasattr(self, "__dict__"):
            dump_dict = {key: getattr(self, key) for key in self.__dict__ if not key.startswith("_")}
        elif hasattr(self, "__slots__"):
            dump_dict = {slot: getattr(self, slot) for slot in self.__slots__ if not slot.startswith("_")}
        else:
            raise TypeError(f"{type(self).__name__} cannot be converted to a dictionary.")
        dump_dict["prop_type"] = self.__class__.__name__  # Needed as __slots__ contains no parent "slots"
        if self.source_data_raw:
            dump_dict["source_data_raw"] = self.source_data_raw  # Needed as __slots__ contains no parent "slots"
        if hasattr(self, "model_dump_extra"):
            self.model_dump_extra(dump_dict)

        if exclude:
            for field in exclude:
                if field in dump_dict:
                    del dump_dict[field]
        return dump_dict

    @classmethod
    def get_subclasses(cls: Type["Prop"]) -> tuple:
        immediate_subclasses = [
            subclass for subclass in cls.__subclasses__() if type(subclass.prop_type) is not FieldInfo
        ]
        all_subclasses = []
        for subclass in immediate_subclasses:
            all_subclasses.append(subclass)
            all_subclasses.extend(subclass.get_subclasses())
        return tuple(all_subclasses)

    def set_owner_node(self, owner_node: "Node") -> None:
        self._owner_node = owner_node

    def unset_owner_node(self) -> None:
        self._owner_node = None

    def get_owner_node(self) -> "Node":
        return self._owner_node

    def from_dict(self, data: dict) -> "Node":
        pass

    @property
    def _props_to_add_to_repr(self) -> Dict[str, Any]:
        """Returns a dict of properties to be added to __repr__."""
        return {}

    @property
    def _props_to_remove_from_repr(self) -> Set[str]:
        """Returns a set of properties to be excluded from __repr__."""
        return {"_owner_node", "prop_type"}

    def __repr__(self) -> str:
        """Representation of the property."""
        model_dump = self.model_dump(exclude=self._props_to_remove_from_repr)
        model_dump.update(self._props_to_add_to_repr)
        return f"{self.__class__.__name__}({model_dump})"

    def model_copy(self) -> "Prop":
        return self.duplicate()

    @classmethod
    def unvalidated_construct(cls: Type["Prop"], **kwargs: object) -> "Prop":
        """Only to be used for efficiency reasons. Currently used only in aggregation GFM."""
        self = cls.__new__(cls)
        dataclass_fields = self.__dataclass_fields__
        set_attr = object.__setattr__

        for field, field_info in dataclass_fields.items():
            if field == "prop_type":
                set_attr(self, field, cls.__name__)
                continue

            if field in kwargs:
                set_attr(self, field, kwargs[field])
                continue

            if field in ("_owner_node", "source_data_raw"):
                set_attr(self, field, None)
                continue

            default = field_info.default
            if default.default is not None and default.default != PydanticUndefined:
                value = default.get_default()
            elif (default_factory := field_info.default.default_factory) is not None:
                value = default_factory()
            elif not field_info.default.is_required() and default.default is None:
                value = None
            else:
                if field_info.default.is_required():
                    raise AttributeError(f"{field} is a required field of {cls.__name__}")
                value = None
            set_attr(self, field, value)

        return self

    def duplicate(self) -> "Prop":
        """Workaround for deepcopying the property.

        Pydantic v2 does not allow to exclude attributes when deepcopying.
        """

        def recursive_duplication(orig: object) -> object:
            if isinstance(orig, Prop):  # To avoid error messages in Pycharm, install Pydantic plugin.
                return orig.duplicate()
            elif isinstance(orig, (list, DeepListView)) and all(
                isinstance(item, (str, int, float, bool)) for item in orig
            ):
                return list(orig).copy()
            elif isinstance(orig, (dict, DeepMappingView)) and all(
                isinstance(item, (str, int, float, bool)) for item in orig.values()
            ):
                return dict(orig).copy()
            elif isinstance(orig, (list, DeepListView)):
                return [recursive_duplication(item) for item in orig]
            elif isinstance(orig, (dict, DeepMappingView)):
                return {key: recursive_duplication(val) for key, val in orig.items()}
            else:
                return orig

        keyword_arguments = {}

        for field in self.fields_to_duplicate():
            if field not in ("_owner_node", "_read_only"):
                field_value = getattr(self, field)
                keyword_arguments[field] = recursive_duplication(field_value)
        return self.__class__.unvalidated_construct(**keyword_arguments)

    def fields_to_duplicate(self) -> "Prop":
        if hasattr(self, "__dataclass_fields__"):
            return self.__dataclass_fields__.keys()
        else:
            raise AttributeError(f"__dataclass_fields__ missing in {self.__class__.__name__}")

    # For development, we check if nodes are read-only.
    if EnvProps().VERIFY_NODE_READ_ONLY:

        def __post_init__(self):
            # Protect all nested Props with _read_only flag.
            if hasattr(self, "__dataclass_fields__"):
                for field, _field_info in self.__dataclass_fields__.items():
                    field_value = getattr(self, field)
                    if isinstance(field_value, (DeepListView, list)) and all(
                        isinstance(elem, Prop) for elem in field_value
                    ):
                        for elem in field_value:
                            object.__setattr__(elem, "_read_only", True)
                    elif isinstance(field_value, (DeepMappingView, dict)) and all(
                        isinstance(elem, Prop) for elem in field_value.values()
                    ):
                        for elem in field_value.values():
                            object.__setattr__(elem, "_read_only", True)
                    elif isinstance(field_value, Prop):
                        object.__setattr__(field_value, "_read_only", True)
            else:
                raise AttributeError(f"__dataclass_fields__ missing in {self.__class__.__name__}")

        def make_read_only(self) -> None:
            self._read_only = True

        def __getattribute__(self, item: str):
            """Wrapper to avoid modification of prop attributes."""
            attr_val = object.__getattribute__(self, item)

            attr_val_type = type(attr_val)
            if attr_val_type is list:
                return DeepListView(attr_val)
            if attr_val_type is dict:
                # Only return fields in DeepMappingView.
                if item not in object.__getattribute__(self, "__dataclass_fields__"):
                    return attr_val
                return DeepMappingView(attr_val)
            if attr_val_type is set:
                return DeepSetView(attr_val)
            return attr_val

        def __setattr__(self, name: str, value: object):
            if name not in (
                "_read_only",
                "_owner_node",
            ):  # Allow post initialization modification if _owner_node is being set.
                if getattr(self, "_read_only", False) or getattr(self, "_owner_node", None):
                    raise AttributeError(f"Cannot modify attributes of {self.__class__.__name__}")
            object.__setattr__(self, name, value)

    else:

        def make_read_only(self) -> None:
            return
