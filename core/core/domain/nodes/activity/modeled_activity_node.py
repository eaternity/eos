from typing import Any, Literal, Optional

from pydantic import Field
from structlog import get_logger

from core.domain.nodes.activity_node import ActivityNode
from core.domain.props.environmental_flows_prop import EnvironmentalFlowsProp

logger = get_logger()


class ModeledActivityNode(ActivityNode):
    node_type: Literal["ModeledActivityNode"] = Field(default="ModeledActivityNode")
    aggregated_cache: Optional[EnvironmentalFlowsProp] = Field(default=None)

    def model_copy(self, *, update: dict[str, Any] | None = None, deep: bool = False) -> "ModeledActivityNode":
        # temporarily make aggregated cache writable, and unset owner nodes for all props.
        if deep:
            if self.aggregated_cache:
                orig_read_only_state = self.aggregated_cache._read_only
                self.aggregated_cache._read_only = False
            self.unset_owner_node_for_props()

        copied_node = super().model_copy(update=update, deep=deep)

        # Reset read only and owner nodes.
        if deep:
            if self.aggregated_cache:
                self.aggregated_cache._read_only = orig_read_only_state
                copied_node.aggregated_cache._read_only = orig_read_only_state

            self.set_owner_node_for_props()
            copied_node.set_owner_node_for_props()

        return copied_node
