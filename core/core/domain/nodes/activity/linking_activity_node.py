from typing import Literal

from pydantic import Field
from structlog import get_logger

from core.domain.nodes.activity_node import ActivityNode

logger = get_logger()


class LinkingActivityNode(ActivityNode):
    node_type: Literal["LinkingActivityNode"] = Field(default="LinkingActivityNode")
