from typing import Type

from core.domain.nodes.activity.elementary_resource_emission_node import ElementaryResourceEmissionNode
from core.domain.nodes.activity.food_processing_activity_node import FoodProcessingActivityNode
from core.domain.nodes.activity.linking_activity_node import LinkingActivityNode
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.nodes.activity.supply_sheet_activity import SupplySheetActivityNode
from core.domain.nodes.activity.transport_activity_node import TransportActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.flow.practice_flow_node import PracticeFlowNode
from core.domain.nodes.flow_node import FlowNode


def node_class_from_type(node_type: str) -> Type:
    # TODO: this might be better implemented dynamically like in the props module...
    match node_type:
        case ElementaryResourceEmissionNode.__name__:
            return ElementaryResourceEmissionNode
        case ModeledActivityNode.__name__:
            return ModeledActivityNode
        case FoodProductFlowNode.__name__:
            return FoodProductFlowNode
        case PracticeFlowNode.__name__:
            return PracticeFlowNode
        case FoodProcessingActivityNode.__name__:
            return FoodProcessingActivityNode
        case LinkingActivityNode.__name__:
            return LinkingActivityNode
        case TransportActivityNode.__name__:
            return TransportActivityNode
        case SupplySheetActivityNode.__name__:
            return SupplySheetActivityNode
        case FlowNode.__name__:
            return FlowNode
        case ActivityNode.__name__:
            return ActivityNode
