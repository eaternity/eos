from typing import Literal, Optional, Tuple, Union

from pydantic import Field
from structlog import get_logger

from core.domain.nodes.node import Node
from core.domain.props import EnvironmentalFlowsProp, LocationProp, QuantityProp
from core.domain.props.quantity_prop import RawQuantity

logger = get_logger()


class ActivityNode(Node):
    node_type: Literal["ActivityNode"] = Field(default="ActivityNode")
    activity_location: Optional[Union[str, list[LocationProp]]] = Field(default=None)
    activity_date: Optional[str] = Field(default=None)
    production_amount: Optional[Union[RawQuantity, QuantityProp]] = Field(default=None)
    impact_assessment_supply_for_root: Optional[QuantityProp] = Field(default=None)
    environmental_flows: Optional[EnvironmentalFlowsProp] = Field(default=None)

    def add_sub_flow(self, sub_flow: "Node") -> None:
        self.sub_flows.append(sub_flow)

    @property
    def _location_and_its_source(self) -> Tuple[Optional[str], Optional[str]]:
        """Method that extracts a location from the node.

        returns: Tuple(source_of_location, location)
        """
        return self._location_as_string(self.activity_location)

    @property
    def _amount_and_its_source(self) -> Tuple[Optional[str], Optional[str]]:
        """Method that tries to extract an amount from the node.

        This method can be overridden by subclasses.

        returns: Tuple(source_of_amount, amount)
        """
        amount_key = None
        amount_value = None
        if self.production_amount:
            amount_value = self.production_amount.short_string_representation()
            if isinstance(self.production_amount, QuantityProp):
                amount_key = f"production_amount [{self.production_amount.for_reference.removeprefix('amount_')}]"
            else:  # it's a RawQuantity
                amount_key = "production_amount [raw_quantity]"
        if amount_key and amount_value:
            return amount_key, amount_value
        else:
            return super()._amount_and_its_source
