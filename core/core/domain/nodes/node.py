"Node Baseclass declaration."
import copy
from dataclasses import dataclass
from functools import lru_cache
from typing import TYPE_CHECKING, Any, Callable, Dict, List, Literal, Optional, Tuple, Type, Union
from uuid import UUID

from pydantic import BaseModel, ConfigDict, Field, PrivateAttr, ValidationInfo, field_validator
from structlog import get_logger

from core.domain.deep_mapping_view import DeepListView, DeepMappingView
from core.domain.prop import Prop
from core.domain.props import (
    GfmStateProp,
    GlossaryTermProp,
    LegacyAPIInformationProp,
    LocationProp,
    QuantityProp,
    ReferencelessQuantityProp,
    ScarceWaterProp,
)
from core.domain.props.impact_assessment_prop import ImpactAssessmentProp
from core.domain.props.json_prop import JsonProp
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.props.quantity_prop import RawQuantity, ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop import UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING
from core.domain.props.required_matching_prop import RequiredMatchingProp
from core.domain.props.utils import ignore_none
from core.domain.term import Term
from core.envprops import EnvProps

if TYPE_CHECKING:
    from core.domain.calculation import Calculation

logger = get_logger()


class Node(BaseModel):
    # meta-data:
    namespace_uid: Optional[UUID] = Field(default=None)
    access_group_xid: Optional[str] = Field(default=None)
    access_group_uid: Optional[UUID] = Field(default=None)
    api_version: int = Field(default=1)
    xid: Optional[str] = Field(default=None)
    uid: Optional[UUID] = Field(default=None)
    node_type: Literal["Node"] = Field(default="Node")
    is_persistent: Optional[bool] = Field(default=False)
    added_by: Optional[str] = Field(default=None)

    # node properties:
    raw_input: Optional[JsonProp] = Field(default=None)
    legacy_API_information: Optional[LegacyAPIInformationProp] = Field(default=None)
    required_matching: Optional[list[RequiredMatchingProp]] = Field(default=None)
    gfm_state: Optional[GfmStateProp] = Field(default=None)
    global_gfm_state: Optional[GfmStateProp] = Field(default=None)
    impact_assessment: Optional[ImpactAssessmentProp] = Field(default=None)
    scarce_water_consumption: Optional[ScarceWaterProp] = Field(default=None)
    original_node_uid: Optional[LinkToUidProp] = Field(default=None)

    # some private attributes to keep track of the graph:
    _parent_nodes: [] = PrivateAttr(default_factory=list)
    _sub_nodes: [] = PrivateAttr(default_factory=list)
    _calculation: Optional["Calculation"] = PrivateAttr(default_factory=None)

    def __init__(self, **kwargs: Any):  # noqa: ANN401
        if "node_type" in kwargs and kwargs["node_type"] != type(self).__name__:
            raise ValueError("node_type does not match class name.")

        raw_input = copy.deepcopy(kwargs.get("raw_input", {}))
        if raw_input is None:
            raw_input = {}

        kwargs_keys_to_delete = []  # delete after loop to avoid potential side effects
        for kwargs_key, kwargs_val in kwargs.items():
            if kwargs_key not in self.model_fields:
                # Commenting out for performance.
                # logger.warning(
                #     f"Field {kwargs_key} is not defined in model {type(self).__name__}, "
                #     f"moving it into raw_input field."
                # )
                raw_input[kwargs_key] = kwargs_val
                kwargs_keys_to_delete.append(kwargs_key)
            elif "api_version" in kwargs:
                model_field = self.model_fields[kwargs_key]
                field_api_version = 1
                if model_field.json_schema_extra is not None:
                    print(model_field.json_schema_extra)
                    field_api_version = model_field.json_schema_extra.get("api_version", 1)
                if kwargs["api_version"] < field_api_version:
                    # this means that the data was created for an older API version, where this field did not exist:
                    logger.warning(
                        f"Moving field {kwargs_key} into raw_input field, "
                        f"because data was defined for a previous api version."
                    )
                    raw_input[kwargs_key] = kwargs_val
                    kwargs_keys_to_delete.append(kwargs_key)

        for kwargs_key in kwargs_keys_to_delete:
            del kwargs[kwargs_key]

        if len(raw_input) > 0:
            kwargs["raw_input"] = raw_input

        super().__init__(**kwargs)
        self.set_owner_node_for_props()

        self._parent_nodes = []
        self._sub_nodes = []
        self._calculation = None

    @classmethod
    @lru_cache(maxsize=None)
    def subclasses_with_self(cls: Type["Node"]) -> Tuple[Type["Node"], ...]:
        subclasses = [cls]
        for subclass in cls.__subclasses__():
            subclasses.extend(subclass.subclasses_with_self())
        return tuple(subclasses)

    @classmethod
    @lru_cache(maxsize=None)
    def subclasses_with_self_as_dict(cls: Type["Node"]) -> Dict[Type["Node"], ...]:
        subclasses = {cls.__name__: cls}
        for subclass in cls.__subclasses__():
            subclasses.update(subclass.subclasses_with_self_as_dict())
        return subclasses

    @field_validator("*")
    def validate_no_referenceless_quantity_for_all_fields(
        cls, v: Optional[QuantityProp], info: ValidationInfo
    ) -> Optional[QuantityProp]:
        if v is not None and (type(v) is ReferencelessQuantityProp):
            raise ValueError(f"{info.field_name} cannot be a ReferencelessQuantityProp")
        return v

    def set_owner_node_for_props(self) -> None:
        for field_name in self.model_fields_set:
            self._set_owner_node_for_single_prop(field_name)

    def _set_owner_node_for_single_prop(self, field_name: str) -> None:
        if field_name != "aggregated_cache":
            attr = getattr(self, field_name)
            if isinstance(attr, Prop):
                attr.set_owner_node(self)
            if isinstance(attr, (list, DeepListView)) and all(isinstance(x, Prop) for x in attr):
                for x in attr:
                    x.set_owner_node(self)

    def unset_owner_node_for_props(self) -> None:
        for field_name in self.model_fields_set:
            self._unset_owner_node_for_single_prop(field_name)

    def _unset_owner_node_for_single_prop(self, field_name: str) -> None:
        if field_name != "aggregated_cache":
            attr = getattr(self, field_name)
            if isinstance(attr, Prop):
                attr.unset_owner_node()
            if isinstance(attr, (list, DeepListView)) and all(isinstance(x, Prop) for x in attr):
                for x in attr:
                    x.unset_owner_node()

    def __setattr__(self, name: str, value: Any) -> None:  # noqa: ANN401
        if name in {"_calculation", "_parent_nodes", "_sub_nodes", "xid", "uid", "added_by"}:
            object.__setattr__(self, name, value)
            return

        if getattr(self, "_calculation", None) is not None:
            raise ValueError(
                "Cannot modify node after it has been added to a calculation graph. " "Use PropMutation instead."
            )

        self.__apply_mutation__(name, value)

    def ignore_fields(self, fields_to_be_ignored: Optional[list[str]] = None) -> None:
        """Ignore fields according to the client's access group configuration."""
        if not fields_to_be_ignored:
            return
        # probably ignoring meta-data fields would mess up the calculation
        fields_to_never_ignore = frozenset(
            {
                "xid",
                "uid",
                "access_group_xid",
                "access_group_uid",
                "node_type",
                "is_persistent",
                "added_by",
            }
        )
        for field_to_be_ignored in fields_to_be_ignored:
            if hasattr(self, field_to_be_ignored) and field_to_be_ignored not in fields_to_never_ignore:
                default_value = self.model_fields[field_to_be_ignored].default
                setattr(self, field_to_be_ignored, default_value)

    def get_prop_by_inheritance(
        self,
        prop_name: str,
        ignore_if: Callable[[Union[str, dict, DeepMappingView, list, DeepListView, Prop]], bool] = ignore_none,
    ) -> Tuple[Optional["Prop"] | Optional[frozenset], Optional["Node"]]:
        """Get the prop by inheritance.

        :param prop_name: str, the name of the prop to get
        :param ignore_if: Optional[Callable[[Union[str, dict, DeepMappingView, list, DeepListView, Prop]], bool]],
            a function that takes the prop and returns True if it should be ignored. If the prop is ignored,
            the search continues to the next parent node.
        :return: Tuple[Optional[Prop], Optional[Node]], the prop and the node where the prop was found
        """

        def get_closest_node_with_prop(node: "Node") -> Optional["Node"]:
            ancestors_of_parents_with_prop = []
            for parent_node in node.get_parent_nodes():
                if (prop := getattr(parent_node, prop_name, None)) and not ignore_if(prop):
                    ancestors_of_parents_with_prop.append(parent_node)
                else:
                    ancestors_of_parents_with_prop.append(get_closest_node_with_prop(parent_node))

            if len(ancestors_of_parents_with_prop) == 0:
                # for cases where no parent nodes are found
                return None
            if len(ancestors_of_parents_with_prop) == 1:
                return ancestors_of_parents_with_prop[0]
            if all(elem is None for elem in ancestors_of_parents_with_prop):
                return ancestors_of_parents_with_prop[0]
            elif not any(elem is None for elem in ancestors_of_parents_with_prop):
                if all(
                    getattr(elem, prop_name) == getattr(ancestors_of_parents_with_prop[0], prop_name)
                    for elem in ancestors_of_parents_with_prop
                ):
                    return ancestors_of_parents_with_prop[0]
            raise TypeError(f"{node} has multiple parents with different {prop_name}.")

        if (prop := getattr(self, prop_name, None)) and not ignore_if(prop):
            closest_node_with_prop: "Node" = self
        else:
            closest_node_with_prop: "Node" = get_closest_node_with_prop(self)

        if closest_node_with_prop:
            return getattr(closest_node_with_prop, prop_name), closest_node_with_prop
        else:
            return None, None

    def __apply_mutation__(self, name: str, value: Prop | list[Prop]):
        """This method is used to apply a mutation to a node, which is used by the PropMutation class.

        This method should never be called directly, but only by the PropMutation class.
        :param name: str
        :param value: NodeProp
        :return:
        """
        # Commented out for performance.
        # if name not in self.model_fields:
        #     logger.warning(f"Setting extra field {name} on node")

        super().__setattr__(name, value)
        self._set_owner_node_for_single_prop(name)

    def add_parent_node(self, parent: "Node") -> None:
        from core.domain.nodes.activity_node import ActivityNode
        from core.domain.nodes.flow_node import FlowNode

        # needs alternating btw flow and activity
        if isinstance(self, FlowNode) and not isinstance(parent, ActivityNode):
            raise ValueError(f"Flow {self} can only have a activity as parent, not a flow {parent}")
        if isinstance(self, ActivityNode) and not isinstance(parent, FlowNode):
            raise ValueError(f"Activity {self} can only have a flow as parent, not an activity {parent}")

        if isinstance(self, FlowNode) and len(self._parent_nodes) == 1:
            raise ValueError(
                f"Trying to add parent node {parent} to flow node {self}. But flow node can only have "
                f"one parent, and it already has {self._parent_nodes}."
            )

        self._parent_nodes = list(self._parent_nodes) + [parent]

    def remove_parent_node(self, parent: "Node") -> None:
        tmp_parent_nodes = list(self._parent_nodes)
        tmp_parent_nodes.remove(parent)
        self._parent_nodes = tmp_parent_nodes

    def remove_private_attributes(self) -> None:
        self._sub_nodes = []
        self._parent_nodes = []
        self._calculation = None

    def add_sub_node(self, child: "Node") -> None:
        from core.domain.nodes.activity.linking_activity_node import LinkingActivityNode
        from core.domain.nodes.activity_node import ActivityNode
        from core.domain.nodes.flow_node import FlowNode

        # needs alternating btw flow and activity
        if isinstance(self, FlowNode) and not isinstance(child, ActivityNode):
            raise ValueError(f"Flow {self} can only have an activity as child, not a flow {child}")
        if isinstance(self, ActivityNode) and not isinstance(child, FlowNode):
            raise ValueError(f"Activity {self} can only have a flow as child, not an activity {child}")

        if isinstance(self, (FlowNode, LinkingActivityNode)) and len(self._sub_nodes) == 1:
            raise ValueError(
                f"Trying to add child node {child} to flow node {self}. But flow node can only have "
                f"one child, and it already has {self._sub_nodes}."
            )

        self._sub_nodes = list(self._sub_nodes) + [child]

    def remove_sub_node(self, child: "Node") -> None:
        tmp_sub_nodes = list(self._sub_nodes)
        tmp_sub_nodes.remove(child)
        self._sub_nodes = tmp_sub_nodes

    def set_calculation(self, calculation: "Calculation") -> None:
        self._calculation = calculation

    def get_calculation(self) -> "Calculation":
        return self._calculation

    @staticmethod
    def from_dict(data: dict) -> "Node":
        """Create a node from a dict."""
        # do the import here to avoid circular imports
        from core.domain.nodes import node_class_from_type

        # make a deepcopy of the data to avoid side effects:
        data = copy.deepcopy(data)
        # determine the class type
        node_class = node_class_from_type(node_type=data["node_type"])
        node = node_class(**data)
        return node

    def shallow_model_dump(self) -> dict:
        """Copy node with its attributes.

        If the attribute is a Prop, it has to be "copied" such that the _owner_node field can be set to duplicated node.
        """
        kwargs = {}
        for field_name in self.model_fields_set:
            attr = getattr(self, field_name)
            if isinstance(attr, Prop):
                kwargs[field_name] = copy.copy(attr)
            elif isinstance(attr, (list, DeepListView)) and all(isinstance(x, Prop) for x in attr):
                kwargs[field_name] = [copy.copy(x) for x in attr]
            elif isinstance(attr, (DeepListView, DeepMappingView)):
                # Warning: private attributes of Deep*View should never be accessed, but for efficiency,
                # we access it only during model copy of nodes.
                kwargs[field_name] = attr._data
            else:
                kwargs[field_name] = attr

        return kwargs

    @property
    def _name_and_its_source(self) -> Tuple[Optional[str], Optional[str]]:
        """Method that tries to extract a name from the node.

        This method can be overridden by subclasses.

        returns: Tuple(source_of_name, name)
        """
        name_key = None
        name_value = None
        if self.raw_input:  # try to get name from raw_input
            if (
                hasattr(self.raw_input, "titles")
                and isinstance(self.raw_input.titles, list)
                and len(self.raw_input.titles) > 0
            ):
                name_key = "title [raw_input]"
                name_value = self.raw_input.titles[0]["value"]
            elif hasattr(self.raw_input, "name"):
                name_key = "name [raw_input]"
                name_value = self.raw_input.name
        return name_key, name_value

    @property
    def _amount_and_its_source(self) -> Tuple[Optional[str], Optional[str]]:
        """Method that tries to extract an amount from the node.

                This method can be overridden by subclasses.
                    # prop_data[field_name] = [
                    #     await prop.as_dict(glossary_service, include_term_meta_data=include_term_meta_data)
                    #     for prop in field_name
                    # ]
                return prop_data

        returns: Tuple(source_of_amount, amount)
        """
        amount_key = None
        amount_value = None
        return amount_key, amount_value

    @property
    def _additional_info_useful_for_debugging(self) -> Dict[str, Any]:
        """Method that generates a dict of additional info useful for debugging.

        This method can be overridden by subclasses.

        returns: Dict[str, Any] of additional info useful for debugging
        """
        return {}

    @property
    def _location_and_its_source(self) -> Tuple[Optional[str], Optional[str]]:
        """Method that extracts a location from the node.

        Actual implementation is deferred to Activity and Flow Node.
        """
        return None, None

    @staticmethod
    def _location_as_string(location: str | List[LocationProp] | None) -> Tuple[Optional[str], Optional[str]]:
        """Method for extracting a location as a string."""
        if location:
            if isinstance(location, (DeepListView, list)):
                location_key = "location"
                location_value = "; ".join([loc.short_string_representation() for loc in location])
            else:
                location_key = "location [raw_string]"
                location_value = location
            return location_key, location_value
        else:
            return None, None

    def __repr__(self) -> str:
        """Representation of this node."""
        name = self.name()
        if name:
            return f"{type(self).__name__}[{str(self.uid)}, {name}]"
        else:
            return f"{type(self).__name__}[{str(self.uid)}]"

    def __str__(self) -> str:
        """String representation of this node."""
        return self.__repr__()

    def name(self) -> Optional[str]:
        """Get the name of the node."""
        _, name_value = self._name_and_its_source
        return name_value

    def __hash__(self):
        return self.uid.__hash__()

    def one_line_representation_for_debugging(self, additional_properties_to_print: List[str] = None) -> str:
        """A one line representation of this node. Useful for debugging."""
        # uid
        node_info = {"uid": str(self.uid)}

        # name
        name_key, name_value = self._name_and_its_source
        if name_value and name_key:
            node_info[name_key] = name_value

        # amount
        amount_key, amount_value = self._amount_and_its_source
        if amount_value and amount_key:
            node_info[amount_key] = amount_value

        # location
        location_key, location_value = self._location_and_its_source
        if location_key and location_value:
            node_info[location_key] = location_value

        # additional information that is relevant to understand how the calc-graph got constructed:
        additional_info = self._additional_info_useful_for_debugging
        node_info.update(additional_info)

        # add additional user-requested properties:
        if additional_properties_to_print:
            for additional_propertry in additional_properties_to_print:
                if hasattr(self, additional_propertry):
                    node_info[additional_propertry] = str(getattr(self, additional_propertry))

        return f"{type(self).__name__}[{node_info}]"

    def format_as_tree(self) -> str:
        """A tree of the subgraph of this node. Useful for debugging."""
        result = self.one_line_representation_for_debugging()
        for sub_node in self.get_sub_nodes():
            sub_tree_str = sub_node.format_as_tree()
            prefixed_sub_tree = "* ".join(("\n" + sub_tree_str.lstrip()).splitlines(True))
            result += prefixed_sub_tree
        return result

    def __eq__(self, other: Any):  # noqa: ANN401
        if isinstance(other, Node):
            if self.uid != other.uid:
                return False
            if self.access_group_uid != other.access_group_uid:
                return False
            if self.node_type != other.node_type:
                return False
            for key in self.model_fields_set:
                if self.__dict__[key] != other.__dict__[key]:
                    return False
            return True
        else:
            return False

    @staticmethod
    def from_node(class_instance: "Node") -> "Node":
        """Makes a model_copy of the class_instance."""
        return class_instance.model_copy()

    def _conversion_factor_to_and_from_fixed_amount_reference(
        self, origin_or_target_ref: ReferenceAmountEnum, fixed_amount: ReferencelessQuantityProp
    ) -> Optional[float]:
        """Conversion factor from/to a reference_amount (origin/target) to/from amount for a fixed amount reference."""
        if origin_or_target_ref == ReferenceAmountEnum.self_reference:
            raise ValueError(f"{self} referencing self cannot be converted to reference another amount.")

        conversion_string = UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING[
            GlossaryTermProp.get_term_from_uid(fixed_amount.get_unit_term().sub_class_of).xid
        ]

        fixed_amount_in_standard_unit = fixed_amount.value * fixed_amount.get_unit_term().data.get(conversion_string)

        amount, unit = self._node_amount()
        if not unit.sub_class_of == fixed_amount.get_unit_term().sub_class_of:
            raise ValueError("Cannot convert between two different types of units.")
        amount_in_standard_unit = amount * unit.data.get(conversion_string)

        if origin_or_target_ref == ReferenceAmountEnum.amount_for_root_node:
            supply = self._supply_for_root()
            return fixed_amount_in_standard_unit / (amount_in_standard_unit * supply)
        elif origin_or_target_ref == ReferenceAmountEnum.amount_for_activity_production_amount:
            return fixed_amount_in_standard_unit / amount_in_standard_unit
        else:
            raise TypeError(f"Unknown reference {origin_or_target_ref}.")

    def _convert_from_amount_for_fixed_amount_reference(
        self, value: float, origin_fixed_amount: ReferencelessQuantityProp, target_reference: ReferenceAmountEnum
    ) -> Optional[float]:
        """Conversion from amount for fixed amount to target reference amount."""
        factor = self._conversion_factor_to_and_from_fixed_amount_reference(target_reference, origin_fixed_amount)
        return value / factor

    def _convert_to_amount_for_fixed_amount_reference(
        self, value: float, origin_reference: ReferenceAmountEnum, target_fixed_amount: ReferencelessQuantityProp
    ) -> Optional[float]:
        """Conversion from origin reference amount to amount for fixed amount."""
        factor = self._conversion_factor_to_and_from_fixed_amount_reference(origin_reference, target_fixed_amount)
        return value * factor

    def _node_amount(self) -> Tuple[float, Term]:
        """Returns the amount of the node."""
        from core.domain.nodes.activity_node import ActivityNode
        from core.domain.nodes.flow_node import FlowNode

        if isinstance(self, FlowNode):
            if self.amount:
                return self.amount.value, self.amount.get_unit_term()
            elif isinstance(self.amount_in_original_source_unit, RawQuantity):
                raise ValueError(f"{self} is a RawQuantity, and unit_term cannot be extracted.")
            elif self.amount_in_original_source_unit:
                return self.amount_in_original_source_unit.value, self.amount_in_original_source_unit.get_unit_term()
            else:
                raise ValueError(f"{self} node has no flow amount.")
        elif isinstance(self, ActivityNode):
            return self.production_amount.value, self.production_amount.get_unit_term()
        else:
            raise ValueError(f"Unknown node type: {self}.")

    def _supply_for_root(self) -> Tuple[float, Term]:
        """Returns the supply for the root."""
        from core.domain.nodes.activity_node import ActivityNode
        from core.domain.nodes.flow_node import FlowNode

        if isinstance(self, FlowNode):
            if self.get_parent_nodes():
                if self.get_parent_nodes()[0].impact_assessment_supply_for_root:
                    return self.get_parent_nodes()[0].impact_assessment_supply_for_root.value
                else:
                    raise ValueError(
                        f"{self} node's parent has no supply for root. Amount cannot be converted to for root amount."
                    )
            else:
                # root flow node
                return 1.0
        elif isinstance(self, ActivityNode):
            if self.impact_assessment_supply_for_root:
                return self.impact_assessment_supply_for_root.value
            else:
                raise ValueError(f"{self} node has no supply for root. Amount cannot be converted to for root amount.")

    def convert_quantity_between_references(
        self,
        value: float,
        origin_reference: ReferenceAmountEnum | ReferencelessQuantityProp,
        target_reference: ReferenceAmountEnum | ReferencelessQuantityProp,
    ) -> Optional[float]:
        """Conversion from origin reference amount to target reference amount."""
        if target_reference == ReferenceAmountEnum.amount_for_activity_production_amount:
            node_amount, _ = self._node_amount()
            if node_amount == 0:
                return 0.0
        elif target_reference == ReferenceAmountEnum.amount_for_root_node:
            node_amount, _ = self._node_amount()
            if node_amount == 0:
                return 0.0
            supply = self._supply_for_root()
            if supply == 0.0:
                return 0.0
        if value == 0.0:
            return 0.0

        if isinstance(origin_reference, ReferencelessQuantityProp) and isinstance(
            target_reference, ReferencelessQuantityProp
        ):
            intermediate_value = self._convert_from_amount_for_fixed_amount_reference(
                value, origin_reference, ReferenceAmountEnum.amount_for_activity_production_amount
            )
            return self._convert_to_amount_for_fixed_amount_reference(
                intermediate_value, ReferenceAmountEnum.amount_for_activity_production_amount, target_reference
            )
        elif isinstance(origin_reference, ReferencelessQuantityProp):
            return self._convert_from_amount_for_fixed_amount_reference(value, origin_reference, target_reference)
        elif isinstance(target_reference, ReferencelessQuantityProp):
            return self._convert_to_amount_for_fixed_amount_reference(value, origin_reference, target_reference)
        else:
            if origin_reference == target_reference:
                return value
            elif (
                origin_reference == ReferenceAmountEnum.amount_for_activity_production_amount
                and target_reference == ReferenceAmountEnum.amount_for_root_node
            ):
                supply_for_root = self._supply_for_root()
                return value * supply_for_root
            elif (
                origin_reference == ReferenceAmountEnum.amount_for_root_node
                and target_reference == ReferenceAmountEnum.amount_for_activity_production_amount
            ):
                supply_for_root = self._supply_for_root()
                return value / supply_for_root
            else:
                raise TypeError(
                    f"Unrecognized origin and target references: {origin_reference} and {target_reference}."
                )

    # For development, we check if nodes are read-only.
    if EnvProps().VERIFY_NODE_READ_ONLY:

        def __getattribute__(self, item: str):
            """Wrapper to avoid modification of lists and dicts."""
            attr_val = object.__getattribute__(self, item)

            attr_val_type = type(attr_val)
            if attr_val_type is list:
                return DeepListView(attr_val)
            if attr_val_type is dict:
                # Only return fields in DeepMappingView.
                if item not in object.__getattribute__(self, "model_fields"):
                    return attr_val
                return DeepMappingView(attr_val)
            return attr_val

        def get_sub_nodes(self) -> ["Node"]:
            return DeepListView(self._sub_nodes)

        def get_parent_nodes(self) -> ["Node"]:
            return DeepListView(self._parent_nodes)

    else:

        def get_sub_nodes(self) -> ["Node"]:
            return self._sub_nodes

        def get_parent_nodes(self) -> ["Node"]:
            return self._parent_nodes

    model_config = ConfigDict(
        extra="forbid", validate_assignment=EnvProps().PYDANTIC_VALIDATE_ASSIGNMENT, populate_by_name=True
    )


@dataclass
class PreAndPostCalcNodes:
    pre_calc: Node
    post_calc: Optional[Node] = None
