from typing import Literal, Optional, Tuple, Union

from pydantic import Field
from structlog import get_logger

from core.domain.deep_mapping_view import DeepListView
from core.domain.nodes.node import Node
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.props.link_to_xid_prop import LinkToXidProp
from core.domain.props.location_prop import LocationProp
from core.domain.props.matrix_gfm_error_prop import MatrixGfmErrorProp
from core.domain.props.names_prop import NamesProp, RawName
from core.domain.props.origin_data_prop import OriginDataProp
from core.domain.props.quantity_prop import QuantityProp, RawQuantity

logger = get_logger()


class FlowNode(Node):
    product_name: Optional[Union[list[RawName], NamesProp]] = Field(default=None)
    node_type: Literal["FlowNode"] = Field(default="FlowNode")
    amount: Optional[QuantityProp] = Field(default=None)
    amount_in_original_source_unit: Optional[Union[QuantityProp, RawQuantity]] = Field(default=None)
    glossary_tags: Optional[list[GlossaryTermProp]] = Field(default=None)
    flow_location: Optional[Union[str, list[LocationProp]]] = Field(default=None)
    origin_data: Optional[OriginDataProp] = Field(default=None)
    link_to_sub_node: Optional[Union[LinkToXidProp, LinkToUidProp]] = Field(default=None)
    matrix_gfm_error: Optional[MatrixGfmErrorProp] = Field(default=None)

    # Raw fields that we receive from the API:
    raw_transport: Optional[str] = Field(default=None)

    @property
    def tag_term_xids(self) -> frozenset[str]:
        """Property returning a set of xids of terms in glossary_tags."""
        # TODO Eventually, product_name and glossary_tags should be merged into glossary_tags.
        product_name_terms_present = self.product_name and isinstance(self.product_name, NamesProp)
        if product_name_terms_present:
            tag_term_xids = frozenset(p.get_term().xid for p in self.product_name.terms)
        else:
            tag_term_xids = frozenset()
        return tag_term_xids

    def get_origin_flow_location(self) -> Optional[list[LocationProp]]:
        """Returns the origin location of the node."""
        if isinstance(self.flow_location, (DeepListView, list)):
            return self.flow_location
        else:
            return None

    @property
    def _location_and_its_source(self) -> Tuple[Optional[str], Optional[str]]:
        """Method that extracts a location from the node.

        returns: Tuple(source_of_location, location)
        """
        return self._location_as_string(self.flow_location)

    @property
    def _name_and_its_source(self) -> Tuple[Optional[str], Optional[str]]:
        """Method that tries to extract a name from the node.

        returns: Tuple(source_of_name, name)
        """
        name_key = None
        name_value = None
        if self.product_name:
            if isinstance(self.product_name, NamesProp):
                name_key = "product_name"
                if self.product_name.source_data_raw:
                    name_value = self.product_name.source_data_raw[0]["value"]
                else:
                    name_value = self.product_name.terms[0].get_term().name
            else:  # it's a RawName
                name_key = "product_name [raw_name]"
                name_value = self.product_name[0]["value"]
        if name_key and name_value:
            return name_key, name_value
        else:
            return super()._name_and_its_source

    @property
    def _amount_and_its_source(self) -> Tuple[Optional[str], Optional[str]]:
        """Method that tries to extract an amount from the node.

        This method can be overridden by subclasses.

        returns: Tuple(source_of_amount, amount)
        """
        amount_key = None
        amount_value = None
        if self.amount:
            amount_value = self.amount.short_string_representation()
            amount_key = f"converted_amount [{self.amount.for_reference.removeprefix('amount_')}]"
        elif self.amount_in_original_source_unit:
            amount_value = self.amount_in_original_source_unit.short_string_representation()
            if isinstance(self.amount_in_original_source_unit, QuantityProp):
                orig_amount = self.amount_in_original_source_unit.for_reference.removeprefix("amount_")
                amount_key = f"amount_in_original_source_unit [{orig_amount}]"
            else:  # it's a RawQuantity
                amount_key = "amount_in_original_source_unit [raw_quantity]"
        if amount_key and amount_value:
            return amount_key, amount_value
        else:
            return super()._amount_and_its_source
