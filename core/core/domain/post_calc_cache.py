from datetime import date
from typing import Dict, List, NamedTuple, Optional
from uuid import UUID

from pydantic import BaseModel, ConfigDict, field_serializer

from core.domain.final_graph_criterion import FinalGraphTableCriterion
from core.domain.nodes.node import Node


class PostCalcDataMetaInformation(BaseModel):
    """Stores meta-information about the cached calc-data in the node-table."""

    updated_at: date
    final_graph_table_criterion: Optional[FinalGraphTableCriterion] = None

    model_config = ConfigDict(extra="forbid")

    @field_serializer("updated_at")
    def serialize_updated_at(self, updated_at: date) -> str:
        return updated_at.isoformat()


class PostCalcCache(BaseModel):
    """Represents a cache for post-calculation data, containing a node, final graph, and meta information."""

    node: Node
    final_graph_table: Optional[List[Dict]] = None
    meta_information: PostCalcDataMetaInformation

    model_config = ConfigDict(extra="forbid")


class NodeCacheInfo(NamedTuple):
    uid: UUID
    root_flow_uid: UUID
    recipe_portions: Optional[int]
    production_portions: Optional[int]
    sold_portions: Optional[int]
    access_group_uid: Optional[UUID]
    cache_present: bool
    final_graph_table: Optional[Dict[str, str]]
    post_calc_data_meta_information: Optional[PostCalcDataMetaInformation]
    root_flow_post_calc_data_meta_information: Optional[PostCalcDataMetaInformation]
