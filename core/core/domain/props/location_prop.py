"""Location property to store location information."""

import math
import uuid
from enum import Enum
from typing import Any, Dict, Literal, Optional, Type, Union

import numpy as np
from pydantic import ConfigDict, Field, field_validator, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs
from structlog import get_logger

from core.domain.prop import Prop
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.term import Term

logger = get_logger()


class LocationSourceEnum(str, Enum):
    """Enumerator for location sources."""

    default = "DEFAULT"
    foodex = "FOODEX"
    gadm = "GADM"
    fao_stat = "FAO STAT"
    google = "GOOGLE"
    unknown = "UNKNOWN"

    def __repr__(self) -> str:
        return self.value


class LocationQualifierEnum(str, Enum):
    """Enumerator for qualifying location as being known of unknown."""

    known = "KNOWN"
    unknown = "UNKNOWN"

    def __repr__(self) -> str:
        return self.value


def get_gadm_term_level(gadm_term: Term) -> Optional[int]:
    """Get GADM term level."""
    return len(gadm_term.xid.split("."))


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class LocationProp(Prop):
    """Property for storing location information."""

    prop_type: Literal["LocationProp"] = Field(default="LocationProp")

    address: str = Field()
    location_qualifier: LocationQualifierEnum = Field()
    latitude: Optional[float] = Field(default=None)
    longitude: Optional[float] = Field(default=None)
    country_code: Optional[str] = Field(default=None)
    term_uid: Optional[uuid.UUID] = Field(default=None)
    source: Optional[LocationSourceEnum] = Field(default=None)

    model_config = ConfigDict(extra="forbid")

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls: Type["LocationProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        """Modify initialize kwargs to pass validation."""
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values
        # these are included when serializing the model, but we only use term_uid to define the term to be used:
        if "term_xid" in values_kwargs:
            del values_kwargs["term_xid"]
        if "term_name" in values_kwargs:
            del values_kwargs["term_name"]
        return values

    def short_string_representation(self, precision_for_representing_floats: int = 2) -> str:
        """Short string representation of LocationProp for debugging."""
        rounded_lat = np.round(self.latitude, precision_for_representing_floats)
        rounded_lng = np.round(self.longitude, precision_for_representing_floats)
        return f"{self.address}, {rounded_lat, rounded_lng}"

    def model_dump_extra(self, dump_dict: Dict[str, Any]) -> Dict[str, Any]:
        """Additional LocationProp information is serialized into the dump dictionary."""
        if self.term_uid is not None:
            dump_dict["term_uid"] = str(self.term_uid)
            try:
                term = self.get_term()
                dump_dict["term_name"] = term.name
                dump_dict["term_xid"] = term.xid
            except KeyError:
                logger.debug(f"Term with uid {self.term_uid} is not in glossary service cache.")
        return dump_dict

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in LocationProp using data dictionary."""
        for key, val in data.items():
            if key == "term_uid" and val is not None:
                if isinstance(val, str):
                    self.term_uid = uuid.UUID(val)
                else:
                    self.term_uid = val
            else:
                if hasattr(self, key):
                    setattr(self, key, val)
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def get_term(self) -> Optional[Term]:
        """Obtain associated location term."""
        return GlossaryTermProp.get_term_from_uid(self.term_uid)

    @field_validator("country_code")
    @classmethod
    def validate_country_code(cls: Type["LocationProp"], value: str) -> str | None:
        """Validate country code of location."""
        if value is not None:
            if len(value) == 2 or len(value) == 3:
                return value
            else:
                raise ValueError(f"Invalid country code: {value}. Country code must be 2 or 3 characters long.")
        else:
            return None

    def __eq__(self, other: object) -> bool:
        """Compare other objects with LocationProp."""
        if type(other) is type(self):
            return all(
                getattr(self, attr) == getattr(other, attr) for attr in self.__annotations__ if attr != "_owner_node"
            )
        return False

    def exact_and_identical(self, other: "LocationProp") -> bool:
        """Check if two LocationProps exact loacations and identical."""
        if not (
            self.location_qualifier == LocationQualifierEnum.known
            and isinstance(self.longitude, float)
            and isinstance(self.latitude, float)
        ):
            return False
        if not (
            other.location_qualifier == LocationQualifierEnum.known
            and isinstance(other.longitude, float)
            and isinstance(other.latitude, float)
        ):
            return False

        gadm_term_self = self.get_term()
        gadm_term_other = other.get_term()
        if gadm_term_self is None or gadm_term_other is None:
            return False
        gadm_term_level_self = get_gadm_term_level(gadm_term_self)
        gadm_term_level_other = get_gadm_term_level(gadm_term_other)
        if (
            gadm_term_level_self > 2
            and gadm_term_level_other > 2
            and gadm_term_level_self == gadm_term_level_other
            and math.isclose(self.longitude, other.longitude)
            and math.isclose(self.latitude, other.latitude)
        ):
            return True
        else:
            return False
