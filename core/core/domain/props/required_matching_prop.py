from enum import IntFlag
from typing import Any, Dict, List, Literal, Optional

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.prop import Prop
from core.domain.props.names_prop import RawName

logger = get_logger()


class MatchingFailureReasonEnum(IntFlag):
    """IntFlag for the reasons why a matching failed."""

    compound_product = 1
    ingredient_splitter = 2
    no_matching_product = 4
    after_splitter = 8
    after_splitter_and_last_ingredient = 16  # added to make it easier for matchers to identify data-errors

    def __repr__(self) -> str:
        return ",".join([flag.name for flag in MatchingFailureReasonEnum if self & flag])

    def __str__(self) -> str:
        return self.__repr__()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid", use_enum_values=True), slots=True, repr=False)
class RequiredMatchingProp(Prop):
    """Property containing manual matching information."""

    prop_type: Literal["RequiredMatchingProp"] = Field(default="RequiredMatchingProp")

    matching_failure_reason: MatchingFailureReasonEnum = Field()
    names_to_match: Optional[List[RawName]] = Field(default=None)
    faulty_declarations: Optional[List[RawName]] = Field(default=None)
    matching_context: Optional[dict] = Field(default=None)

    def from_dict(self, data: Dict[str, Any]) -> None:
        self.matching_failure_reason = data["matching_failure_reason"]
        if data.get("names_to_match"):
            self.names_to_match = data["names_to_match"]
        if data.get("faulty_declarations"):
            self.faulty_declarations = data["faulty_declarations"]
        if data.get("matching_context"):
            self.matching_context = data["matching_context"]

    def __eq__(self, other: object) -> bool:
        if type(self) is type(other):
            return (
                self.matching_failure_reason == other.matching_failure_reason
                and self.names_to_match == other.names_to_match
                and self.faulty_declarations == other.faulty_declarations
                and self.matching_context == other.matching_context
            )
        return False
