from typing import TYPE_CHECKING, Union

from core.domain.deep_mapping_view import DeepListView, DeepMappingView
from core.domain.prop import Prop
from core.domain.props.location_prop import LocationProp, LocationQualifierEnum
from core.domain.props.quantity_prop import ReferenceAmountEnum

if TYPE_CHECKING:
    from core.domain.nodes.node import Node


DFU_IMPROVEMENT_PERCENTAGE_THRESHOLD = 1e-10


def ignore_none(_: Union[str, dict, DeepMappingView, list, DeepListView, Prop]) -> bool:
    """Ignore nothing."""
    return False


def flow_location_is_known(flow_location: Union[list, DeepListView, Prop]) -> bool:
    """Check if flow_location is known."""
    if (
        isinstance(flow_location, (list, DeepListView))
        and len(flow_location) == 1
        and isinstance(flow_location[0], LocationProp)
        and flow_location[0].location_qualifier.value == LocationQualifierEnum.known.value
    ):
        return True
    else:
        return False


def has_one_of_predefined_tags(tag_term_xids: frozenset[str], tag_term_xids_to_search: frozenset[str]) -> bool:
    """Check if glossary_tags contains one of the predefined tags."""
    assert isinstance(tag_term_xids, frozenset)
    if any([tag_term_xid in tag_term_xids for tag_term_xid in tag_term_xids_to_search]):
        return True
    else:
        return False


def calculate_percentage_difference(value: float, benchmark: float) -> float:
    return 100 * (benchmark - value) / benchmark


def get_rating(value_to_compare: float, thresholds: dict) -> str | None:
    if value_to_compare < 0:
        return None
    if value_to_compare <= thresholds["A"]:
        return "A"
    elif value_to_compare <= thresholds["B"]:
        return "B"
    elif value_to_compare <= thresholds["C"]:
        return "C"
    elif value_to_compare <= thresholds["D"]:
        return "D"
    else:
        return "E"


def get_daily_food_unit(
    node: "Node", reference: ReferenceAmountEnum = "amount_for_activity_production_amount"
) -> float | None:
    if not node:
        return None

    if not hasattr(node, "daily_food_unit"):
        parent_nodes = node.get_parent_nodes()
        if len(parent_nodes) != 1:
            return None
        node = parent_nodes[0]
        if not hasattr(node, "daily_food_unit"):
            return None

    dfu_prop = node.daily_food_unit
    if dfu_prop is None:
        return None

    dfu_for_reference = dfu_prop.amount_for_reference(reference)
    dfu_value = dfu_for_reference.value
    if dfu_value < 0:
        return None

    return dfu_value
