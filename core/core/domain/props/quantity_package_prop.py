"""Quantity packages (e.g., nutrient values) with multiple quantities (with a common reference)."""

from enum import Enum
from typing import Any, Dict, Literal, Optional, Set, Type, Union
from uuid import UUID

from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs

from core.domain.prop import Prop
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp
from core.domain.term import Term


def to_uuid(uid: str | UUID) -> UUID | None:
    """Convert string or UUID into UUID."""
    if isinstance(uid, UUID):
        return uid
    elif isinstance(uid, str):
        return UUID(uid)
    else:
        raise ValueError(f"{uid} should either be a string or a UUID.")


class QuantityPackageSourceEnum(str, Enum):
    """Enumerator for quantity package sources."""

    api = "api"
    database = "database"
    model = "model"

    def __repr__(self) -> str:
        return self.value


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class QuantityPackageProp(Prop):
    """Quantity package property with multiple quantities (with a common reference)."""

    prop_type: Literal["QuantityPackageProp"] = Field(default="QuantityPackageProp")

    prop_term_uid: Optional[UUID] = Field(default=None)  # UUID of the term associated with the property.
    quantities: Dict[UUID, ReferencelessQuantityProp] = Field()
    source: Optional[QuantityPackageSourceEnum] = Field(default=None)
    for_reference: ReferenceAmountEnum = Field()

    @property
    def _props_to_remove_from_repr(self) -> Set[str]:
        """Returns a set of properties to be excluded from __repr__."""
        return {"_owner_node", "prop_type", "unit_term_uid", "unit_term_xid"}

    @classmethod
    def pre_init_modifications(
        cls: Type["QuantityPackageProp"], values: Union[Dict, ArgsKwargs]
    ) -> Union[Dict, ArgsKwargs]:
        # name and xid of the quantity term are included in serialization, but removed for deserialization:
        try:
            if isinstance(values, ArgsKwargs):
                values_kwargs = values.kwargs
            else:
                values_kwargs = values

            keys_to_delete = ["prop_term_xid", "prop_term_name"]
            for_reference = None
            for key in values_kwargs.keys():
                if key in set(ReferenceAmountEnum):
                    if not for_reference:
                        for_reference = key
                    keys_to_delete.append(key)

            if not for_reference:
                return values

            quantities_dict = values_kwargs[for_reference]
            values_kwargs["for_reference"] = for_reference
            values_kwargs["quantities"] = {
                to_uuid(term_uid): qty if isinstance(qty, ReferencelessQuantityProp) else qty["quantity"]
                for term_uid, qty in quantities_dict.items()
            }

            for key in keys_to_delete:
                if key in values_kwargs:
                    del values_kwargs[key]
        except ValueError:
            pass

        return values

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls: Type["QuantityPackageProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        return cls.pre_init_modifications(values)

    def get_prop_term(self) -> Term | None:
        """Get term associated with the property."""
        if self.prop_term_uid:
            return GlossaryTermProp.get_term_from_uid(self.prop_term_uid)
        else:
            return None

    @staticmethod
    def serialize_quantities(quantities: dict[UUID, ReferencelessQuantityProp], exclude: Optional[set] = None) -> dict:
        """Serialize quantities field."""
        try:
            return {
                str(term_uid): {
                    "quantity": qty.model_dump_base(exclude=exclude),
                    "quantity_term_xid": quantity_term.xid,
                    "quantity_term_name": quantity_term.name,
                }
                for term_uid, qty in quantities.items()
                for quantity_term in [GlossaryTermProp.get_term_from_uid(term_uid)]
            }
        except KeyError:
            return {
                str(term_uid): {"quantity": qty.model_dump_base(exclude=exclude)}
                for term_uid, qty in quantities.items()
            }

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Serialize the entire model."""
        try:
            requested_references = self._owner_node.get_calculation().requested_quantity_references
        except AttributeError:
            requested_references = []

        if len(requested_references) == 0 or requested_references == self.for_reference:
            output = {
                (
                    self.for_reference.value
                    if isinstance(self.for_reference, ReferenceAmountEnum)
                    else self.for_reference
                ): self.serialize_quantities(self.quantities, exclude=exclude)
            }
        elif len(requested_references) == 1:
            try:
                amount_for_reference = getattr(self, requested_references[0])()
                output = {
                    (
                        requested_references[0].value
                        if isinstance(requested_references[0], ReferenceAmountEnum)
                        else requested_references[0]
                    ): self.serialize_quantities(amount_for_reference.quantities, exclude=exclude)
                }
            except (TypeError, ValueError):
                output = {
                    (
                        self.for_reference.value
                        if isinstance(self.for_reference, ReferenceAmountEnum)
                        else self.for_reference
                    ): self.serialize_quantities(self.quantities, exclude=exclude)
                }
        else:
            output = {}
            for req_ref in requested_references:
                try:
                    amount_for_reference = getattr(self, req_ref)()
                    if isinstance(req_ref, ReferenceAmountEnum):
                        dict_key = req_ref.value
                    else:
                        dict_key = req_ref
                    output[dict_key] = self.serialize_quantities(amount_for_reference.quantities, exclude=exclude)
                except (TypeError, ValueError):
                    pass
            if not output:
                output = {
                    (
                        self.for_reference.value
                        if isinstance(self.for_reference, ReferenceAmountEnum)
                        else self.for_reference
                    ): self.serialize_quantities(self.quantities)
                }

        output["prop_type"] = self.prop_type
        prop_term = self.get_prop_term()
        if prop_term:
            output["prop_term_uid"] = str(self.prop_term_uid)
            output["prop_term_xid"] = str(prop_term.xid)
            output["prop_term_name"] = str(prop_term.name)

        if self.source:
            output["source"] = self.source

        if self.source_data_raw:
            output["source_data_raw"] = self.source_data_raw
        if exclude:
            for field in exclude:
                if field in output:
                    del output[field]
        return output

    def from_dict(self, data: dict) -> None:
        """Construct model from dict."""
        new_data = self.pre_init_modifications(data)
        self.for_reference = new_data["for_reference"]
        self.quantities = {
            key: qty if isinstance(qty, ReferencelessQuantityProp) else ReferencelessQuantityProp(**qty)
            for key, qty in new_data["quantities"].items()
        }
        if "prop_term_uid" in new_data:
            self.prop_term_uid = to_uuid(new_data["prop_term_uid"])
        if data.get("source"):
            self.source = data["source"]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def _convert_value_for_reference(
        self, value: float, gram_term_uid: UUID, target_reference: ReferenceAmountEnum
    ) -> float:
        if not (
            self.for_reference == ReferenceAmountEnum.amount_for_100g
            or target_reference == ReferenceAmountEnum.amount_for_100g
        ):
            value = self._owner_node.convert_quantity_between_references(value, self.for_reference, target_reference)
        elif self.for_reference == ReferenceAmountEnum.amount_for_100g:
            value = self._owner_node.convert_quantity_between_references(
                value, ReferencelessQuantityProp(value=100.0, unit_term_uid=gram_term_uid), target_reference
            )
        else:  # target_reference == ReferenceAmountEnum.amount_for_100g:
            value = self._owner_node.convert_quantity_between_references(
                value, self.for_reference, ReferencelessQuantityProp(value=100.0, unit_term_uid=gram_term_uid)
            )
        return value

    def amount_for_reference(self, target_reference: ReferenceAmountEnum) -> Union["QuantityPackageProp", None]:
        """Convert to target reference."""
        if self.for_reference == target_reference:
            return self

        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        glossary_service = service_provider.glossary_service
        gram_term = glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", glossary_service.root_subterms["EOS_units"].access_group_uid)
        ]

        new_quantities_dict: Dict[UUID, ReferencelessQuantityProp] = {}
        for key, qty in self.quantities.items():
            value = self._convert_value_for_reference(qty.value, gram_term.uid, target_reference)
            new_quantities_dict[key] = ReferencelessQuantityProp.unvalidated_construct(
                unit_term_uid=qty.unit_term_uid, value=value
            )
        return QuantityPackageProp.unvalidated_construct(quantities=new_quantities_dict, for_reference=target_reference)

    def amount_for_100g(self) -> Union["QuantityPackageProp", None]:
        """Return amount for 100g."""
        target_reference = ReferenceAmountEnum.amount_for_100g
        return self.amount_for_reference(target_reference)

    def amount_for_root_node(self) -> Union["QuantityPackageProp", None]:
        """Return amount for root node."""
        target_reference = ReferenceAmountEnum.amount_for_root_node
        return self.amount_for_reference(target_reference)

    def amount_for_activity_production_amount(self) -> Union["QuantityPackageProp", None]:
        """Return amount for production amount."""
        target_reference = ReferenceAmountEnum.amount_for_activity_production_amount
        return self.amount_for_reference(target_reference)

    def __eq__(self, other: object):
        """Check for equality of two QuantityPackageProps."""
        if type(self) is type(other):
            if self.for_reference != other.for_reference:
                return False
            if self.prop_term_uid != other.prop_term_uid:
                return False
            if self.source != other.source:
                return False
            for qty_uid, qty in self.quantities.items():
                if qty_uid not in other.quantities:
                    return False
                elif qty != other.quantities[qty_uid]:
                    return False
            return True
        else:
            return False
