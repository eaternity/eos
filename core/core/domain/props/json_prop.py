from typing import Literal, Type

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass

from core.domain.prop import Prop


@dataclass(kw_only=True, config=ConfigDict(extra="allow"), repr=False)  # slots=True)
class JsonProp(Prop):
    prop_type: Literal["JsonProp"] = Field(default="JsonProp")

    @property
    def model_fields_set(self) -> set[str]:
        return set(self.__dict__.keys())

    def get_data(self) -> dict:
        return self.__dict__

    def from_dict(self, data: dict) -> "JsonProp":
        return self.__init__(**data)

    def __eq__(self, other: object) -> bool:
        """Compare other objects with JsonProp."""
        if type(other) is type(self):
            return self.__dict__ == other.__dict__
        return False

    def fields_to_duplicate(self) -> "JsonProp":
        if hasattr(self, "__dict__"):
            return self.__dict__.keys()
        else:
            raise AttributeError(f"__dict__ missing in {self.__class__.__name__}")

    @classmethod
    def unvalidated_construct(cls: Type["JsonProp"], **kwargs: object) -> "JsonProp":
        """Call the normal constructor to behave the same as prop()."""
        return cls(**kwargs)
