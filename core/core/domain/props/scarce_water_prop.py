"""Scarce water prop."""

from typing import Any, Dict, Literal, Optional, Type, Union

from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs
from structlog import get_logger

from core.domain.props.quantity_prop import QuantityProp, ReferenceAmountEnum
from core.domain.props.utils import (
    DFU_IMPROVEMENT_PERCENTAGE_THRESHOLD,
    calculate_percentage_difference,
    get_daily_food_unit,
    get_rating,
)

logger = get_logger()
RATING_THRESHOLDS = {"A": 18.31, "B": 36.62, "C": 73.24, "D": 211.97}


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True)
class ScarceWaterProp(QuantityProp):
    """Quantity prop for scarce water."""

    prop_type: Literal["ScarceWaterProp"] = Field(default="ScarceWaterProp")

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls: Type["ScarceWaterProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        values = QuantityProp.pre_init_modifications(values)
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values

        for key in (
            "scarce_water_consumption_rating",
            "scarce_water_consumption_award",
            "scarce_water_consumption_improvement_percentage",
            "scarce_water_consumption_reduction_value",
        ):
            if key in values_kwargs:
                del values_kwargs[key]
        return values

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        output = QuantityProp.model_dump_base(self, exclude=exclude)

        try:
            requested_references = self._owner_node.get_calculation().requested_quantity_references
        except AttributeError:
            requested_references = []
        # TODO: I think in most other places we have amount_for_root_node as default, why not here?
        if not requested_references:
            requested_references = ["amount_for_activity_production_amount"]

        for reference in requested_references:
            daily_food_unit_for_reference = get_daily_food_unit(self.get_owner_node(), reference)
            if daily_food_unit_for_reference:
                food_unit_for_reference = daily_food_unit_for_reference * 5
            else:
                food_unit_for_reference = None
            try:
                scarce_water_qty_for_reference = self.amount_for_reference(reference)
                if (
                    scarce_water_qty_for_reference is None
                    or scarce_water_qty_for_reference.get_unit_term().xid != "EOS_liter"
                ):
                    # No rating is available if unit is not in liters.
                    continue
            except (ValueError, TypeError):
                continue

            # these quantities should not depend on the reference-value, so we can just take
            # the first reference
            if "scarce_water_consumption_rating" not in output:
                value = self.get_water_footprint_rating(scarce_water_qty_for_reference.value, food_unit_for_reference)
                if value is not None:
                    output["scarce_water_consumption_rating"] = value

            if "scarce_water_consumption_improvement_percentage" not in output:
                value = self.get_water_footprint_improvement_percentage(
                    scarce_water_qty_for_reference.value, food_unit_for_reference
                )
                if value is not None:
                    output["scarce_water_consumption_improvement_percentage"] = value

            if "scarce_water_consumption_award" not in output:
                if output.get("scarce_water_consumption_rating") is not None:
                    if (
                        output.get("scarce_water_consumption_rating") == "A"
                        or output.get("scarce_water_consumption_rating") == "B"
                    ):
                        output["scarce_water_consumption_award"] = True
                    else:
                        output["scarce_water_consumption_award"] = False

            if "scarce_water_consumption_reduction_value" not in output:
                output["scarce_water_consumption_reduction_value"] = {}
            value = self.get_water_footprint_reduction_value(
                scarce_water_qty_for_reference.value, food_unit_for_reference
            )
            if value is not None:
                output["scarce_water_consumption_reduction_value"][
                    reference.value if isinstance(reference, ReferenceAmountEnum) else reference
                ] = value

        return output

    @staticmethod
    def get_water_footprint_rating(scarce_water_liters: float, food_unit: float | None) -> str | None:
        if scarce_water_liters < 0.0 or food_unit is None or food_unit < DFU_IMPROVEMENT_PERCENTAGE_THRESHOLD:
            return ""
        return get_rating(scarce_water_liters / food_unit, RATING_THRESHOLDS)

    @staticmethod
    def get_water_footprint_improvement_percentage(scarce_water_liters: float, food_unit: float | None) -> float | None:
        if food_unit is None:
            return None
        if food_unit < DFU_IMPROVEMENT_PERCENTAGE_THRESHOLD:
            return 0.0
        return calculate_percentage_difference(scarce_water_liters / food_unit, RATING_THRESHOLDS["B"])

    @staticmethod
    def get_water_footprint_reduction_value(scarce_water_liters: float, food_unit: float | None) -> float | None:
        if food_unit is None:
            return None
        return RATING_THRESHOLDS["B"] * food_unit - scarce_water_liters

    def __eq__(self, other: object):
        """Check for equality of two ScarceWaterProp."""
        return QuantityProp.__eq__(self, other)
