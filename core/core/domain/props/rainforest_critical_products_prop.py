"""Rainforest critical products prop."""

from typing import Any, Dict, Literal, Optional, Type, Union

from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs
from structlog import get_logger

from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.quantity_package_prop import QuantityPackageProp

logger = get_logger()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True)
class RainforestCriticalProductsProp(QuantityPackageProp):
    """Quantity package property with multiple quantities (with a common reference)."""

    prop_type: Literal["RainforestCriticalProductsProp"] = Field(default="RainforestCriticalProductsProp")

    @model_validator(mode="before")
    @classmethod
    def pre_root(
        cls: Type["RainforestCriticalProductsProp"], values: Union[Dict, ArgsKwargs]
    ) -> Union[Dict, ArgsKwargs]:
        values = QuantityPackageProp.pre_init_modifications(values)
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values

        if "rainforest_rating" in values_kwargs:
            del values_kwargs["rainforest_rating"]
        return values

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        output = QuantityPackageProp.model_dump_base(self, exclude=exclude)
        output["rainforest_rating"] = self.get_rainforest_rating()
        return output

    def get_rainforest_rating(self) -> str | None:
        # Retrieve amount for 100g such that we can do cutoff based on percentages.

        try:
            amount_for_root_node = self.get_owner_node().amount.amount_for_root_node().value
        except (ValueError, TypeError, AttributeError):
            amount_for_root_node = None
        if amount_for_root_node is None or amount_for_root_node == 0.0:
            # if the flow amount is 0, we cannot calculate the rainforest rating and therefore want to return the worst
            # rating by default.
            return "E"

        try:
            quantities_for_100g = self.amount_for_100g()
        except (ValueError, TypeError, AttributeError):
            quantities_for_100g = None

        if not quantities_for_100g:
            logger.debug(f"Cannot retrieve rainforest rating for owner node [{self.get_owner_node()}].")
            return None

        amounts: dict = {
            "EOS_rainforest_conservation_certified": 0.0,
            "EOS_certified_rainforest_not_specified": 0.0,
            "EOS_not_certified_for_rainforest": 0.0,
        }

        for key, val in quantities_for_100g.quantities.items():
            term = GlossaryTermProp.get_term_from_uid(key)
            unit = val.get_unit_term()
            assert unit.data.get("mass-in-g")
            mass_in_g = val.value * unit.data.get("mass-in-g")
            amounts[term.xid] += mass_in_g

        is_rainforest_conservation_certified_present: bool = False
        is_certified_rainforest_not_specified_present: bool = False
        is_not_certified_for_rainforest_present: bool = False

        cutoff_value = 1.0

        if amounts["EOS_rainforest_conservation_certified"] > cutoff_value:
            is_rainforest_conservation_certified_present = True

        if amounts["EOS_certified_rainforest_not_specified"] > cutoff_value:
            is_certified_rainforest_not_specified_present = True

        if amounts["EOS_not_certified_for_rainforest"] > cutoff_value:
            is_not_certified_for_rainforest_present = True

        if not is_not_certified_for_rainforest_present and not is_certified_rainforest_not_specified_present:
            return "A"
        elif is_certified_rainforest_not_specified_present and not is_not_certified_for_rainforest_present:
            return "B"
        elif is_rainforest_conservation_certified_present and is_not_certified_for_rainforest_present:
            return "B"
        else:
            return "E"

    def __eq__(self, other: object):
        """Check for equality of two RainforestCriticalProductsProp objects."""
        return QuantityPackageProp.__eq__(self, other)
