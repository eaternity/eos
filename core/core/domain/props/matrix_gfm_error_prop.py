"""Module containing property with matrix gfm error message."""

from typing import Any, Dict, Literal, Type

from pydantic import ConfigDict, Field, field_validator
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.prop import Prop

logger = get_logger()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class MatrixGfmErrorProp(Prop):
    """Property containing matrix gfm error message."""

    prop_type: Literal["MatrixGfmErrorProp"] = Field(default="MatrixGfmErrorProp")

    value: str = Field()

    @field_validator("value")
    @classmethod
    def validate_error_message(cls: Type["MatrixGfmErrorProp"], value: str) -> str:
        """Validate language code of names."""
        if value.startswith("Error: "):
            return value
        else:
            raise ValueError(f"{value} is not a string containing an error.")

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in MatrixGfmErrorProp using data dictionary."""
        self.value = data["value"]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object) -> bool:
        """Compare other objects with MatrixGfmErrorProp."""
        if type(other) is type(self):
            return self.value == other.value
        return False
