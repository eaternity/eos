from enum import Enum
from typing import Any, Dict, Literal, Optional

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass

from core.domain.prop import Prop


class LegacyIngredientTypeEnum(str, Enum):
    conceptual_ingredients = "conceptual-ingredients"
    recipes = "recipes"
    commercial_ingredients = "commercial-ingredients"


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True)
class LegacyAPIInformationProp(Prop):
    prop_type: Literal["LegacyAPIInformationProp"] = Field(default="LegacyAPIInformationProp")

    type: Optional[LegacyIngredientTypeEnum] = Field(default=None)

    # Properties as they came in from the legacy API request body before they were updated by
    # gap filling modules.
    # It is useful to store the origin location of the food product for legacy conversion.
    properties_pre_gfms: Optional[Dict[str, Any]] = Field(default=None)

    # Required for scaling down the production amount for "piece" unit in legacy API. 1 piece is equivalent to
    # production_amount / recipe_portions of a sub-recipe.
    recipe_portions: Optional[float] = Field(default=None)
