"""Matching quantity prop."""

from typing import Dict, Literal, Union
from uuid import UUID

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.props.quantity_package_prop import QuantityPackageProp, to_uuid
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop_with_additional_info import (
    ReferencelessQuantityPropWithAdditionalInfo,
)

logger = get_logger()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True)
class QuantityPackagePropWithAdditionalInfoOnQuantities(QuantityPackageProp):
    """Quantity package for aggregating the amounts and client-sent-product-strings (examples) for each matching."""

    prop_type: Literal["QuantityPackagePropWithAdditionalInfoOnQuantities"] = Field(
        default="QuantityPackagePropWithAdditionalInfoOnQuantities"
    )
    quantities: Dict[UUID, ReferencelessQuantityPropWithAdditionalInfo] = Field()

    def amount_for_reference(self, target_reference: ReferenceAmountEnum) -> Union["QuantityPackageProp", None]:
        """Convert to target reference."""
        if self.for_reference == target_reference:
            return self

        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        glossary_service = service_provider.glossary_service
        gram_term = glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", glossary_service.root_subterms["EOS_units"].access_group_uid)
        ]

        new_quantities_dict: Dict[UUID, ReferencelessQuantityPropWithAdditionalInfo] = {}
        for key, qty in self.quantities.items():
            value = self._convert_value_for_reference(qty.value, gram_term.uid, target_reference)
            new_quantities_dict[key] = ReferencelessQuantityPropWithAdditionalInfo(
                unit_term_uid=qty.unit_term_uid, value=value, examples=qty.examples, dfu_per_100g=qty.dfu_per_100g
            )
        return QuantityPackageProp(quantities=new_quantities_dict, for_reference=target_reference)

    def from_dict(self, data: dict) -> None:
        """Construct model from dict."""
        new_data = self.pre_init_modifications(data)
        self.for_reference = new_data["for_reference"]
        self.quantities = {
            key: (
                qty
                if isinstance(qty, ReferencelessQuantityPropWithAdditionalInfo)
                else ReferencelessQuantityPropWithAdditionalInfo(**qty)
            )
            for key, qty in new_data["quantities"].items()
        }
        if "prop_term_uid" in new_data:
            self.prop_term_uid = to_uuid(new_data["prop_term_uid"])
        if data.get("source"):
            self.source = data["source"]
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object):
        """Check for equality of two QuantityPackageProps."""
        if type(self) is type(other):
            if self.for_reference != other.for_reference:
                return False
            if self.prop_term_uid != other.prop_term_uid:
                return False
            if self.source != other.source:
                return False
            for qty_uid, qty in self.quantities.items():
                if qty_uid not in other.quantities:
                    return False
                elif qty != other.quantities[qty_uid]:
                    return False
            return True
        else:
            return False
