from typing import Any, Dict, Literal
from uuid import UUID

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass

from core.domain.prop import Prop


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class LinkToUidProp(Prop):
    prop_type: Literal["LinkToUidProp"] = Field(default="LinkToUidProp")
    uid: UUID = Field()
    duplicate_sub_node: bool = Field(default=True)

    def model_dump_extra(self, dump_dict: Dict[str, Any]) -> Dict[str, Any]:
        dump_dict["uid"] = str(self.uid)
        return dump_dict

    def __eq__(self, other: object) -> bool:
        if type(other) is type(self):
            return self.uid == other.uid and self.duplicate_sub_node == other.duplicate_sub_node
        return False
