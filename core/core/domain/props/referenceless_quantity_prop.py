"""Quantities (value & unit) without a reference. Should not be a part of Node class."""

from enum import Enum
from typing import Any, Dict, Literal, Optional, Set, Type, Union
from uuid import UUID

from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs

from core.domain.prop import Prop
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.term import Term


class ConvertibleUnits(str, Enum):
    mass = "mass"
    time = "time"
    energy = "energy"
    volume = "volume"
    electric_current = "current"
    length = "length"


UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING = {
    "EOS_units_mass": "mass-in-g",
    "EOS_units_time": "time-in-hour",
    "EOS_units_energy": "energy-in-kj",
    "EOS_units_volume": "volume-in-ml",
    "EOS_units_electric_current": "current-in-wh",
    "EOS_units_length": "length-in-m",
}

UNIT_TYPE_TERM_XID_TO_STRING = {
    "EOS_units_mass": ConvertibleUnits.mass.value,
    "EOS_units_time": ConvertibleUnits.time.value,
    "EOS_units_energy": ConvertibleUnits.energy.value,
    "EOS_units_volume": ConvertibleUnits.volume.value,
    "EOS_units_electric_current": ConvertibleUnits.electric_current.value,
    "EOS_units_length": ConvertibleUnits.length.value,
}


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class ReferencelessQuantityProp(Prop):
    """Property for quantities without a reference. Should not be a part of Node class."""

    prop_type: Literal["ReferencelessQuantityProp"] = Field(default="ReferencelessQuantityProp")

    value: float = Field()
    unit_term_uid: UUID = Field()

    @property
    def _props_to_remove_from_repr(self) -> Set[str]:
        """Returns a set of properties to be excluded from __repr__."""
        return {"_owner_node", "prop_type", "unit_term_uid", "unit_term_xid"}

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls: Type["ReferencelessQuantityProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        # these are included when serializing the model, but we only use term_uid to define the term to be used:
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values
        if "unit_term_xid" in values_kwargs:
            del values_kwargs["unit_term_xid"]
        if "unit_term_name" in values_kwargs:
            del values_kwargs["unit_term_name"]
        return values

    def model_dump_extra(self, dump_dict: Dict[str, Any], overwrite_unit_term: Optional[Term] = None) -> Dict[str, Any]:
        """Additional ReferencelessQuantityProp information is serialized into the dump dictionary."""
        if overwrite_unit_term:
            unit_term = overwrite_unit_term
        else:
            unit_term = self.get_unit_term()
        dump_dict["unit_term_uid"] = str(unit_term.uid)
        dump_dict["unit_term_xid"] = str(unit_term.xid)
        dump_dict["unit_term_name"] = str(unit_term.name)
        return dump_dict

    def get_unit_term(self) -> Term:
        """Obtain term associated with the unit."""
        return GlossaryTermProp.get_term_from_uid(self.unit_term_uid)

    @property
    def unit(self) -> str:
        return self.get_unit_term().name

    def unit_conversion_factor_and_new_unit(self, origin_unit: Term) -> tuple[float, Term]:
        try:
            requested_units: dict = self._owner_node.get_calculation().requested_units
        except AttributeError:
            return 1.0, origin_unit

        if not requested_units:
            return 1.0, origin_unit

        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        glossary_service = service_provider.glossary_service

        parent_of_origin_unit = glossary_service.terms_by_uid.get(origin_unit.sub_class_of)
        if (
            parent_of_origin_unit
            and UNIT_TYPE_TERM_XID_TO_STRING.get(parent_of_origin_unit.xid, "") in requested_units
            and UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING.get(parent_of_origin_unit.xid)
        ):
            requested_unit = requested_units[UNIT_TYPE_TERM_XID_TO_STRING.get(parent_of_origin_unit.xid, "")].value
            requested_unit_term = glossary_service.terms_by_xid_ag_uid[
                (f"EOS_{requested_unit}", origin_unit.access_group_uid)
            ]
            if requested_unit_term.sub_class_of == origin_unit.sub_class_of:
                return (
                    self.convert_between_same_unit_types(
                        1.0,
                        origin_unit,
                        requested_unit_term,
                        UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING.get(parent_of_origin_unit.xid),
                    ),
                    requested_unit_term,
                )
        return 1.0, origin_unit

    @staticmethod
    def convert_between_same_unit_types(
        origin_amount: float, origin_unit: Term, target_unit: Term, base_unit: str
    ) -> float:
        """Convert amount between two of the same unit types (e.g., mass units, energy units)."""
        amount_in_base_unit = origin_amount * origin_unit.data.get(base_unit)
        amount_in_target_unit = amount_in_base_unit / target_unit.data.get(base_unit)
        return amount_in_target_unit

    def _serialize_object(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Helper method to serialize object because super().model_dump_base() is not working."""
        origin_unit = self.get_unit_term()
        if origin_unit.data and any(v in origin_unit.data for v in UNIT_TYPE_TERM_XID_TO_CONVERSION_STRING.values()):
            unit_conversion_factor, new_unit = self.unit_conversion_factor_and_new_unit(origin_unit)
            output = {"value": self.value * unit_conversion_factor}
            self.model_dump_extra(output, new_unit)
        else:
            output = {"value": self.value}
            self.model_dump_extra(output)

        output["prop_type"] = self.prop_type
        if self.source_data_raw:
            output["source_data_raw"] = self.source_data_raw

        if exclude:
            for field in exclude:
                if field in output:
                    del output[field]
        return output

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        output = self._serialize_object(exclude)
        return output

    def referenceless_quantitiy_from_dict(self, data: dict) -> None:
        """Compose class from dict."""
        self.value = data["value"]
        if isinstance(data["unit_term_uid"], str):
            self.unit_term_uid = UUID(data["unit_term_uid"])
        else:
            self.unit_term_uid = data["unit_term_uid"]

    def from_dict(self, data: dict) -> None:
        """Wrapper to avoid super()."""
        self.referenceless_quantitiy_from_dict(data)
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]

    def __eq__(self, other: object):
        """Check for equality of two ReferencelessQuantityProp."""
        if type(other) is type(self):
            if self.unit_term_uid != other.unit_term_uid:
                return False
            if self.value != other.value:
                return False
            return True
        else:
            return False
