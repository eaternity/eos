"""Module containing property with origin data."""
from typing import Any, Dict, Literal, Optional, Type, cast

from pydantic import ConfigDict, Field, field_validator
from pydantic.dataclasses import dataclass
from structlog import get_logger

from core.domain.prop import Prop
from core.domain.props.glossary_term_prop import GlossaryTermProp

logger = get_logger()


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class OriginDataProp(Prop):
    """Property containing origin data information."""

    prop_type: Literal["OriginDataProp"] = Field(default="OriginDataProp")

    fao_code: Optional[GlossaryTermProp] = Field(default=None)
    stats_data_year_column: Optional[str] = Field(default=None)

    @field_validator("stats_data_year_column")
    @classmethod
    def validate_error_message(cls: Type["OriginDataProp"], value: str) -> str:
        """Validate language code of names."""
        year_integer = int(value)

        if year_integer >= 0:
            return value
        else:
            raise ValueError(f"{value} is not a valid year.")

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Serialize the entire model."""
        output = {"prop_type": self.prop_type}

        if self.fao_code:
            output["fao_code"] = self.fao_code.model_dump_base(exclude=exclude)

        for field_name in ("stats_data_year_column", "source_data_raw"):
            if getattr(self, field_name):
                output[field_name] = getattr(self, field_name)

        if exclude:
            for field in exclude:
                if field in output:
                    del output[field]
        return output

    def from_dict(self, data: Dict[str, Any]) -> None:
        """Fill in OriginDataProp using data dictionary."""
        if data.get("fao_code"):
            fao_code = data["fao_code"]
            if isinstance(fao_code, GlossaryTermProp):
                self.fao_code = fao_code
            else:
                self.fao_code = GlossaryTermProp(**fao_code)

        for field_name in ("stats_data_year_column", "source_data_raw"):
            if data.get(field_name):
                setattr(self, field_name, data[field_name])

    def __eq__(self, other: object) -> bool:
        """Compare other objects with OriginDataProp."""
        if type(self) is type(other):
            other_prop = cast(OriginDataProp, other)
            return (
                self.fao_code == other_prop.fao_code
                and self.stats_data_year_column == other_prop.stats_data_year_column
            )
        return False
