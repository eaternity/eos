"""Quantities (value & unit) without a reference and examples. Should not be a part of Node class."""

from typing import Any, Dict, List, Literal, Optional

from pydantic import ConfigDict, Field
from pydantic.dataclasses import dataclass

from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True, repr=False)
class ReferencelessQuantityPropWithAdditionalInfo(ReferencelessQuantityProp):
    """Property for quantities without a reference with examples. Should not be a part of Node class."""

    prop_type: Literal["ReferencelessQuantityPropWithAdditionalInfo"] = Field(
        default="ReferencelessQuantityPropWithAdditionalInfo"
    )

    examples: List[str] = Field(default_factory=list)
    dfu_per_100g: Optional[float] = Field(None)

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        """Additional ReferencelessQuantityPropWithAdditionalInfo information is serialized into the dump dictionary."""
        output = self._serialize_object(exclude)
        if self.examples:
            output["examples"] = list(self.examples)
        if self.dfu_per_100g is not None:
            output["dfu_per_100g"] = self.dfu_per_100g
        return output

    def from_dict(self, data: dict) -> None:
        """Wrapper to avoid super()."""
        self.referenceless_quantitiy_from_dict(data)
        if data.get("source_data_raw"):
            self.source_data_raw = data["source_data_raw"]
        if data.get("examples"):
            self.examples = data["examples"]
        if data.get("dfu_per_100g"):
            self.dfu_per_100g = data["dfu_per_100g"]

    def __eq__(self, other: object):
        """Check for equality of two ReferencelessQuantityPropWithAdditionalInfo."""
        if type(other) is type(self):
            if self.unit_term_uid != other.unit_term_uid:
                return False
            if self.value != other.value:
                return False
            if self.examples != other.examples:
                return False
            if self.dfu_per_100g != other.dfu_per_100g:
                return False
            return True
        else:
            return False
