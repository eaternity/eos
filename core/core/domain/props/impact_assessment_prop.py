"""Impact Assessment Property."""

from typing import Any, Dict, Literal, Optional, Type, Union

from pydantic import ConfigDict, Field, model_validator
from pydantic.dataclasses import dataclass
from pydantic_core import ArgsKwargs
from structlog import get_logger

from core.domain.props.quantity_package_prop import QuantityPackageProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.utils import (
    DFU_IMPROVEMENT_PERCENTAGE_THRESHOLD,
    calculate_percentage_difference,
    get_daily_food_unit,
    get_rating,
)
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID

logger = get_logger()


# Constants for CO2 emissions ratings in kgco2eq/dfu:
RATING_THRESHOLDS = {"A": 393.15 / 1000, "B": 786.29 / 1000, "C": 1667.0 / 1000, "D": 3033.00 / 1000}
AVERAGE_KG_CO2_PER_FOOD_UNIT = 778.7 / 1000


@dataclass(kw_only=True, config=ConfigDict(extra="forbid"), slots=True)
class ImpactAssessmentProp(QuantityPackageProp):
    """Quantity package property with multiple quantities (with a common reference)."""

    prop_type: Literal["ImpactAssessmentProp"] = Field(default="ImpactAssessmentProp")

    @model_validator(mode="before")
    @classmethod
    def pre_root(cls: Type["ImpactAssessmentProp"], values: Union[Dict, ArgsKwargs]) -> Union[Dict, ArgsKwargs]:
        values = QuantityPackageProp.pre_init_modifications(values)
        if isinstance(values, ArgsKwargs):
            values_kwargs = values.kwargs
        else:
            values_kwargs = values

        for key in (
            "co2_rating",
            "co2_award",
            "co2_value_improvement_percentage",
            "co2_value_reduction_value",
        ):
            if key in values_kwargs:
                del values_kwargs[key]
        return values

    def model_dump_base(self, exclude: Optional[set] = None) -> Dict[str, Any]:
        output = QuantityPackageProp.model_dump_base(self, exclude=exclude)
        try:
            requested_references = self._owner_node.get_calculation().requested_quantity_references
        except AttributeError:
            requested_references = []
        # TODO: I think in most other places we have amount_for_root_node as default, why not here?
        if not requested_references:
            requested_references = ["amount_for_activity_production_amount"]

        for reference in requested_references:
            co2_value_in_kg_for_reference = self.get_co2_value_in_kg(reference)
            if co2_value_in_kg_for_reference is None:
                continue

            dfu_value_for_reference = get_daily_food_unit(self.get_owner_node())
            if dfu_value_for_reference is not None:
                fu_value_for_reference = 5 * dfu_value_for_reference
            else:
                fu_value_for_reference = None

            # these quantities should not depend on the reference-value, so we can just take
            # the first reference
            if "co2_rating" not in output:
                output["co2_rating"] = self.get_co2_rating(co2_value_in_kg_for_reference, fu_value_for_reference)
            if "co2_value_improvement_percentage" not in output:
                output["co2_value_improvement_percentage"] = self.get_co2_value_improvement_percentage(
                    co2_value_in_kg_for_reference, fu_value_for_reference
                )
            if "co2_award" not in output:
                output["co2_award"] = output["co2_rating"] == "A"

            # the reduction value depends on the reference-value, so we need to calculate it for each reference
            if "co2_value_reduction_value" not in output:
                output["co2_value_reduction_value"] = {}
            output["co2_value_reduction_value"][
                reference.value if isinstance(reference, ReferenceAmountEnum) else reference
            ] = self.get_co2_value_reduction_value(co2_value_in_kg_for_reference, fu_value_for_reference)

        return output

    def get_co2_value_in_kg(
        self, reference: ReferenceAmountEnum = "amount_for_activity_production_amount"
    ) -> float | None:
        quantities_dict = self.amount_for_reference(reference)
        co2_value_in_kg = None

        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        service_provider = service_locator.service_provider
        glossary_service = service_provider.glossary_service
        for term_uid, value in quantities_dict.quantities.items():
            term = glossary_service.get_term_by_id(term_uid)
            if term.xid == IPCC_2013_GWP_100_XID:
                co2_value_in_kg = value.value
                break
        if co2_value_in_kg is None:
            return None

        return co2_value_in_kg

    @staticmethod
    def get_co2_rating(co2_value_in_kg: float, fu_value: float | None) -> str | None:
        if fu_value is None or fu_value < DFU_IMPROVEMENT_PERCENTAGE_THRESHOLD:
            return "E"
        return get_rating(co2_value_in_kg / fu_value, RATING_THRESHOLDS)

    @staticmethod
    def get_co2_value_improvement_percentage(co2_value_in_kg: float, fu_value: float | None) -> float | None:
        if fu_value is None:
            return None
        if fu_value < DFU_IMPROVEMENT_PERCENTAGE_THRESHOLD:
            return 0.0
        return calculate_percentage_difference(co2_value_in_kg / fu_value, AVERAGE_KG_CO2_PER_FOOD_UNIT)

    @staticmethod
    def get_co2_value_reduction_value(co2_value_in_kg: float, fu_value: float | None) -> float | None:
        if fu_value is None:
            return None

        return AVERAGE_KG_CO2_PER_FOOD_UNIT * fu_value - co2_value_in_kg

    def __eq__(self, other: object):
        """Check for equality of two ImpactAssessmentProps."""
        return QuantityPackageProp.__eq__(self, other)
