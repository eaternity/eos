from dataclasses import dataclass
from typing import Optional


@dataclass
class Namespace:
    name: str
    xid: str
    uid: Optional[str] = None
