import logging
from enum import Enum
from typing import Dict, List, Optional, Union
from uuid import UUID

from pydantic.dataclasses import dataclass

from core.orchestrator.exceptions import GapFillingModuleError


class LogLevel(int, Enum):
    NotSet = logging.NOTSET
    Debug = logging.DEBUG
    Info = logging.INFO
    Warning = logging.WARNING
    Error = logging.ERROR
    Critical = logging.CRITICAL


class ErrorClassification(str, Enum):
    unclassified = "unclassified"
    missing_matching = "missing_matching"
    missing_lca_inventory = "missing_lca_inventory"
    missing_sub_flows_in_client_sent_data = "missing_sub_flows_in_client_sent_data"
    missing_nutrients = "missing_nutrients"
    failed_amount_estimation = "failed_amount_estimation"
    fallback_without_amount_estimation = "fallback_without_amount_estimation"
    zero_sum_ingredient_amounts = "zero_sum_ingredient_amounts"
    fallback_fastest_transport = "fallback_fastest_transport"


@dataclass
class DataError:
    message: str
    gfm_name: str = "Unknown"
    level: LogLevel = LogLevel.Error
    node_uid: Optional[UUID] = None
    scheduler_iteration: int = -1
    traceback: Optional[str] = None
    error_classification: ErrorClassification = ErrorClassification.unclassified
    additional_specification: Optional[Dict[str, Optional[Union[str, List[str]]]]] = None

    def __str__(self):
        return f"{self.level}: {self.message}"

    @staticmethod
    def from_gfm_error(gfm_error: GapFillingModuleError) -> "DataError":
        "Build a DataError dataclass instance from GFM Exception."
        return DataError(
            f"{gfm_error}",
            scheduler_iteration=gfm_error.scheduler_iteration,
            node_uid=gfm_error.node_uid,
            gfm_name=gfm_error.gfm_name,
            traceback=gfm_error.traceback,
            error_classification=ErrorClassification.unclassified,
        )

    # TODO: remove this by where this is used to not verify a string but a state information
    def startswith(self, s: str) -> bool:
        return self.message.startswith(s)

    # TODO: remove this by where this is used to not verify a string but a state information
    def endswith(self, s: str) -> bool:
        return self.message.endswith(s)
