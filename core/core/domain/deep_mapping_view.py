from collections.abc import Mapping, Sequence
from typing import Hashable, List


class DeepMappingView(Mapping):
    def __init__(self, original_dict: dict):
        """This class acts as a proxy to a json dictionary.

        This class provides read-only view access to the original_dict and its
        nested json structures inside it. Accessing sub-json objects or sub-json arrays via this Proxy will return
        another read-only proxy to that sub-structure, such that mutations of nested data should not be allowed.

        When gap-filling-modules are reading from the calculation graph, they should only have view access via these
        proxy objects. All mutations of the calculation graph should be done using `core/graph_manager/mutations`.

        :param original_dict: dict is the original python dictionary that should be wrapped inside.
        """
        self._data: dict = original_dict

    def __len__(self):
        return len(self._data)

    def __getitem__(self, key: Hashable):
        if key in self._data:
            value = self._data[key]
            if isinstance(value, dict):
                # need to create a sub view of the child mapping:
                value = DeepMappingView(value)
            elif isinstance(value, list):
                # need to create a sub view of the child sequence:
                value = DeepListView(value)
            return value
        if hasattr(self.__class__, "__missing__"):
            return self.__class__.__missing__(self, key)
        raise KeyError(key)

    def __iter__(self):
        return iter(self._data)

    def __contains__(self, key: Hashable):
        return key in self._data

    def __repr__(self):
        return repr(self._data)


class DeepListView(Sequence):
    def __init__(self, original_list: list):
        """This class acts as a proxy to a json sub-field that is of type array.

        The proxy provides gap-filling-modules
        read-only view access to the `original_list` and it's nested json structures inside it. Accessing sub-json
        objects or sub-json arrays via this Proxy will return another read-only proxy to that sub-structure, such
        that mutations of nested data should not be allowed.

        When gap-filling-modules are reading from the calculation graph, they should only have view access via these
        proxy objects. All mutations of the calculation graph should be done using `core/graph_manager/mutations`.

        :param original_list: list is the original python list that should be wrapped inside.
        """
        self._data: list = original_list

    def __len__(self):
        return len(self._data)

    def __getitem__(self, i: slice | int):
        if isinstance(i, slice):
            return self.__class__(self._data[i])
        else:
            value = self._data[i]
            if isinstance(value, dict):
                # need to create a sub view of the child mapping:
                value = DeepMappingView(value)
            elif isinstance(value, list):
                # need to create a sub view of the child sequence:
                value = DeepListView(value)
            return value

    def __iter__(self):
        return iter(self._data)

    def __contains__(self, item: object):
        return item in self._data

    def __repr__(self):
        return repr(self._data)

    def __lt__(self, other: "DeepListView"):
        return self._data < self.__cast(other)

    def __le__(self, other: "DeepListView"):
        return self._data <= self.__cast(other)

    def __eq__(self, other: "DeepListView" | List):
        return self._data == self.__cast(other)

    def __gt__(self, other: "DeepListView"):
        return self._data > self.__cast(other)

    def __ge__(self, other: "DeepListView"):
        return self._data >= self.__cast(other)

    def __add__(self, other: "DeepListView"):
        return self.__class__(self._data + other._data)

    @staticmethod
    def __cast(other: "DeepListView" | List) -> list:
        return other._data if isinstance(other, DeepListView) else other

    def copy(self) -> "DeepListView":
        return self.__class__(self._data)

    def count(self, item: object) -> int:
        return self._data.count(item)


class DeepSetView:
    def __init__(self, original_set: set):
        self._set = set(original_set)

    def __contains__(self, item: object):
        return item in self._set

    def __iter__(self):
        return iter(self._set)

    def __len__(self):
        return len(self._set)

    def copy(self) -> "DeepSetView":
        return self.__class__(self._set.copy())

    def __repr__(self):
        return repr(self._set)


def undo_mapping_view(mapping_view: DeepMappingView | DeepListView) -> dict | list:
    if isinstance(mapping_view, DeepMappingView):
        new_dict = {}
        for key, val in mapping_view.items():
            if isinstance(val, (DeepMappingView, DeepListView)):
                new_dict[key] = undo_mapping_view(val)
            elif isinstance(val, DeepSetView):
                new_dict[key] = set(val)
            else:
                new_dict[key] = val
        return new_dict
    if isinstance(mapping_view, DeepListView):
        new_list = [
            undo_mapping_view(item)
            if isinstance(item, (DeepMappingView, DeepListView))
            else set(item)
            if isinstance(item, DeepSetView)
            else item
            for item in mapping_view
        ]
        return new_list
