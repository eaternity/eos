import uuid
from dataclasses import dataclass
from typing import Optional


@dataclass
class GlossaryLink:
    """
    Corresponds to the glossary_link db table (and also the glossary_grouping table).
    Links one or more terms (e.g. "dried" "pineapple") to either a (process) node or a (nutrition) term.
    """

    gap_filling_module: str
    term_uids: list[uuid.UUID]
    linked_term_uid: Optional[uuid.UUID] = None
    linked_node_uid: Optional[uuid.UUID] = None
    uid: Optional[uuid.UUID] = None

    def __post_init__(self):
        """Ensure that either linked_term_uid or linked_node_uid is set, but not both."""
        if self.linked_term_uid is None and self.linked_node_uid is None:
            raise ValueError("GlossaryLink must be linked to either a term or a node, but not both.")
        if self.linked_term_uid is not None and self.linked_node_uid is not None:
            raise ValueError("GlossaryLink must be linked to either a term or a node.")
