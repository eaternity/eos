from dataclasses import dataclass
from typing import Optional


@dataclass(slots=True)
class MatchingItem:
    """Corresponds to the matching db table and the matching_terms association table."""

    gap_filling_module: str

    uid: Optional[str] = None
    access_group_uid: Optional[str] = None
    lang: Optional[str] = None
    # The string that is matched against the user input; will be lowercased when inserted into the db
    matching_string: Optional[str] = None
    matched_node_uid: Optional[str] = None
    data: Optional[dict] = None

    term_uids: list[str] = None

    def as_dict(self) -> dict:
        return_dict = {
            "uid": self.uid,
            "gap_filling_module": self.gap_filling_module,
            "access_group_uid": self.access_group_uid,
            "lang": self.lang,
            "matching_string": self.matching_string,
            "matched_node_uid": self.matched_node_uid,
            "data": self.data,
        }
        if self.term_uids:
            return_dict["term_uids"] = [str(t) for t in self.term_uids]
        else:
            return_dict["term_uids"] = []
        return return_dict
