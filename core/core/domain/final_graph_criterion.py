from typing import List, Optional

from pydantic import BaseModel


class KeepCustomerSendData(BaseModel):
    """Keep only the data that is sent to the customer."""

    depth: int


class FinalGraphTableCriterion(BaseModel):
    """The criterion for the final graph table."""

    keep_customer_send_data: Optional[KeepCustomerSendData] = None
    keep_rows_with_fields: Optional[List[str]] = None  # if row has non-empty entry for this field, keep it
    # In the future we can implement more criteria here, e.g.:
    # keep_base_products: Optional[bool] = None
