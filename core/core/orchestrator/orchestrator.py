"Orchestrator."

import asyncio
import time
import traceback
from uuid import uuid4

from gap_filling_modules.abstract_gfm import GFM_STATE_PROP_NAME, GLOBAL_GFM_STATE_PROP_NAME, AbstractGapFillingWorker
from gap_filling_modules.abstract_util.enum import GapFillingWorkerStatusEnum, NodeGfmStateEnum
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.nodes.activity.food_processing_activity_node import FoodProcessingActivityNode
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props.gfm_state_prop import GfmStateProp
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.envprops import EnvProps
from core.graph_manager.abstract_graph_observer import AbstractGraphObserver
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.mutations.add_node_mutation import AddNodeMutation
from core.graph_manager.mutations.prop_mutation import PropMutation
from core.orchestrator.exceptions import GapFillingModuleError
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.service.glossary_service import GlossaryService

logger = get_logger()


class Orchestrator(AbstractGraphObserver):
    "Orchestrator."

    def __init__(
        self,
        calc_graph: CalcGraph,
        glossary_service: GlossaryService,
        gap_filling_module_loader: GapFillingModuleLoader,
        print_as_tree_between_gfm_runs: bool = False,
    ):
        super().__init__()

        self.calc_graph = calc_graph
        self.glossary_service = glossary_service
        self.gap_filling_module_loader = gap_filling_module_loader
        self.scheduled_gap_filling_modules: list[AbstractGapFillingWorker] = []
        self.scheduled_gap_filling_module_counts: dict[str, int] = {}
        self.perf_counter_ns_per_gfm: dict[str, int] = {}
        self.run_counts_per_gfm: dict[str, int] = {}
        self._new_nodes: list[Node] = []
        self.print_as_tree_between_gfm_runs = print_as_tree_between_gfm_runs  # useful for debugging

        # register the orchestrator to listen for 'notify_new_graph_node'
        self.calc_graph.add_graph_observer(self)

    async def run(self) -> None:
        max_scheduler_iters = 10_000_000  # prevents infinite loop

        logger.info("starting to run orchestrator...")
        start_orchestrator_ns = time.perf_counter_ns()

        if self.calc_graph.root_node_uid:
            await self.add_initial_nodes_from_root_node_uid()
        elif self.calc_graph.linked_sub_node_infos:
            await self.add_initial_nodes_from_linked_sub_node_infos()

        await self.add_gap_filling_workers_on_new_nodes()

        logger.debug("start scheduler and running gap filling modules...")
        counter = 0
        last_yield_time_ns = time.perf_counter_ns()
        env_props: EnvProps = EnvProps()
        while len(self.scheduled_gap_filling_modules):
            counter += 1

            if counter > max_scheduler_iters:
                logger.error("stopping orchestrator because max iterations reached...")
                break

            if counter % 50_000 == 0:
                logger.warn(
                    f"Long running calculation! Orchestrator scheduler iteration: {counter}. "
                    f"There are still {len(self.scheduled_gap_filling_modules)} gap filling modules scheduled. "
                    f"There are {len(self.calc_graph)} nodes in the graph."
                )

            mod = self.scheduled_gap_filling_modules.pop()
            logger.debug(f"run next gap filling module {mod}...")

            if mod.node.gfm_state.worker_states[mod.__class__.__name__] in (
                NodeGfmStateEnum.canceled.value,
                NodeGfmStateEnum.finished.value,
            ):
                await self.update_global_gfm_state_prop([mod], new_state=NodeGfmStateEnum.canceled)
                continue

            # TODO: for now convert generator to set to fix bugs where it is used multiple times with "in" checks:
            status = mod.can_run_now()

            if status == GapFillingWorkerStatusEnum.ready:
                try:
                    start_ns = time.perf_counter_ns()
                    await mod.run(self.calc_graph)
                    current_time_ns = time.perf_counter_ns()
                    dur_ns = current_time_ns - start_ns

                    if current_time_ns - last_yield_time_ns >= env_props.YIELD_INTERVAL_NS:
                        await asyncio.sleep(0)  # Yield control to allow other tasks to run
                        last_yield_time_ns = current_time_ns

                    if self.print_as_tree_between_gfm_runs:
                        logger.debug(f"after running the gap filling module: {mod.__class__.__name__}")
                        logger.debug(self.calc_graph.format_as_tree())

                    await self.update_gfm_stats(dur_ns, mod)
                    await self.update_gfm_state_prop_of_node([mod], new_state=NodeGfmStateEnum.finished)
                    await self.update_global_gfm_state_prop([mod], new_state=NodeGfmStateEnum.finished)
                except Exception as e:
                    logger.warn(
                        "error running gap filling module",
                        gfm_name=mod.__class__.__name__,
                        scheduler_iteration=counter,
                        node_uid=mod.node.uid,
                        error_msg=str(e),
                        traceback=traceback.format_exc(),
                    )
                    raise GapFillingModuleError(
                        gfm_name=mod.__class__.__name__,
                        scheduler_iteration=counter,
                        node_uid=mod.node.uid,
                        error_msg=str(e),
                        traceback=traceback.format_exc(),
                    ) from e
                await self.add_gap_filling_workers_on_new_nodes()
            elif status == GapFillingWorkerStatusEnum.reschedule:
                logger.debug(f"gap filling module cannot run now. Reschedule it for later: {mod.__class__.__name__}")

                mod.reschedule_counter += 1

                if mod.reschedule_counter > 2000:
                    logger.warn(
                        f"gap filling module was rescheduled more than 2000 times, "
                        f"cancelling it: {mod.__class__.__name__}"
                    )

                    continue

                if mod.reschedule_counter > 500 and mod.reschedule_counter % 500 == 0:
                    logger.warn(
                        f"gap filling module was rescheduled more than "
                        f"{mod.reschedule_counter} times: {mod.__class__.__name__}"
                    )

                # add to head of list. meaning: module will run last in the scheduled list
                self.scheduled_gap_filling_modules.insert(0, mod)
            elif status == GapFillingWorkerStatusEnum.cancel:
                logger.debug(f"gap filling module cancelled: {mod.__class__.__name__}")
                await self.update_gfm_state_prop_of_node([mod], new_state=NodeGfmStateEnum.canceled)
                await self.update_global_gfm_state_prop([mod], new_state=NodeGfmStateEnum.canceled)

        dur_orchestrator_sec = (time.perf_counter_ns() - start_orchestrator_ns) / 1e9
        logger.info("finished all gap filling modules in orchestrator. Duration [sec]: ", duration=dur_orchestrator_sec)

        perf_counter_s_per_gfm = {k: v / 1e9 for k, v in self.perf_counter_ns_per_gfm.items()}
        perf_counter_s_per_gfm = dict(sorted(perf_counter_s_per_gfm.items(), key=lambda item: item[1], reverse=True))
        logger.info("total runtime per gfm [sec]:", perf_counter_s_per_gfm=perf_counter_s_per_gfm)

        if dur_orchestrator_sec > 20.0:
            logger.warn("orchestrator took more than 20 seconds. Duration [sec]: ", duration=dur_orchestrator_sec)
            self.print_gfm_stats()
            self.print_node_stats()

    async def add_initial_nodes_from_root_node_uid(self) -> None:
        # start by loading root node from database:
        root_node = await self.calc_graph.service_provider.node_service.find_by_uid(
            self.calc_graph.root_node_uid, include_deleted_nodes=True
        )
        root_node.original_node_uid = LinkToUidProp(uid=root_node.uid)
        if isinstance(root_node, ActivityNode):
            raise ValueError("Root node must be FlowNode.")
        else:
            assert isinstance(root_node, FlowNode), "Root node must be FlowNode."
            assert not root_node.get_parent_nodes(), "Root node must not have parent nodes."
            initial_mutation = AddNodeMutation(
                created_by_module=Orchestrator.__name__,
                new_node=root_node,
                copy=True,
            )
            self.calc_graph.apply_mutation(initial_mutation)
            edges = await self.calc_graph.service_provider.node_service.get_sub_graph_by_uid(root_node.uid, max_depth=1)
            assert edges, "FlowNode has to contain children = an activity"
            child_of_root_node_uid = edges[0].child_uid

        # FIXME: workaround for FlowNode is root node, we also pass the child recipe nodes uid
        child_of_root_node = await self.calc_graph.service_provider.node_service.find_by_uid(
            child_of_root_node_uid, include_deleted_nodes=True
        )
        child_of_root_node.original_node_uid = LinkToUidProp(uid=child_of_root_node.uid)

        # Fill in calculation's child_of_root_node_uid if it is empty.
        if not self.calc_graph.calculation.child_of_root_node_uid:
            self.calc_graph.calculation.child_of_root_node_uid = child_of_root_node_uid

        assert isinstance(child_of_root_node, ActivityNode)
        # Remove fields if they are specified to be ignored in the access_group
        for node in [root_node, child_of_root_node]:
            assert isinstance(node, Node)
            if node.access_group_uid:
                access_group = await self.calc_graph.service_provider.access_group_service.get_access_group_by_uid(
                    node.access_group_uid
                )
                if access_group and access_group.data:
                    node.ignore_fields(access_group.data.get("ignore_fields"))

        initial_mutation = AddNodeMutation(
            created_by_module=Orchestrator.__name__,
            new_node=child_of_root_node,
            parent_node_uid=root_node.uid,
            copy=True,
        )
        self.calc_graph.apply_mutation(initial_mutation)

    async def add_initial_nodes_from_linked_sub_node_infos(self) -> None:
        # start by adding a dummy root node and a dummy child of activity node.
        root_node_uid = uuid4()
        child_of_root_node_uid = uuid4()
        root_mutation = AddNodeMutation(
            created_by_module=Orchestrator.__name__,
            new_node=FoodProductFlowNode(
                uid=root_node_uid, amount_in_original_source_unit={"value": 1.0, "unit": "production amount"}
            ),
            copy=False,
        )
        self.calc_graph.apply_mutation(root_mutation)

        child_of_root_mutation = AddNodeMutation(
            created_by_module=Orchestrator.__name__,
            new_node=FoodProcessingActivityNode(uid=child_of_root_node_uid),
            parent_node_uid=root_node_uid,
            copy=False,
        )
        self.calc_graph.apply_mutation(child_of_root_mutation)

        # Set calc_graph's and calculation's root_node and child_of_root_node uid.
        self.calc_graph.root_node_uid = root_node_uid
        self.calc_graph.calculation.root_node_uid = root_node_uid
        self.calc_graph.calculation.child_of_root_node_uid = child_of_root_node_uid

        linked_pre_and_post_calc_sub_nodes = await self.calc_graph.service_provider.node_service.find_many_by_uids(
            uids=[linked_sub_node_info.uid for linked_sub_node_info in self.calc_graph.linked_sub_node_infos],
            include_deleted_nodes=True,
            include_post_calc_data=True,
        )

        for linked_sub_node_info in self.calc_graph.linked_sub_node_infos:
            linked_pre_and_post_calc_sub_node = linked_pre_and_post_calc_sub_nodes.get(linked_sub_node_info.uid)
            if linked_pre_and_post_calc_sub_node is None:
                raise ValueError(f"No node exists in the database with uid={linked_sub_node_info.uid}.")

            if self.calc_graph.calculation.use_cached_final_root and linked_pre_and_post_calc_sub_node.post_calc:
                linked_sub_node = linked_pre_and_post_calc_sub_node.post_calc
                previous_gfm_state = linked_sub_node.gfm_state

                # Explicitly schedule AddClientNodesGapFillingWorker such that the node's children can be added.
                if previous_gfm_state:
                    gfm_state_dict = dict(previous_gfm_state.worker_states)
                    gfm_state_dict["AddClientNodesGapFillingWorker"] = NodeGfmStateEnum.scheduled
                    linked_sub_node.gfm_state = GfmStateProp.unvalidated_construct(worker_states=gfm_state_dict)
                else:
                    linked_sub_node.gfm_state = GfmStateProp.unvalidated_construct(
                        worker_states={"AddClientNodesGapFillingWorker": NodeGfmStateEnum.scheduled}
                    )
            else:
                linked_sub_node = linked_pre_and_post_calc_sub_node.pre_calc

            linked_sub_node.original_node_uid = LinkToUidProp(uid=linked_sub_node.uid)
            # Add a flow between the attached linked_sub_node and the child-of-root to scale by flow_amount.
            flow_node_uid = uuid4()
            self.calc_graph.apply_mutation(
                AddNodeMutation(
                    created_by_module=Orchestrator.__name__,
                    new_node=FoodProductFlowNode(
                        uid=flow_node_uid,
                        amount_in_original_source_unit=linked_sub_node_info.flow_amount,
                        original_node_uid=LinkToUidProp(
                            uid=linked_sub_node.uid
                        ),  # Add linked_sub_node.uid as original_node_uid to make it traceable.
                    ),
                    parent_node_uid=child_of_root_node_uid,
                    copy=False,
                )
            )
            if isinstance(linked_sub_node, FlowNode):
                # Add dummy activity node above linked_sub_node
                dummy_activity_node_uid = uuid4()

                self.calc_graph.apply_mutation(
                    AddNodeMutation(
                        created_by_module=Orchestrator.__name__,
                        new_node=FoodProcessingActivityNode(uid=dummy_activity_node_uid),
                        parent_node_uid=flow_node_uid,
                        copy=False,
                    )
                )
                parent_node_uid = dummy_activity_node_uid
            else:
                parent_node_uid = flow_node_uid

            self.calc_graph.apply_mutation(
                AddNodeMutation(
                    created_by_module=Orchestrator.__name__,
                    new_node=linked_sub_node,
                    parent_node_uid=parent_node_uid,
                    copy=True,
                )
            )
            added_linked_sub_node = self.calc_graph.get_node_by_uid(linked_sub_node_info.uid)
            if added_linked_sub_node.access_group_uid:
                access_group = await self.calc_graph.service_provider.access_group_service.get_access_group_by_uid(
                    added_linked_sub_node.access_group_uid
                )
                if access_group and access_group.data:
                    added_linked_sub_node.ignore_fields(access_group.data.get("ignore_fields"))

    async def update_gfm_stats(self, dur_ns: int, mod: AbstractGapFillingWorker) -> None:
        if mod.__class__.__name__ not in self.perf_counter_ns_per_gfm:
            self.perf_counter_ns_per_gfm[mod.__class__.__name__] = 0
        self.perf_counter_ns_per_gfm[mod.__class__.__name__] += dur_ns

        if mod.__class__.__name__ not in self.run_counts_per_gfm:
            self.run_counts_per_gfm[mod.__class__.__name__] = 0
        self.run_counts_per_gfm[mod.__class__.__name__] += 1

    def print_node_stats(self) -> None:
        "Print stats about nodes added by which gap filling module."
        nodes_add_by_stats = {}
        for _, node in self.calc_graph._nodes_by_uid.items():
            added_by = node.added_by
            if added_by not in nodes_add_by_stats:
                nodes_add_by_stats[added_by] = 0
            nodes_add_by_stats[added_by] += 1
        logger.warn("node stats added by gap filling module:", node_stats=nodes_add_by_stats)

    def print_gfm_stats(self) -> None:
        "Log stats at info level."
        logger.warn("run counts per gfm:", run_counts_per_gfm=self.run_counts_per_gfm)

        logger.warn(
            "total runtime per gfm [sec]:",
            perf_counter_ns_per_gfm={k: v / 1e9 for k, v in self.perf_counter_ns_per_gfm.items()},
        )

        logger.warn(
            "average runtime per gfm [ms]",
            avg_runtime_ms_per_gfm={
                k: self.perf_counter_ns_per_gfm[k] / 1e6 / v for k, v in self.run_counts_per_gfm.items()
            },
        )

    async def update_gfm_state_prop_of_node(
        self, gfm_workers: list[AbstractGapFillingWorker], new_state: NodeGfmStateEnum
    ) -> None:
        """Updates the gfm_state property of a node with the given gfm_name and new_state.

        Args:
        gfm_workers: the gap filling module workers of which the status should be updated
                     Requirement: all workers must have the same node
        new_state: NodeGfmStateEnum (i.e. "scheduled" or "finished" or "cancelled")
                   Requirement: "finished"/"cancelled" are only allowed if gfm's state was "scheduled" before.
        """
        gfm_names = [worker.__class__.__name__ for worker in gfm_workers]
        node = gfm_workers[0].node
        assert all([worker.node.uid == node.uid for worker in gfm_workers]), "all workers must have the same node"

        prev_gfm_state = node.gfm_state
        if prev_gfm_state:
            gfm_state_data = dict(prev_gfm_state.worker_states)
        else:
            gfm_state_data = {}

        if new_state == NodeGfmStateEnum.canceled or new_state == NodeGfmStateEnum.finished:
            # check if all gfm's were scheduled before:
            for gfm_name in gfm_names:
                assert (
                    gfm_state_data[gfm_name] == NodeGfmStateEnum.scheduled.value
                ), f"cannot set gfm_state to {new_state} because it was not scheduled before"

        for gfm_name in gfm_names:
            gfm_state_data[gfm_name] = new_state.value

        gfm_state_mutation = PropMutation(
            created_by_module=Orchestrator.__name__,
            node_uid=node.uid,
            prop_name=GFM_STATE_PROP_NAME,
            prop=GfmStateProp.unvalidated_construct(worker_states=gfm_state_data),
        )
        self.calc_graph.apply_mutation(gfm_state_mutation)

    async def update_global_gfm_state_prop(
        self, gfm_workers: list[AbstractGapFillingWorker], new_state: NodeGfmStateEnum
    ) -> None:
        """Updates the global gfm_state property of the root node with the given gfm_name and new_state.

        Args:
        gfm_workers: the gap filling module workers of which the status should be updated
                     Requirement: all workers must have the same node
        new_state: NodeGfmStateEnum (i.e. "scheduled" or "finished" or "cancelled")
                   Requirement: "finished"/"cancelled" are only allowed if gfm's state was "scheduled" before.
        """
        # Update the scheduled_gfm_counts and check if there is a global change of scheduled gfm types:
        gfm_names = [worker.__class__.__name__ for worker in gfm_workers]
        changed_global_state = False
        for gfm_name in gfm_names:
            if gfm_name not in self.scheduled_gap_filling_module_counts:
                # init counter of newly encountered gfm_name:
                self.scheduled_gap_filling_module_counts[gfm_name] = 0

            if new_state == NodeGfmStateEnum.scheduled:
                if self.scheduled_gap_filling_module_counts[gfm_name] == 0:
                    # counts changes from 0 to 1
                    changed_global_state = True
                self.scheduled_gap_filling_module_counts[gfm_name] += 1

            else:  # finished or cancelled
                self.scheduled_gap_filling_module_counts[gfm_name] -= 1
                if self.scheduled_gap_filling_module_counts[gfm_name] == 0:
                    # counts changed from 1 to 0
                    changed_global_state = True
                assert self.scheduled_gap_filling_module_counts[gfm_name] >= 0, "must not be negative"

        # Update global scheduling state in root node if there was a change:
        if changed_global_state:
            # recalculate the global scheduling state (i.e. for all gfm types if there are any globally scheduled):
            global_gfm_state_data = {}
            for gfm_name, count in self.scheduled_gap_filling_module_counts.items():
                if count > 0:
                    new_state = NodeGfmStateEnum.scheduled
                else:
                    new_state = NodeGfmStateEnum.finished
                global_gfm_state_data[gfm_name] = new_state.value

            # create the mutation on the root node:
            global_gfm_state_mutation = PropMutation(
                created_by_module=Orchestrator.__name__,
                node_uid=self.calc_graph.get_root_node().uid,
                prop_name=GLOBAL_GFM_STATE_PROP_NAME,
                prop=GfmStateProp.unvalidated_construct(worker_states=global_gfm_state_data),
            )
            self.calc_graph.apply_mutation(global_gfm_state_mutation)

            # Would it be better to remove global_gfm_state from the root_node and keep it only in calculation?
            if self.calc_graph.calculation:
                self.calc_graph.calculation.global_gfm_state = self.calc_graph.get_root_node().global_gfm_state
            else:
                self.calc_graph.calculation = Calculation(
                    global_gfm_state=self.calc_graph.get_root_node().global_gfm_state
                )

    def notify_new_graph_node(self, node: Node) -> None:
        self._new_nodes.append(node)

    async def add_gap_filling_workers_on_new_nodes(self) -> None:
        logger.debug(f"there are {len(self._new_nodes)} new nodes in the graph... scheduling gap filling workers...")

        # for each new node, we spawn gap filling workers:
        for node_view in self._new_nodes:
            logger.debug(f"spawning gap filling modules on new_node ({node_view})")

            preset_states = node_view.gfm_state
            preset_gfms_blacklist = []

            if preset_states:
                preset_states = dict(preset_states.worker_states)

                for module in preset_states.keys():
                    if preset_states[module] != NodeGfmStateEnum.scheduled:
                        preset_gfms_blacklist.append(module)

            newly_scheduled_gfms: list[AbstractGapFillingWorker] = []

            calculation = self.calc_graph.calculation
            gfms_for_which_to_save_most_shallow_nodes = (
                calculation.gfms_for_which_to_save_most_shallow_nodes if calculation else []
            )

            for gap_filling_factory in self.gap_filling_module_loader.initialized_modules:
                gap_filling_worker: AbstractGapFillingWorker = gap_filling_factory.spawn_worker(node_view)

                # if we have a list of GFMs that should run on this module and current GFM is not in it, we skip it
                if preset_gfms_blacklist and gap_filling_worker.__class__.__name__ in preset_gfms_blacklist:
                    continue

                # skip blacklisted gap filling modules:
                if calculation and gap_filling_worker.__class__.__name__ in calculation.gfms_to_skip:
                    continue

                if gap_filling_worker.should_be_scheduled() or (
                    preset_states
                    and preset_states.get(gap_filling_worker.__class__.__name__) == NodeGfmStateEnum.scheduled
                ):
                    # spawn gap filling modules on new node in graph:
                    self.scheduled_gap_filling_modules.append(gap_filling_worker)
                    newly_scheduled_gfms.append(gap_filling_worker)

                    # add node to set of nodes that can change dynamically for this gfm:
                    gfm_name = gap_filling_worker.__class__.__name__
                    if gfm_name in gfms_for_which_to_save_most_shallow_nodes:
                        self.calc_graph.nodes_that_can_change_dynamically_for_each_gfm.setdefault(gfm_name, set()).add(
                            node_view.uid
                        )

            if len(newly_scheduled_gfms) > 0:
                await self.update_gfm_state_prop_of_node(newly_scheduled_gfms, new_state=NodeGfmStateEnum.scheduled)
                await self.update_global_gfm_state_prop(newly_scheduled_gfms, new_state=NodeGfmStateEnum.scheduled)

        # clear list:
        self._new_nodes = []
