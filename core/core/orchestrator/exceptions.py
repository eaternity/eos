from uuid import UUID


class GapFillingModuleError(Exception):
    def __init__(self, gfm_name: str, scheduler_iteration: int, error_msg: str, node_uid: UUID, traceback: str):
        self.gfm_name = gfm_name
        self.scheduler_iteration = scheduler_iteration
        self.error_msg = error_msg
        self.node_uid = node_uid
        self.traceback = traceback

    def __str__(self):
        return (
            f"{self.gfm_name} failed on node {self.node_uid} at scheduler iteration {self.scheduler_iteration}"
            f" with error: {self.error_msg} \n\n {self.traceback}"
        )
