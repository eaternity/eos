from pydantic_settings import BaseSettings, SettingsConfigDict


class EnvProps(BaseSettings):
    LOG_LEVEL: str = "WARNING"
    RABBITMQ_DEFAULT_USER: str = ""
    RABBITMQ_DEFAULT_PASS: str = ""
    GCP_KEY: str = ""
    GADM_CACHE_URL: str = ""
    GDRIVE_GADM_FOLDER_ID: str = ""
    GDRIVE_FAO_FOLDER_ID: str = ""
    GDRIVE_SERVICE_ACCOUNT_FILE: str = "./secrets/service_account.json"
    PRODUCTMATCHINGS_COLUMN_NAME: str = "ID Brightway (from Brightway Datasets)"
    MAX_CONCURRENT_ORCHESTRATIONS: int = 3
    # used for seeding initial namespace for inventory nodes:
    SEED_SUPERUSER_PASSWORD: str = "EATERNITY_PASSWORD"
    EATERNITY_TOKEN: str = "EATERNITY_TOKEN"  # this is NOT base64 encoded
    EATERNITY_NAMESPACE_UUID: str = "8590530c-4e69-57c6-52b0-18eee6dbea56"
    EATERNITY_NAMESPACE_XID: str = "eaternity"
    EATERNITY_NAMESPACE_NAME: str = "eaternity"
    ROOT_GLOSSARY_TERM_UUID: str = "ROOT_GLOSSARY_TERM_UUID"

    ECOTRANSIT_CALCULATION_METHOD: str = ""
    ECOTRANSIT_CALCULATION_YEAR: str = ""
    ECOTRANSIT_CUSTOMER: str = ""
    ECOTRANSIT_PASSWORD: str = ""
    ECOTRANSIT_WSDL_URL: str = ""

    AIRTABLE_API_KEY: str = ""
    AIRTABLE_BASE_ID: str = ""
    AIRTABLE_TABLE_ID: str = ""
    AIRTABLE_VIEW_NAME: str = ""
    AIRTABLE_MATCHING_TABLE_ID: str = ""
    GITHUB_SSH_KEY: str = ""

    POSTGRES_PASSWORD: str = ""
    POSTGRES_USER: str = "eosuser"
    POSTGRES_DB: str = "eos"
    POSTGRES_HOST: str = "localhost"
    POSTGRES_PORT: str = "5444"
    POSTGRES_SCHEMA: str = "public"
    POSTGRES_CONNECTIONS: int = 5

    PYDANTIC_VALIDATE_ASSIGNMENT: bool = False
    VERIFY_NODE_READ_ONLY: bool = True
    HIGH_MEMORY_WATERMARK_MIB: float = 8000.0
    LOW_MEMORY_WATERMARK_MIB: float = 6000.0
    YIELD_INTERVAL_NS: int = 50e6  # 50ms
    LOAD_CACHES_FROM_RUNNING_PODS: bool = False
    SUBSCRIBE_CACHE_INVALIDATIONS: bool = False

    RUN_INGREDIENT_SPLITTER: bool = True
    UNMATCH_BASED_ON_INGREDIENTS_DECLARATION: bool = True

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8", extra="allow")
