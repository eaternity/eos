import typing
from dataclasses import dataclass, field
from typing import Dict, List, Optional, Type, Union
from uuid import UUID

import numpy as np
from structlog import get_logger

from core.domain.calculation import Calculation, LinkedSubNodeInfo
from core.domain.data_error import DataError, ErrorClassification
from core.domain.nodes import ElementaryResourceEmissionNode, ModeledActivityNode
from core.domain.nodes.node import Node
from core.graph_manager.abstract_graph_observer import AbstractGraphObserver
from core.graph_manager.mutations.abstract_mutation import AbstractMutation
from core.service.glossary_service import GlossaryService

if typing.TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

from database.postgres.pg_term_mgr import IPCC_2013_GWP_100

logger = get_logger()


@dataclass
class MetaDataForMostShallowNodes:
    """Stores meta data for the most shallow nodes in the graph."""

    is_most_shallow_node_for_gfms: list[str] = field(default_factory=lambda: [])
    has_sub_nodes_that_can_change_dynamically_for_gfms: dict[str, bool] = field(default_factory=lambda: {})


class CalcGraph:
    """Encapsulates Nodes and Edges to give outside a read-only-view to these.

    Mutations should go through a central interface and logged.
    """

    def __init__(
        self,
        service_provider: "ServiceProvider",
        glossary_service: GlossaryService,
        root_node_uid: Optional[UUID] = None,
        linked_sub_node_infos: Optional[List[LinkedSubNodeInfo]] = None,
        calculation: Optional[Calculation] = None,
    ) -> None:
        """Initialize the CalcGraph.

        The calculation can either be initialized by defining the
        root_node_uid or by defining a set of linked_sub_node_infos. In the latter case, a
        dummy root flow and child of root activity are added.

        Args:
            service_provider: service provider
            glossary_service: glossary service
            root_node_uid: uid of the root node
            linked_sub_node_infos: LinkedSubNodeInfo of nodes to be added as link_to_sub_node
            of sub_flows of child_of_root_node.
            calculation: optional calculation object
        """
        self.service_provider = service_provider  # Service provider for various services.
        self.root_node_uid: Optional[UUID] = root_node_uid  # UID of the root node.
        self.linked_sub_node_infos: Optional[
            List[LinkedSubNodeInfo]
        ] = linked_sub_node_infos  # UIDs of the link_to_sub_nodes.
        if self.root_node_uid is None and not self.linked_sub_node_infos:
            raise ValueError("One from root_node_uid and linked_sub_node_infos has to be set.")

        if self.root_node_uid is not None and self.linked_sub_node_infos:
            raise ValueError(
                "Only one of root_node_uid or linked_sub_node_infos should be set."
                f"Received root_node_uid={self.root_node_uid} and linked_sub_node_infos={self.linked_sub_node_infos}"
            )

        self.glossary_service = glossary_service  # Glossary service for term-related operations.
        self.calculation = calculation  # Optional calculation object to make calculation parameters accessible.
        if not self.calculation:
            # Initialize empty Calculation if it doesn't already exist.
            self.calculation = Calculation(root_node_uid=self.root_node_uid)

        self._nodes_by_uid: dict[int, Node] = {}  # Dictionary to store nodes by int representation of UUID.

        # all the nodes that might change dynamically in the future for each gfm
        self.nodes_that_can_change_dynamically_for_each_gfm: dict[str, set[UUID]] = {}

        # all the meta data for the most shallow nodes
        self.meta_data_for_most_shallow_nodes: dict[UUID, MetaDataForMostShallowNodes] = {}

        # nodes for which we want to calculate the supply
        self.nodes_to_calculate_supply: set[UUID] = set()

        # set of environmental flows required for the requested impact_assessments
        self.required_environmental_flows: set[UUID] = set()

        self._mutation_log: list[AbstractMutation] = []  # List to log mutations.
        self._graph_observers: list[AbstractGraphObserver] = []  # List of graph observers.
        self.data_errors_log: list[DataError] = []  # List to log data errors.

    # TODO: unused for now
    # async def load_node_from_db(self, uid: UUID) -> None:
    #     node = await self.pg_graph_mgr.find_by_uid(uid)
    #     self.add_node(node)

    def get_root_node(self) -> Optional[Node]:
        return self.get_node_by_uid(self.root_node_uid)

    def child_of_root_node(self) -> Optional[Node]:
        # FIXME better name, or remove that getter method when
        # root-node as flow node fully works
        root_node = self.get_root_node()
        assert root_node
        return root_node.get_sub_nodes()[0]

    def get_node_by_uid(self, uid: UUID) -> Optional[Node]:
        # Changed from type(uid) is UUID to isinstance(uid, UUID) because the uid returned from the database has
        # the type asyncpg.asyncpg.pgproto.pgproto.UUID.
        if not isinstance(uid, UUID):
            raise ValueError("uid must be of type UUID")
        return self._nodes_by_uid.get(uid.int)

    def get_node_uids(self) -> list[UUID]:
        return [UUID(int=uid_int) for uid_int in self._nodes_by_uid.keys()]

    def __len__(self):
        return len(self._nodes_by_uid)

    def get_mutation_log(self) -> [AbstractMutation]:
        return self._mutation_log

    def append_data_errors_log_entry(
        self,
        log: str,
        error_classification: ErrorClassification = ErrorClassification.unclassified,
        additional_specification: Optional[Dict[str, Optional[Union[str, List[str]]]]] = None,
        node_uid: Optional[UUID] = None,
    ) -> None:
        self.data_errors_log.append(
            DataError(
                log,
                error_classification=error_classification,
                additional_specification=additional_specification,
                node_uid=node_uid,
            )
        )

    def add_node(self, node: Node) -> None:
        node.set_calculation(self.calculation)

        # TODO: need to properly implement this use case..
        if node.uid.int in self._nodes_by_uid:
            raise ValueError("node with that uid already exists in the graph.")

        self._nodes_by_uid[node.uid.int] = node
        for obs in self._graph_observers:
            obs.notify_new_graph_node(node)

    def add_edge(self, parent_node_uid: UUID, child_node_uid: UUID) -> None:
        child_node = self._nodes_by_uid[child_node_uid.int]
        parent_node = self._nodes_by_uid[parent_node_uid.int]
        parent_node.add_sub_node(child_node)
        child_node.add_parent_node(parent_node)

    def add_graph_observer(self, graph_observer: AbstractGraphObserver) -> None:
        self._graph_observers.append(graph_observer)

    def remove_edge(self, parent_node_uid: UUID, child_node_uid: UUID) -> None:
        child_node = self._nodes_by_uid[child_node_uid.int]
        parent_node = self._nodes_by_uid[parent_node_uid.int]
        self._nodes_by_uid[parent_node_uid.int].remove_sub_node(child_node)
        self._nodes_by_uid[child_node_uid.int].remove_parent_node(parent_node)

    def _get_cutoff_graph(
        self, stop_at_node_types: set[Type[Node]], max_depth: Optional[int] = None
    ) -> dict[int, Node]:
        root_node = self.get_root_node()

        # special case (just in case filter directly filters out the root node:
        if isinstance(root_node, tuple(stop_at_node_types)):
            return {}

        # initialize the output_nodes dictionary (containing node and tree-index) with the root node
        output_nodes: dict[int, (Node, int)] = {root_node.uid.int: (root_node, "")}
        # initialize the shortest_path_lengths dictionary with the root node
        shortest_path_lengths: dict[int, int] = {root_node.uid.int: 0}
        # initialize the stack with the root node
        stack: list[Node] = [root_node]

        # graph traversal using node stack:
        while len(stack) > 0:
            current_node = stack.pop()
            depth_of_current_node = shortest_path_lengths[current_node.uid.int]

            # only traverse further if we have not reached the max_depth:
            if max_depth is None or depth_of_current_node < max_depth:
                sub_nodes = current_node.get_sub_nodes()

                def _get_declaration_index_value(node: Node) -> float:
                    if hasattr(node, "declaration_index"):
                        declaration_index = node.declaration_index
                        if declaration_index:
                            return declaration_index.value
                    return float("inf")  # Append nodes with no declaration index at the end

                sorted_sub_nodes: List[Node] = sorted(sub_nodes, key=_get_declaration_index_value)

                # iterate over the sub_nodes:
                for index, sub_node in enumerate(sorted_sub_nodes):
                    # don't do anything if hitting stopping condition:
                    if isinstance(sub_node, tuple(stop_at_node_types)):
                        continue

                    # if the sub_node was not yet processed before:
                    if sub_node.uid.int not in output_nodes:
                        # if current path for arriving at the child node is shorter than the stored shortest path
                        # length, update the shortest path length for this child and add the child node to the
                        # nodes_to_visit list:
                        if depth_of_current_node + 1 < shortest_path_lengths.get(sub_node.uid.int, np.inf):
                            shortest_path_lengths[sub_node.uid.int] = depth_of_current_node + 1
                            parent_tree_index = output_nodes[current_node.uid.int][1]
                            new_tree_index = parent_tree_index + "." + str(index + 1)
                            output_nodes[sub_node.uid.int] = (sub_node, new_tree_index)
                            # for further processing add it to stack:
                            stack.append(sub_node)

        return output_nodes

    async def get_serialized_graph(self, exclude_node_props: List[str], max_depth: Optional[int] = None) -> [dict]:
        """Filter the graph and return a list of all serialized nodes."""
        node_list_by_uid = self._get_cutoff_graph(
            stop_at_node_types={
                ModeledActivityNode,
                ElementaryResourceEmissionNode,
            },
            max_depth=max_depth,
        )
        serialized_graph = []
        for node, tree_index in node_list_by_uid.values():
            node_dict = node.model_dump(mode="json", exclude_none=True, exclude=set(exclude_node_props))
            node_dict["sub_node_uids"] = [str(child_node.uid) for child_node in node.get_sub_nodes()]
            node_dict["tree_index"] = tree_index
            serialized_graph.append(node_dict)
        return serialized_graph

    def get_requested_impact_assessments(self) -> list[str]:
        if self.calculation:
            requested_impact_assessments = self.calculation.requested_impact_assessments
            if not requested_impact_assessments:
                requested_impact_assessments = [IPCC_2013_GWP_100]
        else:
            requested_impact_assessments = [IPCC_2013_GWP_100]
        return requested_impact_assessments

    def apply_mutation(self, mutation: AbstractMutation) -> None:
        mutation.apply(self)
        self._mutation_log.append(mutation)

    def format_as_tree(self) -> str:
        """A tree representation of the graph. Useful for debugging."""
        return f"CalcGraph: {len(self._nodes_by_uid)} nodes: \n " + self.get_root_node().format_as_tree()
