import uuid
from dataclasses import dataclass, field
from typing import TYPE_CHECKING, Optional
from uuid import UUID

from gap_filling_modules.abstract_util.enum import NodeGfmStateEnum
from structlog import get_logger

from core.domain.nodes import ElementaryResourceEmissionNode, ModeledActivityNode
from core.domain.nodes.node import Node
from core.domain.props.gfm_state_prop import GfmStateProp
from core.graph_manager.mutations.abstract_mutation import AbstractMutation

if TYPE_CHECKING:
    from core.graph_manager.calc_graph import CalcGraph

logger = get_logger()


@dataclass(kw_only=True)
class DuplicateNodeMutation(AbstractMutation):
    """Mutation that adds a new node (and corresponding edge) to the graph."""

    duplicate_child_nodes: bool
    gfms_to_not_schedule: tuple = None
    parent_node_uid: Optional[UUID]
    source_node_uid: Optional[UUID]
    new_node_uid: UUID
    source_uid_to_duplicate_uid_mapping: dict = field(default_factory=dict)

    @staticmethod
    def shallow_copy_node(node: Node) -> Node:
        """Copy node with its attributes.

        If the attribute is a Prop, it has to be "copied" such that the _owner_node field can be set to duplicated node.
        """
        kwargs = node.shallow_model_dump()

        copied_model = node.__class__.model_construct(**kwargs)
        return copied_model

    def apply(self, calc_graph: "CalcGraph") -> None:
        """Apply the mutation to the calc_graph."""
        source_node = calc_graph.get_node_by_uid(self.source_node_uid)
        assert source_node, f"Could not find node {self.source_node_uid} in this calc_graph, cannot apply PropMutation."

        new_node = self._duplicate_node(source_node)

        new_node.added_by = self.created_by_module
        calc_graph.add_node(new_node)

        if self.parent_node_uid:
            calc_graph.add_edge(self.parent_node_uid, self.new_node_uid)

        # if we need to duplicate the whole subtree, we create dupes for those nodes as well
        if self.duplicate_child_nodes:
            self._recursive_child_node_duplication(source_node, new_node, calc_graph)

    @staticmethod
    def deserialize_attributes(data: dict) -> dict:
        """Deserialize all non-trivial class attributes."""
        data = super(DuplicateNodeMutation, DuplicateNodeMutation).deserialize_attributes(data)
        if data.get("parent_node_uid"):
            data["parent_node_uid"] = UUID(data["parent_node_uid"])
        if data.get("source_node_uid"):
            data["source_node_uid"] = UUID(data["source_node_uid"])
        data["new_node_uid"] = UUID(data["new_node_uid"])
        return data

    def _duplicate_node(self, source_node: Node) -> Node:
        new_node = self.shallow_copy_node(source_node)

        # Delete node's associated graph structure.
        new_node.uid = self.new_node_uid
        new_node.remove_private_attributes()
        new_node.set_owner_node_for_props()

        # We need to modify the gfm_state, which is done in retrieve_source_node_dict:
        self.modify_gfm_state(new_node=new_node)
        return new_node

    def modify_gfm_state(self, new_node: Node, gfms_to_reschedule: Optional[list] = None) -> None:
        if self.gfms_to_not_schedule or gfms_to_reschedule:
            if new_node.gfm_state:
                current_gfm_state = dict(new_node.gfm_state.worker_states).copy()
            else:
                current_gfm_state = {}
        else:
            return

        if self.gfms_to_not_schedule:
            for gfm in self.gfms_to_not_schedule:
                if gfm in current_gfm_state.keys():
                    current_gfm_state[gfm] = NodeGfmStateEnum.finished.value
                else:
                    current_gfm_state[gfm] = NodeGfmStateEnum.canceled.value

        if gfms_to_reschedule:
            for gfm in gfms_to_reschedule:
                current_gfm_state[gfm] = NodeGfmStateEnum.scheduled.value

        new_node.gfm_state = GfmStateProp.unvalidated_construct(worker_states=current_gfm_state)

    def _recursive_child_node_duplication(
        self, source_current_level_node: Node, new_current_level_node: Node, calc_graph: "CalcGraph"
    ) -> None:
        source_current_level_sub_nodes = source_current_level_node.get_sub_nodes()

        for source_sub_node in source_current_level_sub_nodes:
            if isinstance(source_sub_node, ModeledActivityNode):
                if not isinstance(calc_graph.get_node_by_uid(self.source_node_uid), ModeledActivityNode):
                    # we don't want to duplicate the modeled-activity-node but we still want to have the edge
                    calc_graph.add_edge(new_current_level_node.uid, source_sub_node.uid)
                    continue

            if isinstance(source_sub_node, ElementaryResourceEmissionNode):
                predefined_mapping = str(source_sub_node.uid)  # Keep node uids for Emission nodes.
            else:
                predefined_mapping = self.source_uid_to_duplicate_uid_mapping.get(str(source_sub_node.uid))

            if predefined_mapping:
                new_sub_node_uid = UUID(predefined_mapping)
            else:
                new_sub_node_uid = uuid.uuid4()

            if not predefined_mapping:
                self.source_uid_to_duplicate_uid_mapping[str(source_sub_node.uid)] = str(new_sub_node_uid)

            new_sub_node = self.shallow_copy_node(source_sub_node)

            # Delete node's associated graph structure.
            new_sub_node.uid = new_sub_node_uid
            new_sub_node.remove_private_attributes()
            new_sub_node.set_owner_node_for_props()

            subs_of_source_sub_node = source_sub_node.get_sub_nodes()
            schedule_link_term_to_activity = False
            for sub in subs_of_source_sub_node:
                if isinstance(sub, ModeledActivityNode):
                    schedule_link_term_to_activity = True

            if schedule_link_term_to_activity:
                self.modify_gfm_state(new_sub_node, gfms_to_reschedule=["LinkTermToActivityNodeGapFillingWorker"])
            else:
                self.modify_gfm_state(new_sub_node)

            if not calc_graph.get_node_by_uid(new_sub_node_uid):
                new_sub_node.added_by = self.created_by_module
                calc_graph.add_node(new_sub_node)

            if subs_of_source_sub_node:
                self._recursive_child_node_duplication(source_sub_node, new_sub_node, calc_graph)

            calc_graph.add_edge(new_current_level_node.uid, new_sub_node.uid)
