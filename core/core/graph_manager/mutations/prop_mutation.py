from dataclasses import dataclass
from typing import TYPE_CHECKING
from uuid import UUID

from structlog import get_logger

from core.domain.prop import Prop
from core.domain.props import prop_from_dict
from core.graph_manager.mutations.abstract_mutation import AbstractMutation

if TYPE_CHECKING:
    from core.graph_manager.calc_graph import CalcGraph


logger = get_logger()


@dataclass(kw_only=True)
class PropMutation(AbstractMutation):
    """Mutation that modifies a prop on a node."""

    node_uid: UUID
    prop_name: str
    prop: Prop

    def apply(self, calc_graph: "CalcGraph") -> None:
        """Apply the mutation to the calc_graph."""
        node = calc_graph.get_node_by_uid(self.node_uid)
        assert node, f"Could not find node {self.node_uid} in this calc_graph, cannot apply PropMutation."
        assert self.prop.get_owner_node() is None, f"prop {self.prop} should not already have associated _owner_node."

        self.prop.make_read_only()  # Lock prop from being modified
        node.__apply_mutation__(self.prop_name, self.prop)

    def serialize_attributes(self, data: dict) -> dict:
        """Serialize all non-trivial class attributes."""
        data = super().serialize_attributes(data)

        # Set owner node as None temporarily for serialization so that the "pure" Prop without _owner_node
        # information is serialized.
        saved_owner_node = data["prop"].get_owner_node()
        data["prop"].set_owner_node(None)

        data["prop"] = data["prop"].model_dump(exclude={"_owner_node"})

        # Reset owner node as saved_owner_node.
        self.prop.set_owner_node(saved_owner_node)
        return data

    @staticmethod
    def deserialize_attributes(data: dict) -> dict:
        """Deserialize all non-trivial class attributes."""
        data = super(PropMutation, PropMutation).deserialize_attributes(data)
        data["node_uid"] = UUID(data["node_uid"])
        data["prop"] = prop_from_dict(data["prop"])
        return data
