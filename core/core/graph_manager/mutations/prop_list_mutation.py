from dataclasses import dataclass
from typing import TYPE_CHECKING
from uuid import UUID

from structlog import get_logger

from core.domain.prop import Prop
from core.domain.props import prop_from_dict
from core.graph_manager.mutations.abstract_mutation import AbstractMutation

if TYPE_CHECKING:
    from core.graph_manager.calc_graph import CalcGraph


logger = get_logger()


@dataclass(kw_only=True)
class PropListMutation(AbstractMutation):
    """Mutation that modifies a list of props on a node."""

    node_uid: UUID
    prop_name: str
    props: list[Prop]
    append: bool

    def apply(self, calc_graph: "CalcGraph") -> None:
        """Apply the mutation to the calc_graph."""
        node = calc_graph.get_node_by_uid(self.node_uid)
        assert node, f"Could not find node {self.node_uid} in this calc_graph, cannot apply PropMutation."

        for prop in self.props:
            if isinstance(prop, Prop):
                prop.make_read_only()  # Lock prop from being modified
                assert prop.get_owner_node() is None, f"prop {prop} should not already have associated _owner_node."

        if self.append:
            if self.prop_name not in node.model_fields_set:
                node.__apply_mutation__(self.prop_name, self.props)
            else:
                node.__apply_mutation__(
                    self.prop_name, [old_prop for old_prop in getattr(node, self.prop_name)] + self.props
                )
        else:
            node.__apply_mutation__(self.prop_name, self.props)

    def serialize_attributes(self, data: dict) -> dict:
        """Serialize all non-trivial class attributes."""
        data = super().serialize_attributes(data)

        # Set owner node as None temporarily for serialization so that the "pure" Prop without _owner_node
        # information is serialized.
        saved_owner_nodes = [p.get_owner_node() for p in data["props"]]
        for p in data["props"]:
            p.set_owner_node(None)

        data["props"] = [p.model_dump(exclude={"_owner_node"}) for p in data["props"]]

        # Reset owner node as saved_owner_nodes.
        for p, saved_owner_node in zip(self.props, saved_owner_nodes):
            p.set_owner_node(saved_owner_node)
        return data

    @staticmethod
    def deserialize_attributes(data: dict) -> dict:
        """Deserialize all non-trivial class attributes."""
        data = super(PropListMutation, PropListMutation).deserialize_attributes(data)
        data["node_uid"] = UUID(data["node_uid"])
        data["props"] = [prop_from_dict(p) for p in data["props"]]
        return data
