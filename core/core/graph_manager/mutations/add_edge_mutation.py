from dataclasses import dataclass
from typing import TYPE_CHECKING
from uuid import UUID

from core.graph_manager.mutations.abstract_mutation import AbstractMutation

if TYPE_CHECKING:
    from core.graph_manager.calc_graph import CalcGraph


@dataclass(kw_only=True)
class AddEdgeMutation(AbstractMutation):
    """Add an edge between two nodes."""

    from_node_uid: UUID
    to_node_uid: UUID

    def apply(self, calc_graph: "CalcGraph") -> None:
        """Apply the mutation to the calc_graph."""
        calc_graph.add_edge(self.from_node_uid, self.to_node_uid)

    @staticmethod
    def deserialize_attributes(data: dict) -> dict:
        """Deserialize all non-trivial class attributes."""
        data = super(AddEdgeMutation, AddEdgeMutation).deserialize_attributes(data)
        data["from_node_uid"] = UUID(data["from_node_uid"])
        data["to_node_uid"] = UUID(data["to_node_uid"])
        return data
