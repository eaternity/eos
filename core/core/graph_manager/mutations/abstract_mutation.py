from dataclasses import dataclass
from typing import TYPE_CHECKING, Optional
from uuid import UUID

from pydantic import model_serializer

if TYPE_CHECKING:
    from core.graph_manager.calc_graph import CalcGraph


@dataclass(kw_only=True)
class AbstractMutation:
    """Abstract class for all mutations."""

    created_by_module: Optional[str] = None

    def apply(self, calc_graph: "CalcGraph") -> None:
        """Apply the mutation to the calc_graph."""
        pass

    def serialize_attributes(self, data: dict) -> dict:
        """Serialize all non-trivial class attributes."""
        for k, v in data.items():
            if isinstance(v, UUID):
                data[k] = str(v)
        return data

    @staticmethod
    def deserialize_attributes(data: dict) -> dict:
        """Deserialize all non-trivial class attributes."""
        return data

    @model_serializer(mode="plain")
    def as_dict(self) -> dict:
        """Serialize the mutation to a dictionary."""
        data = self.__dict__.copy()
        data["mutation_type"] = self.__class__.__name__
        data.update(self.serialize_attributes(data))
        return data

    @classmethod
    def from_dict(cls: "AbstractMutation", data: dict) -> "AbstractMutation":
        """Deserialize the mutation from a dictionary."""
        data = data.copy()
        del data["mutation_type"]
        data = cls.deserialize_attributes(data)
        return cls(**data)
