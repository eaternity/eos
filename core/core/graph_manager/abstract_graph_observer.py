from structlog import get_logger

from core.domain.nodes.node import Node

logger = get_logger()


class AbstractGraphObserver:
    def __init__(self):
        pass

    def notify_new_graph_node(self, node: Node):
        pass
