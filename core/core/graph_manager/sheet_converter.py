"""Sheet converter for Kale exports. Currently used mostly for Valora exports."""

import copy
from typing import List, Optional

from structlog import get_logger

from core.domain.calculation import FinalGraphTableCriterion
from core.domain.deep_mapping_view import DeepListView
from core.domain.nodes import (
    ElementaryResourceEmissionNode,
    FoodProcessingActivityNode,
    FoodProductFlowNode,
    ModeledActivityNode,
    PracticeFlowNode,
    TransportActivityNode,
)
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.props import GlossaryTermProp, JsonProp, LocationProp, QuantityPackageProp
from core.domain.props.legacy_api_information_prop import LegacyAPIInformationProp
from core.domain.props.link_to_xid_prop import LinkToXidProp
from core.domain.props.location_prop import LocationQualifierEnum
from core.domain.props.names_prop import NamesProp
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.props.referenceless_quantity_prop import ReferencelessQuantityProp
from core.graph_manager.calc_graph import CalcGraph
from core.orchestrator.orchestrator import Orchestrator
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID

logger = get_logger()

CONSERVATION_TAG_XID_TO_NAME = {
    "J0001": "generic conserved",
    "J0003": "not conserved",
    "J0131": "cooled",
    "J0136": "frozen",
    "J0111": "canned",
    "J0116": "dried",
}

PERISHABILITY_TAG_XID_TO_NAME = {
    "EOS_STABLE": "stable",
    "EOS_PERISHABLE": "perishable",
    "EOS_HIGH-PERISHABLE": "highly-perishable",
}

OPTIONAL_RAW_INPUTS_TO_RETURN = [
    "article_number",
    "business_unit",
    "end_product",
    "certificates",
    "packaging",
]


class OutputGraphToTableConverter:
    """Converter of CalcGraph instances to a JSON-type table.

    Code refactored from Valora Dagster pipeline.
    """

    def __init__(self):
        self.data_errors: str = ""
        self.max_depth_of_returned_table: Optional[int] = None
        self.references: list[ReferenceAmountEnum] = [ReferenceAmountEnum.amount_for_root_node.value]

    @staticmethod
    def _get_conservation(flow_node: FoodProductFlowNode) -> str:
        conservation_tags = []
        for xid in flow_node.tag_term_xids:
            if xid in CONSERVATION_TAG_XID_TO_NAME:
                conservation_tags.append(CONSERVATION_TAG_XID_TO_NAME[xid])
        return ",".join(conservation_tags)

    @staticmethod
    def _get_perishability(flow_node: FoodProductFlowNode) -> str:
        perishability_tags = []
        for xid in flow_node.tag_term_xids:
            if xid in PERISHABILITY_TAG_XID_TO_NAME:
                perishability_tags.append(PERISHABILITY_TAG_XID_TO_NAME[xid])
        return ",".join(perishability_tags)

    @staticmethod
    def _check_if_node_is_unskippable(node: Node) -> bool:
        """Check if the given node is of a type that must be provided in output."""
        if isinstance(node, (ModeledActivityNode, ElementaryResourceEmissionNode)):
            return False
        else:
            return True

    @staticmethod
    def _parse_quantity_package(qty_pkg: Optional[QuantityPackageProp]) -> dict:
        """Parse nutrient dict into columns with appropriate names."""
        if not qty_pkg:
            return {}
        else:
            return {
                f"{GlossaryTermProp.get_term_from_uid(key).name}": data.value
                for key, data in qty_pkg.quantities.items()
            }

    def _parse_nutrient_package_into_columns(self, nutrients: Optional[QuantityPackageProp], reference: str) -> dict:
        """Parse nutrient dict into columns with appropriate names."""
        if not nutrients:
            return {}
        else:

            def _nutrient_name_and_unit(nutrient: str, data: ReferencelessQuantityProp) -> str:
                return (
                    f"{GlossaryTermProp.get_term_from_uid(nutrient).name.lower().replace(' ', '_')}"
                    f"_{data.unit.lower().replace(' ', '_')}"
                )

            return {
                "EOS_nutrients_"
                f"{self._get_key_name(None, _nutrient_name_and_unit(nutrient, data), reference)}": data.value
                for nutrient, data in nutrients.quantities.items()
            }

    def _parse_nutrient_values(self, node: Node) -> dict:
        """Parse `nutrient_values` property into columns for each individual nutrient."""
        food_node = None
        if isinstance(node, FoodProductFlowNode):
            food_node = node
        elif node.get_parent_nodes() and isinstance(node.get_parent_nodes()[0], FoodProductFlowNode):
            food_node = node.get_parent_nodes()[0]
        nutrient_dict = {}

        if food_node:
            # take precedence for aggregated_nutrients
            nutrient_values = food_node.aggregated_nutrients
            source = "aggregated"
            # if no aggregated_nutrients, take nutrient_values (normally come from the EOS-database)
            if not nutrient_values:
                nutrient_values = food_node.nutrient_values
                source = "database"
            if nutrient_values:
                for reference in self.references:
                    try:
                        nutrient_content_per_reference = nutrient_values.amount_for_reference(reference)
                        nutrient_dict.update(
                            self._parse_nutrient_package_into_columns(nutrient_content_per_reference, reference)
                        )
                    except ValueError:
                        continue

                    nutrient_dict["EOS_nutrients_source"] = source

                    if food_node.daily_food_unit:
                        try:
                            dfu_for_reference = food_node.daily_food_unit.amount_for_reference(reference)
                            if dfu_for_reference:
                                nutrient_dict[
                                    f"aggregated_daily_food_unit_per_{reference.replace('amount_for_', '')}"
                                ] = dfu_for_reference.value
                        except ValueError:
                            continue
        return nutrient_dict

    @staticmethod
    def _get_node_location(current_node: Node) -> str:
        if isinstance(current_node, ActivityNode):
            location_prop = current_node.activity_location
        else:
            location_prop = current_node.flow_location

        if location_prop:
            if isinstance(location_prop, str):
                return location_prop

            location_terms = [loc.get_term().xid for loc in location_prop if loc.get_term()]
            if location_terms:
                if len(location_terms) == 1:
                    location = location_terms[0]
                else:
                    location = str(location_terms)
            elif (
                isinstance(location_prop, (list, DeepListView))
                and isinstance(location_prop[0], LocationProp)
                and location_prop[0].location_qualifier == LocationQualifierEnum.unknown
            ):
                location = LocationQualifierEnum.unknown.value
            else:
                location = ""
        else:
            location = ""
        return location

    @staticmethod
    def _get_node_name(current_node: Node) -> str:
        current_node_name = None
        if hasattr(current_node, "product_name"):
            if current_node.product_name and isinstance(current_node.product_name, NamesProp):
                if current_node.product_name.source_data_raw is not None:
                    current_node_name = current_node.product_name.source_data_raw[0].get("value")
                else:
                    current_node_name = ",".join([term.get_term().name for term in current_node.product_name.terms])
            elif current_node.product_name:
                current_node_name = current_node.product_name[0].get("value")

        if current_node_name is None and current_node.raw_input is not None:
            raw_input = current_node.raw_input
            if hasattr(raw_input, "names") and raw_input.names:
                current_node_name = raw_input.names[0].get("value")
            elif hasattr(raw_input, "titles") and raw_input.titles:
                current_node_name = raw_input.titles[0].get("value")

        return current_node_name

    def _get_name(self, node: Node) -> str:
        # TODO: can we reuse the name method in the node-class?
        node_name = self._get_node_name(node)
        if node_name is None and isinstance(node, ActivityNode) and node.get_parent_nodes():
            # Try getting the name from parent flow (primary product of the activity) for activity nodes.
            node_name = self._get_node_name(node.get_parent_nodes()[0])

        # TODO: can we cleane this up a bit?
        if node_name is None:  # this node has been subdivided
            try:  # to get the name of this node's first child
                first_child = node.get_sub_nodes()[0]
                # Check if first_child is subdivision.
                if hasattr(first_child, "is_subdivision") and first_child.is_subdivision:
                    if hasattr(first_child, "product_name"):
                        if first_child.product_name and isinstance(first_child.product_name, NamesProp):
                            if first_child.product_name.source_data_raw is not None:
                                node_name = first_child.product_name.source_data_raw[0].get("value")
                            else:
                                node_name = first_child.product_name.terms[0].get_term().name
                        elif first_child.product_name:
                            node_name = first_child.product_name[0].get("value")

                    if node_name is None and first_child.raw_input is not None:
                        if "names" in first_child.raw_input.model_fields_set:
                            node_name = first_child.raw_input.names[0].get("value")
                        elif "titles" in first_child.raw_input.model_fields_set:
                            node_name = first_child.raw_input.titles[0].get("value")
                        else:
                            node_name = type(node).__name__
            except Exception:  # if the first child has no name, we use the node_type
                node_name = type(node).__name__

        if node_name is None:
            node_name = type(node).__name__

        return node_name

    def _get_node_xid(self, node: Node) -> Optional[str]:
        xid = None
        if node.xid:
            return node.xid
        if isinstance(node, FoodProductFlowNode):
            if isinstance(node.link_to_sub_node, LinkToXidProp):
                return node.link_to_sub_node.xid
            if (
                isinstance(node.legacy_API_information, LegacyAPIInformationProp)
                and node.legacy_API_information.properties_pre_gfms
                and isinstance(node.legacy_API_information.properties_pre_gfms.get("link_to_sub_node"), LinkToXidProp)
            ):
                return node.legacy_API_information.properties_pre_gfms["link_to_sub_node"].xid
        return xid

    def _get_xid(self, node: Node) -> str:
        node_xid = self._get_node_xid(node)
        if node_xid is None and node.get_parent_nodes():
            node_xid = self._get_node_xid(node.get_parent_nodes()[0])
        if node_xid is None:
            node_xid = ""
        return node_xid

    @staticmethod
    def _get_key_name(
        unit: Optional[str] = None, specification: Optional[str] = None, reference: Optional[str] = None
    ) -> str:
        key_str = ""
        if unit:
            key_str += f"{unit}"
        if specification:
            if key_str:
                key_str += "_"
            key_str += f"{specification}"
        if reference:
            if key_str:
                key_str += "_"
            key_str += f"per_{reference.replace('amount_for_', '')}"
        return key_str

    def _iterate_graph(
        self,
        rows: list[dict],
        node: Node,
        level: int,
        tree_index: str = "",
        parent_flow_node: Node | None = None,
        final_graph_table_criterion: Optional[FinalGraphTableCriterion] = None,
    ) -> dict:
        """Recursively iterate the given CalcGraph."""
        activity_node = None
        if isinstance(node, ActivityNode):
            activity_node = node

        flow_node = None
        if isinstance(node, FlowNode):
            flow_node = node
        elif parent_flow_node and isinstance(parent_flow_node, FlowNode):
            flow_node = parent_flow_node

        if node.raw_input:
            raw_input = node.raw_input
        else:
            raw_input = JsonProp.unvalidated_construct()  # to avoid errors

        node_name = self._get_name(node)
        node_xid = self._get_xid(node)

        if isinstance(node, FoodProductFlowNode):
            is_subdivision = node.is_subdivision
        else:
            is_subdivision = False

        if isinstance(node, FoodProductFlowNode):
            is_dried = node.is_dried()
        else:
            is_dried = False

        # TODO: can we reuse the _location_and_its_source in the node-class?
        location = self._get_node_location(node)
        if not location and parent_flow_node:
            # If location information is not available in the node activity node, see if the information is available
            # in the parent_flow_node (i.e., the row for transportation food processing activity node contains
            # the destination location).
            location = self._get_node_location(parent_flow_node)

        if isinstance(node, FoodProcessingActivityNode) and node.ingredient_amount_estimator_error_squares:
            error_squares = str(self._parse_quantity_package(node.ingredient_amount_estimator_error_squares))
        else:
            error_squares = ""

        if isinstance(node, FoodProcessingActivityNode) and node.ingredient_amount_estimator_solution_status:
            solution_status = node.ingredient_amount_estimator_solution_status.value
        else:
            solution_status = ""

        impact_assessment_dict = {}
        if node.impact_assessment:
            try:
                impact_assessment_dump = node.impact_assessment.model_dump()
            except ValueError:
                impact_assessment_dump = {}
            for reference in self.references:
                if reference in impact_assessment_dump:
                    for _, val in impact_assessment_dump[reference].items():
                        if val.get("quantity_term_xid") == IPCC_2013_GWP_100_XID:
                            impact_assessment_dict[self._get_key_name("kg", "co2eq", reference)] = val["quantity"][
                                "value"
                            ]
                else:
                    impact_assessment_dict[self._get_key_name("kg", "co2eq", reference)] = ""
                impact_assessment_dict[
                    self._get_key_name("kg", "co2eq_reduction_value", reference)
                ] = impact_assessment_dump.get("co2_value_reduction_value", {}).get(reference, "")

            # get other information from the impact assessment
            for key in ("co2_rating", "co2_award", "co2_value_improvement_percentage"):
                impact_assessment_dict[key] = impact_assessment_dump.get(key, "")

        scarce_water_consumption_dict = {}
        if node.scarce_water_consumption:
            scarce_water_consumption_dump = node.scarce_water_consumption.model_dump()
            for reference in self.references:
                if reference in scarce_water_consumption_dump:
                    scarce_water_consumption_dict[
                        self._get_key_name("liter", "scarce_water_consumption", reference)
                    ] = scarce_water_consumption_dump[reference]
                else:
                    scarce_water_consumption_dict[
                        self._get_key_name("liter", "scarce_water_consumption", reference)
                    ] = ""
                self._get_key_name("kg", "co2eq_reduction_value", reference)

                scarce_water_consumption_dict[
                    self._get_key_name("liter", "scarce_water_consumption_reduction_value", reference)
                ] = scarce_water_consumption_dump.get("scarce_water_consumption_reduction_value", {}).get(reference, "")

            # get other information from the scarce water consumption
            for key in (
                "scarce_water_consumption_rating",
                "scarce_water_consumption_award",
                "scarce_water_consumption_improvement_percentage",
            ):
                scarce_water_consumption_dict[key] = scarce_water_consumption_dump.get(key, "")

        if isinstance(node, FlowNode):
            node_production_amount = None
            node_production_unit = None
        else:
            # in eos, 1 kg of production_amount is assumed, if not otherwise specified
            if node.production_amount:
                node_production_amount = node.production_amount.value
                node_production_unit = node.production_amount.unit
            else:
                node_production_amount = 1.0
                node_production_unit = None

        flow_amount_dict = {}
        flow_unit = ""
        if flow_node:
            flow_amount = flow_node.amount
            if flow_amount:
                flow_unit = flow_node.amount.unit
                for reference in self.references:
                    try:
                        flow_amount_dict[
                            self._get_key_name(None, "flow_amount", reference)
                        ] = flow_amount.amount_for_reference(reference).value
                    except ValueError:
                        flow_amount_dict[self._get_key_name(None, "flow_amount", reference)] = ""

        node_types = []
        if flow_node:
            node_types.append("flow")
        if activity_node:
            node_types.append("activity")
        general_node_type = ",".join(node_types)

        if isinstance(node, FlowNode) and node.matrix_gfm_error:
            matrix_gfm_error = node.matrix_gfm_error.value
        else:
            matrix_gfm_error = ""

        # Add conservation:
        if parent_flow_node and isinstance(parent_flow_node, FoodProductFlowNode):
            conservation_tags = self._get_conservation(parent_flow_node)
            perishability_tags = self._get_perishability(parent_flow_node)
            if parent_flow_node.raw_processing:
                processing_tags = str(parent_flow_node.raw_processing.get("value", ""))
            else:
                processing_tags = ""
            is_combined_product = parent_flow_node.is_combined_product()
        elif isinstance(node, FoodProductFlowNode):
            conservation_tags = self._get_conservation(node)
            perishability_tags = self._get_perishability(node)
            if node.raw_processing:
                processing_tags = str(node.raw_processing.get("value", ""))
            else:
                processing_tags = ""
            is_combined_product = node.is_combined_product()
        else:
            conservation_tags = ""
            perishability_tags = ""
            processing_tags = ""
            is_combined_product = ""

        # add gtin, producer and other context-fields
        if parent_flow_node and isinstance(parent_flow_node, FoodProductFlowNode):
            gtin = parent_flow_node.gtin if parent_flow_node.gtin else ""
            supplier = parent_flow_node.supplier if parent_flow_node.supplier else ""
            producer = parent_flow_node.producer if parent_flow_node.producer else ""
        elif isinstance(node, FoodProductFlowNode):
            gtin = node.gtin if node.gtin else ""
            supplier = node.supplier if node.supplier else ""
            producer = node.producer if node.producer else ""
        else:
            gtin = ""
            supplier = ""
            producer = ""

        # we don't "print" this type of node, but we provide its amount to its children
        if isinstance(node, (FoodProductFlowNode, PracticeFlowNode)) and node.get_sub_nodes():
            if self._check_if_node_is_unskippable(node.get_sub_nodes()[0]):
                parent_flow_node = node
                sub_row = self._iterate_graph(
                    rows,
                    node.get_sub_nodes()[0],
                    level,
                    parent_flow_node=parent_flow_node,
                    tree_index=tree_index,
                    final_graph_table_criterion=final_graph_table_criterion,
                )
                return sub_row

        # TODO: Maybe this can be cleaned up a bit, e.g. by reusing tag_term_xids in the food-product-flow-node class
        product_name_prop = None
        if hasattr(node, "product_name") and node.product_name and isinstance(node.product_name, NamesProp):
            product_name_prop = node.product_name
        elif (
            hasattr(parent_flow_node, "product_name")
            and parent_flow_node.product_name
            and isinstance(parent_flow_node.product_name, NamesProp)
        ):
            product_name_prop = parent_flow_node.product_name
        if product_name_prop:
            matched_terms = [f"{prop.get_term().name} [{prop.get_term().xid}]" for prop in product_name_prop.terms]
            matched_terms = ", ".join(matched_terms)
        else:
            matched_terms = ""

        brightway_node = ""
        brightway_node_from_sub = None
        if (
            isinstance(node, FlowNode)
            and node.get_sub_nodes()
            and node.get_sub_nodes()
            and isinstance(node.get_sub_nodes()[0], ModeledActivityNode)
        ):
            brightway_node_from_sub = node.get_sub_nodes()[0]
        if brightway_node_from_sub:
            # Loading in the module causes circular dependencies.
            from core.service.service_provider import ServiceLocator

            service_provider = ServiceLocator().service_provider
            try:
                brightway_node_from_cache = service_provider.node_service.cache.get_cached_node_by_uid(
                    brightway_node_from_sub.uid
                )

                raw_input_data = brightway_node_from_cache.raw_input
                required_fields = {"key", "name", "reference_product"}

                if raw_input_data and required_fields.issubset(raw_input_data.model_fields_set):
                    brightway_node = (
                        f"Key: {str(tuple(raw_input_data.key))}\n"
                        f"Name: {raw_input_data.name}\n"
                        f"Reference product: {raw_input_data.reference_product}"
                    )
                    if brightway_node_from_cache.activity_location and isinstance(
                        brightway_node_from_cache.activity_location, str
                    ):
                        brightway_node += f"\nLocation: {brightway_node_from_cache.activity_location}"

                else:
                    brightway_node = ""
            except KeyError:
                brightway_node = ""
        else:
            brightway_node = ""

        if isinstance(node, FlowNode) and getattr(node.origin_data, "fao_code_term", None):
            fao_code_term = node.origin_data.fao_code.get_term()
            fao_code = f"{fao_code_term.xid}\n" f"Name: {fao_code_term.name}"
        else:
            fao_code = ""

        if (
            isinstance(node, FoodProductFlowNode)
            and node.nutrient_values
            and node.nutrient_values.get_prop_term() is not None
        ):
            nutrient_file = node.nutrient_values.get_prop_term().xid
        else:
            nutrient_file = ""

        transport = ""
        transport_mode_options = ""
        transport_mode_distances = ""
        if node.get_sub_nodes():
            if isinstance(node, ActivityNode):
                location_prop = node.activity_location
            else:
                location_prop = node.flow_location
            if self._check_if_node_is_unskippable(node.get_sub_nodes()[0]) and location_prop:
                if isinstance(node, ActivityNode) and node.transport:
                    transport_mode_distances = str(node.transport.model_dump())
                    if node.transport.qualified_modes:
                        transport_mode_options = ", ".join(node.transport.qualified_modes)
                    if node.transport.cheapest_mode:
                        transport = node.transport.cheapest_mode

        recipe_portions = getattr(node, "recipe_portions", "")
        production_portions = getattr(node, "production_portions", "")
        sold_portions = getattr(node, "sold_portions", "")

        nutr_vals = {}
        if isinstance(node, FoodProductFlowNode):
            node_with_nutrients = node
        elif node.get_parent_nodes() and isinstance(node.get_parent_nodes()[0], FoodProductFlowNode):
            node_with_nutrients = node.get_parent_nodes()[0]
        else:
            node_with_nutrients = None

        if (
            node_with_nutrients
            and node_with_nutrients.nutrient_values
            and isinstance(node_with_nutrients.nutrient_values, QuantityPackageProp)
            and node_with_nutrients.nutrient_values.source_data_raw
        ):
            nutr_vals = node_with_nutrients.nutrient_values.source_data_raw

        nutrients_declaration = {
            f"nutrients_declaration_{self._get_key_name(None, key, ReferenceAmountEnum.amount_for_100g.value)}": val
            for key, val in nutr_vals.items()
        }

        if isinstance(node, FoodProductFlowNode) and node.nutrient_upscale_ratio:
            nutrient_upscale_ratio = node.nutrient_upscale_ratio.value
        else:
            nutrient_upscale_ratio = ""

        if flow_node and isinstance(flow_node, FoodProductFlowNode) and flow_node.animal_products:
            animal_products = flow_node.animal_products.model_dump()
            animal_welfare_rating = animal_products["animal_welfare_rating"]
        else:
            animal_welfare_rating = ""

        amount_per_category_in_flow = ""
        if flow_node and isinstance(flow_node, FoodProductFlowNode) and flow_node.amount_per_category_in_flow:
            amount_per_category_in_flow_prop = flow_node.amount_per_category_in_flow.model_dump()
            for reference in self.references:
                if reference in amount_per_category_in_flow_prop:
                    amount_per_category_in_flow = ",".join(
                        sorted(
                            [
                                val["quantity_term_name"]
                                for key, val in amount_per_category_in_flow_prop[reference].items()
                            ]
                        )
                    )
                    # we can exit the for loop over self.references, because we found the first one
                    break

        if flow_node and isinstance(flow_node, FoodProductFlowNode) and flow_node.rainforest_critical_products:
            rainforest_critical_products_prop = flow_node.rainforest_critical_products.model_dump()
            rainforest_critical_products_rating = rainforest_critical_products_prop["rainforest_rating"]
        else:
            rainforest_critical_products_rating = ""

        vita_score_dict = {}
        if flow_node and isinstance(flow_node, FoodProductFlowNode) and flow_node.vitascore:
            vita_prop = flow_node.vitascore.model_dump()
            if level == 0:
                vita_indicators = vita_prop.get("amount_for_activity_production_amount", {})
            else:
                vita_indicators = vita_prop.get("amount_for_root_node", {})
            for val in vita_indicators.values():
                # TODO: hardcoded per_root_node in the name because we probably want to change our vita-score
                # implementation in the future in such a way that the reference will become important
                vita_score_dict[
                    (
                        self._get_key_name(
                            None,
                            f"{val['quantity_term_name'].lower().replace(' ', '_')}_points",
                            ReferenceAmountEnum.amount_for_root_node.value,
                        )
                    )
                ] = val["quantity"]["value"]
            vita_score_dict["vitascore_rating"] = vita_prop["vitascore_rating"]
            vita_score_dict["vitascore_improvement_percentage"] = vita_prop["vitascore_improvement_percentage"]

        # continue with subnodes
        sub_nodes = [sub_node for sub_node in node.get_sub_nodes() if self._check_if_node_is_unskippable(sub_node)]
        is_leaf = len(sub_nodes) == 0

        # Initialize aggregated statistics for the current node
        from gap_filling_modules.processing_gfm import (
            ProcessingGapFillingFactory,  # importing here to avoid circular dependencies
        )

        processing_names = ProcessingGapFillingFactory.model_by_name.keys()

        all_categories_co2_aggregations_all_references = {}

        for reference in self.references:
            # TODO: currently we only have amount_per_root correctly implemented for these co2-aggregations
            if reference != ReferenceAmountEnum.amount_for_root_node.value:
                continue
            transport_co2_aggregations_for_reference = {
                self._get_key_name("kg", f"co2eq_only_{kind}", reference): 0.0
                for kind in [
                    "truck_transport",
                    "sea_transport",
                    "air_transport",
                    "cooling_of_transport",
                    "freezing_of_transport",
                ]
            }

            processing_co2_aggregations_for_reference = {
                self._get_key_name(
                    "kg", f"co2eq_only_processing_{processing_name.lower().replace(' ', '_')}", reference
                ): 0.0
                for processing_name in processing_names
            }

            all_categories_co2_aggregations_for_reference = {
                self._get_key_name("kg", "co2eq_only_all_transport", reference): 0.0,
                self._get_key_name("kg", "co2eq_only_all_processing", reference): 0.0,
                self._get_key_name("kg", "co2eq_only_greenhouse", reference): 0.0,
                self._get_key_name("kg", "co2eq_only_base", reference): 0.0,
                **transport_co2_aggregations_for_reference,
                **processing_co2_aggregations_for_reference,
            }

            kg_co2eq_per_reference = impact_assessment_dict.get(self._get_key_name("kg", "co2eq", reference), "")

            if kg_co2eq_per_reference != "":
                if is_leaf:
                    if isinstance(node, PracticeFlowNode):
                        if node_name.endswith("ProcessingGFM"):
                            processing_name = node_name.removesuffix(" ProcessingGFM")
                            if processing_name in processing_names:
                                all_categories_co2_aggregations_for_reference[
                                    self._get_key_name(
                                        "kg",
                                        f"co2eq_only_processing_{processing_name.lower().replace(' ', '_')}",
                                        reference,
                                    )
                                ] = kg_co2eq_per_reference
                            else:
                                logger.error(f"Processing name not in aggregation dict: {processing_name}")
                        elif node_name.startswith("Transport preserved by"):
                            if node_name.startswith("Transport preserved by freezing"):
                                all_categories_co2_aggregations_for_reference[
                                    self._get_key_name("kg", "co2eq_only_freezing_of_transport", reference)
                                ] = kg_co2eq_per_reference
                            elif node_name.startswith("Transport preserved by chilling"):
                                all_categories_co2_aggregations_for_reference[
                                    self._get_key_name("kg", "co2eq_only_cooling_of_transport", reference)
                                ] = kg_co2eq_per_reference
                            else:
                                logger.error(f"Transport name not in aggregation dict: {node_name}")
                    elif isinstance(node, FoodProductFlowNode):
                        sub_activity = node.get_sub_nodes()[0]
                        if isinstance(sub_activity, ModeledActivityNode):
                            if sub_activity.impact_assessment:
                                # extract base co2:
                                char_for_local_activity = (
                                    sub_activity.impact_assessment.amount_for_activity_production_amount()
                                )

                                if char_for_local_activity:
                                    for key, val in char_for_local_activity.quantities.items():
                                        if GlossaryTermProp.get_term_from_uid(key).xid == IPCC_2013_GWP_100_XID:
                                            # convert to the amount of the parent flow node amount per root:
                                            all_categories_co2_aggregations_for_reference[
                                                self._get_key_name("kg", "co2eq_only_base", reference)
                                            ] = (
                                                val.value
                                                * flow_amount_dict[self._get_key_name(None, "flow_amount", reference)]
                                                / sub_activity.production_amount.value
                                            )
                            else:
                                logger.warning(f"Missing impact assessment for {sub_activity}")
                    elif isinstance(node, FlowNode):
                        # TODO: check if this should not better be a PracticeFlowNode?
                        if "Greenhouse [EOS_prod_greenhouse]" in matched_terms:
                            all_categories_co2_aggregations_for_reference[
                                self._get_key_name("kg", "co2eq_only_greenhouse", reference)
                            ] = kg_co2eq_per_reference
                else:
                    # aggregate transport only from the activity up, because the sub "Fuel"-FlowNodes
                    # do not contain kg_co2eq
                    if isinstance(node, TransportActivityNode):
                        if "Truck transport [EOS_GROUND]" in matched_terms:
                            all_categories_co2_aggregations_for_reference[
                                self._get_key_name("kg", "co2eq_only_truck_transport", reference)
                            ] = kg_co2eq_per_reference
                        elif "Sea transport [EOS_SEA]" in matched_terms:
                            all_categories_co2_aggregations_for_reference[
                                self._get_key_name("kg", "co2eq_only_sea_transport", reference)
                            ] = kg_co2eq_per_reference
                        elif "Air transport [EOS_AIR]" in matched_terms:
                            all_categories_co2_aggregations_for_reference[
                                self._get_key_name("kg", "co2eq_only_air_transport", reference)
                            ] = kg_co2eq_per_reference

            all_categories_co2_aggregations_for_reference[
                self._get_key_name("kg", "co2eq_only_all_transport", reference)
            ] = sum(
                all_categories_co2_aggregations_for_reference[key] for key in transport_co2_aggregations_for_reference
            )
            all_categories_co2_aggregations_for_reference[
                self._get_key_name("kg", "co2eq_only_all_processing", reference)
            ] = sum(
                all_categories_co2_aggregations_for_reference[key] for key in processing_co2_aggregations_for_reference
            )
            all_categories_co2_aggregations_all_references.update(all_categories_co2_aggregations_for_reference)

        row = {
            "name": "  " * level + node_name,
            "xid": node_xid,
            "gtin": gtin,
            "access_group_xid": getattr(node, "access_group_xid", ""),
            "supplier": supplier,
            "producer": producer,
            "level": level,
            "tree_index": tree_index,
            "is_leaf": is_leaf,
            "flow_node_type": type(flow_node).__name__ if flow_node else "",
            "activity_node_type": type(activity_node).__name__ if activity_node else "",
            "general_node_type": general_node_type,
            **{key: getattr(raw_input, key, "") for key in OPTIONAL_RAW_INPUTS_TO_RETURN},
            **flow_amount_dict,
            "flow_unit": flow_unit,
            "node_production_amount": node_production_amount,
            "node_production_unit": node_production_unit,
            "recipe_portions": recipe_portions,
            "production_portions": production_portions,
            "sold_portions": sold_portions,
            "production_date": getattr(node, "activity_date", ""),
            "location": location,
            "transport": transport,
            "conservation": conservation_tags,
            "perishability": perishability_tags,
            "processing": processing_tags,
            "is_combined_product": is_combined_product,
            "is_subdivision": is_subdivision,
            "is_dried": is_dried,
            "nutrient_upscale_ratio": nutrient_upscale_ratio,
            "matched_terms": matched_terms,
            "nutrient_file": nutrient_file,
            "fao_code": fao_code,
            "brightway_node": brightway_node,
            "amount_per_category_in_flow": amount_per_category_in_flow,
            "transport_mode_distances": transport_mode_distances,
            "transport_mode_options": transport_mode_options,
            "matrix_gfm_error": matrix_gfm_error,
            "error_squares": error_squares,
            "solution_status": solution_status,
            **impact_assessment_dict,
            **scarce_water_consumption_dict,
            **vita_score_dict,
            "animal_welfare_rating": animal_welfare_rating,
            "rainforest_critical_products_rating": rainforest_critical_products_rating,
            **all_categories_co2_aggregations_all_references,
            **nutrients_declaration,
            **self._parse_nutrient_values(node),
        }
        if self.max_depth_of_returned_table is not None and level > self.max_depth_of_returned_table:
            append_row = False
        elif final_graph_table_criterion:
            # if the final_graph_criterion is set, we only keep the row if one of the 'keep' criteria is met
            append_row = False
            if not append_row and final_graph_table_criterion.keep_customer_send_data:
                # import here to avoid circular dependencies
                from gap_filling_modules.add_client_nodes_gfm import AddClientNodesGapFillingWorker
                from gap_filling_modules.merge_linked_nodes_gfm import MergeLinkedNodesGapFillingWorker

                # TODO: Not all nodes added by the MergeLinkedNodesGapFillingWorker are necessarily
                # nodes sent by the customer. Make this more robust!
                if node.added_by not in (
                    Orchestrator.__name__,
                    AddClientNodesGapFillingWorker.__name__,
                ) and not (parent_flow_node and parent_flow_node.added_by == MergeLinkedNodesGapFillingWorker.__name__):
                    append_row = False
                elif final_graph_table_criterion.keep_customer_send_data.depth <= 0:
                    append_row = False
                else:
                    # we keep the row and reduce the depth by one
                    append_row = True
                    final_graph_table_criterion = copy.deepcopy(final_graph_table_criterion)
                    final_graph_table_criterion.keep_customer_send_data.depth -= 1
            if not append_row and final_graph_table_criterion.keep_rows_with_fields:
                for field in final_graph_table_criterion.keep_rows_with_fields:
                    if row.get(field):
                        append_row = True
                        break
        else:
            append_row = True

        if append_row:
            rows.append(row)

        # make sure order is correct
        sub_nodes = sorted(
            sub_nodes,
            key=lambda x: x.declaration_index.value if (hasattr(x, "declaration_index") and x.declaration_index) else 0,
        )

        for index, sub_node in enumerate(sub_nodes):
            sub_row = self._iterate_graph(
                rows,
                sub_node,
                level + 1,
                tree_index=tree_index + "." + str(index + 1),
                final_graph_table_criterion=final_graph_table_criterion,
            )
            for key in all_categories_co2_aggregations_all_references:
                if isinstance(sub_row[key], (int, float)) and isinstance(row[key], (int, float)):
                    row[key] += sub_row[key]

        if level == 0:
            # sanity check that the aggregation worked correctly
            from core.tests.conftest import is_close

            for reference in self.references:
                # TODO: currently we only have amount_per_root correctly implemented for these co2-aggregations
                if reference != ReferenceAmountEnum.amount_for_root_node.value:
                    continue
                kg_co2_per_reference = impact_assessment_dict.get(self._get_key_name("kg", "co2eq", reference), "")
                if kg_co2_per_reference and not is_close(
                    sum(
                        row[key]
                        for key in all_categories_co2_aggregations_all_references
                        if reference.replace("amount_for_", "") in key
                        and "co2eq_only_all_transport" not in key
                        and "co2eq_only_all_processing" not in key
                        and row[key]
                    ),
                    kg_co2_per_reference,
                ):
                    aggregated_statistics = {
                        key: row[key]
                        for key in all_categories_co2_aggregations_all_references
                        if reference.replace("amount_for_", "") in key
                        and "co2eq_only_all_transport" not in key
                        and "co2eq_only_all_processing" not in key
                    }
                    logger.error(
                        (
                            "Aggregated statistics do not sum up to kg_co2eq_per_root_product."
                            f"aggregated_statistics: {aggregated_statistics}, "
                            f"{self._get_key_name('kg', 'co2eq', reference)}: {kg_co2_per_reference}"
                        )
                    )

        return row

    def run(
        self,
        calc_graph: CalcGraph,
        max_depth_of_returned_table: Optional[int] = None,
        references: Optional[List[ReferenceAmountEnum]] = None,
        final_graph_table_criterion: Optional[FinalGraphTableCriterion] = None,
    ) -> list[dict]:
        """Run the convertor."""
        self.max_depth_of_returned_table = max_depth_of_returned_table
        if references:
            self.references = references
        root_node = calc_graph.get_node_by_uid(calc_graph.root_node_uid)
        rows = []
        self._iterate_graph(
            rows, root_node, level=0, tree_index="", final_graph_table_criterion=final_graph_table_criterion
        )

        return rows
