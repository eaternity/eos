"Node service."

import asyncio
import gzip
import sys
import uuid
from datetime import datetime
from io import BytesIO
from typing import TYPE_CHECKING, Any, Dict, List, Optional

import asyncpg
import httpx
import msgpack
from asyncpg.exceptions import ForeignKeyViolationError
from structlog import get_logger

from api.app.settings import Settings
from core.domain.calculation import FinalGraphTableCriterion
from core.domain.edge import Edge, EdgeToUpsert, EdgeTypeEnum, XidAccessGroupUid
from core.domain.nodes import ElementaryResourceEmissionNode, FlowNode, ModeledActivityNode
from core.domain.nodes.node import Node, PreAndPostCalcNodes
from core.domain.post_calc_cache import PostCalcCache, PostCalcDataMetaInformation
from core.domain.prop import Prop
from database.postgres.pg_graph_mgr import PostgresGraphMgr

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

api_settings = Settings()

CACHED_NODE_TYPES = [
    ModeledActivityNode.__name__,
    ElementaryResourceEmissionNode.__name__,
    FlowNode.__name__,
]


logger = get_logger()


class NodeCache:
    """Caches LCA nodes and edges at startup time.

    These nodes are heavily used in LCA calculations and never updated, so it makes sense to cache them.
    ATM this is just backed by a simple in-memory dict. Also, there's no way to reinit the cache
    TODO implement smarter / faster caching, if necessary
    """

    def __init__(self):
        self._node_cache: dict[uuid.UUID, Node] = dict()
        self._node_child_edge_cache: dict[uuid.UUID, list[Edge]] = dict()
        self._node_parent_edge_cache: dict[uuid.UUID, list[Edge]] = dict()
        self._cached_inventory_nodes: set[uuid.UUID] = set()
        self._binary_cache: bytes = None
        self.loaded: bool = False

    def get_cached_node_by_uid(self, uid: uuid) -> Optional[Node]:
        """Get cached node by uid.

        Raises:
            KeyError if node is not found
        """
        # We need to copy the node here, otherwise the node is modified in the cache:
        return Node.from_node(self._node_cache[uid])

    def add_node(self, node: Node) -> None:
        self._node_cache[node.uid] = node

    def add_edge(self, edge: Edge) -> None:
        if edge.parent_uid not in self._node_child_edge_cache:
            # initialize the edge cache for this edge's parent node
            self._node_child_edge_cache[edge.parent_uid] = [edge]
        else:
            self._node_child_edge_cache[edge.parent_uid].append(edge)

        if edge.child_uid not in self._node_parent_edge_cache:
            # initialize the edge cache for this edge's child node
            self._node_parent_edge_cache[edge.child_uid] = [edge]
        else:
            self._node_parent_edge_cache[edge.child_uid].append(edge)

    def get_parents_by_uid(self, node_uid: uuid.UUID) -> list[Edge]:
        if node_uid not in self._node_parent_edge_cache:
            raise KeyError("edges of that node are not cached")
        else:
            return self._node_parent_edge_cache[node_uid]

    def get_sub_graph_by_uid(self, node_uid: uuid.UUID, max_depth: int) -> list[Edge]:
        """Only max_depth 1 is supported ATM."""
        if max_depth > 1:
            raise ValueError("only max_depth=1 is supported atm")
        elif node_uid not in self._node_child_edge_cache:
            raise KeyError("edges of that node are not cached")
        else:
            return self._node_child_edge_cache[node_uid]

    def invalidate_cached_node_by_uid(self, uid: uuid) -> None:
        "Make sure that the node is removed from cache if it was in there."
        if uid in self._node_cache:
            del self._node_cache[uid]

        if uid in self._node_child_edge_cache:
            for edge in self._node_child_edge_cache[uid]:
                if edge.child_uid in self._node_parent_edge_cache:
                    if edge in self._node_parent_edge_cache[edge.child_uid]:
                        self._node_parent_edge_cache[edge.child_uid].remove(edge)
                    else:
                        logger.warning(f"edge {edge} was not found in parent edge cache of child {edge.child_uid}")
            del self._node_child_edge_cache[uid]

        if uid in self._node_parent_edge_cache:
            for edge in self._node_parent_edge_cache[uid]:
                if edge.parent_uid in self._node_child_edge_cache:
                    if edge in self._node_child_edge_cache[edge.parent_uid]:
                        self._node_child_edge_cache[edge.parent_uid].remove(edge)
                    else:
                        logger.warning(f"edge {edge} was not found in child edge cache of parent {edge.parent_uid}")
            del self._node_parent_edge_cache[uid]

    def _initialize_from_running_pods(self) -> None:
        logger.info("Fetch binary node cache from other pods...")
        response = httpx.get(
            url=f"{api_settings.api_url}/nodes/cache",
            headers={"Authorization": f"Basic {api_settings.EATERNITY_AUTH_KEY}"},
            timeout=5,
        )

        if response.status_code != 200:
            raise Exception(f"Failed to fetch binary node cache from other pods: {response}")
        self._binary_cache = response.content

        logger.info("Deserialize binary node cache...")
        with gzip.GzipFile(fileobj=BytesIO(self._binary_cache), mode="rb") as decompressor:
            (
                self._node_cache,
                self._node_child_edge_cache,
                self._node_parent_edge_cache,
                self._cached_inventory_nodes,
            ) = msgpack.unpack(decompressor, object_hook=NodeCache.deserializer, strict_map_key=False)

        logger.info("Done getting node cache...")

    async def _initialize_from_db(self, graph_mgr: PostgresGraphMgr, load_every_inventory_nodes: bool) -> None:
        logger.info("Starting to fill the node cache...")
        if load_every_inventory_nodes:
            lca_nodes = await graph_mgr.get_nodes_by_type(CACHED_NODE_TYPES)
        else:
            self._cached_inventory_nodes = await graph_mgr.get_uids_of_inventory_nodes_to_cache()
            lca_nodes = await graph_mgr.get_pruned_nodes_to_cache()

        for lca_node in lca_nodes:
            self.add_node(lca_node)

        logger.info("Starting to fill the edge cache...")
        if load_every_inventory_nodes:
            lca_links = await graph_mgr.get_lca_links()
        else:
            lca_links = await graph_mgr.get_edges_below_pruned_nodes_to_cache()

        for lca_link in lca_links:
            self.add_edge(lca_link)

        logger.info("Starting to compute the binary cache...")

        self._update_binary_cache()

        self.loaded = True
        logger.info("Loaded node and edge cache from DB", node_count=len(lca_nodes), edge_count=len(lca_links))

    def _update_binary_cache(self) -> None:
        logger.info("Updating binary node cache...")

        with BytesIO() as fileobj:
            fileobj.mode = "wb"

            with gzip.GzipFile(mode="wb", compresslevel=9, fileobj=fileobj) as compressor:
                packer = msgpack.Packer(default=NodeCache.serializer)
                compressor.write(
                    packer.pack(
                        (
                            self._node_cache,
                            self._node_child_edge_cache,
                            self._node_parent_edge_cache,
                            self._cached_inventory_nodes,
                        )
                    )
                )
                compressor.flush()

            self._binary_cache = fileobj.getvalue()

        logger.info("Done updating binary node cache...")

    @classmethod
    def serializer(cls: type["NodeCache"], obj: object) -> dict:
        if isinstance(obj, Node):
            return Node.model_dump(obj)
        elif isinstance(obj, Edge):
            return obj.serialize()
        elif isinstance(obj, uuid.UUID):
            return {"cls": uuid.UUID.__name__, "v": str(obj)}
        elif isinstance(obj, set):
            return {"cls": set.__name__, "v": list(obj)}
        elif isinstance(obj, int) and sys.maxsize < obj:
            return {"cls": int.__name__, "v": str(obj)}
        else:
            return obj

    @classmethod
    def deserializer(cls: type["NodeCache"], obj: object) -> object:
        if "cls" in obj and obj["cls"] == uuid.UUID.__name__:
            return uuid.UUID(obj["v"])
        elif "cls" in obj and obj["cls"] == set.__name__:
            return set(obj["v"])
        elif "cls" in obj and obj["cls"] == int.__name__:
            return int(obj["v"])
        elif "node_type" in obj:
            return Node.from_dict(obj)
        else:
            return Edge.deserialize(obj)

    def clear(self) -> None:
        self._node_cache.clear()
        self._node_child_edge_cache.clear()
        self._node_parent_edge_cache.clear()
        self._cached_inventory_nodes.clear()
        self._binary_cache = None
        self.loaded = False

    def __len__(self):
        return len(self._node_cache)


class NodeService:
    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.graph_mgr = None
        self._lock = asyncio.Lock()
        self.cache = NodeCache()
        self.load_every_inventory_nodes = False
        # when load_every_inventory_nodes is False, only self.cache._cached_inventory_nodes are cached.

    async def init(self) -> None:
        self.graph_mgr = self.service_provider.postgres_db.graph_mgr
        self.product_mgr = self.service_provider.postgres_db.product_mgr

        if self.service_provider.messaging_service is not None and api_settings.SUBSCRIBE_CACHE_INVALIDATIONS:
            # Used for indicating that a single node (message contains node_uid) should be invalidated.
            await self.service_provider.messaging_service.subscribe_cache_invalidation(
                "node", self._node_on_cache_invalidation_msg
            )

            # Used for indicating that the set of cached_inventory_nodes has been updated on the database.
            # On message, we have to reload the entire node cache according to the new set of pruned inventory nodes
            # to cache.
            await self.service_provider.messaging_service.subscribe_cache_invalidation(
                "inventory", self._inventory_on_cache_invalidation_msg
            )

        await self.ensure_cache_is_loaded()

    async def _broadcast_cache_invalidation(self, node_uid: uuid.UUID) -> None:
        self.cache.invalidate_cached_node_by_uid(node_uid)
        if self.service_provider.messaging_service is None:
            # no messaging service, so we invalidate the cache directly just within this instance:
            await self._node_on_cache_invalidation_msg(node_uid.bytes)
        else:
            logger.debug("emitting a cache invalidation for node", node_uid=node_uid)
            await self.service_provider.messaging_service.emit_invalidate("node", node_uid.bytes)

    async def _node_on_cache_invalidation_msg(self, msg_bytes: bytes) -> None:
        node_uid = uuid.UUID(bytes=msg_bytes)
        if self.load_every_inventory_nodes or (node_uid in self.cache._cached_inventory_nodes):
            async with self._lock:
                # reload node from database and replace the one in our cache:
                node = await self.graph_mgr.find_by_uid(node_uid)
                if node is None:
                    self.cache.invalidate_cached_node_by_uid(node_uid)
                else:
                    lca_parent_links = await self.graph_mgr.get_parents_by_uid(node_uid)
                    lca_child_links = await self.graph_mgr.get_sub_graph_by_uid(node_uid, max_depth=1)
                    self.cache.invalidate_cached_node_by_uid(node_uid)
                    self.cache.add_node(node)
                    for lca_link in lca_child_links + lca_parent_links:
                        self.cache.add_edge(lca_link)

                self.cache._binary_cache = None

                logger.debug("node was updated from db", node_uid=node_uid)

    async def _inventory_on_cache_invalidation_msg(self, _: bytes) -> None:
        if not self.load_every_inventory_nodes:
            async with self._lock:
                # reload all nodes and edges according to the updated inventory pruned_nodes_to_cache:
                lca_nodes = await self.graph_mgr.get_pruned_nodes_to_cache()
                lca_links = await self.graph_mgr.get_edges_below_pruned_nodes_to_cache()
                self.cache.clear()
                self.cache._cached_inventory_nodes = await self.graph_mgr.get_uids_of_inventory_nodes_to_cache()
                for lca_node in lca_nodes:
                    self.cache.add_node(lca_node)
                for lca_link in lca_links:
                    self.cache.add_edge(lca_link)
                self.cache._binary_cache = None
                self.cache.loaded = True

                logger.debug("all node caches were updated from db according to pruned_nodes_to_cache.")

    async def get_root_flow_or_activity(
        self, uid: uuid.UUID, conn: Optional[asyncpg.connection.Connection] = None, allow_deleted: bool = False
    ) -> Optional[Node]:
        """Get the root-flow or the root-activity defined by the uid.

        This method also add parent and child nodes if corresponding edges exist in the database.
        """
        recipe_node = await self.graph_mgr.get_root_flow_or_activity(uid, conn, allow_deleted)
        return recipe_node

    async def ensure_cache_is_loaded(self) -> None:
        if self.cache.loaded:
            # have this first check here, so we don't even need to acquire the lock if the cache is already loaded
            logger.debug("Node and edge caches are already initialized. Do nothing.")
            return

        async with self._lock:
            if self.cache.loaded:
                # we need this a second time here, in case another coroutine was waiting for the lock while a first one
                # was loading the cache
                logger.debug("Node and edge caches were already initialized in the meantime. Do nothing.")
                return

            import time

            start = time.time()
            logger.info("Initializing node and edge cache...")

            if api_settings.LOAD_CACHES_FROM_RUNNING_PODS:
                try:
                    self.cache._initialize_from_running_pods()
                except Exception as e:
                    logger.warn("Failed to initialize caches from other pods, fallback to DB", exception=str(e))
                    await self.cache._initialize_from_db(self.graph_mgr, self.load_every_inventory_nodes)
            else:
                await self.cache._initialize_from_db(self.graph_mgr, self.load_every_inventory_nodes)

            duration = time.time() - start
            logger.info(f"Node and edge cache initialization took {duration}s")

    async def update_and_broadcast_inventory_invalidation(self) -> None:
        if self.service_provider.messaging_service is None:
            # no messaging service, so we invalidate the cache directly just within this instance:
            await self._inventory_on_cache_invalidation_msg(b"")
        else:
            logger.debug("emitting a node service inventory invalidation.")
            await self.service_provider.messaging_service.emit_invalidate("inventory", b"")

    async def get_sub_graph_by_uid(
        self, uid: uuid.UUID, max_depth: int = 1, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Edge]:
        if max_depth == 1:
            try:
                return self.cache.get_sub_graph_by_uid(uid, max_depth)
            except (ValueError, KeyError):
                pass
                # cache was not used. We need to instead use connection to postgres:

        edges = await self.graph_mgr.get_sub_graph_by_uid(uid, max_depth=max_depth, conn=conn)
        return edges

    async def get_parents_by_uid(
        self, uid: uuid.UUID, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Edge]:
        try:
            return self.cache.get_parents_by_uid(uid)
        except KeyError:
            pass
            # cache was not used. We need to instead use connection to postgres:

        edges = await self.graph_mgr.get_parents_by_uid(uid, conn)
        return edges

    async def upsert_node_by_uid(self, node: Node) -> Node:
        updated_node = await self.graph_mgr.upsert_node_by_uid(node)
        if updated_node.node_type in CACHED_NODE_TYPES:
            await self._broadcast_cache_invalidation(updated_node.uid)
        return updated_node

    async def add_post_calc_cache_to_node_by_uid(
        self,
        uid: uuid.UUID,
        node_with_data: Node,
        final_graph_table: Optional[List[Dict[str, str]]] = None,
        final_graph_table_criterion: Optional[FinalGraphTableCriterion] = None,
    ) -> bool:
        meta_information = PostCalcDataMetaInformation(
            updated_at=datetime.now().date(), final_graph_table_criterion=final_graph_table_criterion
        )
        post_calc_cache = PostCalcCache(
            node=node_with_data, final_graph_table=final_graph_table, meta_information=meta_information
        )
        return await self.graph_mgr.add_post_calc_data_to_node_by_uid(uid, post_calc_cache)

    async def find_by_uid(self, uid: uuid.UUID, include_deleted_nodes: bool = False) -> Optional[Node]:
        try:
            node_from_cache = self.cache.get_cached_node_by_uid(uid)
            if getattr(node_from_cache, "aggregated_cache", None) is not None:
                node_from_cache.aggregated_cache._read_only = True
            return node_from_cache
        except KeyError:
            node = await self.graph_mgr.find_by_uid(uid, include_deleted_nodes=include_deleted_nodes)
            return node

    async def find_many_by_uids(
        self,
        uids: List[uuid.UUID],
        exclude_node_types: list[str] = None,
        include_deleted_nodes: bool = False,
        include_post_calc_data: bool = False,
    ) -> Dict[uuid.UUID, PreAndPostCalcNodes]:
        found_nodes: dict = {}
        nodes_to_find_from_db: set = set()
        for uid in uids:
            if include_post_calc_data:
                nodes_to_find_from_db.add(uid)
            else:
                try:
                    node_from_cache = self.cache.get_cached_node_by_uid(uid)
                    if exclude_node_types and str(node_from_cache.node_type) in exclude_node_types:
                        logger.debug(f"{node_from_cache} has a node_type that is one of {exclude_node_types}")
                    else:
                        if getattr(node_from_cache, "aggregated_cache", None) is not None:
                            node_from_cache.aggregated_cache._read_only = True
                        found_nodes[uid] = PreAndPostCalcNodes(pre_calc=node_from_cache)
                except KeyError:
                    nodes_to_find_from_db.add(uid)
        found_nodes.update(
            await self.graph_mgr.find_many_by_uids(
                uids=list(nodes_to_find_from_db),
                exclude_node_types=exclude_node_types,
                included_deleted_nodes=include_deleted_nodes,
                include_post_calc_data=include_post_calc_data,
            )
        )
        return found_nodes

    async def add_edge(self, edge: Edge) -> Edge:
        new_edge: Edge = await self.graph_mgr.add_edge(edge=edge)
        self.cache.invalidate_cached_node_by_uid(edge.parent_uid)
        if edge.edge_type == EdgeTypeEnum.lca_link:
            await self._broadcast_cache_invalidation(new_edge.parent_uid)
        return new_edge

    async def add_edges(self, edges: list[Edge], conn: Optional[asyncpg.connection.Connection] = None) -> None:
        await self.graph_mgr.add_edges(edges, conn=conn)

    async def delete_then_insert_many_edges(
        self,
        edges_to_delete: list[Edge],
        edges_to_insert: list[Edge],
        lock_on_uid: Optional[str] = None,
        md5_hash: Optional[uuid.UUID] = None,
    ) -> None:
        await self.graph_mgr.delete_then_insert_many_edges(
            edges_to_delete, edges_to_insert, lock_on_uid=lock_on_uid, md5_hash=md5_hash
        )

    async def delete_many_edges(self, edges_to_delete: list[Edge], lock_on_uid: Optional[str] = None) -> None:
        await self.graph_mgr.delete_many_edges(edges_to_delete, lock_on_uid=lock_on_uid)

    async def update_node_prop(
        self, node_uid: uuid.UUID, prop_name: str, prop: Prop | int, append: bool = False
    ) -> None:
        node_type, data = await self.graph_mgr.update_node_prop(node_uid, prop_name, prop, append)
        if node_type in CACHED_NODE_TYPES:
            await self._broadcast_cache_invalidation(node_uid)

    async def update_node_props(self, node: Node) -> bool:
        was_updated = await self.graph_mgr.update_node_props(node)
        if was_updated and node.node_type in CACHED_NODE_TYPES:
            await self._broadcast_cache_invalidation(node.uid)
        return was_updated

    async def update_node_access_group_uid(self, node_uid: uuid.UUID, access_group_uid: uuid.UUID) -> None:
        node_type, data = await self.graph_mgr.update_node_access_group_uid(node_uid, access_group_uid)
        if node_type in CACHED_NODE_TYPES:
            await self._broadcast_cache_invalidation(node_uid)

    async def delete_node_and_edges(self, node_uid: uuid.UUID) -> bool:
        was_deleted = await self.graph_mgr.delete_node_and_edges(str(node_uid))
        if was_deleted:
            # we need to invalidate the cache:
            self.cache.invalidate_cached_node_by_uid(node_uid)
        return was_deleted

    async def delete_many_nodes_and_edges(self, node_uids: list[uuid.UUID]) -> None:
        await self.graph_mgr.delete_many_nodes_and_edges([str(node_uid) for node_uid in node_uids])
        logger.info("Done deleting nodes and edges from database, Broadcasting cache invalidation.")
        for node_uid in node_uids:
            await self._broadcast_cache_invalidation(node_uid)
        self.cache._update_binary_cache()

    async def update_many_nodes_and_edges_from_local_cache(self, node_uids: list[uuid.UUID]) -> None:
        nodes_to_add = []
        edges_to_add = []
        edges_to_delete = []
        for node_uid in node_uids:
            # this is using the edges as they are in cache
            new_edges = await self.get_sub_graph_by_uid(node_uid, max_depth=1)
            # this is using the edges as they are in the database
            old_edges = await self.graph_mgr.get_sub_graph_by_uid(node_uid, max_depth=1)
            for old_edge in old_edges:
                if old_edge not in new_edges:
                    edges_to_delete.append(old_edge)
            edges_to_add.extend(new_edges)
            nodes_to_add.append(self.cache.get_cached_node_by_uid(node_uid))

        # Because of db table edge's REFERENCES, we always insert nodes first, then edges.
        await self.graph_mgr.add_nodes(nodes_to_add)
        await self.graph_mgr.add_edges(edges_to_add)
        await self.delete_many_edges(edges_to_delete)

        for node in nodes_to_add:
            if node.node_type in CACHED_NODE_TYPES:
                await self._broadcast_cache_invalidation(node.uid)
        self.cache._update_binary_cache()

    async def create_recipe(self, **kwargs: Any) -> Node:  # noqa: ANN401
        """Directly insert a recipe and its sub-recipe & ingredients."""
        if "uid" not in kwargs:
            kwargs["uid"] = str(uuid.uuid4())

        new_recipe = Node(**kwargs)

        return await self.graph_mgr.upsert_node_by_uid(new_recipe)

    async def find_access_group_uid_by_uid(self, uid: uuid.UUID) -> uuid.UUID:
        try:
            cached_node = self.cache.get_cached_node_by_uid(uid)
            access_group_uid = cached_node.access_group_uid
        except KeyError:
            access_group_uid = None

        if not access_group_uid:
            access_group_uid = await self.graph_mgr.find_access_group_uid_by_uid(uid)
            if access_group_uid is not None:
                access_group_uid = uuid.UUID(access_group_uid)

        return access_group_uid

    async def mark_recipe_as_deleted(self, recipe_node: Node) -> None:
        await self.graph_mgr.mark_recipe_as_deleted(recipe_node.uid)

    async def upsert_by_recipe_id(
        self, recipe_node: Node, transient: bool = False
    ) -> tuple[list[EdgeToUpsert], list[EdgeToUpsert], str, Optional[str], bool, Optional[uuid.UUID]]:
        # Assign uids. This mimics the behaviour from graph_mgr.upsert_node_by_uid which creates uids
        # if they are None.
        if not recipe_node.uid:
            recipe_node.uid = uuid.uuid4()

        (
            edges_to_delete,
            edges_to_insert,
            self_uid,
            parent_uid,
            not_changed,
            md5_hash,
        ) = await self.graph_mgr.upsert_recipe(recipe_node, transient)

        return edges_to_delete, edges_to_insert, self_uid, parent_uid, not_changed, md5_hash
        ###################################################################################################
        # The linking to sub-recipes now happens in add_client_nodes_gfm dynamically during a calculation.
        # That allows us to run batch requests where there might be sub-recipes that are not yet created.
        # Leaving the old code here, just in case we need to revert back to this approach at some point:
        #
        # if isinstance(sub_node, LegacyRecipeIngredientFlowNode):
        #
        #     if created_sub_node.raw_input.linked_recipe_uid is None:
        #         # in batch requests it can happen that the linked recipe does not exist yet, when saving
        #         # the node during the first pass through the batch. So then we need to skip it here and
        #         # instead they will be linked during calculation in gfm.
        #         continue
        #
        #     # find sub-recipe in db:
        #     sub_recipe_node = await self.graph_mgr.find_by_uid(sub_node.uid, conn)
        #
        #     edge = Edge(parent_uid=sub_recipe_node.uid,
        #                 child_uid=created_sub_node.raw_input.linked_recipe_uid,
        #                 edge_type=EdgeTypeEnum.link_to_subrecipe)
        #
        #     await self.graph_mgr.add_edge(conn=conn, edge=edge)
        ###################################################################################################

    async def map_xid_access_group_uids_to_uids(
        self, xid_access_group_uids_to_map_to_uid: list[XidAccessGroupUid]
    ) -> dict[XidAccessGroupUid, uuid.UUID]:
        return await self.product_mgr.map_xid_access_group_uids_to_uids(xid_access_group_uids_to_map_to_uid)

    async def process_link_to_subnode_edges(
        self,
        batch_activity_uid: str,
        edges_to_insert: [EdgeToUpsert],
        edges_to_delete: [EdgeToUpsert],
        md5_hash: Optional[uuid.UUID],
        xid_access_group_uids_to_map_to_uid: list[XidAccessGroupUid],
    ) -> tuple[str, bool]:
        if xid_access_group_uids_to_map_to_uid:
            mapping_dict = await self.map_xid_access_group_uids_to_uids(xid_access_group_uids_to_map_to_uid)
        else:
            mapping_dict = {}
        edges_to_insert = [
            Edge(
                parent_uid=edge.parent_uid,
                child_uid=edge.child_uid if edge.child_uid else mapping_dict.get(edge.child_xid_access_group_uid),
                edge_type=EdgeTypeEnum.link_to_subrecipe,
            )
            for edge in edges_to_insert
        ]
        edges_to_delete = [
            Edge(
                parent_uid=edge.parent_uid,
                child_uid=edge.child_uid,
                edge_type=EdgeTypeEnum.link_to_subrecipe,
            )
            for edge in edges_to_delete
        ]

        def remove_common_edges(list1: list[Edge], list2: list[Edge]) -> tuple[list, list]:
            # Create a copy of the lists to avoid modifying them while iterating
            list1_copy = list1[:]
            list2_copy = list2[:]

            # Iterate through each dictionary in the first list
            for d in list2_copy:
                if d in list1_copy:
                    # If the dictionary is in both lists, remove it from both
                    list1.remove(d)
                    list2.remove(d)
            return list1, list2

        edges_to_insert_cleaned, edges_to_delete_cleaned = remove_common_edges(edges_to_insert, edges_to_delete)

        if edges_to_delete_cleaned or edges_to_insert_cleaned:
            try:
                await self.delete_then_insert_many_edges(
                    edges_to_delete_cleaned,
                    edges_to_insert_cleaned,
                    lock_on_uid=str(batch_activity_uid),
                    md5_hash=md5_hash,
                )
            except ForeignKeyViolationError:
                await self.delete_many_edges(edges_to_delete, lock_on_uid=str(batch_activity_uid))
                return str(batch_activity_uid), False
        return str(batch_activity_uid), True

    async def upsert_recipe_and_insert_edges(self, recipe: Node) -> bool:
        edges_to_delete, edges_to_insert, _, _, skipped_upsert, md5_hash = await self.upsert_by_recipe_id(recipe)
        xid_access_group_uids_to_map_to_uid = [
            edge.child_xid_access_group_uid for edge in edges_to_insert if edge.child_xid_access_group_uid is not None
        ]
        await self.process_link_to_subnode_edges(
            str(recipe.uid), edges_to_insert, edges_to_delete, md5_hash, xid_access_group_uids_to_map_to_uid
        )
        return skipped_upsert

    async def clear_mock_legacy_co2_nodes(self) -> None:
        updated_node_uids = await self.graph_mgr.clear_mock_legacy_co2_nodes()
        for node_uid in updated_node_uids:
            await self._broadcast_cache_invalidation(node_uid=node_uid)
            await self._node_on_cache_invalidation_msg(node_uid.bytes)
