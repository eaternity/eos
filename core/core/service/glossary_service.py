import gzip
import uuid
from io import BytesIO
from typing import TYPE_CHECKING, List, Optional
from uuid import UUID

import httpx
import msgpack
from structlog import get_logger

from api.app.settings import Settings
from core.domain.term import Term
from database.postgres.settings import PgSettings

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

api_settings = Settings()

logger = get_logger()
settings = PgSettings()

TermsByXidKey = (str, UUID)


class GlossaryService:
    """A service that exposes the glossary to the rest of the system. Keeps a cache of terms in memory."""

    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.pg_term_mgr = None

        # terms' cache. Key: term uid; value: term
        self.terms_by_uid: dict[UUID, Term] = dict()
        # binary version of terms' cache
        self.terms_by_uid_binary: bytes = None

        # additional dict with tuple key: (term_xid, access_group_uid)
        self.terms_by_xid_ag_uid: dict[TermsByXidKey, Term] = dict()

        self.alias_to_xid: dict[str, str] = dict()

        self.root_term = None
        self.root_subterms: dict[str, Term] = dict()

        self.child_terms_by_parent_uid: dict[int, List[Term]] = dict()

    async def init(self) -> None:
        if self.terms_by_uid and self.terms_by_xid_ag_uid:
            raise RuntimeError("Glossary cache is already initialized! Should not call init again!")

        self.pg_term_mgr = self.service_provider.postgres_db.get_term_mgr()

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None and api_settings.SUBSCRIBE_CACHE_INVALIDATIONS:
            await self.service_provider.messaging_service.subscribe_cache_invalidation(
                "term", self._on_cache_invalidation_msg
            )

        logger.info("start initializing glossary cache...")

        # get terms from other pods, or DB
        self.terms_by_uid_binary = None
        all_terms = (
            await self._get_all_terms_from_running_pods()
            if api_settings.LOAD_CACHES_FROM_RUNNING_PODS
            else await self.pg_term_mgr.find_all()
        )

        # Insert all terms into the cache
        for term in all_terms:
            self._put_term_into_cache(term)
            for aliased_unit in term.data.get("alias", []):
                self.alias_to_xid[aliased_unit.upper()] = term.xid

        # reinitialize binary cache, not needed if we got the binary from running pods
        if self.terms_by_uid_binary is None:
            self._update_binary_cache()

        logger.info("done initializing glossary cache...")
        logger.debug("start initializing root glossary term subterms cache...")

        self.root_term = self.terms_by_uid.get(uuid.UUID(settings.ROOT_GLOSSARY_TERM_UUID))

        for term in all_terms:
            if term.sub_class_of == uuid.UUID(settings.ROOT_GLOSSARY_TERM_UUID):
                self.root_subterms[term.xid] = term

        logger.debug("done initializing root glossary term subterms cache...")

    def _update_binary_cache(self) -> None:
        logger.info("Updating binary glossary cache...")

        with BytesIO() as fileobj:
            fileobj.mode = "wb"

            with gzip.GzipFile(mode="wb", compresslevel=9, fileobj=fileobj) as compressor:
                msgpack.pack([Term.serialize(term) for term in self.terms_by_uid.values()], compressor)
                compressor.flush()

            self.terms_by_uid_binary = fileobj.getvalue()

        logger.info("Done updating binary glossary cache...")

    async def _get_all_terms_from_running_pods(self) -> list[Term]:
        try:
            logger.info("Fetch binary glossary cache from other pods...")
            response = httpx.get(
                url=f"{api_settings.api_url}/glossary/cache",
                headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
                timeout=5,
            )

            if response.status_code != 200:
                raise Exception(f"Failed to fetch glossary cache from other pods: {response}")
            self.terms_by_uid_binary = response.content

            logger.info("Deserialize binary glossary cache...")
            with gzip.GzipFile(fileobj=BytesIO(self.terms_by_uid_binary), mode="rb") as decompressor:
                all_terms = msgpack.unpack(decompressor, object_hook=Term.deserialize)

            logger.info("Done getting binary glossary cache...")
            return all_terms
        except Exception as e:
            logger.warn("Failed to fetch glossary from other pods, fallback to DB", exception=str(e))
            return await self.pg_term_mgr.find_all()

    def _put_term_into_cache(self, term: Term) -> None:
        # first check if it already existed in cache:
        old_term = self.terms_by_uid.get(term.uid, None)
        if old_term:
            # we need to make sure that the key in `terms_by_xid_ag_uid` is correctly updated
            # First remove item from dict using the old key
            key: TermsByXidKey = (old_term.xid, old_term.access_group_uid)
            del self.terms_by_xid_ag_uid[key]

            # also make sure to delete the old sub_class_of in `child_terms_by_parent_uid`
            if old_term.sub_class_of is not None:
                if old_term.sub_class_of.int in self.child_terms_by_parent_uid:
                    self.child_terms_by_parent_uid[old_term.sub_class_of.int].remove(old_term)

        # now insert new item:
        self.terms_by_uid[term.uid] = term
        key: TermsByXidKey = (term.xid, term.access_group_uid)
        if key in self.terms_by_xid_ag_uid:
            raise ValueError("tuple term_xid access_group_uid must be unique. found duplicate key with different uid")
        self.terms_by_xid_ag_uid[key] = term

        if term.uid == uuid.UUID(settings.ROOT_GLOSSARY_TERM_UUID):
            self.root_term = term

        if term.sub_class_of == uuid.UUID(settings.ROOT_GLOSSARY_TERM_UUID):
            self.root_subterms[term.xid] = term

        if term.sub_class_of is not None:
            self.child_terms_by_parent_uid.setdefault(term.sub_class_of.int, []).append(term)

    def _remove_term_from_cache(self, term_uid: uuid.UUID) -> None:
        # allow deletion only by uid because we need to keep both dict's in sync
        term = self.terms_by_uid.get(term_uid, None)
        if term:
            xid_key: TermsByXidKey = (term.xid, term.access_group_uid)
            del self.terms_by_xid_ag_uid[xid_key]
            del self.terms_by_uid[term_uid]
            # also update the sub_class_of in `child_terms_by_parent_uid`
            if term.sub_class_of is not None:
                if term.sub_class_of.int in self.child_terms_by_parent_uid:
                    self.child_terms_by_parent_uid[term.sub_class_of.int].remove(term)

    def clear_cache(self) -> None:
        self.terms_by_uid.clear()
        self.terms_by_xid_ag_uid.clear()
        self.child_terms_by_parent_uid.clear()

    async def _on_cache_invalidation_msg(self, msg_bytes: bytes) -> None:
        term_uid = UUID(bytes=msg_bytes)

        # reload term from database and replace the one in our cache:
        term = await self.pg_term_mgr.get_term_by_uid(term_uid)

        if term is not None:
            self._put_term_into_cache(term)
        else:
            self._remove_term_from_cache(term_uid)

        self.terms_by_uid_binary = None

        logger.debug("term was updated from db", term_uid=term_uid)

    def get_term_by_id(self, uid: UUID) -> Optional[Term]:
        return self.terms_by_uid[uid]

    async def get_term_by_uid(self, uid: UUID) -> Optional[Term]:
        if uid in self.terms_by_uid:
            return self.terms_by_uid[uid]
        else:
            return await self.pg_term_mgr.get_term_by_uid(uid)

    async def get_terms_by_xid(self, term_xid: str) -> Optional[list]:
        # TODO: this cannot efficiently use the cache currently.
        #  We should check if we can completely get rid of this access type (i.e. by xid but without namespace)
        terms = await self.pg_term_mgr.get_terms_by_xid(term_xid)
        return terms

    async def get_term_by_xid_and_access_group_uid(self, term_xid: str, access_group_uid: str) -> Optional[Term]:
        # TODO: does not have to be async
        key = (term_xid, UUID(access_group_uid))
        return self.terms_by_xid_ag_uid.get(key, None)

    async def put_term_by_uid(self, term: Term) -> Optional[Term]:
        term = await self.pg_term_mgr.insert_term(term)

        # first we update our local cache in this worker
        self._put_term_into_cache(term)

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None:
            logger.debug("emitting a cache invalidation for glossary term", term_uid=term.uid)
            await self.service_provider.messaging_service.emit_invalidate("term", term.uid.bytes)

        return term

    async def put_many_terms(self, terms: List[Term]) -> None:
        await self.pg_term_mgr.insert_many_terms(terms)
        await self.update_cache_for_many_terms(terms)

    async def update_cache_for_many_terms(self, terms: List[Term]) -> None:
        for term in terms:
            # first we update our local cache in this worker
            self._put_term_into_cache(term)

            # Optional messaging_service in case multiple worker caches need to be synchronized
            # (many unittests don't need it, so better to keep it optional)
            if self.service_provider.messaging_service is not None:
                logger.debug("emitting a cache invalidation for glossary term", term_uid=term.uid)
                await self.service_provider.messaging_service.emit_invalidate("term", term.uid.bytes)

    def get_sub_terms_by_uid(self, term_uid: UUID, depth: int = 10) -> List[Term]:
        if term_uid not in self.terms_by_uid:
            return []

        # Use DFS to get sub-terms up to the specified depth
        def recursive_gather_sub_terms(
            parent_term: Term, current_depth: int, max_depth: int, result: List[Term]
        ) -> None:
            if current_depth > max_depth:
                return
            if parent_term.uid.int not in self.child_terms_by_parent_uid:
                return
            for child_term in self.child_terms_by_parent_uid[parent_term.uid.int]:
                result.append(child_term)
                recursive_gather_sub_terms(child_term, current_depth + 1, max_depth, result)

        starting_term = self.terms_by_uid[term_uid]
        all_children = []
        recursive_gather_sub_terms(starting_term, 1, depth, all_children)
        return all_children
