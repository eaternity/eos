from core.domain.user import UserPermissionsEnum

# basic actions


def check_if_can_delete_node(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.delete, False)


def check_if_can_post_node(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.post, False)


def check_if_can_read_node(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.read, False)


def check_if_can_update_node(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.update, False)


# access group management actions


def check_if_is_group_admin(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.is_group_admin, False)


def check_if_can_add_members(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.add_users_to_group, False)


def check_if_can_create_access_group(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.create_access_group, False)


def check_if_can_delete_access_group(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.delete_access_group, False) or check_if_is_group_admin(permissions)


def check_if_can_delete_members(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.delete_users_from_group, False)


def check_if_can_give_permissions(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.give_permissions_in_access_group, False)


def check_if_can_view_members(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.view_access_group_members, False)


# child access group actions


def check_if_can_delete_in_child_access_group(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.child_access_group_delete, False)


def check_if_can_give_permissions_in_child_access_group(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.give_permissions_in_child_access_group, False)


def check_if_can_post_in_child_access_group(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.child_access_group_post, False)


def check_if_can_read_in_child_access_group(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.child_access_group_read, False)


def check_if_can_update_in_child_access_group(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.child_access_group_update, False)


# glossary terms actions


def check_if_can_create_terms(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.create_terms, False)


def check_if_can_edit_terms(permissions: dict) -> bool:
    return permissions.get(UserPermissionsEnum.edit_terms, False)


def check_if_all_access_groups_can_read_node(permissions_by_access_group_uid: dict, access_group_uids: list) -> bool:
    allowed = all(
        permissions_by_access_group_uid.get(access_group_uid, {}).get(UserPermissionsEnum.read.value, False)
        for access_group_uid in access_group_uids
    )
    return allowed
