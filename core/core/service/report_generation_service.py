from datetime import date
from typing import TYPE_CHECKING, List, Optional

from core.domain.calculation import Calculation
from core.domain.post_calc_cache import NodeCacheInfo

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider


class ReportGenerationService:
    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider

    async def init(self) -> None:
        self.graph_mgr = self.service_provider.postgres_db.graph_mgr
        self.calculation_mgr = self.service_provider.postgres_db.pg_calc_mgr

    async def get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
        self,
        access_group_uids: List[str],
        start_date: date,
        end_date: date,
        node_type: Optional[str] = None,
        ignore_deleted: bool = False,
        get_tabular_cache: bool = False,
    ) -> List[NodeCacheInfo]:
        node_cache_infos = await self.graph_mgr.get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
            access_group_uids,
            start_date,
            end_date,
            node_type=node_type,
            ignore_deleted=ignore_deleted,
            get_tabular_cache=get_tabular_cache,
        )
        return node_cache_infos

    async def get_all_calculations_by_root_node_uids(
        self,
        uids: List[str],
    ) -> List[Calculation]:
        all_calculations = await self.calculation_mgr.find_all_by_root_node_uids(uids)
        return all_calculations
