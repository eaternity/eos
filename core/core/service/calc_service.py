"Calculation Service."
import asyncio
import copy
import gc
import io
import os
import traceback
import typing
from contextlib import asynccontextmanager
from typing import AsyncGenerator, Tuple

import psutil
from pydantic_settings import SettingsConfigDict
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.data_error import DataError
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes.activity.elementary_resource_emission_node import ElementaryResourceEmissionNode
from core.domain.nodes.activity.food_processing_activity_node import FoodProcessingActivityNode
from core.domain.nodes.activity.supply_sheet_activity import SupplySheetActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node
from core.domain.nodes.root_with_subflows_dto import RootWithSubFlowsDto
from core.domain.props.quantity_prop import ReferenceAmountEnum
from core.domain.util import get_system_process_data
from core.envprops import EnvProps
from core.graph_manager.calc_graph import CalcGraph
from core.graph_manager.sheet_converter import OutputGraphToTableConverter
from core.orchestrator.exceptions import GapFillingModuleError
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.orchestrator.orchestrator import Orchestrator
from core.service.glossary_service import GlossaryService
from core.service.node_service import NodeService

if typing.TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

logger = get_logger()


class CalcServiceSettings(EnvProps):
    "Calculation Service settings."
    MAX_CONCURRENT_ORCHESTRATIONS: int = 3
    ENABLE_PROFILING: bool = False
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


class OrchestratorAtLimitException(Exception):  # noqa: D101
    pass


class MemoryAwareSemaphore:
    def __init__(self):
        self._high_watermark = CalcServiceSettings().HIGH_MEMORY_WATERMARK_MIB
        self._low_watermark = CalcServiceSettings().LOW_MEMORY_WATERMARK_MIB
        self._tasks_running = 0
        self._is_constrained = False
        self._condition = asyncio.Condition()

    def _check_memory(self) -> float:
        current_process = psutil.Process()
        memory_info = current_process.memory_info().rss
        memory_usage_mib = memory_info / (1024 * 1024)
        return memory_usage_mib

    def _update_mode(self) -> None:
        memory_usage_mib = self._check_memory()
        if memory_usage_mib > self._high_watermark:
            self._is_constrained = True
            logger.warn(
                "Calculation is put on hold due to high memory watermark exceeded.",
                memory_usage_mib=memory_usage_mib,
            )
        elif memory_usage_mib < self._low_watermark:
            self._is_constrained = False

    def get_free_slot_count(self) -> int:
        self._update_mode()
        if self._is_constrained:
            num_slots = 1
        else:
            num_slots = CalcServiceSettings().MAX_CONCURRENT_ORCHESTRATIONS
        return max(num_slots - self._tasks_running, 0)

    @asynccontextmanager
    async def acquire(self) -> AsyncGenerator[None, None]:
        async with self._condition:
            while self.get_free_slot_count() < 1:
                logger.debug("Waiting for free slot.")
                await self._condition.wait()
            self._tasks_running += 1
            logger.debug("Acquired free slot.", tasks_running=self._tasks_running)

        try:
            yield
        finally:
            async with self._condition:
                self._tasks_running -= 1
                logger.debug("Released free slot.", tasks_running=self._tasks_running)
                num_to_wake = self.get_free_slot_count()
                if self._is_constrained:
                    logger.debug("Memory constrained, run gc collect.")
                    gc.collect()
                if num_to_wake > 0:
                    logger.debug("Notifying free slot.", num_to_wake=num_to_wake)
                    self._condition.notify(num_to_wake)


class CalcService:
    "Calculation Service."

    def __init__(
        self,
        service_provider: "ServiceProvider",
        glossary_service: GlossaryService,
        node_service: NodeService,
        gap_filling_module_loader: GapFillingModuleLoader,
    ):
        self.service_provider = service_provider
        self.glossary_service = glossary_service
        self.gap_filling_module_loader = gap_filling_module_loader
        self.node_service = node_service
        self.semaphore = MemoryAwareSemaphore()
        self.enable_profiling = CalcServiceSettings().ENABLE_PROFILING

    def create_calc_graph_and_orchestrator(self, calculation: Calculation) -> Tuple[CalcGraph, Orchestrator]:
        """Create calculation graph and GFM orchestrator.

        Returns:
            tuple: a tuple of calculation graph and the orchestrator.
        """
        # create the calculation graph:
        logger.debug("creating the calculation graph...")
        calc_graph = CalcGraph(
            self.service_provider,
            root_node_uid=calculation.root_node_uid,
            linked_sub_node_infos=calculation.linked_sub_node_infos,
            glossary_service=self.glossary_service,
            calculation=calculation,
        )

        # initialize orchestrator with the calculation graph:
        logger.debug("creating the orchestrator...")
        orchestrator = Orchestrator(
            calc_graph=calc_graph,
            glossary_service=self.glossary_service,
            gap_filling_module_loader=self.gap_filling_module_loader,
        )
        return calc_graph, orchestrator

    async def calculate(self, calculation: Calculation, raise_if_full: bool = False) -> Tuple[Calculation, Node]:
        """Run the calculation.

        Args:
            calculation: Calculation object
            raise_if_full: If True, it'll raise `OrchestratorAtLimitException` if the semaphore is already at its limit.

        Returns:
            tuple: a tuple of calculation and root node
        """
        if not calculation.linked_sub_node_infos:
            # We have to try prepending root flow by accessing the node table
            # because currently in the calculation database,
            # child_of_root_node_uid is persisted as the root_node_uid.
            # See FIXME in pg_calculation_mgr.
            if calculation.root_node_uid is None:
                parent_edges = await self.service_provider.node_service.get_parents_by_uid(
                    calculation.child_of_root_node_uid
                )
                if parent_edges:
                    root_flow_edge = None
                    for parent_edge in parent_edges:
                        if parent_edge.edge_type == EdgeTypeEnum.root_to_recipe.value:
                            root_flow_edge = parent_edge
                            break

                    if root_flow_edge:
                        calculation.root_node_uid = root_flow_edge.parent_uid

            if calculation.root_node_uid is None:
                child_of_root_node_uid = calculation.child_of_root_node_uid
                graph_mgr = self.service_provider.postgres_db.graph_mgr
                child_of_root_node = await self.service_provider.node_service.find_by_uid(child_of_root_node_uid)
                if isinstance(child_of_root_node, (FoodProcessingActivityNode, SupplySheetActivityNode)):
                    root_flow = await graph_mgr.upsert_node_by_uid(
                        FoodProductFlowNode(
                            access_group_uid=child_of_root_node.access_group_uid,
                            access_group_xid=child_of_root_node.access_group_xid,
                            namespace_uid=child_of_root_node.namespace_uid,
                        )
                    )
                elif child_of_root_node is not None:
                    # By default we add a generic FlowNode.
                    root_flow = await graph_mgr.upsert_node_by_uid(
                        FlowNode(
                            access_group_uid=child_of_root_node.access_group_uid,
                            access_group_xid=child_of_root_node.access_group_xid,
                            namespace_uid=child_of_root_node.namespace_uid,
                        )
                    )
                else:
                    root_flow = None
                if root_flow is not None:
                    await graph_mgr.add_edge(
                        Edge(
                            parent_uid=root_flow.uid,
                            child_uid=child_of_root_node_uid,
                            edge_type=EdgeTypeEnum.root_to_recipe,
                        )
                    )
                    calculation.root_node_uid = root_flow.uid

        if raise_if_full and self.semaphore.get_free_slot_count() < 1:
            raise OrchestratorAtLimitException("The orchestrator is already at its limit of maximum concurrent runs.")

        # start running the orchestrator, which will internally call gap filling modules:
        async with self.semaphore.acquire():
            if not self.enable_profiling:
                calculation, calculated_root_node = await self.create_and_run_orchestrator(calculation=calculation)
            else:
                import cProfile
                import pstats
                from datetime import datetime

                # Profile the calculation if profiling is enabled
                with cProfile.Profile() as pr:
                    calculation, calculated_root_node = await self.create_and_run_orchestrator(calculation=calculation)

                # Create timestamp for unique filenames
                timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")

                # Save profiling results
                out_dir = os.path.join("./profiling/temp_data", "calculations")
                if not os.path.exists(out_dir):
                    os.makedirs(out_dir)
                csv_file_path = os.path.join(out_dir, f"profile_{timestamp}_{calculation.uid}.csv")
                prof_file_path = os.path.join(out_dir, f"profile_{timestamp}_{calculation.uid}.prof")

                # Generate stats and save to files
                result_stream = io.StringIO()
                stats = pstats.Stats(pr, stream=result_stream).sort_stats("cumulative")
                stats.print_stats()
                stats.dump_stats(prof_file_path)

                # Format and save CSV
                result_str = result_stream.getvalue()
                result_str = "ncalls" + result_str.split("ncalls")[-1]
                result_str = "\n".join([",".join(line.rstrip().split(None, 5)) for line in result_str.split("\n")])
                with open(csv_file_path, "w+") as f:
                    f.write(result_str)

                logger.info(
                    "profiling results saved",
                    csv_path=csv_file_path,
                    prof_path=prof_file_path,
                )

            return calculation, calculated_root_node

    async def create_and_run_orchestrator(self, calculation: Calculation) -> tuple[Calculation, Node]:
        calc_graph, orchestrator = self.create_calc_graph_and_orchestrator(calculation)

        try:
            await orchestrator.run()
        except GapFillingModuleError as e:
            calculation.data_errors.append(DataError.from_gfm_error(e))
            calculation.success = False

        calculated_root_node = calc_graph.get_root_node()
        calculated_child_of_root_node = calc_graph.child_of_root_node()

        if calculation.return_log:
            calculation.mutations = calc_graph.get_mutation_log()

        if calculation.return_final_graph:
            calculation.final_graph = await calc_graph.get_serialized_graph(
                calculation.exclude_node_props, calculation.max_depth_of_returned_graph
            )

        def _get_declaration_index_value(node: Node) -> int | float:
            if hasattr(node, "declaration_index"):
                declaration_index = node.declaration_index
                if declaration_index:
                    return declaration_index.value
            return float("inf")  # Append nodes with no declaration index at the end

        if calculation.return_final_root:
            if calculation.final_root is None:
                all_sub_flows = list(calculated_child_of_root_node.get_sub_nodes())
                sorted_sub_flows = sorted(all_sub_flows, key=_get_declaration_index_value)
                calculation.final_root = RootWithSubFlowsDto(
                    flow=calculated_root_node,
                    activity=calculated_child_of_root_node,
                    sub_flows=sorted_sub_flows,
                )
            else:
                calculation.final_root.flow = calculated_root_node
                calculation.final_root.activity = calculated_child_of_root_node
                all_sub_flows = list(calculated_child_of_root_node.get_sub_nodes())
                sorted_sub_flows = sorted(all_sub_flows, key=_get_declaration_index_value)
                calculation.final_root.sub_flows = sorted_sub_flows

        if calculation.save_as_system_process and len(calculation.data_errors) == 0:
            await self.save_as_system_process(calc_graph)

        # flush the applied_matchings
        await self.service_provider.matching_service.flush_applied_matchings_to_db()

        calculation.data_errors.extend(calc_graph.data_errors_log)

        if calculation.return_data_as_table:
            # TODO: Currently we can only give meaningful quantities for all columns with amount_for_root_node
            # as reference. Therefore by default (i.e. if not explicitly specified otherwise by the client)
            # we always set the reference to amount_for_root_node.
            # Note also that for the table-cache we want to have amount_for_root_node as reference in the table
            # while for the node-cache we want to have amount_for_activity_production_amount as reference.
            references: list[ReferenceAmountEnum] = (
                calculation.requested_quantity_references
                if calculation.requested_quantity_references
                else [ReferenceAmountEnum.amount_for_root_node]
            )

            client_set_amount_for_reference = copy.deepcopy(calculation.requested_quantity_references)
            calculation.requested_quantity_references = references
            try:
                calculation.final_graph_table = OutputGraphToTableConverter().run(
                    calc_graph,
                    calculation.max_depth_of_returned_table,
                    references,
                    calculation.final_graph_table_criterion,
                )
            except Exception as e:
                logger.error(
                    "Error: Failed converting to table output",
                    exception=e,
                    traceback="".join(traceback.format_exception(e)),
                )
                calculation.data_errors.append(DataError(message="Error: Failed converting to table output"))
            finally:
                calculation.requested_quantity_references = client_set_amount_for_reference

        def _verify_node_calc_result(node: Node) -> bool:
            if node.impact_assessment is None:
                return False
            if isinstance(node, FoodProductFlowNode):
                if node.daily_food_unit is None:
                    return False
                if node.vitascore is None:
                    return False
            return True

        if calculation.cache_final_root:
            for uid, node in (
                (calculation.root_node_uid, calculated_root_node),
                (calculation.child_of_root_node_uid, calculated_child_of_root_node),
            ):
                if _verify_node_calc_result(node):
                    # for deserializing the node we only want to have the amount_for_activity_production_amount
                    # and not the amount_for_root_node
                    client_set_amount_for_reference = copy.deepcopy(calculation.requested_quantity_references)
                    calculation.requested_quantity_references = [
                        ReferenceAmountEnum.amount_for_activity_production_amount
                    ]
                    success = False
                    try:
                        success = await self.service_provider.node_service.add_post_calc_cache_to_node_by_uid(
                            uid=uid,
                            node_with_data=node,
                            final_graph_table=calculation.final_graph_table,
                            final_graph_table_criterion=calculation.final_graph_table_criterion,
                        )
                    except Exception as e:
                        logger.error(
                            f"Failed to add post-calculation data of {node}.",
                            exception=e,
                            traceback="".join(traceback.format_exception(e)),
                        )
                        calculation.data_errors.append(
                            DataError(message=f"Failed to add post-calculation data of {node}.")
                        )
                    finally:
                        calculation.requested_quantity_references = client_set_amount_for_reference
                        if not success:
                            logger.warning(f"Failed to add post-calculation data of {node}.")

        return calculation, calculated_root_node

    async def save_as_system_process(self, calc_graph: CalcGraph) -> None:
        """Save nodes as system processes.

        To separate the loaded cache from the calculated properties of a currently running calculation, we wrap the
        specific properties in a Prop object with the name "aggregated_cache". This way, we can easily distinguish them.
        :param calc_graph: CalcGraph of the calculation
        """
        for current_node_uid in calc_graph.nodes_to_calculate_supply:
            current_node = calc_graph.get_node_by_uid(current_node_uid)
            if isinstance(current_node, ElementaryResourceEmissionNode):
                # Emission nodes do not have environmental flows to cache.
                continue
            aggregated_cache = get_system_process_data(current_node, calc_graph)
            await self.node_service.update_node_prop(
                current_node.uid, "aggregated_cache", aggregated_cache, append=False
            )

    async def find_by_uid(self, uid: str) -> Calculation:
        "Find UID in postgres calculation manager."
        pg_calc_mgr = self.service_provider.postgres_db.get_calc_mgr()
        return await pg_calc_mgr.find_by_uid(uid)
