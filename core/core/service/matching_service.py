import asyncio
import gzip
import json
import time
import unicodedata
import uuid
from dataclasses import dataclass
from io import BytesIO
from typing import TYPE_CHECKING, Dict, Optional
from uuid import UUID

import httpx
import msgpack
from structlog import get_logger

from api.app.settings import Settings
from core.domain.matching_item import MatchingItem
from core.domain.term import Term
from core.service.glossary_service import GlossaryService
from database.postgres.pg_matching_mgr import PgMatchingMgr

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

api_settings = Settings()
logger = get_logger()


# we normalize the matching string to NFD to make sure that we don't have any unicode ambiguity issues
UNICODE_NORMALIZATION_FORM = "NFD"


@dataclass(slots=True)
class CachedMatchingItem:
    term_uids: tuple[str, ...]
    gap_filling_module: str
    lang: str
    matched_node_uid: Optional[str] = None
    data: Optional[dict] = None

    @classmethod
    def serializer(cls: type["CachedMatchingItem"], obj: object) -> dict:
        if isinstance(obj, CachedMatchingItem):
            return {
                "cls": CachedMatchingItem.__name__,
                "t": obj.term_uids,
                "g": obj.gap_filling_module,
                "l": obj.lang,
                "m": obj.matched_node_uid,
                "d": obj.data,
            }
        elif isinstance(obj, uuid.UUID):
            return {"cls": uuid.UUID.__name__, "value": str(obj)}
        else:
            return obj

    @classmethod
    def deserializer(cls: type["CachedMatchingItem"], obj: object) -> object:
        if "cls" in obj:
            if obj["cls"] == CachedMatchingItem.__name__:
                return CachedMatchingItem(
                    term_uids=tuple(obj["t"]),
                    gap_filling_module=obj["g"],
                    lang=obj["l"],
                    matched_node_uid=obj["m"] if obj["m"] else None,
                    data=obj["d"] if obj["d"] else None,
                )
            elif obj["cls"] == uuid.UUID.__name__:
                return uuid.UUID(obj["value"])
        return obj


@dataclass(slots=True)
class AppliedMatchingItem:
    node_uid: str
    root_node_uid: str
    matching_uid: str
    matching_context: Dict[str, str]


class MatchingService:
    """A service that exposes matchings to the rest of the system. Keeps a cache of matchings in memory."""

    # Invalidating matching items recomputes the binary cache once per item.
    # If more items than expected get invalidated together, we might need some optimization.
    MAX_EXPECTED_INVALIDATED_MATCHING_ITEMS = 100

    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.pg_matching_mgr: PgMatchingMgr = None  # noqa
        self.glossary_service: GlossaryService = None  # noqa

        # Cache; allows multiple terms to be associated with a matching str.
        # key: matching str (lowercased!);
        # value: dict that maps from uid to the CachedMatchingItem
        self._terms_by_matching_str: dict[str, dict[str, CachedMatchingItem]] = dict()
        # binary version of _terms_by_matching_str
        self._terms_by_matching_str_binary: bytes = None

        self._applied_matching_items: list[AppliedMatchingItem] = []
        self.background_tasks = set()

    async def init(self) -> None:
        if self._terms_by_matching_str:
            raise RuntimeError("Matching cache is already initialized! Should not call init again!")

        self.pg_matching_mgr = self.service_provider.postgres_db.get_pg_matching_mgr()
        self.glossary_service = self.service_provider.glossary_service

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None and api_settings.SUBSCRIBE_CACHE_INVALIDATIONS:
            await self.service_provider.messaging_service.subscribe_cache_invalidation(
                "matching", self._on_cache_invalidation_msg
            )

        logger.info("start initializing matching cache...")  # we load all in memory
        start_time = time.time()

        # get matching items from other pods, or DB
        if api_settings.LOAD_CACHES_FROM_RUNNING_PODS:
            try:
                self._get_terms_by_matching_str_from_running_pods()
            except Exception as e:
                logger.warn("Failed to fetch matching items from other pods, fallback to DB", exception=str(e))
                await self._get_terms_by_matching_str_from_db()
        else:
            await self._get_terms_by_matching_str_from_db()

        logger.info(
            f"done initializing matching cache with {len(self._terms_by_matching_str)} items, "
            f"took {time.time() - start_time:.2f}s"
        )

    def _update_binary_cache(self) -> None:
        logger.info("Updating binary matching items cache...")

        with BytesIO() as fileobj:
            fileobj.mode = "wb"

            with gzip.GzipFile(mode="wb", compresslevel=9, fileobj=fileobj) as compressor:
                packer = msgpack.Packer(default=CachedMatchingItem.serializer)
                compressor.write(packer.pack(self._terms_by_matching_str))
                compressor.flush()

            self._terms_by_matching_str_binary = fileobj.getvalue()

        logger.info("Done updating binary matching items cache...")

    async def _get_terms_by_matching_str_from_db(self) -> None:
        all_items: list[MatchingItem] = await self.pg_matching_mgr.get_matching_items()
        for item in all_items:
            self._update_matching_item_in_cache(item)

        # reinitialize binary cache with DB items
        self._update_binary_cache()

    def _get_terms_by_matching_str_from_running_pods(self) -> None:
        logger.info("Fetch binary matching items cache from other pods...")
        response = httpx.get(
            url=f"{api_settings.api_url}/matching/cache",
            headers={"Authorization": f"Basic {api_settings.EATERNITY_AUTH_KEY}"},
            timeout=5,
        )

        if response.status_code != 200:
            raise Exception(f"Failed to fetch matching items cache from other pods: {response}")
        self._terms_by_matching_str_binary = response.content

        logger.info("Deserialize binary matching items cache...")
        with gzip.GzipFile(fileobj=BytesIO(self._terms_by_matching_str_binary), mode="rb") as decompressor:
            self._terms_by_matching_str = msgpack.unpack(decompressor, object_hook=CachedMatchingItem.deserializer)

        logger.info("Done getting matching items cache...")

    def clear_cache(self) -> None:
        self._terms_by_matching_str.clear()

    def _update_matching_item_in_cache(self, matching_item: MatchingItem) -> None:
        """If term_uids are missing in the matching_item, the cached term will be deleted."""

        def delete_preexisting_matching(match_item: MatchingItem, match_set: dict[str, CachedMatchingItem]) -> None:
            if match_item.uid in match_set:
                del match_set[match_item.uid]
                return

        matching_string: str = unicodedata.normalize(
            UNICODE_NORMALIZATION_FORM, matching_item.matching_string.casefold()
        )

        # If term_uids are present or matched_node_uid is present, we are adding the item into cache
        if matching_item.term_uids or matching_item.matched_node_uid:
            if matching_item.term_uids:
                assert isinstance(matching_item.term_uids, list)
                if len(matching_item.term_uids) > 0:
                    assert isinstance(matching_item.term_uids[0], str)
                else:
                    assert matching_item.matched_node_uid is not None

            if matching_string not in self._terms_by_matching_str:
                self._terms_by_matching_str[matching_string] = dict()
            else:
                # need to check for existence in set to prevent duplicate primary keys:
                matching_set = self._terms_by_matching_str[matching_string]
                delete_preexisting_matching(matching_item, matching_set)

            # Update the cache structure to include matched_node_uid
            self._terms_by_matching_str[matching_string][matching_item.uid] = CachedMatchingItem(
                tuple(matching_item.term_uids),
                matching_item.gap_filling_module,
                matching_item.lang,
                matching_item.matched_node_uid,
                matching_item.data,
            )
        else:
            if matching_string in self._terms_by_matching_str:
                matching_set = self._terms_by_matching_str[matching_string]
                delete_preexisting_matching(matching_item, matching_set)
                if len(matching_set) == 0:
                    del self._terms_by_matching_str[matching_string]

    async def _on_cache_invalidation_msg(self, msg_bytes: bytes) -> None:
        my_dict = json.loads(msg_bytes)

        matching_item = MatchingItem(**my_dict)

        # update cache:
        self._update_matching_item_in_cache(matching_item)
        logger.debug("matching_item was updated in cache", matching_string=matching_item.matching_string)

        self._terms_by_matching_str_binary = None

    def get_terms_by_matching_string(
        self, matching_string: str, gap_filling_module_name: str, filter_lang: Optional[str] = None
    ) -> list[list[Term]]:
        matching_items = self.get_matching_by_string(matching_string, gap_filling_module_name, filter_lang)
        return [
            [self.glossary_service.get_term_by_id(UUID(term_uid)) for term_uid in item.term_uids]
            for item in matching_items
        ]

    def get_matching_by_string(
        self,
        matching_string: str,
        gap_filling_module_name: str,
        filter_lang: Optional[str] = None,
        filter_context: Optional[dict] = None,
    ) -> list[MatchingItem]:
        """Returns a list of terms that match the given *lowercased* matching string.

        :param filter_lang: if set, then filter results to only return matching languages
        :param matching_string:
        :param gap_filling_module_name: filters the terms by the given gap filling module
        :return: a list of terms; empty list if no match
        """
        if not matching_string:
            return []

        matching_string_norm: str = unicodedata.normalize(UNICODE_NORMALIZATION_FORM, matching_string.casefold())

        if matching_string_norm not in self._terms_by_matching_str:
            return []
        else:
            matching_set = self._terms_by_matching_str.get(matching_string_norm)
            return [
                MatchingItem(
                    uid=uid,
                    term_uids=list(item.term_uids) if item.term_uids else [],
                    gap_filling_module=item.gap_filling_module,
                    lang=item.lang,
                    matched_node_uid=item.matched_node_uid,
                    data=item.data,
                )
                for uid, item in matching_set.items()
                if item.gap_filling_module == gap_filling_module_name
                and (filter_lang is None or item.lang == filter_lang)
                and (
                    filter_context is None
                    or item.data is None
                    or all(filter_context.get(k) == v for k, v in item.data.items())
                )
            ]

    async def put_matching_item(self, matching_item: MatchingItem) -> MatchingItem:
        # first normalize the string before inserting to database:
        matching_item.matching_string = unicodedata.normalize(
            UNICODE_NORMALIZATION_FORM, matching_item.matching_string.casefold()
        )

        matching_item = await self.pg_matching_mgr.upsert_matching(matching_item)

        # update cache
        self._update_matching_item_in_cache(matching_item)

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None:
            logger.debug(
                "emitting a cache invalidation for matching string", matching_string=matching_item.matching_string
            )
            data = matching_item.as_dict()
            data_bytes = json.dumps(data).encode("utf-8")
            await self.service_provider.messaging_service.emit_invalidate("matching", data_bytes)

        return matching_item

    async def update_cache_for_many_matching_items(self, matching_items: list[MatchingItem]) -> None:
        num_updates = len(matching_items)
        if (
            self.service_provider.messaging_service is not None
            and self.MAX_EXPECTED_INVALIDATED_MATCHING_ITEMS < num_updates
        ):
            logger.warning(
                f"updating unexpectedly many matching items ({num_updates}), binary cache will be recomputed each time"
            )

        for matching_item in matching_items:
            # update cache
            self._update_matching_item_in_cache(matching_item)

            # Optional messaging_service in case multiple worker caches need to be synchronized
            # (many unittests don't need it, so better to keep it optional)
            if self.service_provider.messaging_service is not None:
                logger.debug(
                    "emitting a cache invalidation for matching string", matching_string=matching_item.matching_string
                )
                data = matching_item.as_dict()
                data_bytes = json.dumps(data).encode("utf-8")
                await self.service_provider.messaging_service.emit_invalidate("matching", data_bytes)

    async def add_matching_required(self, node_uid: UUID, root_node_uid: UUID) -> None:
        await self.pg_matching_mgr.add_matching_required(node_uid, root_node_uid)

    async def delete_matching_item(self, matching_item_to_delete: MatchingItem) -> None:
        await self.pg_matching_mgr.delete_matching(matching_item_to_delete.uid)
        assert matching_item_to_delete.term_uids is None

        # update cache
        self._update_matching_item_in_cache(matching_item_to_delete)

        # Optional messaging_service in case multiple worker caches need to be synchronized
        # (many unittests don't need it, so better to keep it optional)
        if self.service_provider.messaging_service is not None:
            logger.debug(
                "emitting a cache invalidation for matching string",
                matching_string=matching_item_to_delete.matching_string,
            )
            data = matching_item_to_delete.as_dict()
            data_bytes = json.dumps(data).encode("utf-8")
            await self.service_provider.messaging_service.emit_invalidate("matching", data_bytes)

    def add_apply_matching_item(
        self, node_uid: str, root_node_uid: str, matching_uid: str, matching_context: Dict[str, str]
    ) -> None:
        """Add a matching item to be applied. Flushes to DB every 100 items as a background task."""
        self._applied_matching_items.append(
            AppliedMatchingItem(
                node_uid,
                root_node_uid,
                matching_uid,
                matching_context,
            )
        )

        # For the moment I decided to flush at the end of each calculation in calc-service to avoid
        # loosing any applied matchings by pod-shutdowns before the 100 applied-matchings are full.
        # Open for discussion on this.
        # if len(self._applied_matching_items) >= 100:
        #     self.flush_applied_matchings_to_db()

    async def flush_applied_matchings_to_db(self) -> None:
        """Flush any remaining pending applied matching items to the database."""
        if not self._applied_matching_items:
            return

        applied_matchings_tuples = [
            (item.node_uid, item.root_node_uid, item.matching_uid, item.matching_context)
            for item in self._applied_matching_items
        ]
        self._applied_matching_items.clear()
        await self.flush_applied_matchings_to_db_background_task(applied_matchings_tuples)

    async def flush_applied_matchings_to_db_background_task(
        self, applied_matchings_tuples: list[tuple[str, str, str, dict[str, str]]]
    ) -> None:
        """Flush any remaining pending applied matching items to the database as a background-task."""
        # Create background task to flush to DB
        task = asyncio.create_task(self.pg_matching_mgr.insert_applied_matchings(applied_matchings_tuples))

        # Need to keep task ref to prevent Heisenbug (https://news.ycombinator.com/item?id=34754276):
        self.background_tasks.add(task)

        # To prevent keeping references to finished tasks forever,
        # make each task remove its own reference from the set after completion:
        task.add_done_callback(self.background_tasks.discard)

    async def stop(self) -> None:
        if self.background_tasks:
            done, _ = await asyncio.wait(self.background_tasks, return_when=asyncio.ALL_COMPLETED)
