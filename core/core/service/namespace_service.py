from typing import TYPE_CHECKING, Any
from uuid import UUID

from structlog import get_logger

from api.dto.v2.customer_dto import CustomerDto
from core.domain.namespace import Namespace
from database.postgres.pg_namespace_mgr import PostgresNamespaceMgr

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider


logger = get_logger()


class NamespaceService:
    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.namespace_mgr: PostgresNamespaceMgr = None  # noqa
        self.access_group_mgr: PostgresAccessMgr = None  # noqa

    async def init(self) -> None:
        self.namespace_mgr = self.service_provider.postgres_db.get_namespace_mgr()
        self.access_group_mgr = self.service_provider.postgres_db.get_access_group_mgr()

    async def upsert_namespace(self, auth_token: str = None, **kwargs: dict[str, Any]) -> tuple[CustomerDto, str]:
        new_namespace = Namespace(**kwargs)

        namespace, token, default_access_group = await self.namespace_mgr.insert_or_update_namespace_wrapper(
            new_namespace,
            auth_token,
        )

        return CustomerDto(name=new_namespace.name, uid=namespace.uid, xid=namespace.xid), token

    # TODO: unused for now
    async def find_all_namespaces(self) -> list[Namespace]:
        return await self.namespace_mgr.find_all()

    # TODO: used only by glossary router unit tests for now
    async def find_namespace_by_uid(self, namespace_uid: str) -> Namespace:
        return await self.namespace_mgr.find_by_namespace_uid(namespace_uid)

    async def find_namespace_by_xid(self, namespace_xid: str) -> Namespace:
        return await self.namespace_mgr.find_by_namespace_xid(namespace_xid)

    async def find_default_access_group_by_ns_xid(self, ns_xid: str) -> UUID:
        access_group = await self.access_group_mgr.get_access_group_by_xid(namespace_xid=ns_xid, ag_xid="default")
        return UUID(access_group.uid)
