from collections import OrderedDict
from copy import deepcopy
from typing import TYPE_CHECKING, Optional

from structlog import get_logger

from core.domain.deep_mapping_view import DeepMappingView
from database.postgres.pg_gfm_cache_mgr import PostgresGfmCacheMgr

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

logger = get_logger()


class GfmCacheService:
    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self._gfm_cache_mgr: PostgresGfmCacheMgr = None  # noqa

        self._cache: dict[str, LRUCache] = {}

    async def init(self) -> None:
        self._gfm_cache_mgr = self.service_provider.postgres_db.get_gfm_cache_mgr()

    async def init_individual_gfm_cache(self, gfm_name: str, max_size: int = 0) -> None:
        self._cache[gfm_name] = LRUCache()

        if max_size:
            await self._cache[gfm_name].set_max_size(max_size)
        else:
            max_size = self._cache[gfm_name].get_max_size()

        # TODO: use limit here? since we may have more pre-cached entries than allowed by LRUCache limit..
        last_cached_entries = await self._gfm_cache_mgr.get_prefill_cache_entries(gfm_name, max_size)

        if last_cached_entries:
            await self._cache[gfm_name].prefill_cache(last_cached_entries)

    def clear_cache(self) -> None:
        for gfm_cache in self._cache.values():
            gfm_cache.clear_cache()

    async def get_cache_entry_by_gfm_name_and_key(
        self,
        gfm_name: str,
        cache_key: str,
        deep_copy: bool = True,
    ) -> Optional[dict | DeepMappingView]:
        local_cache_entry = await self._cache[gfm_name].get(cache_key)

        if not local_cache_entry:
            db_cache_entry = await self._gfm_cache_mgr.get_cache_entry_by_gfm_name_and_key(gfm_name, cache_key)

            if db_cache_entry:
                await self._cache[gfm_name].set(cache_key, db_cache_entry, deep_copy=deep_copy)

                return db_cache_entry
            else:
                return None
        else:
            return local_cache_entry

    async def set_cache_entry_by_gfm_name_and_key(
        self,
        gfm_name: str,
        cache_key: str,
        cache_data: dict,
        load_on_boot: bool = False,
        deep_copy: bool = True,
    ) -> bool:
        db_change_status = await self._gfm_cache_mgr.set_cache_entry_by_gfm_name_and_key(
            gfm_name,
            cache_key,
            cache_data,
            load_on_boot,
        )

        if db_change_status:
            await self._cache[gfm_name].delete(cache_key)
            await self._cache[gfm_name].set(cache_key, cache_data, deep_copy=deep_copy)

        return db_change_status


class LRUCache:
    _cache: OrderedDict
    _MAX_SIZE: int

    def __init__(self):
        self._cache = OrderedDict()
        self._MAX_SIZE = 5000

    async def get(self, key: str) -> Optional[DeepMappingView]:
        try:
            self._cache.move_to_end(key, last=True)

            return DeepMappingView(self._cache[key])
        except KeyError:
            return None

    async def set(self, key: str, value: object, deep_copy: bool = True) -> None:
        if deep_copy:
            self._cache[key] = deepcopy(value)
        else:
            self._cache[key] = value
        self._cache.move_to_end(key, last=True)

        if len(self._cache) > self._MAX_SIZE:
            self._cache.popitem(last=False)

    async def delete(self, key: str) -> None:
        try:
            del self._cache[key]
        except KeyError:
            pass

    async def prefill_cache(self, last_cached_entries: list) -> None:
        for entry in last_cached_entries:
            entry_key = entry.get("cache_key")

            self._cache[entry_key] = entry.get("cache_data")
            self._cache.move_to_end(entry_key, last=True)

    def clear_cache(self) -> None:
        self._cache.clear()

    async def set_max_size(self, max_size: int) -> None:
        self._MAX_SIZE = max_size

        while len(self._cache) > self._MAX_SIZE:
            self._cache.popitem(last=False)

    def get_max_size(self) -> int:
        return self._MAX_SIZE

    def __len__(self):
        return len(self._cache)

    def __repr__(self):
        return f"Currently LRU cache has {len(self)} entries."
