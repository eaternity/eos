import asyncio
import logging
import os
import traceback
import uuid
from enum import IntEnum
from typing import TYPE_CHECKING, Any, Callable, Coroutine, Dict
from uuid import UUID

import aio_pika
from aio_pika import ExchangeType, RobustQueue
from aio_pika.abc import AbstractIncomingMessage, ConsumerTag
from aio_pika.patterns import RPC, NackMessage, RejectMessage
from aio_pika.patterns.rpc import RPCMessageType
from pydantic_settings import SettingsConfigDict
from structlog import get_logger
from structlog.contextvars import get_contextvars

from api.middlewares.request_logging.controller import ContextVars
from core.domain.calculation import Calculation
from core.envprops import EnvProps
from core.service.calc_service import OrchestratorAtLimitException

if TYPE_CHECKING:
    from core.service.service_provider import ServiceProvider

logger = get_logger()


class Priority(IntEnum):
    LOW = 0
    HIGH = 1
    # Note: if adding a new priority level, all pods need to be stopped to initialize a new queue


async def custom_on_call_message(self, method_name: str, message: aio_pika.IncomingMessage) -> None:  # noqa: ANN001
    routing_key = self._format_routing_key(method_name)

    if routing_key not in self.routes:
        logger.warning("Method %r not registered in %r", method_name, self)
        return

    try:
        payload = await self.deserialize_message(message)
        func = self.routes[routing_key]
        result: Any = await self.execute(func, payload)
        message_type = RPCMessageType.RESULT
    except NackMessage as nack:
        if nack.requeue:
            await message.nack(requeue=True)
        else:
            await message.reject(requeue=False)
        return
    except RejectMessage as reject:
        await message.reject(requeue=reject.requeue)
        return
    except Exception as e:
        result = self.serialize_exception(e)
        message_type = RPCMessageType.ERROR

        if self.host_exceptions is True:
            logger.exception(e)

    if not message.reply_to:
        logger.info(
            'RPC message without "reply_to" header %r call result ' "will be lost",
            message,
        )
        await message.ack()
        return

    try:
        result_message = await self.serialize_message(
            payload=result,
            message_type=message_type,
            correlation_id=message.correlation_id,
            delivery_mode=message.delivery_mode,
        )
    except asyncio.CancelledError:
        raise
    except Exception as e:
        result_message = await self.serialize_message(
            payload=e,
            message_type=RPCMessageType.ERROR,
            correlation_id=message.correlation_id,
            delivery_mode=message.delivery_mode,
        )

    try:
        await self.channel.default_exchange.publish(
            result_message,
            message.reply_to,
            mandatory=False,
        )
    except Exception:
        logger.exception("Failed to send reply %r", result_message)
        await message.reject(requeue=False)
        return

    if message_type == RPCMessageType.ERROR.value:
        await message.ack()
        return

    await message.ack()


# Monkey patch the original on_call_message with the custom one
RPC.on_call_message = custom_on_call_message


class MessagingSettings(EnvProps):
    RABBITMQ_HOSTNAME: str = "localhost"
    RABBITMQ_PORT: int = 5672
    RABBITMQ_WORKER_PREFETCH_COUNT: int = 2
    RABBITMQ_WORKER_MAX_RETRIES: int = 3
    RABBITMQ_CACHE_PREFETCH_COUNT: int = 3
    RABBITMQ_RETURN_CALCULATION: bool = False
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


class MessagingService:
    def __init__(self, service_provider: "ServiceProvider"):
        self.service_provider = service_provider
        self.connection = None
        self.cache_channel = None
        self.worker_channel = None
        self.cache_inv_exchange = None
        self.orchestration_worker = None
        self.queue_by_obj_types = dict()
        self.background_tasks = set()
        self.settings = MessagingSettings()

    async def start(self) -> None:
        from core.service.service_provider import ServiceLocator

        service_locator = ServiceLocator()
        self.service_provider = service_locator.service_provider

        logging.info("starting connection to message broker...")
        self.connection = await aio_pika.connect_robust(
            f"amqp://{self.settings.RABBITMQ_DEFAULT_USER}:{self.settings.RABBITMQ_DEFAULT_PASS}@{self.settings.RABBITMQ_HOSTNAME}:{self.settings.RABBITMQ_PORT}/",
        )

        self.cache_channel = await self.connection.channel()
        await self.cache_channel.set_qos(prefetch_count=self.settings.RABBITMQ_CACHE_PREFETCH_COUNT)

        self.cache_inv_exchange = await self.cache_channel.declare_exchange(
            "cache_invalidation_topic",
            ExchangeType.TOPIC,
        )

    async def start_worker_channel(self) -> None:
        self.worker_channel = await self.connection.channel()
        await self.worker_channel.set_qos(prefetch_count=self.settings.RABBITMQ_WORKER_PREFETCH_COUNT)

        self.orchestration_worker = await RPC.create(self.worker_channel)
        service_provider = self.service_provider

        async def calculation_worker(
            *, calculation_uid_int: int, request_id_int: int, result_via_queue: bool
        ) -> bool | bytes:
            calculation_uid = UUID(int=calculation_uid_int)
            request_id = UUID(int=request_id_int)
            tracing_params = {"request_id": str(request_id), "calculation_uid": str(calculation_uid)}
            with ContextVars(**tracing_params):
                logger.info("Start loading calculation in worker")
                try:
                    calculation = await service_provider.postgres_db.pg_calc_mgr.find_by_uid(
                        calculation_uid, increment_retry_count=True
                    )
                except Exception as e:
                    logger.error(
                        "calculation_worker failed loading calculation",
                        exception=e,
                        traceback="".join(traceback.format_exception(e)),
                    )
                    raise e

                if not calculation:
                    logger.error("calculation_worker cannot find calculation uid")
                    raise RejectMessage(requeue=False)

                if calculation.retry_count > 1:
                    logger.info("Start processing calculation in worker (retry)", retry_count=calculation.retry_count)

                if calculation.retry_count > self.settings.RABBITMQ_WORKER_MAX_RETRIES:
                    logger.error("calculation_worker failed too many times", retry_count=calculation.retry_count)
                    raise NackMessage(requeue=False) from None

                try:
                    calculation, node = await service_provider.calc_service.calculate(calculation, raise_if_full=False)
                    logger.info("calculation_worker finished")
                except OrchestratorAtLimitException:
                    logger.error("calculation_worker cannot start calculation")
                    # in this case we decrement again the retry count because it was not really tried this time:
                    await service_provider.postgres_db.pg_calc_mgr.decrement_retry_count(calculation.uid)
                    raise NackMessage(requeue=True) from None
                except Exception as e:
                    logger.error(
                        "calculation_worker failed processing!",
                        exception=e,
                        traceback="".join(traceback.format_exception(e)),
                    )
                    calculation.success = False

                try:
                    if calculation.save_result or not result_via_queue:
                        await service_provider.postgres_db.pg_calc_mgr.upsert_by_uid(calculation)
                        logger.info("calculation_worker finished saving result")

                    if result_via_queue:
                        compressed_data = calculation.get_compressed_result()
                        logger.info("calculation_worker finished. Returning compressed_data")
                        return compressed_data
                    else:
                        logger.info("calculation_worker finished returning result", success=calculation.success)
                        return calculation.success
                except Exception as e:
                    logger.error("calculation_worker failed saving result!")
                    logger.error(
                        "calculation_worker failed saving result!",
                        exception=e,
                        traceback="".join(traceback.format_exception(e)),
                    )
                    raise e

        if self.settings.RABBITMQ_WORKER_PREFETCH_COUNT == 0:
            logger.warning("RabbitMQ prefetch count is set 0 --> Not registering orchestration_worker in this process.")
            return

        await self.orchestration_worker.register(
            "orchestration_worker",
            calculation_worker,
            auto_delete=True,
            arguments={"x-max-priority": max(map(lambda p: p.value, Priority))},
        )

    async def enqueue_calculation(self, calculation: Calculation, priority: Priority = Priority.LOW) -> Calculation:
        calculation_uid: UUID = calculation.uid
        logger.info("enqueuing calculation", calculation_uid=calculation_uid)

        # Retrieve context variables for log tracing across services:
        context = get_contextvars()
        request_id = UUID(context.get("request_id"))
        result_via_queue = (
            self.settings.RABBITMQ_RETURN_CALCULATION and not calculation.return_log and not calculation.save_result
        )

        try:
            task_result = await self.orchestration_worker.call(
                "orchestration_worker",
                priority=priority.value,
                kwargs=dict(
                    calculation_uid_int=calculation_uid.int,
                    request_id_int=request_id.int,
                    result_via_queue=result_via_queue,
                ),
            )
            logger.info("calculation worker call finished", calculation_uid=calculation_uid, task_result=task_result)
        except Exception as e:
            logger.error(
                "calculation worker call failed",
                calculation_uid=calculation_uid,
                traceback_fmt="".join(traceback.format_exception(e)),
                worker_channel_is_closed=self.worker_channel.is_closed,
                event_loop_is_closed=asyncio.get_event_loop().is_closed(),
                orchestration_worker_loop_is_closed=self.orchestration_worker.loop.is_closed(),
            )
            calculation = await self.service_provider.postgres_db.pg_calc_mgr.find_by_uid(calculation.uid)
            calculation.success = False
            return calculation

        if result_via_queue:
            # serialize back into calculation object:
            calculation.set_fields_from_compressed_data(task_result)
        else:
            calculation.success = task_result
            calculation = await self.service_provider.postgres_db.pg_calc_mgr.find_by_uid(calculation.uid)

        logger.info("calculation done", calculation_uid=calculation_uid, success=calculation.success)

        return calculation

    async def subscribe_cache_invalidation(self, obj_type: str, on_msg: Callable[[bytes], Coroutine]) -> None:
        logger.debug("subscribing cache invalidation messages", obj_type=obj_type)

        # generate a unique queue name to make sure that every subscriber is in its own queue (for broadcasting):
        queue_name = str(uuid.uuid4())
        queue = await self.cache_channel.declare_queue(name=queue_name, exclusive=True)

        await queue.bind(self.cache_inv_exchange, routing_key=obj_type)

        async def on_message(message: AbstractIncomingMessage) -> None:
            async with message.process():
                pid = str(os.getpid())
                routing_key = message.routing_key
                logger.debug("received cache invalidation", routing_key=routing_key, pid=pid)

                task = asyncio.create_task(on_msg(message.body))

                # Need to keep task ref to prevent Heisenbug (https://news.ycombinator.com/item?id=34754276):
                self.background_tasks.add(task)

                # To prevent keeping references to finished tasks forever,
                # make each task remove its own reference from the set after completion:
                task.add_done_callback(self.background_tasks.discard)

        consumer_tag = await queue.consume(on_message)
        self.queue_by_obj_types[obj_type]: Dict[str, (RobustQueue, ConsumerTag)] = (queue, consumer_tag)

        logger.debug("subscribed cache invalidation messages", obj_type=obj_type)

    async def stop(self) -> None:
        for obj_type, (queue, consumer_tag) in self.queue_by_obj_types.items():
            logger.debug("destroy queue", obj_type=obj_type)
            await queue.unbind(self.cache_inv_exchange, routing_key=obj_type)
            await queue.cancel(consumer_tag)

        if self.orchestration_worker:
            await self.orchestration_worker.close()

        if self.background_tasks:
            done, _ = await asyncio.wait(self.background_tasks, return_when=asyncio.ALL_COMPLETED)

        logger.info("closing rabbitmq connection")
        await self.connection.close()

    async def emit_invalidate(self, obj_type: str, obj_key: bytes) -> None:
        pid = str(os.getpid())
        logger.debug("emitting a cache invalidation", pid=pid)

        await self.cache_inv_exchange.publish(
            aio_pika.Message(body=obj_key),
            routing_key=obj_type,
        )
