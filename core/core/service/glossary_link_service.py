import uuid
from typing import TYPE_CHECKING, List, Optional

from structlog import get_logger

from core.domain.glossary_link import GlossaryLink
from core.domain.nodes.node import Node
from core.domain.term import Term
from database.postgres.pg_glossary_link_mgr import PgGlossaryLinkMgr

if TYPE_CHECKING:
    from core.service.glossary_service import GlossaryService
    from core.service.service_provider import ServiceProvider

logger = get_logger()


class GlossaryLinkService:
    def __init__(self, service_provider: "ServiceProvider"):
        """Service for glossary links; no caching for now."""
        self.service_provider = service_provider
        self._glossary_link_mgr: PgGlossaryLinkMgr = None  # noqa
        self._glossary_service: GlossaryService = None  # noqa

    async def init(self) -> None:
        self._glossary_link_mgr = self.service_provider.postgres_db.get_pg_glossary_link_mgr()
        self._glossary_service = self.service_provider.glossary_service

    def clear_cache(self) -> None:
        pass  # no caching for now

    async def valid_glossary_links_of_gfm_for_terms(self, terms: list[Term], gfm: str) -> Optional[List[GlossaryLink]]:
        """Load valid glossary links associated with a set of terms of a gap filling module (gfm).

        :param terms: set of terms to which the valid glossary links exist.
        :param gfm: gap filling module that the glossary links are associated with.
        :return: valid glossary links to gfm terms attached to the input terms.
        """
        term_uids = [t.uid for t in terms]

        glossary_links: list[GlossaryLink] = await self._glossary_link_mgr.get_glossary_links(term_uids, gfm)

        valid_glossary_links = [
            glossary_link for glossary_link in glossary_links if set(glossary_link.term_uids) == set(term_uids)
        ]
        return valid_glossary_links

    async def load_linked_term_from_gfm(self, terms: list[Term], gfm: str, error_msg: str) -> Optional[Term]:
        """Load data from a GFM for a list of terms."""
        valid_glossary_links = await self.valid_glossary_links_of_gfm_for_terms(terms, gfm)
        if not valid_glossary_links:
            logger.debug(error_msg)
            return None
        else:
            linked_term_uid = valid_glossary_links[0].linked_term_uid  # just take the first one, for now
            return self._glossary_service.get_term_by_id(linked_term_uid)

    async def load_fao_code_term(self, terms: list[Term]) -> Optional[Term]:
        """Load FAO data for a list of terms."""
        error_msg = f"Terms {terms} don't have a linked / corresponding FAO code term."
        fao_term = await self.load_linked_term_from_gfm(terms, "FAO", error_msg)
        return fao_term

    async def load_term_nutrients(self, terms: list[Term]) -> Optional[Term]:
        """Load nutrients data for a list of terms."""
        error_msg = f"Terms {terms} don't have a linked nutrients file."
        nutrients_term = await self.load_linked_term_from_gfm(terms, "Nutrients", error_msg)
        return nutrients_term

    # TODO: currently unused
    async def load_process_node(self, terms: list[Term]) -> Node:
        """Load process node data for a list of terms."""
        glossary_links: list[GlossaryLink] = await self._glossary_link_mgr.get_glossary_links(
            [t.uid for t in terms], "LinkTermToActivityNode"
        )
        if not glossary_links:
            raise ValueError(f"terms {terms} don't have a linked / corresponding process node")
        else:
            process_node_uid = glossary_links[0].linked_node_uid  # just take the first one, for now
            return self.service_provider.postgres_db.get_graph_mgr().find_by_uid(process_node_uid)

    async def get_glossary_links_by_gfm(self, gap_filling_module: str) -> list[GlossaryLink]:
        return await self._glossary_link_mgr.get_data_of_gfm(gap_filling_module)

    async def insert_glossary_link(self, glossary_link: GlossaryLink) -> GlossaryLink:
        return await self._glossary_link_mgr.insert_glossary_link(glossary_link)

    async def delete_glossary_links_by_uids(self, uids: list[uuid.UUID] | list[str]) -> None:
        await self._glossary_link_mgr.delete_glossary_links_by_uids(uids)
