from uuid import UUID

from structlog import get_logger

from core.domain.nodes.node import Node

logger = get_logger()


class ModeledActivityNodeService:
    """A class designed to manage the interaction of Gap Filling Modules (GFMs) with Brightway nodes.

    Overview:
    ---------
    This class encapsulates methods and logic for working with Brightway nodes in the context of GFMs.
    It is specifically tailored for the experimental feature of running GFMs on Brightway nodes.

    Purpose:
    -------
    The primary intention of this class is to provide a centralized location for handling operations related to
    Brightway nodes. By consolidating this functionality, it becomes easier to extend and modify the behavior of
    GFMs interacting with Brightway nodes in the future.

    Note:
    ----
    The feature of running GFMs on Brightway nodes is currently experimental and should be used exclusively for testing.

    Args:
    ----
        service_provider (ServiceProvider): The service provider associated with this service.

    Attributes:
    ----------
        activity_uids_below_bw_nodes_to_compute (list[UUID]): A list of activity UUIDs of Brightway nodes
        for which we want to run GFMs.
        flow_uids_below_bw_nodes_to_compute (list[UUID]): A list of flow UUIDs of intermediary flows of Brightway nodes
        for which we want to run GFMs.
        initialized (bool): Indicates whether the service has been initialized.

    Methods:
    -------
        - init(activity_flow_uid_tuples_below_bw_nodes_to_compute: list[tuple[UUID, UUID]]):
          Initialize the service with activity and flow UUIDs.

        - should_bw_node_be_computed(node: Node, gfm_name: str) -> bool:
          Determine if a Brightway node should be computed based on its node type and GFM name.

        - get_location_of_brightway_node(node: Node, raw_location: str) -> str:
          Retrieve the location of a Brightway node, with a special case for mapping 'rer' to (non-meaningful)
          locations 'USA' and 'CHE'.

        - get_closest_product_node(node: Node) -> Node:
          Get the closest product node for an intermediary flow node.

    """

    def __init__(self) -> None:
        self.activity_uids_below_bw_nodes_to_compute: list[UUID] = []
        self.flow_uids_below_bw_nodes_to_compute: list[UUID] = []
        self.initialized: bool = False

    async def init(self, activity_flow_uid_tuples_below_bw_nodes_to_compute: list[tuple[UUID, UUID]]) -> None:
        """Initialize the service with activity and flow UUIDs for which we want to run GFMs.

        :param: activity_flow_uid_tuples_below_bw_nodes_to_compute: A list of tuples of UUIDs of activity and the
        UUIDs of their child-flows for which we want to run GFMs.
        """
        for activity_uid, flow_uid in activity_flow_uid_tuples_below_bw_nodes_to_compute:
            self.activity_uids_below_bw_nodes_to_compute.append(activity_uid)
            logger.warning(f"activity_uid = {activity_uid}")
            self.flow_uids_below_bw_nodes_to_compute.append(flow_uid)
            logger.warning(f"flow_uid = {flow_uid}")
        self.initialized = True
        logger.warning(
            f"{self.__class__.__name__} initialized. Currently, this feature is experimental"
            " and should only be used for testing."
        )

    def should_bw_node_be_computed(self, node: Node, gfm_name: str) -> bool:
        """Interface for GFMs to answer the question whether the GFM should be scheduled on the given node.

        :param node: The node to check.
        :param gfm_name: The name of the GFM.
        :return: True if the GFM should be scheduled on the node, False otherwise.
        """
        if not self.initialized:
            # by default we don't schedule GFMs on brightway nodes
            return False

        if gfm_name in ["LocationGapFillingWorker"]:
            # Since flow nodes in brightway have no location fields, we need to run the location GFM on the parent- and
            # sub-node of the flow node.
            # When we run on the sub-node, we assign the location of the sub-node also to the flow node.
            # The location in the parent-node is picked up in the TransportModeDistanceGapFillingWorker.
            if node.uid in self.activity_uids_below_bw_nodes_to_compute:
                return True
            parent_nodes = node.get_parent_nodes()
            if parent_nodes and (parent_nodes[0].uid in self.flow_uids_below_bw_nodes_to_compute):
                return True
        elif gfm_name in ["TransportDecisionGapFillingWorker", "TransportModeDistanceGapFillingWorker"]:
            if node.uid in self.flow_uids_below_bw_nodes_to_compute:
                return True
        else:
            return False

    def get_location_of_brightway_node(self, node: Node, raw_location: str) -> str:
        """Retrieve the location of a Brightway node,.

        'RER' gets mapped to (non-meaningful) locations 'USA' and 'CHE' for testing purposes.

        :param node: The node for which we want to retrieve the location.
        :param raw_location: The raw location of the node.
        :return: The new raw location of the node.
        """
        if raw_location.lower() == "rer":
            # this is a hack. The location 'rer' is currently not implemented. To still be able to use
            # cached ecotransit results, we map 'rer' for the child nodes of a flow_to_compute to 'USA'
            # and for the parent node of a flow_to_compute to 'CHE'
            parent_nodes = node.get_parent_nodes()
            if parent_nodes and (parent_nodes[0].uid in self.flow_uids_below_bw_nodes_to_compute):
                raw_location = "USA"
            else:
                raw_location = "CHE"
            logger.warning(f"In Node {node.uid} 'RER' got mapped to: '{raw_location}'")
        return raw_location

    def get_closest_product_node(self, node: Node) -> Node:
        """Get the closest product node for an intermediary flow node.

        Currently, this is just the parent node of the intermediary flow node.
        """
        parent_node = node.get_parent_nodes()[0]
        assert parent_node in self.activity_uids_below_bw_nodes_to_compute
        return parent_node
