use std::collections::{HashMap, HashSet};
use std::io::prelude::*;

use anyhow::Context;
use google_drive3::api::FileList;
use google_drive3::{hyper_rustls, hyper_util, yup_oauth2, DriveHub};
use http_body_util::BodyExt;
use scraper::{Html, Selector};
use tracing::{error, info, warn};

pub async fn download_gadm_files(options: &crate::Options) -> anyhow::Result<()> {
    if let Some(ref secret_path) = options.gdrive_credentials {
        info!("trying to download missing GADM files from Google Drive");
        // offer fallback to service-account if client-secret cannot be read
        let auth = if let Ok(secret) = yup_oauth2::read_application_secret(secret_path).await {
            yup_oauth2::InstalledFlowAuthenticator::builder(
                secret,
                yup_oauth2::InstalledFlowReturnMethod::HTTPRedirect,
            )
            .build()
            .await?
        } else {
            let secret = yup_oauth2::read_service_account_key(secret_path)
                .await
                .context(format!("{:?}", secret_path))?;
            yup_oauth2::ServiceAccountAuthenticator::builder(secret)
                .build()
                .await?
        };
        let scopes = [
            "https://www.googleapis.com/auth/devstorage.read_write",
            "https://www.googleapis.com/auth/spreadsheets",
            "https://www.googleapis.com/auth/drive",
        ];

        let hub = DriveHub::new(
            hyper_util::client::legacy::Client::builder(hyper_util::rt::TokioExecutor::new())
                .build(
                    hyper_rustls::HttpsConnectorBuilder::new()
                        .with_native_roots()?
                        .https_or_http()
                        .enable_http1()
                        .build(),
                ),
            auth,
        );
        let gdrive_folder_id = options
            .gdrive_folder_id
            .clone()
            .ok_or(anyhow::anyhow!("no folder id given"))?;
        let query = format!("'{gdrive_folder_id}' in parents and name contains '.gpkg'");

        let mut file_list: Option<FileList> = None;
        for i in 0..10 {
            // linear back-off
            tokio::time::sleep(tokio::time::Duration::from_millis(1000 * i)).await;
            let timeout = tokio::time::timeout(
                tokio::time::Duration::from_secs(60),
                hub.files()
                    .list()
                    .add_scopes(scopes)
                    .q(&query)
                    .page_size(1000)
                    .doit(),
            );
            match timeout.await {
                Ok(result) => match result {
                    Ok((response, filelist)) => {
                        if response.status() == 200 {
                            file_list = Some(filelist);
                            break;
                        } else {
                            error!("file list failed {:?} retrying", response);
                            // return Err(anyhow::anyhow!(format!("file list failed {:?}", response)));
                        }
                    }
                    Err(e) => {
                        error!("file list error {:?} retrying", e);
                    }
                },
                Err(_) => error!("timeout"),
            }
        }
        if file_list.is_none() {
            return Err(anyhow::anyhow!(format!("remote file list is None")));
        }
        // safety: is none case is handled above
        #[allow(clippy::unwrap_used)]
        if let Some(files) = file_list.unwrap().files {
            if files.is_empty() {
                return Err(anyhow::anyhow!("file list empty from gdrive"));
            }
            let mut remote_files = HashMap::new();
            for file in files {
                if let (Some(file_id), Some(file_name)) = (file.name, file.id) {
                    remote_files.insert(file_id, file_name);
                }
            }
            let missing = files_missing(&options.gadm_dir, remote_files.keys())?;
            // TODO verify that requested countries are available remote

            // TODO if no local files, and no google drive files error out with helpful message to use download tool
            for file_name in missing {
                if !options.countries.is_empty()
                    && !options
                        .countries
                        .iter()
                        .any(|country| file_name.contains(country))
                {
                    // skip the countries not requested
                    continue;
                }
                if let Some(file_id) = remote_files.get(&file_name) {
                    info!(filename = file_name, "downloading");
                    // **IMPORTANT**: don't stream chunks to file, dump from RAM is needed!
                    // **REASONS**: 1. k8s uses EBD shared mount to multiple services, consistent
                    //                 file content must be guaranteed!
                    //              2. incomplete downloads using streamed chunks may leave an
                    //                 inconsistent file on disk!
                    let mut success = false;
                    for i in 0..10 {
                        // linear back-off
                        tokio::time::sleep(tokio::time::Duration::from_millis(1000 * i)).await;

                        let timeout = tokio::time::timeout(
                            tokio::time::Duration::from_secs(300),
                            hub.files()
                                .get(file_id)
                                .add_scopes(scopes)
                                .param("alt", "media")
                                .doit(),
                        );
                        match timeout.await {
                            Ok(result) => match result {
                                Ok((response, _)) => {
                                    let body = response.into_body();
                                    let full_file_name = options.gadm_dir.join(&file_name);
                                    let data = body.collect().await?.to_bytes();
                                    let mut file = std::fs::File::create(&full_file_name)?;
                                    file.write_all(&data)?;
                                    success = true;
                                    break;
                                }
                                Err(e) => {
                                    error!("file download error {:?} retrying", e);
                                }
                            },
                            Err(_) => error!("timeout"),
                        }
                    }
                    if !success {
                        return Err(anyhow::anyhow!("failed to download"));
                    }
                }
            }
            info!("all files already locally");
        } else {
            return Err(anyhow::anyhow!("no files on gdrive"));
        }
        Ok(())
    } else {
        warn!("using fallback GADM source to download");
        // fallback, download from official GADM sources
        let resp = reqwest::get(&options.gadm_fallback_url).await?;
        let text = resp.text().await?;

        let document = Html::parse_document(&text);
        let selector = Selector::parse(r#"table > tbody > tr > td > a"#)
            .map_err(|_| anyhow::anyhow!("parsing error"))?;
        // first bild remote files list
        let mut remote_files = vec![];
        for title in document.select(&selector) {
            if let Some(href) = title.value().attr("href") {
                if href.ends_with(".gpkg") {
                    remote_files.push(href.to_string());
                }
            }
        }
        let missing = files_missing(&options.gadm_dir, remote_files.iter())?;

        for file_name in missing {
            if !options.countries.is_empty()
                && !options
                    .countries
                    .iter()
                    .any(|country| file_name.contains(country))
            {
                // skip the countries not requested
                continue;
            }

            info!(filename = file_name, "downloading");
            let full_url = format!("{}{}", options.gadm_fallback_url, file_name);
            let resp = reqwest::get(full_url).await?;
            let data = resp.bytes().await?;
            // **IMPORTANT**: do not download chunks directly into file, dump from RAM is needed!
            // **REASONS**: 1. k8s uses EBD shared mount to multiple services, consistent file content must be guaranteed!
            //              2. incomplete downloads using streamed chunks may leave an incomplete file on disk!
            let full_file_name = options.gadm_dir.join(&file_name);
            let mut file = std::fs::File::create(&full_file_name)?;
            file.write_all(&data)?;
        }
        Ok(())
    }
}

fn files_missing<'a, P, I>(gadm_dir: P, files_available: I) -> anyhow::Result<Vec<String>>
where
    P: AsRef<std::path::Path> + core::fmt::Debug,
    I: IntoIterator<Item = &'a String>,
{
    let remote_files = HashSet::from_iter(files_available.into_iter().map(|s| s.to_string()));
    let mut local_files = HashSet::new();
    for entry in
        std::fs::read_dir(&gadm_dir).map_err(|e| anyhow::anyhow!("err: {e:?} for {gadm_dir:?}"))?
    {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            path.file_name()
                .map(|fname| fname.to_str().map(|s| local_files.insert(s.to_string())));
        }
    }
    let diff = remote_files.difference(&local_files);
    let retval: Vec<String> = diff.map(|f| f.to_string()).collect();
    Ok(retval)
}
