//!Geopackage reader optimised for our purpose
//!- find layers: `select table_name from gpkg_contents order by table_name;`
//!- take last layer, except for china, take layer 3 (index 2)? result: `ADM_ADM_4`
//!- then load only from table_name `select geom, GID_0, GID_1, ... from ADM_ADM_4`
use std::collections::{HashMap, HashSet};
use std::time::Instant;

use geo::{BoundingRect, Contains, MultiPolygon, Point, Rect};
use geozero::wkb;
use sqlx::sqlite::SqlitePool;
use tracing::info;

pub type GidRows = Vec<(wkb::Decode<geo::Geometry>, String, String, String)>;
pub type GadmRows = Vec<(MultiPolygon<f64>, Rect<f64>, String, String, String)>;
pub type GadmCache = HashMap<String, GadmRows>;

pub async fn load_gadm<P: AsRef<std::path::Path>>(
    filename: P,
) -> anyhow::Result<(String, GadmRows)> {
    let filename = filename.as_ref();

    let filename_str = filename
        .file_name()
        .ok_or(anyhow::anyhow!("no filename"))?
        .to_str()
        .ok_or(anyhow::anyhow!("cannot convert filename to string"))?;
    let path_str = filename
        .to_str()
        .ok_or(anyhow::anyhow!("cannot convert filename to string"))?;
    let now = Instant::now();

    let pool = SqlitePool::connect(&format!("sqlite://{path_str}")).await?;

    let table_names: Vec<(String,)> =
        sqlx::query_as("select table_name from gpkg_contents order by table_name")
            .fetch_all(&pool)
            .await?;
    let table_name: String = if filename_str.contains("CHN") {
        table_names
            .into_iter()
            .nth_back(1)
            .map(|row| row.0)
            .ok_or(anyhow::anyhow!("no table_name found"))?
    } else {
        table_names
            .into_iter()
            .last()
            .map(|row| row.0)
            .ok_or(anyhow::anyhow!("no table_name found"))?
    };
    // safety: the databases hold no other values, panic never triggered
    //         if the DBs hold that value, panic would happen while startup of the service -> immediately noticed -> no problem
    #[allow(clippy::panic)]
    let gid_columns = match table_name.as_str() {
        "ADM_ADM_0" => "GID_0, '' as GID_1, GID_0 as GID_HIGHEST",
        "ADM_ADM_1" => "GID_0, GID_1, GID_1 as GID_HIGHEST",
        "ADM_ADM_2" => "GID_0, GID_1, GID_2 as GID_HIGHEST",
        "ADM_ADM_3" => "GID_0, GID_1, GID_3 as GID_HIGHEST",
        "ADM_ADM_4" => "GID_0, GID_1, GID_4 as GID_HIGHEST",
        "ADM_ADM_5" => "GID_0, GID_1, GID_5 as GID_HIGHEST",
        _ => panic!("unpossible ;)"),
    };
    let rows: GidRows = sqlx::query_as(&format!("select geom, {gid_columns} from {table_name};"))
        .fetch_all(&pool)
        .await?;
    // safety: geometries in GADM are sane, unlikely to not find a bounding rectangle
    //         panic would happen at startup and noticed right away.
    #[allow(clippy::unwrap_used)]
    let rows: GadmRows = rows
        .into_iter()
        .map(|row| {
            if let geo::Geometry::MultiPolygon(mp) = row.0.geometry.unwrap() {
                let rect = mp.bounding_rect().unwrap();
                (mp, rect, row.1, row.2, row.3)
            } else {
                let mp = MultiPolygon::<f64>::new(vec![]);
                let rect = mp.bounding_rect().unwrap();
                (mp, rect, row.1, row.2, row.3)
            }
        })
        .collect();
    let loaded = now.elapsed().as_secs_f64();
    info!("{filename_str}: {table_name}. load time {loaded}");
    Ok((filename_str.to_string(), rows))
}

/// based on python `_get_gadm_id(country_code, google_place_types, latitude, longitude`
pub async fn gadm_id(
    gadm_cache: &GadmCache,
    country_code: &str,
    google_place_types: HashSet<String>,
    latitude: f64,
    longitude: f64,
) -> anyhow::Result<String> {
    let b = HashSet::from([
        "colloquial_area".to_string(),
        "establishment".to_string(),
        "natural_feature".to_string(),
    ]);

    let gps_point = Point::new(longitude, latitude);
    let filename = format!("gadm41_{country_code}.gpkg");
    let rows = gadm_cache
        .get(&filename)
        .ok_or(anyhow::anyhow!("no cache for this GADM file"))?;
    let region = rows
        .iter()
        .find(|row| row.1.contains(&gps_point) && row.0.contains(&gps_point));
    if let Some(region) = region {
        if google_place_types.contains("administrative_area_level_1") {
            // GID_1
            Ok(region.3.to_string())
        } else if google_place_types.intersection(&b).count() > 0 {
            // # we can check the size of the returned google result to determine if it is a city or a village:
            // GID_0
            Ok(region.2.to_string())
        } else {
            // GID_HIGHEST
            Ok(region.4.to_string())
        }
    } else {
        Err(anyhow::anyhow!("not found"))
    }
}

/// loads all GADM files into RAM
/// supposed to be called before starting the webserver
pub async fn load_gadm_cache(options: &crate::Options) -> anyhow::Result<GadmCache> {
    let mut all_gadm: GadmCache = HashMap::new();
    for entry in std::fs::read_dir(&options.gadm_dir)
        .map_err(|e| anyhow::anyhow!("err: {e:?} for {:?}", options.gadm_dir))?
    {
        let entry = entry?;
        let path = entry.path();
        #[allow(clippy::unwrap_used)]
        if path.is_file()
            && (options.countries.is_empty()
                || options.countries.iter().any(|country| {
                    path.file_name()
                        .unwrap()
                        .to_str()
                        .unwrap()
                        .contains(country)
                }))
        {
            let (dbfn, rows) = load_gadm(path).await?;
            all_gadm.insert(dbfn, rows);
        }
    }
    info!("loading done.");
    Ok(all_gadm)
}

pub fn verify_gadm_cache(gadm_cache: &GadmCache, options: &crate::Options) -> anyhow::Result<()> {
    if gadm_cache.is_empty() {
        return Err(anyhow::anyhow!("in Memory DB is empty"));
    }
    if !options.countries.is_empty()
        && !options.countries.iter().all(|country| {
            let country_filename = format!("gadm41_{country}.gpkg");
            gadm_cache.get(&country_filename).is_some()
        })
    {
        return Err(anyhow::anyhow!("not all requested countries loaded"));
    }
    if options.countries.is_empty() && gadm_cache.len() < 25 {
        return Err(anyhow::anyhow!("not enough country files loaded"));
    }
    Ok(())
}
