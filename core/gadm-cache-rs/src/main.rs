use std::{collections::HashSet, sync::Arc};

use axum::{
    extract::{Query, State},
    routing::get,
    Router,
};
use clap::Parser;
use reqwest::StatusCode;
use serde::Deserialize;
use tracing::{error, info};
use valuable::Valuable;

mod gadm;
mod gdrive;

#[derive(Deserialize, Debug, Valuable, Clone)]
struct GadmQuery {
    country_code: String,
    google_place_types: String,
    latitude: f64,
    longitude: f64,
}

async fn gadm_id(args: Query<GadmQuery>, State(cache): State<Arc<gadm::GadmCache>>) -> String {
    let gadm_query: GadmQuery = args.0;
    let google_place_types: HashSet<String> = gadm_query
        .google_place_types
        .split(',')
        .map(|s| s.to_owned())
        .collect();
    match gadm::gadm_id(
        &cache,
        &gadm_query.country_code,
        google_place_types,
        gadm_query.latitude,
        gadm_query.longitude,
    )
    .await
    {
        Ok(gadm_id) => {
            info!(request = gadm_query.as_value(), response = gadm_id);

            gadm_id
        }
        Err(_) => {
            error!("Nothing found for {gadm_query:?}");
            "ERROR".to_owned()
        }
    }
}

/// minimal sanity check of the cache, needed for k8s readinessProbe
async fn gadm_status(State(cache): State<Arc<gadm::GadmCache>>) -> (StatusCode, String) {
    if cache.len() > 200 {
        (StatusCode::OK, format!("GADM files cached {}", cache.len()))
    } else {
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("not enough GADM files cached {}", cache.len()),
        )
    }
}

#[derive(clap::Parser, Debug)]
#[command(name = "log-file mirror", about = "listens on json based nginx logfile and replays the requests",
          author, version, about, long_about = None)]
pub struct Options {
    /// path to where the GADM files are / should be downloaded to
    #[arg(short, long, env = "GADM_CACHE_DIR")]
    gadm_dir: std::path::PathBuf,
    /// Bind IP address and port for the service
    #[arg(short, long, default_value = "127.0.0.1:9753", env = "GADM_CACHE_BIND")]
    bind: String,
    /// google drive credentials file
    #[arg(short, long, env = "GDRIVE_SERVICE_ACCOUNT_FILE")]
    gdrive_credentials: Option<std::path::PathBuf>,
    /// google drive Folder ID where the GADM files can be downloaded
    #[arg(short, long, env = "GDRIVE_GADM_FOLDER_ID")]
    gdrive_folder_id: Option<String>,
    /// 3 letter country code, if used only those countries will be loaded into memory (and downloaded if missing)
    /// environment variable can contain comma separated values
    #[arg(short, env = "GADM_CACHE_COUNTRIES")]
    countries: Vec<String>,
    /// fall back URL if google drive is not accesible
    #[arg(long, default_value = "https://geodata.ucdavis.edu/gadm/gadm4.1/gpkg/")]
    gadm_fallback_url: String,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt().json().init();

    let mut options = Options::parse();
    options.countries = options
        .countries
        .into_iter()
        .flat_map(|c| {
            if c.contains(',') {
                c.split(',').map(|s| s.to_owned()).collect::<Vec<String>>()
            } else {
                vec![c]
            }
        })
        .collect();
    info!(options = format!("{:?}", options), "log replayer started");

    gdrive::download_gadm_files(&options).await?;
    // load cache before starting the webserver
    // allows us to share the cache without locks
    let all_gadm = Arc::new(gadm::load_gadm_cache(&options).await?);
    gadm::verify_gadm_cache(&all_gadm, &options)?;

    let app = Router::new()
        .route(
            "/status",
            get(gadm_status).with_state(Arc::clone(&all_gadm)),
        )
        .route("/id", get(gadm_id).with_state(Arc::clone(&all_gadm)));
    let listener = tokio::net::TcpListener::bind(&options.bind).await?;
    axum::serve(listener, app).await?;
    Ok(())
}
