# "GADM contains point" written as Rust service

- Use GADM data version 4.1
- data format is Geopackage, which in turn is a sqlite DB
- The coordinate reference system is longitude/latitude and the WGS84 datum.
- our data sources are <https://geodata.ucdavis.edu/gadm/gadm4.1/gpkg/> and <https://drive.google.com/drive/folders/10eFawlhQinxzqSGJXmU02N9jODy_dPhk> can also be found in `eos/temp_data/location_gfm`
- total size of sqlite files is 4.7GiB

## usage

```bash
./gadm-cache-rs --gadm-dir ~/Documents/eaternity/eos/temp_data/location_gfm
# or
export GADM_CACHE_DIR=~/Documents/eaternity/eos/temp_data/location_gfm
export GDRIVE_SERVICE_ACCOUNT_FILE=~/Documents/eaternity/eos/secrets/service_account.json
export GDRIVE_GADM_FOLDER_ID=10eFawlhQinxzqSGJXmU02N9jODy_dPhk
./gadm-cache-rs
```

## build

```bash
docker build -t gadm-cache-rs .
```

## deployment for CI

- Create a new git-tag that starts with `gadm-cache-v`, this will trigger CI to build and upload this version to the GitLab Package registry.
- once successfully uploaded in the package registry update `GADM_BIN_URL` and `GADM_BIN_NAME` in the `./ci/run_tests.sh` scripts accordingly.

The proper solution would be to split up the repositories and build each python and rust module on their own. For now it's all rolled into one, see the `.gitlab-ci.yml`.

## performance

- similar project, Java based <https://github.com/westnordost/countryboundaries> uses raster to have fast speeds
- use geoJSON for high speed country identification? <https://geojson.io/#map=2.52/20.09/1.59> found by clicking on `admin 0 countries` in <https://geojson.xyz/>
- rust based, inspired by the java project above <https://github.com/a-b-street/country-geocoder> using geojson aswell, WGS84 - same GADM data!
- GDAL is the engine behind FIONA <https://docs.rs/gdal-sys/> can be used by Rust too, e.h. using `gdal_sys::OGR_DS_GetLayerCount` function
  - not needed, loading directly from sqlite is enough and faster

## TODO

- [x] write minimal python PoC to measure speed and memory use
- [x] write minimal Rust PoC to measure speed and memory use
- [x] separate timings:
  - load GADM file into RAM
  - lookup coordinates to find ID
- [x] env variable for which GADM files should exist (so worst case need downloading), fallback is downloading ALL of them, comma separated list of 3 letter codes
- [x] download GADM files if local folder is empty, try first from google drive, if not possible from official sources
- [x] make a nested build Dockerfile for conveniently running the service without local rust install
- [x] Docker will likely have the local temp folder mounted into it

### optionally

- [ ] implement query for `seed_gadm_glossary()` inside of `core/gap_filling_modules/location_gfm.py`
  - [ ] `geopandas.geometry_area_perimeter` maps to [geo.geodesic_perimeter_area_signed](https://docs.rs/geo/latest/geo/geometry/struct.GeometryCollection.html#method.geodesic_perimeter_area_signed)
  - [ ] `geopandas.representative_point` maps to [InteriorPoint](https://docs.rs/geo/latest/geo/algorithm/interior_point/trait.InteriorPoint.html)
  - [ ] `geopandas.centroid` maps to [Centroid](https://docs.rs/geo/latest/geo/algorithm/centroid/trait.Centroid.html)
