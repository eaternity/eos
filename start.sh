#! /usr/bin/env sh
set -e

export APP_MODULE=api.app.server:fastapi_app
export GUNICORN_CONF=gunicorn_conf.py

exec poetry run gunicorn -k "uvicorn.workers.UvicornWorker" --preload -c "$GUNICORN_CONF" "$APP_MODULE"
