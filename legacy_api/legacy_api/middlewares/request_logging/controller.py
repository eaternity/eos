from types import TracebackType
from typing import Any, Optional

from fastapi import Request
from structlog.contextvars import bind_contextvars, clear_contextvars, get_contextvars

LOG_REQUEST_HEADERS = ["user-agent", "x-rewind-request-id"]
MAX_HEADER_VALUE_LEN = 150


class ContextVars:
    __original_context_vars: dict

    def __init__(self, **kwargs: Any):  # noqa: ANN401
        self.__original_context_vars = {}
        self.__context_vars = kwargs

    def __enter__(self):
        self.__original_context_vars = get_contextvars()
        bind_contextvars(**self.__context_vars)

    def __exit__(
        self,
        exc_type: Optional[type[BaseException]],
        exc_val: Optional[BaseException],
        exc_tb: Optional[TracebackType],
    ):
        clear_contextvars()
        bind_contextvars(**self.__original_context_vars)


def extract_request_metadata(request: Request) -> dict:
    client = request.client.host
    request_url = str(request.url)
    request_uri = request.url.path
    request_method = request.method

    all_headers = dict(request.headers)
    headers = {}
    for key, value in all_headers.items():
        if key.lower() in LOG_REQUEST_HEADERS:
            _value = value
            if not isinstance(_value, str):
                _value = str(_value)

            if len(_value) > MAX_HEADER_VALUE_LEN:
                _value = f"{_value[:MAX_HEADER_VALUE_LEN]}..."
            headers[key.lower()] = _value

    request_metadata = {
        "client": client,
        "url": request_url,
        "uri": request_uri,
        "method": request_method,
        "headers": headers,
    }

    return request_metadata
