import uuid
from typing import Annotated, Optional

import aiohttp
from fastapi import Depends, Query
from fastapi.security import APIKeyHeader
from starlette import status
from starlette.responses import Response
from structlog import get_logger

from core.domain.calculation import RequestedMassUnitEnum
from core.domain.props.referenceless_quantity_prop import ConvertibleUnits
from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.common import (
    REQUESTED_QUANTITY_REFERENCES,
    get_statuscode_and_error_message,
    sort_batch_into_xids,
)
from legacy_api.converter.to_batch_input_dto import to_serialized_batch_input_dto
from legacy_api.converter.to_recipe import convert_batch_item_to_recipe_result
from legacy_api.dto.legacy.recipe import RecipeResultWithStatusDto, WrappedRecipeDto

logger = get_logger()
settings = Settings()
api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


async def parse_recipe_result(
    api_response: aiohttp.ClientResponse,
    full_resource: Annotated[bool, Query(alias="full-resource")],
    indicators: bool,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")],
    recipe_id: str,
    request_id: Optional[int] = None,
) -> RecipeResultWithStatusDto:
    response_recipe = None
    statuscode = None
    response_request_id = request_id
    try:
        result = await api_response.json()
        if result.get("batch"):
            batch_by_xid = sort_batch_into_xids(result.get("batch"))
            batch_item = batch_by_xid.get(recipe_id, {})
            recipe = convert_batch_item_to_recipe_result(batch_item, full_resource, nutrient_values, indicators)
            statuscode, message, is_valid_recipe = get_statuscode_and_error_message(batch_item, recipe)
            if is_valid_recipe:
                response_recipe = recipe
            if not response_request_id:
                response_request_id = batch_item.get("request_id")
    except Exception as e:
        logger.error("Error parsing recipe result", error=str(e), exc_info=True)
    if not statuscode:
        statuscode = status.HTTP_500_INTERNAL_SERVER_ERROR
        message = "Internal server error"
    return RecipeResultWithStatusDto(
        statuscode=statuscode,
        message=message,
        request_id=response_request_id,
        recipe_id=recipe_id,
        recipe=response_recipe,
    )


async def create_new_recipe_common(
    recipe: WrappedRecipeDto,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
    skip_calc: Optional[bool] = None,
) -> RecipeResultWithStatusDto:
    if not recipe.recipe.id:
        recipe.recipe.id = str(uuid.uuid4())  # Generate ID for later identification from batch.

    if kitchen_id is not None:
        recipe.recipe.kitchen_id = kitchen_id

    api_response = await api_client.post_and_forward_error(
        f"{settings.api_v2_url}/calculation/graphs?priority=1",
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(dict(recipe.recipe.model_dump()), transient, skip_calc),
    )
    recipe_id = recipe.recipe.id
    return await parse_recipe_result(api_response, full_resource, indicators, nutrient_values, recipe_id)


async def get_recipe_by_id_common(
    recipe_id: str,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
    skip_calc: Optional[bool] = None,
) -> RecipeResultWithStatusDto:
    batch_to_get = [
        {
            "input_root": {
                "existing_root": {
                    "xid": recipe_id,
                },
            },
            "return_final_graph": True,
            "max_depth_of_returned_graph": 3,
            "gfms_to_skip": ["PostNutrientsAggregationGapFillingWorker"],
            "explicitly_attach_cached_elementary_resource_emission": False,
            "skip_calc": skip_calc,
        }
    ]
    batch_to_get[0]["requested_quantity_references"] = REQUESTED_QUANTITY_REFERENCES
    batch_to_get[0]["requested_units"] = {ConvertibleUnits.mass.value: RequestedMassUnitEnum.gram.value}

    if kitchen_id is not None:
        batch_to_get[0]["input_root"]["existing_root"]["access_group_xid"] = kitchen_id

    api_response = await api_client.post_and_forward_error(
        f"{settings.api_v2_url}/calculation/graphs?priority=1",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_get},
    )
    return await parse_recipe_result(api_response, full_resource, indicators, nutrient_values, recipe_id)


async def put_recipe_common(
    recipe_id: str,
    recipe: WrappedRecipeDto,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
    skip_calc: Optional[bool] = None,
) -> RecipeResultWithStatusDto:
    recipe.recipe.id = recipe_id
    if kitchen_id is not None:
        recipe.recipe.kitchen_id = kitchen_id

    api_response = await api_client.post_and_forward_error(
        f"{settings.api_v2_url}/calculation/graphs?priority=1",
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(dict(recipe.recipe.model_dump()), transient, skip_calc),
    )
    return await parse_recipe_result(api_response, full_resource, indicators, nutrient_values, recipe_id)


async def delete_recipe_common(
    recipe_id: str,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> None:
    batch_to_delete = [
        {
            "nodes_dto": {
                "existing_node": {
                    "xid": recipe_id,
                }
            }
        }
    ]
    if kitchen_id is not None:
        batch_to_delete[0]["nodes_dto"]["existing_node"]["access_group_xid"] = kitchen_id

    api_response = await api_client.post(
        f"{settings.api_v2_url}/calculation/delete_graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_delete},
    )
    logger.info("deletion with status", status=api_response.status)

    try:
        result = await api_response.json()
        batch_item = result.get("batch")[0]
        if batch_item.get("statuscode") == 200:
            response.status_code = 204
        else:
            response.status_code = batch_item["statuscode"]
    except Exception:
        response.status_code = api_response.status
