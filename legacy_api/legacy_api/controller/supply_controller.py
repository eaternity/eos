import uuid
from typing import Annotated, Optional

import aiohttp
from fastapi import Depends, Query
from fastapi.security import APIKeyHeader
from starlette import status
from starlette.responses import Response
from structlog import get_logger

from core.domain.calculation import RequestedMassUnitEnum
from core.domain.nodes import SupplySheetActivityNode
from core.domain.props.referenceless_quantity_prop import ConvertibleUnits
from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.common import (
    REQUESTED_QUANTITY_REFERENCES,
    get_statuscode_and_error_message,
    sort_batch_into_xids,
)
from legacy_api.converter.to_batch_input_dto import to_serialized_batch_input_dto
from legacy_api.converter.to_supply import convert_batch_item_to_supply_result
from legacy_api.dto.legacy.supply import PutOrPostSupplyResultDto, SupplyResultWithStatusDto, WrappedSupplyDto

logger = get_logger()
settings = Settings()
api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


async def parse_supply_result(
    api_response: aiohttp.ClientResponse,
    full_resource: Annotated[bool, Query(alias="full-resource")],
    indicators: bool,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")],
    supply_id: str,
    request_id: Optional[int] = None,
) -> SupplyResultWithStatusDto:
    response_supply = None
    statuscode = None
    response_request_id = request_id
    try:
        result = await api_response.json()
        if result.get("batch"):
            batch_by_xid = sort_batch_into_xids(result.get("batch"))
            batch_item = batch_by_xid.get(supply_id, {})
            supply = convert_batch_item_to_supply_result(batch_item, full_resource, nutrient_values, indicators)
            statuscode, message, is_valid_supply = get_statuscode_and_error_message(batch_item, supply)
            if is_valid_supply:
                response_supply = supply
            if not response_request_id:
                response_request_id = batch_item.get("request_id")
    except Exception as e:
        logger.error("Error parsing supply result", error=str(e), exc_info=True)
    if not statuscode:
        statuscode = status.HTTP_500_INTERNAL_SERVER_ERROR
        message = "Internal server error"
    return SupplyResultWithStatusDto(
        statuscode=statuscode,
        message=message,
        request_id=response_request_id,
        supply_id=supply_id,
        supply=response_supply,
    )


async def parse_supply_put_and_post(api_response: aiohttp.ClientResponse, supply_id: str) -> PutOrPostSupplyResultDto:
    response_request_id = None
    statuscode = None
    try:
        result = await api_response.json()
        if result.get("batch"):
            batch_by_xid = sort_batch_into_xids(result.get("batch"))
            correct_supply = batch_by_xid.get(supply_id, {})
            if correct_supply:
                statuscode = correct_supply["statuscode"]
                message = correct_supply["message"]
                response_request_id = correct_supply.get("request_id")
    except Exception as e:
        logger.error("Error parsing supply result", error=str(e), exc_info=True)
    if not statuscode:
        statuscode = status.HTTP_500_INTERNAL_SERVER_ERROR
        message = "Internal server error"
    return PutOrPostSupplyResultDto(
        statuscode=statuscode, message=message, supply_id=supply_id, request_id=response_request_id
    )


async def get_supply_by_id_common(
    supply_id: str,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
    skip_calc: Optional[bool] = None,
) -> SupplyResultWithStatusDto:
    batch_to_get = [
        {
            "input_root": {
                "existing_root": {
                    "xid": supply_id,
                },
            },
            "return_final_graph": True,
            "max_depth_of_returned_graph": 3,
            "gfms_to_skip": ["PostNutrientsAggregationGapFillingWorker"],
            "explicitly_attach_cached_elementary_resource_emission": False,
            "skip_calc": skip_calc,
        }
    ]
    batch_to_get[0]["requested_quantity_references"] = REQUESTED_QUANTITY_REFERENCES
    batch_to_get[0]["requested_units"] = {ConvertibleUnits.mass.value: RequestedMassUnitEnum.gram.value}

    if kitchen_id is not None:
        batch_to_get[0]["input_root"]["existing_root"]["access_group_xid"] = kitchen_id

    api_response = await api_client.post_and_forward_error(
        f"{settings.api_v2_url}/calculation/graphs?priority=1",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_get},
    )
    return await parse_supply_result(api_response, full_resource, indicators, nutrient_values, supply_id)


async def create_new_supply_common(
    supply: WrappedSupplyDto,
    transient: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
    skip_calc: Optional[bool] = None,
) -> PutOrPostSupplyResultDto:
    if not supply.supply.id:
        supply.supply.id = str(uuid.uuid4())  # Generate ID for later identification from batch.

    if kitchen_id is not None:
        supply.supply.kitchen_id = kitchen_id

    url = f"{settings.api_v2_url}/calculation/graphs?return_immediately=True&priority=0"

    api_response = await api_client.post_and_forward_error(
        url,
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(
            dict(supply.supply.model_dump()), transient, node_type=SupplySheetActivityNode.__name__, skip_calc=skip_calc
        ),
    )
    return await parse_supply_put_and_post(api_response, supply.supply.id)


async def put_supply_common(
    supply_id: str,
    supply: WrappedSupplyDto,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
    skip_calc: Optional[bool] = None,
) -> PutOrPostSupplyResultDto | SupplyResultWithStatusDto:
    supply.supply.id = supply_id
    if kitchen_id is not None:
        supply.supply.kitchen_id = kitchen_id

    if full_resource:
        url = f"{settings.api_v2_url}/calculation/graphs?priority=1"
    else:
        url = f"{settings.api_v2_url}/calculation/graphs?return_immediately=True&priority=0"

    api_response = await api_client.post_and_forward_error(
        url,
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(
            dict(supply.supply.model_dump()), transient, node_type=SupplySheetActivityNode.__name__, skip_calc=skip_calc
        ),
    )
    if full_resource:
        return await parse_supply_result(api_response, full_resource, indicators, nutrient_values, supply_id)
    else:
        return await parse_supply_put_and_post(api_response, supply_id)


async def delete_supply_common(
    supply_id: str,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),
    kitchen_id: Optional[str] = None,
) -> None:
    batch_to_delete = [
        {
            "nodes_dto": {
                "existing_node": {
                    "xid": supply_id,
                }
            }
        }
    ]
    if kitchen_id is not None:
        batch_to_delete[0]["nodes_dto"]["existing_node"]["access_group_xid"] = kitchen_id

    api_response = await api_client.post(
        f"{settings.api_v2_url}/calculation/delete_graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_delete},
    )
    logger.info("deletion with status", status=api_response.status)

    try:
        result = await api_response.json()
        batch_item = result.get("batch")[0]
        if batch_item.get("statuscode") == 200:
            response.status_code = 204
        else:
            response.status_code = batch_item["statuscode"]
    except Exception:
        response.status_code = api_response.status
