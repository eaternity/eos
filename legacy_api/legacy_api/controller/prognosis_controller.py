from typing import List

from fastapi import HTTPException, status
from structlog import get_logger

from core.domain.nodes.activity.food_processing_activity_node import FoodProcessingActivityNode
from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.dto.legacy.prognosis import RecipeSoldPortionsDto, RecipeUpdateStatusDto

logger = get_logger()
settings = Settings()


async def post_portions(
    credentials: str, kitchen_id: str, recipes_sold_portions: List[RecipeSoldPortionsDto]
) -> List[RecipeUpdateStatusDto]:
    partial_nodes = []
    for recipe in recipes_sold_portions:
        recipe_id = recipe.recipe_id
        partial_node = {
            "xid": recipe_id,
            "node_type": FoodProcessingActivityNode.__name__,
            **({"sold_portions": recipe.sold_portions} if recipe.sold_portions is not None else {}),
            **({"production_portions": recipe.production_portions} if recipe.production_portions is not None else {}),
        }
        partial_nodes.append(partial_node)

    api_response = await api_client.post(
        f"{settings.api_v2_url}/nodes/update_nodes",
        headers={"Authorization": credentials},
        json={"nodes": partial_nodes},
    )
    update_status = await api_response.json()

    if len(update_status) != len(recipes_sold_portions):
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Failed to assemble update response.",
        )

    recipes_status = []
    for recipe, was_updated in zip(recipes_sold_portions, update_status):
        recipes_status.append(
            RecipeUpdateStatusDto(
                **{"recipe-id": recipe.recipe_id, "status": "UPDATED" if was_updated else "NOT_FOUND"}
            )
        )
    return recipes_status
