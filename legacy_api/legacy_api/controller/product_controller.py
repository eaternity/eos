from fastapi import Depends
from fastapi.security import APIKeyHeader
from starlette import status
from starlette.responses import Response
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.to_batch_input_dto import to_serialized_batch_input_dto
from legacy_api.dto.legacy.product import ProductDto

logger = get_logger()
settings = Settings()
api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


async def put_product_common(
    product: ProductDto,
    response: Response,
    transient: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> str | Response:
    api_response = await api_client.post_and_forward_error(
        f"{settings.api_v2_url}/calculation/graphs?priority=1",
        headers={"Authorization": f"{credentials}"},
        json=to_serialized_batch_input_dto(dict(product.model_dump()), transient),
    )

    try:
        result = await api_response.json()
        batch_item = result.get("batch")[0]
        product = batch_item.get("final_root")
        if batch_item.get("statuscode") == 200:
            response.status_code = 202
        else:
            response.status_code = batch_item["statuscode"]
        response.body = b""
        return response
    except Exception as e:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        logger.exception(e)
        return "Internal server error"
