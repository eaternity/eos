import dataclasses
import hashlib
import re
import uuid
from typing import Optional

from structlog import get_logger

from core.domain.calculation import RequestedMassUnitEnum
from core.domain.nodes import (
    FoodProcessingActivityNode,
    FoodProductFlowNode,
    LinkingActivityNode,
    SupplySheetActivityNode,
)
from core.domain.props.legacy_api_information_prop import LegacyIngredientTypeEnum
from core.domain.props.referenceless_quantity_prop import ConvertibleUnits
from legacy_api.converter.common import (
    NEW_TO_LEGACY_API_NAME_MAPPING,
    PROPS_TO_MOVE_TO_ROOT_FLOW,
    PROPS_TO_REROUTE,
    REQUESTED_QUANTITY_REFERENCES,
    STRINGS_TO_CONVERT_TO_RAW_NAME,
)

logger = get_logger()


@dataclasses.dataclass
class AuxiliaryBatchInfo:
    valid_nodes: dict[str, list[int]]
    invalid_nodes: dict[str, list[int]]
    duplicated_supply_or_recipe_ids: set[str]
    duplicated_request_ids: set[int]


def build_warning_message_for_batch_item(
    recipe_or_supply_id: str, request_id: int, auxiliary_batch_info: AuxiliaryBatchInfo, node_type: str
) -> str:
    message = ""
    if recipe_or_supply_id in auxiliary_batch_info.duplicated_supply_or_recipe_ids:
        message += (
            f"{node_type.capitalize()} ID "
            f"'{recipe_or_supply_id}' in multiple batch items. Using the item appearing last in the list."
        )
    if request_id in auxiliary_batch_info.duplicated_request_ids:
        if message:
            message += "\n"
        message += f"Request ID '{request_id}' in multiple batch items. Using the item appearing last in the list."
    return message


def to_ingredient_and_to_root(recipe_supply_product_props: dict, node_type: str) -> tuple[dict, dict]:
    """Convert legacy recipe/supply/product to new BatchInputItemDto format."""
    sub_flows = []
    ingredient_activity_flow_pairs: dict[str, dict] = {}
    # handling ingredients declaration in products
    if recipe_supply_product_props.get("ingredients_declaration"):
        recipe_supply_product_props["ingredients_declaration"] = [
            {
                "language": "de",
                "value": recipe_supply_product_props.get("ingredients_declaration"),
            },
        ]

    if "kitchen_id" in recipe_supply_product_props:
        access_group_xid = recipe_supply_product_props["kitchen_id"]
    else:
        access_group_xid = None

    # remove None-ingredients
    if recipe_supply_product_props.get("nutrient_values"):
        recipe_nutrient_values = {
            key: value for key, value in recipe_supply_product_props.get("nutrient_values").items() if value is not None
        }
    # handling ingredients declaration in recipes & supplies
    else:
        recipe_nutrient_values = None

    # we wan to also have the information about the supplier in the ingredients
    supplier = recipe_supply_product_props.get("supplier")

    if ingredients := recipe_supply_product_props.get("ingredients"):
        for ingredient in ingredients:
            ingredient_node_type = FoodProductFlowNode.__name__
            link_to_xid_dict = {
                "xid": ingredient.get("id", str(uuid.uuid4()))
            }  # If ingredient ID is not given, generate here for correct linking.
            props_not_in_raw_input = {
                prop: ingredient.get(NEW_TO_LEGACY_API_NAME_MAPPING.get(prop, prop))
                for prop in PROPS_TO_REROUTE
                if ingredient.get(NEW_TO_LEGACY_API_NAME_MAPPING.get(prop, prop), None) is not None
            }

            # only take the first part of the language string if it is in the format xx-yy
            if "product_name" in props_not_in_raw_input:
                for lang_product_name in props_not_in_raw_input["product_name"]:
                    language_parts = re.split("[-_]", lang_product_name["language"])
                    if len(language_parts) == 2:
                        lang_product_name["language"] = language_parts[0]

            for string_to_convert in STRINGS_TO_CONVERT_TO_RAW_NAME:
                if string_to_convert in props_not_in_raw_input:
                    props_not_in_raw_input[string_to_convert] = {
                        "language": "en",
                        "value": props_not_in_raw_input[string_to_convert],
                    }

            amount_dict = {}
            if ingredient.get("amount") is not None:
                amount_dict["value"] = ingredient.get("amount")
                if ingredient.get("unit"):
                    amount_dict["unit"] = ingredient.get("unit")

            legacy_API_information_dict = {}
            ingredient_type = ingredient.get("type").value
            if ingredient_type:
                legacy_API_information_dict["type"] = ingredient_type
            else:
                legacy_API_information_dict["type"] = LegacyIngredientTypeEnum.conceptual_ingredients

            if ingredient.get("nutrient_values"):
                ingredient_nutrient_values = {
                    key: value for key, value in ingredient.get("nutrient_values").items() if value is not None
                }
            else:
                ingredient_nutrient_values = None

            if ingredient.get("ingredients_declaration"):
                ingredient_ingredients_declaration = [
                    {
                        "value": ingredient.get("ingredients_declaration"),
                        "language": "de",
                    }
                ]
            else:
                ingredient_ingredients_declaration = None

            # strip obsolete and unset keys
            keys_to_delete = [
                "type",
                "id",
                "amount",
                "unit",
                "nutrient_values",
                "ingredients_declaration",
                *[NEW_TO_LEGACY_API_NAME_MAPPING.get(prop, prop) for prop in PROPS_TO_REROUTE],
            ]
            keys_to_delete += [
                key for (key, value) in ingredient.items() if value is None and key not in keys_to_delete
            ]
            for key in keys_to_delete:
                if key in ingredient:
                    del ingredient[key]

            sub_flow_dict = {
                "node_type": ingredient_node_type,
                "access_group_xid": access_group_xid,
                "raw_input": ingredient,
                "link_to_sub_node": link_to_xid_dict,
                **props_not_in_raw_input,
            }

            if ingredient_ingredients_declaration:
                sub_flow_dict["ingredients_declaration"] = ingredient_ingredients_declaration

            if ingredient_nutrient_values:
                sub_flow_dict["nutrient_values"] = ingredient_nutrient_values

            if supplier:
                sub_flow_dict["supplier"] = supplier

            if ingredient_type and ingredient_type in (
                LegacyIngredientTypeEnum.conceptual_ingredients,
                LegacyIngredientTypeEnum.commercial_ingredients,
            ):
                sub_flow_dict["link_to_sub_node"]["xid"] = f"FoodService{sub_flow_dict['link_to_sub_node']['xid']}"
                ingredient_activity_flow_pair = {
                    "input_root": {
                        "activity": {
                            "xid": sub_flow_dict["link_to_sub_node"]["xid"],
                            "access_group_xid": access_group_xid,
                            "node_type": LinkingActivityNode.__name__,
                            "production_amount": {"value": 1.0, "unit": "kilogram"},
                            "legacy_API_information": legacy_API_information_dict,
                        },
                        "sub_flows": [
                            {
                                "node_type": FoodProductFlowNode.__name__,
                                "access_group_xid": access_group_xid,
                                "product_name": sub_flow_dict.get("product_name"),
                                "gtin": sub_flow_dict.get("gtin"),
                                "producer": sub_flow_dict.get("producer"),
                                "supplier": sub_flow_dict.get("supplier"),
                                "amount_in_original_source_unit": {"value": 1.0, "unit": "kilogram"},
                            }
                        ],
                    },
                    "skip_calc": True,
                }

                if sub_flow_ingredients_declaration := sub_flow_dict.get("ingredients_declaration"):
                    ingredient_activity_flow_pair["input_root"]["sub_flows"][0][
                        "ingredients_declaration"
                    ] = sub_flow_ingredients_declaration

                if sub_flow_nutrient_values := sub_flow_dict.get("nutrient_values"):
                    ingredient_activity_flow_pair["input_root"]["sub_flows"][0][
                        "nutrient_values"
                    ] = sub_flow_nutrient_values

                if sub_flow_dict.get("raw_input"):
                    if sub_flow_dict["raw_input"].get("linked_ingredient"):
                        ingredient_activity_flow_pair["input_root"]["activity"]["raw_input"] = {
                            "linked_ingredient": sub_flow_dict["raw_input"]["linked_ingredient"]
                        }
                        ingredient_activity_flow_pair["input_root"]["sub_flows"][0]["raw_input"] = {
                            "linked_ingredient": sub_flow_dict["raw_input"]["linked_ingredient"]
                        }

                ingredient_activity_flow_pairs[link_to_xid_dict["xid"]] = ingredient_activity_flow_pair

            if amount_dict:
                sub_flow_dict["amount_in_original_source_unit"] = amount_dict

            sub_flows.append(sub_flow_dict)

        del recipe_supply_product_props["ingredients"]

    if recipe_supply_product_props["id"]:
        recipe_id = recipe_supply_product_props["id"]
    else:
        recipe_id = str(uuid.uuid4())

    if "location" in recipe_supply_product_props:
        # it is a recipe -> already contains the field name location, does not need to be mapped to origin
        new_to_legacy_api_name_mapping = {
            key: value if key not in ("activity_location", "flow_location") else "location"
            for key, value in NEW_TO_LEGACY_API_NAME_MAPPING.items()
        }
    else:
        new_to_legacy_api_name_mapping = NEW_TO_LEGACY_API_NAME_MAPPING
    props_not_in_raw_input = {
        prop: recipe_supply_product_props.get(new_to_legacy_api_name_mapping.get(prop, prop))
        for prop in (set(PROPS_TO_REROUTE) - set(PROPS_TO_MOVE_TO_ROOT_FLOW))
        if (recipe_supply_product_props.get(new_to_legacy_api_name_mapping.get(prop, prop), None)) is not None
    }
    for string_to_convert in STRINGS_TO_CONVERT_TO_RAW_NAME:
        if string_to_convert in props_not_in_raw_input:
            props_not_in_raw_input[string_to_convert] = {
                "language": "en",
                "value": props_not_in_raw_input[string_to_convert],
            }

    for date_field in ("supply_date", "date"):
        if date_field in recipe_supply_product_props:
            props_not_in_raw_input["activity_date"] = recipe_supply_product_props[date_field]

    production_amount_dict = {}
    if recipe_supply_product_props.get("amount"):
        production_amount_dict["value"] = recipe_supply_product_props.get("amount")
    if recipe_supply_product_props.get("unit"):
        production_amount_dict["unit"] = recipe_supply_product_props.get("unit")

    servings = recipe_supply_product_props.get("recipe_portions")
    if servings is None:
        servings = recipe_supply_product_props.get("servings_deprecated")
    if servings is None:
        servings = 1

    if props_not_in_raw_input.get("recipe_portions") is None:
        props_not_in_raw_input["recipe_portions"] = servings

    if not production_amount_dict:
        if weight := recipe_supply_product_props.get("weight"):
            production_amount_dict["value"] = servings * weight
            production_amount_dict["unit"] = "gram"

    root_flow_props = {
        prop: recipe_supply_product_props.get(new_to_legacy_api_name_mapping.get(prop, prop))
        for prop in PROPS_TO_MOVE_TO_ROOT_FLOW
        if recipe_supply_product_props.get(new_to_legacy_api_name_mapping.get(prop, prop), None)
    }

    if node_type == FoodProcessingActivityNode.__name__:
        root_flow_props["amount_in_original_source_unit"] = {"value": 1 / servings, "unit": "production amount"}

    for string_to_convert in STRINGS_TO_CONVERT_TO_RAW_NAME:
        if string_to_convert in root_flow_props:
            root_flow_props[string_to_convert] = {
                "language": "en",
                "value": root_flow_props[string_to_convert],
            }
    if recipe_nutrient_values is not None:
        root_flow_props["nutrient_values"] = recipe_nutrient_values

    # It is left as FoodProductFlowNode even for SupplySheetActivityNode because we need to aggregate
    # food related properties (e.g., daily food unit in the root flow).
    root_flow_node_type = FoodProductFlowNode.__name__

    flow_dict = {"node_type": root_flow_node_type, **root_flow_props}

    # strip obsolete and unset keys
    keys_to_delete = [
        "id",
        "kitchen_id",
        "amount",
        "unit",
        "nutrient_values",
        "ingredients_declaration",
        "supply_date",
        "date",
        *[new_to_legacy_api_name_mapping.get(prop, prop) for prop in PROPS_TO_REROUTE],
        *[new_to_legacy_api_name_mapping.get(prop, prop) for prop in PROPS_TO_MOVE_TO_ROOT_FLOW],
    ]
    keys_to_delete += [
        key for (key, value) in recipe_supply_product_props.items() if value is None and key not in keys_to_delete
    ]
    for key in keys_to_delete:
        if key in recipe_supply_product_props:
            del recipe_supply_product_props[key]

    activity_dict = {
        "node_type": node_type,
        "xid": recipe_id,
        "access_group_xid": access_group_xid,
        "raw_input": recipe_supply_product_props,
        **props_not_in_raw_input,
    }

    if production_amount_dict:
        activity_dict["production_amount"] = production_amount_dict

    if node_type == FoodProcessingActivityNode.__name__:
        activity_dict["legacy_API_information"] = {
            "type": LegacyIngredientTypeEnum.recipes
        }  # It is a recipe not supply.

    batch_input_item_dict = {
        "input_root": {
            "flow": flow_dict,
            "activity": activity_dict,
            "sub_flows": sub_flows,
        },
    }
    return ingredient_activity_flow_pairs, batch_input_item_dict


def to_serialized_batch_input_dto(
    recipe_supply_product_props: dict,
    transient: bool,
    skip_calc: Optional[bool] = None,
    node_type: str = FoodProcessingActivityNode.__name__,
) -> dict:
    """Convert legacy recipe/supply/product to new BatchInputItemDto format."""
    ingredient_activity_flow_pairs, batch_input_item_dict = to_ingredient_and_to_root(
        recipe_supply_product_props, node_type
    )
    if transient:
        batch_input_item_dict["transient"] = True
    batch_list = [batch_input_item_dict] + list(ingredient_activity_flow_pairs.values())
    for request_id, batch_item in enumerate(batch_list):
        batch_item["return_final_graph"] = True
        batch_item["max_depth_of_returned_graph"] = 3
        batch_item["gfms_to_skip"] = ["PostNutrientsAggregationGapFillingWorker"]
        batch_item["explicitly_attach_cached_elementary_resource_emission"] = False
        batch_item["request_id"] = request_id
        batch_item["requested_quantity_references"] = REQUESTED_QUANTITY_REFERENCES
        batch_item["requested_units"] = {ConvertibleUnits.mass.value: RequestedMassUnitEnum.gram.value}
        if not batch_item.get("skip_calc"):  # don't override skip_calc if it was explicitely set to true
            batch_item["skip_calc"] = skip_calc

    batch_dict = {"batch": batch_list}
    return batch_dict


def has_cycle_or_undefined(batch_nodes: dict[str, list[str]]) -> dict[str, bool]:
    """Method to find if batch_nodes contain a cycle or have undefined references to sub-nodes.

    The method uses a depth first search https://www.geeksforgeeks.org/detect-cycle-in-a-graph/ to detect a cycle or
    undefined node in a batch.

    :param batch_nodes: dictionary of key = node-id and value = list of subnode-id's, e.g.,
    {"A": ["B", "C"], "B": ["A"], "C": ["A"]}.
    :return: result: dictionary of key = node-id and value = boolean expressing that the node has either a cycle or
    undefined references.
    """
    visited = {}
    result = {}

    for node_xid in batch_nodes:
        visited[node_xid] = False

    def depth_first_search(node_id: str, dfs_stack: list) -> bool:
        visited[node_id] = True
        dfs_stack.append(node_id)

        for sub_node_id in batch_nodes.get(node_id, []):
            if sub_node_id not in batch_nodes:
                result[node_id] = True  # Referenced node is not defined
                return True
            if not visited[sub_node_id]:
                if depth_first_search(sub_node_id, dfs_stack):
                    result[node_id] = True
                    return True
            elif sub_node_id in result and result[sub_node_id]:
                result[node_id] = True  # use previous result
                return True
            elif sub_node_id in dfs_stack:
                result[node_id] = True  # loop detected
                return True

        dfs_stack.pop()
        result[node_id] = False
        return False

    for node_xid in batch_nodes:
        stack = []
        if not visited[node_xid]:
            depth_first_search(node_xid, stack)

    return result


def get_valid_and_invalid_legacy_batch(
    batch: list, node_type: str, duplicated_supply_or_recipe_ids: set
) -> tuple[dict[str, list[int]], dict[str, list[int]], set]:
    supply_or_recipe_id_to_request_ids = {}
    if node_type == "recipe":
        node_with_sub_ingredients = {}
        for batch_item in batch:
            sub_supply_or_recipe_ids = []
            for ingredient in batch_item[node_type].get("ingredients"):
                if ingredient.get("type") == LegacyIngredientTypeEnum.recipes:
                    sub_supply_or_recipe_ids.append(ingredient["id"])

            if batch_item[node_type]["id"] in node_with_sub_ingredients:
                supply_or_recipe_id_to_request_ids[batch_item[node_type]["id"]].append(batch_item["request_id"])
                duplicated_supply_or_recipe_ids.add(batch_item[node_type]["id"])
            else:
                supply_or_recipe_id_to_request_ids[batch_item[node_type]["id"]] = [batch_item["request_id"]]

            node_with_sub_ingredients[batch_item[node_type]["id"]] = sub_supply_or_recipe_ids

        invalidity_dict = has_cycle_or_undefined(node_with_sub_ingredients)
        valid_nodes = {
            key: supply_or_recipe_id_to_request_ids[key] for key, val in invalidity_dict.items() if val is False
        }
        invalid_nodes = {
            key: supply_or_recipe_id_to_request_ids[key] for key, val in invalidity_dict.items() if val is True
        }
    elif node_type == "supply":
        for batch_item in batch:
            if batch_item[node_type]["id"] in supply_or_recipe_id_to_request_ids:
                supply_or_recipe_id_to_request_ids[batch_item[node_type]["id"]].append(batch_item["request_id"])
                duplicated_supply_or_recipe_ids.add(batch_item[node_type]["id"])
            else:
                supply_or_recipe_id_to_request_ids[batch_item[node_type]["id"]] = [batch_item["request_id"]]
        valid_nodes = supply_or_recipe_id_to_request_ids
        invalid_nodes = {}
    else:
        raise ValueError(f"node_type {node_type} is not valid, it must be one of recipe or supply")
    return valid_nodes, invalid_nodes, duplicated_supply_or_recipe_ids


def legacy_to_new_batch_converter(
    batch: list, node_type: str, transient: bool, skip_calc: Optional[bool] = None
) -> tuple[dict, AuxiliaryBatchInfo]:
    all_ingredients_dict = {}
    all_batch_items = []
    batch_with_unique_request_id = {}
    duplicated_supply_or_recipe_ids = set()
    duplicated_request_ids = set()

    for batch_item in batch:
        if not batch_item[node_type].get("id"):
            batch_item[node_type]["id"] = str(uuid.uuid4())

        if batch_item.get("request_id") is None:
            request_id_from_supply_or_recipe_id = int(
                hashlib.sha256(batch_item[node_type]["id"].encode()).hexdigest(), 16
            )
            if request_id_from_supply_or_recipe_id in batch_with_unique_request_id:
                duplicated_supply_or_recipe_ids.add(batch_item[node_type]["id"])

            batch_with_unique_request_id[request_id_from_supply_or_recipe_id] = batch_item
            batch_with_unique_request_id[request_id_from_supply_or_recipe_id][
                "request_id"
            ] = request_id_from_supply_or_recipe_id
        else:
            if batch_item.get("request_id") in batch_with_unique_request_id:
                duplicated_request_ids.add(batch_item.get("request_id"))

            batch_with_unique_request_id[batch_item.get("request_id")] = batch_item

    batch_with_unique_request_id = list(batch_with_unique_request_id.values())
    valid_nodes, invalid_nodes, duplicated_supply_or_recipe_ids = get_valid_and_invalid_legacy_batch(
        batch_with_unique_request_id, node_type, duplicated_supply_or_recipe_ids
    )

    for batch_item in batch_with_unique_request_id:
        if batch_item[node_type]["id"] in invalid_nodes:
            # Skipping invalid nodes.
            continue

        if batch_item["request_id"] != valid_nodes[batch_item[node_type]["id"]][-1]:
            # Skipping repeated supply_or_recipe id.
            continue

        if node_type == "supply":
            eos_node_type = SupplySheetActivityNode.__name__
        else:
            eos_node_type = FoodProcessingActivityNode.__name__

        ingredient_activity_flow_pairs, batch_input_item_dict = to_ingredient_and_to_root(
            batch_item[node_type], eos_node_type
        )

        if batch_item["transient"]:
            batch_input_item_dict["transient"] = True
        batch_input_item_dict["return_final_graph"] = True
        batch_input_item_dict["max_depth_of_returned_graph"] = 3
        batch_input_item_dict["gfms_to_skip"] = ["PostNutrientsAggregationGapFillingWorker"]
        batch_input_item_dict["explicitly_attach_cached_elementary_resource_emission"] = False
        batch_input_item_dict["requested_quantity_references"] = REQUESTED_QUANTITY_REFERENCES
        batch_input_item_dict["requested_units"] = {ConvertibleUnits.mass.value: RequestedMassUnitEnum.gram.value}
        if not batch_input_item_dict.get("skip_calc"):  # don't override skip_calc if it was explicitely set to true
            batch_input_item_dict["skip_calc"] = skip_calc

        all_batch_items.append(batch_input_item_dict)
        all_ingredients_dict.update(ingredient_activity_flow_pairs)

    if transient:
        for item in all_batch_items:
            item["transient"] = True
    all_batch_items.extend(list(all_ingredients_dict.values()))

    return {"batch": all_batch_items}, AuxiliaryBatchInfo(
        valid_nodes=valid_nodes,
        invalid_nodes=invalid_nodes,
        duplicated_supply_or_recipe_ids=duplicated_supply_or_recipe_ids,
        duplicated_request_ids=duplicated_request_ids,
    )
