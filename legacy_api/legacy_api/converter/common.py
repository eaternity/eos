import logging
import re
from enum import Enum
from types import MappingProxyType
from typing import Any, Optional, Union

from starlette import status

from core.domain.data_error import ErrorClassification
from core.domain.props.legacy_api_information_prop import LegacyIngredientTypeEnum
from legacy_api.dto.legacy.nutrient_value import nutrient_to_unit
from legacy_api.dto.legacy.recipe import RecipeResultDto
from legacy_api.dto.legacy.supply import SupplyResultDto

NEW_TO_LEGACY_API_NAME_MAPPING = MappingProxyType(
    {
        "product_name": "names",
        "flow_location": "origin",
        "activity_location": "origin",
        "raw_production": "production",
        "raw_conservation": "conservation",
        "raw_processing": "processing",
        "raw_transport": "transport",
    }
)

PROPS_TO_MOVE_TO_ROOT_FLOW = (
    "raw_conservation",
    "raw_production",
    "raw_processing",
    "product_name",
    "raw_transport",
    "transport",
    "flow_location",
    "gtin",
    "producer",
    "supplier",
)

INGREDIENT_PROP_DEFAULTS = {
    "origin": "",
    "transport": "",
    "production": "",
    "processing": "",
    "conservation": "",
    "packaging": "",
}

STRINGS_TO_CONVERT_TO_RAW_NAME = ("raw_production", "raw_conservation", "raw_processing")

PROPS_TO_REROUTE = (
    "raw_transport",
    "flow_location",
    "product_name",
    "production_amount",
    "percentage",
    "amount_in_original_source_unit",
    "raw_production",
    "raw_conservation",
    "raw_processing",
    "recipe_portions",
    "production_portions",
    "sold_portions",
    "gtin",
    "producer",
    "supplier",
)  # Most of these properties can be rerouted to/from legacy API in the same way, and therefore grouped together.


REQUESTED_QUANTITY_REFERENCES = ["amount_for_activity_production_amount"]

VITASCORE_EOS_TO_LEGACY_MAPPING = {
    "EOS_vita-score": "vita-score",
    "EOS_Diet-low-in-fruits": "fruit-risk-factor",
    "EOS_Diet-low-in-vegetables": "vegetable-risk-factor",
    "EOS_Diet-low-in-whole-grains": "wholegrain-risk-factor",
    "EOS_Diet-low-in-nuts-and-seeds": "nuts-seeds-risk-factor",
    "EOS_Diet-low-in-milk": "milk-risk-factor",
    "EOS_Diet-high-in-processed-meat": "processed-meat-risk-factor",
    "EOS_Diet-high-in-red-meat": "red-meat-risk-factor",
    "EOS_Diet-high-in-sodium-(salt)": "salt-risk-factor",
    "EOS_Diet-high-in-energy": "high-in-energy-risk-factor",
    "EOS_Diet-high-in-fat": "high-in-fat-risk-factor",
    "EOS_Diet-low-in-fat": "low-in-fat-risk-factor",
    "EOS_Diet-high-in-protein": "high-in-protein-risk-factor",
    "EOS_Diet-low-in-protein": "low-in-protein-risk-factor",
}


class NodeTypeEnum(str, Enum):
    recipe = "recipe"
    supply = "supply"


def conversion_factor_between_mass_units(source: str, target: str) -> float | None:
    if source == "gram":
        if target == "gram":
            return 1e0
        if target == "milligram":
            return 1e3
        if target == "microgram":
            return 1e6

    if source == "milligram":
        if target == "gram":
            return 1e-3
        if target == "milligram":
            return 1e0
        if target == "microgram":
            return 1e3

    if source == "microgram":
        if target == "gram":
            return 1e-6
        if target == "milligram":
            return 1e-3
        if target == "microgram":
            return 1e0

    if source == "kilogram":
        if target == "gram":
            return 1e3
        if target == "milligram":
            return 1e6
        if target == "microgram":
            return 1e9


def sort_batch_into_xids(batch: list) -> dict:
    batch_by_xid = {}
    for item in batch:
        if item.get("skip_calc"):
            continue
        elif item.get("final_root"):
            batch_by_xid[item.get("final_root").get("activity").get("xid")] = item
        elif item.get("input_root"):
            if item.get("input_root").get("activity"):
                batch_by_xid[item.get("input_root").get("activity").get("xid")] = item
            elif item.get("input_root").get("existing_root"):
                batch_by_xid[item.get("input_root").get("existing_root").get("xid")] = item
    return batch_by_xid


def get_final_graph_by_uid(batch_item: dict) -> dict:
    final_graph_by_uid = {}
    if final_graph_from_batch := batch_item.get("final_graph"):
        for node_in_graph in final_graph_from_batch:
            final_graph_by_uid[node_in_graph.get("uid")] = node_in_graph
    return final_graph_by_uid


def extract_source_data_raw(prop_name: str, prop_datum: str | list | dict) -> str | list | dict:
    if isinstance(prop_datum, dict) and prop_datum.get("prop_type"):
        if not prop_datum.get("source_data_raw"):
            logging.debug(f"Could not find source_data_raw for {prop_name}")
        else:
            return prop_datum.get("source_data_raw")
    else:
        return prop_datum


def convert_raw_name_to_str(props: dict) -> None:
    for string_to_convert in STRINGS_TO_CONVERT_TO_RAW_NAME:
        legacy_string_to_convert = NEW_TO_LEGACY_API_NAME_MAPPING.get(string_to_convert, string_to_convert)
        if legacy_string_to_convert in props:
            props[legacy_string_to_convert] = props[legacy_string_to_convert].get("value")


def separate_amount_into_value_and_unit(node: dict, props: dict) -> None:
    for amount_prop in ("amount", "amount_in_original_source_unit", "production_amount"):
        if node.get(amount_prop) and isinstance(node.get(amount_prop), dict):
            if amount_prop == "production_amount":
                amount = node.get(amount_prop).get("self_reference")
            else:
                amount = node.get(amount_prop).get("amount_for_activity_production_amount")

            unit = node.get(amount_prop).get("unit_term_name")
            if amount is not None and unit:
                props["amount"] = amount
                props["unit"] = unit.lower()
                break
            else:
                source_data_raw = extract_source_data_raw(amount_prop, node.get(amount_prop))
                if isinstance(source_data_raw, dict):
                    props["amount"] = source_data_raw.get("value")
                    props["unit"] = source_data_raw.get("unit")


def convert_location_from_list_to_string(props: dict, location_name: str) -> None:
    if props.get(location_name):
        if isinstance(props.get(location_name), list):
            if len(props[location_name]) > 1:
                logging.warning(
                    f"Multiple location found when converting to legacy DTO {props[location_name]}. "
                    "Taking the first one."
                )
            props[location_name] = props[location_name][0]


def extract_nutrients_from_nutrient_prop(nutrient_prop: dict) -> dict | None:
    if not nutrient_prop.get("amount_for_activity_production_amount"):
        return

    nutrient_dict = {}

    for nutrient in nutrient_prop.get("amount_for_activity_production_amount", {}).values():
        nutrient_type = nutrient.get("quantity_term_name", "").lower()
        value = nutrient.get("quantity", {}).get("value")
        unit = nutrient.get("quantity", {}).get("unit_term_name", "").lower()

        if value is not None and (nutrient_type and unit):
            if unit == "kilocalorie" and nutrient_type == "energy":
                nutrient_dict["energy_kcal"] = value
            elif unit == "kilojoule" and nutrient_type == "energy":
                nutrient_dict["energy_kjoule"] = value
            elif unit == "volume percent" and nutrient_type == "alcohol":
                nutrient_dict["alcohol_volume_percent"] = value
            else:
                conversion_factor = conversion_factor_between_mass_units(unit, nutrient_to_unit.get(nutrient_type, ""))
                if conversion_factor is not None:
                    nutrient_dict[f"{nutrient_type}_{nutrient_to_unit.get(nutrient_type,'')}"] = (
                        conversion_factor * value
                    )

    if nutrient_dict:
        return nutrient_dict
    else:
        return None


def extract_energy_from_nutrients(nutrients: dict) -> dict | None:
    for nutrient in nutrients.values():
        nutrient_type = nutrient.get("quantity_term_name", "").lower()
        value = nutrient.get("quantity", {}).get("value")
        unit = nutrient.get("quantity", {}).get("unit_term_name", "").lower()

        if value is not None and (nutrient_type and unit):
            if unit == "kilocalorie" and nutrient_type == "energy":
                return value
            elif unit == "kilojoule" and nutrient_type == "energy":
                return value / 4.184
    return None


def fill_in_nutrient_values(props: dict, node: dict) -> None:
    if node_nutrient_values := node.get("nutrient_values"):
        if node_nutrient_values.get("prop_type"):
            nutrient_prop = node_nutrient_values
            props["nutrient_values"] = extract_nutrients_from_nutrient_prop(nutrient_prop)
        else:
            props["nutrient_values"] = node_nutrient_values

    if not props.get("nutrient_values"):
        if aggregated_nutrients := node.get("aggregated_nutrients"):
            props["nutrient_values"] = extract_nutrients_from_nutrient_prop(aggregated_nutrients)


def convert_prop_data(
    props: dict[str, Any],
    node: dict[str, str | list | dict],
    parent_flow: Optional[dict[str, str | list | dict]] = None,
    new_to_legacy_api_name_mapping: dict[str, str] = NEW_TO_LEGACY_API_NAME_MAPPING,
    is_supply: bool = False,
    nutrient_values: bool = False,
) -> dict[str, Any]:
    if props is None:
        props = {}
    for prop in PROPS_TO_REROUTE:
        legacy_prop = new_to_legacy_api_name_mapping.get(prop, prop)
        if prop == "flow_location":  # flow_location is handled separately.
            if (
                node.get("legacy_API_information")
                and node.get("legacy_API_information").get("properties_pre_gfms")
                and node.get("legacy_API_information").get("properties_pre_gfms").get("origin")
            ):
                props[legacy_prop] = node.get("legacy_API_information").get("properties_pre_gfms").get("origin")
                continue

        if prop == "production_amount" or prop == "amount_in_original_source_unit":
            continue

        prop_data = node.get(prop)
        if prop_data is not None:
            if isinstance(prop_data, list):
                props[legacy_prop] = [extract_source_data_raw(prop, prop_datum) for prop_datum in prop_data]
            else:
                props[legacy_prop] = extract_source_data_raw(prop, prop_data)

    if parent_flow:
        for prop in PROPS_TO_MOVE_TO_ROOT_FLOW:
            legacy_prop = new_to_legacy_api_name_mapping.get(prop, prop)
            if prop_data := parent_flow.get(prop):
                if isinstance(prop_data, list):
                    props[legacy_prop] = [extract_source_data_raw(prop, prop_datum) for prop_datum in prop_data]
                else:
                    props[legacy_prop] = extract_source_data_raw(prop, prop_data)

    separate_amount_into_value_and_unit(node, props)
    convert_location_from_list_to_string(props, new_to_legacy_api_name_mapping.get("activity_location", "location"))
    convert_raw_name_to_str(props)
    if "activity_date" in node:
        if is_supply:
            date_name = "supply-date"
        else:
            date_name = "date"

        props[date_name] = node["activity_date"]

    if not parent_flow:
        # for legacy compatibility fill in default response values for ingredients if not present:
        for prop, default_response in INGREDIENT_PROP_DEFAULTS.items():
            if not props.get(prop):
                props[prop] = default_response

    if nutrient_values:
        if parent_flow:
            fill_in_nutrient_values(props, parent_flow)
        else:
            fill_in_nutrient_values(props, node)

    return props


def get_vitascore(vitascore: dict, amount_per_category_in_flow: dict) -> dict:
    vitascore_dict = {}
    for legacy_key in VITASCORE_EOS_TO_LEGACY_MAPPING.values():
        if legacy_key != "vita-score":
            vitascore_dict[f"{legacy_key}-amount-gram"] = 0.0

    for point in vitascore.values():
        if VITASCORE_EOS_TO_LEGACY_MAPPING.get(point["quantity_term_xid"]) is not None:
            legacy_key = f"{VITASCORE_EOS_TO_LEGACY_MAPPING[point['quantity_term_xid']]}-points"
            vitascore_dict[legacy_key] = round(point["quantity"]["value"], 2)

    for food_category in amount_per_category_in_flow.values():
        if VITASCORE_EOS_TO_LEGACY_MAPPING.get(food_category["quantity_term_xid"]) is not None:
            legacy_key = f"{VITASCORE_EOS_TO_LEGACY_MAPPING[food_category['quantity_term_xid']]}-amount-gram"
            try:
                amount_in_gram = food_category["quantity"]["value"] * conversion_factor_between_mass_units(
                    food_category["quantity"]["unit_term_name"].lower(), "gram"
                )
                vitascore_dict[legacy_key] = round(amount_in_gram, 2)
            except TypeError:
                # TODO: for the moment we don't return the available kcal-amounts in the legacy API
                # If we we want to return them, we need to implement a conversion function for the energy values
                vitascore_dict[legacy_key] = 0.0

    return vitascore_dict


def get_energy_kcal(parent_flow: dict) -> float | None:
    energy_kcal = None
    if (
        parent_flow.get("nutrient_values") is not None
        and parent_flow["nutrient_values"].get("amount_for_activity_production_amount") is not None
    ):
        energy_kcal = extract_energy_from_nutrients(
            parent_flow["nutrient_values"]["amount_for_activity_production_amount"]
        )
    if (
        not energy_kcal
        and parent_flow.get("aggregated_nutrients") is not None
        and parent_flow["aggregated_nutrients"].get("amount_for_activity_production_amount") is not None
    ):
        energy_kcal = extract_energy_from_nutrients(
            parent_flow["aggregated_nutrients"]["amount_for_activity_production_amount"]
        )
    return energy_kcal


def get_results_and_indicators(parent_flow: dict, indicators: bool) -> tuple[dict, dict]:
    try:
        food_unit = round(parent_flow["daily_food_unit"]["amount_for_activity_production_amount"] * 5, 2)
    except (TypeError, KeyError):
        food_unit = None

    try:
        co2_rating = parent_flow["impact_assessment"]["co2_rating"]
    except (TypeError, KeyError):
        co2_rating = None

    try:
        eaternity_award = parent_flow["impact_assessment"]["co2_award"]
    except (TypeError, KeyError):
        eaternity_award = None

    try:
        co2_value_improvement_percentage = round(parent_flow["impact_assessment"]["co2_value_improvement_percentage"])
    except (TypeError, KeyError):
        co2_value_improvement_percentage = None

    try:
        co2_value_reduction_value_in_kg = parent_flow["impact_assessment"]["co2_value_reduction_value"][
            "amount_for_activity_production_amount"
        ]
        co2_value_reduction_value = round(co2_value_reduction_value_in_kg * 1000)  # Convert to gram for legacy API.
    except (TypeError, KeyError):
        co2_value_reduction_value = None

    if indicators:
        try:
            vita_score = get_vitascore(
                parent_flow["vitascore"]["amount_for_activity_production_amount"],
                parent_flow["amount_per_category_in_flow"]["amount_for_activity_production_amount"],
            )

            if parent_flow["vitascore"].get("vitascore_rating") is not None:
                vita_score["vita-score-rating"] = parent_flow["vitascore"].get("vitascore_rating")
            else:
                vita_score["vita-score-rating"] = ""

            if parent_flow["vitascore"].get("vitascore_improvement_percentage") is not None:
                vita_score["vita-score-improvement-percentage"] = round(
                    parent_flow["vitascore"].get("vitascore_improvement_percentage"), 2
                )

            energy_kcal = get_energy_kcal(parent_flow)
            if energy_kcal is not None:
                vita_score["energy-kcals"] = round(energy_kcal, 2)
                vita_score["nutrition-label"] = 450 < energy_kcal < 850

        except (TypeError, KeyError):
            vita_score = None

        # Scarce Water Consumption:
        scarce_water_dict = {}
        try:
            scarce_water_dict["scarce-water-liters"] = round(
                parent_flow["scarce_water_consumption"]["amount_for_activity_production_amount"], 2
            )
        except (TypeError, KeyError):
            scarce_water_dict["scarce-water-liters"] = None

        for key in (
            "scarce_water_consumption_rating",
            "scarce_water_consumption_improvement_percentage",
            "scarce_water_consumption_reduction_value",
            "scarce_water_consumption_award",
        ):
            legacy_key = re.sub("scarce_water_consumption", "water_footprint", key)
            legacy_key = re.sub("_", "-", legacy_key)
            try:
                if key == "scarce_water_consumption_reduction_value":
                    value = parent_flow["scarce_water_consumption"][key]["amount_for_activity_production_amount"]
                else:
                    value = parent_flow["scarce_water_consumption"][key]
                if isinstance(value, float):
                    value = round(value, 2)
                scarce_water_dict[legacy_key] = value
            except (TypeError, KeyError):
                scarce_water_dict[legacy_key] = None

        try:
            rainforest_rating = parent_flow["rainforest_critical_products"]["rainforest_rating"]
        except (TypeError, KeyError):
            rainforest_rating = None

        try:
            animal_treatment_rating = parent_flow["animal_products"]["animal_welfare_rating"]
        except (TypeError, KeyError):
            animal_treatment_rating = None
    else:
        vita_score = None
        rainforest_rating = None
        animal_treatment_rating = None
        scarce_water_dict = {}

    if rainforest_rating is None:
        rainforest_label = None
    elif rainforest_rating == "A":
        rainforest_label = True
    else:
        rainforest_label = False

    if animal_treatment_rating is None:
        animal_treatment_label = None
    elif animal_treatment_rating == "A":
        animal_treatment_label = True
    else:
        animal_treatment_label = False

    indicators_dict = {
        "vita_score": vita_score,
        "environment": {
            "rainforest_label": rainforest_label,
            "rainforest_rating": rainforest_rating,
            "animal_treatment_label": animal_treatment_label,
            "animal_treatment_rating": animal_treatment_rating,
            **scarce_water_dict,
        },
    }
    parent_results = {
        "rating": co2_rating,
        "eaternity-award": eaternity_award,
        "co2-value-improvement-percentage": co2_value_improvement_percentage,
        "co2-value-reduction-value": co2_value_reduction_value,
        "food-unit": food_unit,
    }
    return indicators_dict, parent_results


def find_quantity_value(quantities_dict: dict[str, dict], quantity_term_name: str) -> Optional[float]:
    """Helper function to extract a quantity value from a dictionary of quantities."""
    for _, value in quantities_dict.items():
        if "quantity_term_name" in value and value["quantity_term_name"] == quantity_term_name:
            return value["quantity"]["value"]
    return None


# TODO: deal with link_to_xid
def convert_ingredient_ids_to_legacy(ingredients: list) -> list:
    for ingredient in ingredients:
        # convert back to legacy API:
        if ingredient["uid"]:
            ingredient["id"] = ingredient["uid"]
            del ingredient["uid"]
        if ingredient["xid"]:
            ingredient["id"] = ingredient["xid"]
            del ingredient["xid"]
    return ingredients


# Unused since ingredient declaration is not returned in the response payload.
def convert_ingredient_declarations(ingredients: list) -> list:
    for ingredient in ingredients:
        try:
            ingredient_declarations = ingredient.get("ingredients-declaration", [{}])

            ingredients_declaration = ""

            # TODO: rework this approach once we implement working with multiple languages?
            # for now, we prioritize picking the first german declaration in the list
            for declaration in ingredient_declarations:
                if declaration.get("language") == "de":
                    ingredients_declaration = declaration.get("value")
                    break

            # if we don't have any german declaration, then pick the first one
            # as it is supposed to be the primary one
            if not ingredients_declaration:
                ingredients_declaration = ingredient_declarations[0].get("value")

            ingredient["ingredients-declaration"] = ingredients_declaration

        except TypeError:
            ingredient["ingredients-declaration"] = ""

    return ingredients


def find_node_from_final_graph(node_uid: str, final_graph: dict) -> dict | None:
    node = None
    for graph_node in final_graph:
        if graph_node.get("uid") == node_uid:
            node = graph_node
            break
    return node


def get_sub_nodes_from_graph(node: dict, final_graph: dict | None) -> dict | None:
    node_uid = node.get("uid")
    if not final_graph:
        return
    if not node_uid:
        return

    self_node = find_node_from_final_graph(node_uid, final_graph)
    if not self_node:
        return

    sub_node_uids = self_node.get("sub_node_uids")
    if not sub_node_uids:
        return

    return find_node_from_final_graph(sub_node_uids[0], final_graph)


def convert_batch_item_to_result(
    batch_item: dict, full_resource: bool, nutrient_values: bool, indicators: bool, node_type: NodeTypeEnum
) -> RecipeResultDto | SupplyResultDto | dict | None:
    final_root = batch_item.get("final_root")

    if final_root:
        node = final_root.get("activity")

        try:
            co2_value_in_kg = find_quantity_value(
                node["impact_assessment"]["amount_for_activity_production_amount"], "IPCC 2013 climate change GWP 100a"
            )
            # in legacy api co2-value needs to be returned in "[g CO₂e(quivalent) / serving (given or normalized)]"
            # the new api uses impact assessment unit 'EOS_kg-co2-eq',
            # so we convert kg to gram and normalize by serving:
            root_co2_value_for_activity_prod_amount = co2_value_in_kg * 1000
        except TypeError:
            root_co2_value_for_activity_prod_amount = None

        parent_flow = final_root.get("flow")
        try:
            co2_value_in_kg = find_quantity_value(
                parent_flow["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            )
            # in legacy api co2-value needs to be returned in "[g CO₂e(quivalent) / serving (given or normalized)]"
            # the new api uses impact assessment unit 'EOS_kg-co2-eq',
            # so we convert kg to gram and normalize by serving:
            root_co2_value_for_root_flow = co2_value_in_kg * 1000
        except TypeError:
            root_co2_value_for_root_flow = None

        try:
            if parent_flow["amount"]["unit_term_xid"] == "EOS_gram":
                final_weight_per_portion = parent_flow["amount"]["amount_for_activity_production_amount"]
            else:
                final_weight_per_portion = None
        except TypeError:
            final_weight_per_portion = None

        sub_flows = final_root.get("sub_flows", [])
        ingredients_out = []
        if full_resource:
            for sub_flow in sub_flows:
                try:
                    co2_value_in_kg = find_quantity_value(
                        sub_flow["impact_assessment"]["amount_for_activity_production_amount"],
                        "IPCC 2013 climate change GWP 100a",
                    )
                    # in legacy api co2-value needs to be returned in
                    # "[g CO₂equivalent / serving (given or normalized)]"
                    # the new api uses impact assessment unit 'EOS_kg-co2-eq',
                    # so we convert kg to gram and normalize by serving (root flow amount is 1/servings):
                    co2_value = co2_value_in_kg * 1000
                except TypeError:
                    co2_value = None

                sub_flow_indicator_dict, sub_flow_results = get_results_and_indicators(sub_flow, indicators)

                # rename "food-unit" field and remove "eaternity-award" field for backwards compatibility:
                sub_flow_results = {
                    "foodUnit" if k == "food-unit" else k: v
                    for k, v in sub_flow_results.items()
                    if k != "eaternity-award"
                }

                # to get the original input properties instead of the properties from the merged sub flow
                # we use the the original properties that are stored in the legacy API information
                if sub_flow.get("legacy_API_information") and (
                    properties_pre_gfms := sub_flow.get("legacy_API_information").get("properties_pre_gfms")
                ):
                    for key, value in properties_pre_gfms.items():
                        sub_flow[key] = value

                sub_flow_props = sub_flow.get("raw_input", {})
                sub_flow_props = convert_prop_data(sub_flow_props, sub_flow, nutrient_values=nutrient_values)

                if sub_flow.get("raw_input", {}).get("linked_ingredient"):
                    ingredient_id = sub_flow.get("raw_input").get("linked_ingredient").get("id")
                elif sub_flow.get("link_to_sub_node"):
                    ingredient_id = sub_flow.get("link_to_sub_node").get("xid")
                elif sub_flow.get("xid"):
                    ingredient_id = sub_flow.get("xid")
                else:
                    ingredient_id = sub_flow.get("uid")

                if root_co2_value_for_activity_prod_amount is not None and co2_value is not None:
                    if root_co2_value_for_activity_prod_amount == 0 and co2_value == 0:
                        bar_chart = 0
                    else:
                        bar_chart = round(100 * co2_value / root_co2_value_for_activity_prod_amount)
                else:
                    bar_chart = None

                sub_activity = get_sub_nodes_from_graph(sub_flow, batch_item.get("final_graph"))
                if sub_activity:
                    legacy_API_information = sub_activity.get("legacy_API_information")
                else:
                    legacy_API_information = None
                if not legacy_API_information:
                    # The merging gap filling module deletes the LinkingActivityNode, which carries the information:
                    # legacy_API_information.type = conceptual-ingredients.
                    ingredient_type = LegacyIngredientTypeEnum.conceptual_ingredients
                else:
                    ingredient_type = legacy_API_information.get("type")

                ingredient_id = ingredient_id.removeprefix("FoodService")
                ingredients_out.append(
                    {
                        "id": ingredient_id,
                        "co2-value": round(co2_value) if co2_value is not None else None,
                        "bar-chart": bar_chart,
                        **sub_flow_results,
                        **sub_flow_props,
                        "type": ingredient_type,
                    }
                )
                if indicators:
                    ingredients_out[-1]["indicators"] = sub_flow_indicator_dict

        matrix_gfm_error = node.get("matrix_gfm_error")
        if matrix_gfm_error:
            info_text = "Failed calculating co2. Probably missing matching ingredient."
        else:
            info_text = None

        indicator_dict, parent_flow_results = get_results_and_indicators(parent_flow, indicators)

        if node.get("raw_input"):
            recipe_or_supply_props = node.get("raw_input")
            keys_to_delete = ["id", "kitchen_id"]
            for key in keys_to_delete:
                if key in recipe_or_supply_props:
                    del recipe_or_supply_props[key]
        else:
            recipe_or_supply_props = {}

        if node_type == NodeTypeEnum.recipe:
            # Unlike products, supplies, and ingredients, recipes have a location field instead of an origin field
            new_to_legacy_api_name_mapping = {
                key: value if key not in ("activity_location", "flow_location") else "location"
                for key, value in NEW_TO_LEGACY_API_NAME_MAPPING.items()
            }
        else:
            new_to_legacy_api_name_mapping = NEW_TO_LEGACY_API_NAME_MAPPING

        recipe_or_supply_props = convert_prop_data(
            recipe_or_supply_props,
            node,
            parent_flow=parent_flow,
            new_to_legacy_api_name_mapping=new_to_legacy_api_name_mapping,
            is_supply=(node_type == NodeTypeEnum.supply),
        )

        # we need to return the new and old field names, because javaland is doing the same:

        if node_type == NodeTypeEnum.recipe:
            servings = node.get("recipe_portions", None)
            if servings is None:
                servings = 1
            if recipe_or_supply_props.get("recipe_portions") is None:
                recipe_or_supply_props["recipe_portions"] = servings
            if recipe_or_supply_props.get("servings_deprecated") is None:
                recipe_or_supply_props["servings_deprecated"] = servings

        recipe_or_supply_result_dict = {
            "id": node.get("xid"),
            "kitchen_id": node.get("access_group_xid"),
            "ingredients": ingredients_out,
            "info_text": info_text,
            "co2_value": round(root_co2_value_for_root_flow) if root_co2_value_for_root_flow is not None else None,
            "final_weight_per_portion": final_weight_per_portion,
            **parent_flow_results,
            **recipe_or_supply_props,
        }
        if indicators:
            recipe_or_supply_result_dict["indicators"] = indicator_dict

        if node_type == NodeTypeEnum.recipe:
            recipe_or_supply_result_dto = RecipeResultDto.model_validate(recipe_or_supply_result_dict)
        else:
            recipe_or_supply_result_dto = SupplyResultDto.model_validate(recipe_or_supply_result_dict)

        return recipe_or_supply_result_dto
    else:
        if batch_item:
            return batch_item
        else:
            return None


def get_statuscode_and_error_message(
    batch_item: dict, supply_or_recipe: Union[SupplyResultDto, RecipeResultDto, dict, None]
) -> tuple[int, str, str]:
    is_valid_recipe_or_supply = False
    statuscode = None
    if isinstance(supply_or_recipe, (SupplyResultDto, RecipeResultDto)):
        is_valid_recipe_or_supply = True
        if missing_ingredient_error := next(
            (
                data_error
                for data_error in batch_item.get("data_errors", [])
                if data_error.get("error_classification") == ErrorClassification.missing_sub_flows_in_client_sent_data
            ),
            None,
        ):
            statuscode = 670
            message = "At least one (sub-) recipe has no ingredients."
            xid = missing_ingredient_error.get("additional_specification", {}).get("xid")
            if xid is not None:
                message += f" Id that caused this issue: {xid}."
            if batch_item.get("legacyAPI_response_behavior") and (
                empty_recipe_behavior := batch_item.get("legacyAPI_response_behavior").get("empty_recipe_behavior")
            ):
                for field_name in empty_recipe_behavior.get("remove_fields_from_response", []):
                    if getattr(supply_or_recipe, field_name, None) is not None:
                        setattr(supply_or_recipe, field_name, None)
                if empty_recipe_behavior.get("statuscode") is not None:
                    statuscode = empty_recipe_behavior.get("statuscode")
                if empty_recipe_behavior.get("message") is not None:
                    message = empty_recipe_behavior.get("message")
        elif next(
            (
                data_error
                for data_error in batch_item.get("data_errors", [])
                if data_error.get("error_classification") == ErrorClassification.missing_matching
            ),
            None,
        ):
            statuscode = 602
            message = (
                "At least one ingredient name could not get automatically matched into the Eaternity Database. "
                "Retry once a day or try with a different ingredient name."
            )
            all_missing_matching_names = [
                data_error.get("additional_specification", {}).get("name")
                for data_error in batch_item.get("data_errors", [])
                if data_error.get("error_classification") == ErrorClassification.missing_matching
            ]
            # Sort to have deterministic output for the tests
            sorted_sub_lists = [sorted(sub_list) for sub_list in all_missing_matching_names]
            sorted_outer_list = sorted(sorted_sub_lists, key=lambda x: x[0] if x else "")

            if all_missing_matching_names:
                message += f" Names that couldn't be matched: {sorted_outer_list}."
        elif missing_lca_inventory_error := next(
            (
                data_error
                for data_error in batch_item.get("data_errors", [])
                if data_error.get("error_classification") == ErrorClassification.missing_lca_inventory
            ),
            None,
        ):
            statuscode = 671
            message = "For at least one ingredient no lca inventory was found."
            terms = missing_lca_inventory_error.get("additional_specification", {}).get("terms")
            if terms is not None:
                message += f" Combination of terms for which no lca inventory was found: {terms}."
        elif missing_nutrients_error := next(
            (
                data_error
                for data_error in batch_item.get("data_errors", [])
                if data_error.get("error_classification") == ErrorClassification.missing_nutrients
            ),
            None,
        ):
            statuscode = 672
            message = (
                "For at least one ingredient no nutrients were found. "
                f"{missing_nutrients_error.get('additional_specification', '')}"
            )
            terms = missing_nutrients_error.get("additional_specification", {}).get("terms")
            if terms is not None:
                message += f" Combination of terms for which no nutrients were found: {terms}."
        elif failed_amount_estimation_error := next(
            (
                data_error
                for data_error in batch_item.get("data_errors", [])
                if data_error.get("error_classification") == ErrorClassification.failed_amount_estimation
            ),
            None,
        ):
            statuscode = 673
            message = "For at least one ingredient, amount estimation failed. "
            additional_specification = failed_amount_estimation_error.get("additional_specification", {})
            if additional_specification:
                declaration = failed_amount_estimation_error.get("additional_specification", {}).get("declaration")
                if declaration is not None:
                    message += f" Declaration for which the amount estimation failed: {declaration}."

        elif supply_or_recipe.co2_value is None:
            statuscode = 678
            message = "something went wrong, cannot calculate co2 value"
        elif supply_or_recipe.indicators and supply_or_recipe.indicators.vita_score is None:
            statuscode = 679
            message = "something went wrong, cannot calculate vita score"
        elif supply_or_recipe.indicators and supply_or_recipe.indicators.environment is None:
            statuscode = 680
            message = "something went wrong, cannot calculate environment score"
        elif any(
            data_error.get("error_classification") == ErrorClassification.fallback_fastest_transport
            for data_error in batch_item.get("data_errors", [])
        ):
            statuscode = 681
            message = (
                "For at least one ingredient, even the fastest mode of transport was too slow given the storage time. "
                "Nevertheless, the fastest mode was added as a fallback."
            )
        elif any(
            data_error.get("error_classification") == ErrorClassification.fallback_without_amount_estimation
            for data_error in batch_item.get("data_errors", [])
        ):
            statuscode = batch_item["statuscode"]
            message = (
                "For at least one ingredient, nutrient subdivision was skipped due to amount estimation failure "
                "or missing nutrients."
            )
        elif any(
            data_error.get("error_classification") == ErrorClassification.zero_sum_ingredient_amounts
            for data_error in batch_item.get("data_errors", [])
        ):
            statuscode = batch_item["statuscode"]
            message = "For at least one ingredient that has an amount, the sub-ingredients summed to zero amount."
        else:
            statuscode = batch_item["statuscode"]
            message = batch_item["message"]
    elif isinstance(supply_or_recipe, dict):
        msg = supply_or_recipe["message"]
        kitchen_id = None
        input_root = supply_or_recipe.get("input_root")
        if "activity" in input_root:
            kitchen_id = input_root["activity"]["access_group_xid"]
        else:
            kitchen_id = input_root["existing_root"]["access_group_xid"]
        if msg == f"Group {kitchen_id} does not exist.":
            statuscode = 611
            message = msg
        else:
            statuscode = supply_or_recipe["statuscode"]
            message = supply_or_recipe["message"]
    if not statuscode:
        statuscode = status.HTTP_500_INTERNAL_SERVER_ERROR
        message = "Internal server error"
    return statuscode, message, is_valid_recipe_or_supply
