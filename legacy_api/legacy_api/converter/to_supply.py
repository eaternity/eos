from legacy_api.converter.common import NodeTypeEnum, convert_batch_item_to_result
from legacy_api.dto.legacy.supply import SupplyResultDto


def convert_batch_item_to_supply_result(
    batch_item: dict, full_resource: bool, nutrient_values: bool, indicators: bool
) -> SupplyResultDto | dict | None:
    return convert_batch_item_to_result(
        batch_item, full_resource, nutrient_values, indicators, node_type=NodeTypeEnum.supply
    )
