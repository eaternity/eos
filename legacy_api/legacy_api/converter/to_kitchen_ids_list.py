def convert_access_group_ids_to_kitchen_ids(access_group_ids: list[dict]) -> list[str]:
    kitchen_ids = []

    for access_group in access_group_ids:
        kitchen_ids.append(access_group.get("xid", ""))

    return kitchen_ids
