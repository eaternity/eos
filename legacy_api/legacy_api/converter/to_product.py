from legacy_api.converter.common import convert_prop_data, find_quantity_value, get_results_and_indicators
from legacy_api.dto.legacy.product import ProductResultDto


def convert_node_to_product_result(
    recipe: dict, full_resource: bool, nutrient_values: bool, indicators: bool
) -> ProductResultDto:
    node = recipe.get("activity")
    parent_flow = recipe.get("flow")
    sub_flows = recipe.get("sub_flows", [])

    ingredients_out = []
    if full_resource:
        for sub_flow in sub_flows:
            sub_flow_props = sub_flow.get("raw_input", {})
            sub_flow_props = convert_prop_data(sub_flow_props, sub_flow)

            try:
                co2_value_in_kg = find_quantity_value(
                    sub_flow["impact_assessment"]["amount_for_activity_production_amount"],
                    "IPCC 2013 climate change GWP 100a",
                )
                # the new api uses impact assessment unit 'EOS_kg-co2-eq',
                # so we convert kg to gram and normalize by serving:
                co2_value = round(co2_value_in_kg * 1000)
            except TypeError:
                co2_value = None

            sub_flow_indicator_dict, sub_flow_results = get_results_and_indicators(sub_flow, indicators)

            # rename for backwards compatibility:
            sub_flow_results = {"foodUnit" if k == "food-unit" else k: v for k, v in sub_flow_results.items()}

            if sub_flow.get("xid"):
                ingredient_id = sub_flow.get("xid")
            else:
                ingredient_id = sub_flow.get("uid")
            ingredients_out.append({"id": ingredient_id, "co2-value": co2_value, **sub_flow_results, **sub_flow_props})
            if indicators:
                ingredients_out[-1]["indicators"] = sub_flow_indicator_dict

    matrix_gfm_error = node.get("matrix_gfm_error")
    if matrix_gfm_error:
        info_text = "Failed calculating co2. Probably missing matching ingredient."
    else:
        info_text = None

    try:
        co2_value_in_kg = find_quantity_value(
            node["impact_assessment"]["amount_for_activity_production_amount"], "IPCC 2013 climate change GWP 100a"
        )
        # the new api uses impact assessment unit 'EOS_kg-co2-eq',
        # so we convert kg to gram and normalize by serving:
        co2_value = round(co2_value_in_kg * 1000)
    except TypeError:
        co2_value = None

    indicator_dict, parent_flow_results = get_results_and_indicators(parent_flow, indicators)

    if node.get("raw_input"):
        recipe_props = node.get("raw_input")
        keys_to_delete = ["id", "kitchen_id"]
        for key in keys_to_delete:
            if key in recipe_props:
                del recipe_props[key]
    else:
        recipe_props = {}

    recipe_props = convert_prop_data(recipe_props, node, parent_flow, nutrient_values=nutrient_values)

    recipe_result_dict = {
        "id": node.get("xid"),
        "kitchen_id": node.get("access_group_xid"),
        "ingredients": ingredients_out,
        "info_text": info_text,
        "co2_value": co2_value,
        **parent_flow_results,
        **recipe_props,
    }

    if indicators:
        recipe_result_dict["indicators"] = indicator_dict

    recipe_result_dto = ProductResultDto.model_validate(recipe_result_dict)

    return recipe_result_dto
