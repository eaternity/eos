from legacy_api.converter.common import NodeTypeEnum, convert_batch_item_to_result, sort_batch_into_xids
from legacy_api.dto.legacy.recipe import RecipeResultDto


def convert_batch_to_recipe_result(
    batch: list, full_resource: bool, nutrient_values: bool, indicators: bool, recipe_id: str
) -> RecipeResultDto | dict | None:
    batch_by_xid = sort_batch_into_xids(batch)
    batch_item = batch_by_xid.get(recipe_id, {})
    return convert_batch_item_to_recipe_result(batch_item, full_resource, nutrient_values, indicators)


def convert_batch_item_to_recipe_result(
    batch_item: dict, full_resource: bool, nutrient_values: bool, indicators: bool
) -> RecipeResultDto | dict | None:
    return convert_batch_item_to_result(
        batch_item, full_resource, nutrient_values, indicators, node_type=NodeTypeEnum.recipe
    )
