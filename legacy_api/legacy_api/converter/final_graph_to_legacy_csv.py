from typing import List, Tuple

from api.controller.report_generation_controller import DISPLAY_FOOD_UNIT


def translate_final_graph_to_recipe_csv(final_graph: List[dict], current_row_index: int) -> Tuple[int, List[dict]]:
    new_rows = []
    for row in final_graph:
        if row["level"] == 0:
            if row.get("flow_unit") == "Gram":
                to_gram = 1
            elif row.get("flow_unit") == "Kilogram":
                to_gram = 1000
            elif row.get("flow_amount_per_root_node"):
                raise ValueError(f"Unexpected flow unit: {row.get('flow_unit')}")
            new_row = {
                "Row Index": current_row_index,
                "ID": row.get("xid", "").removeprefix("FoodService"),
                "Title": row.get("name").lstrip(" "),
                "Instructions": "TODO",  # has to be migrated from jl and implemented in the sheet-converter
                "Create Date": "TODO",  # has to be added to the sheet-converter?
                "Production Date": row.get("production_date"),
                "Kitchen Id": row.get("access_group_xid"),
                "Kitchen Name": "TODO",  # has to be added to the sheet-converter?
                "Weight [gram/serving]": (
                    row.get("flow_amount_per_root_node") * to_gram  # convert from kg to g
                    if row.get("flow_amount_per_root_node")
                    else ""
                ),
                "CO2-Value [gram CO2/serving]": (
                    int(row.get("kg_co2eq_per_root_node") * 1000)  # convert from kg to g
                    if row.get("kg_co2eq_per_root_node")
                    else ""
                ),
                "Energy [kJ/serving]": (
                    row.get("EOS_nutrients_energy_kilocalorie_per_root_node") * 4.184
                    if row.get("EOS_nutrients_energy_kilocalorie_per_root_node")
                    else ""
                ),
                "Portions per Recipe": row.get("recipe_portions", ""),
                "Recipe Portions planned or produced": row.get("production_portions", ""),
                "Recipe Portions sold": row.get("sold_portions", ""),
                "Animal Welfare Label": True if row.get("animal_welfare_rating") == "A" else False,
                "Rainforest Label": True if row.get("rainforest_rating") == "A" else False,
                "Water Scarcity Footprint [liter/serving]": row.get("liter_scarce_water_consumption_per_root_node"),
                "Water Scarcity Rating": row.get("scarce_water_consumption_rating"),
                "Vita score [Serving]": row.get("vita_score_points_per_root_node"),
                "Vita Score Rating": row.get("vitascore_rating"),
                # TODO: here we probably want to include the new vita-score categories (high-in-fat etc.)
                # also we need to extend the sheet-converter to also return g/serving
                "Fruit Risk Factor Amount [g/serving]": "TODO",
                "Fruit Risk Points [points/serving]": row.get("diet_low_in_fruits_points_per_root_node"),
                "Vegetable Risk Factor Amount [g/serving]": "TODO",
                "Vegetable Risk Points [points/serving]": row.get("diet_low_in_vegetables_points_per_root_node"),
                "Wholegrain Risk Factor Amount [g/serving]": "TODO",
                "Wholegrain Risk Points [points/serving]": row.get("diet_low_in_whole_grains_points_per_root_node"),
                "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
                "Nuts Seeds Risk Points [points/serving]": row.get("diet_low_in_nuts_and_seeds_points_per_root_node"),
                "Milk Risk Factor Amount [g/serving]": "TODO",
                "Milk Risk Points [points/serving]": row.get("diet_low_in_milk_points_per_root_node"),
                "Processed Meat Risk Factor Amount [g/serving]": "TODO",
                "Processed Meat Risk Points [points/serving]": row.get(
                    "diet_high_in_processed_meat_points_per_root_node"
                ),
                "Red Meat Risk Factor Amount [g/serving]": "TODO",
                "Red Meat Risk Points [points/serving]": row.get("diet_high_in_red_meat_points_per_root_node"),
                "Salt Risk Factor Amount [g/serving]": "TODO",
                "Salt Risk Points [points/serving]": row.get("diet_high_in_sodium_(salt)_points_per_root_node"),
                "Eaternity Award": row.get("co2_award"),
                "Vegetarian/Vegan": "TODO",  # not yet implemented in eos
                "Categorization Tags": "TODO",  # not yet implemented in eos
                "Base Product Tags": row.get("amount_per_category_in_flow"),  # not yet aligned with jl
                "Food Unit": (
                    row.get("aggregated_daily_food_unit_per_root_node") * 5 / DISPLAY_FOOD_UNIT
                    if row.get("aggregated_daily_food_unit_per_root_node")
                    else ""
                ),
                "CO2 FU Rating": row.get("co2_rating"),
                "CO2 Improvement Percentage": row.get("co2_value_improvement_percentage"),
                "Menu Line Name": "TODO",  # has to be implemented properly in the sheet-converter
                "Menu Line ID": "TODO",  # has to be implemented properly in the sheet-converter
                "Palm oil Amount (critical)": "TODO",  # not yet implemented in eos
                "Palm oil Amount (not critical)": "TODO",  # not yet implemented in eos
                "Soy Amount (critical)": "TODO",  # not yet implemented in eos
                "Soy Amount (not critical)": "TODO",  # not yet implemented in eos
                "Animal Welfare Rating": row.get("animal_welfare_rating"),
                "Rainforest Rating": row.get("rainforest_critical_products_rating"),
                "Water Scarcity Reduction Value": row.get(
                    "liter_scarce_water_consumption_reduction_value_per_root_node"
                ),
                "Water Scarcity Improvement Percentage": row.get("scarce_water_consumption_improvement_percentage"),
                "Vita Score Improvement Percentage": row.get("vitascore_improvement_percentage"),
            }
            new_row = {k: round(v, 3) if isinstance(v, float) else v for k, v in new_row.items()}
            new_rows.append(new_row)
            current_row_index += 1
    return current_row_index, new_rows


def translate_final_graph_to_ingredient_csv(final_graph: List[dict], current_row_index: int) -> Tuple[int, List[dict]]:
    recipe_data = {}
    new_rows = []
    recipe_portions = 1
    ingredient_index = 0
    for row in final_graph:
        if row["level"] == 0:
            # store the recipe data
            recipe_data = {
                "ID": row.get("xid", "").removeprefix("FoodService"),
                "Title": row.get("name").lstrip(" "),
                "Location": "TODO",  # kitchen-location -> has to be added to the sheet-converter
                "Kitchen Name": "TODO",  # has to be added to the sheet-converter?
                "Production Date": row.get("production_date"),
            }
            recipe_portions = row.get("recipe_portions")
            if not recipe_portions:
                recipe_portions = 1
            recipe_portion_numbers = {
                "Portions per Recipe": recipe_portions,
                "Recipe Portions planned or produced": row.get("production_portions", ""),
                "Recipe Portions sold": row.get("sold_portions", ""),
            }
        else:
            if row.get("flow_unit") == "Gram":
                to_gram = 1
            elif row.get("flow_unit") == "Kilogram":
                to_gram = 1000
            elif row.get("flow_amount_per_root_node"):
                raise ValueError(f"Unexpected flow unit: {row.get('flow_unit')}")
            # TODO: check that all quantities are for the correct reference
            new_row = {
                "Row Index": current_row_index,
                **recipe_data,
                "Index": ingredient_index,
                "Weight [g]": (
                    row.get("flow_amount_per_root_node") * recipe_portions * to_gram
                    if row.get("flow_amount_per_root_node")
                    else ""
                ),
                "Name": row.get("name").lstrip(" "),
                "Type": "TODO",  # not yet implemented in eos
                "Product ID": row.get("xid", "").removeprefix("FoodService"),
                "Production": "TODO",  # has to be added to the sheet-converter
                "Origin": row.get("flow_location"),
                "Transport": row.get("raw_transport"),
                "g CO2": (
                    row.get("kg_co2eq_per_root_node") * recipe_portions * 1000
                    if row.get("kg_co2eq_per_root_node")
                    else 0.0
                ),
                "CO2 Base Value [g]": (
                    row.get("kg_co2eq_only_base_per_root_node") * recipe_portions * 1000
                    if row.get("kg_co2eq_only_base_per_root_node")
                    else 0.0
                ),
                "CO2 Greenhouse [g]": (
                    row.get("kg_co2eq_only_greenhouse_per_root_node") * recipe_portions * 1000
                    if row.get("kg_co2eq_per_rkg_co2eq_only_greenhouse_per_root_nodeoot_node")
                    else 0.0
                ),
                "CO2 Organic [g]": 0.0,  # not yet implemented in eos
                "CO2 Frozen Conservation [g]": (
                    row.get("kg_co2eq_only_processing_freezing_per_root_node") * recipe_portions * 1000
                    if row.get("kg_co2eq_only_processing_freezing_per_root_node")
                    else 0.0
                ),
                "CO2 Land Transport [g]": (
                    row.get("kg_co2eq_only_truck_transport_per_root_node") * recipe_portions * 1000
                    if row.get("kg_co2eq_only_truck_transport_per_root_node")
                    else 0.0
                ),
                "CO2 Sea Transport [g]": (
                    row.get("kg_co2eq_only_sea_transport_per_root_node") * recipe_portions * 1000
                    if row.get("kg_co2eq_only_sea_transport_per_root_node")
                    else 0.0
                ),
                "CO2 Air Transport [g]": (
                    row.get("kg_co2eq_only_air_transport_per_root_node") * recipe_portions * 1000
                    if row.get("kg_co2eq_only_air_transport_per_root_node")
                    else 0.0
                ),
                "Energy [kJ]": (
                    row.get("EOS_nutrients_energy_kilocalorie_per_root_node") * recipe_portions * 4.184
                    if row.get("EOS_nutrients_energy_kilocalorie_per_root_node")
                    else ""
                ),
                "Food Unit": (
                    row.get("aggregated_daily_food_unit_per_root_node") * 5 * recipe_portions
                    if row.get("aggregated_daily_food_unit_per_root_node")
                    else ""
                ),
                "CO2 Improvement Percentage": row.get(
                    "co2_value_improvement_percentage"
                ),  # TODO: very different from jl
                "CO2 Reduction Value": row.get(
                    "kg_co2eq_reduction_value_per_root_node"
                ),  # TODO: very different from jl
                "Water Scarcity Footprint [liter]": (
                    row.get("liter_scarce_water_consumption_per_root_node") * recipe_portions
                    if row.get("liter_scarce_water_consumption_per_root_node")
                    else ""
                ),
                "Total Soy Amount [g/serving] (critical)": "TODO",  # not yet implemented in eos
                "Total Soy Amount [g/serving] (not critical)": "TODO",  # not yet implemented in eos
                "Total Palmoil Amount [g/serving] not certified": "TODO",  # not yet implemented in eos
                "Total Palmoil Amount [g/serving] certified": "TODO",  # not yet implemented in eos
                "Tags": "TODO",  # not yet implemented in eos
                "meat origins": "TODO",  # not yet implemented in eos
                "Animal Welfare Rating": row.get("animal_welfare_rating"),
                "Rainforest Rating": row.get("rainforest_critical_products_rating"),
                "Water Scarcity Rating": row.get("scarce_water_consumption_rating"),
                "CO2 FU Rating": row.get("co2_rating"),
                # TODO: here we probably want to include the new vita-score categories (high-in-fat etc.)
                # also we need to extend the sheet-converter to also return g/serving
                "Fruit Risk Factor Amount [g/serving]": "TODO",
                "Fruit Risk Points [points/serving]": row.get("diet_low_in_fruits_points_per_root_node"),
                "Vegetable Risk Factor Amount [g/serving]": "TODO",
                "Vegetable Risk Points [points/serving]": row.get("diet_low_in_vegetables_points_per_root_node"),
                "Wholegrain Risk Factor Amount [g/serving]": "TODO",
                "Wholegrain Risk Points [points/serving]": row.get("diet_low_in_whole_grains_points_per_root_node"),
                "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
                "Nuts Seeds Risk Points [points/serving]": row.get("diet_low_in_nuts_and_seeds_points_per_root_node"),
                "Milk Risk Factor Amount [g/serving]": "TODO",
                "Milk Risk Points [points/serving]": row.get("diet_low_in_milk_points_per_root_node"),
                "Processed Meat Risk Factor Amount [g/serving]": "TODO",
                "Processed Meat Risk Points [points/serving]": row.get(
                    "diet_high_in_processed_meat_points_per_root_node"
                ),
                "Red Meat Risk Factor Amount [g/serving]": "TODO",
                "Red Meat Risk Points [points/serving]": row.get("diet_high_in_red_meat_points_per_root_node"),
                "Salt Risk Factor Amount [g/serving]": "TODO",
                "Salt Risk Points [points/serving]": row.get("diet_high_in_sodium_(salt)_points_per_root_node"),
                "Vita Score Improvement Percentage": row.get("vitascore_improvement_percentage"),
                "Vita score [Serving]": row.get("vita_score_points_per_root_node"),
                "Vita Score Rating": row.get("vitascore_rating"),
                "Water Scarcity Improvement Percentage": row.get("scarce_water_consumption_improvement_percentage"),
                "Portions per Recipe": row.get("recipe_portions", ""),
                "Recipe Portions planned or produced": row.get("production_portions", ""),
                "Recipe Portions sold": row.get("sold_portions", ""),
                "Water Scarcity Reduction Value": (
                    row.get("liter_scarce_water_consumption_reduction_value_per_root_node") * recipe_portions
                    if row.get("liter_scarce_water_consumption_reduction_value_per_root_node")
                    else ""
                ),
            }
            new_row = {k: round(v, 3) if isinstance(v, float) else v for k, v in new_row.items()}
            new_row.update(recipe_portion_numbers)
            new_rows.append(new_row)
            current_row_index += 1
            ingredient_index += 1
    return current_row_index, new_rows
