import uuid
from typing import Annotated

from fastapi import APIRouter, Depends, Query, Response
from fastapi.security import APIKeyHeader
from starlette import status
from structlog import get_logger

from core.domain.calculation import RequestedMassUnitEnum
from core.domain.props.referenceless_quantity_prop import ConvertibleUnits
from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.controller.product_controller import put_product_common
from legacy_api.converter.common import REQUESTED_QUANTITY_REFERENCES
from legacy_api.converter.to_product import convert_node_to_product_result
from legacy_api.dto.legacy.product import ProductDto, ProductResultDto

logger = get_logger()
settings = Settings()
product_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@product_router.get("/{product_id}", response_model_exclude_none=True)
async def get_product_by_id(
    product_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008
) -> ProductResultDto | str:
    batch_to_get = [
        {
            "input_root": {
                "existing_root": {
                    "xid": product_id,
                },
            },
            "return_final_graph": True,
            "max_depth_of_returned_graph": 3,
            "gfms_to_skip": ["PostNutrientsAggregationGapFillingWorker"],
            "explicitly_attach_cached_elementary_resource_emission": False,
        }
    ]
    batch_to_get[0]["requested_quantity_references"] = REQUESTED_QUANTITY_REFERENCES
    batch_to_get[0]["requested_units"] = {ConvertibleUnits.mass.value: RequestedMassUnitEnum.gram.value}

    api_response = await api_client.post_and_forward_error(
        f"{settings.api_v2_url}/calculation/graphs?priority=1",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_get},
    )
    try:
        result = await api_response.json()
        batch_item = result.get("batch")[0]
        product = batch_item.get("final_root")
        if product:
            return convert_node_to_product_result(product, full_resource, nutrient_values, indicators)
        else:
            response.status_code = batch_item["statuscode"]
            return batch_item.get("message", "")
    except Exception as e:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        logger.exception(e)
        return "Internal server error"


# TODO: deprecate this endpoint since it's not mentioned in Apiary API docs?
@product_router.post("/", deprecated=True)
async def create_new_product(
    product: ProductDto,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008
) -> str:
    logger.info("called create_new_product")
    if not product.id:
        product.id = str(uuid.uuid4())
    return await put_product_common(product, response=response, transient=False, credentials=credentials)


@product_router.put("/{product_id}", response_model_exclude_none=True)
async def put_product(
    product_id: str,
    product: ProductDto,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008
) -> str:
    logger.info("called put_product")
    product.id = product_id
    return await put_product_common(product, response=response, transient=False, credentials=credentials)


# TODO: there is no such endpoint in API documentation, should it stay just in case?
@product_router.delete("/{product_id}", status_code=204, response_class=Response)
async def delete_product(
    product_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)  # noqa: B008
) -> None:
    batch_to_delete = [
        {
            "nodes_dto": {
                "existing_node": {
                    "xid": product_id,
                }
            }
        }
    ]

    api_response = await api_client.post(
        f"{settings.api_v2_url}/calculation/delete_graphs",
        headers={"Authorization": f"{credentials}"},
        json={"batch": batch_to_delete},
    )
    logger.info("deletion with status", status=api_response.status)

    try:
        result = await api_response.json()
        batch_item = result.get("batch")[0]
        if batch_item.get("statuscode") == 200:
            response.status_code = 204
        else:
            response.status_code = batch_item["statuscode"]
    except Exception:
        response.status_code = api_response.status
