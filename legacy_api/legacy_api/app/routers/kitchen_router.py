from typing import Union

from fastapi import APIRouter, Depends, Request, Response, status
from fastapi.responses import PlainTextResponse
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.to_kitchen_ids_list import convert_access_group_ids_to_kitchen_ids
from legacy_api.dto.legacy.api import ApiResponseDto, Error500ResponseDto, handle_api_response
from legacy_api.dto.legacy.kitchen import GetKitchensResultDto, WrappedKitchenDto

logger = get_logger()
settings = Settings()
kitchen_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@kitchen_router.post(
    "/",
    include_in_schema=False,
    # response_model=Union[WrappedKitchenDto, ApiResponseDto, Error500ResponseDto],
    response_model=None,
    # response_model_exclude_none=True,
)
@kitchen_router.post(
    "",
    response_model=None,
    # response_model=Union[WrappedKitchenDto, ApiResponseDto, Error500ResponseDto], response_model_exclude_none=True
)
async def create_new_kitchen(
    kitchen: WrappedKitchenDto,
    response: Response,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedKitchenDto | ApiResponseDto | Error500ResponseDto:
    kitchen_data = kitchen.model_dump().get("kitchen")
    kitchen_data["type"] = "kitchen"

    api_response = await api_client.post(
        f"{settings.api_v2_url}/access-groups/upsert_access_group?legacy_api_request=true",
        headers={"Authorization": f"{credentials}"},
        json={
            "access_group": kitchen_data,
        },
    )

    async def deal_with(result: dict) -> WrappedKitchenDto | ApiResponseDto:
        kitchen = result.get("access_group")

        if kitchen:
            response.status_code = 201
            response.headers[
                "location"
            ] = f"{str(request.base_url).rstrip('/')}{settings.BASE_PATH}/kitchens/{kitchen['xid']}"
            return WrappedKitchenDto(kitchen=kitchen)
        else:
            response.status_code = api_response.status
            return ApiResponseDto(api_response=kitchen)

    return await handle_api_response(response, api_response, deal_with)


@kitchen_router.get("/", include_in_schema=False)
@kitchen_router.get("")
async def get_all_kitchens(
    response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> GetKitchensResultDto | ApiResponseDto | Error500ResponseDto:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/access-groups/get_sub_access_groups?legacy_api_request=true&access_group_type=kitchen",
        headers={"Authorization": f"{credentials}"},
    )

    async def deal_with(kitchens: dict) -> GetKitchensResultDto | ApiResponseDto:
        response.status_code = api_response.status
        if api_response.status < 300 and kitchens is not None:
            return GetKitchensResultDto(kitchens=convert_access_group_ids_to_kitchen_ids(kitchens))
        return ApiResponseDto(api_response=kitchens)

    return await handle_api_response(response, api_response, deal_with)


@kitchen_router.get(
    "/{kitchen_id}",
    # response_model=Union[WrappedKitchenDto, PlainTextResponse, Error500ResponseDto],
    # response_model_exclude_none=True,
    response_model=None,
)
async def get_kitchen_by_id(
    kitchen_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> WrappedKitchenDto | PlainTextResponse | Error500ResponseDto:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/access-groups/get_access_group_by_id",
        headers={"Authorization": f"{credentials}"},
        json={
            "access_group": {
                "xid": kitchen_id,
            },
        },
    )

    async def deal_with(result: dict) -> WrappedKitchenDto | PlainTextResponse:
        kitchen = result.get("access_group")
        response.status_code = api_response.status
        if api_response.status < 300 and kitchen is not None:
            return WrappedKitchenDto(kitchen=kitchen)
        else:
            return PlainTextResponse("Kitchen not found", status_code=api_response.status)

    return await handle_api_response(response, api_response, deal_with)


@kitchen_router.put(
    "/{kitchen_id}",
    response_model=Union[WrappedKitchenDto, ApiResponseDto, Error500ResponseDto],
    response_model_exclude_none=True,
)
async def put_kitchen(
    kitchen_id: str,
    kitchen: WrappedKitchenDto,
    response: Response,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> WrappedKitchenDto | ApiResponseDto | Error500ResponseDto:
    kitchen_data = kitchen.model_dump().get("kitchen")
    kitchen_data["type"] = "kitchen"
    kitchen_data["xid"] = kitchen_id

    api_response = await api_client.post(
        f"{settings.api_v2_url}/access-groups/upsert_access_group?legacy_api_request=true",
        headers={"Authorization": f"{credentials}"},
        json={
            "access_group": kitchen_data,
        },
    )

    async def deal_with(result: dict) -> WrappedKitchenDto | ApiResponseDto:
        kitchen = result.get("access_group")
        response.status_code = api_response.status
        if api_response.status < 300 and kitchen is not None:
            response.headers[
                "location"
            ] = f"{str(request.base_url).rstrip('/')}{settings.BASE_PATH}/kitchens/{kitchen['xid']}"
            return WrappedKitchenDto(kitchen=kitchen)
        else:
            return ApiResponseDto(api_response=kitchen)

    return await handle_api_response(response, api_response, deal_with)


@kitchen_router.delete("/{kitchen_id}", status_code=204, response_class=Response)
async def delete_kitchen(
    kitchen_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> None:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/access-groups/delete_access_group",
        headers={"Authorization": f"{credentials}"},
        json={"access_group": {"xid": kitchen_id}},
    )
    if api_response.status == status.HTTP_200_OK:
        response.status_code = status.HTTP_204_NO_CONTENT
    else:
        response.status_code = api_response.status
