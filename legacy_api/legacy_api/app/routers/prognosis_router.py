from fastapi import APIRouter, Depends
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.settings import Settings
from legacy_api.controller.prognosis_controller import post_portions
from legacy_api.dto.legacy.prognosis import RecipeSoldPortionsWrapperDto, RecipeUpdateStatusWrapperDto

logger = get_logger()
settings = Settings()
prognosis_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@prognosis_router.post("/{kitchen_id}/portions/", include_in_schema=False)
@prognosis_router.post("/{kitchen_id}/portions")
async def update_portions(
    kitchen_id: str,
    recipe_sold_portions: RecipeSoldPortionsWrapperDto,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> RecipeUpdateStatusWrapperDto:
    recipeUpdateStatus = await post_portions(credentials, kitchen_id, recipe_sold_portions.recipes)
    return RecipeUpdateStatusWrapperDto(recipes=recipeUpdateStatus)
