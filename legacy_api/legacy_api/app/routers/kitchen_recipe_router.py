import json
from typing import Annotated, Optional, Union

from fastapi import APIRouter, Depends, Header, Query, Response
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.controller.recipe_controller import (
    create_new_recipe_common,
    delete_recipe_common,
    get_recipe_by_id_common,
    put_recipe_common,
)
from legacy_api.dto.legacy.recipe import GetRecipesResultDto, RecipeResultWithStatusDto, WrappedRecipeDto

logger = get_logger()
settings = Settings()
kitchen_recipe_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@kitchen_recipe_router.get("/{recipe_id}", response_model_exclude_none=True)
async def get_kitchen_recipe_by_id(
    kitchen_id: str,
    recipe_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> RecipeResultWithStatusDto:
    recipe_result_with_status = await get_recipe_by_id_common(
        recipe_id, full_resource, nutrient_values, indicators, credentials, kitchen_id=kitchen_id, skip_calc=skip_calc
    )
    response.status_code = recipe_result_with_status.statuscode
    return recipe_result_with_status


@kitchen_recipe_router.get("/", include_in_schema=False, response_model_exclude_none=True)
@kitchen_recipe_router.get("", response_model_exclude_none=True)
async def get_all_kitchen_recipes(
    kitchen_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> GetRecipesResultDto | str:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/nodes/get_nodes?node_type=FoodProcessingActivityNode",
        headers={"Authorization": f"{credentials}"},
        json=[{"access_group": {"xid": kitchen_id}}],
    )
    try:
        nodes = await api_response.json()
    except Exception:
        nodes = None

    if nodes is not None:
        return GetRecipesResultDto(recipes=[node.get("xid", "") for node in nodes])
    else:
        response.status_code = api_response.status_code

        return json.dumps({"api_response": api_response.text})


@kitchen_recipe_router.post(
    "/", include_in_schema=False, response_model=Union[RecipeResultWithStatusDto, str], response_model_exclude_none=True
)
@kitchen_recipe_router.post("", response_model=Union[RecipeResultWithStatusDto, str], response_model_exclude_none=True)
async def create_new_kitchen_recipe(
    kitchen_id: str,
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> RecipeResultWithStatusDto:
    recipe_result_with_status = await create_new_recipe_common(
        recipe,
        full_resource,
        nutrient_values,
        transient,
        indicators,
        credentials,
        kitchen_id=kitchen_id,
        skip_calc=skip_calc,
    )
    response.status_code = recipe_result_with_status.statuscode
    return recipe_result_with_status


@kitchen_recipe_router.put("/{recipe_id}", response_model_exclude_none=True)
async def put_kitchen_recipe(
    kitchen_id: str,
    recipe_id: str,
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> RecipeResultWithStatusDto:
    recipe_result_with_status = await put_recipe_common(
        recipe_id,
        recipe,
        full_resource,
        nutrient_values,
        transient,
        indicators,
        credentials,
        kitchen_id=kitchen_id,
        skip_calc=skip_calc,
    )
    response.status_code = recipe_result_with_status.statuscode
    return recipe_result_with_status


@kitchen_recipe_router.delete("/{recipe_id}", status_code=204, response_class=Response)
async def delete_kitchen_recipe(
    kitchen_id: str, recipe_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> None:
    await delete_recipe_common(recipe_id, response, credentials, kitchen_id=kitchen_id)
