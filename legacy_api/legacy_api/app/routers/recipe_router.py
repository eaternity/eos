from typing import Annotated, Optional, Union

from fastapi import APIRouter, Depends, Header, Query, Response, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.controller.recipe_controller import (
    create_new_recipe_common,
    delete_recipe_common,
    get_recipe_by_id_common,
    parse_recipe_result,
    put_recipe_common,
)
from legacy_api.converter.to_batch_input_dto import build_warning_message_for_batch_item, legacy_to_new_batch_converter
from legacy_api.dto.legacy.recipe import (
    BatchItemResultDto,
    BatchItemResultDtoWithRecipe,
    BatchRecipeDto,
    RecipeResultWithStatusDto,
    WrappedRecipeDto,
)

logger = get_logger()
settings = Settings()
recipe_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@recipe_router.get("/{recipe_id}", response_model=Union[RecipeResultWithStatusDto], response_model_exclude_none=True)
async def get_recipe_by_id(
    recipe_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> RecipeResultWithStatusDto:
    recipe_result_with_status = await get_recipe_by_id_common(
        recipe_id, full_resource, nutrient_values, indicators, credentials, skip_calc=skip_calc
    )
    response.status_code = recipe_result_with_status.statuscode
    return recipe_result_with_status


@recipe_router.post(
    "/", include_in_schema=False, response_model=Union[RecipeResultWithStatusDto], response_model_exclude_none=True
)
@recipe_router.post("", response_model=Union[RecipeResultWithStatusDto], response_model_exclude_none=True)
async def create_new_recipe(
    recipe: WrappedRecipeDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> RecipeResultWithStatusDto:
    recipe_result_with_status = await create_new_recipe_common(
        recipe, full_resource, nutrient_values, transient, indicators, credentials, skip_calc=skip_calc
    )
    response.status_code = recipe_result_with_status.statuscode
    return recipe_result_with_status


@recipe_router.put("/{recipe_id}", response_model=Union[RecipeResultWithStatusDto], response_model_exclude_none=True)
async def put_recipe(
    recipe_id: str,
    response: Response,
    recipe: WrappedRecipeDto,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> RecipeResultWithStatusDto:
    recipe_result_with_status = await put_recipe_common(
        recipe_id, recipe, full_resource, nutrient_values, transient, indicators, credentials, skip_calc=skip_calc
    )
    response.status_code = recipe_result_with_status.statuscode
    return recipe_result_with_status


@recipe_router.delete("/{recipe_id}", status_code=204, response_class=Response)
async def delete_recipe(
    recipe_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> None:
    await delete_recipe_common(recipe_id, response, credentials)


@recipe_router.post("/batch", response_model_exclude_none=True)
async def post_batch_recipes(
    batch: list[BatchRecipeDto],
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> list[BatchItemResultDtoWithRecipe | BatchItemResultDto]:
    json_batch, auxiliary_batch_info = legacy_to_new_batch_converter(
        [batch_item.model_dump() for batch_item in batch], "recipe", transient, skip_calc=skip_calc
    )
    response_batch = []
    for invalid_recipe_id, invalid_request_ids in auxiliary_batch_info.invalid_nodes.items():
        for invalid_request_id in invalid_request_ids:
            warning_message = build_warning_message_for_batch_item(
                invalid_recipe_id, invalid_request_id, auxiliary_batch_info, "recipe"
            )
            base_message = f"Recipe was NOT saved: The received recipe with id {invalid_recipe_id}"
            " references a missing subrecipe or its structure is cyclic."
            if warning_message:
                message = f"{base_message}\n{warning_message}"
            else:
                message = base_message

            response_batch.append(
                BatchItemResultDto(
                    statuscode=status.HTTP_400_BAD_REQUEST,
                    message=message,
                    request_id=invalid_request_id,
                    recipe_id=invalid_recipe_id,
                )
            )

    api_response = await api_client.post_and_forward_error(
        f"{settings.api_v2_url}/calculation/graphs?priority=1",
        headers={"Authorization": f"{credentials}"},
        json=json_batch,
    )
    try:
        result = await api_response.json()
    except Exception:
        result = None
        for valid_recipe_id, valid_request_ids in auxiliary_batch_info.valid_nodes.items():
            for valid_request_id in valid_request_ids:
                response_batch.append(
                    BatchItemResultDto(
                        statuscode=api_response.status,
                        request_id=valid_request_id,
                        recipe_id=valid_recipe_id,
                        message=build_warning_message_for_batch_item(
                            valid_recipe_id, valid_request_id, auxiliary_batch_info, "recipe"
                        ),
                    )
                )

    if result and result.get("batch"):
        for recipe_id, request_ids in auxiliary_batch_info.valid_nodes.items():
            try:
                batch_item_response = await parse_recipe_result(
                    api_response, full_resource, indicators, nutrient_values, recipe_id
                )
                if isinstance(batch_item_response, RecipeResultWithStatusDto):
                    for request_id in request_ids:
                        copy_batch_item_response = batch_item_response.model_copy(deep=True)
                        copy_batch_item_response.message += build_warning_message_for_batch_item(
                            recipe_id, request_id, auxiliary_batch_info, "recipe"
                        )
                        copy_batch_item_response.request_id = request_id
                        response_batch.append(copy_batch_item_response)
                elif isinstance(batch_item_response, str):
                    for request_id in request_ids:
                        response_batch.append(
                            BatchItemResultDto(
                                statuscode=batch_item_response.status_code,
                                request_id=request_id,
                                recipe_id=recipe_id,
                                message=build_warning_message_for_batch_item(
                                    recipe_id, request_id, auxiliary_batch_info, "recipe"
                                ),
                            )
                        )
            except Exception:
                for request_id in request_ids:
                    response_batch.append(
                        BatchItemResultDto(
                            statuscode=api_response.status,
                            request_id=request_id,
                            recipe_id=recipe_id,
                            message=build_warning_message_for_batch_item(
                                recipe_id, request_id, auxiliary_batch_info, "recipe"
                            ),
                        )
                    )

    # Sort the response according to request-id. Send the item to the last if request-id does not exist.
    # The reqeust-id should always exist if the rest of the code runs fine.
    result = sorted(response_batch, key=lambda x: getattr(x, "request_id", float("inf")))
    return result
