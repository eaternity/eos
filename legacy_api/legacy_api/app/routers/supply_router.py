import json
from typing import Annotated, Optional, Union

from fastapi import APIRouter, Depends, Header, Query, Response
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.controller.supply_controller import (
    create_new_supply_common,
    delete_supply_common,
    get_supply_by_id_common,
    parse_supply_put_and_post,
    put_supply_common,
)
from legacy_api.converter.to_batch_input_dto import build_warning_message_for_batch_item, legacy_to_new_batch_converter
from legacy_api.dto.legacy.supply import (
    BatchSupplyDto,
    GetSuppliesResultDto,
    PutOrPostSupplyResultDto,
    SupplyResultWithStatusDto,
    WrappedSupplyDto,
)

logger = get_logger()
settings = Settings()
supply_router = APIRouter()
kitchen_supply_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@supply_router.get("/{supply_id}", response_model_exclude_none=True)
async def get_supply_by_id(
    supply_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> SupplyResultWithStatusDto:
    supply_result_with_status = await get_supply_by_id_common(
        supply_id, full_resource, nutrient_values, indicators, credentials, skip_calc=skip_calc
    )
    response.status_code = supply_result_with_status.statuscode
    return supply_result_with_status


@kitchen_supply_router.get(
    "/{supply_id}", response_model=Union[SupplyResultWithStatusDto], response_model_exclude_none=True
)
async def get_kitchen_supply_by_id(
    kitchen_id: str,
    supply_id: str,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = True,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> SupplyResultWithStatusDto:
    supply_result_with_status = await get_supply_by_id_common(
        supply_id, full_resource, nutrient_values, indicators, credentials, kitchen_id=kitchen_id, skip_calc=skip_calc
    )
    response.status_code = supply_result_with_status.statuscode
    return supply_result_with_status


@kitchen_supply_router.get("/", include_in_schema=False)
@kitchen_supply_router.get("")
async def get_all_kitchen_supplies(
    kitchen_id: str, response: Response, credentials: APIKeyHeader = Depends(api_key_header)
) -> GetSuppliesResultDto:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/nodes/get_nodes?node_type=SupplySheetActivityNode",
        headers={"Authorization": f"{credentials}"},
        json=[{"access_group": {"xid": kitchen_id}}],
    )
    try:
        nodes = await api_response.json()
    except Exception:
        nodes = None

    if nodes is not None:
        return GetSuppliesResultDto(supplies=[node.get("xid", "") for node in nodes])
    else:
        response.status_code = api_response.status

        return json.dumps({"api_response": api_response.text})


@supply_router.post("/", include_in_schema=False, response_model_exclude_none=True)
@supply_router.post("", response_model_exclude_none=True)
async def create_new_supply(
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,  # noqa ARG001
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,  # noqa ARG001
    transient: bool = False,
    indicators: bool = False,  # noqa ARG001
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> PutOrPostSupplyResultDto:
    supply_result_with_status = await create_new_supply_common(supply, transient, credentials, skip_calc=skip_calc)
    response.status_code = supply_result_with_status.statuscode
    return supply_result_with_status


@kitchen_supply_router.post("/", include_in_schema=False, response_model_exclude_none=True)
@kitchen_supply_router.post("", response_model_exclude_none=True)
async def create_new_kitchen_supply(
    kitchen_id: str,
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,  # noqa ARG001
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,  # noqa ARG001
    transient: bool = False,
    indicators: bool = False,  # noqa ARG001
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> PutOrPostSupplyResultDto:
    supply_result_with_status = await create_new_supply_common(
        supply, transient, credentials, kitchen_id=kitchen_id, skip_calc=skip_calc
    )
    response.status_code = supply_result_with_status.statuscode
    return supply_result_with_status


@supply_router.put("/{supply_id}", response_model_exclude_none=True)
async def put_supply(
    supply_id: str,
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> PutOrPostSupplyResultDto | SupplyResultWithStatusDto:
    supply_result_with_status = await put_supply_common(
        supply_id, supply, full_resource, nutrient_values, transient, indicators, credentials, skip_calc=skip_calc
    )
    response.status_code = supply_result_with_status.statuscode
    return supply_result_with_status


@kitchen_supply_router.put("/{supply_id}", response_model_exclude_none=True)
async def put_kitchen_supply(
    kitchen_id: str,
    supply_id: str,
    supply: WrappedSupplyDto,
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,
    transient: bool = False,
    indicators: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> PutOrPostSupplyResultDto | SupplyResultWithStatusDto:
    supply_result_with_status = await put_supply_common(
        supply_id,
        supply,
        full_resource,
        nutrient_values,
        transient,
        indicators,
        credentials,
        kitchen_id=kitchen_id,
        skip_calc=skip_calc,
    )
    response.status_code = supply_result_with_status.statuscode
    return supply_result_with_status


@supply_router.delete("/{supply_id}", status_code=204, response_class=Response)
async def delete_supply(
    supply_id: str,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> None:
    await delete_supply_common(supply_id, response, credentials)


@kitchen_supply_router.delete("/{supply_id}", status_code=204, response_class=Response)
async def delete_kitchen_supply(
    kitchen_id: str,
    supply_id: str,
    response: Response,
    credentials: APIKeyHeader = Depends(api_key_header),
) -> None:
    await delete_supply_common(supply_id, response, credentials, kitchen_id=kitchen_id)


@supply_router.post("/batch", response_model_exclude_none=True)
async def post_batch_supplies(
    batch: list[BatchSupplyDto],
    response: Response,
    full_resource: Annotated[bool, Query(alias="full-resource")] = False,  # noqa ARG001
    nutrient_values: Annotated[bool, Query(alias="nutrient-values")] = False,  # noqa ARG001
    transient: bool = False,
    indicators: bool = False,  # noqa ARG001
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> list[PutOrPostSupplyResultDto]:
    json_batch, auxiliary_batch_info = legacy_to_new_batch_converter(
        [batch_item.model_dump() for batch_item in batch], "supply", transient, skip_calc=skip_calc
    )
    response_batch = []

    url = f"{settings.api_v2_url}/calculation/graphs?return_immediately=True&priority=0"

    api_response = await api_client.post_and_forward_error(
        url,
        headers={"Authorization": f"{credentials}"},
        json=json_batch,
    )
    try:
        result = await api_response.json()
    except Exception:
        result = None
        for valid_supply_id, valid_request_ids in auxiliary_batch_info.valid_nodes.items():
            for valid_request_id in valid_request_ids:
                response_batch.append(
                    PutOrPostSupplyResultDto(
                        statuscode=api_response.status,
                        request_id=valid_request_id,
                        supply_id=valid_supply_id,
                        message=build_warning_message_for_batch_item(
                            valid_supply_id, valid_request_id, auxiliary_batch_info, "supply"
                        ),
                    )
                )

    if result:
        for supply_id, request_ids in auxiliary_batch_info.valid_nodes.items():
            try:
                batch_item_response = parse_supply_put_and_post(api_response, supply_id, response)

                if isinstance(batch_item_response, PutOrPostSupplyResultDto):
                    for request_id in request_ids:
                        copy_batch_item_respone = batch_item_response.model_copy(deep=True)
                        copy_batch_item_respone.message += build_warning_message_for_batch_item(
                            supply_id, request_id, auxiliary_batch_info, "supply"
                        )
                        copy_batch_item_respone.request_id = request_id
                        response_batch.append(copy_batch_item_respone)
                elif isinstance(batch_item_response, str):
                    response_batch.append(
                        PutOrPostSupplyResultDto(
                            statuscode=batch_item_response.status_code,
                            request_id=request_id,
                            recipe_id=supply_id,
                            message=build_warning_message_for_batch_item(
                                supply_id, request_id, auxiliary_batch_info, "recipe"
                            ),
                        )
                    )

            except Exception:
                for request_id in request_ids:
                    response_batch.append(
                        PutOrPostSupplyResultDto(
                            statuscode=api_response.status,
                            request_id=request_id,
                            supply_id=supply_id,
                            message=build_warning_message_for_batch_item(
                                supply_id, request_id, auxiliary_batch_info, "supply"
                            ),
                        )
                    )

    # Sort the response according to request-id. Send the item to the last if request-id does not exist.
    # The request-id should always exist if the rest of the code runs fine.
    return sorted(response_batch, key=lambda x: getattr(x, "request_id", float("inf")))
