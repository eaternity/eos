import csv
import io
from typing import Annotated, Optional

from fastapi import APIRouter, Depends, Header, Query, Response
from fastapi.security import APIKeyHeader
from structlog import get_logger

from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings
from legacy_api.converter.final_graph_to_legacy_csv import (
    translate_final_graph_to_ingredient_csv,
    translate_final_graph_to_recipe_csv,
)

logger = get_logger()
settings = Settings()
csv_export_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@csv_export_router.get("/recipes", response_model_exclude_none=True)
async def get_recipes_export(
    kitchen_ids: Annotated[str, Query(alias="kitchen-ids")],
    production_date_from: Annotated[str, Query(alias="production-date-from")],
    production_date_to: Annotated[str, Query(alias="production-date-to")],
    locale: Optional[str] = None,  # TODO: what is this used for in jl?
    enabled_as_ingredients: Optional[bool] = Query(default=False),  # TODO: what is this used for in jl?
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> Response:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/calculation/reports/tabular-access-groups-data?start_date={production_date_from}&end_date={production_date_to}&return_full_calculation_results=true&node_type=FoodProcessingActivityNode",
        headers={"Authorization": f"{credentials}"},
        json=[{"access_group": {"xid": kitchen_id}} for kitchen_id in kitchen_ids.split(",")],
    )
    try:
        response = await api_response.json()
        final_graphs = response.get("final_graphs")
        all_new_rows = []
        row_index = 1
        for final_graph in final_graphs:
            row_index, new_rows = translate_final_graph_to_recipe_csv(final_graph, row_index)
            all_new_rows.extend(new_rows)
        output = io.StringIO()
        csv_writer = csv.DictWriter(output, fieldnames=list(all_new_rows[0].keys()), lineterminator="\n")
        csv_writer.writeheader()
        csv_writer.writerows(all_new_rows)
        return Response(content=output.getvalue(), media_type="text/csv")
    except Exception as e:
        return f"Error: {e}"


@csv_export_router.get("/recipes/with-ingredients", response_model_exclude_none=True)
async def get_recipes_with_ingredients_export(
    kitchen_ids: Annotated[str, Query(alias="kitchen-ids")],
    production_date_from: Annotated[str, Query(alias="production-date-from")],
    production_date_to: Annotated[str, Query(alias="production-date-to")],
    locale: Optional[str] = None,  # TODO: what is this used for in jl?
    enabled_as_ingredients: Optional[bool] = Query(default=False),  # TODO: what is this used for in jl?
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> Response:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/calculation/reports/tabular-access-groups-data?start_date={production_date_from}&end_date={production_date_to}&return_full_calculation_results=true&node_type=FoodProcessingActivityNode",
        headers={"Authorization": f"{credentials}"},
        json=[{"access_group": {"xid": kitchen_id}} for kitchen_id in kitchen_ids.split(",")],
    )
    try:
        response = await api_response.json()
        final_graphs = response.get("final_graphs")
        all_new_rows = []
        row_index = 1
        for final_graph in final_graphs:
            row_index, new_rows = translate_final_graph_to_ingredient_csv(final_graph, row_index)
            all_new_rows.extend(new_rows)
        output = io.StringIO()
        csv_writer = csv.DictWriter(output, fieldnames=list(all_new_rows[0].keys()), lineterminator="\n")
        csv_writer.writeheader()
        csv_writer.writerows(all_new_rows)
        return Response(content=output.getvalue(), media_type="text/csv")
    except Exception as e:
        return f"Error: {e}"


@csv_export_router.get("/supplies", response_model_exclude_none=True)
async def get_supplies_export(
    kitchen_ids: Annotated[str, Query(alias="kitchen-ids")],
    production_date_from: Annotated[str, Query(alias="production-date-from")],
    production_date_to: Annotated[str, Query(alias="production-date-to")],
    locale: Optional[str] = None,  # TODO: what is this used for in jl?
    enabled_as_ingredients: Optional[bool] = Query(default=False),  # TODO: what is this used for in jl?
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> Response:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/calculation/reports/tabular-access-groups-data?start_date={production_date_from}&end_date={production_date_to}&return_full_calculation_results=true&node_type=SupplySheetActivityNode",
        headers={"Authorization": f"{credentials}"},
        json=[{"access_group": {"xid": kitchen_id}} for kitchen_id in kitchen_ids.split(",")],
    )
    try:
        response = await api_response.json()
        final_graphs = response.get("final_graphs")
        all_new_rows = []
        row_index = 1
        for final_graph in final_graphs:
            row_index, new_rows = translate_final_graph_to_ingredient_csv(final_graph, row_index)
            all_new_rows.extend(new_rows)
        output = io.StringIO()
        csv_writer = csv.DictWriter(output, fieldnames=list(all_new_rows[0].keys()), lineterminator="\n")
        csv_writer.writeheader()
        csv_writer.writerows(all_new_rows)
        return Response(content=output.getvalue(), media_type="text/csv")
    except Exception as e:
        return f"Error: {e}"


@csv_export_router.get("/recipes/{recipe_id}", response_model_exclude_none=True)
async def get_single_recipe_export(
    recipe_id: str,
    credentials: APIKeyHeader = Depends(api_key_header),
    skip_calc: Optional[bool] = Header(default=None, convert_underscores=False),
) -> Response:
    api_response = await api_client.post(
        f"{settings.api_v2_url}/calculation/graphs?priority=1",
        headers={"Authorization": f"{credentials}"},
        json={
            "batch": [
                {
                    "input_root": {"existing_root": {"xid": recipe_id}},
                    "return_data_as_table": True,
                    "requested_quantity_references": ["amount_for_root_node"],
                    "final_graph_table_criterion": {
                        "keep_customer_send_data": {
                            "depth": 2,
                        }
                    },
                }
            ]
        },
    )
    try:
        response = await api_response.json()
        final_graph = response.get("batch")[0].get("final_graph_table")
        recipe_row_index = 1
        _, new_recipe_rows = translate_final_graph_to_recipe_csv(final_graph, recipe_row_index)

        ingredient_row_index = 1
        _, new_ingredient_rows = translate_final_graph_to_ingredient_csv(final_graph, ingredient_row_index)

        output = io.StringIO()
        csv_writer = csv.DictWriter(output, fieldnames=list(new_recipe_rows[0].keys()), lineterminator="\n")
        csv_writer.writeheader()
        csv_writer.writerows(new_recipe_rows)

        # Add 4 blank rows
        output.write("\n" * 4)

        # Add "Ingredients:" row
        output.write("Ingredients:\n")

        # Write ingredient CSV
        ingredient_csv_writer = csv.DictWriter(
            output, fieldnames=list(new_ingredient_rows[0].keys()), lineterminator="\n"
        )
        ingredient_csv_writer.writeheader()
        ingredient_csv_writer.writerows(new_ingredient_rows)

        return Response(content=output.getvalue(), media_type="text/csv")
    except Exception as e:
        return f"Error: {e}"
