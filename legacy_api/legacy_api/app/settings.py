import base64

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    TITLE: str = "ayce"

    JSON_LOGGING: bool = False
    LOG_LEVEL: str = "DEBUG"
    EATERNITY_TOKEN: str = "EATERNITY_TOKEN"  # this is NOT base64 encoded
    API_V2_HOSTNAME: str = "localhost"
    API_V2_PORT: str = "8040"
    API_V2_BASE_PATH: str = "/v2"
    BASE_PATH: str = "/api"

    @property
    def EATERNITY_AUTH_KEY(self) -> str:
        basic_auth_key = base64.b64encode(f"{self.EATERNITY_TOKEN}:".encode("utf-8"))
        return basic_auth_key.decode("utf-8")

    @property
    def api_v2_url(self) -> str:
        return f"http://{self.API_V2_HOSTNAME}:{self.API_V2_PORT}{self.API_V2_BASE_PATH}"

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8", extra="allow")
