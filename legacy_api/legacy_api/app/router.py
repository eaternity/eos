from fastapi import APIRouter, Request
from fastapi.responses import JSONResponse
from structlog import get_logger

from legacy_api.app.routers.csv_export_router import csv_export_router
from legacy_api.app.routers.kitchen_recipe_router import kitchen_recipe_router
from legacy_api.app.routers.kitchen_router import kitchen_router
from legacy_api.app.routers.product_router import product_router
from legacy_api.app.routers.prognosis_router import prognosis_router
from legacy_api.app.routers.recipe_router import recipe_router
from legacy_api.app.routers.supply_router import kitchen_supply_router, supply_router
from legacy_api.app.server import api_client
from legacy_api.app.settings import Settings

settings = Settings()
logger = get_logger()

# Create a base router without prefix for handling both /api and /v2 paths
base_router = APIRouter(prefix=settings.BASE_PATH.replace("/api", ""))
api_router = APIRouter(prefix="/api")

# Add all the regular routes to the api_router
api_router.include_router(kitchen_router, prefix="/kitchens")
api_router.include_router(kitchen_recipe_router, prefix="/kitchens/{kitchen_id}/recipes")
api_router.include_router(recipe_router, prefix="/recipes")
api_router.include_router(kitchen_supply_router, prefix="/kitchens/{kitchen_id}/supplies")
api_router.include_router(supply_router, prefix="/supplies")
api_router.include_router(product_router, prefix="/products")
api_router.include_router(csv_export_router, prefix="/csv-export")
api_router.include_router(prognosis_router, prefix="/prognosis")

# Include the api_router in the base_router
base_router.include_router(api_router)


# Create a catch-all route for v2 paths
@base_router.api_route("/v2/{path:path}", methods=["GET", "POST", "PUT", "DELETE", "PATCH"])
async def forward_to_v2(request: Request, path: str) -> JSONResponse:
    try:
        forward_to_url = f"{settings.api_v2_url}/{path}"
        headers = {k: v for k, v in request.headers.items() if k.lower() != "host"}
        body = await request.body()
        response = await api_client.session.request(
            method=request.method,
            url=forward_to_url,
            headers=headers,
            params=request.query_params,
            data=body if body else None,
        )
        content = await response.json() if response.content_length else None
        return JSONResponse(content=content, status_code=response.status, headers=dict(response.headers))
    except Exception as e:
        logger.error("Error forwarding request to v2", error=str(e))
        return JSONResponse(content={"detail": "Error forwarding request to v2 service"}, status_code=500)


# Export the base_router as the main router
router = base_router
