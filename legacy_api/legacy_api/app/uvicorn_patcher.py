"""Monkey patch uvicorn."""


def monkey_patch_uvicorn_status_line() -> None:
    """Monkey patch used for extending the status codes into 6xx."""
    # Monkey patch STATUS_LINE to allow for 6xx status codes.
    import uvicorn.protocols.http.httptools_impl as httptools_impl

    httptools_impl.STATUS_LINE = {
        status_code: httptools_impl._get_status_line(status_code) for status_code in range(100, 700)
    }
