"Minimalistic REST API client."
import typing

import aiohttp
from aiohttp.typedefs import LooseHeaders, StrOrURL
from fastapi import HTTPException
from structlog import get_logger
from structlog.contextvars import get_contextvars

logger = get_logger()


class ApiClient:
    "Wrapper to provide a REST API client."

    def __init__(self):
        self.session = None

    async def connect(self) -> None:
        "Start a new client session."
        # force_close = disable keep-alive, limit = max connections
        # conn = aiohttp.TCPConnector(force_close=True, limit=100)
        conn = aiohttp.TCPConnector(keepalive_timeout=0, limit=100, force_close=False)
        timeout = aiohttp.ClientTimeout(total=60 * 60)
        self.session = aiohttp.ClientSession(connector=conn, timeout=timeout)

    async def close(self) -> None:
        "Close the connection."
        await self.session.close()

    async def post_and_forward_error(
        self,
        url: StrOrURL,
        headers: typing.Optional[LooseHeaders] = None,
        json: typing.Optional[typing.Any] = None,  # noqa: ANN401
    ) -> aiohttp.ClientResponse:
        api_response = await self.post(url, headers=headers, json=json)
        if api_response.status >= 400:
            content = await api_response.content.read()
            raise HTTPException(status_code=api_response.status, detail=content)
        return api_response

    async def post(
        self,
        url: StrOrURL,
        headers: typing.Optional[LooseHeaders] = None,
        json: typing.Optional[typing.Any] = None,  # noqa: ANN401
        auth: typing.Tuple[str, str] = None,
    ) -> aiohttp.ClientResponse:
        context = get_contextvars()
        request_id = context.get("request_id")
        if request_id:
            headers["x-cloud-trace-context"] = request_id
        if auth:
            auth = aiohttp.BasicAuth(*auth)
        return await self.session.post(url, headers=headers, json=json, auth=auth)
