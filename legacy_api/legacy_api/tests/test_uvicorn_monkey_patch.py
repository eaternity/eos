import asyncio
from multiprocessing import Process

import pytest
import uvicorn
from fastapi import FastAPI, Response
from httpx import AsyncClient

from legacy_api.app.uvicorn_patcher import monkey_patch_uvicorn_status_line


def create_app() -> FastAPI:
    app = FastAPI(title="dummy title")

    @app.get("/test")
    def read_test(response: Response) -> dict:
        response.status_code = 678
        return {"message": "Custom status code"}

    return app


def run_server() -> None:
    monkey_patch_uvicorn_status_line()
    uvicorn.run(create_app(), host="0.0.0.0", port=8060, log_level="debug")


@pytest.mark.asyncio
async def test_6xx_status_code() -> None:
    server_process = Process(target=run_server, daemon=True)
    server_process.start()

    # Wait for the server to start up
    await asyncio.sleep(2)  # You may adjust the sleep duration if needed
    try:
        async with AsyncClient() as ac:
            response = await ac.get("http://localhost:8060/test")
            assert response.status_code == 678
    finally:
        # Terminate the server process
        server_process.terminate()
        server_process.join()
