import copy
import os
from typing import Any, Optional

import pytest
from httpx import AsyncClient
from structlog import get_logger

from api.tests.test_api_for_recipes import (
    TEST_RECIPE_EXPECTED_CO2,
    TEST_RECIPE_EXPECTED_DFU,
    TEST_RECIPE_EXPECTED_DFU_ING1,
    TEST_RECIPE_EXPECTED_DFU_ING2,
)
from api.tests.test_api_for_supplies import (
    TEST_SUPPLY_EXPECTED_CO2,
    TEST_SUPPLY_EXPECTED_CO2_ING1,
    TEST_SUPPLY_EXPECTED_CO2_ING2,
    TEST_SUPPLY_EXPECTED_DFU,
    TEST_SUPPLY_EXPECTED_DFU_ING1,
    TEST_SUPPLY_EXPECTED_DFU_ING2,
)
from core.domain.matching_item import MatchingItem
from core.service.service_provider import ServiceLocator

from .conftest import TEST_KITCHEN_ADMIN_ID, TEST_KITCHEN_SUPERUSER_ID, AppFixtureReturn, check_response_dict

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()

TEST_LEGACY_SUPPLY_EXPECTED_CO2 = round(TEST_RECIPE_EXPECTED_CO2 * 1000)
TEST_LEGACY_SUPPLY_EXPECTED_DFU = TEST_RECIPE_EXPECTED_DFU
TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1 = TEST_RECIPE_EXPECTED_DFU_ING1
TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2 = TEST_RECIPE_EXPECTED_DFU_ING2
TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE = 56
TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE = 247
TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1 = 100
TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2 = 44
TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1 = 92
TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2 = 155


TEST_SUPPLY_ADMIN_ID = "test_supply_admin_legacy"
TEST_SUPPLY_SUPERUSER_ID = "test_supply_superuser_legacy"
TEST_SUPPLY = {
    "supply": {
        "supplier": "Vegetable Supplies Ltd.",
        "supplier-id": "el3i5y-2in5y-2hbllll01",
        "invoice-id": "778888800000001",
        "supply-date": "2023-06-20",
        "ingredients": [
            {
                "id": "100100191",
                "gtin": "4001724810001",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Tomaten",
                    },
                ],
                "amount": 150,
                "unit": "gram",
                "origin": "spain",
                "transport": "air",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
                # update 5.4.: uncommented ingredients-declaration because this field is no longer ignored
                # but now leads to failures in this example (missing matching)
                # "ingredients-declaration": "Hähnchenbrustfilet (53 %), Panade (28%) "
                # "(Weizenmehl, Wasser, modifizierte Weizen-stärke, Weizenstärke, Speisesalz, Gewürze, Hefe), "
                # "Wasser, Putenformfleisch-schinken aus Putenfleischteilen zusammengefügt (7 %) (Putenkeulenfleisch, "
                # "Nitritpökelsalz (Kochsalz, Konservierungsstoff: E 250), Maltodextrin, Dextrose, Gewürz"
                # "extrakte, Stabilisator: E450), Käse (7 %), Kartoffelstärke, Stabilisator: modif"
                # "izierte Stärke, Salz, Speisesalz, Stärke, Maltodextrin, Milcheiweiß, Pflanzene"
                # "iweiß, Dextrose, Zucker, Gewürzextrakte, Geschmacksverstärker: E 620",
                "nutrient-values": {
                    "energy-kjoule": 89,
                    "fat-gram": 0.3,
                    "protein-gram": 0.8,
                    "vitamine-k-microgram": 0.21,
                    "vitamine-a1-microgram": 0.12345,
                    "manganese-microgram": 0,
                    "fibers-gram": 1.2,
                    "water-gram": 93.8,
                },
            },
            {
                "id": "100199894",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Zwiebeln",
                    },
                ],
                "amount": 78,
                "unit": "gram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",
                "packaging": "",
            },
        ],
    }
}

TEST_SUPPLY_UNMATCHED = copy.deepcopy(TEST_SUPPLY)
for supply_ingredient in TEST_SUPPLY_UNMATCHED["supply"]["ingredients"]:
    supply_ingredient["names"][0]["value"] = f"unmatched_{supply_ingredient['names'][0]['value']}"


def adapt_expected_response(expected_response: dict, dried_onion_nutrients: Optional[dict] = None) -> None:
    for ingredient in expected_response["supply"]["ingredients"]:
        if "ingredients-declaration" in ingredient:
            del ingredient["ingredients-declaration"]
        if "nutrient-values" in ingredient:
            new_nutr_values_dict = {}
            for nutr, val in ingredient["nutrient-values"].items():
                val /= 100
                val *= ingredient["amount"]
                if nutr == "energy-kjoule":
                    val /= 4.184
                    new_nutr_values_dict["energy-kcal"] = val
                else:
                    new_nutr_values_dict[nutr] = val
            ingredient["nutrient-values"] = new_nutr_values_dict
        elif dried_onion_nutrients is not None:
            ingredient["nutrient-values"] = {
                key: val * ingredient["amount"] / 100 for key, val in dried_onion_nutrients.items()
            }


def expected_response_without_nutrients(expected_response: dict) -> dict:
    expected_response_without_nutrients_dict = copy.deepcopy(expected_response)
    for index in range(2):
        if "nutrient-values" in expected_response_without_nutrients_dict["supply"]["ingredients"][index]:
            del expected_response_without_nutrients_dict["supply"]["ingredients"][index]["nutrient-values"]
    return expected_response_without_nutrients_dict


@pytest.mark.asyncio
async def test_create_delete_and_get_supply_by_superuser(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    load_tomato_and_onion_nutrients: dict[str, dict[str, float]],
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # trying POST request
        response = await ac.post(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )

        assert response.status_code == 200
        assert response.json().get("supply-id") is not None
        temp_supply_id = response.json().get("supply-id")

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{temp_supply_id}?full-resource=true&nutrient-values=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200

        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY)
        expected_response["supply"]["id"] = temp_supply_id
        expected_response["supply"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = temp_supply_id

        adapt_expected_response(expected_response, load_tomato_and_onion_nutrients["dried_onion"])

        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{temp_supply_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{temp_supply_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{TEST_SUPPLY_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200
        assert response.json()["statuscode"] == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{TEST_SUPPLY_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        expected_response["supply"]["id"] = TEST_SUPPLY_SUPERUSER_ID
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = TEST_SUPPLY_SUPERUSER_ID

        check_response_dict(
            expected_response_without_nutrients(expected_response),
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        # check if supplies listing works correctly
        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"supplies": [TEST_SUPPLY_SUPERUSER_ID]}

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{TEST_SUPPLY_SUPERUSER_ID}?transient=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/"
            f"{TEST_SUPPLY_SUPERUSER_ID}?transient=true&full-resource=true&nutrient-values=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = TEST_SUPPLY_SUPERUSER_ID
        expected_response["request-id"] = 0
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/{TEST_SUPPLY_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404


@pytest.mark.asyncio
async def test_create_delete_and_get_supply_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    load_tomato_and_onion_nutrients: dict[str, dict[str, float]],
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/"
            f"{TEST_SUPPLY_ADMIN_ID}?full-resource=true&nutrient-values=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200

        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY)
        expected_response["supply"]["id"] = TEST_SUPPLY_ADMIN_ID
        expected_response["supply"]["kitchen-id"] = TEST_KITCHEN_ADMIN_ID
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = TEST_SUPPLY_ADMIN_ID

        adapt_expected_response(expected_response, load_tomato_and_onion_nutrients["dried_onion"])

        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_SUPPLY,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/supplies/{TEST_SUPPLY_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = TEST_SUPPLY_ADMIN_ID
        assert response.status_code == 200
        check_response_dict(
            expected_response_without_nutrients(expected_response),
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )


@pytest.mark.asyncio
async def test_supplies_endpoint(
    app_fixture: AppFixtureReturn,
    create_simple_customer: tuple[str, str, str],
    load_tomato_and_onion_nutrients: dict[str, dict[str, float]],
) -> None:
    _, customer_token, _ = create_simple_customer
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY)

        for ingredient in expected_response["supply"]["ingredients"]:
            if "ingredients-declaration" in ingredient:
                del ingredient["ingredients-declaration"]

        response = await ac.post("/api/supplies/", auth=(customer_token, ""), json=TEST_SUPPLY)
        assert response.status_code == 200
        assert response.json()["statuscode"] == 200
        assigned_supply_id = response.json()["supply-id"]

        response = await ac.get(f"/api/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, ""))
        expected_response["supply"]["id"] = assigned_supply_id
        expected_response["supply"]["kitchen-id"] = response.json()["supply"]["kitchen-id"]
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = assigned_supply_id

        assert response.status_code == 200
        check_response_dict(
            expected_response_without_nutrients(expected_response),
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.delete(f"/api/supplies/{assigned_supply_id}", auth=(customer_token, ""))
        assert response.status_code == 204

        response = await ac.get(f"/api/{assigned_supply_id}", auth=(customer_token, ""))
        assert response.status_code == 404

        response = await ac.put(
            f"/api/supplies/{assigned_supply_id}?full-resource=true&nutrient-values=true",
            auth=(customer_token, ""),
            json=TEST_SUPPLY,
        )

        adapt_expected_response(expected_response, load_tomato_and_onion_nutrients["dried_onion"])
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = assigned_supply_id
        expected_response["request-id"] = 0

        assert response.status_code == 200
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.delete(f"/api/supplies/{assigned_supply_id}", auth=(customer_token, ""))
        assert response.status_code == 204

        response = await ac.post(
            "/api/supplies/?full-resource=true&nutrient-values=true", auth=(customer_token, ""), json=TEST_SUPPLY
        )
        assert response.status_code == 200
        assigned_supply_id = response.json()["supply-id"]
        assert response.json() == {
            "statuscode": 200,
            "message": "",
            "supply-id": assigned_supply_id,
            "request-id": 0,
        }

        response = await ac.get(f"/api/supplies/{assigned_supply_id}?full-resource=false", auth=(customer_token, ""))
        expected_response_without_ingredients = copy.deepcopy(expected_response)
        expected_response_without_ingredients["supply"]["ingredients"] = []
        expected_response_without_ingredients["supply"]["id"] = assigned_supply_id
        expected_response_without_ingredients["statuscode"] = 200
        expected_response_without_ingredients["message"] = ""
        expected_response_without_ingredients["supply-id"] = assigned_supply_id
        # we don't have request-ids for get-requests
        del expected_response_without_ingredients["request-id"]

        assert response.status_code == 200
        check_response_dict(
            expected_response_without_ingredients,
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
        )

        response = await ac.get(f"/api/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, ""))
        expected_response["message"] = ""
        expected_response["statuscode"] = 200
        expected_response["supply-id"] = assigned_supply_id
        expected_response["supply"]["id"] = assigned_supply_id
        del expected_response["request-id"]
        assert response.status_code == 200
        check_response_dict(
            expected_response_without_nutrients(expected_response),
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )


@pytest.mark.asyncio
async def test_supplies_batch(
    app_fixture: AppFixtureReturn,
    create_simple_customer_and_kitchen: tuple[str, str, str],
    load_tomato_and_onion_nutrients: dict[str, dict[str, float]],
) -> None:
    _ = app_fixture
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen

    test_supply_with_kitchen_id = copy.deepcopy(TEST_SUPPLY)
    test_supply_with_kitchen_id["supply"]["kitchen-id"] = kitchen_xid

    test_supply_with_double_onion = copy.deepcopy(test_supply_with_kitchen_id)
    test_supply_with_double_onion["supply"]["ingredients"][1]["amount"] *= 2

    test_supply_id = "test-supply-id"
    test_supply_with_kitchen_id["supply"]["id"] = test_supply_id

    test_batch = [
        {"request-id": 0, **test_supply_with_kitchen_id},
        {"request-id": 1, **test_supply_with_double_onion},
        {"request-id": 1, **test_supply_with_double_onion},
        {"request-id": 2, **test_supply_with_kitchen_id},
    ]
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post("/api/supplies/batch", auth=(customer_token, ""), json=test_batch)

    assert response.status_code == 200
    assert len(response.json()) == 3
    for index in range(3):
        assert response.json()[index]["request-id"] == index
        supply_id = response.json()[index]["supply-id"]
        if index in (0, 2):
            expected_response: dict[str, Any] = copy.deepcopy(test_supply_with_kitchen_id)
            expected_response["supply"]["ingredients"] = []
            assert supply_id == test_supply_id
            assert response.json()[index]["message"] == (
                "Supply ID 'test-supply-id' in multiple batch items. " "Using the item appearing last in the list."
            )
            expected_co2 = TEST_LEGACY_SUPPLY_EXPECTED_CO2
            expected_dfu = TEST_LEGACY_SUPPLY_EXPECTED_DFU
            expected_co2_improvement_percentage = TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE
            expected_co2_reduction_value = TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE
        else:
            expected_response: dict[str, Any] = copy.deepcopy(test_supply_with_double_onion)
            expected_response["supply"]["ingredients"] = []
            expected_response["supply"]["id"] = supply_id

            assert response.json()[index]["message"] == (
                "Request ID '1' in multiple batch items. Using the item appearing last in the list."
            )
            expected_co2 = round(TEST_RECIPE_EXPECTED_CO2 * 2 * 1000)
            expected_dfu = TEST_RECIPE_EXPECTED_DFU_ING1 + TEST_RECIPE_EXPECTED_DFU_ING2 * 2
            expected_co2_improvement_percentage = 51
            expected_co2_reduction_value = 402

        for ingredient in expected_response["supply"]["ingredients"]:
            if "ingredients-declaration" in ingredient:
                del ingredient["ingredients-declaration"]

        async with AsyncClient(
            app=app_fixture,
            base_url="http://localhost:8050",
        ) as ac:
            get_response = await ac.get(
                f"/api/kitchens/{kitchen_xid}/supplies/{supply_id}?full-resource=false", auth=(customer_token, "")
            )

        assert get_response.status_code == 200
        expected_response["message"] = ""
        expected_response["statuscode"] = 200
        expected_response["supply-id"] = supply_id
        check_response_dict(
            expected_response,
            get_response.json(),
            "supply",
            expected_co2_value=expected_co2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=expected_co2_improvement_percentage,
            expected_co2_reduction_value=expected_co2_reduction_value,
            expected_daily_food_unit=expected_dfu,
        )

        if index != 0:
            async with AsyncClient(
                app=app_fixture,
                base_url="http://localhost:8050",
            ) as ac:
                delete_response = await ac.delete(
                    f"/api/kitchens/{kitchen_xid}/supplies/{supply_id}", auth=(customer_token, "")
                )

            assert delete_response.status_code == 204

    # With nutrient-values=true. Full-ressource=true does not have an influence for this endpoint to be compatible with
    # the javaland implementation
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/supplies/batch?full-resource=true&nutrient-values=true", auth=(customer_token, ""), json=test_batch
        )
        for index in range(3):
            assert response.json()[index]["request-id"] == index
            supply_id = response.json()[index]["supply-id"]
            if index in (0, 2):
                assert supply_id == test_supply_id
                assert response.json()[index] == {
                    "statuscode": 200,
                    "message": (
                        "Supply ID 'test-supply-id' in multiple batch items. "
                        "Using the item appearing last in the list."
                    ),
                    "supply-id": test_supply_id,
                    "request-id": index,
                }
                expected_response_get: dict[str, Any] = copy.deepcopy(test_supply_with_kitchen_id)

                expected_co2 = TEST_LEGACY_SUPPLY_EXPECTED_CO2
                expected_dfu = TEST_LEGACY_SUPPLY_EXPECTED_DFU
                expected_dfu_ing1 = TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1
                expected_dfu_ing2 = TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2
                expected_co2_improvement_percentage = TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE
                expected_co2_reduction_value = TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE
                expected_co2_improvement_percentage_ing_1 = TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1
                expected_co2_improvement_percentage_ing_2 = TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2
                expected_co2_reduction_value_ing_1 = TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1
                expected_co2_reduction_value_ing_2 = TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2
            else:
                assert response.json()[index] == {
                    "statuscode": 200,
                    "message": "Request ID '1' in multiple batch items. Using the item appearing last in the list.",
                    "supply-id": supply_id,
                    "request-id": index,
                }
                expected_response_get: dict[str, Any] = copy.deepcopy(test_supply_with_double_onion)

                expected_co2 = round(TEST_RECIPE_EXPECTED_CO2 * 2 * 1000)
                expected_dfu = TEST_RECIPE_EXPECTED_DFU_ING1 + TEST_RECIPE_EXPECTED_DFU_ING2 * 2
                expected_dfu_ing1 = TEST_RECIPE_EXPECTED_DFU_ING1
                expected_dfu_ing2 = TEST_RECIPE_EXPECTED_DFU_ING2 * 2

                expected_co2_improvement_percentage = 51
                expected_co2_reduction_value = 402
                expected_co2_improvement_percentage_ing_1 = TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1
                expected_co2_improvement_percentage_ing_2 = TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2
                expected_co2_reduction_value_ing_1 = TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1
                expected_co2_reduction_value_ing_2 = TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2 * 2

            for ingredient in expected_response_get["supply"]["ingredients"]:
                if "ingredients-declaration" in ingredient:
                    del ingredient["ingredients-declaration"]

            expected_response_get["message"] = ""
            expected_response_get["supply-id"] = supply_id
            expected_response_get["statuscode"] = 200
            expected_response_get["supply"]["id"] = supply_id
            adapt_expected_response(expected_response_get, load_tomato_and_onion_nutrients["dried_onion"])

            response_get = await ac.get(
                f"/api/supplies/{supply_id}?full-resource=true&nutrient-values=true", auth=(customer_token, "")
            )

            check_response_dict(
                expected_response_get,
                response_get.json(),
                "supply",
                expected_co2_value=expected_co2,
                expected_co2_rating="A",
                expect_eaternity_award_field=None,
                expected_co2_improvement_percentage=expected_co2_improvement_percentage,
                expected_co2_reduction_value=expected_co2_reduction_value,
                expected_daily_food_unit=expected_dfu,
                expected_ingredients_co2_value=[0, expected_co2],
                expected_ingredients_co2_rating=["A", "B"],
                expected_ingredients_bar_chart=[0, 100],
                expected_ingredients_food_unit=[expected_dfu_ing1, expected_dfu_ing2],
                expected_ingredients_co2_improvement_percentage=[
                    expected_co2_improvement_percentage_ing_1,
                    expected_co2_improvement_percentage_ing_2,
                ],
                expected_ingredients_co2_reduction_value=[
                    expected_co2_reduction_value_ing_1,
                    expected_co2_reduction_value_ing_2,
                ],
            )


@pytest.mark.asyncio
async def test_supplies_unmatched(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_namespace_uid, kitchen_xid = create_simple_customer_and_kitchen
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY_UNMATCHED)

        for ingredient in expected_response["supply"]["ingredients"]:
            if "ingredients-declaration" in ingredient:
                del ingredient["ingredients-declaration"]

        response = await ac.post(
            f"/api/kitchens/{kitchen_xid}/supplies/", auth=(customer_token, ""), json=TEST_SUPPLY_UNMATCHED
        )
        assert response.status_code == 200
        assert response.json()["statuscode"] == 200
        assigned_supply_id = response.json()["supply-id"]

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, "")
        )
        expected_response["supply"]["id"] = assigned_supply_id
        expected_response["supply"]["kitchen-id"] = kitchen_xid

        expected_response["message"] = (
            "At least one ingredient name could not get automatically matched into "
            "the Eaternity Database. "
            "Retry once a day or try with a different ingredient name. "
            "Names that couldn't be matched: [['unmatched_Tomaten'], ['unmatched_Zwiebeln']]."
        )
        expected_response["statuscode"] = 602
        expected_response["supply-id"] = assigned_supply_id

        assert response.status_code == 602
        assert "co2-value" not in response.json()["supply"], "CO2 values should be none as no matching exists."

        check_response_dict(
            expected_response_without_nutrients(expected_response),
            response.json(),
            "supply",
        )

        service_provider = ServiceLocator().service_provider

        access_group = await service_provider.access_group_service.get_access_group_by_xid(
            kitchen_xid, customer_namespace_uid
        )

        foodex2_access_group_uid = await service_provider.namespace_service.find_default_access_group_by_ns_xid(
            "foodex2"
        )
        tomato_term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "A0DMX", str(foodex2_access_group_uid)
        )
        onion_term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            "A1480", str(foodex2_access_group_uid)
        )
        ingredient_terms = [tomato_term, onion_term]

        for index in range(2):
            matching_string = TEST_SUPPLY_UNMATCHED["supply"]["ingredients"][index]["names"][0]["value"]
            language = TEST_SUPPLY_UNMATCHED["supply"]["ingredients"][index]["names"][0]["language"]
            matching_term_uids = [str(ingredient_terms[index].uid)]
            await service_provider.matching_service.put_matching_item(
                MatchingItem(
                    gap_filling_module="MatchProductName",
                    access_group_uid=str(access_group.uid),
                    lang=language,
                    matching_string=matching_string,
                    term_uids=matching_term_uids,
                )
            )

        # The second get after matching should correctly return the CO2 values.
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, "")
        )
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = assigned_supply_id

        assert response.status_code == 200
        check_response_dict(
            expected_response_without_nutrients(expected_response),
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )


@pytest.mark.parametrize(
    "non_food_xid",
    [
        "EAT-0002",
        "EOS_data_error",
    ],
)
@pytest.mark.asyncio
async def test_supplies_matched_to_data_error_and_non_food(
    app_fixture: AppFixtureReturn,
    create_simple_customer_and_kitchen: tuple[str, str, str],
    load_tomato_and_onion_nutrients: dict[str, dict[str, float]],
    non_food_xid: str,
) -> None:
    customer_token, customer_namespace_uid, kitchen_xid = create_simple_customer_and_kitchen
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # we match the first ingredient to a non-food term
        test_supply_unmatched = copy.deepcopy(TEST_SUPPLY)
        supply_ingredient = test_supply_unmatched["supply"]["ingredients"][0]
        supply_ingredient["names"][0]["value"] = f"{supply_ingredient['names'][0]['value']}_matched_to_non_food"

        # remove nutrient-values and ingredients-declaration
        for ingredient in test_supply_unmatched["supply"]["ingredients"]:
            if "ingredients-declaration" in ingredient:
                del ingredient["ingredients-declaration"]
            if "nutrient-values" in ingredient:
                del ingredient["nutrient-values"]

        # match to non-food term
        service_provider = ServiceLocator().service_provider
        access_group = await service_provider.access_group_service.get_access_group_by_xid(
            kitchen_xid, customer_namespace_uid
        )
        non_food_terms = await service_provider.glossary_service.get_terms_by_xid(non_food_xid)
        matching_string = test_supply_unmatched["supply"]["ingredients"][0]["names"][0]["value"]
        language = test_supply_unmatched["supply"]["ingredients"][0]["names"][0]["language"]
        matching_term_uids = [str(non_food_terms[0].uid)]
        await service_provider.matching_service.put_matching_item(
            MatchingItem(
                gap_filling_module="MatchProductName",
                access_group_uid=str(access_group.uid),
                lang=language,
                matching_string=matching_string,
                term_uids=matching_term_uids,
            )
        )

        # check if that the response is as expected
        expected_response: dict[str, Any] = copy.deepcopy(test_supply_unmatched)

        response = await ac.post(
            f"/api/kitchens/{kitchen_xid}/supplies/", auth=(customer_token, ""), json=test_supply_unmatched
        )

        assigned_supply_id = response.json()["supply-id"]

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, "")
        )
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = assigned_supply_id
        expected_response["supply"]["id"] = assigned_supply_id
        expected_response["supply"]["kitchen-id"] = kitchen_xid

        assert response.status_code == 200

        # TODO: what do we want to have as result for food-unit etc. for non-food ingredients?
        check_response_dict(
            expected_response_without_nutrients(expected_response),
            response.json(),
            "supply",
            expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,  # does not change because the tomato previously also
            # didn't contribute
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=64,  # changes because of the changed food-unit
            expected_co2_reduction_value=350,  # changes because of the changed food-unit
            expected_daily_food_unit=0.14,  # food-unit of the entire supply-sheet changes because of the new matching
            expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
            expected_ingredients_co2_rating=["E", "B"],  # "E" is the default value
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[None, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
            expected_ingredients_co2_improvement_percentage=[
                None,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                None,
                TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        # now do the same but also make sure that nutrient-subdivision-gfm and ingredient-amount-estimation are executed
        # by adding nutrient-values and ingredients-declaration to the second ingredient
        test_supply_unmatched_amount_estimation = copy.deepcopy(test_supply_unmatched)
        test_supply_unmatched_amount_estimation["supply"]["ingredients"][1][
            "nutrient-values"
        ] = load_tomato_and_onion_nutrients["onion"]
        test_supply_unmatched_amount_estimation["supply"]["ingredients"][1][
            "ingredients-declaration"
        ] = f"Zwiebeln, {supply_ingredient['names'][0]['value']}"
        test_supply_unmatched_amount_estimation["supply"]["ingredients"][1]["names"][0][
            "value"
        ] = "Zwiebeln with ingredients_declaration"
        test_supply_unmatched_amount_estimation["supply"]["ingredients"][1]["origin"] = "Zürich Schweiz"
        test_supply_unmatched_amount_estimation["supply"]["ingredients"][1]["id"] = "new_100100191_02"

        # check that the response is as expected
        expected_response: dict[str, Any] = copy.deepcopy(test_supply_unmatched_amount_estimation)
        for ingredient in expected_response["supply"]["ingredients"]:
            if "ingredients-declaration" in ingredient:
                del ingredient["ingredients-declaration"]

        response = await ac.post(
            f"/api/kitchens/{kitchen_xid}/supplies/",
            auth=(customer_token, ""),
            json=test_supply_unmatched_amount_estimation,
        )

        assigned_supply_id = response.json()["supply-id"]

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/supplies/{assigned_supply_id}?full-resource=true", auth=(customer_token, "")
        )
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = assigned_supply_id
        expected_response["supply"]["id"] = assigned_supply_id
        expected_response["supply"]["kitchen-id"] = kitchen_xid

        assert response.status_code == 200
        # update 6.9.2024: drying process added to onion as an ingredient of a dried combined-product
        check_response_dict(
            expected_response_without_nutrients(expected_response),
            response.json(),
            "supply",
            expected_co2_value=170,
            expected_co2_rating="B",
            expect_eaternity_award_field=None,
            expected_co2_improvement_percentage=34,
            expected_co2_reduction_value=89,
            expected_daily_food_unit=0.066,
            expected_ingredients_co2_value=[None, 170],
            expected_ingredients_co2_rating=[None, "D"],
            expected_ingredients_bar_chart=[None, 100],
            expected_ingredients_food_unit=[None, 0.016],
            expected_ingredients_co2_improvement_percentage=[
                None,
                -166,
            ],
            expected_ingredients_co2_reduction_value=[
                None,
                -106,
            ],
        )


TEST_SUPPLY_02_LEGACY_CO2_VALUE = round(TEST_SUPPLY_EXPECTED_CO2 * 1000)
TEST_SUPPLY_02_LEGACY_CO2_VALUE_ING1 = round(TEST_SUPPLY_EXPECTED_CO2_ING1 * 1000)
TEST_SUPPLY_02_LEGACY_CO2_VALUE_ING2 = round(TEST_SUPPLY_EXPECTED_CO2_ING2 * 1000)

TEST_SUPPLY_02_LEGACY_DFU_VALUE = TEST_SUPPLY_EXPECTED_DFU
TEST_SUPPLY_02_LEGACY_DFU_VALUE_ING1 = TEST_SUPPLY_EXPECTED_DFU_ING1
TEST_SUPPLY_02_LEGACY_DFU_VALUE_ING2 = TEST_SUPPLY_EXPECTED_DFU_ING2

TEST_SUPPLY_02_LEGACY_BAR_CHART_ING1 = round(100 * TEST_SUPPLY_EXPECTED_CO2_ING1 / TEST_SUPPLY_EXPECTED_CO2)
TEST_SUPPLY_02_LEGACY_BAR_CHART_ING2 = round(100 * TEST_SUPPLY_EXPECTED_CO2_ING2 / TEST_SUPPLY_EXPECTED_CO2)


TEST_SUPPLY_02 = {
    "supply": {
        "supplier": "Vegetable Supplies Ltd.",
        "supplier-id": "el3i5y-2in5y-2hbllll01",
        "invoice-id": "778888800000001_02",
        "supply-date": "2023-06-20",
        "ingredients": [
            {
                "id": "100100191_02",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Zwiebeln_Kartoffeln_Produkt",
                    },
                ],
                "amount": 150,
                "unit": "gram",
                "origin": "Zürich Schweiz",
                "transport": "ground",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
                "ingredients-declaration": "Zwiebeln, Kartoffeln",
                "nutrient-values": {  # generated with nutrient_mixer in core/core/test_ingredient_amount_estimator
                    # with ratio  Zwiebeln : Kartoffeln = 7 : 3
                    "energy-kcal": 50.213899999999995,
                    "fat-gram": 0.16999999999999998,
                    "saturated-fat-gram": 0.0,
                    "carbohydrates-gram": 9.579999999999998,
                    "water-gram": 85.91,
                    "sucrose-gram": 5.109999999999999,
                    "protein-gram": 1.5099999999999998,
                    "sodium-chloride-gram": 3.5599999999999996 * 1e-3,
                    "chlorine-milligram": 28.299999999999997,
                },
            },
            {
                "id": "300100191_02",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Kartoffel_Tomaten_Produkt",
                    },
                ],
                "amount": 270,
                "unit": "gram",
                "origin": "Zürich Schweiz",
                "transport": "ground",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
                "ingredients-declaration": "Kartoffeln, Tomaten",
                "nutrient-values": {  # generated with nutrient_mixer in core/core/test_ingredient_amount_estimator
                    # with ratio Kartoffeln : Tomaten = 6 : 4
                    "energy-kcal": 54.39639999999999,
                    "fat-gram": 0.18,
                    "saturated-fat-gram": 0.0,
                    "carbohydrates-gram": 10.64,
                    "water-gram": 84.74000000000001,
                    "sucrose-gram": 1.7000000000000002,
                    "protein-gram": 1.52,
                    "sodium-milligram": 2.44,
                    "chlorine-milligram": 42.0,
                },
            },
        ],
    }
}


@pytest.mark.asyncio
async def test_supplies_with_ingredients_declaration_and_nutrient_values(
    app_fixture: AppFixtureReturn,
    create_simple_customer: tuple[str, str, str],
) -> None:
    _, customer_token, _ = create_simple_customer
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        expected_response: dict[str, Any] = copy.deepcopy(TEST_SUPPLY_02)

        for ingredient in expected_response["supply"]["ingredients"]:
            if "ingredients-declaration" in ingredient:
                del ingredient["ingredients-declaration"]

        response = await ac.post("/api/supplies", auth=(customer_token, ""), json=TEST_SUPPLY_02)
        assert response.status_code == 200
        assert response.json()["statuscode"] == 200
        assigned_supply_id = response.json()["supply-id"]

        response = await ac.get(
            f"/api/supplies/{assigned_supply_id}?full-resource=true&nutrient-values=true", auth=(customer_token, "")
        )
        expected_response["supply"]["id"] = assigned_supply_id
        expected_response["supply"]["kitchen-id"] = response.json()["supply"]["kitchen-id"]
        adapt_expected_response(expected_response)
        expected_response["statuscode"] = 200
        expected_response["message"] = ""
        expected_response["supply-id"] = assigned_supply_id

        assert response.status_code == 200
        check_response_dict(
            expected_response,
            response.json(),
            "supply",
            expected_co2_value=TEST_SUPPLY_02_LEGACY_CO2_VALUE,
            expected_daily_food_unit=TEST_SUPPLY_02_LEGACY_DFU_VALUE,
            expected_ingredients_co2_value=[TEST_SUPPLY_02_LEGACY_CO2_VALUE_ING1, TEST_SUPPLY_02_LEGACY_CO2_VALUE_ING2],
            expected_ingredients_bar_chart=[TEST_SUPPLY_02_LEGACY_BAR_CHART_ING1, TEST_SUPPLY_02_LEGACY_BAR_CHART_ING2],
            expected_ingredients_food_unit=[TEST_SUPPLY_02_LEGACY_DFU_VALUE_ING1, TEST_SUPPLY_02_LEGACY_DFU_VALUE_ING2],
            expected_co2_reduction_value=331,
            expected_co2_improvement_percentage=82,
            expected_ingredients_co2_improvement_percentage=[
                76,
                85,
            ],
            expected_ingredients_co2_reduction_value=[
                107,
                224,
            ],
            expected_co2_rating="A",
            expect_eaternity_award_field=None,
            expected_ingredients_co2_rating=["A", "A"],
            allow_dict_diff_to_be_empty=False,
        )
