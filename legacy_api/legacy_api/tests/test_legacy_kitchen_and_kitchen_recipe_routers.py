import copy
import os
from typing import Any

import pytest
from httpx import AsyncClient
from structlog import get_logger

from .conftest import (
    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
    EXPECTED_CO2_REDUCTION_VALUE,
    EXPECTED_CO2_REDUCTION_VALUE_ING_1,
    EXPECTED_CO2_REDUCTION_VALUE_ING_2,
    TEST_KITCHEN_ADMIN_ID,
    TEST_KITCHEN_SUPERUSER_ID,
    TEST_RECIPE,
    TEST_RECIPE_ADMIN_ID,
    TEST_RECIPE_IN_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
    TEST_RECIPE_IN_SUBRECIPE_2_REDUCTION_VALUE,
    TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
    TEST_RECIPE_LEGACY_CO2_RATING,
    TEST_RECIPE_LEGACY_CO2_VALUE,
    TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE_2,
    TEST_RECIPE_LEGACY_FOOD_UNIT,
    TEST_RECIPE_LEGACY_ING1_FOOD_UNIT,
    TEST_RECIPE_LEGACY_ING2_CO2_VALUE,
    TEST_RECIPE_LEGACY_ING2_FOOD_UNIT,
    TEST_RECIPE_SUPERUSER_ID,
    TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN,
    TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID,
    TEST_RECIPE_WITH_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
    TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE,
    TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE_TOMATO_INGREDIENT,
    TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_FOOD_UNIT,
    TEST_RECIPE_WITH_SUBRECIPE_2_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
    TEST_RECIPE_WITH_SUBRECIPE_2_REDUCTION_VALUE,
    TEST_RECIPE_WITH_SUBRECIPE_2_SUPERUSER,
    TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_IMPROVEMENT_PERCENTAGE,
    TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_REDUCTION_VALUE,
    TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID,
    AppFixtureReturn,
    check_response_dict,
)

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()


@pytest.mark.asyncio
async def test_create_and_get_and_delete_and_create_kitchen_by_superuser(
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    logger.info("finished all fixtures in test_create_and_get_and_delete_and_create_kitchen_by_superuser")
    return


@pytest.mark.asyncio
async def test_create_and_get_and_delete_and_create_kitchen_by_admin(
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    return


@pytest.mark.asyncio
async def test_get_all_kitchens(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # First try with trailing slash
        response = await ac.get(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"kitchens": [TEST_KITCHEN_ADMIN_ID]}

        # Now try without a trailing slash
        response = await ac.get(
            "/api/kitchens",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"kitchens": [TEST_KITCHEN_ADMIN_ID]}

        response = await ac.get(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"kitchens": []}


@pytest.mark.asyncio
async def test_get_all_kitchens_of_customer(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            "/api/kitchens",
            auth=(customer_token, ""),
        )
        assert response.status_code == 200
        assert response.json() == {"kitchens": [kitchen_xid]}


@pytest.mark.asyncio
async def test_create_delete_and_get_kitchen_recipe_by_superuser(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # testing POST request
        response = await ac.post(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        temp_recipe_id = response.json().get("recipe").get("id")
        TEST_RECIPE_WITH_ID: dict[str, Any] = copy.deepcopy(TEST_RECIPE)
        TEST_RECIPE_WITH_ID["recipe"]["id"] = temp_recipe_id
        TEST_RECIPE_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        TEST_RECIPE_WITH_ID["recipe"]["recipe-portions"] = TEST_RECIPE_WITH_ID["recipe"]["servings"]

        expected_result_for_post = copy.deepcopy(TEST_RECIPE_WITH_ID)
        expected_result_for_post["message"] = ""
        expected_result_for_post["recipe-id"] = temp_recipe_id
        expected_result_for_post["request-id"] = 0
        expected_result_for_post["statuscode"] = 200

        check_response_dict(
            expected_result_for_post,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating=TEST_RECIPE_LEGACY_CO2_RATING,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{temp_recipe_id}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        expected_get_resp = copy.deepcopy(TEST_RECIPE_WITH_ID)
        expected_get_resp["message"] = ""
        expected_get_resp["recipe-id"] = temp_recipe_id
        expected_get_resp["statuscode"] = 200

        check_response_dict(
            expected_get_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating=TEST_RECIPE_LEGACY_CO2_RATING,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{temp_recipe_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{temp_recipe_id}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404

        # TODO: should we test mutation log response here as well?
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        TEST_RECIPE_WITH_ID = copy.deepcopy(TEST_RECIPE)
        TEST_RECIPE_WITH_ID["recipe"]["id"] = TEST_RECIPE_SUPERUSER_ID
        TEST_RECIPE_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        TEST_RECIPE_WITH_ID["recipe"]["recipe-portions"] = TEST_RECIPE_WITH_ID["recipe"]["servings"]
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        expected_get_resp["message"] = ""
        expected_get_resp["recipe-id"] = TEST_RECIPE_SUPERUSER_ID
        expected_get_resp["statuscode"] = 200
        expected_get_resp["recipe"]["id"] = TEST_RECIPE_SUPERUSER_ID
        check_response_dict(
            expected_get_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating=TEST_RECIPE_LEGACY_CO2_RATING,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        assert response.json() == {"recipes": [TEST_RECIPE_SUPERUSER_ID]}


@pytest.mark.asyncio
async def test_create_delete_and_get_kitchen_recipe_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # testing that an admin can't post recipes into a wrong group
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 401

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        TEST_RECIPE_WITH_ID = copy.deepcopy(TEST_RECIPE)
        TEST_RECIPE_WITH_ID["recipe"]["id"] = TEST_RECIPE_ADMIN_ID
        TEST_RECIPE_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_ADMIN_ID
        TEST_RECIPE_WITH_ID["recipe"]["recipe-portions"] = TEST_RECIPE_WITH_ID["recipe"]["servings"]

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        expected_get_resp = copy.deepcopy(TEST_RECIPE_WITH_ID)
        expected_get_resp["message"] = ""
        expected_get_resp["recipe-id"] = TEST_RECIPE_ADMIN_ID
        expected_get_resp["statuscode"] = 200
        check_response_dict(
            expected_get_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating=TEST_RECIPE_LEGACY_CO2_RATING,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        # making sure that an admin can't delete a resource that's not from their group
        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 401

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        expected_get_resp = copy.deepcopy(TEST_RECIPE_WITH_ID)
        expected_get_resp["message"] = ""
        expected_get_resp["recipe-id"] = TEST_RECIPE_ADMIN_ID
        expected_get_resp["statuscode"] = 200
        check_response_dict(
            expected_get_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating=TEST_RECIPE_LEGACY_CO2_RATING,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )


@pytest.mark.asyncio
async def test_create_delete_and_get_kitchen_recipe_with_subrecipe_by_superuser(
    app_fixture: AppFixtureReturn, create_kitchen_recipe_with_subrecipe_by_superuser: None
) -> None:
    _ = create_kitchen_recipe_with_subrecipe_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # testing POST request
        TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE_2_SUPERUSER)
        TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID["recipe"]["id"] = TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID
        TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID["recipe"][
            "recipe-portions"
        ] = TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID["recipe"]["servings"]

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/"
            f"{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )

        assert response.status_code == 200
        expected_get_resp = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID)
        expected_get_resp["message"] = ""
        expected_get_resp["recipe-id"] = TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID
        expected_get_resp["statuscode"] = 200
        check_response_dict(
            expected_get_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_FOOD_UNIT,
            expect_eaternity_award_field=True,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE_2,
                TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE_TOMATO_INGREDIENT,
            ],
            expected_ingredients_co2_rating=["A", "A"],
            expected_ingredients_bar_chart=[92, 8],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_2_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
            ],
            expected_ingredients_co2_improvement_percentage=[
                TEST_RECIPE_IN_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
                TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_IMPROVEMENT_PERCENTAGE,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_RECIPE_IN_SUBRECIPE_2_REDUCTION_VALUE,
                TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_REDUCTION_VALUE,
            ],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE_WITH_SUBRECIPE_2_SUPERUSER,
        )
        assert response.status_code == 200

        # TODO: should we test mutation log response here as well?
        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/"
            f"{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        expected_get_resp = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_WITH_ID)
        expected_get_resp["message"] = ""
        expected_get_resp["recipe-id"] = TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID
        expected_get_resp["statuscode"] = 200
        check_response_dict(
            expected_get_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE,
            expect_eaternity_award_field=True,
            expected_co2_rating="A",
            expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE_2,
                TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE_TOMATO_INGREDIENT,
            ],
            expected_ingredients_co2_rating=["A", "A"],
            expected_ingredients_bar_chart=[92, 8],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_2_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
            ],
            expected_ingredients_co2_improvement_percentage=[
                TEST_RECIPE_IN_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
                TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_IMPROVEMENT_PERCENTAGE,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_RECIPE_IN_SUBRECIPE_2_REDUCTION_VALUE,
                TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_REDUCTION_VALUE,
            ],
        )


@pytest.mark.asyncio
async def test_create_delete_and_get_kitchen_recipe_with_subrecipe_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # first insert the subrecipe, so that we can actually use it afterwards:
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        # now add the outer recipe that contains the above subrecipe
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN,
        )
        assert response.status_code == 200

        TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN)
        TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID["recipe"]["id"] = TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID
        TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID["recipe"]["kitchen-id"] = TEST_KITCHEN_ADMIN_ID
        TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID["recipe"][
            "recipe-portions"
        ] = TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID["recipe"]["servings"]

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/"
            f"{TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )

        assert response.status_code == 200
        expected_get_resp = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID)
        expected_get_resp["message"] = ""
        expected_get_resp["recipe-id"] = TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID
        expected_get_resp["statuscode"] = 200
        check_response_dict(
            expected_get_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE_2,
                TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE_TOMATO_INGREDIENT,
            ],
            expected_ingredients_co2_rating=["A", "A"],
            expected_ingredients_bar_chart=[92, 8],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_2_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
            ],
            expected_ingredients_co2_improvement_percentage=[
                TEST_RECIPE_IN_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
                TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_IMPROVEMENT_PERCENTAGE,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_RECIPE_IN_SUBRECIPE_2_REDUCTION_VALUE,
                TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_REDUCTION_VALUE,
            ],
        )

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN,
        )
        assert response.status_code == 200

        # TODO: should we test mutation log response here as well?
        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}/recipes/"
            f"{TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        expected_get_resp = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE_ADMIN_WITH_ID)
        expected_get_resp["message"] = ""
        expected_get_resp["recipe-id"] = TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID
        expected_get_resp["statuscode"] = 200
        check_response_dict(
            expected_get_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE_2,
                TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE_TOMATO_INGREDIENT,
            ],
            expected_ingredients_co2_rating=["A", "A"],
            expected_ingredients_bar_chart=[92, 8],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_2_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT,
            ],
            expected_ingredients_co2_improvement_percentage=[
                TEST_RECIPE_IN_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE,
                TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_IMPROVEMENT_PERCENTAGE,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_RECIPE_IN_SUBRECIPE_2_REDUCTION_VALUE,
                TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_REDUCTION_VALUE,
            ],
        )
