import asyncio
import base64
import json
import os
import time
import uuid
from asyncio import Event, Task
from typing import Any, AsyncGenerator, Generator, Optional
from unittest import mock

import pytest
import pytest_asyncio
import uvicorn
from _pytest.fixtures import SubRequest
from deepdiff import DeepDiff
from gap_filling_modules.ingredicalc.helpers import transform_eurofir
from gap_filling_modules.processing_gfm import ProcessingGapFillingFactory
from httpx import AsyncClient, Response
from lxml import etree
from structlog import get_logger

from api.app.server import fastapi_app as fastapi_app_api
from api.app.server import startup_finished_event
from api.app.settings import Settings
from api.tests.test_api_for_recipes import (
    ONION_DRYING_UPSCALING,
    TEST_RECIPE_EXPECTED_CO2,
    TEST_RECIPE_EXPECTED_CO2_OF_ONION_TRANSPORT,
    TEST_RECIPE_EXPECTED_CO2_WITHOUT_UPSCALING,
    TEST_RECIPE_EXPECTED_DFU,
    TEST_RECIPE_EXPECTED_DFU_NO_DRYING,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_ONION_UPSCALED,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_TRANSPORT,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU,
)
from api.tests.test_calc_router import TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION
from core.domain.glossary_link import GlossaryLink
from core.domain.nodes.activity.elementary_resource_emission_node import ElementaryResourceEmissionNode
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator
from core.tests.conftest import add_flow, add_greenhouse_mock_nodes, seed_processing
from database.postgres.settings import PgSettings
from legacy_api.app.api_client import ApiClient
from legacy_api.app.server import api_client, app_shutdown, app_startup
from legacy_api.app.server import fastapi_app as fastapi_app_legacy
from legacy_api.app.server import postgres_db
from legacy_api.app.settings import Settings as LegacyApiSettings
from legacy_api.converter.common import INGREDIENT_PROP_DEFAULTS
from legacy_api.dto.legacy.nutrient_value import NutrientValueDto

AppFixtureReturn = AsyncGenerator[Any, Any]

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()
settings = Settings()
legacy_api_settings = LegacyApiSettings()
TOLERANCE = 1e-8


def is_close(a: float, b: float, tol: float = TOLERANCE) -> bool:
    return abs(a - b) < tol


CUSTOMER = {
    "name": "test_legacy_customer_name",
    "namespace_uid": "dbc54992-0ce2-40e5-8410-eda55b702ec4",
    "namespace_xid": "test_legacy_customer_xid",
}

USER_SUPER = {
    "email": "user_super@example.com",
    # password is optional right now, leaving this field as
    # a placeholder for future authentication features
    "password": "",
    "is_superuser": True,
}
USER_SUPER_ID = str(uuid.uuid4())

USER_ADMIN = {
    "email": "user_admin@example.com",
    "password": "",
    "is_superuser": False,
}
USER_ADMIN_ID = str(uuid.uuid4())

USER_BASIC = {
    "email": "user_basic@example.com",
    "password": "",
    "is_superuser": False,
}
USER_BASIC_ID = str(uuid.uuid4())

TEST_KITCHEN_SUPERUSER_ID = "test_kitchen_superuser_legacy_id"
TEST_KITCHEN_SUPERUSER = {
    "xid": TEST_KITCHEN_SUPERUSER_ID,
    "name": "test_kitchen_superuser_legacy_name",
    "location": "test_location",
}

TEST_KITCHEN_SUPERUSER_PARENT_ID = "test_kitchen_superuser_parent_legacy_id"
TEST_KITCHEN_SUPERUSER_PARENT = {
    "xid": TEST_KITCHEN_SUPERUSER_PARENT_ID,
    "name": "test_kitchen_superuser_parent_legacy_name",
    "location": "test_location",
}

TEST_KITCHEN_ADMIN_ID = "test_kitchen_admin_legacy_id"
TEST_KITCHEN_ADMIN = {
    "xid": TEST_KITCHEN_ADMIN_ID,
    "name": "test_kitchen_admin_legacy_name",
    "location": "test_location",
}

SERVINGS = 140
TEST_RECIPE = {
    "recipe": {
        "titles": [{"language": "de", "value": "Kürbisrisotto"}],
        "author": "Eckart Witzigmann",
        "date": "2023-06-20",
        "location": "Zürich Schweiz",
        "servings": SERVINGS,
        "instruction": "Den Karottenkuchen im Ofen backen und noch warm geniessen.",
        "ingredients": [
            {
                "id": "100199191",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Tomaten",
                    },
                ],
                "amount": 150,
                "unit": "kilogram",
                "origin": "spain",
                "transport": "air",
                "production": "greenhouse",
                "sourcing-origin": "test it",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
                "producer": "Tomato Farm Ltd.",
            },
            {
                "id": "100199894",
                "type": "conceptual-ingredients",
                "names": [
                    {
                        "language": "de",
                        "value": "Zwiebeln",
                    },
                ],
                "amount": 78,
                "unit": "kilogram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",
                "packaging": "",
            },
        ],
    }
}
TEST_RECIPE_SUPERUSER_ID = "test_recipe_superuser_legacy_id"
TEST_RECIPE_ADMIN_ID = "test_recipe_admin_legacy_id"


TEST_RECIPE_WITH_SUBRECIPE = {
    "request-id": 1.123,
    "recipe": {
        "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
        "ingredients": [
            {
                "transport": "air",
                "processing": "raw",
                "packaging": "plastic",
                "id": "test_recipe_id",
                "origin": "Z\u00fcrich Schweiz",
                "production": "standard",
                "conservation": "fresh",
                "type": "recipes",
                "amount": 50,
                "unit": "kilogram",
            },
            {
                "transport": "ground",
                "processing": "",
                "packaging": "",
                "id": "242342343",
                "names": [{"language": "de", "value": "Tomaten"}],
                "origin": "france",
                "production": "organic",
                "type": "conceptual-ingredients",
                "amount": 78,
                "unit": "kilogram",
            },
        ],
        "servings": 140,
        "date": "2023-06-20",
        "id": "test_recipe_with_subrecipe_id",
        "location": "Z\u00fcrich Schweiz",
        "production-portions": 500,
        "sold-portions": 300,
        "menu-line-name": "EOS test recipe with subrecipe",
        "menu-line-id": 10101010,
        "kitchen-id": "kitchen01",
    },
}

TEST_RECIPE_LEGACY_CO2_VALUE = round(TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000 / SERVINGS)
TEST_RECIPE_LEGACY_CO2_RATING = "A"
TEST_RECIPE_LEGACY_ING2_CO2_VALUE = round(TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000)
TEST_RECIPE_LEGACY_CO2_VALUE_WITHOUT_DRYING = round(
    (TEST_RECIPE_EXPECTED_CO2_WITHOUT_UPSCALING + TEST_RECIPE_EXPECTED_CO2_OF_ONION_TRANSPORT) * 1000 * 1000 / SERVINGS
)
TEST_RECIPE_LEGACY_ING2_CO2_VALUE_WITHOUT_DRYING = round(
    (TEST_RECIPE_EXPECTED_CO2_WITHOUT_UPSCALING + TEST_RECIPE_EXPECTED_CO2_OF_ONION_TRANSPORT) * 1000 * 1000
)

TOMATO_TRUCK_TRANSPORT_FROM_SPAIN_CO2_VALUE = 19544
TOMATO_TRUCK_TRANSPORT_CO2_IMPROVEMENT_PERCENTAGE = 79
TOMATO_TRUCK_TRANSPORT_CO2_REDUCTION_VALUE = 72866

TEST_RECIPE_WITH_TOMATO_TRANSPORT_CO2_IMPROVEMENT_PERCENTAGE = 52
TEST_RECIPE_WITH_TOMATO_TRANSPORT_CO2_REDUCTION_VALUE = 1627

TEST_RECIPE_WITH_SUBRECIPE_CO2_VALUE = 93031
TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE = round(TEST_RECIPE_WITH_SUBRECIPE_CO2_VALUE / SERVINGS)
TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE = 36
TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE = 369
TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE = round(TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000 * 50 / 228)
TEST_RECIPE_IN_SUBRECIPE_IMPROVEMENT_PERCENTAGE = 56
TEST_RECIPE_IN_SUBRECIPE_REDUCTION_VALUE = 54243
TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE = round(
    TEST_RECIPE_WITH_SUBRECIPE_CO2_VALUE - TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000 * 50 / 228
)
TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_IMPROVEMENT_PERCENTAGE = -5
TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_REDUCTION = -2557
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# *1000 because legacy co2eq value is in gram instead of kilogram

TEST_RECIPE_FOOD_UNIT = TEST_RECIPE_EXPECTED_DFU * 1000
TEST_RECIPE_LEGACY_FOOD_UNIT = TEST_RECIPE_EXPECTED_DFU * 1000 / SERVINGS
TEST_RECIPE_NO_DRYING_LEGACY_FOOD_UNIT = TEST_RECIPE_EXPECTED_DFU_NO_DRYING * 1000 / SERVINGS
TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT = TEST_RECIPE_EXPECTED_DFU * 1000 * 50 / 228
TEST_RECIPE_LEGACY_ING1_FOOD_UNIT = 23.734
TEST_RECIPE_LEGACY_ING2_FOOD_UNIT_NO_DRYING = 16.414
TEST_RECIPE_LEGACY_ING2_FOOD_UNIT = TEST_RECIPE_FOOD_UNIT - TEST_RECIPE_LEGACY_ING1_FOOD_UNIT
TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT = 37.1685 / SERVINGS
TEST_RECIPE_WITH_SUBRECIPE_FOOD_UNIT = 37.1685
TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT = (
    TEST_RECIPE_WITH_SUBRECIPE_FOOD_UNIT - TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT
)
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# Note that the daily food unit is not scaled by servings even in eaternity-cloud (Javaland).

TEST_RECIPE_EXPECTED_SCARCE_WATER_LITER = round(TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION * 1000 / SERVINGS, 2)

EXPECTED_CO2_IMPROVEMENT_PERCENTAGE = 56
EXPECTED_CO2_REDUCTION_VALUE = 1767
EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1 = 100
EXPECTED_CO2_REDUCTION_VALUE_ING_1 = 92410
EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2 = 44
EXPECTED_CO2_REDUCTION_VALUE_ING_2 = 154936

TEST_RECIPE_LEGACY_VITASCORE = {
    "energy-kcals": 2003.74,
    "nutrition-label": False,
    "fruit-risk-factor-amount-gram": 0.0,
    "fruit-risk-factor-points": 61.24,
    "milk-risk-factor-amount-gram": 0.0,
    "milk-risk-factor-points": 9.42,
    "nuts-seeds-risk-factor-amount-gram": 0.0,
    "nuts-seeds-risk-factor-points": 13.98,
    "processed-meat-risk-factor-amount-gram": 0.0,
    "processed-meat-risk-factor-points": 0.0,
    "red-meat-risk-factor-amount-gram": 0.0,
    "red-meat-risk-factor-points": 0.0,
    "salt-risk-factor-amount-gram": 0.48,
    "salt-risk-factor-points": 0.0,
    "vegetable-risk-factor-amount-gram": 1628.57,
    "vegetable-risk-factor-points": 0.0,
    "vita-score-improvement-percentage": -7.02,
    "vita-score-points": 363.88,
    "vita-score-rating": "C",
    "wholegrain-risk-factor-amount-gram": 0.0,
    "wholegrain-risk-factor-points": 82.86,
    "high-in-energy-risk-factor-points": 88.66,
    "high-in-fat-risk-factor-points": 0.0,
    "low-in-fat-risk-factor-points": 86.54,
    "high-in-protein-risk-factor-points": 0.0,
    "low-in-protein-risk-factor-points": 21.18,
}

TEST_RECIPE_LEGACY_VITASCORE_ING1 = {
    "energy-kcals": 31906.5,
    "nutrition-label": False,
    "fruit-risk-factor-amount-gram": 0.0,
    "fruit-risk-factor-points": 61.24,
    "milk-risk-factor-amount-gram": 0.0,
    "milk-risk-factor-points": 9.42,
    "nuts-seeds-risk-factor-amount-gram": 0.0,
    "nuts-seeds-risk-factor-points": 13.98,
    "processed-meat-risk-factor-amount-gram": 0.0,
    "processed-meat-risk-factor-points": 0.0,
    "red-meat-risk-factor-amount-gram": 0.0,
    "red-meat-risk-factor-points": 0.0,
    "salt-risk-factor-amount-gram": 6.10,
    "salt-risk-factor-points": 0.0,
    "vegetable-risk-factor-amount-gram": 150000.0,
    "vegetable-risk-factor-points": 0.0,
    "vita-score-improvement-percentage": 2.31,
    "vita-score-points": 332.13,
    "vita-score-rating": "B",
    "wholegrain-risk-factor-amount-gram": 0.0,
    "wholegrain-risk-factor-points": 82.86,
    "high-in-energy-risk-factor-points": 88.66,
    "high-in-fat-risk-factor-points": 0.0,
    "low-in-fat-risk-factor-points": 58.34,
    "high-in-protein-risk-factor-points": 0.0,
    "low-in-protein-risk-factor-points": 17.63,
}
TEST_RECIPE_LEGACY_VITASCORE_ING2 = {
    "energy-kcals": 248616.49,
    "nutrition-label": False,
    "fruit-risk-factor-amount-gram": 0.0,
    "fruit-risk-factor-points": 61.24,
    "milk-risk-factor-amount-gram": 0.0,
    "milk-risk-factor-points": 9.42,
    "nuts-seeds-risk-factor-amount-gram": 0.0,
    "nuts-seeds-risk-factor-points": 13.98,
    "processed-meat-risk-factor-amount-gram": 0.0,
    "processed-meat-risk-factor-points": 0.0,
    "red-meat-risk-factor-amount-gram": 0.0,
    "red-meat-risk-factor-points": 0.0,
    "salt-risk-factor-amount-gram": 61.64,
    "salt-risk-factor-points": 0.0,
    "vegetable-risk-factor-amount-gram": 78000.0,
    "vegetable-risk-factor-points": 0.0,
    "vita-score-improvement-percentage": -7.78,
    "vita-score-points": 366.46,
    "vita-score-rating": "C",
    "wholegrain-risk-factor-amount-gram": 0.0,
    "wholegrain-risk-factor-points": 82.86,
    "high-in-energy-risk-factor-points": 88.66,
    "high-in-fat-risk-factor-points": 0.0,
    "low-in-fat-risk-factor-points": 88.66,
    "high-in-protein-risk-factor-points": 0.0,
    "low-in-protein-risk-factor-points": 21.64,
}

ZERO_VITA_SCORE = {
    "energy-kcals": 0.0,
    "nutrition-label": False,
    "fruit-risk-factor-amount-gram": 0.0,
    "fruit-risk-factor-points": 0.0,
    "milk-risk-factor-amount-gram": 0.0,
    "milk-risk-factor-points": 0.0,
    "nuts-seeds-risk-factor-amount-gram": 0.0,
    "nuts-seeds-risk-factor-points": 0.0,
    "processed-meat-risk-factor-amount-gram": 0.0,
    "processed-meat-risk-factor-points": 0.0,
    "red-meat-risk-factor-amount-gram": 0.0,
    "red-meat-risk-factor-points": 0.0,
    "salt-risk-factor-amount-gram": 0.0,
    "salt-risk-factor-points": 0.0,
    "vegetable-risk-factor-amount-gram": 0.0,
    "vegetable-risk-factor-points": 0.0,
    "vita-score-improvement-percentage": 0.0,  # We decided that this should be 0.0 for amount=0.0
    "vita-score-points": 0.0,
    "vita-score-rating": "E",  # We decided that this should be "E" for amount=0.0
    "wholegrain-risk-factor-amount-gram": 0.0,
    "wholegrain-risk-factor-points": 0.0,
    "high-in-energy-risk-factor-points": 0.0,
    "high-in-fat-risk-factor-points": 0.0,
    "low-in-fat-risk-factor-points": 0.0,
    "high-in-protein-risk-factor-points": 0.0,
    "low-in-protein-risk-factor-points": 0.0,
}
ZERO_AMOUNT_INDICATORS = {
    "animal-treatment-label": False,  # We decided that this should be False for amount=0.0
    "animal-treatment-rating": "E",  # We decided that this should be "E" for amount=0.0
    "rainforest-label": False,
    "rainforest-rating": "E",
    "scarce-water-liters": 0.0,
    "water-footprint-award": False,  # this is consistent with the javaland result for amount=0.0
    # removing "water-footprint-improvement-percentage" is consistent with the javaland result for amount=0.0
    "water-footprint-rating": "",  # this is consistent with the javaland result for amount=0.0
    "local-label": False,
    "local-rating": "E",  # We decided that this should be "E" for amount=0.0
    "season-label": False,
    "season-rating": "E",
    "vita-score": ZERO_VITA_SCORE,
}

# The main difference of this recipe compared to the one above it has "spain" instead of "Zürich" as origin
# and that the tomato ingredient is set to "dried" instead of "fresh".
TEST_RECIPE_WITH_SUBRECIPE_2_SUPERUSER = {
    "recipe": {
        "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
        "date": "2023-06-20",
        "location": "Zürich Schweiz",
        "servings": SERVINGS,
        "ingredients": [
            {
                "id": TEST_RECIPE_SUPERUSER_ID,
                "type": "recipes",
                "names": [{"language": "de", "value": "Kürbisrisotto"}],
                "amount": 50,
                "unit": "kilogram",
                "origin": "spain",
                "transport": "air",
                "production": "standard",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
            },
            {
                "id": "242342343",
                "type": "conceptual-ingredients",
                "names": [{"language": "de", "value": "Tomaten"}],  # Changed from Kartoffeln for consistency with API.
                "amount": 78,
                "unit": "kilogram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",  # Left dried here (deleted in API) to verify GreenhouseGFM is not triggered.
                "packaging": "",
            },
        ],
    }
}


TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN_ID = "test_sub_recipe_admin_legacy_id"
TEST_RECIPE_WITH_SUBRECIPE_2_ADMIN = {
    "recipe": {
        "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
        "date": "2023-06-20",
        "location": "Zürich Schweiz",
        "servings": SERVINGS,
        "ingredients": [
            {
                "id": TEST_RECIPE_ADMIN_ID,
                "type": "recipes",
                "names": [{"language": "de", "value": "Kürbisrisotto"}],
                "amount": 50,
                "unit": "kilogram",
                "origin": "spain",
                "transport": "air",
                "production": "greenhouse",
                "processing": "raw",
                "conservation": "fresh",
                "packaging": "plastic",
            },
            {
                "id": "242342343",
                "type": "conceptual-ingredients",
                "names": [{"language": "de", "value": "Tomaten"}],  # Changed from Kartoffeln for consistency with API.
                "amount": 78,
                "unit": "kilogram",
                "origin": "france",
                "transport": "ground",
                "production": "organic",
                "processing": "",
                "conservation": "dried",
                "packaging": "",
            },
        ],
    }
}


TEST_RECIPE_WITH_SUBRECIPE_2_OTHER_INGREDIENT_DRIED_LEGACY_FOOD_UNIT = (
    TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT + 82.474
)

TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID = "test_sub_recipe_superuser_legacy_id"

TRANSPORT_CO2_TOMATO_SPAIN_TO_SPAIN = 0.0014836710955010035
TRANSPORT_CO2_ONION_FRANCE_TO_SPAIN = 0.001438066239999999

TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE_2 = round(
    (
        TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_ONION_UPSCALED
        + TRANSPORT_CO2_TOMATO_SPAIN_TO_SPAIN
        + TRANSPORT_CO2_ONION_FRANCE_TO_SPAIN
    )
    * 1000
    * 1000
)


TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE_TOMATO_INGREDIENT = round(
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_TRANSPORT * 1000 * 1000
)

TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE = round(
    (TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE_2 + TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_CO2_VALUE_TOMATO_INGREDIENT)
    / SERVINGS
)

TEST_RECIPE_WITH_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE = 90
TEST_RECIPE_WITH_SUBRECIPE_2_REDUCTION_VALUE = 2982

TEST_RECIPE_IN_SUBRECIPE_2_IMPROVEMENT_PERCENTAGE = 54
TEST_RECIPE_IN_SUBRECIPE_2_REDUCTION_VALUE = 52175

TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_IMPROVEMENT_PERCENTAGE = 99
TEST_RECIPE_WITH_SUBRECIPE_2_TOMATO_INGREDIENT_REDUCTION_VALUE = 365272

# Additional 82.47 due to drying of tomato.
# *1000 kilogram instead of gram used as units in the ingredients (in api package, it uses gram)
# Note that the daily food unit is not scaled by servings even in eaternity-cloud (Javaland).
TEST_RECIPE_WITH_SUBRECIPE_2_LEGACY_FOOD_UNIT = (TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU * 1000 + 82.47) / SERVINGS


def pytest_configure() -> None:
    """Helper function for setting global variables across this test package."""
    pytest.ENCODED_CUSTOMER_TOKEN = ""
    pytest.ENCODED_USER_SUPER_TOKEN = ""
    pytest.ENCODED_USER_ADMIN_TOKEN = ""
    pytest.ENCODED_USER_BASIC_TOKEN = ""

    # these aren't used in any legacy API tests,
    # but let them stay for possible future purposes
    pytest.TEST_KITCHEN_SUPERUSER_PARENT_UID = ""
    pytest.TEST_KITCHEN_SUPERUSER_UID = ""
    pytest.TEST_KITCHEN_ADMIN_UID = ""


@pytest.fixture(scope="package")
def event_loop() -> Generator[asyncio.AbstractEventLoop, Any, None]:
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


class UvicornServer(uvicorn.Server):
    serve_task: Task
    did_start: Event
    did_close: Event

    def __init__(self, *args: tuple, **kwargs: dict[str, Any]):
        super().__init__(*args, **kwargs)
        self.did_start = Event()
        self.did_close = Event()

    async def start(self) -> None:
        self.serve_task = asyncio.create_task(self.serve())
        self.serve_task.add_done_callback(lambda _: self.did_close.set())
        await self.did_start.wait()

    async def startup(self, sockets: list = None) -> None:
        await super().startup(sockets)
        self.did_start.set()

    async def shutdown(self, **kwargs: dict[str, Any]) -> None:
        await super().shutdown(**kwargs)
        self.serve_task.cancel()
        await self.did_close.wait()


@pytest_asyncio.fixture(scope="module")
async def api_server(reset_db: None) -> AsyncGenerator[Any, Any]:
    """Fixture for API server.

    this fixture is module-scoped, so that not only the database,
    but main API's cache is also reset after each module
    """
    _ = reset_db
    settings = PgSettings()

    graph_mgr = postgres_db.get_graph_mgr()

    co2_emission = await graph_mgr.upsert_node_by_uid(
        ElementaryResourceEmissionNode(
            node_type=ElementaryResourceEmissionNode.__name__,
            production_amount={"value": 1.0, "unit": "kilogram"},
            key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
            name="Carbon dioxide, fossil",
            categories=["air"],
        )
    )
    # necessary for transportation & orchestration unit tests
    await postgres_db.get_product_mgr().bulk_insert_xid_uid_mappings(
        [
            (
                settings.EATERNITY_NAMESPACE_UUID,
                "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
                co2_emission.uid,
            )
        ],
    )
    # add the nodes necessary to for the greenhouse gfm
    await add_greenhouse_mock_nodes(postgres_db, graph_mgr, co2_emission, country_code="FR")
    await add_greenhouse_mock_nodes(postgres_db, graph_mgr, co2_emission, country_code="CH")

    term_mgr = postgres_db.get_term_mgr()
    foodex2_term_group_uid = ""
    groups = await term_mgr.find_all_term_access_groups()
    for group in groups:
        if "FoodEx2" in group.data.get("name"):
            foodex2_term_group_uid = group.uid
            break
    # the cooling terms have to be added always because they might also be relevant if cooling
    # was not explicitly declared in the recipe (e.g. for perishable products)
    cooled_term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid("J0131", foodex2_term_group_uid)

    cooling_process = await graph_mgr.upsert_node_by_uid(
        ModeledActivityNode(
            activity_location="GLO",
            production_amount={"value": 1, "unit": "kg*day"},
            id="('ecoinvent 3.6 cutoff', '001decfdb6f2319cd8578e05c91c247d')",
            key=[
                "ecoinvent 3.6 cutoff",
                "001decfdb6f2319cd8578e05c91c247d",
            ],
            flow=None,
            name="market for operation, reefer, cooling",
            type="process",
            database="ecoinvent 3.6 cutoff",
            filename=None,
            reference_product="market for operation, reefer, cooling",
        )
    )

    await postgres_db.get_pg_glossary_link_mgr().insert_glossary_link(
        GlossaryLink(
            gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
            term_uids=[cooled_term.uid],
            linked_node_uid=cooling_process.uid,
        )
    )

    await add_flow(graph_mgr, cooling_process, co2_emission, 0.005, "biosphere")

    config = uvicorn.Config(
        app=fastapi_app_api, host="0.0.0.0", port=8040, workers=1, log_level="debug", access_log=False, log_config=None
    )
    server = UvicornServer(config=config)
    await server.start()

    logger.debug("start wait for api server to be up and initialized...")
    startup_finished_event.wait()  # wait for this multiprocessing.Event from the api server process
    logger.debug("finished wait")

    # wait for the api server to actually accept incoming connections
    for _retries in range(8):
        async with AsyncClient() as session:
            api_response = await session.get(
                f"{legacy_api_settings.api_v2_url}/status",
            )
            try:
                result = api_response.json()
                assert result == {"service": "eos-v2", "status": "successful"}
                break
            except Exception as _e:
                logger.debug("API server is not yet ready. Retry in 1 sec...")
                time.sleep(1)

    logger.warning("api app startup finished")

    yield

    logger.warning("api app shutdown started...")
    # we need to unset the multiprocessing.Event that indicates that the server finished starting up, because this
    # fixture has scope "module" and therefore if there are multiple test modules running within the same test session
    # the second test module will start a new server process and needs to wait for it to finish its startup:
    startup_finished_event.clear()

    # proc.terminate()
    await server.shutdown()
    logger.warning("api app shutdown finished...")


@pytest_asyncio.fixture(scope="module")
async def app_fixture(api_server: AsyncGenerator[Any, Any]) -> AppFixtureReturn:
    _ = api_server
    await app_startup(fastapi_app_legacy, schema="test_pg")

    logger.warning("yield legacy_api...")
    yield fastapi_app_legacy

    logger.warning("start shutdown of legacy_api")
    await app_shutdown()
    logger.warning("finished shutdown of legacy_api")


async def put_json(api_client: ApiClient, *args: tuple, **kwargs: dict) -> dict:
    "Make http PUT request and return resulting JSON."
    api_response = await api_client.session.put(*args, **kwargs)
    assert api_response.status == 200
    response_json = await api_response.json()
    return response_json


async def fetch_json(api_client: ApiClient, *args: tuple, **kwargs: dict) -> dict:
    "Make http GET request and return resulting JSON."
    api_response = await api_client.session.get(*args, **kwargs)
    assert api_response.status == 200
    response_json = await api_response.json()
    return response_json


async def post_json(api_client: ApiClient, *args: tuple, **kwargs: dict) -> dict:
    "Make http POST request and return resulting JSON."
    api_response = await api_client.post(*args, **kwargs)
    response_json = await api_response.json()
    return response_json


def check_response_dict(
    request_dict: dict,
    response_dict: dict,
    node_type: str,
    expected_co2_value: Optional[int] = None,
    expected_co2_rating: Optional[str] = None,
    expect_eaternity_award_field: Optional[bool] = True,
    expected_co2_improvement_percentage: Optional[int] = None,
    expected_co2_reduction_value: Optional[int] = None,
    expected_daily_food_unit: Optional[float] = None,
    expected_indicators: Optional[dict[str, bool | str | float]] = None,
    expected_ingredients_co2_value: Optional[list[int]] = None,
    expected_ingredients_co2_rating: Optional[list[str]] = None,
    expected_ingredients_co2_improvement_percentage: Optional[list[int]] = None,
    expected_ingredients_co2_reduction_value: Optional[list[int]] = None,
    expected_ingredients_bar_chart: Optional[list[int]] = None,
    expected_ingredients_food_unit: Optional[list[float]] = None,
    expected_ingredients_indicators: Optional[list[dict[str, bool | str | float]]] = None,
    response_as_request: Optional[bool] = False,
    allow_dict_diff_to_be_empty: Optional[bool] = True,
    expected_final_weight_per_portion: Optional[bool | float] = True,
) -> None:
    """Helper function to check that response-dict is as expected."""
    if request_dict[node_type].get("unit") and request_dict[node_type]["unit"].lower() == "kilogram":
        request_dict[node_type]["amount"] *= 1000
        request_dict[node_type]["unit"] = "gram"

    for ingredient in request_dict[node_type].get("ingredients", []):
        for prop, default_response in INGREDIENT_PROP_DEFAULTS.items():
            if not ingredient.get(prop):
                ingredient[prop] = default_response
        if ingredient.get("unit") and ingredient["unit"].lower() == "kilogram":
            ingredient["amount"] *= 1000
            ingredient["unit"] = "gram"

        if not ingredient.get("unit"):
            ingredient["unit"] = "gram"

    dict_diff = DeepDiff(
        request_dict,
        response_dict,
        verbose_level=2,
        ignore_numeric_type_changes=True,  # to ignore int/float type changes
        significant_digits=6,
    )
    if response_as_request:
        assert list(dict_diff.keys()) == ["values_changed"]
        changed_values = {
            key: value["new_value"] for key, value in dict_diff["values_changed"].items() if value is not None
        }
        expected_changed_values = {f"root['{node_type}']['co2-value']": expected_co2_value}
        if expected_co2_rating is not None:
            expected_changed_values[f"root['{node_type}']['rating']"] = expected_co2_rating
            if expect_eaternity_award_field is not None:
                expected_changed_values[f"root['{node_type}']['eaternity-award']"] = expected_co2_rating == "A"
        if expected_co2_improvement_percentage is not None:
            expected_changed_values[
                f"root['{node_type}']['co2-value-improvement-percentage']"
            ] = expected_co2_improvement_percentage
        if expected_co2_reduction_value is not None:
            expected_changed_values[f"root['{node_type}']['co2-value-reduction-value']"] = expected_co2_reduction_value

        for ing_index, co2_val in enumerate(expected_ingredients_co2_value):
            if co2_val is not None:
                expected_changed_values[f"root['{node_type}']['ingredients'][{ing_index}]['co2-value']"] = co2_val
        if expected_ingredients_co2_rating is not None:
            for ing_index, co2_rating in enumerate(expected_ingredients_co2_rating):
                if co2_rating is not None:
                    expected_changed_values[f"root['{node_type}']['ingredients'][{ing_index}]['rating']"] = co2_rating
        if expected_ingredients_co2_improvement_percentage is not None:
            for ing_index, co2_improvement_percentage in enumerate(expected_ingredients_co2_improvement_percentage):
                if co2_improvement_percentage is not None:
                    expected_changed_values[
                        f"root['{node_type}']['ingredients'][{ing_index}]['co2-value-improvement-percentage']"
                    ] = co2_improvement_percentage
        if expected_ingredients_co2_reduction_value is not None:
            for ing_index, co2_reduction_value in enumerate(expected_ingredients_co2_reduction_value):
                if co2_reduction_value is not None:
                    expected_changed_values[
                        f"root['{node_type}']['ingredients'][{ing_index}]['co2-value-reduction-value']"
                    ] = co2_reduction_value
        for ing_index, bar_chart_val in enumerate(expected_ingredients_bar_chart):
            if bar_chart_val is not None:
                expected_changed_values[f"root['{node_type}']['ingredients'][{ing_index}]['bar-chart']"] = bar_chart_val

        assert changed_values == expected_changed_values

    else:
        if (not dict_diff) and allow_dict_diff_to_be_empty:
            return

        if list(dict_diff.keys()) != ["dictionary_item_added"]:
            raise AssertionError(f"should only have dictionary_item_added. But got dict_diff: {dict_diff.pretty()}")

        not_none_items_added = {
            key: value for key, value in dict_diff["dictionary_item_added"].items() if value is not None
        }
        expected_not_none_items_added = {}

        if expected_co2_value is not None:
            expected_not_none_items_added[f"root['{node_type}']['co2-value']"] = expected_co2_value
        if expected_co2_rating is not None:
            expected_not_none_items_added[f"root['{node_type}']['rating']"] = expected_co2_rating
            if expect_eaternity_award_field:
                expected_not_none_items_added[f"root['{node_type}']['eaternity-award']"] = expected_co2_rating == "A"
        if expected_co2_improvement_percentage is not None:
            expected_not_none_items_added[
                f"root['{node_type}']['co2-value-improvement-percentage']"
            ] = expected_co2_improvement_percentage
        if expected_co2_reduction_value is not None:
            expected_not_none_items_added[
                f"root['{node_type}']['co2-value-reduction-value']"
            ] = expected_co2_reduction_value
        if expected_daily_food_unit is not None:
            expected_food_unit = 5 * expected_daily_food_unit
            expected_not_none_items_added[f"root['{node_type}']['food-unit']"] = round(expected_food_unit, 2)

        if expected_indicators:
            expected_indicators_dict = {"environment": {}}
            for indicator_name, indicator_val in expected_indicators.items():
                if indicator_name == "vita-score":
                    expected_indicators_dict[indicator_name] = indicator_val
                else:
                    expected_indicators_dict["environment"][indicator_name] = indicator_val
            expected_not_none_items_added[f"root['{node_type}']['indicators']"] = expected_indicators_dict

        if expected_ingredients_co2_value:
            for ing_index, co2_val in enumerate(expected_ingredients_co2_value):
                if co2_val is not None:
                    expected_not_none_items_added[
                        f"root['{node_type}']['ingredients'][{ing_index}]['co2-value']"
                    ] = co2_val

        if expected_ingredients_co2_rating:
            for ing_index, co2_rating in enumerate(expected_ingredients_co2_rating):
                if co2_rating is not None:
                    expected_not_none_items_added[
                        f"root['{node_type}']['ingredients'][{ing_index}]['rating']"
                    ] = co2_rating

        if expected_ingredients_co2_improvement_percentage is not None:
            for ing_index, co2_improvement_percentage in enumerate(expected_ingredients_co2_improvement_percentage):
                if co2_improvement_percentage is not None:
                    expected_not_none_items_added[
                        f"root['{node_type}']['ingredients'][{ing_index}]['co2-value-improvement-percentage']"
                    ] = co2_improvement_percentage

        if expected_ingredients_co2_reduction_value is not None:
            for ing_index, co2_reduction_value in enumerate(expected_ingredients_co2_reduction_value):
                if co2_reduction_value is not None:
                    expected_not_none_items_added[
                        f"root['{node_type}']['ingredients'][{ing_index}]['co2-value-reduction-value']"
                    ] = co2_reduction_value

        if expected_ingredients_bar_chart:
            for ing_index, bc_val in enumerate(expected_ingredients_bar_chart):
                if bc_val is not None:
                    expected_not_none_items_added[
                        f"root['{node_type}']['ingredients'][{ing_index}]['bar-chart']"
                    ] = bc_val
        if expected_ingredients_food_unit:
            for ing_index, dfu_val in enumerate(expected_ingredients_food_unit):
                if dfu_val is not None:
                    expected_not_none_items_added[
                        f"root['{node_type}']['ingredients'][{ing_index}]['foodUnit']"
                    ] = round(dfu_val * 5, 2)
        if expected_ingredients_indicators:
            for ing_index, indicators_dict in enumerate(expected_ingredients_indicators):
                expected_ingredient_indicators_dict = {"environment": {}}
                for indicator_name, indicator_val in indicators_dict.items():
                    if indicator_name == "vita-score":
                        expected_ingredient_indicators_dict[indicator_name] = indicator_val
                    else:
                        expected_ingredient_indicators_dict["environment"][indicator_name] = indicator_val
                expected_not_none_items_added[
                    f"root['{node_type}']['ingredients'][{ing_index}]['indicators']"
                ] = expected_ingredient_indicators_dict

        if expected_final_weight_per_portion is not None:
            if isinstance(expected_final_weight_per_portion, bool):
                # we just expect it to be there but do not check the value:
                expected_not_none_items_added[
                    f"root['{node_type}']['final-weight-per-portion']"
                ] = not_none_items_added[f"root['{node_type}']['final-weight-per-portion']"]
            else:
                expected_not_none_items_added[
                    f"root['{node_type}']['final-weight-per-portion']"
                ] = expected_final_weight_per_portion

        assert not_none_items_added == expected_not_none_items_added


@pytest_asyncio.fixture
async def create_simple_customer(app_fixture: AppFixtureReturn) -> tuple[str, str, bool]:
    _ = app_fixture
    customer_namespace_uid = "e9e527d7-8574-452e-8c74-eb7927eaddb5"
    customer_namespace_xid = "simple_customer_xid"
    response_json = await put_json(
        api_client,
        f"{legacy_api_settings.api_v2_url}/batch/customers/{customer_namespace_xid}",
        headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        json={
            "customer": {
                "name": "test customer",
                "uid": customer_namespace_uid,
            }
        },
    )
    customer_token = response_json.get("auth_token")
    status = response_json.get("status")
    return customer_namespace_uid, customer_token, status


@pytest_asyncio.fixture
async def load_tomato_and_onion_nutrients() -> dict[str, dict[str, float]]:
    service_provider = ServiceLocator().service_provider
    foodex2_access_group_uid = await service_provider.namespace_service.find_default_access_group_by_ns_xid("foodex2")

    nutrients_to_load = {
        "onion": "A1480",
        "tomato": "A0DMX",
    }

    nutrients_eos = {}
    for term_name, xid in nutrients_to_load.items():
        term = await service_provider.glossary_service.get_term_by_xid_and_access_group_uid(
            xid, str(foodex2_access_group_uid)
        )
        nutrients_term = await service_provider.glossary_link_service.load_term_nutrients([term])
        nutrients_eos[term_name] = NutrientValueDto(**transform_eurofir(nutrients_term.data["nutr-vals"])).model_dump(
            exclude_none=True, by_alias=True
        )

    nutrients_eos["dried_onion"] = {key: val * ONION_DRYING_UPSCALING for key, val in nutrients_eos["onion"].items()}
    nutrients_eos["dried_onion"]["water-gram"] = 10
    return nutrients_eos


@pytest_asyncio.fixture
async def seed_processing_data() -> None:
    # seed processing-data
    service_provider = ServiceLocator().service_provider
    postgres_db = service_provider.postgres_db
    await seed_processing(postgres_db, service_provider)

    processing_factory = [
        mod
        for mod in service_provider.gap_filling_module_loader.initialized_modules
        if isinstance(mod, ProcessingGapFillingFactory)
    ][0]
    await processing_factory.init_cache()


@pytest_asyncio.fixture
async def create_simple_customer_and_kitchen(
    app_fixture: AppFixtureReturn, create_simple_customer: tuple[str, str, bool]
) -> tuple[str, str, str]:
    # Step 1: Create a customer (use fixture):
    customer_namespace_uid, customer_token, status = create_simple_customer

    # Step 2: Create a kitchen under this customer:
    kitchen_xid = "kitchen01"
    async with AsyncClient(app=app_fixture, base_url="http://localhost:8050") as ac:
        await ac.put(
            f"/api/kitchens/{kitchen_xid}",
            auth=(customer_token, ""),
            json={
                "kitchen": {
                    "name": "test kitchen",
                    "location": "Zürich Schweiz",
                },
            },
        )
    return customer_token, customer_namespace_uid, kitchen_xid


@pytest_asyncio.fixture(scope="module")
async def create_customer(api_server: AsyncGenerator[Any, Any]) -> None:
    _ = api_server
    logger.debug("start create_customer")

    response_json = await put_json(
        api_client,
        f"{legacy_api_settings.api_v2_url}/batch/customers/{CUSTOMER.get('namespace_xid')}",
        headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        json={
            "customer": {
                "name": f"{CUSTOMER.get('name')}",
                "uid": CUSTOMER.get("namespace_uid"),
            }
        },
    )

    assert response_json.get("status") is True
    # even though it's not currently used in any legacy API tests,
    # let it stay here for potential future purposes
    pytest.ENCODED_CUSTOMER_TOKEN = response_json.get("auth_token")

    response_json = await put_json(
        api_client,
        f"{legacy_api_settings.api_v2_url}/users/{USER_SUPER_ID}/",
        headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        json={"user": USER_SUPER},
    )

    assert response_json.get("status") is True
    auth_token = response_json.get("auth_token")
    pytest.ENCODED_USER_SUPER_TOKEN = base64.b64encode(auth_token.encode("utf-8")).decode("utf-8")

    response_json = await put_json(
        api_client,
        f"{legacy_api_settings.api_v2_url}/users/{USER_ADMIN_ID}/",
        headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        json={"user": USER_ADMIN},
    )

    assert response_json.get("status") is True
    auth_token = response_json.get("auth_token")
    pytest.ENCODED_USER_ADMIN_TOKEN = base64.b64encode(auth_token.encode("utf-8")).decode("utf-8")

    response_json = await put_json(
        api_client,
        f"{legacy_api_settings.api_v2_url}/users/{USER_BASIC_ID}/",
        headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        json={"user": USER_BASIC},
    )

    assert response_json.get("status") is True
    auth_token = response_json.get("auth_token")
    pytest.ENCODED_USER_BASIC_TOKEN = base64.b64encode(auth_token.encode("utf-8")).decode("utf-8")

    # testing that everything's fine with auth token generation, just in case
    response_json = await fetch_json(
        api_client,
        f"{legacy_api_settings.api_v2_url}/auth/validate_token/",
        headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
    )
    assert response_json == {"status": "authenticated"}

    response_json = await fetch_json(
        api_client,
        f"{legacy_api_settings.api_v2_url}/auth/validate_token/",
        headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
    )
    assert response_json == {"status": "authenticated"}

    response_json = await fetch_json(
        api_client,
        f"{legacy_api_settings.api_v2_url}/auth/validate_token/",
        headers={"Authorization": f"Basic {pytest.ENCODED_USER_BASIC_TOKEN}"},
    )
    assert response_json == {"status": "authenticated"}


@pytest_asyncio.fixture(scope="module")
async def create_and_get_and_delete_and_create_kitchen_by_superuser(
    app_fixture: AppFixtureReturn, create_customer: None
) -> None:
    _ = create_customer
    logger.info("start create_and_get_and_delete_and_create_kitchen_by_superuser")
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/kitchens",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "kitchen": TEST_KITCHEN_SUPERUSER,
            },
        )
        assert response.status_code == 406

        # create a wrapper access group via normal API,
        # so that superuser has a parent default access group
        response_json = await post_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "access_group": TEST_KITCHEN_SUPERUSER_PARENT,
                "namespace_uid": CUSTOMER.get("namespace_uid"),
            },
        )
        assert response_json.get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_SUPERUSER_PARENT["uid"] = response_json.get("access_group", {}).get("uid")
        pytest.TEST_KITCHEN_SUPERUSER_PARENT_UID = response_json.get("access_group", {}).get("uid")

        # add this access group as a default one for superuser
        response_json = await put_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "user": USER_SUPER,
                "legacy_api_default_access_group_uid": pytest.TEST_KITCHEN_SUPERUSER_PARENT_UID,
            },
        )
        assert response_json.get("legacy_default_access_group_id") == pytest.TEST_KITCHEN_SUPERUSER_PARENT_UID
        assert response_json.get("status") is True

        # post the kitchen via legacy API
        response = await ac.post(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "kitchen": TEST_KITCHEN_SUPERUSER,
            },
        )
        assert response.status_code == 201
        data = response.json()
        assert data is not None
        assert data.get("kitchen") is not None

        # get this group's UUID via normal API
        response_json = await post_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_SUPERUSER_ID},
            },
        )
        assert response_json.get("access_group", {}).get("uid") is not None
        pytest.TEST_KITCHEN_SUPERUSER_UID = response_json.get("access_group", {}).get("uid")

        # add this access group as a default one for superuser
        response_json = await put_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "user": USER_SUPER,
                "legacy_api_default_access_group_uid": pytest.TEST_KITCHEN_SUPERUSER_UID,
            },
        )
        assert response_json.get("legacy_default_access_group_id") == pytest.TEST_KITCHEN_SUPERUSER_UID
        assert response_json.get("status") is True

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        data = response.json()
        assert data.get("kitchen").get("name") == TEST_KITCHEN_SUPERUSER.get("name")
        assert data.get("kitchen").get("location") == TEST_KITCHEN_SUPERUSER.get("location")

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404
        assert response.text == "Kitchen not found"

        response = await ac.post(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "kitchen": TEST_KITCHEN_SUPERUSER,
            },
        )
        assert response.status_code == 406

        # add supplementary access group as a default one for superuser
        response_json = await put_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "user": USER_SUPER,
                "legacy_api_default_access_group_uid": pytest.TEST_KITCHEN_SUPERUSER_PARENT_UID,
            },
        )
        assert response_json.get("legacy_default_access_group_id") == pytest.TEST_KITCHEN_SUPERUSER_PARENT_UID
        assert response_json.get("status") is True

        response = await ac.post(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "kitchen": TEST_KITCHEN_SUPERUSER,
            },
        )
        assert response.status_code == 201
        data = response.json()
        assert data is not None
        assert data.get("kitchen") is not None

        # get this group's UUID via normal API
        response_json = await post_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_SUPERUSER_ID},
            },
        )
        assert response_json.get("access_group", {}).get("uid") is not None
        pytest.TEST_KITCHEN_SUPERUSER_UID = response_json.get("access_group", {}).get("uid")

        # add this access group as a default one for superuser
        response_json = await put_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "user": USER_SUPER,
                "legacy_api_default_access_group_uid": pytest.TEST_KITCHEN_SUPERUSER_UID,
            },
        )
        assert response_json.get("legacy_default_access_group_id") == pytest.TEST_KITCHEN_SUPERUSER_UID
        assert response_json.get("status") is True

        await ac.aclose()
    logger.info("finish create_and_get_and_delete_and_create_kitchen_by_superuser")


@pytest_asyncio.fixture(scope="module")
async def create_and_get_and_delete_and_create_kitchen_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: None,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # this request has to be sent via `requests` library
        # just like any other request to the regular API service
        await post_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/access-groups/upsert_members",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json={
                "members": [
                    {
                        "user_id": USER_ADMIN_ID,
                        "permissions": {
                            UserPermissionsEnum.create_access_group: True,
                        },
                    }
                ],
                "access_group": {"xid": TEST_KITCHEN_SUPERUSER_ID},
                "namespace_uid": CUSTOMER.get("namespace_uid"),
            },
        )

        # add this access group as a default one for superuser
        response_json = await put_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": pytest.TEST_KITCHEN_SUPERUSER_UID,
            },
        )
        assert response_json.get("legacy_default_access_group_id") == pytest.TEST_KITCHEN_SUPERUSER_UID
        assert response_json.get("status") is True

        response = await ac.post(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json={"kitchen": TEST_KITCHEN_ADMIN},
        )
        assert response.status_code == 201, "a new kitchen should have been created"
        data = response.json()
        assert data is not None
        assert data.get("kitchen") is not None

        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json={"kitchen": TEST_KITCHEN_ADMIN},
        )
        assert response.status_code == 200, "kitchen should have been updated"

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        data = response.json()
        assert data.get("kitchen").get("name") == TEST_KITCHEN_ADMIN.get("name")
        assert data.get("kitchen").get("location") == TEST_KITCHEN_ADMIN.get("location")

        response = await ac.delete(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/kitchens/{TEST_KITCHEN_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404
        assert response.text == "Kitchen not found"

        response = await ac.post(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json={"kitchen": TEST_KITCHEN_ADMIN},
        )
        assert response.status_code == 201, "a new kitchen should have been created"
        data = response.json()
        assert data is not None
        assert data.get("kitchen") is not None

        # get this group's UUID via normal API
        response_json = await post_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_ADMIN_ID},
            },
        )
        assert response_json.get("access_group", {}).get("uid") is not None
        pytest.TEST_KITCHEN_ADMIN_UID = response_json.get("access_group", {}).get("uid")

        # add this access group as a default one for superuser
        response_json = await put_json(
            api_client,
            f"{legacy_api_settings.api_v2_url}/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": pytest.TEST_KITCHEN_ADMIN_UID,
            },
        )
        assert response_json.get("legacy_default_access_group_id") == pytest.TEST_KITCHEN_ADMIN_UID
        assert response_json.get("status") is True

        response = await ac.post(
            "/api/kitchens/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json={"kitchen": TEST_KITCHEN_ADMIN},
        )
        assert response.status_code == 201, "a new kitchen should have been created"
        data = response.json()
        assert data is not None
        assert data.get("kitchen") is not None

        await ac.aclose()


@pytest_asyncio.fixture(scope="module")
async def create_kitchen_recipe_with_subrecipe_by_superuser(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # first insert the subrecipe, so that we can actually use it afterwards:
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        # now add the outer recipe that contains the above subrecipe
        response = await ac.put(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE_WITH_SUBRECIPE_2_SUPERUSER,
        )
        assert response.status_code == 200


@pytest_asyncio.fixture(scope="module")
async def reset_db() -> None:
    await postgres_db.connect(schema="test_pg")
    await postgres_db.reset_db(schema="test_pg")


ECOTRANSIT_RESPONSES_FOLDER = "ecotransit_responses"
SAMPLE_JSONS_DIR = os.path.join(
    os.path.dirname(__file__),
    "..",
    "..",
    "..",
    "core",
    "core",
    "tests",
    "sample_jsons",
)


@pytest.fixture(scope="package", autouse=True)
def geolocation_api_mock(request: SubRequest) -> None:
    """Mocks the Google geolocation API to return a fixed result."""

    def mocked_requests_get(url: str) -> Response:
        url_to_check = url.lower()

        if (
            "test_admin_location" in url_to_check
            or "test_location" in url_to_check
            or "schweiz" in url_to_check
            or "switzerland" in url_to_check
            or "paris" in url_to_check
        ):
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "zurich_google_maps_query_response.json",
            )
        elif "spain" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "spain_google_maps_query_response.json",
            )
        elif "nonexistingcountry" in url_to_check:
            not_found = {
                "error": {
                    "code": 404,
                    "message": "Not Found",
                    "errors": [{"message": "Not Found", "domain": "geolocation", "reason": "notFound"}],
                }
            }
            return Response(status_code=404, content=json.dumps(not_found).encode("utf-8"))
        else:
            raise NotImplementedError(f"Mocking not implemented for {url}")

        with open(json_sample_path) as response_json_sample_file:
            resp_json = json.load(response_json_sample_file)

        mock_response = Response(status_code=200, content=json.dumps(resp_json).encode("utf-8"))

        return mock_response

    print("Patching 'gap_filling_modules.location_gfm._google_api_lookup'")
    patched = mock.patch("gap_filling_modules.location_gfm._google_api_lookup", new=mocked_requests_get)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'gap_filling_modules.location_gfm._google_api_lookup'")

    request.addfinalizer(unpatch)


XML_FILES_MAPPING = {
    "wsdl": "wsdl.xml",
    "wsdl=1": "wsdl_1.xml",
    "xsd=1": "xsd_1.xml",
    "xsd=2": "xsd_2.xml",
}


@pytest.fixture(scope="package", autouse=True)
def ecotransit_wsdl_call_mock(request: SubRequest) -> None:
    """Mocks the EcoTransIT WSDL call to return a fixed XML schema file."""

    def mocked_wsdls_get(self: None, url: str) -> bytes:
        _ = self
        url_to_check = url.lower()
        schema_file_type = url_to_check.split("?")[-1]

        if schema_file_type not in XML_FILES_MAPPING:
            raise NotImplementedError(f"WSDL load mocking not implemented for {url}")

        xml_sample_path = os.path.join(
            SAMPLE_JSONS_DIR,
            XML_FILES_MAPPING[schema_file_type],
        )

        with open(xml_sample_path, "r") as response_xml_sample_file:
            mock_xml = bytes(response_xml_sample_file.read().encode("utf-8"))

        return mock_xml

    print("Patching 'zeep.transports.Transport._load_remote_data'")
    patched = mock.patch("zeep.transports.Transport._load_remote_data", new=mocked_wsdls_get)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'zeep.transports.Transport._load_remote_data'")

    request.addfinalizer(unpatch)


@pytest.fixture(scope="package", autouse=True)
def ecotransit_call_mock(request: SubRequest) -> None:
    """Mocks the EcoTransIT API call to return a fixed result in XML format."""

    def mocked_ecotransit_send_request(self: None, address: str, message: bytes, headers: dict) -> Response:
        _ = self
        _ = address
        _ = headers
        departure = {}
        destination = {}
        mode = ""

        parsed_message = etree.fromstring(message)
        root = parsed_message.getroottree().getroot()

        for stuff in root.iter():
            if "wgs84" in stuff.tag:
                if stuff.prefix == "ns1":
                    # departure coords
                    departure = dict(stuff.attrib)
                elif stuff.prefix == "ns2":
                    # destination coords
                    destination = dict(stuff.attrib)

            elif stuff.prefix == "ns3":
                if "air" in stuff.tag:
                    mode = "air"
                elif "sea" in stuff.tag:
                    mode = "sea"
                elif "road" in stuff.tag:
                    mode = "road"

            if mode:
                break

        sample_response_xml_file = (
            f"{departure.get('longitude')}_{departure.get('latitude')}_"
            f"{destination.get('longitude')}_{destination.get('latitude')}_{mode}.xml"
        )
        xml_sample_path = os.path.join(
            SAMPLE_JSONS_DIR,
            ECOTRANSIT_RESPONSES_FOLDER,
            sample_response_xml_file,
        )

        if not os.path.isfile(xml_sample_path):
            print(f"WARNING: {xml_sample_path} does not exist. Return 404 status.")
            return Response(status_code=404)

        with open(xml_sample_path, "r") as response_xml_sample_file:
            mock_response = Response(status_code=200, content=bytes(response_xml_sample_file.read().encode("utf-8")))

        return mock_response

    print("Patching 'zeep.transports.Transport.post'")
    patched = mock.patch("zeep.transports.Transport.post", new=mocked_ecotransit_send_request)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'zeep.transports.Transport.post'")

    request.addfinalizer(unpatch)


@pytest.fixture(scope="package", autouse=True)
def virtual_memory_lock() -> None:
    """Mocks virtual memory such that we do not accidentally exit tests because of lack of memory."""
    with mock.patch("psutil.virtual_memory") as mock_vm:
        mock_vm.return_value.percent = 30
        yield mock_vm
