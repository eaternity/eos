import copy
import csv
from io import StringIO

import deepdiff
import pytest
from httpx import AsyncClient

from legacy_api.tests.test_legacy_supply_router import TEST_SUPPLY

from .conftest import (
    TEST_KITCHEN_SUPERUSER_ID,
    TEST_RECIPE_SUPERUSER_ID,
    TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID,
    AppFixtureReturn,
)

# TODO: infer the values from the other benchmarks that we already have?
EXPECTED_SINGLE_SUBRECIPE_CSV = [
    {
        "ID": "test_recipe_superuser_legacy_id",
        "Title": "Kürbisrisotto",
        "Instructions": "TODO",
        "Create Date": "TODO",
        "Production Date": "2023-06-20",
        "Kitchen Id": "test_kitchen_superuser_legacy_id",
        "Kitchen Name": "TODO",
        "Weight [gram/serving]": "1628.571",
        "CO2-Value [gram CO2/serving]": "1381",
        "Energy [kJ/serving]": "8383.63",
        "Portions per Recipe": "140",
        "Recipe Portions planned or produced": "",
        "Recipe Portions sold": "",
        "Animal Welfare Label": "True",
        "Rainforest Label": "False",
        "Water Scarcity Footprint [liter/serving]": "21.703",
        "Water Scarcity Rating": "A",
        "Vita score [Serving]": "363.879",
        "Vita Score Rating": "C",
        "Fruit Risk Factor Amount [g/serving]": "TODO",
        "Fruit Risk Points [points/serving]": "61.24",
        "Vegetable Risk Factor Amount [g/serving]": "TODO",
        "Vegetable Risk Points [points/serving]": "0.0",
        "Wholegrain Risk Factor Amount [g/serving]": "TODO",
        "Wholegrain Risk Points [points/serving]": "82.86",
        "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
        "Nuts Seeds Risk Points [points/serving]": "13.98",
        "Milk Risk Factor Amount [g/serving]": "TODO",
        "Milk Risk Points [points/serving]": "9.42",
        "Processed Meat Risk Factor Amount [g/serving]": "TODO",
        "Processed Meat Risk Points [points/serving]": "0.0",
        "Red Meat Risk Factor Amount [g/serving]": "TODO",
        "Red Meat Risk Points [points/serving]": "0.0",
        "Salt Risk Factor Amount [g/serving]": "TODO",
        "Salt Risk Points [points/serving]": "0.0",
        "Eaternity Award": "True",
        "Vegetarian/Vegan": "TODO",
        "Categorization Tags": "TODO",
        "Base Product Tags": (
            "Diet high in energy,Diet high in fat,Diet high in protein,Diet high in sodium (salt),"
            "Diet low in fat,Diet low in protein,Diet low in vegetables,Plant based products,Vegetables"
        ),
        "Food Unit": "2.527",
        "CO2 FU Rating": "A",
        "CO2 Improvement Percentage": "56.115",
        "Menu Line Name": "TODO",
        "Menu Line ID": "TODO",
        "Palm oil Amount (critical)": "TODO",
        "Palm oil Amount (not critical)": "TODO",
        "Soy Amount (critical)": "TODO",
        "Soy Amount (not critical)": "TODO",
        "Animal Welfare Rating": "A",
        "Rainforest Rating": "E",
        "Water Scarcity Reduction Value": "126.36",
        "Water Scarcity Improvement Percentage": "85.342",
        "Vita Score Improvement Percentage": "-7.023",
    }
]

EXPECTED_SINGLE_SUBRECIPE_WITH_INGREDIENTS_CSV = [
    {
        "ID": "test_recipe_superuser_legacy_id",
        "Title": "Kürbisrisotto",
        "Location": "TODO",
        "Kitchen Name": "TODO",
        "Production Date": "2023-06-20",
        "Index": "0",
        "Weight [g]": "150000.0",
        "Name": "Tomaten",
        "Type": "TODO",
        "Product ID": "100199191",
        "Production": "TODO",
        "Origin": "",
        "Transport": "",
        "g CO2": "0.0",
        "CO2 Base Value [g]": "0.0",
        "CO2 Greenhouse [g]": "0.0",
        "CO2 Organic [g]": "0.0",
        "CO2 Frozen Conservation [g]": "0.0",
        "CO2 Land Transport [g]": "0.0",
        "CO2 Sea Transport [g]": "0.0",
        "CO2 Air Transport [g]": "0.0",
        "Energy [kJ]": "133496.796",
        "Food Unit": "118.673",
        "CO2 Improvement Percentage": "100.0",
        "CO2 Reduction Value": "92.41",
        "Water Scarcity Footprint [liter]": "1444.156",
        "Total Soy Amount [g/serving] (critical)": "TODO",
        "Total Soy Amount [g/serving] (not critical)": "TODO",
        "Total Palmoil Amount [g/serving] not certified": "TODO",
        "Total Palmoil Amount [g/serving] certified": "TODO",
        "Tags": "TODO",
        "meat origins": "TODO",
        "Animal Welfare Rating": "A",
        "Rainforest Rating": "A",
        "Water Scarcity Rating": "A",
        "CO2 FU Rating": "A",
        "Fruit Risk Factor Amount [g/serving]": "TODO",
        "Fruit Risk Points [points/serving]": "61.24",
        "Vegetable Risk Factor Amount [g/serving]": "TODO",
        "Vegetable Risk Points [points/serving]": "0.0",
        "Wholegrain Risk Factor Amount [g/serving]": "TODO",
        "Wholegrain Risk Points [points/serving]": "82.86",
        "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
        "Nuts Seeds Risk Points [points/serving]": "13.98",
        "Milk Risk Factor Amount [g/serving]": "TODO",
        "Milk Risk Points [points/serving]": "9.42",
        "Processed Meat Risk Factor Amount [g/serving]": "TODO",
        "Processed Meat Risk Points [points/serving]": "0.0",
        "Red Meat Risk Factor Amount [g/serving]": "TODO",
        "Red Meat Risk Points [points/serving]": "0.0",
        "Salt Risk Factor Amount [g/serving]": "TODO",
        "Salt Risk Points [points/serving]": "0.0",
        "Vita Score Improvement Percentage": "2.314",
        "Vita score [Serving]": "243.473",
        "Vita Score Rating": "B",
        "Water Scarcity Improvement Percentage": "66.769",
        "Portions per Recipe": "140",
        "Recipe Portions planned or produced": "",
        "Recipe Portions sold": "",
        "Water Scarcity Reduction Value": "2901.636",
    },
    {
        "ID": "test_recipe_superuser_legacy_id",
        "Title": "Kürbisrisotto",
        "Location": "TODO",
        "Kitchen Name": "TODO",
        "Production Date": "2023-06-20",
        "Index": "1",
        "Weight [g]": "78000.0",
        "Name": "Zwiebeln",
        "Type": "TODO",
        "Product ID": "100199894",
        "Production": "TODO",
        "Origin": "",
        "Transport": "",
        "g CO2": "189540.0",
        "CO2 Base Value [g]": "189540.0",
        "CO2 Greenhouse [g]": "0.0",
        "CO2 Organic [g]": "0.0",
        "CO2 Frozen Conservation [g]": "0.0",
        "CO2 Land Transport [g]": "0.0",
        "CO2 Sea Transport [g]": "0.0",
        "CO2 Air Transport [g]": "0.0",
        "Energy [kJ]": "1040211.398",
        "Food Unit": "447.377",
        "CO2 Improvement Percentage": "69.687",
        "CO2 Reduction Value": "3.112",
        "Water Scarcity Footprint [liter]": "1594.227",
        "Total Soy Amount [g/serving] (critical)": "TODO",
        "Total Soy Amount [g/serving] (not critical)": "TODO",
        "Total Palmoil Amount [g/serving] not certified": "TODO",
        "Total Palmoil Amount [g/serving] certified": "TODO",
        "Tags": "TODO",
        "meat origins": "TODO",
        "Animal Welfare Rating": "A",
        "Rainforest Rating": "E",
        "Water Scarcity Rating": "A",
        "CO2 FU Rating": "A",
        "Fruit Risk Factor Amount [g/serving]": "TODO",
        "Fruit Risk Points [points/serving]": "61.24",
        "Vegetable Risk Factor Amount [g/serving]": "TODO",
        "Vegetable Risk Points [points/serving]": "0.0",
        "Wholegrain Risk Factor Amount [g/serving]": "TODO",
        "Wholegrain Risk Points [points/serving]": "82.86",
        "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
        "Nuts Seeds Risk Points [points/serving]": "13.98",
        "Milk Risk Factor Amount [g/serving]": "TODO",
        "Milk Risk Points [points/serving]": "9.42",
        "Processed Meat Risk Factor Amount [g/serving]": "TODO",
        "Processed Meat Risk Points [points/serving]": "0.0",
        "Red Meat Risk Factor Amount [g/serving]": "TODO",
        "Red Meat Risk Points [points/serving]": "0.0",
        "Salt Risk Factor Amount [g/serving]": "TODO",
        "Salt Risk Points [points/serving]": "0.0",
        "Vita Score Improvement Percentage": "-7.782",
        "Vita score [Serving]": "366.458",
        "Vita Score Rating": "C",
        "Water Scarcity Improvement Percentage": "90.269",
        "Portions per Recipe": "140",
        "Recipe Portions planned or produced": "",
        "Recipe Portions sold": "",
        "Water Scarcity Reduction Value": "14788.734",
    },
]

EXPECTED_SINGLE_RECIPE_WITH_SUBRECIPE_CSV = [
    {
        "ID": "test_sub_recipe_superuser_legacy_id",
        "Title": "Parent Recipe with child recipes",
        "Instructions": "TODO",
        "Create Date": "TODO",
        "Production Date": "2023-06-20",
        "Kitchen Id": "test_kitchen_superuser_legacy_id",
        "Kitchen Name": "TODO",
        "Weight [gram/serving]": "914.286",
        "CO2-Value [gram CO2/serving]": "345",
        "Energy [kJ/serving]": "9036.269",
        "Portions per Recipe": "140",
        "Recipe Portions planned or produced": "",
        "Recipe Portions sold": "",
        "Animal Welfare Label": "True",
        "Rainforest Label": "False",
        "Water Scarcity Footprint [liter/serving]": "82.624",
        "Water Scarcity Rating": "B",
        "Vita score [Serving]": "338.592",
        "Vita Score Rating": "B",
        "Fruit Risk Factor Amount [g/serving]": "TODO",
        "Fruit Risk Points [points/serving]": "61.24",
        "Vegetable Risk Factor Amount [g/serving]": "TODO",
        "Vegetable Risk Points [points/serving]": "0.0",
        "Wholegrain Risk Factor Amount [g/serving]": "TODO",
        "Wholegrain Risk Points [points/serving]": "82.86",
        "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
        "Nuts Seeds Risk Points [points/serving]": "13.98",
        "Milk Risk Factor Amount [g/serving]": "TODO",
        "Milk Risk Points [points/serving]": "9.42",
        "Processed Meat Risk Factor Amount [g/serving]": "TODO",
        "Processed Meat Risk Points [points/serving]": "0.0",
        "Red Meat Risk Factor Amount [g/serving]": "TODO",
        "Red Meat Risk Points [points/serving]": "0.0",
        "Salt Risk Factor Amount [g/serving]": "TODO",
        "Salt Risk Points [points/serving]": "0.0",
        "Eaternity Award": "True",
        "Vegetarian/Vegan": "TODO",
        "Categorization Tags": "TODO",
        "Base Product Tags": (
            "Diet high in energy,Diet high in fat,Diet high in protein,Diet high in sodium (salt),"
            "Diet low in fat,Diet low in protein,Diet low in vegetables,Plant based products,Vegetables"
        ),
        "Food Unit": "2.671",
        "CO2 FU Rating": "A",
        "CO2 Improvement Percentage": "89.613",
        "Menu Line Name": "TODO",
        "Menu Line ID": "TODO",
        "Palm oil Amount (critical)": "TODO",
        "Palm oil Amount (not critical)": "TODO",
        "Soy Amount (critical)": "TODO",
        "Soy Amount (not critical)": "TODO",
        "Animal Welfare Rating": "A",
        "Rainforest Rating": "E",
        "Water Scarcity Reduction Value": "73.853",
        "Water Scarcity Improvement Percentage": "47.197",
        "Vita Score Improvement Percentage": "0.414",
    }
]

EXPECTED_SINGLE_RECIPE_WITH_SUBRECIPE_WITH_INGREDIENTS_CSV = [
    {
        "ID": "test_sub_recipe_superuser_legacy_id",
        "Title": "Parent Recipe with child recipes",
        "Location": "TODO",
        "Kitchen Name": "TODO",
        "Production Date": "2023-06-20",
        "Index": "0",
        "Weight [g]": "50000.0",
        "Name": "Kürbisrisotto",
        "Type": "TODO",
        "Product ID": "test_recipe_superuser_legacy_id",
        "Production": "TODO",
        "Origin": "",
        "Transport": "",
        "g CO2": "44487.527",
        "CO2 Base Value [g]": "41565.789",
        "CO2 Greenhouse [g]": "0.0",
        "CO2 Organic [g]": "0.0",
        "CO2 Frozen Conservation [g]": "0.0",
        "CO2 Land Transport [g]": "2921.737",
        "CO2 Sea Transport [g]": "0.0",
        "CO2 Air Transport [g]": "0.0",
        "Energy [kJ]": "257392.148",
        "Food Unit": "124.134",
        "CO2 Improvement Percentage": "99.671",
        "CO2 Reduction Value": "96.345",
        "Water Scarcity Footprint [liter]": "666.312",
        "Total Soy Amount [g/serving] (critical)": "TODO",
        "Total Soy Amount [g/serving] (not critical)": "TODO",
        "Total Palmoil Amount [g/serving] not certified": "TODO",
        "Total Palmoil Amount [g/serving] certified": "TODO",
        "Tags": "TODO",
        "meat origins": "TODO",
        "Animal Welfare Rating": "A",
        "Rainforest Rating": "E",
        "Water Scarcity Rating": "A",
        "CO2 FU Rating": "A",
        "Fruit Risk Factor Amount [g/serving]": "TODO",
        "Fruit Risk Points [points/serving]": "61.24",
        "Vegetable Risk Factor Amount [g/serving]": "TODO",
        "Vegetable Risk Points [points/serving]": "0.0",
        "Wholegrain Risk Factor Amount [g/serving]": "TODO",
        "Wholegrain Risk Points [points/serving]": "82.86",
        "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
        "Nuts Seeds Risk Points [points/serving]": "13.98",
        "Milk Risk Factor Amount [g/serving]": "TODO",
        "Milk Risk Points [points/serving]": "9.42",
        "Processed Meat Risk Factor Amount [g/serving]": "TODO",
        "Processed Meat Risk Points [points/serving]": "0.0",
        "Red Meat Risk Factor Amount [g/serving]": "TODO",
        "Red Meat Risk Points [points/serving]": "0.0",
        "Salt Risk Factor Amount [g/serving]": "TODO",
        "Salt Risk Points [points/serving]": "0.0",
        "Vita Score Improvement Percentage": "-7.023",
        "Vita score [Serving]": "275.219",
        "Vita Score Rating": "C",
        "Water Scarcity Improvement Percentage": "85.342",
        "Portions per Recipe": "140",
        "Recipe Portions planned or produced": "",
        "Recipe Portions sold": "",
        "Water Scarcity Reduction Value": "3879.467",
    },
    {
        "ID": "test_sub_recipe_superuser_legacy_id",
        "Title": "Parent Recipe with child recipes",
        "Location": "TODO",
        "Kitchen Name": "TODO",
        "Production Date": "2023-06-20",
        "Index": "1",
        "Weight [g]": "78000.0",
        "Name": "Tomaten",
        "Type": "TODO",
        "Product ID": "242342343",
        "Production": "TODO",
        "Origin": "",
        "Transport": "",
        "g CO2": "0.0",
        "CO2 Base Value [g]": "0.0",
        "CO2 Greenhouse [g]": "0.0",
        "CO2 Organic [g]": "0.0",
        "CO2 Frozen Conservation [g]": "0.0",
        "CO2 Land Transport [g]": "0.0",
        "CO2 Sea Transport [g]": "0.0",
        "CO2 Air Transport [g]": "0.0",
        "Energy [kJ]": "1007685.492",
        "Food Unit": "474.084",
        "CO2 Improvement Percentage": "100.0",
        "CO2 Reduction Value": "4.733",
        "Water Scarcity Footprint [liter]": "10901.051",
        "Total Soy Amount [g/serving] (critical)": "TODO",
        "Total Soy Amount [g/serving] (not critical)": "TODO",
        "Total Palmoil Amount [g/serving] not certified": "TODO",
        "Total Palmoil Amount [g/serving] certified": "TODO",
        "Tags": "TODO",
        "meat origins": "TODO",
        "Animal Welfare Rating": "A",
        "Rainforest Rating": "A",
        "Water Scarcity Rating": "B",
        "CO2 FU Rating": "A",
        "Fruit Risk Factor Amount [g/serving]": "TODO",
        "Fruit Risk Points [points/serving]": "61.24",
        "Vegetable Risk Factor Amount [g/serving]": "TODO",
        "Vegetable Risk Points [points/serving]": "0.0",
        "Wholegrain Risk Factor Amount [g/serving]": "TODO",
        "Wholegrain Risk Points [points/serving]": "82.86",
        "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
        "Nuts Seeds Risk Points [points/serving]": "13.98",
        "Milk Risk Factor Amount [g/serving]": "TODO",
        "Milk Risk Points [points/serving]": "9.42",
        "Processed Meat Risk Factor Amount [g/serving]": "TODO",
        "Processed Meat Risk Points [points/serving]": "0.0",
        "Red Meat Risk Factor Amount [g/serving]": "TODO",
        "Red Meat Risk Points [points/serving]": "0.0",
        "Salt Risk Factor Amount [g/serving]": "TODO",
        "Salt Risk Points [points/serving]": "0.0",
        "Vita Score Improvement Percentage": "2.314",
        "Vita score [Serving]": "332.133",
        "Vita Score Rating": "B",
        "Water Scarcity Improvement Percentage": "37.209",
        "Portions per Recipe": "140",
        "Recipe Portions planned or produced": "",
        "Recipe Portions sold": "",
        "Water Scarcity Reduction Value": "6459.9",
    },
]


EXPECTED_RECIPE_CSV = [
    copy.deepcopy(EXPECTED_SINGLE_RECIPE_WITH_SUBRECIPE_CSV[0]),
    copy.deepcopy(EXPECTED_SINGLE_SUBRECIPE_CSV[0]),
]


EXPECTED_RECIPE_WITH_INGREDIENTS_CSV = [
    copy.deepcopy(EXPECTED_SINGLE_RECIPE_WITH_SUBRECIPE_WITH_INGREDIENTS_CSV[0]),
    copy.deepcopy(EXPECTED_SINGLE_RECIPE_WITH_SUBRECIPE_WITH_INGREDIENTS_CSV[1]),
    copy.deepcopy(EXPECTED_SINGLE_SUBRECIPE_WITH_INGREDIENTS_CSV[0]),
    copy.deepcopy(
        EXPECTED_SINGLE_SUBRECIPE_WITH_INGREDIENTS_CSV[1],
    ),
]

EXPECTED_SUPPLY_CSV = [
    {
        "ID": None,  # TODO: here, currently the uid is returned
        "Title": "SupplySheetActivityNode",
        "Location": "TODO",
        "Kitchen Name": "TODO",
        "Production Date": "2023-06-20",
        "Index": "0",
        "Weight [g]": "150.0",
        "Name": "Tomaten",
        "Type": "TODO",
        "Product ID": "100100191",
        "Production": "TODO",
        "Origin": "",
        "Transport": "",
        "g CO2": "0.0",
        "CO2 Base Value [g]": "0.0",
        "CO2 Greenhouse [g]": "0.0",
        "CO2 Organic [g]": "0.0",
        "CO2 Frozen Conservation [g]": "0.0",
        "CO2 Land Transport [g]": "0.0",
        "CO2 Sea Transport [g]": "0.0",
        "CO2 Air Transport [g]": "0.0",
        "Energy [kJ]": "133.5",
        "Food Unit": "0.119",
        "CO2 Improvement Percentage": "100.0",
        "CO2 Reduction Value": "0.092",
        "Water Scarcity Footprint [liter]": "1.444",
        "Total Soy Amount [g/serving] (critical)": "TODO",
        "Total Soy Amount [g/serving] (not critical)": "TODO",
        "Total Palmoil Amount [g/serving] not certified": "TODO",
        "Total Palmoil Amount [g/serving] certified": "TODO",
        "Tags": "TODO",
        "meat origins": "TODO",
        "Animal Welfare Rating": "A",
        "Rainforest Rating": "A",
        "Water Scarcity Rating": "A",
        "CO2 FU Rating": "A",
        "Fruit Risk Factor Amount [g/serving]": "TODO",
        "Fruit Risk Points [points/serving]": "61.24",
        "Vegetable Risk Factor Amount [g/serving]": "TODO",
        "Vegetable Risk Points [points/serving]": "0.0",
        "Wholegrain Risk Factor Amount [g/serving]": "TODO",
        "Wholegrain Risk Points [points/serving]": "82.86",
        "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
        "Nuts Seeds Risk Points [points/serving]": "13.98",
        "Milk Risk Factor Amount [g/serving]": "TODO",
        "Milk Risk Points [points/serving]": "9.42",
        "Processed Meat Risk Factor Amount [g/serving]": "TODO",
        "Processed Meat Risk Points [points/serving]": "0.0",
        "Red Meat Risk Factor Amount [g/serving]": "TODO",
        "Red Meat Risk Points [points/serving]": "0.0",
        "Salt Risk Factor Amount [g/serving]": "TODO",
        "Salt Risk Points [points/serving]": "0.0",
        "Vita Score Improvement Percentage": "50.735",
        "Vita score [Serving]": "167.5",
        "Vita Score Rating": "A",
        "Water Scarcity Improvement Percentage": "66.769",
        "Portions per Recipe": "1",
        "Recipe Portions planned or produced": "",
        "Recipe Portions sold": "",
        "Water Scarcity Reduction Value": "2.902",
    },
    {
        "ID": None,  # TODO: here, currently the uid is returned
        "Title": "SupplySheetActivityNode",
        "Location": "TODO",
        "Kitchen Name": "TODO",
        "Production Date": "2023-06-20",
        "Index": "1",
        "Weight [g]": "78.0",
        "Name": "Zwiebeln",
        "Type": "TODO",
        "Product ID": "100199894",
        "Production": "TODO",
        "Origin": "",
        "Transport": "",
        "g CO2": "189.54",
        "CO2 Base Value [g]": "189.54",
        "CO2 Greenhouse [g]": "0.0",
        "CO2 Organic [g]": "0.0",
        "CO2 Frozen Conservation [g]": "0.0",
        "CO2 Land Transport [g]": "0.0",
        "CO2 Sea Transport [g]": "0.0",
        "CO2 Air Transport [g]": "0.0",
        "Energy [kJ]": "1040.211",
        "Food Unit": "0.447",
        "CO2 Improvement Percentage": "95.756",
        "CO2 Reduction Value": "4.277",
        "Water Scarcity Footprint [liter]": "1.594",
        "Total Soy Amount [g/serving] (critical)": "TODO",
        "Total Soy Amount [g/serving] (not critical)": "TODO",
        "Total Palmoil Amount [g/serving] not certified": "TODO",
        "Total Palmoil Amount [g/serving] certified": "TODO",
        "Tags": "TODO",
        "meat origins": "TODO",
        "Animal Welfare Rating": "A",
        "Rainforest Rating": "E",
        "Water Scarcity Rating": "A",
        "CO2 FU Rating": "A",
        "Fruit Risk Factor Amount [g/serving]": "TODO",
        "Fruit Risk Points [points/serving]": "61.24",
        "Vegetable Risk Factor Amount [g/serving]": "TODO",
        "Vegetable Risk Points [points/serving]": "0.0",
        "Wholegrain Risk Factor Amount [g/serving]": "TODO",
        "Wholegrain Risk Points [points/serving]": "82.86",
        "Nuts Seeds Risk Factor Amount [g/serving]": "TODO",
        "Nuts Seeds Risk Points [points/serving]": "13.98",
        "Milk Risk Factor Amount [g/serving]": "TODO",
        "Milk Risk Points [points/serving]": "9.42",
        "Processed Meat Risk Factor Amount [g/serving]": "TODO",
        "Processed Meat Risk Points [points/serving]": "0.0",
        "Red Meat Risk Factor Amount [g/serving]": "TODO",
        "Red Meat Risk Points [points/serving]": "0.0",
        "Salt Risk Factor Amount [g/serving]": "TODO",
        "Salt Risk Points [points/serving]": "0.0",
        "Vita Score Improvement Percentage": "-7.782",
        "Vita score [Serving]": "277.798",
        "Vita Score Rating": "C",
        "Water Scarcity Improvement Percentage": "90.269",
        "Portions per Recipe": "1",
        "Recipe Portions planned or produced": "",
        "Recipe Portions sold": "",
        "Water Scarcity Reduction Value": "14.789",
    },
]


@pytest.mark.asyncio
async def test_recipe_csv_endpoints(
    app_fixture: AppFixtureReturn, create_kitchen_recipe_with_subrecipe_by_superuser: None
) -> None:
    _ = create_kitchen_recipe_with_subrecipe_by_superuser
    # single recipe (top-recipe)
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/csv-export/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
    assert response.status_code == 200
    csvs = response.text.split("\n\n\n\nIngredients:\n")

    for calculated_csv, expected_csv in zip(
        csvs,
        [EXPECTED_SINGLE_SUBRECIPE_CSV, EXPECTED_SINGLE_SUBRECIPE_WITH_INGREDIENTS_CSV],
    ):
        csv_reader = csv.DictReader(StringIO(calculated_csv))
        csv_data = [{k: v for k, v in row.items() if k != "Row Index"} for row in csv_reader]
        dict_diff = deepdiff.DeepDiff(csv_data, expected_csv, ignore_order=True, verbose_level=2)
        assert dict_diff == {}, dict_diff

    # single recipe (sub-recipe)
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/csv-export/recipes/{TEST_RECIPE_WITH_SUBRECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
    assert response.status_code == 200
    csvs = response.text.split("\n\n\n\nIngredients:\n")

    for calculated_csv, expected_csv in zip(
        csvs,
        [EXPECTED_SINGLE_RECIPE_WITH_SUBRECIPE_CSV, EXPECTED_SINGLE_RECIPE_WITH_SUBRECIPE_WITH_INGREDIENTS_CSV],
    ):
        csv_reader = csv.DictReader(StringIO(calculated_csv))
        csv_data = [{k: v for k, v in row.items() if k != "Row Index"} for row in csv_reader]
        dict_diff = deepdiff.DeepDiff(csv_data, expected_csv, ignore_order=True, verbose_level=2)
        assert dict_diff == {}, dict_diff

    # monthly export without ingredients
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/csv-export/recipes?kitchen-ids={TEST_KITCHEN_SUPERUSER_ID}&production-date-from=2023-06-01&production-date-to=2024-06-30",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
    assert response.status_code == 200
    csv_reader = csv.DictReader(StringIO(response.text))
    csv_data = [{k: v for k, v in row.items() if k != "Row Index"} for row in csv_reader]
    dict_diff = deepdiff.DeepDiff(csv_data, EXPECTED_RECIPE_CSV, ignore_order=True, verbose_level=2)
    assert dict_diff == {}, dict_diff

    # monthly export with ingredients
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/csv-export/recipes/with-ingredients?kitchen-ids={TEST_KITCHEN_SUPERUSER_ID}&production-date-from=2023-06-01&production-date-to=2024-06-30",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
    assert response.status_code == 200
    csv_reader = csv.DictReader(StringIO(response.text))
    csv_data = [{k: v for k, v in row.items() if k != "Row Index"} for row in csv_reader]
    dict_diff = deepdiff.DeepDiff(csv_data, EXPECTED_RECIPE_WITH_INGREDIENTS_CSV, ignore_order=True, verbose_level=2)
    assert dict_diff == {}, dict_diff


@pytest.mark.asyncio
async def test_supply_csv_endpoints(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # trying POST request
        response = await ac.post(
            f"/api/kitchens/{TEST_KITCHEN_SUPERUSER_ID}/supplies/",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_SUPPLY,
        )

        assert response.status_code == 200

    # monthly supply export
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/csv-export/supplies?kitchen-ids={TEST_KITCHEN_SUPERUSER_ID}&production-date-from=2023-06-01&production-date-to=2024-06-30",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
    assert response.status_code == 200
    csv_reader = csv.DictReader(StringIO(response.text))
    csv_data = [{k: v for k, v in row.items() if k != "Row Index"} for row in csv_reader]
    # TODO: remove this again
    for row in csv_data:
        row["ID"] = None
    dict_diff = deepdiff.DeepDiff(csv_data, EXPECTED_SUPPLY_CSV, ignore_order=True, verbose_level=2)
    assert dict_diff == {}, dict_diff
