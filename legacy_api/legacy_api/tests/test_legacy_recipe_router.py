import asyncio
import copy
import os
import uuid
from typing import Any, Optional
from unittest import mock

import pytest
from asyncpg.exceptions import ForeignKeyViolationError
from httpx import AsyncClient, Response
from structlog import get_logger

from api.tests.conftest import FastAPIReturn
from api.tests.test_api_for_recipes import (
    TEST_RECIPE_EXPECTED_CO2,
    TEST_RECIPE_EXPECTED_DFU,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_GREENHOUSE_CONTRIBUTION,
)
from api.tests.test_calc_router import (
    TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION,
    TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_ONION,
    TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_TOMATO,
)
from core.domain.calculation import Calculation
from core.domain.edge import Edge
from core.domain.nodes.activity.linking_activity_node import LinkingActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.props.link_to_xid_prop import LinkToXidProp
from core.service.service_provider import ServiceLocator
from legacy_api.app.server import api_client, postgres_db
from legacy_api.tests.test_legacy_supply_router import (
    TEST_LEGACY_SUPPLY_EXPECTED_CO2,
    TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
    TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
    TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
    TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
    TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
    TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
    TEST_LEGACY_SUPPLY_EXPECTED_DFU,
    TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1,
    TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2,
    TEST_SUPPLY,
)

from .conftest import (
    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
    EXPECTED_CO2_REDUCTION_VALUE,
    EXPECTED_CO2_REDUCTION_VALUE_ING_1,
    EXPECTED_CO2_REDUCTION_VALUE_ING_2,
    SERVINGS,
    TEST_KITCHEN_ADMIN_ID,
    TEST_KITCHEN_SUPERUSER_ID,
    TEST_RECIPE,
    TEST_RECIPE_ADMIN_ID,
    TEST_RECIPE_EXPECTED_SCARCE_WATER_LITER,
    TEST_RECIPE_IN_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
    TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
    TEST_RECIPE_IN_SUBRECIPE_REDUCTION_VALUE,
    TEST_RECIPE_LEGACY_CO2_VALUE,
    TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
    TEST_RECIPE_LEGACY_CO2_VALUE_WITHOUT_DRYING,
    TEST_RECIPE_LEGACY_FOOD_UNIT,
    TEST_RECIPE_LEGACY_ING1_FOOD_UNIT,
    TEST_RECIPE_LEGACY_ING2_CO2_VALUE,
    TEST_RECIPE_LEGACY_ING2_CO2_VALUE_WITHOUT_DRYING,
    TEST_RECIPE_LEGACY_ING2_FOOD_UNIT,
    TEST_RECIPE_LEGACY_ING2_FOOD_UNIT_NO_DRYING,
    TEST_RECIPE_LEGACY_VITASCORE,
    TEST_RECIPE_LEGACY_VITASCORE_ING1,
    TEST_RECIPE_LEGACY_VITASCORE_ING2,
    TEST_RECIPE_NO_DRYING_LEGACY_FOOD_UNIT,
    TEST_RECIPE_SUPERUSER_ID,
    TEST_RECIPE_WITH_SUBRECIPE,
    TEST_RECIPE_WITH_SUBRECIPE_CO2_VALUE,
    TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
    TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
    TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT,
    TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE,
    TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_IMPROVEMENT_PERCENTAGE,
    TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_REDUCTION,
    TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
    TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE,
    TEST_RECIPE_WITH_TOMATO_TRANSPORT_CO2_IMPROVEMENT_PERCENTAGE,
    TEST_RECIPE_WITH_TOMATO_TRANSPORT_CO2_REDUCTION_VALUE,
    TOMATO_TRUCK_TRANSPORT_CO2_IMPROVEMENT_PERCENTAGE,
    TOMATO_TRUCK_TRANSPORT_CO2_REDUCTION_VALUE,
    TOMATO_TRUCK_TRANSPORT_FROM_SPAIN_CO2_VALUE,
    ZERO_AMOUNT_INDICATORS,
    AppFixtureReturn,
    check_response_dict,
    legacy_api_settings,
)

os.environ["POSTGRES_SCHEMA"] = "test_pg"
logger = get_logger()


def add_nutrients_to_expected_response(expected_response_orig: dict, nutrient_data: list[dict]) -> dict:
    expected_response = copy.deepcopy(expected_response_orig)
    for ing_index, ingredient in enumerate(expected_response["recipe"]["ingredients"]):
        ingredient["nutrient-values"] = {
            key: val * ingredient["amount"] / 100 * 1000 for key, val in nutrient_data[ing_index].items()
        }
    return expected_response


def delete_field_from_recipe_or_ingredient(
    main_recipe: dict, field_to_delete: str, delete_from: str = "recipe"
) -> dict:
    assert delete_from in (
        "recipe",
        "ingredient",
    ), f"delete_from has to be either 'recipe' or 'ingredient', not {delete_from}"
    new_recipe = copy.deepcopy(main_recipe)

    match delete_from:
        case "recipe":
            del new_recipe["recipe"][field_to_delete]
        case "ingredient":
            for ingredient in new_recipe["recipe"]["ingredients"]:
                del ingredient[field_to_delete]

    return new_recipe


def add_field_from_recipe_or_ingredient(
    main_recipe: dict, field_to_add: str, new_value: str | float | int | dict | list, add_to: str = "recipe"
) -> dict:
    assert add_to in (
        "recipe",
        "ingredient",
    ), f"add_to has to be either 'recipe' or 'ingredient', not {add_to}"
    new_recipe = copy.deepcopy(main_recipe)

    match add_to:
        case "recipe":
            new_recipe["recipe"][field_to_add] = new_value
        case "ingredient":
            for idx, ingredient in enumerate(new_recipe["recipe"]["ingredients"]):
                assert isinstance(
                    new_value, list
                ), f"For add_to='ingredient', new_value should be a list not {type(new_value)}."
                ingredient[field_to_add] = new_value[idx]

    return new_recipe


@pytest.mark.asyncio
async def test_create_delete_and_get_recipe_by_superuser(
    app_fixture: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: None,
    load_tomato_and_onion_nutrients: dict[str, dict[str, float]],
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        # testing POST request without trailing slash
        response = await ac.post(
            "/api/recipes?indicators=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        # testing POST request with trailing slash
        response = await ac.post(
            "/api/recipes/?indicators=true&full-resource=true&nutrient-values=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        expected_recipe_resp = copy.deepcopy(TEST_RECIPE)
        expected_recipe_resp["recipe"]["id"] = response.json()["recipe"]["id"]
        expected_recipe_resp["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp["message"] = ""
        expected_recipe_resp["recipe-id"] = response.json()["recipe"]["id"]
        expected_recipe_resp["request-id"] = 0
        expected_recipe_resp["statuscode"] = 200

        nutrition_data = [
            val for key, val in load_tomato_and_onion_nutrients.items() if key in ("tomato", "dried_onion")
        ]
        check_response_dict(
            add_nutrients_to_expected_response(expected_recipe_resp, nutrition_data),
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_indicators={
                "vita-score": TEST_RECIPE_LEGACY_VITASCORE,
                "scarce-water-liters": TEST_RECIPE_EXPECTED_SCARCE_WATER_LITER,
                "water-footprint-award": True,
                "water-footprint-improvement-percentage": 85.34,
                "water-footprint-rating": "A",
                "rainforest-label": False,
                "rainforest-rating": "E",
                "animal-treatment-label": True,
                "animal-treatment-rating": "A",
                "local-label": False,
                "local-rating": "E",
                "season-label": False,
                "season-rating": "E",
            },
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
            expected_ingredients_indicators=[
                {
                    "animal-treatment-label": True,
                    "animal-treatment-rating": "A",
                    "rainforest-label": True,
                    "rainforest-rating": "A",
                    "scarce-water-liters": round(TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_TOMATO * 1000, 2),
                    "water-footprint-award": True,
                    "water-footprint-improvement-percentage": 66.77,
                    "water-footprint-rating": "A",
                    "local-label": False,
                    "local-rating": "E",
                    "season-label": False,
                    "season-rating": "E",
                    "vita-score": TEST_RECIPE_LEGACY_VITASCORE_ING1,
                },
                {
                    "animal-treatment-label": True,
                    "animal-treatment-rating": "A",
                    "rainforest-label": False,
                    "rainforest-rating": "E",
                    "scarce-water-liters": round(TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_ONION * 1000, 2),
                    "water-footprint-award": True,
                    "water-footprint-improvement-percentage": 90.27,
                    "water-footprint-rating": "A",
                    "local-label": False,
                    "local-rating": "E",
                    "season-label": False,
                    "season-rating": "E",
                    "vita-score": TEST_RECIPE_LEGACY_VITASCORE_ING2,
                },
            ],
        )
        assert response.json().get("recipe").get("id") is not None
        temp_recipe_id = response.json().get("recipe").get("id")

        response = await ac.get(
            f"/api/recipes/{temp_recipe_id}?nutrient-values=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200

        expected_recipe_resp = copy.deepcopy(TEST_RECIPE)
        expected_recipe_resp["recipe"]["id"] = temp_recipe_id
        expected_recipe_resp["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp["statuscode"] = 200
        expected_recipe_resp["message"] = ""
        expected_recipe_resp["recipe-id"] = temp_recipe_id
        check_response_dict(
            add_nutrients_to_expected_response(expected_recipe_resp, nutrition_data),
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        response = await ac.delete(
            f"/api/recipes/{temp_recipe_id}", headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"}
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/recipes/{temp_recipe_id}", headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"}
        )
        assert response.status_code == 404

        # TODO: should we test mutation log response here as well?
        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        assert len(response.json()["recipe"]["ingredients"]) == 0  # since full-resource=false by default.

        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 200
        expected_recipe_resp = copy.deepcopy(TEST_RECIPE)
        expected_recipe_resp["statuscode"] = 200
        expected_recipe_resp["message"] = ""
        expected_recipe_resp["recipe-id"] = TEST_RECIPE_SUPERUSER_ID
        expected_recipe_resp["recipe"]["id"] = TEST_RECIPE_SUPERUSER_ID
        expected_recipe_resp["recipe"]["kitchen-id"] = TEST_KITCHEN_SUPERUSER_ID
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )

        # Test for transient:
        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}?transient=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200
        assert len(response.json()["recipe"]["ingredients"]) == 0  # since full-resource=false by default.

        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}?full-resource=false",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
        )
        assert response.status_code == 404

        # Test for different servings:
        new_servings = 500
        test_recipe_new_servings = copy.deepcopy(TEST_RECIPE)
        test_recipe_new_servings["recipe"]["recipe-portions"] = new_servings

        # Test for transient:
        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_SUPERUSER_ID}?transient=true&indicators=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_SUPER_TOKEN}"},
            json=test_recipe_new_servings,
        )
        expected_recipe_resp_new_servings = copy.deepcopy(expected_recipe_resp)
        expected_recipe_resp_new_servings["recipe"]["recipe-portions"] = new_servings
        expected_recipe_resp_new_servings["recipe"]["ingredients"] = []
        expected_recipe_resp_new_servings["recipe-id"] = TEST_RECIPE_SUPERUSER_ID
        expected_recipe_resp_new_servings["request-id"] = 0
        expected_recipe_resp_new_servings["statuscode"] = 200
        expected_recipe_resp_new_servings["message"] = ""

        expected_vitascore_new_servings = copy.deepcopy(TEST_RECIPE_LEGACY_VITASCORE)
        expected_vitascore_new_servings["energy-kcals"] = 561.05
        expected_vitascore_new_servings["fruit-risk-factor-points"] = 61.24
        expected_vitascore_new_servings["milk-risk-factor-points"] = 9.42
        expected_vitascore_new_servings["nutrition-label"] = True
        expected_vitascore_new_servings["nuts-seeds-risk-factor-points"] = 13.98
        expected_vitascore_new_servings["salt-risk-factor-amount-gram"] = 0.14
        expected_vitascore_new_servings["vegetable-risk-factor-amount-gram"] = 456.0
        expected_vitascore_new_servings["vita-score-improvement-percentage"] = 19.05
        expected_vitascore_new_servings["vita-score-points"] = 275.22
        expected_vitascore_new_servings["vita-score-rating"] = "A"
        expected_vitascore_new_servings["wholegrain-risk-factor-points"] = 82.86
        expected_vitascore_new_servings["high-in-energy-risk-factor-points"] = 0.0

        check_response_dict(
            expected_recipe_resp_new_servings,
            response.json(),
            "recipe",
            expected_co2_value=round(TEST_RECIPE_EXPECTED_CO2 * 1000 * 1000 / new_servings),
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=round(EXPECTED_CO2_REDUCTION_VALUE * SERVINGS / new_servings),
            expected_daily_food_unit=TEST_RECIPE_EXPECTED_DFU * 1000 / new_servings,
            expected_indicators={
                "vita-score": expected_vitascore_new_servings,
                "scarce-water-liters": round(TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION * 1000 / new_servings, 2),
                "water-footprint-award": True,
                "water-footprint-improvement-percentage": 85.34,
                "water-footprint-rating": "A",
                "rainforest-label": False,
                "rainforest-rating": "E",
                "animal-treatment-label": True,
                "animal-treatment-rating": "A",
                "local-label": False,
                "local-rating": "E",
                "season-label": False,
                "season-rating": "E",
            },
        )


@pytest.mark.asyncio
async def test_create_delete_and_get_recipe_by_admin(
    app_fixture: AppFixtureReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: None,
) -> None:
    _ = create_and_get_and_delete_and_create_kitchen_by_admin
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
        test_recipe_with_id["recipe"]["id"] = TEST_RECIPE_ADMIN_ID
        test_recipe_with_id["recipe"]["kitchen-id"] = TEST_KITCHEN_ADMIN_ID
        test_recipe_with_id["recipe"]["recipe-portions"] = test_recipe_with_id["recipe"]["servings"]
        test_recipe_with_id["statuscode"] = 200
        test_recipe_with_id["message"] = ""
        test_recipe_with_id["recipe-id"] = TEST_RECIPE_ADMIN_ID
        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            test_recipe_with_id,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        response = await ac.delete(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 204

        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 404

        response = await ac.put(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
            json=TEST_RECIPE,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true",
            headers={"Authorization": f"Basic {pytest.ENCODED_USER_ADMIN_TOKEN}"},
        )
        assert response.status_code == 200
        check_response_dict(
            test_recipe_with_id,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )


@pytest.mark.asyncio
async def test_create_recipe_with_missing_fields(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen
    recipe_xid = "recipe01"

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        for removed_field, removed_from in [
            ("unit", "ingredient"),
            ("location", "recipe"),
            ("transport", "ingredient"),
            ("conservation", "ingredient"),
        ]:
            test_recipe_without_prop = delete_field_from_recipe_or_ingredient(
                TEST_RECIPE, removed_field, delete_from=removed_from
            )
            test_recipe_without_prop["recipe"]["id"] = recipe_xid
            test_recipe_without_prop["recipe"]["kitchen-id"] = kitchen_xid

            response = await ac.put(
                f"/api/kitchens/{kitchen_xid}/recipes/{recipe_xid}?full-resource=true",
                auth=(customer_token, ""),
                json=test_recipe_without_prop,
            )
            response.raise_for_status()

            expected_resp_recipe = copy.deepcopy(test_recipe_without_prop)
            expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
            expected_resp_recipe["statuscode"] = 200
            expected_resp_recipe["message"] = ""
            expected_resp_recipe["request-id"] = 0
            expected_resp_recipe["recipe-id"] = "recipe01"
            if removed_field == "transport":
                expected_co2 = round(
                    (TEST_RECIPE_LEGACY_ING2_CO2_VALUE + TOMATO_TRUCK_TRANSPORT_FROM_SPAIN_CO2_VALUE) / SERVINGS
                )
                expected_co2_improvement_percentage = TEST_RECIPE_WITH_TOMATO_TRANSPORT_CO2_IMPROVEMENT_PERCENTAGE
                expected_co2_reduction_value = TEST_RECIPE_WITH_TOMATO_TRANSPORT_CO2_REDUCTION_VALUE
                expected_food_unit = TEST_RECIPE_LEGACY_FOOD_UNIT
                expected_ingredients_co2_value = [
                    TOMATO_TRUCK_TRANSPORT_FROM_SPAIN_CO2_VALUE,
                    TEST_RECIPE_LEGACY_ING2_CO2_VALUE,
                ]
                expected_ingredients_co2_improvement_percentage = [
                    TOMATO_TRUCK_TRANSPORT_CO2_IMPROVEMENT_PERCENTAGE,
                    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
                ]
                expected_ingredients_co2_reduction_value = [
                    TOMATO_TRUCK_TRANSPORT_CO2_REDUCTION_VALUE,
                    EXPECTED_CO2_REDUCTION_VALUE_ING_2,
                ]
                expected_ingredients_co2_rating = ["A", "B"]
                expected_ingredients_bar_chart = [9, 91]
                expected_ingredients_food_unit = [TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT]
                expected_co2_rating = "A"
            elif removed_field == "unit":
                expected_co2 = round(TEST_RECIPE_EXPECTED_CO2 * 1000 / SERVINGS)
                expected_co2_improvement_percentage = EXPECTED_CO2_IMPROVEMENT_PERCENTAGE
                expected_co2_reduction_value = round(EXPECTED_CO2_REDUCTION_VALUE / 1000)
                expected_food_unit = TEST_RECIPE_EXPECTED_DFU / SERVINGS
                expected_ingredients_co2_value = [0, round(TEST_RECIPE_EXPECTED_CO2 * 1000)]
                expected_ingredients_co2_rating = ["A", "B"]
                expected_ingredients_bar_chart = [0, 100]
                expected_ingredients_food_unit = [
                    TEST_RECIPE_LEGACY_ING1_FOOD_UNIT / 1000,
                    TEST_RECIPE_LEGACY_ING2_FOOD_UNIT / 1000,
                ]
                expected_co2_rating = "A"
                expected_ingredients_co2_improvement_percentage = [
                    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
                ]
                expected_ingredients_co2_reduction_value = [
                    round(EXPECTED_CO2_REDUCTION_VALUE_ING_1 / 1000),
                    round(EXPECTED_CO2_REDUCTION_VALUE_ING_2 / 1000),
                ]
            elif removed_field == "conservation":
                expected_co2 = round(TEST_RECIPE_LEGACY_CO2_VALUE_WITHOUT_DRYING)
                expected_co2_improvement_percentage = 83
                expected_co2_reduction_value = 923
                expected_food_unit = TEST_RECIPE_NO_DRYING_LEGACY_FOOD_UNIT
                expected_ingredients_co2_value = [
                    0,
                    TEST_RECIPE_LEGACY_ING2_CO2_VALUE_WITHOUT_DRYING,
                ]
                expected_ingredients_co2_rating = ["A", "A"]
                expected_ingredients_bar_chart = [0, 100]
                expected_ingredients_food_unit = [
                    TEST_RECIPE_LEGACY_ING1_FOOD_UNIT,
                    TEST_RECIPE_LEGACY_ING2_FOOD_UNIT_NO_DRYING,
                ]
                expected_co2_rating = "A"
                expected_ingredients_co2_improvement_percentage = [
                    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                    58,
                ]
                expected_ingredients_co2_reduction_value = [
                    EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                    36842,
                ]
            else:
                expected_co2 = TEST_RECIPE_LEGACY_CO2_VALUE
                expected_food_unit = TEST_RECIPE_LEGACY_FOOD_UNIT
                expected_ingredients_co2_value = [0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE]
                expected_ingredients_co2_rating = ["A", "B"]
                expected_ingredients_bar_chart = [0, 100]
                expected_ingredients_food_unit = [TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT]
                expected_co2_improvement_percentage = EXPECTED_CO2_IMPROVEMENT_PERCENTAGE
                expected_co2_reduction_value = EXPECTED_CO2_REDUCTION_VALUE
                expected_ingredients_co2_improvement_percentage = [
                    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                    EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
                ]
                expected_ingredients_co2_reduction_value = [
                    EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                    EXPECTED_CO2_REDUCTION_VALUE_ING_2,
                ]
                expected_co2_rating = "A"

            check_response_dict(
                expected_resp_recipe,
                response.json(),
                "recipe",
                expected_co2_value=expected_co2,
                expected_co2_rating=expected_co2_rating,
                expected_co2_improvement_percentage=expected_co2_improvement_percentage,
                expected_co2_reduction_value=expected_co2_reduction_value,
                expected_daily_food_unit=expected_food_unit,
                expected_ingredients_co2_value=expected_ingredients_co2_value,
                expected_ingredients_co2_rating=expected_ingredients_co2_rating,
                expected_ingredients_bar_chart=expected_ingredients_bar_chart,
                expected_ingredients_food_unit=expected_ingredients_food_unit,
                expected_ingredients_co2_improvement_percentage=expected_ingredients_co2_improvement_percentage,
                expected_ingredients_co2_reduction_value=expected_ingredients_co2_reduction_value,
            )


@pytest.mark.asyncio
async def test_put_response_as_request_with_missing_fields(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen
    recipe_xid = "recipe01"

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        test_recipe_without_transport_and_location = delete_field_from_recipe_or_ingredient(
            TEST_RECIPE, "transport", delete_from="ingredient"
        )
        test_recipe_without_transport_and_location = delete_field_from_recipe_or_ingredient(
            test_recipe_without_transport_and_location, "location", delete_from="recipe"
        )
        test_recipe_without_transport_and_location["recipe"]["id"] = recipe_xid
        test_recipe_without_transport_and_location["recipe"]["kitchen-id"] = kitchen_xid

        # if we remove transport and location, the expected co2 is higher. The reason is that
        # 1) In TEST_RECIPE we specify transport = 'air' for which we don't have the ecotransit response,
        # therefore, there is no transport in this case
        # 2) Even though we remove the location in test_recipe_without_transport_and_location, the location GFM
        # can identify the location from the access-group of the kitchen. Since we don't specify transport,
        # we will have truck-transport to this location.

        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{recipe_xid}?full-resource=true",
            auth=(customer_token, ""),
            json=test_recipe_without_transport_and_location,
        )
        first_response_json = response.json()
        expected_resp_recipe = copy.deepcopy(test_recipe_without_transport_and_location)
        expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
        expected_resp_recipe["statuscode"] = 200
        expected_resp_recipe["message"] = ""
        expected_resp_recipe["request-id"] = 0
        expected_resp_recipe["recipe-id"] = "recipe01"
        assert response.status_code == 200
        check_response_dict(
            expected_resp_recipe,
            first_response_json,
            "recipe",
            expected_co2_value=round(
                (TEST_RECIPE_LEGACY_ING2_CO2_VALUE + TOMATO_TRUCK_TRANSPORT_FROM_SPAIN_CO2_VALUE) / SERVINGS
            ),
            expected_co2_improvement_percentage=TEST_RECIPE_WITH_TOMATO_TRANSPORT_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_RECIPE_WITH_TOMATO_TRANSPORT_CO2_REDUCTION_VALUE,
            expect_eaternity_award_field=True,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[
                TOMATO_TRUCK_TRANSPORT_FROM_SPAIN_CO2_VALUE,
                TEST_RECIPE_LEGACY_ING2_CO2_VALUE,
            ],
            expected_co2_rating="A",
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[9, 91],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                TOMATO_TRUCK_TRANSPORT_CO2_IMPROVEMENT_PERCENTAGE,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                TOMATO_TRUCK_TRANSPORT_CO2_REDUCTION_VALUE,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        # Add the information back again, then rerun.
        recipe_with_information_added_back = add_field_from_recipe_or_ingredient(
            first_response_json, "transport", ["air", "ground"], add_to="ingredient"
        )
        recipe_with_information_added_back = add_field_from_recipe_or_ingredient(
            recipe_with_information_added_back, "location", "Zürich, Schweiz", add_to="recipe"
        )
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{recipe_xid}?full-resource=true",
            auth=(customer_token, ""),
            json=recipe_with_information_added_back,
        )
        response.raise_for_status()

        check_response_dict(
            recipe_with_information_added_back,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_ingredients_co2_value=[0, None],
            expected_ingredients_co2_rating=[None, None],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                None,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                None,
            ],
            response_as_request=True,
        )


@pytest.mark.asyncio
async def test_recipes_xid_linking(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    sub_recipe_xid = "test_recipe_id"

    test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_id["recipe"]["id"] = sub_recipe_xid

    unmatched_test_recipe_with_subrecipe = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    unmatched_recipe_id = "unmatched-recipe-id"
    del unmatched_test_recipe_with_subrecipe["request-id"]
    unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["id"] = unmatched_recipe_id
    original_product_name = [
        {"language": "de", "value": "K\u00fcrbisrisotto"},
        {"language": "en", "value": "TEST"},
    ]
    unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["names"] = original_product_name
    unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["type"] = "conceptual-ingredients"

    def _check_that_is_unmatched(response: Response, unmatched_string: str) -> None:
        assert response.status_code == 602
        assert "co2-value" not in response.json()["recipe"]
        assert response.json()["statuscode"] == 602
        assert response.json()["message"] == (
            "At least one ingredient name could not get automatically matched into "
            "the Eaternity Database. "
            "Retry once a day or try with a different ingredient name. "
            f"Names that couldn't be matched: [[{unmatched_string}]]."
        )

    def _check_that_matching_is_correct(response: Response) -> None:
        assert response.status_code == 200
        expected_resp_recipe = copy.deepcopy(unmatched_test_recipe_with_subrecipe)
        expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
        expected_resp_recipe["statuscode"] = 200
        expected_resp_recipe["message"] = ""
        expected_resp_recipe["request-id"] = 0
        expected_resp_recipe["recipe-id"] = "test_recipe_with_subrecipe_id"
        if len(response.json()["recipe"]["ingredients"][0]["names"]) == 1:
            # ignore if we don't have all original language strings in the response
            del expected_resp_recipe["recipe"]["ingredients"][0]["names"][1]
        check_response_dict(
            expected_resp_recipe,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="B",
            expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT,
            expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
                TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE,
            ],
            expected_ingredients_co2_rating=["A", "C"],
            expected_ingredients_bar_chart=[46, 54],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
            ],
            expected_ingredients_co2_improvement_percentage=[
                TEST_RECIPE_IN_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
                TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_IMPROVEMENT_PERCENTAGE,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_RECIPE_IN_SUBRECIPE_REDUCTION_VALUE,
                TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_REDUCTION,
            ],
        )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{sub_recipe_xid}",
            auth=(customer_token, ""),
            json=test_recipe_with_id,
        )

        assert response.status_code == 200

        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{unmatched_test_recipe_with_subrecipe['recipe']['id']}",
            auth=(customer_token, ""),
            json=unmatched_test_recipe_with_subrecipe,
        )

        _check_that_is_unmatched(response, "'Kürbisrisotto', 'TEST'")

    # try to rematch and then make sure it is not unmatched if not all names are resent
    unmatched_recipe_uid = await postgres_db.product_mgr.find_uid_by_xid(
        customer_uid, f"FoodService{unmatched_recipe_id}"
    )
    edges = await postgres_db.graph_mgr.get_sub_graph_by_uid(unmatched_recipe_uid, max_depth=1)
    assert len(edges) == 1
    flow_node: FlowNode = await postgres_db.graph_mgr.find_by_uid(edges[0].child_uid)
    flow_node.link_to_sub_node = LinkToXidProp(xid=sub_recipe_xid)
    flow_node.product_name = original_product_name

    await postgres_db.graph_mgr.upsert_node_by_uid(flow_node)

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=unmatched_test_recipe_with_subrecipe,
        )
        _check_that_matching_is_correct(response)

        # Test with same product name but longer description of same language. Should not be unmatched
        modified_unmatched_test_recipe_with_subrecipe = copy.deepcopy(unmatched_test_recipe_with_subrecipe)
        modified_unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["names"] = [
            original_product_name[0],
        ]
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{modified_unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=modified_unmatched_test_recipe_with_subrecipe,
        )
        _check_that_matching_is_correct(response)

    # try to rematch and then unmatch using a different product name
    unmatched_recipe_uid = await postgres_db.product_mgr.find_uid_by_xid(
        customer_uid, f"FoodService{unmatched_recipe_id}"
    )
    edges = await postgres_db.graph_mgr.get_sub_graph_by_uid(unmatched_recipe_uid, max_depth=1)
    assert len(edges) == 1
    flow_node: FlowNode = await postgres_db.graph_mgr.find_by_uid(edges[0].child_uid)
    flow_node.link_to_sub_node = LinkToXidProp(xid=sub_recipe_xid)
    flow_node.product_name = original_product_name

    await postgres_db.graph_mgr.upsert_node_by_uid(flow_node)

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=unmatched_test_recipe_with_subrecipe,
        )
        _check_that_matching_is_correct(response)

        # Test with a new product name. Should be unmatched, and no co2 values should be returned.
        modified_unmatched_test_recipe_with_subrecipe = copy.deepcopy(unmatched_test_recipe_with_subrecipe)
        modified_unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["names"] = [
            {
                "language": "de",
                "value": "Not K\u00fcrbisrisotto",
            }
        ]
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{modified_unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=modified_unmatched_test_recipe_with_subrecipe,
        )
        _check_that_is_unmatched(response, "'Not Kürbisrisotto'")

    # try to rematch and then unmatch using a different language
    unmatched_recipe_uid = await postgres_db.product_mgr.find_uid_by_xid(
        customer_uid, f"FoodService{unmatched_recipe_id}"
    )
    edges = await postgres_db.graph_mgr.get_sub_graph_by_uid(unmatched_recipe_uid, max_depth=1)
    assert len(edges) == 1
    flow_node: FlowNode = await postgres_db.graph_mgr.find_by_uid(edges[0].child_uid)
    flow_node.link_to_sub_node = LinkToXidProp(xid=sub_recipe_xid)
    flow_node.product_name = original_product_name

    await postgres_db.graph_mgr.upsert_node_by_uid(flow_node)

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=unmatched_test_recipe_with_subrecipe,
        )
        _check_that_matching_is_correct(response)

        # Test with a new product name with new language. Should be unmatched, and no co2 values should be returned.
        modified_unmatched_test_recipe_with_subrecipe = copy.deepcopy(unmatched_test_recipe_with_subrecipe)
        modified_unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["names"] = [
            {
                "language": "xy",
                "value": "K\u00fcrbisrisotto",
            }
        ]
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{modified_unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=modified_unmatched_test_recipe_with_subrecipe,
        )
        _check_that_is_unmatched(response, "'Kürbisrisotto'")

    # try to rematch and then make sure it is not unmatched for de-ch instead of de
    unmatched_recipe_uid = await postgres_db.product_mgr.find_uid_by_xid(
        customer_uid, f"FoodService{unmatched_recipe_id}"
    )
    edges = await postgres_db.graph_mgr.get_sub_graph_by_uid(unmatched_recipe_uid, max_depth=1)
    assert len(edges) == 1
    flow_node: FlowNode = await postgres_db.graph_mgr.find_by_uid(edges[0].child_uid)
    flow_node.link_to_sub_node = LinkToXidProp(xid=sub_recipe_xid)
    flow_node.product_name = original_product_name

    await postgres_db.graph_mgr.upsert_node_by_uid(flow_node)

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=unmatched_test_recipe_with_subrecipe,
        )
        _check_that_matching_is_correct(response)

        # Test with same product name but longer description of same language. Should not be unmatched
        modified_unmatched_test_recipe_with_subrecipe = copy.deepcopy(unmatched_test_recipe_with_subrecipe)
        modified_unmatched_test_recipe_with_subrecipe["recipe"]["ingredients"][0]["names"] = [
            {
                "language": "de-ch",
                "value": "K\u00fcrbisrisotto",
            }
        ]
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{modified_unmatched_test_recipe_with_subrecipe['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=modified_unmatched_test_recipe_with_subrecipe,
        )
        _check_that_matching_is_correct(response)


@pytest.mark.asyncio
async def test_recipe_broken_ingredient_amount_estimation(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    test_recipe_id = "test-recipe-id"
    test_recipe_with_all_fresh = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_all_fresh["recipe"]["ingredients"][1]["conservation"] = "fresh"
    test_recipe_with_all_fresh["recipe"]["id"] = test_recipe_id
    test_recipe_with_all_fresh["recipe"]["kitchen-id"] = kitchen_xid

    test_top_recipe_with_nutrients = {
        "recipe": {
            "id": "top-recipe",
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "en", "value": "Top recipe with nutrient values"}],
            "ingredients": [
                {
                    "id": test_recipe_id,
                    "type": "recipes",
                    "amount": sum(ing["amount"] for ing in test_recipe_with_all_fresh["recipe"]["ingredients"])
                    / test_recipe_with_all_fresh["recipe"]["servings"],
                    "unit": "kilogram",
                    "nutrient-values": {
                        "carbohydrates-gram": 35,
                        "water-gram": 65,
                        "protein-gram": 0,
                        "energy-kcal": 140,
                        "fat-gram": 0,
                    },
                }
            ],
        }
    }

    with mock.patch("gap_filling_modules.ingredient_amount_estimator_gfm.run", side_effect=ValueError()):
        async with AsyncClient(
            app=app_fixture,
            base_url="http://localhost:8050",
        ) as ac:
            response = await ac.post(
                "/api/recipes/batch",
                auth=(customer_token, ""),
                json=[test_recipe_with_all_fresh, test_top_recipe_with_nutrients],
            )

            assert response.status_code == 200

            top_recipe_batch = None
            sub_recipe_batch = None

            for batch_item in response.json():
                if batch_item["recipe-id"] == test_recipe_id:
                    sub_recipe_batch = batch_item
                elif batch_item["recipe-id"] == test_top_recipe_with_nutrients["recipe"]["id"]:
                    top_recipe_batch = batch_item

            assert top_recipe_batch["statuscode"] == sub_recipe_batch["statuscode"] == 200
            assert top_recipe_batch["recipe"]["co2-value"] == sub_recipe_batch["recipe"]["co2-value"] == 193
            assert top_recipe_batch["message"].startswith(
                "For at least one ingredient, nutrient subdivision was skipped"
            )


@pytest.mark.asyncio
async def test_recipe_stuck(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    test_recipe_with_stuck = copy.deepcopy(TEST_RECIPE)
    onion_unit_weight = 82.8
    test_recipe_with_stuck["recipe"]["ingredients"][1]["amount"] *= 1000 / onion_unit_weight
    test_recipe_with_stuck["recipe"]["ingredients"][1]["unit"] = "St."

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_SUPERUSER_ID}?full-resource=true",
            auth=(customer_token, ""),
            json=test_recipe_with_stuck,
        )

        assert response.status_code == 200
        expected_recipe_resp = copy.deepcopy(TEST_RECIPE)
        expected_recipe_resp["recipe"]["id"] = TEST_RECIPE_SUPERUSER_ID
        expected_recipe_resp["recipe"]["kitchen-id"] = kitchen_xid
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp["recipe-id"] = TEST_RECIPE_SUPERUSER_ID
        expected_recipe_resp["request-id"] = 0
        expected_recipe_resp["statuscode"] = 200
        expected_recipe_resp["message"] = ""
        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        )


@pytest.mark.asyncio
async def test_recipes_with_subrecipe_stuck(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    sub_recipe_xid = "test_recipe_id"

    test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_id["recipe"]["id"] = sub_recipe_xid

    test_recipe_with_subrecipe_amount_in_st: dict[str, Any] = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_subrecipe_amount_in_st["recipe"]["ingredients"][0]["unit"] = "stück"
    test_recipe_with_subrecipe_amount_in_st["recipe"]["ingredients"][0]["amount"] = (
        TEST_RECIPE_WITH_SUBRECIPE["recipe"]["ingredients"][0]["amount"]
        * SERVINGS
        / sum(ing["amount"] for ing in TEST_RECIPE["recipe"]["ingredients"])
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{sub_recipe_xid}",
            auth=(customer_token, ""),
            json=test_recipe_with_id,
        )

        assert response.status_code == 200
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{test_recipe_with_subrecipe_amount_in_st['recipe']['id']}"
            "?full-resource=true",
            auth=(customer_token, ""),
            json=test_recipe_with_subrecipe_amount_in_st,
        )
        assert response.status_code == 200
        expected_resp_recipe = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
        expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
        expected_resp_recipe["statuscode"] = 200
        expected_resp_recipe["message"] = ""
        expected_resp_recipe["request-id"] = 0
        expected_resp_recipe["recipe-id"] = "test_recipe_with_subrecipe_id"
        check_response_dict(
            expected_resp_recipe,
            response.json(),
            "recipe",
            expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="B",
            expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT,
            expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE,
            expected_ingredients_co2_value=[
                TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
                TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE,
            ],
            expected_ingredients_co2_rating=["A", "C"],
            expected_ingredients_bar_chart=[46, 54],
            expected_ingredients_food_unit=[
                TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
                TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
            ],
            expected_ingredients_co2_improvement_percentage=[
                TEST_RECIPE_IN_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
                TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_IMPROVEMENT_PERCENTAGE,
            ],
            expected_ingredients_co2_reduction_value=[
                TEST_RECIPE_IN_SUBRECIPE_REDUCTION_VALUE,
                TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_REDUCTION,
            ],
        )


@pytest.mark.asyncio
async def test_recipes_with_subrecipe_nonexistent_subrecipe(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    sub_recipe_xid = "test_recipe_id"

    test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_id["recipe"]["id"] = sub_recipe_xid
    test_recipe_with_id["recipe"]["kitchen-id"] = kitchen_xid

    test_recipes_with_subrecipe_with_kitchen_id = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipes_with_subrecipe_with_kitchen_id["recipe"]["kitchen-id"] = kitchen_xid
    # Insert another ingredient such that we can Mock raise ForeignKeyViolationError depending on len(edges).
    test_recipes_with_subrecipe_with_kitchen_id["recipe"]["ingredients"].append(
        test_recipes_with_subrecipe_with_kitchen_id["recipe"]["ingredients"][-1]
    )

    async def side_effect_function(
        edges_to_delete: list[Edge],  # noqa ARG001
        edges_to_insert: list[Edge],  # noqa ARG001
        lock_on_uid: Optional[str] = None,  # noqa ARG001
        md5_hash: Optional[uuid.UUID] = None,  # noqa ARG001
    ) -> None:
        # Raise an exception if the length of edges is 3
        if len(edges_to_insert) == 3:
            raise ForeignKeyViolationError("Foreign Key Constraint Violated.")
        else:
            await ServiceLocator().service_provider.postgres_db.graph_mgr.delete_then_insert_many_edges(
                edges_to_delete, edges_to_insert, lock_on_uid=lock_on_uid, md5_hash=md5_hash
            )

    with mock.patch(
        "core.service.node_service.NodeService.delete_then_insert_many_edges",
        side_effect=side_effect_function,
    ):
        async with AsyncClient(
            app=app_fixture,
            base_url="http://localhost:8050",
        ) as ac:
            response = await ac.post(
                "/api/recipes/batch",
                auth=(customer_token, ""),
                json=[test_recipe_with_id, test_recipes_with_subrecipe_with_kitchen_id],
            )

    assert response.status_code == 200

    assert response.json()[0]["statuscode"] == 612  # status code for missing subrecipe.

    expected_recipe_resp = copy.deepcopy(test_recipe_with_id)
    expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
    expected_recipe_resp["recipe"]["ingredients"] = []
    expected_recipe_resp["message"] = ""
    expected_recipe_resp["recipe-id"] = response.json()[1]["recipe-id"]
    expected_recipe_resp["request-id"] = response.json()[1]["request-id"]
    expected_recipe_resp["statuscode"] = 200
    check_response_dict(
        expected_recipe_resp,
        response.json()[1],
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="A",
        expect_eaternity_award_field=True,
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
    )


@pytest.mark.asyncio
async def test_recipes_batch(
    app_fixture: AppFixtureReturn,
    create_simple_customer_and_kitchen: tuple[str, str, str],
    load_tomato_and_onion_nutrients: dict[str, dict[str, float]],
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen

    test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_id["recipe"]["id"] = "test_recipe_id"

    test_recipe_circular = copy.deepcopy(TEST_RECIPE)
    test_recipe_circular["recipe"]["id"] = "test_recipe_id"
    test_recipe_circular["recipe"]["ingredients"].append(
        {
            "transport": "air",
            "processing": "raw",
            "packaging": "plastic",
            "id": "test_recipe_with_subrecipe_id",
            "names": [{"language": "de", "value": "Parent Recipe with child recipes"}],
            "location": "spain",
            "production": "greenhouse",
            "conservation": "fresh",
            "type": "recipes",
            "amount": 50,
            "unit": "kilogram",
        },
    )

    test_recipe_with_wrong_subrecipe = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_wrong_subrecipe["recipe"]["ingredients"][0]["id"] = "wrong-recipe-id"

    test_recipe_transient: dict[str, dict | bool] = copy.deepcopy(test_recipe_with_id)
    test_recipe_transient["recipe"]["id"] = "test-recipe-transient"
    test_recipe_transient["transient"] = True

    test_recipe_with_subrecipe_transient = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_subrecipe_transient["recipe"]["ingredients"][0]["id"] = "test-recipe-transient"

    test_recipe_all_transient: dict[str, dict | bool] = copy.deepcopy(test_recipe_with_id)
    test_recipe_all_transient["recipe"]["id"] = "test-recipe-all-transient"
    test_recipe_with_subrecipe_all_transient = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_subrecipe_all_transient["recipe"]["ingredients"][0]["id"] = "test-recipe-all-transient"
    test_recipe_with_subrecipe_all_transient["recipe"]["id"] = "test_recipe_with_subrecipe_all_transient"

    test_recipe_with_subrecipe_no_id = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    del test_recipe_with_subrecipe_no_id["recipe"]["id"]

    test_recipe_with_subrecipe_with_missing_matching = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    old_ingredient_name = test_recipe_with_subrecipe_with_missing_matching["recipe"]["ingredients"][1]["names"][0][
        "value"
    ]
    test_recipe_with_subrecipe_with_missing_matching["recipe"]["ingredients"][1]["names"][0][
        "value"
    ] = f"strange_name_for_{old_ingredient_name}"

    circular_batch = [{**test_recipe_circular}, {**TEST_RECIPE_WITH_SUBRECIPE}]
    incorrectly_linked_batch = [{**test_recipe_with_id}, {**test_recipe_with_wrong_subrecipe}]
    repeated_batch = [
        {**test_recipe_with_id},
        {**test_recipe_with_id},
        {**TEST_RECIPE_WITH_SUBRECIPE},
        {**TEST_RECIPE_WITH_SUBRECIPE},
    ]
    valid_batch = [{**test_recipe_with_subrecipe_no_id}, {**test_recipe_with_id}]

    batch_with_missing_matching = [{**test_recipe_with_subrecipe_with_missing_matching}, {**test_recipe_with_id}]

    test_recipe_with_subrecipe_new_amount = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    for ingredient in test_recipe_with_subrecipe_new_amount["recipe"]["ingredients"]:
        ingredient["amount"] *= 2
    test_recipe_with_subrecipe_new_amount["request-id"] = 0

    valid_batch_with_repeated_recipe_id = [
        {**TEST_RECIPE_WITH_SUBRECIPE},
        {**test_recipe_with_id},
        {**test_recipe_with_subrecipe_new_amount},
    ]
    transient_batch = [{**test_recipe_transient}, {**test_recipe_with_subrecipe_transient}]
    all_transient_batch = [{**test_recipe_all_transient}, {**test_recipe_with_subrecipe_all_transient}]

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response_circular = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=circular_batch,
        )

        response_repeated = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=repeated_batch,
        )

        response_unlinked = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=incorrectly_linked_batch,
        )

        response_valid = await ac.post(
            "/api/recipes/batch?full-resource=true&nutrient-values=true",
            auth=(customer_token, ""),
            json=valid_batch,
        )

        response_with_missing_matching = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=batch_with_missing_matching,
        )

        response_valid_with_repeated_recipe_id = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=valid_batch_with_repeated_recipe_id,
        )

        response_transient = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=transient_batch,
        )

        response_transient_all = await ac.post(
            "/api/recipes/batch?full-resource=true&transient=true",
            auth=(customer_token, ""),
            json=all_transient_batch,
        )

    # Assert that request ID is arranged in ascending order.
    assert response_circular.json()[0]["request-id"] < response_circular.json()[1]["request-id"]

    assert response_circular.json()[0]["statuscode"] == 400
    assert response_circular.json()[1]["statuscode"] == 400

    response_repeated_json = response_repeated.json()
    assert response_repeated_json[0]["statuscode"] == 200
    assert response_repeated_json[1]["statuscode"] == 200
    top_recipe = response_repeated_json[0]
    sub_recipe = response_repeated_json[1]

    assert top_recipe["message"].startswith("Request ID '1' in multiple batch items.")
    assert sub_recipe["message"].startswith("Recipe ID 'test_recipe_id' in multiple batch items.")

    for recipe in (top_recipe, sub_recipe):
        del recipe["statuscode"]
        del recipe["message"]
        del recipe["recipe-id"]

    del sub_recipe["request-id"]
    del sub_recipe["recipe"]["kitchen-id"]

    expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
    expected_resp_recipe["recipe"]["ingredients"] = []
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        sub_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="A",
        expect_eaternity_award_field=True,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
    )

    expected_resp_recipe = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    expected_resp_recipe["recipe"]["ingredients"] = []
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    expected_resp_recipe["request-id"] = int(expected_resp_recipe["request-id"])
    check_response_dict(
        expected_resp_recipe,
        top_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="B",
        expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT,
        expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE,
    )

    invalid_item = response_unlinked.json()[0]
    valid_item = response_unlinked.json()[1]
    assert invalid_item["statuscode"] == 400

    # TODO: we should instead also check these two fields for correctness:
    del valid_item["request-id"]
    del valid_item["recipe"]["kitchen-id"]

    expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["recipe-id"] = "test_recipe_id"
    expected_resp_recipe["message"] = ""
    check_response_dict(
        expected_resp_recipe,
        valid_item,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expect_eaternity_award_field=True,
        expected_co2_rating="A",
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
        expected_ingredients_co2_rating=["A", "B"],
        expected_ingredients_bar_chart=[0, 100],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        expected_ingredients_co2_improvement_percentage=[
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
        ],
        expected_ingredients_co2_reduction_value=[
            EXPECTED_CO2_REDUCTION_VALUE_ING_1,
            EXPECTED_CO2_REDUCTION_VALUE_ING_2,
        ],
    )

    response_valid_json = response_valid.json()
    top_recipe = response_valid_json[0]
    sub_recipe = response_valid_json[1]
    assert response_valid_json[0]["request-id"] < response_valid_json[1]["request-id"]

    assigned_recipe_uid = uuid.UUID(response_valid_json[0]["recipe"]["id"])
    assert isinstance(assigned_recipe_uid, uuid.UUID)
    assert str(assigned_recipe_uid) == response_valid_json[0]["recipe"]["id"]

    with pytest.raises(ValueError):
        uuid.UUID(response_valid_json[1]["recipe"]["id"])

    for recipe in (top_recipe, sub_recipe):
        del recipe["statuscode"]
        del recipe["message"]
        del recipe["recipe-id"]

    del sub_recipe["request-id"]
    del sub_recipe["recipe"]["kitchen-id"]
    expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    nutrition_data = [val for key, val in load_tomato_and_onion_nutrients.items() if key in ("tomato", "dried_onion")]

    expected_resp_recipe_with_nutrients = add_nutrients_to_expected_response(expected_resp_recipe, nutrition_data)

    check_response_dict(
        expected_resp_recipe_with_nutrients,
        sub_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="A",
        expect_eaternity_award_field=True,
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
        expected_ingredients_co2_rating=["A", "B"],
        expected_ingredients_bar_chart=[0, 100],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        expected_ingredients_co2_improvement_percentage=[
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
        ],
        expected_ingredients_co2_reduction_value=[
            EXPECTED_CO2_REDUCTION_VALUE_ING_1,
            EXPECTED_CO2_REDUCTION_VALUE_ING_2,
        ],
    )

    sub_recipe_nutrients = {}
    for ingredient in expected_resp_recipe_with_nutrients["recipe"]["ingredients"]:
        for key, val in ingredient["nutrient-values"].items():
            if key in sub_recipe_nutrients:
                sub_recipe_nutrients[key] += val / 2280
            else:
                sub_recipe_nutrients[key] = val / 2280

    nutrition_data = [sub_recipe_nutrients, load_tomato_and_onion_nutrients["tomato"]]

    test_recipe_with_subrecipe_with_new_id = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_subrecipe_with_new_id["request-id"] = int(test_recipe_with_subrecipe_with_new_id["request-id"])
    test_recipe_with_subrecipe_with_new_id["recipe"]["id"] = str(assigned_recipe_uid)
    test_recipe_with_subrecipe_with_new_id["recipe"]["recipe-portions"] = test_recipe_with_subrecipe_with_new_id[
        "recipe"
    ]["servings"]
    check_response_dict(
        add_nutrients_to_expected_response(test_recipe_with_subrecipe_with_new_id, nutrition_data),
        top_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="B",
        expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[
            TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE,
        ],
        expected_ingredients_co2_rating=["A", "C"],
        expected_ingredients_bar_chart=[46, 54],
        expected_ingredients_food_unit=[
            TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
            TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
        ],
        expected_ingredients_co2_improvement_percentage=[
            TEST_RECIPE_IN_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_IMPROVEMENT_PERCENTAGE,
        ],
        expected_ingredients_co2_reduction_value=[
            TEST_RECIPE_IN_SUBRECIPE_REDUCTION_VALUE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_REDUCTION,
        ],
    )

    invalid_item = response_with_missing_matching.json()[0]
    valid_item = response_with_missing_matching.json()[1]
    assert invalid_item["statuscode"] == 602
    assert invalid_item["message"] == (
        "At least one ingredient name could not get automatically matched into "
        "the Eaternity Database. "
        "Retry once a day or try with a different ingredient name. "
        "Names that couldn't be matched: [['strange_name_for_Tomaten']]."
    )

    del valid_item["request-id"]
    del valid_item["recipe"]["kitchen-id"]

    expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["recipe-id"] = "test_recipe_id"
    expected_resp_recipe["message"] = ""
    check_response_dict(
        expected_resp_recipe,
        valid_item,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="A",
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
        expected_ingredients_co2_rating=["A", "B"],
        expected_ingredients_bar_chart=[0, 100],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        expected_ingredients_co2_improvement_percentage=[
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
        ],
        expected_ingredients_co2_reduction_value=[
            EXPECTED_CO2_REDUCTION_VALUE_ING_1,
            EXPECTED_CO2_REDUCTION_VALUE_ING_2,
        ],
    )

    response_valid_with_repeated_recipe_id_json = response_valid_with_repeated_recipe_id.json()
    top_recipe = response_valid_with_repeated_recipe_id_json[0]
    top_recipe_2 = response_valid_with_repeated_recipe_id_json[1]
    sub_recipe = response_valid_with_repeated_recipe_id_json[2]

    assert top_recipe["message"].startswith("Recipe ID 'test_recipe_with_subrecipe_id' in multiple batch items.")
    assert top_recipe_2["message"].startswith("Recipe ID 'test_recipe_with_subrecipe_id' in multiple batch items.")
    assert sub_recipe["message"] == ""

    expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]

    for recipe in (top_recipe, top_recipe_2, sub_recipe):
        del recipe["statuscode"]
        del recipe["message"]
        del recipe["recipe-id"]

    del sub_recipe["request-id"]
    del sub_recipe["recipe"]["kitchen-id"]
    check_response_dict(
        expected_resp_recipe,
        sub_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expect_eaternity_award_field=True,
        expected_co2_rating="A",
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
        expected_ingredients_co2_rating=["A", "B"],
        expected_ingredients_bar_chart=[0, 100],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        expected_ingredients_co2_improvement_percentage=[
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
        ],
        expected_ingredients_co2_reduction_value=[
            EXPECTED_CO2_REDUCTION_VALUE_ING_1,
            EXPECTED_CO2_REDUCTION_VALUE_ING_2,
        ],
    )

    expected_resp_recipe = copy.deepcopy(test_recipe_with_subrecipe_new_amount)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        top_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE * 2 - 1,  # 1 due to rounding
        expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT * 2,  # -0.01 due to rounding.
        expected_co2_rating="B",
        expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE * 2,
        expected_ingredients_co2_value=[
            TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE * 2 + 1,  # 1 due to rounding
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE * 2 - 1,  # 1 due to rounding.
        ],
        expected_ingredients_co2_rating=["A", "C"],
        expected_ingredients_bar_chart=[46, 54],
        expected_ingredients_food_unit=[
            TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT * 2,  # 0.01 due to rounding
            TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT * 2,
        ],
        expected_ingredients_co2_improvement_percentage=[
            TEST_RECIPE_IN_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_IMPROVEMENT_PERCENTAGE,
        ],
        expected_ingredients_co2_reduction_value=[
            TEST_RECIPE_IN_SUBRECIPE_REDUCTION_VALUE * 2 - 1,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_REDUCTION * 2,
        ],
    )

    expected_resp_recipe = copy.deepcopy(test_recipe_with_subrecipe_new_amount)
    expected_resp_recipe["request-id"] = 1
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        top_recipe_2,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE * 2 - 1,  # 1 due to rounding.
        expected_daily_food_unit=(TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT * 2),
        expected_co2_rating="B",
        expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE * 2,
        expected_ingredients_co2_value=[
            TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE * 2 + 1,  # 1 due to rounding.
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE * 2 - 1,  # 1 due to rounding.
        ],
        expected_ingredients_co2_rating=["A", "C"],
        expected_ingredients_bar_chart=[46, 54],
        expected_ingredients_food_unit=[
            TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT * 2,  # 0.01 due to rounding
            TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT * 2,
        ],
        expected_ingredients_co2_improvement_percentage=[
            TEST_RECIPE_IN_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_IMPROVEMENT_PERCENTAGE,
        ],
        expected_ingredients_co2_reduction_value=[
            TEST_RECIPE_IN_SUBRECIPE_REDUCTION_VALUE * 2 - 1,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_REDUCTION * 2,
        ],
    )

    assert response_transient.status_code == 200
    assert response_transient_all.status_code == 200
    response_transient_all_json = response_transient_all.json()

    top_recipe = response_transient_all_json[0]
    sub_recipe = response_transient_all_json[1]

    for recipe in (top_recipe, sub_recipe):
        del recipe["statuscode"]
        del recipe["message"]
        del recipe["recipe-id"]

    del sub_recipe["request-id"]
    del sub_recipe["recipe"]["kitchen-id"]

    expected_resp_recipe = copy.deepcopy(test_recipe_all_transient)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    check_response_dict(
        expected_resp_recipe,
        sub_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="A",
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
        expected_ingredients_co2_rating=["A", "B"],
        expected_ingredients_bar_chart=[0, 100],
        expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
        expected_ingredients_co2_improvement_percentage=[
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
        ],
        expected_ingredients_co2_reduction_value=[
            EXPECTED_CO2_REDUCTION_VALUE_ING_1,
            EXPECTED_CO2_REDUCTION_VALUE_ING_2,
        ],
    )

    expected_resp_recipe = copy.deepcopy(test_recipe_with_subrecipe_all_transient)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    expected_resp_recipe["request-id"] = int(expected_resp_recipe["request-id"])
    check_response_dict(
        expected_resp_recipe,
        top_recipe,
        "recipe",
        expected_co2_value=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="B",
        expected_co2_improvement_percentage=TEST_RECIPE_WITH_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=TEST_RECIPE_WITH_SUBRECIPE_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[
            TEST_RECIPE_LEGACY_CO2_VALUE_IN_SUBRECIPE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_CO2_VALUE,
        ],
        expected_ingredients_co2_rating=["A", "C"],
        expected_ingredients_bar_chart=[46, 54],
        expected_ingredients_food_unit=[
            TEST_RECIPE_IN_SUBRECIPE_LEGACY_FOOD_UNIT,
            TEST_RECIPE_WITH_SUBRECIPE_OTHER_INGREDIENT_LEGACY_FOOD_UNIT,
        ],
        expected_ingredients_co2_improvement_percentage=[
            TEST_RECIPE_IN_SUBRECIPE_IMPROVEMENT_PERCENTAGE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_IMPROVEMENT_PERCENTAGE,
        ],
        expected_ingredients_co2_reduction_value=[
            TEST_RECIPE_IN_SUBRECIPE_REDUCTION_VALUE,
            TEST_RECIPE_WITH_SUBRECIPE_LEGACY_OTHER_INGREDIENT_REDUCTION,
        ],
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        get_response_sub_recipe = await ac.get(
            "api/recipes/test_recipe_id?full-resource=true",
            auth=(customer_token, ""),
        )

        assert get_response_sub_recipe.status_code == 200
        response_json = get_response_sub_recipe.json()
        del response_json["recipe"]["kitchen-id"]
        expected_resp_recipe = copy.deepcopy(test_recipe_with_id)
        expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
        expected_resp_recipe["statuscode"] = 200
        expected_resp_recipe["message"] = ""
        expected_resp_recipe["recipe-id"] = "test_recipe_id"
        check_response_dict(
            expected_resp_recipe,
            response_json,
            "recipe",
            expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
            expected_co2_rating="A",
            expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
            expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
            expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
            expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "B"],
            expected_ingredients_bar_chart=[0, 100],
            expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
        )

        get_response_transient_recipe = await ac.get(
            "api/recipes/test-recipe-transient?full-resource=true",
            auth=(customer_token, ""),
        )

        assert get_response_transient_recipe.status_code == 404

        get_response_all_transient_recipe = await ac.get(
            "api/recipes/test_recipe_with_subrecipe_all_transient?full-resource=true",
            auth=(customer_token, ""),
        )

        assert get_response_all_transient_recipe.status_code == 404


@pytest.mark.asyncio
async def test_recipes_batch_with_repeated_ingredients(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, _, kitchen_xid = create_simple_customer_and_kitchen

    onion_expected_co2 = 0.3145270365097423 * 1000  # update 31.07: value changed because of updated fao-statistics
    onion_expected_dfu = 0.2104259687272727
    dfu_reduction_due_to_weight = 0.033333333333333  # DFU is smaller since (weight) < (sum of ingredient weights).
    carrot_expected_co2 = 0.012979745398003872 * 1000
    carrot_expected_dfu = 0.19288297842424243

    sub_recipe_1003 = {
        "request-id": 4,
        "recipe": {
            "id": "1003",
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": "Salat"}],
            "weight": 900.0,
            "recipe-portions": 1,
            "production-portions": 120,
            "sold-portions": 65,
            "date": "2000-08-17",
            "ingredients": [
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Zwiebeln"}],
                    "amount": 1000.0,
                    "unit": "gram",
                },
            ],
        },
    }
    sub_recipe_1004 = {
        "request-id": 5,
        "recipe": {
            "id": "1004",
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": "Salat"}],
            "recipe-portions": 1,
            "production-portions": 120,
            "weight": 1500.0,
            "sold-portions": 65,
            "date": "2000-08-17",
            "ingredients": [
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    # names field is overwritten by the second "onion".
                    "names": [{"language": "de", "value": "UNMATCHED UNKNOWN"}],
                    "amount": 400.0,
                    "unit": "gram",
                },
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Zwiebeln"}],
                    "amount": 600.0,
                    "unit": "gram",
                },
                {
                    "id": "carrot",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Karotten"}],
                    "amount": 500.0,
                    "unit": "gram",
                },
            ],
        },
    }

    recipe_2006 = {
        "request-id": 6,
        "recipe": {
            "id": int(2006),
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": float(125.273)}],
            "servings": 100,
            "production-portions": 0,
            "sold-portions": 50,
            "weight": 579.0,
            "date": "2000-08-17",
            "ingredients": [
                {"id": "1003", "type": "recipes", "amount": 12300.0, "unit": "gram"},
                {"id": "1004", "type": "recipes", "amount": 45600.0, "unit": "gram"},
            ],
        },
    }

    batch_with_repeated_ingredients = [sub_recipe_1003, sub_recipe_1004, recipe_2006]

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=batch_with_repeated_ingredients,
        )

    assert response.status_code == 200

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/1003",
            auth=(customer_token, ""),
        )
    response_json_1003 = response.json()

    expected_resp_recipe = copy.deepcopy(sub_recipe_1003)
    expected_resp_recipe["recipe"]["servings"] = expected_resp_recipe["recipe"]["recipe-portions"]
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["message"] = ""
    expected_resp_recipe["recipe-id"] = "1003"

    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_1003,
        "recipe",
        expected_co2_value=round(onion_expected_co2),
        expected_co2_rating="A",
        expected_co2_improvement_percentage=54,
        expected_co2_reduction_value=375,
        expected_daily_food_unit=onion_expected_dfu - dfu_reduction_due_to_weight,
        expected_ingredients_co2_value=[round(onion_expected_co2)],
        expected_ingredients_co2_rating=["A"],
        expected_ingredients_bar_chart=[100],
        expected_ingredients_food_unit=[onion_expected_dfu],
        expected_ingredients_co2_improvement_percentage=[62],
        expected_ingredients_co2_reduction_value=[505],
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/1004",
            auth=(customer_token, ""),
        )
    response_json_1004 = response.json()

    expected_resp_recipe = copy.deepcopy(sub_recipe_1004)
    expected_resp_recipe["recipe"]["servings"] = expected_resp_recipe["recipe"]["recipe-portions"]
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["message"] = ""
    expected_resp_recipe["recipe-id"] = "1004"
    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_1004,
        "recipe",
        expected_co2_value=round(onion_expected_co2 + 0.5 * carrot_expected_co2),
        expected_co2_rating="A",
        expect_eaternity_award_field=True,
        expected_co2_improvement_percentage=73,
        expected_co2_reduction_value=874,
        expected_daily_food_unit=onion_expected_dfu + 0.5 * carrot_expected_dfu,
        expected_ingredients_co2_value=[
            round(0.4 * onion_expected_co2),
            round(0.6 * onion_expected_co2),
            round(0.5 * carrot_expected_co2),
        ],
        expected_ingredients_co2_rating=["A", "A", "A"],
        expected_ingredients_bar_chart=[39, 59, 2],
        expected_ingredients_food_unit=[
            0.4 * onion_expected_dfu,
            0.6 * onion_expected_dfu,
            0.5 * carrot_expected_dfu,
        ],
        expected_ingredients_co2_improvement_percentage=[62, 62, 98],
        expected_ingredients_co2_reduction_value=[202, 303, 369],
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/2006",
            auth=(customer_token, ""),
        )
    response_json_2006 = response.json()

    expected_resp_recipe = copy.deepcopy(recipe_2006)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    expected_resp_recipe["recipe"]["id"] = str(expected_resp_recipe["recipe"]["id"])
    expected_resp_recipe["recipe"]["titles"][0]["value"] = str(expected_resp_recipe["recipe"]["titles"][0]["value"])
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["message"] = ""
    expected_resp_recipe["recipe-id"] = "2006"
    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_2006,
        "recipe",
        expected_co2_value=round(
            (onion_expected_co2 + 0.5 * carrot_expected_co2) * 0.456 / 1.5 + onion_expected_co2 * 0.123 / 0.9
        ),
        expected_co2_rating="A",
        expected_co2_improvement_percentage=69,
        expected_co2_reduction_value=317,
        expect_eaternity_award_field=True,
        expected_daily_food_unit=(
            (onion_expected_dfu + 0.5 * carrot_expected_dfu) * 0.456 / 1.5
            + (onion_expected_dfu - dfu_reduction_due_to_weight) * (0.123 / 0.9)
        ),
        expected_ingredients_co2_value=[
            round(onion_expected_co2 * 12.3 / 0.9),
            round((onion_expected_co2 + 0.5 * carrot_expected_co2) * 45.6 / 1.5),
        ],
        expected_ingredients_co2_rating=["A", "A"],
        expected_ingredients_bar_chart=[31, 69],
        expected_ingredients_food_unit=[
            (onion_expected_dfu - dfu_reduction_due_to_weight) * 12.3 / 0.9,
            (onion_expected_dfu + 0.5 * carrot_expected_dfu) * 45.6 / 1.5,
        ],
        expected_ingredients_co2_improvement_percentage=[54, 73],
        expected_ingredients_co2_reduction_value=[5125, 26563],
    )


@pytest.mark.asyncio
async def test_recipes_batch_with_repeated_ingredients_ignoring_weights(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_namespace_uid, kitchen_xid = create_simple_customer_and_kitchen
    kitchen_access_group = await postgres_db.access_group_mgr.get_by_xid(kitchen_xid, customer_namespace_uid)
    kitchen_access_group.data["ignore_fields"] = ["production_amount"]
    await postgres_db.access_group_mgr.upsert_access_group_by_uid(kitchen_access_group)

    onion_expected_co2 = 0.3145270365097423 * 1000  # update 31.07: value changed because of updated fao-statistics
    onion_expected_dfu = 0.2104259687272727
    dfu_reduction_due_to_weight = 0.0  # Weight field should be ignored now
    carrot_expected_co2 = 0.012979745398003872 * 1000
    carrot_expected_dfu = 0.19288297842424243

    sub_recipe_1003 = {
        "request-id": 4,
        "recipe": {
            "id": "1003",
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": "Salat"}],
            "weight": 900.0,
            "recipe-portions": 1,
            "production-portions": 120,
            "sold-portions": 65,
            "date": "2000-08-17",
            "ingredients": [
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Zwiebeln"}],
                    "amount": 1000.0,
                    "unit": "gram",
                },
            ],
        },
    }
    sub_recipe_1004 = {
        "request-id": 5,
        "recipe": {
            "id": "1004",
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": "Salat"}],
            "recipe-portions": 1,
            "production-portions": 120,
            "weight": 1500.0,
            "sold-portions": 65,
            "date": "2000-08-17",
            "ingredients": [
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    # names field is overwritten by the second "onion".
                    "names": [{"language": "de", "value": "UNMATCHED UNKNOWN"}],
                    "amount": 400.0,
                    "unit": "gram",
                },
                {
                    "id": "onion",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Zwiebeln"}],
                    "amount": 600.0,
                    "unit": "gram",
                },
                {
                    "id": "carrot",
                    "type": "conceptual-ingredients",
                    "names": [{"language": "de", "value": "Karotten"}],
                    "amount": 500.0,
                    "unit": "gram",
                },
            ],
        },
    }

    recipe_2006 = {
        "request-id": 6,
        "recipe": {
            "id": int(2006),
            "kitchen-id": kitchen_xid,
            "titles": [{"language": "de", "value": float(125.273)}],
            "servings": 100,
            "production-portions": 0,
            "sold-portions": 50,
            "weight": 579.0,
            "date": "2000-08-17",
            "ingredients": [
                {"id": "1003", "type": "recipes", "amount": 12300.0, "unit": "gram"},
                {"id": "1004", "type": "recipes", "amount": 45600.0, "unit": "gram"},
            ],
        },
    }

    batch_with_repeated_ingredients = [sub_recipe_1003, sub_recipe_1004, recipe_2006]

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=batch_with_repeated_ingredients,
        )

    assert response.status_code == 200

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/1003",
            auth=(customer_token, ""),
        )
    response_json_1003 = response.json()

    expected_resp_recipe = copy.deepcopy(sub_recipe_1003)
    expected_resp_recipe["recipe"]["servings"] = expected_resp_recipe["recipe"]["recipe-portions"]
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["message"] = ""
    expected_resp_recipe["recipe-id"] = "1003"

    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_1003,
        "recipe",
        expected_co2_value=round(onion_expected_co2),
        expected_co2_rating="A",
        expected_co2_improvement_percentage=62,
        expected_co2_reduction_value=505,
        expected_daily_food_unit=onion_expected_dfu - dfu_reduction_due_to_weight,
        expected_ingredients_co2_value=[round(onion_expected_co2)],
        expected_ingredients_co2_rating=["A"],
        expected_ingredients_bar_chart=[100],
        expected_ingredients_food_unit=[onion_expected_dfu],
        expected_ingredients_co2_improvement_percentage=[62],
        expected_ingredients_co2_reduction_value=[505],
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/1004",
            auth=(customer_token, ""),
        )
    response_json_1004 = response.json()

    expected_resp_recipe = copy.deepcopy(sub_recipe_1004)
    expected_resp_recipe["recipe"]["servings"] = expected_resp_recipe["recipe"]["recipe-portions"]
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["message"] = ""
    expected_resp_recipe["recipe-id"] = "1004"
    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_1004,
        "recipe",
        expected_co2_value=round(onion_expected_co2 + 0.5 * carrot_expected_co2),
        expected_co2_rating="A",
        expect_eaternity_award_field=True,
        expected_co2_improvement_percentage=73,
        expected_co2_reduction_value=874,
        expected_daily_food_unit=onion_expected_dfu + 0.5 * carrot_expected_dfu,
        expected_ingredients_co2_value=[
            round(0.4 * onion_expected_co2),
            round(0.6 * onion_expected_co2),
            round(0.5 * carrot_expected_co2),
        ],
        expected_ingredients_co2_rating=["A", "A", "A"],
        expected_ingredients_bar_chart=[39, 59, 2],
        expected_ingredients_food_unit=[
            0.4 * onion_expected_dfu,
            0.6 * onion_expected_dfu,
            0.5 * carrot_expected_dfu,
        ],
        expected_ingredients_co2_improvement_percentage=[62, 62, 98],
        expected_ingredients_co2_reduction_value=[202, 303, 369],
    )

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/2006",
            auth=(customer_token, ""),
        )
    response_json_2006 = response.json()

    expected_resp_recipe = copy.deepcopy(recipe_2006)
    expected_resp_recipe["recipe"]["recipe-portions"] = expected_resp_recipe["recipe"]["servings"]
    expected_resp_recipe["recipe"]["id"] = str(expected_resp_recipe["recipe"]["id"])
    expected_resp_recipe["recipe"]["titles"][0]["value"] = str(expected_resp_recipe["recipe"]["titles"][0]["value"])
    expected_resp_recipe["statuscode"] = 200
    expected_resp_recipe["message"] = ""
    expected_resp_recipe["recipe-id"] = "2006"
    del expected_resp_recipe["request-id"]
    check_response_dict(
        expected_resp_recipe,
        response_json_2006,
        "recipe",
        expected_co2_value=round(
            (onion_expected_co2 + 0.5 * carrot_expected_co2) * 0.456 / 1.5
            + onion_expected_co2 * 0.123  # no renormalization by 0.9 due to ignored weight
        ),
        expected_co2_rating="A",
        expected_co2_improvement_percentage=71,
        expected_co2_reduction_value=328,
        expect_eaternity_award_field=True,
        expected_daily_food_unit=(
            (onion_expected_dfu + 0.5 * carrot_expected_dfu) * 0.456 / 1.5
            + (onion_expected_dfu - dfu_reduction_due_to_weight)
            * 0.123  # no renormalization by 0.9 due to ignored weight
        ),
        expected_ingredients_co2_value=[
            round(onion_expected_co2 * 12.3),  # no renormalization by 0.9 due to ignored weight
            round((onion_expected_co2 + 0.5 * carrot_expected_co2) * 45.6 / 1.5),
        ],
        expected_ingredients_co2_rating=["A", "A"],
        expected_ingredients_bar_chart=[28, 72],
        expected_ingredients_food_unit=[
            (onion_expected_dfu - dfu_reduction_due_to_weight) * 12.3,
            (onion_expected_dfu + 0.5 * carrot_expected_dfu) * 45.6 / 1.5,
        ],
        expected_ingredients_co2_improvement_percentage=[62, 73],
        expected_ingredients_co2_reduction_value=[6209, 26563],
    )


@pytest.mark.asyncio
async def test_calculation_with_subrecipe_with_different_activity_dates(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    test_recipe_with_greenhouse = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_greenhouse["recipe"]["ingredients"][0]["origin"] = "france"
    test_recipe_with_greenhouse["recipe"]["id"] = "test_recipe_id"
    valid_batch = [{**TEST_RECIPE_WITH_SUBRECIPE}, {**test_recipe_with_greenhouse}]
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=valid_batch,
        )

    assert response.status_code == 200
    response_json = response.json()
    top_recipe = response_json[0]

    tomato_greenhouse_per_gram = (
        TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_GREENHOUSE_CONTRIBUTION
        / TEST_RECIPE_WITH_SUBRECIPE["recipe"]["ingredients"][1]["amount"]
    )

    sub_recipe_amount_in_main_recipe = TEST_RECIPE_WITH_SUBRECIPE["recipe"]["ingredients"][0]["amount"]
    sub_recipe_production_amount = sum(TEST_RECIPE["recipe"]["ingredients"][i]["amount"] for i in range(2))
    sub_recipe_tomato_amount = TEST_RECIPE["recipe"]["ingredients"][0]["amount"]

    additional_greenhouse_contribution = (
        tomato_greenhouse_per_gram
        * sub_recipe_tomato_amount
        / sub_recipe_production_amount
        * sub_recipe_amount_in_main_recipe
    )

    expected_test_recipe_with_subrecipe: dict = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    expected_test_recipe_with_subrecipe["recipe"]["recipe-portions"] = expected_test_recipe_with_subrecipe["recipe"][
        "servings"
    ]
    expected_test_recipe_with_subrecipe["statuscode"] = 200
    expected_test_recipe_with_subrecipe["message"] = ""
    expected_test_recipe_with_subrecipe["recipe-id"] = "test_recipe_with_subrecipe_id"
    expected_test_recipe_with_subrecipe["recipe"]["ingredients"] = []
    expected_test_recipe_with_subrecipe["request-id"] = int(expected_test_recipe_with_subrecipe["request-id"])

    check_response_dict(
        expected_test_recipe_with_subrecipe,
        top_recipe,
        "recipe",
        expected_co2_value=round(
            (TEST_RECIPE_WITH_SUBRECIPE_CO2_VALUE + additional_greenhouse_contribution * 1000 * 1000) / SERVINGS
        ),
        expected_co2_rating="B",
        expected_co2_improvement_percentage=22,
        expected_co2_reduction_value=229,
        expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT,
    )

    test_recipe_with_greenhouse_new_date = copy.deepcopy(test_recipe_with_greenhouse)
    test_recipe_with_greenhouse_new_date["recipe"]["date"] = "2023-09-20"

    valid_batch_new_date = [{**TEST_RECIPE_WITH_SUBRECIPE}, {**test_recipe_with_greenhouse_new_date}]

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=valid_batch_new_date,
        )

    assert response.status_code == 200
    response_json = response.json()
    top_recipe = response_json[0]

    check_response_dict(
        expected_test_recipe_with_subrecipe,
        top_recipe,
        "recipe",
        expected_co2_value=round(
            (TEST_RECIPE_WITH_SUBRECIPE_CO2_VALUE + additional_greenhouse_contribution * 1000 * 1000) / SERVINGS
        ),
        expected_co2_rating="B",
        expected_co2_improvement_percentage=22,
        expected_co2_reduction_value=229,
        expected_daily_food_unit=TEST_RECIPE_WITH_SUBRECIPE_LEGACY_FOOD_UNIT,
    )


async def get_recipe_from_new_api(recipe_xid: str, kitchen_xid: str, customer_token: str) -> dict:
    batch_to_get = [
        {
            "input_root": {
                "existing_root": {
                    "xid": recipe_xid,
                    "access_group_xid": kitchen_xid,
                },
            },
            "return_final_graph": True,
        }
    ]
    api_response = await api_client.post(
        f"{legacy_api_settings.api_v2_url}/calculation/graphs?priority=1",
        auth=(customer_token, ""),
        json={"batch": batch_to_get},
    )
    response_json = await api_response.json()
    calculation = Calculation(**response_json["batch"][0])
    return calculation


@pytest.mark.asyncio
async def test_recipe_origin_location(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    sub_recipe_xid = "test_recipe_id"

    test_recipe_with_id = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_id["recipe"]["id"] = sub_recipe_xid

    # Set location of test_recipe_with_id as Spain.
    test_recipe_with_id["recipe"]["location"] = "Spain"

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{sub_recipe_xid}",
            auth=(customer_token, ""),
            json=test_recipe_with_id,
        )

        assert response.status_code == 200

    async def check_subrecipe_location(location_country_code: Optional[str]) -> None:
        calculation = await get_recipe_from_new_api(sub_recipe_xid, kitchen_xid, customer_token)
        assert calculation.final_root.flow.flow_location[0].country_code == location_country_code
        assert calculation.final_root.activity.activity_location is None

    await check_subrecipe_location("ES")

    test_recipe_with_subrecipe = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    assert test_recipe_with_subrecipe["recipe"]["ingredients"][0]["id"] == test_recipe_with_id["recipe"]["id"]

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{test_recipe_with_subrecipe['recipe']['id']}?full-resource=true",
            auth=(customer_token, ""),
            json=test_recipe_with_subrecipe,
        )

        assert response.status_code == 200

    async def check_location_of_subrecipe_flow_and_delete_calculation(location_country_code: Optional[str]) -> None:
        calculation = await get_recipe_from_new_api(
            recipe_xid=test_recipe_with_subrecipe["recipe"]["id"],
            kitchen_xid=kitchen_xid,
            customer_token=customer_token,
        )

        assert calculation.final_root.sub_flows[0].link_to_sub_node.xid == sub_recipe_xid
        if location_country_code:
            assert calculation.final_root.sub_flows[0].flow_location[0].country_code == location_country_code
        else:
            assert calculation.final_root.sub_flows[0].flow_location is None
        sub_recipe_activity_node = None
        sub_recipe_activity_node_uid = None

        for node in calculation.final_graph:
            if node.get("uid") == str(calculation.final_root.sub_flows[0].uid):
                if location_country_code:
                    assert node.get("flow_location") is not None
                    assert node.get("flow_location")[0].get("country_code") == location_country_code
                else:
                    assert node.get("flow_location") is None
                sub_recipe_activity_node_uid = node.get("sub_node_uids")[0]
                break

        assert sub_recipe_activity_node_uid
        for node in calculation.final_graph:
            if node.get("uid") == str(sub_recipe_activity_node_uid):
                sub_recipe_activity_node = node

        assert sub_recipe_activity_node is not None
        assert sub_recipe_activity_node.get("activity_node") is None

    await check_location_of_subrecipe_flow_and_delete_calculation("CH")

    test_recipe_with_subrecipe_no_subrecipe_origin = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    del test_recipe_with_subrecipe_no_subrecipe_origin["recipe"]["ingredients"][0]["origin"]

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/"
            f"{test_recipe_with_subrecipe_no_subrecipe_origin['recipe']['id']}?full-resource=true",
            auth=(customer_token, ""),
            json=test_recipe_with_subrecipe_no_subrecipe_origin,
        )

        assert response.status_code == 200

    await check_location_of_subrecipe_flow_and_delete_calculation(None)


@pytest.mark.asyncio
async def test_recipe_zero_ingredient_amount(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen

    test_recipe_with_ingredient_with_zero_amount = copy.deepcopy(TEST_RECIPE)

    # Add an additional ingredient to be able to set the amount of one to zero
    # and still get a non-zero CO2-value (the CO2-value of the tomato is zero)
    test_recipe_with_ingredient_with_zero_amount["recipe"]["ingredients"].append(
        copy.deepcopy(test_recipe_with_ingredient_with_zero_amount["recipe"]["ingredients"][1])
    )
    test_recipe_with_ingredient_with_zero_amount["recipe"]["ingredients"][2]["amount"] = (
        2 * test_recipe_with_ingredient_with_zero_amount["recipe"]["ingredients"][1]["amount"]
    )
    test_recipe_with_ingredient_with_zero_amount["recipe"]["ingredients"][1]["amount"] = 0.0

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}",
            auth=(customer_token, ""),
            json=test_recipe_with_ingredient_with_zero_amount,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true&indicators=true",
            auth=(customer_token, ""),
        )
        assert response.status_code == 200
        expected_recipe_resp = copy.deepcopy(test_recipe_with_ingredient_with_zero_amount)
        expected_recipe_resp["recipe"]["id"] = response.json()["recipe"]["id"]
        expected_recipe_resp["recipe"]["kitchen-id"] = kitchen_xid
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp["message"] = ""
        expected_recipe_resp["recipe-id"] = response.json()["recipe"]["id"]
        expected_recipe_resp["statuscode"] = 200

        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=2 * TEST_RECIPE_LEGACY_CO2_VALUE - 1,  # due to rounding
            expected_co2_rating="A",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=51,
            expected_co2_reduction_value=2873,
            expected_daily_food_unit=1.448,
            expected_indicators={
                "vita-score": {
                    "vita-score-points": 365.79,
                    "vita-score-rating": "C",
                    "vita-score-improvement-percentage": -7.59,
                    "energy-kcals": 3779.57,
                    "nutrition-label": False,
                    "fruit-risk-factor-amount-gram": 0.0,
                    "fruit-risk-factor-points": 61.24,
                    "vegetable-risk-factor-amount-gram": 2185.71,
                    "vegetable-risk-factor-points": 0.0,
                    "wholegrain-risk-factor-amount-gram": 0.0,
                    "wholegrain-risk-factor-points": 82.86,
                    "nuts-seeds-risk-factor-amount-gram": 0.0,
                    "nuts-seeds-risk-factor-points": 13.98,
                    "milk-risk-factor-amount-gram": 0.0,
                    "milk-risk-factor-points": 9.42,
                    "processed-meat-risk-factor-amount-gram": 0.0,
                    "processed-meat-risk-factor-points": 0.0,
                    "red-meat-risk-factor-amount-gram": 0.0,
                    "red-meat-risk-factor-points": 0.0,
                    "salt-risk-factor-amount-gram": 0.92,
                    "salt-risk-factor-points": 0.0,
                    "high-in-energy-risk-factor-points": 88.66,
                    "high-in-fat-risk-factor-points": 0.0,
                    "low-in-fat-risk-factor-points": 88.24,
                    "high-in-protein-risk-factor-points": 0.0,
                    "low-in-protein-risk-factor-points": 21.4,
                },
                "scarce-water-liters": 33.09,
                "water-footprint-award": True,
                "water-footprint-improvement-percentage": 87.52,
                "water-footprint-rating": "A",
                "rainforest-label": False,
                "rainforest-rating": "E",
                "animal-treatment-label": True,
                "animal-treatment-rating": "A",
                "local-label": False,
                "local-rating": "E",
                "season-label": False,
                "season-rating": "E",
            },
            expected_ingredients_co2_value=[0, 0, 2 * TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
            expected_ingredients_co2_rating=["A", "E", "B"],
            expected_ingredients_bar_chart=[0, 0, 100],
            expected_ingredients_food_unit=[
                TEST_RECIPE_LEGACY_ING1_FOOD_UNIT,
                0.0,
                TEST_RECIPE_LEGACY_ING2_FOOD_UNIT * 2 - 0.002,  # due to rounding
            ],
            expected_ingredients_co2_improvement_percentage=[
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
                0,
                EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
            ],
            expected_ingredients_co2_reduction_value=[
                EXPECTED_CO2_REDUCTION_VALUE_ING_1,
                0,
                2 * EXPECTED_CO2_REDUCTION_VALUE_ING_2,
            ],
            expected_ingredients_indicators=[
                {
                    "animal-treatment-label": True,
                    "animal-treatment-rating": "A",
                    "rainforest-label": True,
                    "rainforest-rating": "A",
                    "scarce-water-liters": round(TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_TOMATO * 1000, 2),
                    "water-footprint-award": True,
                    "water-footprint-improvement-percentage": 66.77,
                    "water-footprint-rating": "A",
                    "local-label": False,
                    "local-rating": "E",
                    "season-label": False,
                    "season-rating": "E",
                    "vita-score": TEST_RECIPE_LEGACY_VITASCORE_ING1,
                },
                ZERO_AMOUNT_INDICATORS,
                {
                    "animal-treatment-label": True,
                    "animal-treatment-rating": "A",
                    "rainforest-label": False,
                    "rainforest-rating": "E",
                    "scarce-water-liters": 2 * round(TEST_RECIPE_EXPECTED_SCARCE_WATER_CONSUMPTION_ONION * 1000, 2)
                    - 0.01,  # due to rounding
                    "water-footprint-award": True,
                    "water-footprint-improvement-percentage": 90.27,
                    "water-footprint-rating": "A",
                    "local-label": False,
                    "local-rating": "E",
                    "season-label": False,
                    "season-rating": "E",
                    "vita-score": {
                        "vita-score-points": 366.46,
                        "vita-score-rating": "C",
                        "vita-score-improvement-percentage": -7.78,
                        "energy-kcals": 497232.98,
                        "nutrition-label": False,
                        "fruit-risk-factor-amount-gram": 0.0,
                        "fruit-risk-factor-points": 61.24,
                        "vegetable-risk-factor-amount-gram": 156000.0,
                        "vegetable-risk-factor-points": 0.0,
                        "wholegrain-risk-factor-amount-gram": 0.0,
                        "wholegrain-risk-factor-points": 82.86,
                        "nuts-seeds-risk-factor-amount-gram": 0.0,
                        "nuts-seeds-risk-factor-points": 13.98,
                        "milk-risk-factor-amount-gram": 0.0,
                        "milk-risk-factor-points": 9.42,
                        "processed-meat-risk-factor-amount-gram": 0.0,
                        "processed-meat-risk-factor-points": 0.0,
                        "red-meat-risk-factor-amount-gram": 0.0,
                        "red-meat-risk-factor-points": 0.0,
                        "salt-risk-factor-amount-gram": 123.29,
                        "salt-risk-factor-points": 0.0,
                        "high-in-energy-risk-factor-points": 88.66,
                        "high-in-fat-risk-factor-points": 0.0,
                        "low-in-fat-risk-factor-points": 88.66,
                        "high-in-protein-risk-factor-points": 0.0,
                        "low-in-protein-risk-factor-points": 21.64,
                    },
                },
            ],
        )


@pytest.mark.asyncio
async def test_recipe_zero_recipe_amount(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen

    test_recipe_with_zero_amount = copy.deepcopy(TEST_RECIPE)

    # Add an additional ingredient to be able to set the amount of one to zero
    # and still get a non-zero CO2-value (the CO2-value of the tomato is zero)
    test_recipe_with_zero_amount["recipe"]["ingredients"][0]["amount"] = 0.0
    test_recipe_with_zero_amount["recipe"]["ingredients"][1]["amount"] = 0.0

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}",
            auth=(customer_token, ""),
            json=test_recipe_with_zero_amount,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true&indicators=true",
            auth=(customer_token, ""),
        )
        assert response.status_code == 200
        expected_recipe_resp = copy.deepcopy(test_recipe_with_zero_amount)
        resp_json = response.json()
        expected_recipe_resp["recipe"]["id"] = resp_json["recipe"]["id"]
        expected_recipe_resp["recipe"]["kitchen-id"] = kitchen_xid
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp["message"] = ""
        expected_recipe_resp["recipe-id"] = resp_json["recipe"]["id"]
        expected_recipe_resp["statuscode"] = 200

        check_response_dict(
            expected_recipe_resp,
            resp_json,
            "recipe",
            expected_co2_value=0.0,
            expected_co2_rating="E",
            expect_eaternity_award_field=True,
            expected_co2_improvement_percentage=0,
            expected_co2_reduction_value=0,
            expected_daily_food_unit=0,
            expected_indicators=ZERO_AMOUNT_INDICATORS,
            expected_ingredients_co2_value=[0, 0],
            expected_ingredients_co2_rating=["E", "E"],
            expected_ingredients_bar_chart=[0, 0],
            expected_ingredients_food_unit=[0.0, 0.0],
            expected_ingredients_co2_improvement_percentage=[0, 0],
            expected_ingredients_co2_reduction_value=[0, 0],
            expected_ingredients_indicators=[ZERO_AMOUNT_INDICATORS, ZERO_AMOUNT_INDICATORS],
        )

        # now test the result if we use that recipe as a subrecipe:
        recipe_with_something_of_zero_amount_subrecipe = {
            "request-id": 1,
            "recipe": {
                "ingredients": [
                    {
                        "id": TEST_RECIPE_ADMIN_ID,
                        "type": "recipes",
                        "amount": 1000,
                        "unit": "gram",
                        "origin": "spain",
                    }
                ],
                "id": "something_of_nothing",
                "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
                "date": "2023-06-20",
                "servings": 10,
                "location": "Zürich, Schweiz",
                "kitchen-id": kitchen_xid,
            },
        }
        response2 = await ac.post(
            f"/api/kitchens/{kitchen_xid}/recipes?full-resource=true&indicators=true",
            auth=(customer_token, ""),
            json=recipe_with_something_of_zero_amount_subrecipe,
        )
        resp2_json = response2.json()
        logger.info("resp2_json", resp2_json=resp2_json)
        assert response.status_code == 200
        assert resp2_json["statuscode"] == 200
        assert "ingredient that has an amount, the sub-ingredients summed to zero amount" in resp2_json["message"]
        assert resp2_json["recipe"]["food-unit"] > 0.1
        assert resp2_json["recipe"]["co2-value"] > 10


@pytest.mark.asyncio
async def test_recipe_missing_ingredients(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen

    test_recipe_with_no_ingredients = copy.deepcopy(TEST_RECIPE)

    # Remove the ingredients. This should result in 670 response.
    test_recipe_with_no_ingredients["recipe"]["ingredients"] = []

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}",
            auth=(customer_token, ""),
            json=test_recipe_with_no_ingredients,
        )
        assert response.status_code == 670

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true&indicators=true",
            auth=(customer_token, ""),
        )
        assert response.status_code == 670
        expected_recipe_resp = copy.deepcopy(test_recipe_with_no_ingredients)
        expected_recipe_resp["recipe"]["id"] = TEST_RECIPE_ADMIN_ID
        expected_recipe_resp["recipe"]["kitchen-id"] = kitchen_xid
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp[
            "message"
        ] = f"At least one (sub-) recipe has no ingredients. Id that caused this issue: {TEST_RECIPE_ADMIN_ID}."
        expected_recipe_resp["recipe-id"] = TEST_RECIPE_ADMIN_ID
        expected_recipe_resp["statuscode"] = 670

        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=0,
            expected_co2_rating="E",
            expect_eaternity_award_field=True,
            expected_indicators={
                "scarce-water-liters": 0.0,
                "water-footprint-award": False,
                "water-footprint-rating": "",
                "local-label": False,
                "local-rating": "E",
                "season-label": False,
                "season-rating": "E",
            },
        )


@pytest.mark.asyncio
async def test_recipe_missing_ingredients_with_customer_configuration_to_remove_from_output(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_namespace_uid, kitchen_xid = create_simple_customer_and_kitchen
    kitchen_access_group = await postgres_db.access_group_mgr.get_by_xid(kitchen_xid, customer_namespace_uid)
    kitchen_access_group.data["legacyAPI_response_behavior"] = {
        "empty_recipe_behavior": {
            "remove_fields_from_response": ["co2_value", "rating", "eaternity_award", "indicators"],
            "statuscode": 200,
            "message": "",
        }
    }
    await postgres_db.access_group_mgr.upsert_access_group_by_uid(kitchen_access_group)

    test_recipe_with_no_ingredients = copy.deepcopy(TEST_RECIPE)

    # Remove the ingredients. This should result in 670 response.
    test_recipe_with_no_ingredients["recipe"]["ingredients"] = []

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}",
            auth=(customer_token, ""),
            json=test_recipe_with_no_ingredients,
        )
        assert response.status_code == 200

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true&indicators=true",
            auth=(customer_token, ""),
        )
        assert response.status_code == 200
        expected_recipe_resp = copy.deepcopy(test_recipe_with_no_ingredients)
        expected_recipe_resp["recipe"]["id"] = TEST_RECIPE_ADMIN_ID
        expected_recipe_resp["recipe"]["kitchen-id"] = kitchen_xid
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp["message"] = ""
        expected_recipe_resp["recipe-id"] = TEST_RECIPE_ADMIN_ID
        expected_recipe_resp["statuscode"] = 200

        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=None,
            expected_co2_rating=None,
            expect_eaternity_award_field=False,
            expected_indicators=None,
        )


@pytest.mark.asyncio
async def test_recipe_zero_storage_time(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen

    with mock.patch(
        "gap_filling_modules.conservation_gfm.ConservationGapFillingWorker"
        ".add_missing_conservation_and_get_storage_time",
        return_value=0,
    ):
        async with AsyncClient(
            app=app_fixture,
            base_url="http://localhost:8050",
        ) as ac:
            response = await ac.put(
                f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}",
                auth=(customer_token, ""),
                json=TEST_RECIPE,
            )
            assert response.status_code == 681
            assert (
                response.json()["message"]
                == "For at least one ingredient, even the fastest mode of transport was too slow given the "
                "storage time. Nevertheless, the fastest mode was added as a fallback."
            )


# TODO: currently we never seed the data needed for processingGFM in the legacyAPI-tests. If we do it here, also
# the other legacyAPI-tests fail because the processing changes the co2-values. In the future, we should seed
# the processing data in all legacyAPI-tests and adjust the expected values accordingly.
# @pytest.mark.asyncio
# async def test_recipe_when_processing_is_seeded(
#     app_fixture: AppFixtureReturn,
#     create_simple_customer_and_kitchen: tuple[str, str, str],
#     seed_processing_data: None,
# ) -> None:
#     _ = seed_processing_data
#     customer_token, _, kitchen_xid = create_simple_customer_and_kitchen

#     async with AsyncClient(
#         app=app_fixture,
#         base_url="http://localhost:8050",
#     ) as ac:
#         response = await ac.put(
#             f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true",
#             auth=(customer_token, ""),
#             json=TEST_RECIPE,
#         )
#         assert response.status_code == 200
#         assert response.status_code == 200
#         expected_recipe_resp = copy.deepcopy(TEST_RECIPE)
#         expected_recipe_resp["recipe"]["id"] = response.json()["recipe"]["id"]
#         expected_recipe_resp["recipe"]["kitchen-id"] = kitchen_xid
#         expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
#         expected_recipe_resp["message"] = ""
#         expected_recipe_resp["recipe-id"] = response.json()["recipe"]["id"]
#         expected_recipe_resp["request-id"] = 0
#         expected_recipe_resp["statuscode"] = 200

#         # values are slightly different compared to the other tests of added processing
#         check_response_dict(
#             expected_recipe_resp,
#             response.json(),
#             "recipe",
#             expected_co2_value=2872,
#             expected_co2_rating="B",
#             expect_eaternity_award_field=True,
#             expected_co2_improvement_percentage=9,
#             expected_co2_reduction_value=277,
#             expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
#             expected_ingredients_co2_value=[3992, 398077],
#             expected_ingredients_co2_rating=["A", "C"],
#             expected_ingredients_bar_chart=[1, 99],
#             expected_ingredients_food_unit=[TEST_RECIPE_LEGACY_ING1_FOOD_UNIT, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
#             expected_ingredients_co2_improvement_percentage=[
#                 96,
#                 -14,
#             ],
#             expected_ingredients_co2_reduction_value=[
#                 88419,
#                 -49704,
#             ],
#         )


@pytest.mark.asyncio
async def test_recipe_missing_lci(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen

    test_recipe_with_missing_lci = copy.deepcopy(TEST_RECIPE)

    # temporarily remove the LCI. This should result in a 671-response.
    test_recipe_with_missing_lci["recipe"]["ingredients"][0]["names"][0]["value"] = "Vitaminpräparate"

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}",
            auth=(customer_token, ""),
            json=test_recipe_with_missing_lci,
        )
        assert response.status_code == 671

        response = await ac.get(
            f"/api/kitchens/{kitchen_xid}/recipes/{TEST_RECIPE_ADMIN_ID}?full-resource=true&indicators=true",
            auth=(customer_token, ""),
        )
        assert response.status_code == 671
        expected_recipe_resp = copy.deepcopy(test_recipe_with_missing_lci)
        expected_recipe_resp["recipe"]["id"] = TEST_RECIPE_ADMIN_ID
        expected_recipe_resp["recipe"]["kitchen-id"] = kitchen_xid
        expected_recipe_resp["recipe"]["recipe-portions"] = expected_recipe_resp["recipe"]["servings"]
        expected_recipe_resp["message"] = (
            "For at least one ingredient no lca inventory was found. "
            "Combination of terms for which no lca inventory was found: ['DIETARY SUPPLEMENT, VITAMIN']."
        )
        expected_recipe_resp["recipe-id"] = TEST_RECIPE_ADMIN_ID
        expected_recipe_resp["statuscode"] = 671

        check_response_dict(
            expected_recipe_resp,
            response.json(),
            "recipe",
            expected_co2_value=None,
            expected_co2_rating=None,
            expect_eaternity_award_field=False,
            expected_co2_improvement_percentage=None,
            expected_co2_reduction_value=None,
            expected_daily_food_unit=None,
            expected_final_weight_per_portion=None,
            expected_indicators={
                "local-label": False,
                "local-rating": "E",
                "season-label": False,
                "season-rating": "E",
            },
            expected_ingredients_co2_value=[None, None],
            expected_ingredients_co2_rating=[None, None],
            expected_ingredients_bar_chart=[None, None],
            expected_ingredients_food_unit=[None, TEST_RECIPE_LEGACY_ING2_FOOD_UNIT],
            expected_ingredients_co2_improvement_percentage=[None, None],
            expected_ingredients_co2_reduction_value=[None, None],
            expected_ingredients_indicators=[
                {
                    "local-label": False,
                    "local-rating": "E",
                    "season-label": False,
                    "season-rating": "E",
                },
                {
                    "local-label": False,
                    "local-rating": "E",
                    "season-label": False,
                    "season-rating": "E",
                    "animal-treatment-label": True,
                    "animal-treatment-rating": "A",
                    "vita-score": TEST_RECIPE_LEGACY_VITASCORE_ING2,
                },
            ],
        )


@pytest.mark.asyncio
async def test_ingredient_declaration_with_failed_estimation_and_missing_matching(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    request_body = [
        {
            "recipe": {
                "date": "2024-06-13",
                "id": "recipe-id",
                "ingredients": [
                    {
                        "amount": 2500,
                        "id": "ing-id",
                        "ingredients-declaration": "Karotten 80 %, Zwiebeln, Tomaten",
                        "names": [{"language": "de", "value": "Karottenfries"}],
                        "nutrient-values": {},
                        "producer": "",
                        "unit": "gram",
                    }
                ],
                "kitchen-id": kitchen_xid,
                "titles": [{"language": "de", "value": "Karottenfries"}],
            },
            "request-id": 8,
        }
    ]
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch?full-resource=true&indicators=true",
            auth=(customer_token, ""),
            json=request_body,
        )

    assert response.status_code == 200
    response_recipe = response.json()[0]
    assert response_recipe["statuscode"] == 602

    request_body_with_single_ing = copy.deepcopy(request_body)
    request_body_with_single_ing[0]["recipe"]["ingredients"][0]["ingredients-declaration"] = "Tomaten"

    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=request_body_with_single_ing,
        )

    assert response.status_code == 200

    expected = request_body_with_single_ing[0]
    expected["recipe"]["ingredients"] = []
    expected["recipe"]["servings"] = 1
    expected["recipe"]["recipe-portions"] = 1
    expected["message"] = ""
    expected["statuscode"] = 200
    expected["recipe-id"] = expected["recipe"]["id"]
    # update 7.11.2024: now  the ingredient-splitter generates a single node below the node with the
    # ingredients-declaration and for this (tomato) will then be treated as a real base product
    # with origin split.
    check_response_dict(
        expected,
        response.json()[0],
        "recipe",
        expected_co2_value=907,
        expected_co2_rating="B",
        expected_co2_improvement_percentage=41,
        expected_co2_reduction_value=633,
        expected_daily_food_unit=0.396,
    )


@pytest.mark.asyncio
async def test_ingredient_declaration_with_matchable_name(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    request_body = [
        {
            "recipe": {
                "date": "2024-06-13",
                "id": "recipe-id",
                "ingredients": [
                    {
                        "amount": 2500,
                        "id": "ing-id",
                        "ingredients-declaration": "Karotten 80 %, Zwiebeln, Tomaten",
                        "names": [{"language": "de", "value": "Tomaten"}],
                        "nutrient-values": {},
                        "producer": "",
                        "unit": "gram",
                    }
                ],
                "kitchen-id": kitchen_xid,
                "titles": [{"language": "de", "value": "Karottenfries"}],
            },
            "request-id": 8,
        }
    ]
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=request_body,
        )

    assert response.status_code == 200

    expected = request_body[0]
    expected["recipe"]["ingredients"] = []
    expected["recipe"]["servings"] = 1
    expected["recipe"]["recipe-portions"] = 1
    expected["message"] = ""
    expected["statuscode"] = 200
    expected["recipe-id"] = expected["recipe"]["id"]
    check_response_dict(
        expected,
        response.json()[0],
        "recipe",
        expected_co2_value=1656,
        expected_co2_rating="C",
        expected_co2_improvement_percentage=-8,
        expected_co2_reduction_value=-116,
        expected_daily_food_unit=0.396,
    )


@pytest.mark.asyncio
async def test_ingredient_declaration_with_all_fixed_percentages(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    request_body = [
        {
            "recipe": {
                "date": "2024-06-13",
                "id": "recipe-id",
                "ingredients": [
                    {
                        "amount": 2500,
                        "id": "ing-id",
                        "ingredients-declaration": "Karotten 80 %, Tomaten",
                        "names": [{"language": "de", "value": "Karottenfries"}],
                        "nutrient-values": {},
                        "producer": "",
                        "unit": "gram",
                    }
                ],
                "kitchen-id": kitchen_xid,
                "titles": [{"language": "de", "value": "Karottenfries"}],
            },
            "request-id": 0,
        }
    ]
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=request_body,
        )

    assert response.status_code == 200

    expected = request_body[0]
    expected["recipe"]["ingredients"] = []
    expected["recipe"]["servings"] = 1
    expected["recipe"]["recipe-portions"] = 1
    expected["message"] = ""
    expected["statuscode"] = 200
    expected["recipe-id"] = expected["recipe"]["id"]
    check_response_dict(
        expected,
        response.json()[0],
        "recipe",
        expected_co2_value=207,
        expected_co2_rating="A",
        expected_co2_improvement_percentage=89,
        expected_co2_reduction_value=1603,
        expected_daily_food_unit=0.464,
    )


@pytest.mark.asyncio
async def test_two_recipes_with_ingredients_reversed(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    request_body_1 = [
        {
            "recipe": {
                "date": "2024-08-15",
                "id": "recipe-id",
                "ingredients": [
                    {
                        "amount": 50,
                        "id": "ing-1",
                        "names": [{"language": "de", "value": "Karotten"}],
                        "unit": "gram",
                    },
                    {
                        "amount": 50,
                        "id": "ing-2",
                        "names": [{"language": "de", "value": "Tomaten"}],
                        "unit": "gram",
                    },
                ],
                "kitchen-id": kitchen_xid,
                "titles": [{"language": "de", "value": "Karottenfries"}],
            },
            "request-id": 0,
        },
    ]
    request_body_2 = [
        {
            "recipe": {
                "date": "2024-08-15",
                "id": "recipe-id",
                "ingredients": [
                    {
                        "amount": 50,
                        "id": "ing-2",
                        "names": [{"language": "de", "value": "Tomaten"}],
                        "unit": "gram",
                    },
                    {
                        "amount": 50,
                        "id": "ing-1",
                        "names": [{"language": "de", "value": "Karotten"}],
                        "unit": "gram",
                    },
                ],
                "kitchen-id": kitchen_xid,
                "titles": [{"language": "de", "value": "Karottenfries"}],
            },
            "request-id": 0,
        }
    ]

    async def run_and_check_response(request: list[dict]) -> None:
        async with AsyncClient(
            app=app_fixture,
            base_url="http://localhost:8050",
        ) as ac:
            response = await ac.post(
                "/api/recipes/batch",
                auth=(customer_token, ""),
                json=request,
            )
            assert response.status_code == 200
            assert "co2-value" in response.json()[0]["recipe"]
            assert response.json()[0]["recipe"]["co2-value"] == 3

    tasks = []
    await run_and_check_response(request_body_1)
    await run_and_check_response(request_body_2)

    for _ in range(10):
        tasks.append(run_and_check_response(request_body_1))
        tasks.append(run_and_check_response(request_body_1))
        tasks.append(run_and_check_response(request_body_1))
        tasks.append(run_and_check_response(request_body_1))
        tasks.append(run_and_check_response(request_body_2))

    await asyncio.gather(*tasks)


@pytest.mark.asyncio
async def test_recipes_edge_and_link_to_sub_node_different(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    request_body_1 = [
        {
            "recipe": {
                "id": "recipe-id",
                "ingredients": [
                    {
                        "amount": 50,
                        "id": "ing-1",
                        "names": [{"language": "de", "value": "Karotten"}],
                        "unit": "gram",
                    },
                ],
                "kitchen-id": kitchen_xid,
                "titles": [{"language": "de", "value": "Karottenfries"}],
            },
            "request-id": 0,
        },
    ]
    request_body_2 = [
        {
            "recipe": {
                "id": "recipe-id",
                "ingredients": [
                    {
                        "amount": 50,
                        "id": "ing-2",
                        "names": [{"language": "de", "value": "Tomaten"}],
                        "unit": "gram",
                    },
                ],
                "kitchen-id": kitchen_xid,
                "titles": [{"language": "de", "value": "Karottenfries"}],
            },
            "request-id": 0,
        },
    ]

    async def send_request(request: list[dict]) -> Response:
        async with AsyncClient(
            app=app_fixture,
            base_url="http://localhost:8050",
        ) as ac:
            response = await ac.post(
                "/api/recipes/batch",
                auth=(customer_token, ""),
                json=request,
            )
            assert response.status_code == 200
        return response

    await send_request(request_body_1)
    await send_request(request_body_2)

    def side_effect(
        edges_to_delete: list[Edge],  # noqa ARG001
        edges_to_insert: list[Edge],  # noqa ARG001
        lock_on_uid: Optional[str] = None,  # noqa ARG001
        md5_hash: Optional[uuid.UUID] = None,  # noqa ARG001
    ) -> None:
        return

    with mock.patch(
        "core.service.node_service.NodeService.delete_then_insert_many_edges",
        side_effect=side_effect,
    ):
        response = await send_request(request_body_1)
        assert response.json()[0]["recipe"]["co2-value"] == 1


@pytest.mark.asyncio
async def test_recipe_with_gtin_and_producer_on_recipe_level(
    app_fixture: AppFixtureReturn, create_simple_customer_and_kitchen: tuple[str, str, str]
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    request_body = [copy.deepcopy(TEST_RECIPE)]
    recipe_producer = "Recipe_Producer"
    recipe_gtin = "recipe_gtin_1234567890123"
    request_body[0]["recipe"]["producer"] = recipe_producer
    request_body[0]["recipe"]["gtin"] = recipe_gtin
    request_body[0]["recipe"]["id"] = "recipe-id"
    request_body[0]["recipe"]["kitchen-id"] = kitchen_xid
    request_body[0]["request-id"] = 0
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch",
            auth=(customer_token, ""),
            json=request_body,
        )

    assert response.status_code == 200

    expected = request_body[0]
    expected["recipe"]["ingredients"] = []
    expected["message"] = ""
    expected["statuscode"] = 200
    expected["recipe-id"] = expected["recipe"]["id"]
    expected["recipe"]["recipe-portions"] = expected["recipe"]["servings"]
    check_response_dict(
        expected,
        response.json()[0],
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="A",
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
    )
    node_uid = await ServiceLocator().service_provider.postgres_db.product_mgr.find_uid_by_xid(
        customer_uid, "recipe-id"
    )
    parent_edges = await ServiceLocator().service_provider.postgres_db.graph_mgr.get_parents_by_uid(node_uid)
    assert len(parent_edges) == 1
    root_flow = await ServiceLocator().service_provider.postgres_db.graph_mgr.find_by_uid(parent_edges[0].parent_uid)
    assert isinstance(root_flow, FoodProductFlowNode)
    assert root_flow.gtin == recipe_gtin
    assert root_flow.producer == recipe_producer


@pytest.mark.asyncio
async def test_recipe_with_sourcing_origin(
    app_fixture: AppFixtureReturn,
    create_simple_customer_and_kitchen: tuple[str, str, str],
) -> None:
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    request_body = [copy.deepcopy(TEST_RECIPE)]
    request_body[0]["recipe"]["id"] = "recipe-id"
    request_body[0]["recipe"]["kitchen-id"] = kitchen_xid
    request_body[0]["request-id"] = 0
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.post(
            "/api/recipes/batch?full-resource=true",
            auth=(customer_token, ""),
            json=request_body,
        )

    assert response.status_code == 200

    expected = request_body[0]
    expected["message"] = ""
    expected["statuscode"] = 200
    expected["recipe-id"] = expected["recipe"]["id"]
    expected["recipe"]["recipe-portions"] = expected["recipe"]["servings"]
    check_response_dict(
        expected,
        response.json()[0],
        "recipe",
        expected_co2_value=TEST_RECIPE_LEGACY_CO2_VALUE,
        expected_co2_rating="A",
        expected_co2_improvement_percentage=EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_RECIPE_LEGACY_FOOD_UNIT,
        expected_ingredients_co2_value=[0, TEST_RECIPE_LEGACY_ING2_CO2_VALUE],
        expected_ingredients_co2_rating=["A", "B"],
        expected_ingredients_bar_chart=[0, 100],
        expected_ingredients_food_unit=[
            TEST_RECIPE_LEGACY_ING1_FOOD_UNIT,
            TEST_RECIPE_LEGACY_ING2_FOOD_UNIT,
        ],
        expected_ingredients_co2_improvement_percentage=[
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
            EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
        ],
        expected_ingredients_co2_reduction_value=[
            EXPECTED_CO2_REDUCTION_VALUE_ING_1,
            EXPECTED_CO2_REDUCTION_VALUE_ING_2,
        ],
    )

    # sourcing-origin should not be present in response if it is not set in request
    assert "sourcing-origin" not in response.json()[0]["recipe"]["ingredients"][1]

    # sourcing-origin must be in raw_string on db if and only if it is set in request
    expected_sourcing_origins = {}
    for ingredient in expected["recipe"]["ingredients"]:
        xid = f'FoodService{ingredient["id"]}'
        if "sourcing-origin" in ingredient:
            expected_sourcing_origins[xid] = ingredient["sourcing-origin"]
        else:
            expected_sourcing_origins[xid] = None

    recipe_node_uid = await ServiceLocator().service_provider.postgres_db.product_mgr.find_uid_by_xid(
        customer_uid, "recipe-id"
    )
    child_edges = await ServiceLocator().service_provider.postgres_db.graph_mgr.get_sub_graph_by_uid(
        recipe_node_uid, max_depth=0
    )
    assert len(child_edges) == len(expected_sourcing_origins)

    for edge in child_edges:
        child_node = await ServiceLocator().service_provider.postgres_db.graph_mgr.find_by_uid(edge.child_uid)
        assert isinstance(child_node, FoodProductFlowNode)
        assert child_node.link_to_sub_node.xid in expected_sourcing_origins
        expected_sourcing_origin = expected_sourcing_origins[child_node.link_to_sub_node.xid]
        if expected_sourcing_origin is None:
            assert "sourcing_origin" not in child_node.raw_input.model_fields_set
        else:
            assert child_node.raw_input.sourcing_origin == expected_sourcing_origin


@pytest.mark.asyncio
async def test_commercial_ingredients(
    app_fixture: AppFixtureReturn,
    create_simple_customer_and_kitchen: tuple[str, str, str],
) -> None:
    # For the moment we can only test that we can receive the commercial-ingredient and the information will
    # end up in the database.
    customer_token, customer_uid, kitchen_xid = create_simple_customer_and_kitchen
    request_body = copy.deepcopy(TEST_SUPPLY)
    original_id = request_body["supply"]["ingredients"][1]["id"]
    linked_ingredient_id = "8717"
    request_body["supply"]["ingredients"][1]["linked-ingredient"] = {
        "id": linked_ingredient_id,
        "type": "conceptual-ingredients",
    }
    request_body["supply"]["ingredients"][1]["type"] = "commercial-ingredients"
    request_body["supply"]["kitchen-id"] = kitchen_xid

    supply_id = "test_commercial_ingredients_supply"
    async with AsyncClient(
        app=app_fixture,
        base_url="http://localhost:8050",
    ) as ac:
        response = await ac.put(
            f"/api/supplies/{supply_id}?full-resource=true",
            auth=(customer_token, ""),
            json=request_body,
        )

    assert response.status_code == 200

    expected_response = copy.deepcopy(request_body)
    expected_response["supply"]["id"] = supply_id
    expected_response["message"] = ""
    expected_response["statuscode"] = 200
    expected_response["supply-id"] = supply_id
    expected_response["request-id"] = 0
    # For commercial-ingredients we expect in the response the id to be replaced by the linked-id
    # and the return-type conceptual-ingredient
    expected_response["supply"]["ingredients"][1]["id"] = linked_ingredient_id
    expected_response["supply"]["ingredients"][1]["type"] = "conceptual-ingredients"

    # Remove nutrient-values from expected response since they're removed in the actual response
    for ingredient in expected_response["supply"]["ingredients"]:
        if "nutrient-values" in ingredient:
            del ingredient["nutrient-values"]

    check_response_dict(
        expected_response,
        response.json(),
        "supply",
        expected_co2_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2,
        expected_co2_rating="A",
        expect_eaternity_award_field=None,
        expected_co2_improvement_percentage=TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE,
        expected_co2_reduction_value=TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE,
        expected_daily_food_unit=TEST_LEGACY_SUPPLY_EXPECTED_DFU,
        expected_ingredients_co2_value=[0, TEST_LEGACY_SUPPLY_EXPECTED_CO2],
        expected_ingredients_co2_rating=["A", "B"],
        expected_ingredients_bar_chart=[0, 100],
        expected_ingredients_food_unit=[TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING1, TEST_LEGACY_SUPPLY_EXPECTED_DFU_ING2],
        expected_ingredients_co2_improvement_percentage=[
            TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_1,
            TEST_LEGACY_SUPPLY_EXPECTED_CO2_IMPROVEMENT_PERCENTAGE_ING_2,
        ],
        expected_ingredients_co2_reduction_value=[
            TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_1,
            TEST_LEGACY_SUPPLY_EXPECTED_CO2_REDUCTION_VALUE_ING_2,
        ],
    )

    # linked-ingredient should not be present in response if it is not set in request
    assert "linked-ingredient" not in response.json()["supply"]["ingredients"][0]

    # linked-ingredient must be in raw_string on db if and only if it is set in request
    expected_linked_ingredients = {}
    for ingredient in expected_response["supply"]["ingredients"]:
        if "linked-ingredient" in ingredient:
            xid = f"FoodService{original_id}"
            expected_linked_ingredients[xid] = ingredient["linked-ingredient"]
        else:
            xid = f'FoodService{ingredient["id"]}'
            expected_linked_ingredients[xid] = None

    supply_node_uid = await ServiceLocator().service_provider.postgres_db.product_mgr.find_uid_by_xid(
        customer_uid, supply_id
    )
    child_edges = await ServiceLocator().service_provider.postgres_db.graph_mgr.get_sub_graph_by_uid(
        supply_node_uid, max_depth=0
    )
    assert len(child_edges) == len(expected_linked_ingredients)

    for edge in child_edges:
        child_flow = await ServiceLocator().service_provider.postgres_db.graph_mgr.find_by_uid(edge.child_uid)
        assert isinstance(child_flow, FoodProductFlowNode)
        sub_child_edges = await ServiceLocator().service_provider.postgres_db.graph_mgr.get_sub_graph_by_uid(
            child_flow.uid, max_depth=0
        )
        assert len(sub_child_edges) == 1
        child_activity = await ServiceLocator().service_provider.postgres_db.graph_mgr.find_by_uid(
            sub_child_edges[0].child_uid
        )
        assert isinstance(child_activity, LinkingActivityNode)
        assert child_flow.link_to_sub_node.xid in expected_linked_ingredients
        expected_linked_ingredient = expected_linked_ingredients[child_flow.link_to_sub_node.xid]
        if expected_linked_ingredient is None:
            assert (
                child_activity.raw_input is None or "linked_ingredient" not in child_activity.raw_input.model_fields_set
            )
        else:
            assert child_activity.raw_input.linked_ingredient == expected_linked_ingredient
