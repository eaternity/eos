from typing import Optional

from pydantic import BaseModel, Field

from legacy_api.dto.legacy.localized_name import LocalizedNameDto
from legacy_api.dto.legacy.nutrient_value import NutrientValueDto


class RawInputDataDto(BaseModel):
    name: Optional[str] = Field(None)
    titles: Optional[list[LocalizedNameDto]] = Field(None)
    author: Optional[str] = Field(None)
    date: Optional[str] = Field(None)
    location: Optional[str] = Field(None)
    recipe_portions: Optional[int] = Field(None)
    production_portions: Optional[int] = Field(None)
    sold_portions: Optional[int] = Field(None)
    instruction: Optional[str] = Field(None)
    menu_line_name: Optional[str] = Field(None)
    menu_line_id: Optional[int] = Field(None)

    # supply fields
    supplier: Optional[str] = Field(None)
    supplier_id: Optional[str] = Field(None)
    invoice_id: Optional[str] = Field(None)
    order_date: Optional[str] = Field(None)
    supply_date: Optional[str] = Field(None)

    # product fields
    gtin: Optional[str] = Field(None)
    names: Optional[list[LocalizedNameDto]] = Field(None)
    amount: Optional[float] = Field(None)
    unit: Optional[str] = Field(None)
    producer: Optional[str] = Field(None)
    ingredients_declaration: Optional[list[LocalizedNameDto]] = Field(None)
    nutrient_values: Optional[NutrientValueDto] = Field(None)
    origin: Optional[str] = Field(None)
    transport: Optional[str] = Field(None)
    production: Optional[str] = Field(None)
    processing: Optional[str] = Field(None)
    conservation: Optional[str] = Field(None)
    packaging: Optional[str] = Field(None)
