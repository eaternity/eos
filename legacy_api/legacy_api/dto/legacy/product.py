from typing import Dict, List, Optional

from pydantic import BaseModel, ConfigDict, Field

from legacy_api.dto.legacy.indicators import WrappedIndicatorsDto
from legacy_api.dto.legacy.legacy_model import LegacyModel

from .climate_score import ClimateAwardDto, ClimateScoreDto
from .localized_name import LocalizedNameDto
from .nutrient_value import NutrientValueDto


class ProductWithoutIngredientsDeclarationDto(LegacyModel):
    id: Optional[str] = Field(None, alias="id")
    gtin: Optional[str] = Field(None, alias="gtin")
    names: Optional[List[LocalizedNameDto]] = Field(None, alias="names")
    amount: Optional[float] = Field(None, alias="amount")
    unit: Optional[str] = Field(None, alias="unit")
    producer: Optional[str] = Field(None, alias="producer")
    date: Optional[str] = Field(None, alias="date")
    nutrient_values: Optional[NutrientValueDto] = Field(None, alias="nutrient-values")
    origin: Optional[str] = Field(None, alias="origin")
    transport: Optional[str] = Field(None, alias="transport")
    production: Optional[str] = Field(None, alias="production")
    processing: Optional[str] = Field(None, alias="processing")
    conservation: Optional[str] = Field(None, alias="conservation")
    packaging: Optional[str] = Field(None, alias="packaging")
    model_config = ConfigDict(populate_by_name=True)

    def get_model_meta_data(self) -> Dict:
        return self.model_dump()


class ProductDto(ProductWithoutIngredientsDeclarationDto):
    ingredients_declaration: Optional[str] = Field(None, alias="ingredients-declaration")

    model_config = ConfigDict(populate_by_name=True)


class WrappedProductDto(BaseModel):
    product: ProductDto

    model_config = ConfigDict(populate_by_name=True)


class ProductResultDto(ProductWithoutIngredientsDeclarationDto, ClimateScoreDto, ClimateAwardDto, WrappedIndicatorsDto):
    pass


class WrappedProductResultDto(BaseModel):
    product: ProductResultDto

    model_config = ConfigDict(populate_by_name=True)
