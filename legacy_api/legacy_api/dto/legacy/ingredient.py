from typing import Dict, List, Optional

from pydantic import ConfigDict, Field, model_validator

from core.domain.props.legacy_api_information_prop import LegacyIngredientTypeEnum
from legacy_api.dto.legacy.indicators import WrappedIndicatorsForIngredientsDto
from legacy_api.dto.legacy.legacy_model import LegacyModel

from .climate_score import ClimateScoreDto
from .localized_name import LocalizedNameDto
from .nutrient_value import NutrientValueDto


class LinkedIngredientDto(LegacyModel):
    id: str
    type: Optional[LegacyIngredientTypeEnum] = LegacyIngredientTypeEnum.conceptual_ingredients


class IngredientWithoutIngredientsDeclarationDto(LegacyModel):
    id: str
    type: Optional[LegacyIngredientTypeEnum] = LegacyIngredientTypeEnum.conceptual_ingredients
    names: Optional[List[LocalizedNameDto]] = None
    amount: float
    unit: Optional[str] = None
    origin: Optional[str] = None
    transport: Optional[str] = None
    production: Optional[str] = None
    producer: Optional[str] = None
    processing: Optional[str] = None
    conservation: Optional[str] = None
    packaging: Optional[str] = None
    sourcing_origin: Optional[str] = Field(None, alias="sourcing-origin")
    nutrient_values: Optional[NutrientValueDto] = Field(None, alias="nutrient-values")
    gtin: Optional[str] = None
    linked_ingredient: Optional[LinkedIngredientDto] = Field(None, alias="linked-ingredient")

    @model_validator(mode="after")
    def validate_names(self) -> "IngredientWithoutIngredientsDeclarationDto":
        """Makes sure that names field is set if type is conceptual_ingredients."""
        if self.type == LegacyIngredientTypeEnum.conceptual_ingredients and not self.names:
            raise ValueError("Names field is required for conceptual_ingredients type")
        return self

    def get_ingredient_meta_data(self) -> Dict:
        return self.model_dump(exclude={"id", "type"})

    model_config = ConfigDict(populate_by_name=True)


class IngredientDto(IngredientWithoutIngredientsDeclarationDto):
    ingredients_declaration: Optional[str] = Field(None, alias="ingredients-declaration")

    model_config = ConfigDict(populate_by_name=True)


class IngredientResultDto(
    IngredientWithoutIngredientsDeclarationDto, ClimateScoreDto, WrappedIndicatorsForIngredientsDto
):
    pass
