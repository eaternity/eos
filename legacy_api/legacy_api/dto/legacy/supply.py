from typing import Optional

from pydantic import BaseModel, ConfigDict, Field

from legacy_api.dto.legacy.indicators import WrappedIndicatorsDto
from legacy_api.dto.legacy.legacy_model import LegacyModel

from .climate_score import ClimateScoreDto
from .ingredient import IngredientDto, IngredientResultDto


class SupplyDto(BaseModel):
    # TODO: is this field necessary?
    kitchen_id: Optional[str] = Field(None, alias="kitchen-id")
    supplier: Optional[str] = Field(None, alias="supplier")
    supplier_id: Optional[str] = Field(None, alias="supplier-id")
    invoice_id: Optional[str] = Field(None, alias="invoice-id")
    order_date: Optional[str] = Field(None, alias="order-date")
    supply_date: Optional[str] = Field(None, alias="supply-date")
    ingredients: list[IngredientDto] = Field(alias="ingredients")
    id: Optional[str] = Field(None, alias="id")
    model_config = ConfigDict(populate_by_name=True)

    def get_model_meta_data(self) -> dict:
        return self.model_dump(exclude={"ingredients"})


class BatchSupplyDto(LegacyModel):
    request_id: Optional[int] = Field(None, alias="request-id")
    transient: Optional[bool] = Field(default=None, alias="transient")
    supply: SupplyDto

    model_config = ConfigDict(populate_by_name=True)


class WrappedSupplyDto(BaseModel):
    supply: SupplyDto


class SupplyResultDto(SupplyDto, ClimateScoreDto, WrappedIndicatorsDto):
    ingredients: list[IngredientResultDto] = Field(alias="ingredients")

    model_config = ConfigDict(populate_by_name=True)


class PutOrPostSupplyResultDto(BaseModel):
    statuscode: int = Field(None, alias="statuscode")
    message: str = Field(None, alias="message")
    supply_id: str = Field(None, alias="supply-id")
    request_id: Optional[int] = Field(None, alias="request-id")

    model_config = ConfigDict(populate_by_name=True)


class SupplyResultWithStatusDto(BaseModel):
    statuscode: int = Field(None, alias="statuscode")
    message: str = Field(None, alias="message")
    request_id: Optional[int] = Field(None, alias="request-id")
    supply_id: Optional[str] = Field(None, alias="supply-id")
    supply: Optional[SupplyResultDto] = Field(None, alias="supply")

    model_config = ConfigDict(populate_by_name=True)


class GetSuppliesResultDto(BaseModel):
    supplies: list[str]  # returns only id's
