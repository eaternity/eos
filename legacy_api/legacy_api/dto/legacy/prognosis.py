from typing import List, Optional

from pydantic import BaseModel, ConfigDict, Field


class RecipeSoldPortionsDto(BaseModel):
    model_config = ConfigDict(populate_by_name=True)

    recipe_id: str = Field(alias="recipe-id")
    sold_portions: Optional[int] = Field(alias="sold-portions", default=None)
    production_portions: Optional[int] = Field(alias="production-portions", default=None)


class RecipeUpdateStatusDto(BaseModel):
    model_config = ConfigDict(populate_by_name=True)

    recipe_id: str = Field(alias="recipe-id")
    status: str = Field(alias="status")


class RecipeSoldPortionsWrapperDto(BaseModel):
    model_config = ConfigDict(populate_by_name=True)

    recipes: List[RecipeSoldPortionsDto] = Field(alias="recipes")


class RecipeUpdateStatusWrapperDto(BaseModel):
    model_config = ConfigDict(populate_by_name=True)

    recipes: List[RecipeUpdateStatusDto] = Field(alias="recipes")
