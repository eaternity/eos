from typing import Optional

from pydantic import BaseModel, ConfigDict, Field

from legacy_api.dto.legacy.legacy_model import LegacyModel


class KitchenDto(LegacyModel):
    name: str = Field(alias="name")
    location: str = Field(alias="location")
    email: Optional[str] = Field(None, alias="email")
    language: Optional[str] = Field(None, alias="language")
    xid: Optional[str] = Field(None, alias="id")
    model_config = ConfigDict(populate_by_name=True)


class GetKitchensResultDto(BaseModel):
    kitchens: list[str]  # returns only id's


class WrappedKitchenDto(BaseModel):
    kitchen: KitchenDto
