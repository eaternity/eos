from typing import Optional

from pydantic import BaseModel, ConfigDict, Field


class VitaScoreDto(BaseModel):
    vita_score_points: Optional[float] = Field(None, alias="vita-score-points")
    vita_score_rating: Optional[str] = Field(None, alias="vita-score-rating")
    vita_score_improvement_percentage: Optional[float] = Field(None, alias="vita-score-improvement-percentage")
    energy_kcals: Optional[float] = Field(None, alias="energy-kcals")
    nutrition_label: Optional[bool] = Field(None, alias="nutrition-label")
    fruit_risk_factor_amount_gram: Optional[float] = Field(None, alias="fruit-risk-factor-amount-gram")
    fruit_risk_factor_points: Optional[float] = Field(None, alias="fruit-risk-factor-points")
    vegetable_risk_factor_amount_gram: Optional[float] = Field(None, alias="vegetable-risk-factor-amount-gram")
    vegetable_risk_factor_points: Optional[float] = Field(None, alias="vegetable-risk-factor-points")
    wholegrain_risk_factor_amount_gram: Optional[float] = Field(None, alias="wholegrain-risk-factor-amount-gram")
    wholegrain_risk_factor_points: Optional[float] = Field(None, alias="wholegrain-risk-factor-points")
    nuts_seeds_risk_factor_amount_gram: Optional[float] = Field(None, alias="nuts-seeds-risk-factor-amount-gram")
    nuts_seeds_risk_factor_points: Optional[float] = Field(None, alias="nuts-seeds-risk-factor-points")
    milk_risk_factor_amount_gram: Optional[float] = Field(None, alias="milk-risk-factor-amount-gram")
    milk_risk_factor_points: Optional[float] = Field(None, alias="milk-risk-factor-points")
    processed_meat_risk_factor_amount_gram: Optional[float] = Field(
        None, alias="processed-meat-risk-factor-amount-gram"
    )
    processed_meat_risk_factor_points: Optional[float] = Field(None, alias="processed-meat-risk-factor-points")
    red_meat_risk_factor_amount_gram: Optional[float] = Field(None, alias="red-meat-risk-factor-amount-gram")
    red_meat_risk_factor_points: Optional[float] = Field(None, alias="red-meat-risk-factor-points")
    salt_risk_factor_amount_gram: Optional[float] = Field(None, alias="salt-risk-factor-amount-gram")
    salt_risk_factor_points: Optional[float] = Field(None, alias="salt-risk-factor-points")
    high_in_energy_risk_factor_points: Optional[float] = Field(None, alias="high-in-energy-risk-factor-points")
    high_in_fat_risk_factor_points: Optional[float] = Field(None, alias="high-in-fat-risk-factor-points")
    low_in_fat_risk_factor_points: Optional[float] = Field(None, alias="low-in-fat-risk-factor-points")
    high_in_protein_risk_factor_points: Optional[float] = Field(None, alias="high-in-protein-risk-factor-points")
    low_in_protein_risk_factor_points: Optional[float] = Field(None, alias="low-in-protein-risk-factor-points")
    model_config = ConfigDict(populate_by_name=True)


class EnvironmentDto(BaseModel):
    scarce_water_liters: Optional[float] = Field(default=None, alias="scarce-water-liters")
    water_footprint_rating: Optional[str] = Field(alias="water-footprint-rating")
    water_footprint_award: Optional[bool] = Field(alias="water-footprint-award")
    water_footprint_improvement_percentage: Optional[float] = Field(alias="water-footprint-improvement-percentage")
    rainforest_label: Optional[bool] = Field(default=None, alias="rainforest-label")
    rainforest_rating: Optional[str] = Field(default=None, alias="rainforest-rating")
    animal_treatment_label: Optional[bool] = Field(default=None, alias="animal-treatment-label")
    animal_treatment_rating: Optional[str] = Field(default=None, alias="animal-treatment-rating")
    season_label: bool = Field(default=False, alias="season-label")  # TODO: Implement season label and remove default.
    season_rating: str = Field(default="E", alias="season-rating")  # TODO: Implement season rating and remove default.
    local_label: bool = Field(default=False, alias="local-label")  # TODO: Implement local label and remove default.
    local_rating: str = Field(default="E", alias="local-rating")  # TODO: Implement local rating and remove default.
    model_config = ConfigDict(populate_by_name=True)


class IndicatorsDto(BaseModel):
    vita_score: Optional[VitaScoreDto] = Field(None, alias="vita-score")
    environment: Optional[EnvironmentDto] = Field(default=None, alias="environment")
    model_config = ConfigDict(populate_by_name=True)


class WrappedIndicatorsDto(BaseModel):
    food_unit: Optional[float] = Field(None, alias="food-unit")
    final_weight_per_portion: Optional[float] = Field(None, alias="final-weight-per-portion")
    indicators: Optional[IndicatorsDto] = Field(None, alias="indicators")
    model_config = ConfigDict(populate_by_name=True)


class WrappedIndicatorsForIngredientsDto(BaseModel):
    """In the legacy API, daily food unit is called food-unit in top recipe and foodUnit in ingredients."""

    food_unit: Optional[float] = Field(None, alias="foodUnit")
    indicators: Optional[IndicatorsDto] = Field(None, alias="indicators")
    model_config = ConfigDict(populate_by_name=True)
