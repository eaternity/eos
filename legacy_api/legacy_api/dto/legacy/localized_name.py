from legacy_api.dto.legacy.legacy_model import LegacyModel


class LocalizedNameDto(LegacyModel):
    language: str
    value: str
