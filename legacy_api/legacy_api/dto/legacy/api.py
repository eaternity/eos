"Allow generic responses."
from typing import Awaitable, Callable

import aiohttp
from fastapi import Response, status
from pydantic import BaseModel, Field
from structlog import get_logger

logger = get_logger()


class ApiResponseDto(BaseModel):
    api_response: dict | str | None = Field()


class Error500ResponseDto(BaseModel):
    status: str = Field()
    key: str = Field()
    message: str = Field()


async def handle_api_response(
    response: Response, api_response: aiohttp.ClientResponse, handler: Callable[[dict], Awaitable[BaseModel]]
) -> BaseModel:
    "Pass response JSON to a handler function and deal with errors."
    try:
        json_data = await api_response.json()

        return await handler(json_data)
    except Exception as e:
        logger.error("Error: %s, handler: %s", e, handler)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        text = api_response.text
        try:
            text = await api_response.content.read()
            text = text.decode("utf-8")
        except Exception as e:
            text = str(e)
        return Error500ResponseDto(status="INTERNAL_SERVER_ERROR", key="server error", message=text)
