from typing import Optional

from pydantic import BaseModel, ConfigDict, Field


class ClimateScoreDto(BaseModel):
    co2_value: Optional[int] = Field(None, alias="co2-value")
    rating: Optional[str] = Field(None, alias="rating")
    co2_value_improvement_percentage: Optional[int] = Field(None, alias="co2-value-improvement-percentage")
    co2_value_reduction_value: Optional[int] = Field(None, alias="co2-value-reduction-value")
    bar_chart: Optional[int] = Field(None, alias="bar-chart")
    info_text: Optional[str] = Field(None, alias="info-text")
    model_config = ConfigDict(populate_by_name=True)


class ClimateAwardDto(BaseModel):
    eaternity_award: Optional[bool] = Field(False, alias="eaternity-award")
    model_config = ConfigDict(populate_by_name=True)
