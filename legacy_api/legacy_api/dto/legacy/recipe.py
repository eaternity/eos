from typing import Optional

from pydantic import BaseModel, ConfigDict, Field

from legacy_api.dto.legacy.indicators import WrappedIndicatorsDto
from legacy_api.dto.legacy.legacy_model import LegacyModel

from .climate_score import ClimateAwardDto, ClimateScoreDto
from .ingredient import IngredientDto, IngredientResultDto
from .localized_name import LocalizedNameDto

FIELDS_TO_HIDE_FROM_THE_DOC = ["weight", "servings"]


def hide_fields_from_docs(s: dict) -> None:
    for field in FIELDS_TO_HIDE_FROM_THE_DOC:
        try:
            s["properties"].pop(field)
        except KeyError:
            continue


class RecipeDto(LegacyModel):
    id: Optional[str] = Field(None, alias="id")
    kitchen_id: Optional[str] = Field(None, alias="kitchen-id")
    titles: Optional[list[LocalizedNameDto]] = Field(None, alias="titles")
    author: Optional[str] = Field(None, alias="author")
    date: Optional[str] = Field(None, alias="date")
    location: Optional[str] = Field(None, alias="location")
    weight: Optional[float] = Field(None, alias="weight")
    servings_deprecated: Optional[int] = Field(None, alias="servings")
    recipe_portions: Optional[int] = Field(None, alias="recipe-portions")
    production_portions: Optional[int] = Field(None, alias="production-portions")
    sold_portions: Optional[int] = Field(None, alias="sold-portions")
    instruction: Optional[str] = Field(None, alias="instruction")
    ingredients: list[IngredientDto] = Field(alias="ingredients")
    menu_line_name: Optional[str] = Field(None, alias="menu-line-name")
    menu_line_id: Optional[int] = Field(None, alias="menu-line-id")
    # these two fields are only used to create matchings-options
    gtin: Optional[str] = Field(None, alias="gtin")
    producer: Optional[str] = Field(None, alias="producer")

    def get_model_meta_data(self) -> dict:
        return self.model_dump(exclude={"kitchen_id", "ingredients"})

    model_config = ConfigDict(populate_by_name=True, json_schema_extra=hide_fields_from_docs)


class WrappedRecipeDto(BaseModel):
    recipe: RecipeDto


class BatchItemResultDto(BaseModel):
    statuscode: int
    message: str
    recipe_id: str = Field(None, alias="recipe-id")
    request_id: int = Field(None, alias="request-id")

    model_config = ConfigDict(populate_by_name=True)


class BatchRecipeDto(LegacyModel):
    request_id: Optional[int] = Field(None, alias="request-id")
    transient: Optional[bool] = Field(default=None, alias="transient")
    recipe: RecipeDto

    model_config = ConfigDict(populate_by_name=True)


class RecipeResultDto(RecipeDto, ClimateScoreDto, ClimateAwardDto, WrappedIndicatorsDto):
    ingredients: list[IngredientResultDto] = Field(alias="ingredients")

    model_config = ConfigDict(populate_by_name=True)


class BatchItemResultDtoWithRecipe(BatchItemResultDto):
    recipe: RecipeResultDto


class RecipeResultWithStatusDto(BaseModel):
    statuscode: int = Field(None, alias="statuscode")
    message: str = Field(None, alias="message")
    request_id: Optional[int] = Field(None, alias="request-id")
    recipe_id: Optional[str] = Field(None, alias="recipe-id")
    recipe: Optional[RecipeResultDto] = Field(None, alias="recipe")

    model_config = ConfigDict(populate_by_name=True)


class GetRecipesResultDto(BaseModel):
    recipes: list[str]  # returns only id's
