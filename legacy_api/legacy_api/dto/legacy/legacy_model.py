"""BaseModel for the legacy API handling legacy-API-wide modifications of incoming requests."""
from typing import Any, Optional

from pydantic import BaseModel


class LegacyModel(BaseModel):
    def __init__(self, **kwargs: Any):  # noqa: ANN401
        for alias_name, field_value in kwargs.items():
            # Check if the field is of type int and the provided value is a float.
            field_name = alias_name.replace("-", "_")
            if (
                isinstance(field_value, float)
                and self.model_fields.get(field_name)
                and getattr(self.model_fields.get(field_name), "annotation", None) in (Optional[int], int)
            ):
                # Convert the float value to int.
                kwargs[alias_name] = int(field_value)
            # Check if the field is of type str and the provided value is a float or an int.
            if (
                isinstance(field_value, (float, int))
                and self.model_fields.get(field_name)
                and getattr(self.model_fields.get(field_name), "annotation", None) in (Optional[str], str)
            ):
                # Convert the float or int value to str.
                kwargs[alias_name] = str(field_value)
        # Call the super __init__ method
        super().__init__(**kwargs)
