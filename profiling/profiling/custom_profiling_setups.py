"Custom profiling setup."
import datetime
import uuid

import structlog
from profiling.abstract_profiling_setups import AbstractProfilingEOS

from core.domain.calculation import Calculation
from core.domain.edge import Edge, EdgeTypeEnum
from core.domain.nodes import FlowNode, FoodProcessingActivityNode, ModeledActivityNode

logger = structlog.get_logger()


class ProfilingBananaJam(AbstractProfilingEOS):
    """Profile the Banana Jam calculation."""

    def __init__(
        self,
        out_dir: str = None,
        profiling: bool = True,
        use_system_processes: bool = False,
        save_as_system_process: bool = False,
        delete_system_process: bool = False,
        logging: bool = False,
        expected_co2: float = 0,
        expected_dfu: float = 0,
    ) -> None:
        """Initialize the profiling setup.

        :param out_dir: The directory to store profiling results.
        :param profiling: Indicates whether profiling should be enabled.
        :param use_system_processes: Indicates whether system processes should be used.
        :param save_as_system_process: Indicates whether we store the aggregated cache in the
                database at the end of the calculation.
        :param delete_system_process: Indicates whether we delete the aggregated cache of all nodes in the calc_graph
                at the end of the calculation.
        :param logging: Indicates whether detailed logging should be enabled.
        :param expected_co2: The expected CO2 value for validation.
        :param excpected_dfu: The expected daily food unit value for validation.
        """
        super().__init__(
            out_dir,
            profiling=profiling,
            use_system_processes=use_system_processes,
            save_as_system_process=save_as_system_process,
            delete_system_process=delete_system_process,
            logging=logging,
            expected_co2=expected_co2,
        )
        self.expected_dfu = expected_dfu

    async def init_calculation(self) -> None:
        """Initialize the Banana Jam calculation."""
        # create the recipe node
        self.root_node = FoodProcessingActivityNode(
            uid=uuid.uuid4(),
            date="2023-09-06",
            names=[{"language": "en", "value": "Banana Jam"}],
            production_amount={"value": 100, "unit": "gram"},
            ingredients_declaration=[{"language": "en", "value": "Bananas, Lemon Juice Concentrate."}],
            nutrient_values={
                "energy_kcal": 100,
                "fat_gram": 0,
                "saturated_fat_gram": 0,
                "carbohydrates_gram": 25,
                "sucrose_gram": 18,
                "protein_gram": 0,
                "sodium_chloride_gram": 0,
            },
            activity_location="Switzerland",
        )

        # add the node to the cache
        self.service_provider.node_service.cache.add_node(self.root_node)

    def check_if_impact_assessment_of_root_node_is_correct(self) -> None:
        """Check if the results for the CO2 value and for the DFU are correct."""
        super().check_if_impact_assessment_of_root_node_is_correct()
        result_root_node = self.calc_graph.get_root_node()
        daily_food_unit = result_root_node.daily_food_unit.amount_for_activity_production_amount().value
        print(f"calculated dfu: {daily_food_unit}")
        print(f"expected dfu:   {self.expected_dfu}")
        assert abs(daily_food_unit - self.expected_dfu) < 0.000001, "root node should have correct dfu amount"


class ProfilingBWCalculations(AbstractProfilingEOS):
    """Profile EOS calculation where we run GFMs on two sub-bw-nodes."""

    def __init__(
        self,
        edb_xid: str,
        out_dir: str = None,
        profiling: bool = True,
        use_system_processes: bool = False,
        save_as_system_process: bool = False,
        delete_system_process: bool = False,
        logging: bool = False,
        expected_co2: float = 0,
    ):
        """Initialize the profiling setup.

        :param edb_xid: The xid of the EDB node.
        :param out_dir: The directory to store profiling results.
        :param profiling: Indicates whether profiling should be enabled.
        :param use_system_processes: Indicates whether system processes should be used.
        :param save_as_system_process: Indicates whether we store the aggregated cache in the database at the end
                of the calculation.
        :param delete_system_process: Indicates whether we delete the aggregated cache of all nodes in the calc_graph
                at the end of the calculation.
        :param logging: Indicates whether detailed logging should be enabled.
        :param expected_co2: The expected CO2 value for validation.
        """
        self.edb_xid = edb_xid
        super().__init__(
            out_dir,
            profiling=profiling,
            use_system_processes=use_system_processes,
            save_as_system_process=save_as_system_process,
            delete_system_process=delete_system_process,
            logging=logging,
            expected_co2=expected_co2,
        )

    async def init_calculation(self) -> None:
        """Initialize the EOS sub-BW calculation."""
        # create an auxillary root node
        self.root_node = ModeledActivityNode(
            uid=uuid.uuid4(),
            titles=[{"value": "dummy_node_for_system_caching"}],
            date=datetime.datetime.now().strftime("%Y-%m-%d"),
            production_amount={"value": 1.0, "unit": "kilogram"},
        )
        # add the node to the cache
        self.service_provider.node_service.cache.add_node(self.root_node)

        product_mgr = self.service_provider.postgres_db.get_product_mgr()

        # find the EDB node corresponding to the oat flour product
        self.bw_node_uid = await product_mgr.find_or_create_uid_by_xid(
            xid=self.edb_xid,
            namespace_uid=self.namespace_uid,
        )
        bw_node = await self.service_provider.node_service.find_by_uid(self.bw_node_uid)

        # add a link between the root node and the EDB node
        flow = FlowNode(
            uid=uuid.uuid4(),
            amount={"value": 1.0, "unit": "kilogram"},
        )
        self.service_provider.node_service.cache.add_node(flow)

        edge_up = Edge(
            parent_uid=self.root_node.uid,
            child_uid=flow.uid,
            edge_type=EdgeTypeEnum.lca_link,
        )
        self.service_provider.node_service.cache.add_edge(edge_up)

        edge_down = Edge(
            parent_uid=flow.uid,
            child_uid=bw_node.uid,
            edge_type=EdgeTypeEnum.lca_link,
        )
        self.service_provider.node_service.cache.add_edge(edge_down)

    def add_additional_calculation_settings(self, calculation: Calculation) -> None:
        """Add relevant GFMs to the calculation settings.

        :param: calculation: The calculation to add the settings to.
        """
        super().add_additional_calculation_settings(calculation)
        calculation.fixed_depth_for_calculating_supply = 2


class ProfilingEosSubBWCalculationsWithTwoBWNodes(ProfilingBWCalculations):
    """Profile EOS calculation where we run GFMs on two sub-bw-nodes."""

    async def init_calculation(self) -> None:
        """Initialize the EOS sub-BW calculation."""
        await super().init_calculation()

        # add the EDB node and its child intermediary flow to the list of nodes for which we want to run GFMs
        edges = await self.service_provider.node_service.get_sub_graph_by_uid(self.bw_node_uid, max_depth=1)
        activity_flow_uid_tuples_below_bw_nodes_to_compute = [(self.bw_node_uid, edges[0].child_uid)]

        # initialize the brightway node service for the sub-bw nodes for which we want to run GFMs
        await self.service_provider.modeled_activity_node_service.init(
            activity_flow_uid_tuples_below_bw_nodes_to_compute
        )

    def add_additional_calculation_settings(self, calculation: Calculation) -> None:
        """Add relevant GFMs to the calculation settings.

        :param: calculation: The calculation to add the settings to.
        """
        super().add_additional_calculation_settings(calculation)
        calculation.gfms_for_which_to_save_most_shallow_nodes = ["TransportDecisionGapFillingWorker"]
        calculation.gfms_for_which_to_load_all_relevant_nodes = ["TransportDecisionGapFillingWorker"]
