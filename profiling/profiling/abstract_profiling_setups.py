import cProfile
import io
import logging
import os
import pstats
import uuid
from abc import ABC, abstractmethod
from asyncio import Semaphore
from uuid import UUID

import structlog

from core.domain.calculation import Calculation
from core.domain.nodes import ModeledActivityNode
from core.domain.nodes.node import Node
from core.domain.util import get_system_process_data
from core.graph_manager.calc_graph import CalcGraph
from core.orchestrator.gap_filling_module_loader import GapFillingModuleLoader
from core.orchestrator.orchestrator import Orchestrator
from core.service.calc_service import CalcService
from core.service.service_provider import ServiceLocator, ServiceProvider
from database.postgres.pg_term_mgr import IPCC_2013_GWP_100_XID
from inventory_importer.settings import Settings

logger = structlog.get_logger()


class AbstractProfilingEOS(ABC):
    """Abstract base class for performance profiling of EOS calculations.

    This class provides a framework for profiling calculations and can be extended to create custom profiling setups.

    Args:
    ----
        out_dir: The directory to store profiling results.
        profiling: Indicates whether profiling should be enabled.
        logging: Indicates whether detailed logging should be enabled.
        expected_co2: The expected CO2 value for validation.

    Attributes:
    ----------
        out_dir: The directory for storing profiling results.
        profiling: Indicates if profiling is enabled.
        logging: Indicates if detailed logging is enabled.
        expected_co2: The expected CO2 value for validation.
        service_provider: An instance of the service provider.
        root_node: The root node of the calculation.
        calc_graph: The calculation graph.
        namespace_uid: The UUID of the selected namespace.

    Methods:
    -------
        - configure_logging(): Configures logging settings based on the 'logging' attribute.
        - init_services(): Initializes the service provider and associated services.
        - init_calculation(): Abstract method for initializing the calculation.
        - run_orchestrator_with_profiling(semaphore, orchestrator): Profiles the orchestrator run and saves the results.
        - run_orchestrator_without_profiling(semaphore, orchestrator): Runs the orchestrator without profiling.
        - run_calculation(): Initializes the calculation and runs it with or without profiling.
        - check_if_impact_assessment_of_root_node_is_correct(): Checks if the impact_assessment
                                                                of the root node is correct.
        - check_for_correctness(): Performs correctness checks, including CO2 validation.
        - run(): The main entry point to execute the profiling setup.

    Note:
    ----
    Subclasses should extend the `init_calculation` method to set up specific profiling scenarios.
    """

    def __init__(
        self,
        out_dir: str,
        profiling: bool,
        use_system_processes: bool,
        save_as_system_process: bool,
        delete_system_process: bool,
        logging: bool,
        expected_co2: float,
    ) -> None:
        """Initialize the profiling setup.

        :param out_dir: The directory to store profiling results.
        :param profiling: Indicates whether profiling should be enabled.
        :param use_system_processes: Indicates whether system processes should be used.
        :param save_as_system_process: Indicates whether we store the aggregated cache in the database at the end of
                                       the calculation.
        :param delete_system_process: Indicates whether we delete the aggregated cache of all nodes in the calc_graph
                                      at the end of the calculation.
        :param logging: Indicates whether detailed logging should be enabled.
        :param expected_co2: The expected CO2 value for validation.
        """
        self.out_dir: str = out_dir
        self.profiling: bool = profiling
        self.use_system_processes: bool = use_system_processes
        self.save_as_system_process = save_as_system_process
        self.delete_system_process = delete_system_process
        self.logging: bool = logging
        self.expected_co2: float = expected_co2
        self.service_provider: ServiceProvider = None
        self.root_node: Node = None
        self.calc_graph: CalcGraph = None
        self.namespace_uid: UUID = Settings().EATERNITY_NAMESPACE_UUID

    def configure_logging(self) -> None:
        """Configure the logging settings based on the 'logging' attribute."""
        if not self.logging:
            # Discard all logs except for warnings and errors
            structlog.configure(wrapper_class=structlog.make_filtering_bound_logger(logging.WARNING))

    async def init_services(self) -> None:
        """Initialize the service provider and associated services."""
        # Create the service provider and initialize all services
        service_locator = ServiceLocator()
        self.service_provider = service_locator.service_provider

        schema = "public"
        await self.service_provider.postgres_db.connect(schema)
        await self.service_provider.messaging_service.start()
        await self.service_provider.init_caches()
        self.service_provider.gap_filling_module_loader = GapFillingModuleLoader()
        await self.service_provider.gap_filling_module_loader.init(self.service_provider)
        await self.service_provider.messaging_service.start_worker_channel()

    @abstractmethod
    async def init_calculation(self) -> None:
        """Abstract method for initializing the calculation.

        Subclasses should implement this method to set up specific profiling scenarios.
        """
        pass

    def add_additional_calculation_settings(self, calculation: Calculation) -> None:
        """Abstract method for adding additional calculation settings.

        :param: calculation: The calculation to add the settings to.
        """
        calculation.use_system_processes = self.use_system_processes
        calculation.save_as_system_process = self.save_as_system_process

    async def run_orchestrator_with_profiling(self, semaphore: Semaphore, orchestrator: Orchestrator) -> None:
        """Profile the orchestrator run and save the results.

        :param semaphore: An asyncio semaphore for controlling concurrent execution.
        :param orchestrator: The orchestrator to profile.
        """
        # Profile the orchestrator run
        with cProfile.Profile() as pr:
            async with semaphore:
                await orchestrator.run()

        # Sort the profile by cumulative time and print the stats
        result = io.StringIO()
        stats_to_dump = pstats.Stats(pr, stream=result).sort_stats("cumulative").print_stats()

        # Dump the result for further processing (like flamegraphs)
        os.makedirs(self.out_dir, exist_ok=True)
        csv_file_path = os.path.join(self.out_dir, "test.csv")
        prof_file_path = os.path.join(self.out_dir, "test.prof")
        stats_to_dump.dump_stats(prof_file_path)

        # Write the result to a CSV file
        result = result.getvalue()
        result = "ncalls" + result.split("ncalls")[-1]
        result = "\n".join([",".join(line.rstrip().split(None, 5)) for line in result.split("\n")])
        with open(csv_file_path, "w+") as f:
            f.write(result)

    async def run_orchestrator_without_profiling(self, semaphore: Semaphore, orchestrator: Orchestrator) -> None:
        """Run the orchestrator without profiling.

        :param semaphore: A semaphore for controlling concurrent execution.
        :param orchestrator: The orchestrator to run.
        """
        async with semaphore:
            await orchestrator.run()

    async def run_calculation(self) -> None:
        """Initialize the calculation and run it with or without profiling."""
        self.calc_service = CalcService(
            self.service_provider,
            self.service_provider.glossary_service,
            self.service_provider.node_service,
            self.service_provider.gap_filling_module_loader,
        )
        calculation = Calculation(uid=uuid.uuid4(), root_node_uid=self.root_node.uid)
        self.add_additional_calculation_settings(calculation)
        self.calc_graph, orchestrator = self.calc_service.create_calc_graph_and_orchestrator(calculation)
        semaphore = Semaphore(value=1)
        if self.profiling:
            await self.run_orchestrator_with_profiling(semaphore, orchestrator)
        else:
            await self.run_orchestrator_without_profiling(semaphore, orchestrator)

    def check_if_impact_assessment_of_root_node_is_correct(self) -> None:
        """Check if the impact_assessment of the root node is correct."""
        result_root_node = self.calc_graph.get_root_node()
        root_char_term = self.service_provider.glossary_service.root_subterms["Root_Impact_Assessments"]
        ipcc_2013_gwp_100_term = self.service_provider.glossary_service.terms_by_xid_ag_uid[
            (IPCC_2013_GWP_100_XID, root_char_term.access_group_uid)
        ]
        calculated_co2 = (
            result_root_node.impact_assessment.amount_for_root_node().quantities[ipcc_2013_gwp_100_term.uid].value
        )
        print(f"expected_co2  : {self.expected_co2}")
        print(f"calculated_co2: {calculated_co2}")
        diff = abs(calculated_co2 - self.expected_co2)
        assert diff < 0.000001, "root node should have the correct CO2 amount"

    def check_for_correctness(self) -> None:
        """Perform correctness checks, including CO2 validation."""
        self.check_if_impact_assessment_of_root_node_is_correct()

    async def save_as_system_processes(self, calc_graph: CalcGraph) -> None:
        """Save nodes as system processes.

        :param calc_graph: CalcGraph of the calculation
        """
        nodes_to_update = set()
        for uid in calc_graph.nodes_to_calculate_supply:
            db_node = await self.service_provider.node_service.graph_mgr.find_by_uid(uid)
            if db_node is not None and isinstance(db_node, ModeledActivityNode):
                logger.warning(f"Adding aggregated_cache to node {uid}")
                calc_node = calc_graph.get_node_by_uid(uid)
                db_node.aggregated_cache = get_system_process_data(calc_node, calc_graph)
                self.service_provider.node_service.cache.add_node(db_node)
                nodes_to_update.add(db_node.uid)

        await self.service_provider.node_service.update_many_nodes_and_edges_from_local_cache(list(nodes_to_update))

    async def delete_system_processes(self, calc_graph: CalcGraph) -> None:
        """Delete the aggregated cache of all nodes in the calc_graph.

        :param calc_graph: CalcGraph of the calculation
        """
        nodes_to_update = set()
        for uid in calc_graph.get_node_uids():
            cached_node = await self.service_provider.node_service.find_by_uid(uid)
            if (
                cached_node
                and isinstance(cached_node, ModeledActivityNode)
                and cached_node.aggregated_cache is not None
            ):
                logger.warning(f"Removing aggregated_cache from node {uid}")
                cached_node.aggregated_cache = None
                cached_node.model_fields_set.remove("aggregated_cache")
                self.service_provider.node_service.cache.add_node(cached_node)
                nodes_to_update.add(cached_node.uid)

        await self.service_provider.node_service.update_many_nodes_and_edges_from_local_cache(list(nodes_to_update))

    async def run(self) -> None:
        """Execute the profiling setup."""
        self.configure_logging()
        await self.init_services()
        await self.init_calculation()
        await self.run_calculation()
        self.check_for_correctness()
        if self.save_as_system_process:
            await self.save_as_system_processes(self.calc_graph)
        if self.delete_system_process:
            await self.delete_system_processes(self.calc_graph)
