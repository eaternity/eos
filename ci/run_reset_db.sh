#!/bin/sh

# activate k8s context:
aws eks --region eu-central-1 update-kubeconfig --name $K8S_CLUSTER_NAME

echo "Deleting old job if exists..."
kubectl delete -n $K8S_NAMESPACE --ignore-not-found job/eos-seed-db

echo "Creating new job..."
kubectl create -n $K8S_NAMESPACE job --from=cronjob/eos-seed-db eos-seed-db
kubectl wait -n $K8S_NAMESPACE --for=condition=ready pod --selector=job-name=eos-seed-db --timeout=600s
kubectl logs -n $K8S_NAMESPACE --follow job/eos-seed-db

echo "Seed db complete"
