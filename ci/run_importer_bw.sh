#!/bin/sh

# activate k8s context:
aws eks --region eu-central-1 update-kubeconfig --name $K8S_CLUSTER_NAME

echo "Deleting old job if exists..."
kubectl delete -n $K8S_NAMESPACE --ignore-not-found job/eos-importer-bw

echo "Creating new job..."
kubectl create -n $K8S_NAMESPACE job --from=cronjob/eos-importer-bw eos-importer-bw
kubectl wait -n $K8S_NAMESPACE --for=condition=ready pod --selector=job-name=eos-importer-bw --timeout=600s

# Follow logs with retry mechanism
echo "Following logs..."
while true; do
    # Check if job is still running
    status=$(kubectl get job -n $K8S_NAMESPACE eos-importer-bw -o jsonpath='{.status.conditions[?(@.type=="Complete")].status}')
    failed_status=$(kubectl get job -n $K8S_NAMESPACE eos-importer-bw -o jsonpath='{.status.conditions[?(@.type=="Failed")].status}')
    
    if [ "$status" = "True" ]; then
        echo "Job completed successfully"
        break
    elif [ "$failed_status" = "True" ]; then
        echo "Job failed"
        exit 1
    fi

    # Try to follow logs, if it breaks, we'll loop and try again
    kubectl logs -n $K8S_NAMESPACE --follow job/eos-importer-bw || echo "Log following interrupted, retrying..."
    sleep 2
done

echo "Importer complete... restart eos deployment..."
kubectl rollout restart -n $K8S_NAMESPACE deployment/eos-api

echo "Waiting for the rollout to complete..."
kubectl rollout status -n $K8S_NAMESPACE deployment/eos-api

echo "Done"

