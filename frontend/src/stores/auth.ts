import { writable } from 'svelte/store'
import { browser } from '$app/environment';

interface AuthStatus {
    state: "LOGGED_OUT" | "TOKEN_AUTHENTICATED" | "PW_AUTHENTICATED";
    token?: string
    email?: string
    password?: string
    defaultNamespace?: string
}

let initAuthData: AuthStatus = {
    state: "LOGGED_OUT",
}

if (browser) {
    let loadedAuthData = localStorage.getItem('auth');
    if (loadedAuthData) {
        initAuthData = JSON.parse(loadedAuthData)
    }
}
export const authStatus = writable<AuthStatus>(initAuthData)

authStatus.subscribe((value) => {
    if (browser) {
        localStorage.auth = JSON.stringify(value)
    }
})
