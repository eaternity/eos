from typing import List, Optional

from pydantic import BaseModel, ConfigDict, Field

from core.domain.calculation import Calculation
from core.domain.props.names_prop import RawName


class KitchenComparisonDataDto(BaseModel):
    value: float
    reduction_with_respect_to_average: Optional[float] = Field(None, alias="reduction-with-respect-to-average")
    restaurant_comparison_trees: Optional[float] = Field(None, alias="restaurant-comparison-trees")
    comparison_to_average_percentage: Optional[float] = Field(None, alias="comparison-to-average-percentage")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class SnippetKitchenTotalCO2ValueComparisonDto(BaseModel):
    """Curret, Average, and Climate Friendly kitchens' total CO2 value per food unit.

    Use for the following JSON field:
        "snippet-kitchen-total-co2-value-comparison -> kitchen-comparison-data -> 0|1|2 -> value"
        "snippet-kitchen-total-co2-value-comparison -> kitchen-comparison-data -> 0|1|2
                                                            -> comparison-to-average-percentage"
        "comparison-percentage-with-percentage" (self.this.comparison_to_average_percentage)

        "snippet-restaurant-certificate -> reduction-with-respect-to-average"
        "snippet-restaurant-certificate -> comparison-to-average-percentage"
        "snippet-restaurant-certificate -> average-kitchen-total-co2-value"
        "snippet-restaurant-certificate -> comparison-to-average-diff-string"
        "snippet-restaurant-certificate -> this-kitchen-total-co2-value"
        "snippet-restaurant-certificate -> has-reduction"
        "snippet-restaurant-certificate -> restaurant-comparison-trees"
    """

    this: KitchenComparisonDataDto
    average: KitchenComparisonDataDto
    climate_friendly: KitchenComparisonDataDto = Field(..., alias="climate-friendly")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class SnippetClimateFriendlyMenuComparisonDto(BaseModel):
    """Percentage of the climate friendly menus.

    Use for the following JSON fields:
        "snippet-climate-friendly-menu-comparison -> climate-friendly-percentages -> data1"
        "number-of-kitchens"
    """

    number_of_kitchens: int = Field(..., alias="number-of-kitchens")
    climate_friendly: float = Field(..., alias="climate-friendly")
    average: float
    this: float
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class SnippetMenuCertificateDto(BaseModel):
    """Average of CO2 per FoodUnit per recipe.

    Use for the following JSON fields:
        "snippet-menu-certificate -> all-recipes-average"
        "snippet-menu-certificate -> climate-friendly-recipes-average"
        "snippet-menu-certificate -> this-recipes-average"
        "certificate-percentage"
        "certificate-percentage-string"
    """

    all_recipes_average: float = Field(..., alias="all-recipes-average")
    climate_friendly_recipes_average: Optional[float] = Field(None, alias="climate-friendly-recipes-average")
    this_recipes_average: float = Field(..., alias="this-recipes-average")
    certificate_percentage: Optional[float] = Field(None, alias="certificate-percentage")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class MatchingWithReductionValueAndExamplesDto(BaseModel):
    ingredient_weight_gram: float = Field(..., alias="ingredient-weight-gram")
    co2_reduction_kilogram: float = Field(..., alias="co2-reduction-kilogram")
    examples: List[str] = Field(..., alias="examples")  # TODO: maybe better to have here a list of RawName
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class CategoryAnalysis(BaseModel):
    category_weight_gram: float = Field(..., alias="category-weight-gram")
    category_name: List[RawName] = Field(..., alias="category-name")
    co2_value_gram: float = Field(..., alias="co2-value-gram")
    co2_improvement_percentage: float = Field(..., alias="co2-improvement-percentage")
    parent_category_name: Optional[List[RawName]] = Field(None, alias="parent-category-name")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class TopOrFlopMenuIngredientDto(BaseModel):
    title: List[RawName] = Field(..., alias="title")
    co2_gram: float = Field(
        ..., alias="co2-gram"
    )  # CO2 value in gram per display food unit (1.6 x food unit) of the recipe
    weight_gram: float = Field(
        ..., alias="weight-gram"
    )  # weight in gram per display food unit (1.6 x food unit) of the recipe
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class SnippetTopFlopMenuDto(BaseModel):
    """Detailed analysis of top and flop menu.

    Computes the ingredients' CO2 (impact) and gram amounts per display food unit (1.6 x food unit) of the recipe.

    Use for the following JSON fields:
        "snippet-top-flop-menu" -> "best-menu-co2value"
        "snippet-top-flop-menu" -> "worst-menu-co2value"
        "snippet-top-flop-menu" -> "best-menu-table"
        "snippet-top-flop-menu" -> "worst-menu-table"
    """

    best_menu_title: Optional[List[RawName]] = Field(..., alias="best-menu-title")
    best_menu_co2_value_gram: Optional[float] = Field(..., alias="best-menu-co2-value-gram")
    worst_menu_title: List[RawName] = Field(..., alias="worst-menu-title")
    worst_menu_co2_value_gram: float = Field(..., alias="worst-menu-co2-value-gram")
    best_menu_table: Optional[List[TopOrFlopMenuIngredientDto]] = Field(..., alias="best-menu-table")
    worst_menu_table: List[TopOrFlopMenuIngredientDto] = Field(..., alias="worst-menu-table")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class BestMenuItem(BaseModel):
    title: List[RawName] = Field(..., alias="title")
    co2_gram: float = Field(
        ..., alias="co2-gram"
    )  # CO2 value in gram per display food unit (1.6 x food unit) of the recipe
    weight_gram: float = Field(
        ..., alias="weight-gram"
    )  # weight in gram per display food unit (1.6 x food unit) of the recipe
    weight_per_portion_gram: float = Field(
        ..., alias="weight-per-portion-gram"
    )  # weight in gram per portion of the recipe
    co2_per_portion_gram: float = Field(
        ..., alias="co2-per-portion-gram"
    )  # CO2 value in gram per portion of the recipe

    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class SnippetBestMenus(BaseModel):
    """Detailed analysis of top and flop menu.

    Computes the ingredients' CO2 (impact) and gram amounts per display food unit (1.6 x food unit) of the recipe.

    Use for the following JSON fields:
        "best-menus" -> "table-data"
    """

    best_menus: Optional[List[BestMenuItem]] = Field(..., alias="table-data")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class SnippetBestWorstIngredients(BaseModel):
    """Detailed analysis of best and worst ingredients.

    Computes the ingredients' CO2 (impact) reduction values.

    Use for the following JSON fields:
        "snippet-best-worst-ingredients" -> "worst-ingredients"
        "snippet-best-worst-ingredients" -> "best-ingredients"
    """

    worst_ingredients: Optional[List[MatchingWithReductionValueAndExamplesDto]] = Field(..., alias="worst-ingredients")
    best_ingredients: Optional[List[MatchingWithReductionValueAndExamplesDto]] = Field(..., alias="best-ingredients")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class SnippetCategoryComparison(BaseModel):
    """Detailed analysis of categories.

    Computes the categories' CO2 (impact) improvement percentages.

    Use for the following JSON fields:
        "category-comparison" -> "category-analysis"
    """

    category_analysis: Optional[List[CategoryAnalysis]] = Field(..., alias="category-analysis")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class MustacheJsonDto(BaseModel):
    snippet_kitchen_total_co2_value_comparison: Optional[SnippetKitchenTotalCO2ValueComparisonDto] = Field(
        None, alias="snippet-kitchen-total-co2-value-comparison"
    )
    snippet_climate_friendly_menu_comparison: Optional[SnippetClimateFriendlyMenuComparisonDto] = Field(
        None, alias="snippet-climate-friendly-menu-comparison"
    )
    snippet_menu_certificate: Optional[SnippetMenuCertificateDto] = Field(None, alias="snippet-menu-certificate")
    snippet_top_flop_menu: Optional[SnippetTopFlopMenuDto] = Field(None, alias="snippet-top-flop-menu")
    snippet_best_worst_ingredients: Optional[SnippetBestWorstIngredients] = Field(
        None, alias="snippet-best-worst-ingredients"
    )
    snippet_category_comparison: Optional[SnippetCategoryComparison] = Field(None, alias="snippet-category-comparison")
    snippet_best_menus: Optional[SnippetBestMenus] = Field(None, alias="best-menus")
    model_config = ConfigDict(extra="forbid", populate_by_name=True)


class ErrorStatisticsDto(BaseModel):
    failed_node_uids: List[str] = Field(None)
    number_of_successful_calculations: int = Field(None)
    number_of_failed_calculations: int = Field(None)


class ReportOutputDto(BaseModel):
    recipe: Optional[Calculation] = None
    supply: Optional[Calculation] = None
    mustache_json: Optional[MustacheJsonDto] = None
    error_statistics: Optional[ErrorStatisticsDto] = None
    model_config = ConfigDict(extra="forbid")


class CSVReportOutputDto(BaseModel):
    final_graphs: Optional[List[List[dict]]] = None
    error_statistics: Optional[ErrorStatisticsDto] = None
    model_config = ConfigDict(extra="forbid")
