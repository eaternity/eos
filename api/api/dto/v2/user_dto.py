from typing import Dict, List, Optional

from pydantic import BaseModel, ConfigDict

from api.dto.v2.access_group_dto import AccessGroupId
from core.domain.user import UserPermissionsEnum


class UserDto(BaseModel):
    user_id: str
    email: Optional[str] = None
    is_superuser: bool
    is_service_account: bool
    legacy_default_access_group_id: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


class UserCreateDto(BaseModel):
    email: Optional[str] = None
    password: Optional[str] = None
    is_superuser: bool
    model_config = ConfigDict(extra="forbid")


class UserCreateStatusDto(BaseModel):
    auth_token: str
    legacy_default_access_group_id: Optional[str] = None
    status: bool
    model_config = ConfigDict(extra="forbid")


class WrappedUserCreateDto(BaseModel):
    user: UserCreateDto
    legacy_api_default_access_group_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


class MemberDto(BaseModel):
    user_id: str
    permissions: Optional[Dict[UserPermissionsEnum, bool]] = None
    model_config = ConfigDict(extra="forbid")

    def __str__(self):
        if self.permissions is not None:
            permission_dict = {f"{str(key)}": val for key, val in self.permissions.items()}
        else:
            permission_dict = self.permissions
        return f"user_id='{self.user_id}' permissions={permission_dict}"


class MemberWithStatusDto(MemberDto):
    statuscode: int
    message: str
    model_config = ConfigDict(extra="forbid")

    def __str__(self):
        if self.permissions is not None:
            permission_dict = {f"{str(key)}": val for key, val in self.permissions.items()}
        else:
            permission_dict = self.permissions
        return (
            f"user_id='{self.user_id}' permissions={permission_dict} "
            f"statuscode={self.statuscode} message={self.message}"
        )


# used in upsert_members as input
# used in get_members as input
# used in delete_members as input
class WrappedMembersDto(BaseModel):
    access_group: AccessGroupId
    members: List[MemberDto]
    namespace_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


# used in upsert_members as output
# used in delete_members as output
class WrappedMembersWithStatusDto(BaseModel):
    access_group: AccessGroupId
    members_with_status: List[MemberWithStatusDto]
    namespace_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")
