from typing import Optional

from pydantic import BaseModel, ConfigDict, Field


class IngredientsDeclarationMappingDto(BaseModel):
    faulty_declaration: str = Field(alias="faulty_declaration")
    fixed_declaration: str = Field(alias="fixed_declaration")
    uid: Optional[str] = Field(None, alias="uid")
    model_config = ConfigDict(populate_by_name=True)


class IngredientsDeclarationMappingsListingDto(BaseModel):
    declarations: list[IngredientsDeclarationMappingDto]


class IngredientsDeclarationMappingSearchInputDto(BaseModel):
    declaration_part: str


class IncomingIngredientsDeclarationMappingDto(BaseModel):
    faulty_declaration: str
    fixed_declaration: str
    # TODO: language support?
