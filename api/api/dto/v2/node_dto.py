from typing import Annotated, List, Optional

from pydantic import BaseModel, ConfigDict, Field, field_validator

from core.domain.calculation import Calculation
from core.domain.nodes.node import Node
from core.domain.nodes.root_with_subflows_dto import NodeIDDto

NodeTypes = Annotated[object, Node.subclasses_with_self()]


class BatchNodePropUpdateDto(BaseModel):
    nodes: List[NodeTypes] = Field()

    @field_validator("nodes", mode="before")
    @staticmethod
    def node_validator(v: List[NodeTypes]) -> List[NodeTypes]:
        type_lookup = Node.subclasses_with_self_as_dict()
        deserialized = []
        for node_item in v:
            if isinstance(node_item, Node):
                deserialized.append(node_item)
                continue
            if node_item["node_type"] not in type_lookup:
                raise ValueError(f"Unknown Flow node_type: {node_item['node_type']}")
            deserialized.append(type_lookup[node_item["node_type"]].model_validate(node_item))
        return deserialized


class ExistingNodeDto(BaseModel):
    existing_node: NodeIDDto = Field(default_factory=NodeIDDto)
    model_config = ConfigDict(extra="forbid")


class BatchItemDto(BaseModel):
    request_id: Optional[int] = None
    namespace_uid: Optional[str] = None
    model_config = ConfigDict(extra="forbid")


class BatchWithExistingNodeInputItemDto(BatchItemDto):
    nodes_dto: ExistingNodeDto
    model_config = ConfigDict(extra="forbid")


class BatchWithExistingNodeOutputItemDto(BatchItemDto):
    statuscode: int
    message: str
    model_config = ConfigDict(extra="forbid")


class BatchInputDto(BaseModel):
    batch: List[Calculation]
    model_config = ConfigDict(extra="forbid")


class BatchOutputDto(BaseModel):
    batch: List[Calculation]
    model_config = ConfigDict(extra="forbid")


class BatchWithExistingNodesInputDto(BaseModel):
    batch: List[BatchWithExistingNodeInputItemDto]
    model_config = ConfigDict(extra="forbid")


class BatchWithExistingNodesOutputDto(BaseModel):
    batch: List[BatchWithExistingNodeOutputItemDto]
    model_config = ConfigDict(extra="forbid")
