from typing import Optional

from pydantic import BaseModel


class CustomerDto(BaseModel):
    uid: str
    xid: str
    name: str


class CreateCustomerDto(BaseModel):
    name: str
    uid: Optional[str] = None


class CustomerCreateStatusDto(BaseModel):
    auth_token: str
    status: bool
    namespace: CustomerDto


class WrappedCreateCustomerDto(BaseModel):
    customer: CreateCustomerDto
    auth_token: Optional[str] = None
