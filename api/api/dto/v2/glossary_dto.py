from typing import List, Optional

from pydantic import BaseModel, ConfigDict, Field


class TermDtoWithoutUid(BaseModel):
    name: str = Field(alias="name")
    sub_class_of: str = Field(alias="sub_class_of")
    data: Optional[dict] = Field(None, alias="data")
    xid: Optional[str] = Field(None, alias="xid")
    access_group_uid: Optional[str] = Field(None, alias="access_group_uid")

    model_config = ConfigDict(populate_by_name=True)


class TermDto(TermDtoWithoutUid):
    uid: Optional[str] = Field(None, alias="uid")
    sub_class_of: Optional[str] = Field(None, alias="sub_class_of")
    model_config = ConfigDict(populate_by_name=True)


class GetAllTermsDto(BaseModel):
    terms: List[TermDto]


class GlossaryLinkDto(BaseModel):
    uid: Optional[str] = Field(None, alias="uid")
    gap_filling_module: str = Field(alias="gap_filling_module")
    term_uids: list[str] = Field(alias="term_uids")
    linked_term_uid: Optional[str] = Field(None, alias="linked_term_uid")
    linked_node_uid: Optional[str] = Field(None, alias="linked_node_uid")


class GetAllGlossaryLinksDto(BaseModel):
    glossary_links: List[GlossaryLinkDto]


class GlossaryLinkBatchDeleteDto(BaseModel):
    uids: List[str]
