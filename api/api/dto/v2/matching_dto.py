from typing import List, Optional

from pydantic import BaseModel, ConfigDict, Field

from api.dto.v2.glossary_dto import TermDto


class MatchingItemDto(BaseModel):
    gap_filling_module: str = Field(alias="gap_filling_module")

    uid: Optional[str] = Field(None, alias="uid")
    access_group_uid: Optional[str] = Field(None, alias="access_group_uid")
    lang: Optional[str] = Field(None, alias="lang")
    matching_string: Optional[str] = Field(None, alias="matching_string")
    matched_node_uid: Optional[str] = Field(None, alias="matched_node_uid")

    term_uids: Optional[list[str]] = Field(None, alias="term_uids")
    terms: Optional[list[TermDto]] = Field(None, alias="terms")
    data: Optional[dict] = Field(None, alias="data")
    model_config = ConfigDict(populate_by_name=True)


class MatchingResultDto(BaseModel):
    matching_items: List[MatchingItemDto]
