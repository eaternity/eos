import base64
from typing import Any, ClassVar, Optional

from pydantic_settings import SettingsConfigDict

from core.envprops import EnvProps


class Settings(EnvProps):
    _instance: ClassVar[Optional["Settings"]] = None
    _initialized: ClassVar[bool] = False

    TITLE: str = "ayce"

    JSON_LOGGING: bool = False
    LOG_LEVEL: str = "DEBUG"
    HOSTNAME: str = "localhost"
    PORT: str = "8040"
    BASE_PATH: str = "/v2"

    @property
    def EATERNITY_AUTH_KEY(self) -> str:
        basic_auth_key = base64.b64encode(f"{self.EATERNITY_TOKEN}:".encode("utf-8"))
        return basic_auth_key.decode("utf-8")

    @property
    def api_url(self) -> str:
        return f"http://{self.HOSTNAME}:{self.PORT}{self.BASE_PATH}"

    def __new__(cls, *args: tuple, **kwargs: dict[str, Any]) -> "Settings":
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, *args: tuple, **kwargs: dict[str, Any]) -> None:
        if self._initialized:
            return
        super().__init__(*args, **kwargs)
        self.__class__._initialized = True

    @classmethod
    def override_settings(cls: type["Settings"], **kwargs: dict[str, Any]) -> None:
        if cls._instance is None:
            cls._instance = cls()
        for key, value in kwargs.items():
            setattr(cls._instance, key, value)

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")
