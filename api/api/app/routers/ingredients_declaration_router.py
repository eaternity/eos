from fastapi import APIRouter, Depends, HTTPException, Request, Response
from fastapi.security import APIKeyHeader
from starlette import status
from structlog import get_logger

from api.app.injector import get_ingredients_declaration_mapping_controller
from api.controller.ingredients_declaration_controller import IngredientsDeclarationMappingController
from api.dto.v2.ingredients_declaration_dto import (
    IncomingIngredientsDeclarationMappingDto,
    IngredientsDeclarationMappingDto,
    IngredientsDeclarationMappingSearchInputDto,
    IngredientsDeclarationMappingsListingDto,
)

logger = get_logger()

ingredients_declaration_mapping_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@ingredients_declaration_mapping_router.get("/{declaration_uid}")
async def get_ingredients_declaration_mapping_by_uid(
    declaration_uid: str,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    ingredients_declaration_controller: IngredientsDeclarationMappingController = Depends(
        get_ingredients_declaration_mapping_controller
    ),
) -> IngredientsDeclarationMappingDto:
    if not request.state.is_superuser:
        # for now, we only allow matching modifications by superusers:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    ingredients_declaration_dto = await ingredients_declaration_controller.get_ingredients_declaration_mapping_by_uid(
        declaration_uid
    )

    if ingredients_declaration_dto:
        return ingredients_declaration_dto
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Ingredients declaration with UUID '{declaration_uid}' not found.",
        )


@ingredients_declaration_mapping_router.post("/by-text/get")
async def get_ingredients_declaration_mapping_by_text(
    declaration_search_dto: IngredientsDeclarationMappingSearchInputDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    ingredients_declaration_controller: IngredientsDeclarationMappingController = Depends(
        get_ingredients_declaration_mapping_controller
    ),
) -> IngredientsDeclarationMappingDto:
    if not request.state.is_superuser:
        # for now, we only allow matching modifications by superusers:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    ingredients_declaration_dto = await ingredients_declaration_controller.get_ingredients_declaration_mapping_by_text(
        declaration_search_dto.declaration_part
    )

    if ingredients_declaration_dto:
        return ingredients_declaration_dto
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Ingredients declaration with string '{declaration_search_dto.declaration_part}' not found.",
        )


@ingredients_declaration_mapping_router.post("/search/")
async def search_ingredients_declaration_mappings_by_text(
    declaration_search_dto: IngredientsDeclarationMappingSearchInputDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    ingredients_declaration_controller: IngredientsDeclarationMappingController = Depends(
        get_ingredients_declaration_mapping_controller
    ),
) -> IngredientsDeclarationMappingsListingDto:
    if not request.state.is_superuser:
        # for now, we only allow matching modifications by superusers:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    result_dto = await ingredients_declaration_controller.search_ingredients_declaration_mappings_by_text(
        declaration_search_dto.declaration_part
    )

    return result_dto


@ingredients_declaration_mapping_router.get("/all/")
async def get_all_ingredients_declaration_mappings(
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    ingredients_declaration_controller: IngredientsDeclarationMappingController = Depends(
        get_ingredients_declaration_mapping_controller
    ),
) -> IngredientsDeclarationMappingsListingDto:
    if not request.state.is_superuser:
        # for now, we only allow matching modifications by superusers:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    all_declarations_dto = await ingredients_declaration_controller.get_all_ingredients_declaration_mappings()

    return all_declarations_dto


@ingredients_declaration_mapping_router.post("/")
@ingredients_declaration_mapping_router.put("/")
async def post_or_put_ingredients_declaration_mapping(
    incoming_declaration_dto: IncomingIngredientsDeclarationMappingDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    ingredients_declaration_controller: IngredientsDeclarationMappingController = Depends(
        get_ingredients_declaration_mapping_controller
    ),
) -> IngredientsDeclarationMappingDto:
    if not request.state.is_superuser:
        # for now, we only allow matching modifications by superusers:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    try:
        return await ingredients_declaration_controller.upsert_ingredients_declaration_mapping(incoming_declaration_dto)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=e.args[0],
        )


@ingredients_declaration_mapping_router.delete("/{declaration_uid}", status_code=204, response_class=Response)
async def delete_ingredients_declaration_by_uid(
    declaration_uid: str,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    ingredients_declaration_controller: IngredientsDeclarationMappingController = Depends(
        get_ingredients_declaration_mapping_controller
    ),
) -> None:
    if not request.state.is_superuser:
        # for now, we only allow matching modifications by superusers:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    ingredients_declaration_deletion_status = (
        await ingredients_declaration_controller.delete_ingredients_declaration_mapping_by_uid(declaration_uid)
    )

    if ingredients_declaration_deletion_status:
        return
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)


@ingredients_declaration_mapping_router.post("/by-text/delete", status_code=204, response_class=Response)
async def delete_ingredients_declaration_mapping_by_text(
    declaration_search_dto: IngredientsDeclarationMappingSearchInputDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    ingredients_declaration_controller: IngredientsDeclarationMappingController = Depends(
        get_ingredients_declaration_mapping_controller
    ),
) -> None:
    if not request.state.is_superuser:
        # for now, we only allow matching modifications by superusers:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    ingredients_declaration_deletion_status = (
        await ingredients_declaration_controller.delete_ingredients_declaration_mapping_by_text(
            declaration_search_dto.declaration_part
        )
    )

    if ingredients_declaration_deletion_status:
        return
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)
