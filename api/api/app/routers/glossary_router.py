from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, Request, Response
from fastapi.security import APIKeyHeader
from starlette import status
from structlog import get_logger

from api.app.injector import get_access_controller, get_glossary_controller
from api.app.settings import Settings
from api.controller.access_group_controller import AccessGroupController
from api.controller.glossary_controller import GlossaryController
from api.dto.v2.glossary_dto import GetAllTermsDto, TermDto, TermDtoWithoutUid
from core.service.permission_service import check_if_can_create_terms, check_if_can_edit_terms

logger = get_logger()
settings = Settings()

glossary_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@glossary_router.get("/", response_model=GetAllTermsDto)
async def get_all_terms(
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    glossary_controller: GlossaryController = Depends(get_glossary_controller),
) -> GetAllTermsDto:
    return await glossary_controller.find_all_glossary()


@glossary_router.get("/cache", response_model=None)
async def get_all_terms_cache(
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    glossary_controller: GlossaryController = Depends(get_glossary_controller),
) -> Response:
    return Response(content=glossary_controller.get_binary_glossary_cache(), media_type="application/octet-stream")


@glossary_router.get("/subtree/")
async def get_sub_terms(
    term_uid: str = None,
    depth: int = 10,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    glossary_controller: GlossaryController = Depends(get_glossary_controller),
) -> GetAllTermsDto:
    if term_uid is None:
        # use root term as default:
        term_uid = settings.ROOT_GLOSSARY_TERM_UUID
    return glossary_controller.get_sub_terms_by_uid(UUID(term_uid), depth)


@glossary_router.get("/by-xid/{term_xid}")
async def get_terms_by_xid(
    term_xid: str,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    glossary_controller: GlossaryController = Depends(get_glossary_controller),
) -> GetAllTermsDto:
    terms_dto = await glossary_controller.get_terms_by_xid(term_xid)

    if not terms_dto:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Terms with XID '{term_xid}' do not exist.",
        )

    return terms_dto


@glossary_router.get("/by-xid/{term_xid}/{access_group_uid}")
async def get_term_by_xid_and_access_group_uid(
    term_xid: str,
    access_group_uid: str,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    glossary_controller: GlossaryController = Depends(get_glossary_controller),
) -> TermDto:
    term_dto = await glossary_controller.get_term_by_xid_and_access_group_uid(term_xid, access_group_uid)

    if not term_dto:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="term does not exist.",
        )

    return term_dto


@glossary_router.put("/by-xid/{term_xid}")
@glossary_router.put("/{term_uid}")
async def put_term(
    request: Request,
    term_dto: TermDtoWithoutUid,
    term_uid: str | None = None,
    term_xid: str | None = None,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    access_controller: AccessGroupController = Depends(get_access_controller),
    glossary_controller: GlossaryController = Depends(get_glossary_controller),
) -> None:
    if term_uid:
        term_uid = UUID(term_uid)

    # a term can be edited only if the user has correct permissions
    # and is a member of the group where this term currently is
    if term_xid:
        try:
            current_term_dto = await glossary_controller.get_term_by_xid_and_access_group_uid(
                term_xid,
                term_dto.access_group_uid,
            )
        except Exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Wrong access group UUID ({term_dto.access_group_uid}) specified in request body",
            ) from None
    else:
        try:
            current_term_dto = await glossary_controller.get_term_by_uid(term_uid)
        except Exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Wrong term UUID ({term_uid}) specified",
            ) from None

    # if this term exists, we check if this user is a member of this term's current access group
    if current_term_dto:
        permissions = await access_controller.get_user_permissions_by_uid(
            request.state,
            current_term_dto.access_group_uid,
        )
    # if this term does not exist, we check if this user is a member of target term's parent term access group
    # (same as in POST endpoint)
    else:
        try:
            parent_term = await glossary_controller.get_term_by_uid(UUID(term_dto.sub_class_of))
            permissions = await access_controller.get_user_permissions_by_uid(
                request.state,
                parent_term.access_group_uid,
            )
        except Exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Wrong `sub_class_of` UUID ({term_dto.sub_class_of}) specified",
            ) from None

    if not request.state.is_superuser:
        try:
            target_group_membership = await access_controller.check_if_user_is_group_member_by_group_uid(
                term_dto.access_group_uid,
                request.state.user,
            )
        except Exception:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Wrong access group UUID ({term_dto.access_group_uid}) specified in request body",
            ) from None
    else:
        target_group_membership = True

    if check_if_can_edit_terms(permissions) and target_group_membership:
        try:
            if term_xid is None:
                await glossary_controller.put_term_by_uid(term_dto, term_uid)
            else:
                await glossary_controller.put_term_by_xid(term_dto, term_xid)
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=str(e),
            ) from None
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@glossary_router.post("/")
async def create_term(
    request: Request,
    term_dto: TermDtoWithoutUid,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    access_controller: AccessGroupController = Depends(get_access_controller),
    glossary_controller: GlossaryController = Depends(get_glossary_controller),
) -> None:
    try:
        parent_term = await glossary_controller.get_term_by_uid(UUID(term_dto.sub_class_of))
        permissions = await access_controller.get_user_permissions_by_uid(
            request.state,
            parent_term.access_group_uid,
        )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Wrong `sub_class_of` UUID ({term_dto.sub_class_of}) specified",
        ) from None

    if not request.state.is_superuser:
        target_group_membership = await access_controller.check_if_user_is_group_member_by_group_uid(
            term_dto.access_group_uid,
            request.state.user,
        )
    else:
        target_group_membership = True

    if check_if_can_create_terms(permissions) and target_group_membership:
        await glossary_controller.create_term(term_dto)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )
