from typing import Annotated, List, Optional

from fastapi import APIRouter, Depends, HTTPException, Query, Request, Response, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from api.app.injector import get_access_controller, get_node_controller
from api.app.routers.calc_router import determine_namespace_uid_and_access_group_uid
from api.controller.access_group_controller import AccessGroupController
from api.controller.node_controller import NodeController
from api.dto.v2.access_group_dto import WrappedAccessGroupIdDto
from api.dto.v2.node_dto import BatchNodePropUpdateDto, BatchWithExistingNodeInputItemDto, ExistingNodeDto, NodeIDDto
from core.domain.nodes.node import Node
from core.service.permission_service import check_if_all_access_groups_can_read_node, check_if_can_update_node

logger = get_logger()

node_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)

NodeTypes = Annotated[object, Node.subclasses_with_self()]


@node_router.get("/cache", response_model=None)
async def get_node_cache(
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    node_controller: NodeController = Depends(get_node_controller),
) -> Response:
    return Response(content=node_controller.get_binary_node_cache(), media_type="application/octet-stream")


@node_router.post("/get_nodes")
async def get_all_nodes(
    request: Request,
    wrapped_access_group_ids: Optional[List[WrappedAccessGroupIdDto]] = None,
    node_type: Optional[str] = Query(default=None, description="Node type."),
    start_date: Optional[str] = Query(default=None, description="Node activity start date in YYYY-MM-DD format."),
    end_date: Optional[str] = Query(default=None, description="Node activity end date in YYYY-MM-DD format."),
    credentials: APIKeyHeader = Depends(api_key_header),
    node_controller: NodeController = Depends(get_node_controller),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> List[NodeTypes]:
    """Get all nodes in an access group."""
    if not wrapped_access_group_ids:
        wrapped_access_group_ids = [None]

    wrapped_access_group_ids = [
        await access_controller.fill_in_missing_info_in_wrapped_access_group_id(request.state, wrapped_access_group_id)
        for wrapped_access_group_id in wrapped_access_group_ids
    ]

    access_group_uids = list(
        set(wrapped_access_group_id.access_group.uid for wrapped_access_group_id in wrapped_access_group_ids)
    )
    permissions_by_access_group_uid = await access_controller.get_user_permissions_by_multiple_uids(
        request_state=request.state, access_group_uids=access_group_uids
    )

    if check_if_all_access_groups_can_read_node(permissions_by_access_group_uid, access_group_uids):
        nodes = await node_controller.get_all_nodes_by_access_group_uids(
            access_group_uids,
            node_type=node_type,
            start_date=node_controller.parse_date(start_date),
            end_date=node_controller.parse_date(end_date),
        )
        return nodes
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@node_router.post("/update_nodes")
async def update_nodes(
    request: Request,
    batch_node_prop_update_dto: BatchNodePropUpdateDto,
    credentials: APIKeyHeader = Depends(api_key_header),
    node_controller: NodeController = Depends(get_node_controller),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> List[bool]:
    """Batch update node properties without triggering recalculations."""
    update_status = []

    for node in batch_node_prop_update_dto.nodes:
        try:
            # First determine namespace and access group info
            batch_item_dto = BatchWithExistingNodeInputItemDto(
                namespace_uid=node.namespace_uid,
                nodes_dto=ExistingNodeDto(
                    existing_node=NodeIDDto(
                        uid=node.uid,
                        xid=node.xid,
                        access_group_uid=node.access_group_uid,
                        access_group_xid=node.access_group_xid,
                    )
                ),
            )
            batch_item_dto = await determine_namespace_uid_and_access_group_uid(
                access_controller, node_controller, batch_item_dto, request
            )

            existing_node = batch_item_dto.nodes_dto.existing_node
            permissions = await access_controller.get_user_permissions_by_uid(
                request.state, str(existing_node.access_group_uid)
            )

            # Check if user has update permission for this node
            if check_if_can_update_node(permissions):
                # Update the node with the existing node's uid for which the permission was checked
                node.uid = existing_node.uid
                was_updated = await node_controller.node_service.update_node_props(node)
                update_status.append(was_updated)
            else:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail=f"You don't have rights to update node with uid {node.uid}.",
                )
        except PermissionError as err:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Unauthorized to update node.",
            ) from err
        except HTTPException as e:
            raise e

    return update_status
