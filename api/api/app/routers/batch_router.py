from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from structlog import get_logger

from api.app.injector import get_admin_controller
from api.app.settings import Settings
from api.controller.admin_controller import AdminController
from api.dto.v2.customer_dto import CustomerCreateStatusDto, WrappedCreateCustomerDto
from api.dto.v2.user_dto import UserDto

logger = get_logger()
batch_router = APIRouter()
security = HTTPBasic()
settings = Settings()


@batch_router.put("/customers/{namespace_xid}")
async def put_customer(
    namespace_xid: str,
    customer: WrappedCreateCustomerDto,
    credentials: HTTPBasicCredentials = Depends(security),
    admin_controller: AdminController = Depends(get_admin_controller),
) -> CustomerCreateStatusDto:
    logger.info("authenticated username: ", namespace=credentials.username)

    if credentials.username != settings.EATERNITY_TOKEN:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect API Key.",
            headers={"WWW-Authenticate": "Basic"},
        )

    customer_create_status_dto = await admin_controller.create_namespace_by_xid(
        namespace_xid, customer.customer, customer.auth_token
    )

    return customer_create_status_dto


@batch_router.get("/customers/{namespace_xid}")
async def get_customer(
    namespace_xid: str,
    credentials: HTTPBasicCredentials = Depends(security),
    admin_controller: AdminController = Depends(get_admin_controller),
) -> List[UserDto]:
    logger.info("authenticated username: ", namespace=credentials.username)

    if credentials.username != settings.EATERNITY_TOKEN:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect API Key.",
            headers={"WWW-Authenticate": "Basic"},
        )

    users_dto = await admin_controller.get_users_from_namespace(namespace_xid)

    return users_dto
