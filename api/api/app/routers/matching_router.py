from fastapi import APIRouter, Depends, HTTPException, Request, Response
from fastapi.security import APIKeyHeader
from starlette import status
from structlog import get_logger

from api.app.injector import get_matching_item_controller
from api.controller.matching_controller import MatchingController
from api.dto.v2.matching_dto import MatchingItemDto, MatchingResultDto

logger = get_logger()

matching_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@matching_router.get("/cache", response_model=None)
async def get_matching_terms_cache(
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    matching_controller: MatchingController = Depends(get_matching_item_controller),
) -> Response:
    return Response(
        content=matching_controller.get_binary_matching_items_cache(), media_type="application/octet-stream"
    )


@matching_router.get("/{matching_string}")
async def get_matching_by_string(
    matching_string: str,
    credentials: APIKeyHeader = Depends(api_key_header),
    matching_controller: MatchingController = Depends(get_matching_item_controller),
) -> MatchingResultDto:
    """Returns a list of terms that match the given *lowercased* matching string."""
    matchings: MatchingResultDto = await matching_controller.get_matching_by_string(matching_string)

    if len(matchings.matching_items) > 0:
        return matchings
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Terms matching '{matching_string}' not found",
        )


@matching_router.post("/")
async def perform_matching(
    matching_item: MatchingItemDto,
    credentials: APIKeyHeader = Depends(api_key_header),
    matching_controller: MatchingController = Depends(get_matching_item_controller),
) -> MatchingResultDto:
    """Returns a list of terms that match the given *lowercased* matching string."""
    matchings: MatchingResultDto = await matching_controller.get_matching_by_string(
        matching_string=matching_item.matching_string,
        gfm_name=matching_item.gap_filling_module,
        filter_lang=matching_item.lang,
        filter_context=matching_item.data,
    )

    if len(matchings.matching_items) > 0:
        return matchings
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Terms matching '{matching_item.matching_string}' not found",
        )


@matching_router.put("/")
async def put_matching_string(
    matching_item: MatchingItemDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    matching_controller: MatchingController = Depends(get_matching_item_controller),
) -> MatchingItemDto:
    """Creates a new matching item. Note that the matching string is lowercased."""
    if not request.state.is_superuser:
        # for now, we only allow matching modifications by superusers:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    try:
        return await matching_controller.put_matching_string(matching_item)
    except KeyError as ke:  # term_uid not found
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=ke.args[0],
        ) from None
