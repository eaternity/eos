import asyncio
import io
import time
import uuid
from typing import List, Optional
from uuid import uuid4

import pandas as pd
from fastapi import APIRouter, Depends, HTTPException, Query, Request, status
from fastapi.security import APIKeyHeader
from starlette.responses import StreamingResponse
from structlog import get_logger

from api.app.injector import get_access_controller, get_node_controller, get_report_generation_controller
from api.controller.access_group_controller import AccessGroupController
from api.controller.node_controller import NodeController
from api.controller.report_generation_controller import ReportGenerationController, SupplyFromRecipeConfigEnum
from api.dto.v2.access_group_dto import LegacyAPIResponseBehavior, WrappedAccessGroupIdDto
from api.dto.v2.node_dto import (
    BatchInputDto,
    BatchOutputDto,
    BatchWithExistingNodeInputItemDto,
    BatchWithExistingNodeOutputItemDto,
    BatchWithExistingNodesInputDto,
    BatchWithExistingNodesOutputDto,
    ExistingNodeDto,
)
from api.dto.v2.report_dto import CSVReportOutputDto, ErrorStatisticsDto, ReportOutputDto
from core.domain.access_group_node import AccessGroup
from core.domain.calculation import Calculation
from core.domain.edge import EdgeToUpsert, XidAccessGroupUid
from core.domain.nodes.activity_node import ActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.root_with_subflows_dto import ExistingRootDto, NodeIDDto
from core.service.messaging_service import Priority
from core.service.permission_service import (
    check_if_can_delete_node,
    check_if_can_post_node,
    check_if_can_read_node,
    check_if_can_update_node,
)

logger = get_logger()

calc_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)

# FIXME: check if we can remove the unused arguments (ARG001)


async def determine_namespace_uid_and_access_group_uid(
    access_controller: AccessGroupController,
    node_controller: NodeController,
    item_in: Calculation | BatchWithExistingNodeInputItemDto,
    request: Request,
) -> Calculation | BatchWithExistingNodeInputItemDto:
    """Helper function to determine the namespace_uid and access_group_uid for a given item_in."""

    async def _find_access_group_uid_from_xid() -> None:
        access_group: AccessGroup = await access_controller.get_access_group_by_xid(
            node_info.access_group_xid, item_in.namespace_uid
        )
        # get this access group's UUID and verify that it exists
        node_info.access_group_uid = uuid.UUID(access_group.uid)
        # make sure that the created node is in the correct namespace of the given access group:
        item_in.namespace_uid = access_group.namespace_uid
        # load any access_group_specific settings
        if (
            isinstance(item_in, Calculation)
            and not item_in.legacyAPI_response_behavior
            and access_group.data
            and access_group.data.get("legacyAPI_response_behavior")
        ):
            item_in.legacyAPI_response_behavior = LegacyAPIResponseBehavior(
                **access_group.data["legacyAPI_response_behavior"]
            )

    async def _find_access_group_xid_from_uid() -> None:
        # get this group's XID and namespace UUID and verify that this access group exists
        group_with_xid = await access_controller.get_access_group_by_uid(str(node_info.access_group_uid))
        node_info.access_group_xid = group_with_xid.xid
        # make sure that the created node is in the correct namespace of the given access group:
        item_in.namespace_uid = group_with_xid.namespace_uid
        # load any access_group_specific settings
        if (
            isinstance(item_in, Calculation)
            and not item_in.legacyAPI_response_behavior
            and group_with_xid.data
            and group_with_xid.data.get("legacyAPI_response_behavior")
        ):
            item_in.legacyAPI_response_behavior = LegacyAPIResponseBehavior(
                **group_with_xid.data["legacyAPI_response_behavior"]
            )

    def _set_legacy_access_group_uid_and_namespace_uid() -> None:
        node_info.access_group_uid = uuid.UUID(request.state.user_legacy_default_access_group.uid)
        item_in.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if item_in.namespace_uid is None:
        item_in.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if isinstance(item_in, Calculation):
        root = item_in.input_root
    else:
        root = item_in.nodes_dto

    if isinstance(root, ExistingRootDto):
        node_info = root.existing_root
        existing_node = True
    elif isinstance(root, ExistingNodeDto):
        node_info = root.existing_node
        existing_node = True
    else:
        node_info = root.activity
        existing_node = False

    if node_info.uid is None:
        if node_info.xid is None:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="a node uid or xid is required")
        node_info.uid = await node_controller.get_uid_by_xid(item_in.namespace_uid, node_info.xid)

    if node_info.uid is not None:
        try:
            node_access_group_uid_from_db = await node_controller.node_service.find_access_group_uid_by_uid(
                node_info.uid
            )
        except (KeyError, TypeError) as e:
            raise PermissionError("access_group_uid is not set for this node uid.") from e
    else:
        node_access_group_uid_from_db = None

    if existing_node:
        if node_info.access_group_uid is not None:
            await _find_access_group_xid_from_uid()
        elif node_info.access_group_xid is not None:
            await _find_access_group_uid_from_xid()
        elif node_access_group_uid_from_db is not None:
            node_info.access_group_uid = node_access_group_uid_from_db
            await _find_access_group_xid_from_uid()
        else:
            _set_legacy_access_group_uid_and_namespace_uid()

        if node_access_group_uid_from_db and node_info.access_group_uid != node_access_group_uid_from_db:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="node was not found in that access_group")
    else:
        if node_access_group_uid_from_db is not None:
            permissions = await access_controller.get_user_permissions_by_uid(
                request.state, str(node_access_group_uid_from_db)
            )
            if not check_if_can_update_node(permissions):
                raise PermissionError("user has no permission to update the access_group of node.")

        if node_info.access_group_xid is None and node_info.access_group_uid is None:
            # in case no group_id was specified, we use the default access_group:
            _set_legacy_access_group_uid_and_namespace_uid()

        if node_info.access_group_xid:
            await _find_access_group_uid_from_xid()

        elif node_info.access_group_uid:
            await _find_access_group_xid_from_uid()

    return item_in


async def assign_uid_namespace_access_group_and_check_permission(
    access_controller: AccessGroupController, node_controller: NodeController, request: Request, item_in: Calculation
) -> Calculation | None:
    try:
        item_in = await determine_namespace_uid_and_access_group_uid(
            access_controller, node_controller, item_in, request
        )
    except PermissionError:
        item_out: Calculation = item_in.model_copy(deep=True)
        item_out.statuscode = status.HTTP_401_UNAUTHORIZED
        if type(item_in.input_root) is ExistingRootDto:
            node_uid = item_in.input_root.existing_root.uid
        else:
            node_uid = item_in.input_root.activity.uid

        item_out.message = f"Node uid {node_uid} already exists in the database under a different access group."
        return item_out
    except HTTPException as e:
        item_out: Calculation = item_in.model_copy(deep=True)
        item_out.statuscode = e.status_code
        item_out.message = e.detail
        return item_out

    if type(item_in.input_root) is ExistingRootDto:
        input_existing_root_dto = item_in.input_root.existing_root
        permissions = await access_controller.get_user_permissions_by_uid(
            request.state, str(input_existing_root_dto.access_group_uid)
        )
        permission_granted = check_if_can_read_node(permissions)
    else:
        activity = item_in.input_root.activity
        permissions = await access_controller.get_user_permissions_by_uid(request.state, str(activity.access_group_uid))
        permission_granted = check_if_can_post_node(permissions)
        if permission_granted:
            item_in.input_root.activity.uid = await node_controller.generate_or_get_uid(
                item_in.input_root.activity, namespace_uid=item_in.namespace_uid, assign_new_uid=True
            )

    if not permission_granted:
        item_out: Calculation = item_in.model_copy(deep=True)
        item_out.statuscode = status.HTTP_401_UNAUTHORIZED
        item_out.message = "You don't have rights for this action."
        return item_out

    return None


async def fill_root_flow_and_activity_uids(
    node_controller: NodeController, item_in: Calculation, item_out: Calculation, allow_deleted: bool = False
) -> bool:
    root_flow_or_activity = await node_controller.get_root_flow_or_activity_node(
        item_in.namespace_uid, item_in.input_root, allow_deleted
    )
    if root_flow_or_activity:
        # Check if the retrieved node is really in the requested access group:
        if root_flow_or_activity.access_group_uid != item_in.input_root.existing_root.access_group_uid:
            root_flow_or_activity = None
    if root_flow_or_activity:
        if isinstance(root_flow_or_activity, ActivityNode):
            item_out.child_of_root_node_uid = root_flow_or_activity.uid
            if root_flow_or_activity.get_parent_nodes():
                item_out.root_node_uid = root_flow_or_activity.get_parent_nodes()[0].uid
        elif isinstance(root_flow_or_activity, FlowNode):
            item_out.root_node_uid = root_flow_or_activity.uid
            item_out.child_of_root_node_uid = root_flow_or_activity.get_sub_nodes()[0].uid
    if root_flow_or_activity:
        return True
    else:
        return False


async def insert_batch_items_and_edges_between(
    access_controller: AccessGroupController,
    node_controller: NodeController,
    request: Request,
    batch_input_dto: BatchInputDto,
    allow_deleted: bool = False,
) -> List[Calculation]:
    """Corresponds to the first and second passes in create_and_calculate_graphs.

    Written into a separate function to faciliate testing.
    """
    # first pass: create all nodes (so that they can link to each other in the second pass)
    output_items: List[Calculation] = []
    output_item_index_dict: dict[str, int] = {}
    edges_to_insert_per_batch_item: dict[str, List[EdgeToUpsert]] = {}
    edges_to_delete_per_batch_item: dict[str, List[EdgeToUpsert]] = {}
    md5_hash_per_batch_item: dict[str, uuid.UUID] = {}
    xid_access_group_uids_to_map_to_uid: List[XidAccessGroupUid] = []

    async def process_item(item_in: Calculation) -> Calculation:
        item_out: Calculation = item_in.model_copy(deep=True)
        if type(item_in.input_root) is ExistingRootDto:
            was_found = await fill_root_flow_and_activity_uids(node_controller, item_in, item_out, allow_deleted)
            if not was_found:
                item_out.statuscode = status.HTTP_404_NOT_FOUND
                item_out.message = "The requested node does not exist."
                return item_out

        else:
            if item_in.input_root.sub_flows:
                if len(item_in.input_root.sub_flows) > 8000:
                    item_out.statuscode = status.HTTP_413_REQUEST_ENTITY_TOO_LARGE
                    item_out.message = (
                        "An activity cannot have more than 8000 sub-flows. \n"
                        "Nest the sub-flows into chunks of sub-activities of smaller sizes.\n"
                        "E.g., try dividing the ingredients into sub-recipes of smaller sizes. Then,"
                        "add these sub-recipes as the ingredients of the top-recipe."
                    )
                    return item_out

            (
                edges_to_delete,
                edges_to_insert,
                self_uid,
                parent_uid,
                not_changed,
                md5_hash,
            ) = await node_controller.upsert_recipe_node(item_in)
            if edges_to_insert:
                edges_to_insert_per_batch_item[self_uid] = edges_to_insert
            if edges_to_delete:
                edges_to_delete_per_batch_item[self_uid] = edges_to_delete
            if md5_hash:
                md5_hash_per_batch_item[self_uid] = md5_hash
            item_out.child_of_root_node_uid = uuid.UUID(self_uid)
            if parent_uid:
                item_out.root_node_uid = uuid.UUID(parent_uid)

            xid_access_group_uids_to_map_to_uid.extend(
                [
                    edge.child_xid_access_group_uid
                    for edge in edges_to_insert
                    if edge.child_xid_access_group_uid is not None
                ]
            )

        item_out.statuscode = status.HTTP_200_OK
        item_out.message = ""
        return item_out

    start_ns = time.perf_counter_ns()
    tasks = [
        assign_uid_namespace_access_group_and_check_permission(access_controller, node_controller, request, item)
        for item in batch_input_dto.batch
    ]
    results = list(await asyncio.gather(*tasks))
    duration_sec = (time.perf_counter_ns() - start_ns) / 1e9
    logger.info(
        "finished zeroth pass (assigning uid, namespace, access_group and checking permission) of batch.",
        duration_sec=duration_sec,
    )

    start_ns = time.perf_counter_ns()
    item_tasks = [process_item(item) for result, item in zip(results, batch_input_dto.batch) if result is None]
    process_results = list(await asyncio.gather(*item_tasks))

    cur = 0
    for i in range(len(results)):
        if results[i] is None:
            results[i] = process_results[cur]
            cur += 1

    duration_sec = (time.perf_counter_ns() - start_ns) / 1e9
    logger.info("finished first pass (upserting nodes) of batch.", duration_sec=duration_sec)

    for item in results:
        if item.statuscode == status.HTTP_200_OK:
            output_item_activity_uid = item.child_of_root_node_uid
            output_item_index_dict[str(output_item_activity_uid)] = len(output_items)
        output_items.append(item)

    start_ns = time.perf_counter_ns()
    edge_tasks = [
        node_controller.process_link_to_subnode_edges(
            batch_activity_uid,
            edges_to_insert_per_batch_item.get(batch_activity_uid, []),
            edges_to_delete_per_batch_item.get(batch_activity_uid, []),
            md5_hash_per_batch_item.get(batch_activity_uid, None),
            xid_access_group_uids_to_map_to_uid,
        )
        for batch_activity_uid in (edges_to_insert_per_batch_item.keys() | edges_to_delete_per_batch_item.keys())
    ]
    edge_task_results = await asyncio.gather(*edge_tasks)
    for batch_activity_uid, result in edge_task_results:
        if result is False:
            output_items[output_item_index_dict[batch_activity_uid]].statuscode = 612
            output_items[
                output_item_index_dict[batch_activity_uid]
            ].message = "Referenced sub-recipe is not in the database."

    duration_sec = (time.perf_counter_ns() - start_ns) / 1e9
    logger.info("finished second pass (inserting edges) of batch.", duration_sec=duration_sec)

    return output_items


@calc_router.get("/graphs/{calculation_uid}")
async def get_calculation_result(
    calculation_uid: str,
    request: Request,  # noqa: ARG001
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
    access_controller: AccessGroupController = Depends(get_access_controller),  # noqa: B008, ARG001
) -> Calculation:
    """Get the result of a calculation."""
    loaded_calculation_result, access_group_uid = await node_controller.load_old_calculation_result(calculation_uid)
    if not loaded_calculation_result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"{calculation_uid} is not found.")

    permissions = await access_controller.get_user_permissions_by_uid(request.state, str(access_group_uid))

    if check_if_can_read_node(permissions):
        return loaded_calculation_result
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@calc_router.post("/graphs")
async def post_graphs(
    batch_input_dto: BatchInputDto,
    request: Request,
    return_immediately: bool = False,
    use_queue: bool = True,
    priority: Priority = Query(
        Priority.LOW,
        description="Priority of this calculation, 0 is lowest.",
    ),
    allow_deleted: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    node_controller: NodeController = Depends(get_node_controller),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> BatchOutputDto:
    return await create_and_calculate_graphs(
        batch_input_dto=batch_input_dto,
        request=request,
        return_immediately=return_immediately,
        use_queue=use_queue,
        priority=priority,
        allow_deleted=allow_deleted,
        credentials=credentials,
        node_controller=node_controller,
        access_controller=access_controller,
    )


async def create_and_calculate_graphs(
    batch_input_dto: BatchInputDto,
    request: Request,
    return_immediately: bool,
    use_queue: bool,
    priority: Priority,
    allow_deleted: bool,
    credentials: APIKeyHeader,  # noqa: B008, ARG001
    node_controller: NodeController,  # noqa: B008
    access_controller: AccessGroupController,  # noqa: B008
) -> BatchOutputDto:
    """Generate a graph or multiple graphs and execute the orchestrator on the specified graph(s)."""
    logger.info("called create_and_calculate_graphs")

    output_items = await insert_batch_items_and_edges_between(
        access_controller, node_controller, request, batch_input_dto, allow_deleted
    )
    # third pass: run orchestrator for each item in the batch:
    start_ns = time.perf_counter_ns()
    tasks = []
    for calculation in output_items:
        if not calculation.skip_calc and calculation.statuscode == status.HTTP_200_OK:
            calculation = await node_controller.init_calculation(calculation)
            tasks.append(
                asyncio.create_task(
                    node_controller.perform_calculation(calculation, use_queue=use_queue, priority=priority)
                )
            )

    if not return_immediately:
        calculation_results = await asyncio.gather(*tasks)
        for index, calculation in enumerate(calculation_results):
            output_items[index] = calculation
    duration_sec = (time.perf_counter_ns() - start_ns) / 1e9
    logger.info("finished third pass (calculations) of batch.", duration_sec=duration_sec)

    # fourth pass: set transient as deleted.
    start_ns = time.perf_counter_ns()
    upsert_tasks = [
        node_controller.node_service.mark_recipe_as_deleted(batch_input_dto.batch[batch_item_index].input_root.activity)
        for batch_item_index, calculation in enumerate(output_items)
        if calculation.transient and calculation.statuscode == status.HTTP_200_OK
    ]
    if upsert_tasks:
        await asyncio.gather(*upsert_tasks)
    duration_sec = (time.perf_counter_ns() - start_ns) / 1e9
    logger.info("finished fourth pass (set-transient) of batch.", duration_sec=duration_sec)

    return BatchOutputDto(batch=output_items)


@calc_router.get("/csv/uid/{node_uid}")
@calc_router.get("/csv/xid/{node_xid}")
async def get_node_as_csv(
    request: Request,
    node_uid: str = None,
    node_xid: str = None,
    allow_deleted: bool = False,
    priority: Priority = Query(
        Priority.LOW,
        description="Priority of this calculation, 0 is lowest.",
    ),
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
    access_controller: AccessGroupController = Depends(get_access_controller),  # noqa: B008
) -> StreamingResponse:
    """Export node and sub-activities as csv."""
    logger.info("called get_node_as_csv")

    if node_uid:
        existing_root = NodeIDDto(uid=node_uid)
    elif node_xid:
        existing_root = NodeIDDto(xid=node_xid)
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="node_uid or node_xid is required")

    calculation = Calculation(
        input_root=ExistingRootDto(existing_root=existing_root),
        return_data_as_table=True,
        return_final_graph=True,
        max_depth_of_returned_graph=None,
        max_depth_of_returned_table=1,
        explicitly_attach_cached_elementary_resource_emission=False,
    )
    calculation_with_error = await assign_uid_namespace_access_group_and_check_permission(
        access_controller, node_controller, request, calculation
    )
    if calculation_with_error is not None:
        raise HTTPException(status_code=calculation_with_error.statuscode, detail=calculation_with_error.message)

    calculation_out: Calculation = calculation.model_copy(deep=True)
    was_found = await fill_root_flow_and_activity_uids(node_controller, calculation, calculation_out, allow_deleted)
    if not was_found:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="The requested node does not exist.")

    start_ns = time.perf_counter_ns()
    calculation_out = await node_controller.init_calculation(calculation_out)
    calculation_result = await node_controller.perform_calculation(calculation_out, priority=priority)
    duration_sec = (time.perf_counter_ns() - start_ns) / 1e9
    logger.info("finished calculation.", duration_sec=duration_sec)

    stream = io.StringIO()
    data_errors = "\n\n".join(e.message for e in calculation_result.data_errors)
    if calculation_result.final_graph_table:
        calculation_result.final_graph_table.append({"name": data_errors})
        df = pd.DataFrame(calculation_result.final_graph_table)
        df.to_csv(stream, index=False)
        response = StreamingResponse(iter([stream.getvalue()]), media_type="text/csv")
        response.headers["Content-Disposition"] = "attachment; filename=export.csv"
    else:
        response = StreamingResponse(iter([data_errors]), media_type="text/plain")
    return response


@calc_router.post("/delete_graphs")
async def delete_graphs(
    batch_input_dto: BatchWithExistingNodesInputDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),  # noqa: B008, ARG001
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
    access_controller: AccessGroupController = Depends(get_access_controller),  # noqa: B008
) -> BatchOutputDto:
    """Delete the nodes in a graph or in multiple graphs."""
    logger.info("called delete_graphs")
    output_items: List[BatchWithExistingNodeOutputItemDto] = []
    for item_in in batch_input_dto.batch:
        try:
            item_in = await determine_namespace_uid_and_access_group_uid(
                access_controller, node_controller, item_in, request
            )
            old_access_group_permission_granted = True
        except PermissionError:
            output_items.append(
                BatchWithExistingNodeOutputItemDto(
                    statuscode=status.HTTP_401_UNAUTHORIZED,
                    message=f"Node uid {item_in.nodes_dto.existing_node.uid} already exists in the database under"
                    " a different access group.",
                    request_id=item_in.request_id,
                )
            )
            old_access_group_permission_granted = False
        except HTTPException as e:
            output_items.append(
                BatchWithExistingNodeOutputItemDto(
                    statuscode=e.status_code,
                    message=e.detail,
                    request_id=item_in.request_id,
                )
            )
            old_access_group_permission_granted = False

        if not old_access_group_permission_granted:
            continue

        existing_node = item_in.nodes_dto.existing_node
        permissions = await access_controller.get_user_permissions_by_uid(
            request.state, str(existing_node.access_group_uid)
        )
        if check_if_can_delete_node(permissions):
            if existing_node.xid:
                result = await node_controller.delete_node_by_xid(existing_node.xid, item_in.namespace_uid)
            else:
                result = await node_controller.delete_node_by_uid(existing_node.uid)
            if not result:
                output_items.append(
                    BatchWithExistingNodeOutputItemDto(
                        statuscode=status.HTTP_400_BAD_REQUEST,
                        message="",
                        request_id=item_in.request_id,
                    )
                )
            else:
                output_items.append(
                    BatchWithExistingNodeOutputItemDto(
                        statuscode=status.HTTP_200_OK,
                        message="",
                        request_id=item_in.request_id,
                    )
                )
        else:
            output_items.append(
                BatchWithExistingNodeOutputItemDto(
                    statuscode=status.HTTP_401_UNAUTHORIZED,
                    message="You don't have rights for this action.",
                    request_id=item_in.request_id,
                )
            )
    return BatchWithExistingNodesOutputDto(batch=output_items)


@calc_router.post("/reports/aggregated-access-groups-data")
async def get_aggregated_access_groups_data(
    request: Request,
    wrapped_access_group_ids: Optional[List[WrappedAccessGroupIdDto]] = None,
    start_date: str = Query(..., description="Reporting start date in YYYY-MM-DD format."),
    end_date: str = Query(..., description="Reporting end date in YYYY-MM-DD format."),
    supply_from_recipe_config: Optional[SupplyFromRecipeConfigEnum] = Query(
        default=None, description="Generate supplies from recipes according to the specified configuration."
    ),
    return_only_mustache_json: bool = Query(
        default=True,
        description="If true, only the mustache JSON will be returned, omitting the full calculation details.",
    ),
    credentials: APIKeyHeader = Depends(api_key_header),
    access_group_controller: AccessGroupController = Depends(get_access_controller),  # noqa: B008
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
    report_generation_controller: ReportGenerationController = Depends(get_report_generation_controller),  # noqa: B008
) -> ReportOutputDto:
    logger.info("called get_aggregated_access_groups_data.")
    access_group_uids = await access_group_controller.multiple_access_group_control(
        wrapped_access_group_ids=wrapped_access_group_ids, request_state=request.state
    )

    logger.info(f"Getting all node uids for access groups {wrapped_access_group_ids} from {start_date} to {end_date}.")

    start_date_formatted = node_controller.parse_date(start_date)
    end_date_formatted = node_controller.parse_date(end_date)

    max_cache_age_in_days = 7

    (
        all_recipe_cache_info,
        all_recipe_infos,
        recipes_missing_cache,
    ) = await report_generation_controller.get_all_recipe_infos(
        access_group_uids=access_group_uids,
        start_date=start_date_formatted,
        end_date=end_date_formatted,
        max_cache_age_in_days=max_cache_age_in_days,
    )
    (
        all_supply_infos,
        supplies_missing_cache,
        recipes_missing_portion_numbers,
    ) = await report_generation_controller.get_all_supply_infos(
        access_group_uids=access_group_uids,
        start_date=start_date_formatted,
        end_date=end_date_formatted,
        supply_from_recipe_config=supply_from_recipe_config,
        all_recipe_cache_info=all_recipe_cache_info,
        max_cache_age_in_days=max_cache_age_in_days,
    )

    tasks = []
    for missing_cache in recipes_missing_cache + supplies_missing_cache:
        batch_input = BatchInputDto(
            batch=[
                report_generation_controller.generate_calculation_object_for_caching_calculation(
                    input_root=ExistingRootDto(
                        existing_root=NodeIDDto(
                            uid=missing_cache.root_flow_uid, access_group_uid=missing_cache.access_group_uid
                        )
                    )
                )
            ]
        )
        tasks.append(
            create_and_calculate_graphs(
                batch_input_dto=batch_input,
                request=request,
                return_immediately=False,
                use_queue=True,
                priority=Priority.LOW,
                allow_deleted=True,
                credentials=credentials,
                node_controller=node_controller,
                access_controller=access_group_controller,
            )
        )

    results = await asyncio.gather(*tasks)

    incomplete_results = [
        str(result.batch[0].input_root.existing_root.uid)
        for result in results
        if not (
            result.batch[0].final_root
            and result.batch[0].final_root.flow.impact_assessment
            and result.batch[0].final_root.flow.daily_food_unit
        )
    ] + recipes_missing_portion_numbers

    calculation_results = {}

    for idx, all_node_infos in enumerate((all_recipe_infos, all_supply_infos)):
        if all_node_infos:
            node_infos_to_compute = [
                node_info for node_info in all_node_infos if str(node_info.uid) not in incomplete_results
            ]
            if not node_infos_to_compute:
                if idx == 0:
                    logger.info("Skip recipe calculation, as no recipe_info is completely matched.")
                    continue
                elif idx == 1:
                    raise ValueError("Supply calculation is impossible, as no supply_info is completely matched.")

            calculation = Calculation(
                uid=uuid4(), linked_sub_node_infos=node_infos_to_compute, use_cached_final_root=True
            )

            result = await node_controller.perform_calculation(
                calculation=calculation, use_queue=False, priority=Priority.LOW
            )
            if idx == 0:
                calculation_results["recipe"] = result
            elif idx == 1:
                calculation_results["supply"] = result

    report_output = ReportOutputDto(**calculation_results)
    if incomplete_results:
        logger.info(f"Failed to calculate {len(incomplete_results)} nodes.")
        logger.info(f"Failed node uids: {incomplete_results}")
    report_output.error_statistics = ErrorStatisticsDto(
        number_of_successful_calculations=len(all_recipe_infos) + len(all_supply_infos) - len(incomplete_results),
        number_of_failed_calculations=len(incomplete_results),
        failed_node_uids=incomplete_results,
    )
    await report_generation_controller.compile_mustache_json_for_report_generation(
        report_output=report_output, node_controller=node_controller
    )
    if return_only_mustache_json:
        report_output.recipe = None
        report_output.supply = None
    return report_output


@calc_router.post("/reports/tabular-access-groups-data")
async def get_tabular_access_groups_data(
    request: Request,
    wrapped_access_group_ids: Optional[List[WrappedAccessGroupIdDto]] = None,
    node_type: str = Query(..., description="Node type for which to generate the report."),
    start_date: str = Query(..., description="Reporting start date in YYYY-MM-DD format."),
    end_date: str = Query(..., description="Reporting end date in YYYY-MM-DD format."),
    credentials: APIKeyHeader = Depends(api_key_header),
    access_group_controller: AccessGroupController = Depends(get_access_controller),  # noqa: B008
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
    report_generation_controller: ReportGenerationController = Depends(get_report_generation_controller),  # noqa: B008
) -> CSVReportOutputDto:
    logger.info("called get_tabular_access_groups_data.")
    access_group_uids = await access_group_controller.multiple_access_group_control(
        wrapped_access_group_ids=wrapped_access_group_ids, request_state=request.state
    )

    logger.info(f"Getting all node uids for access groups {wrapped_access_group_ids} from {start_date} to {end_date}.")

    start_date_formatted = node_controller.parse_date(start_date)
    end_date_formatted = node_controller.parse_date(end_date)

    max_cache_age_in_days = 7
    final_graph_table_criterion = report_generation_controller.get_default_final_graph_table_criterion_for_reports()

    root_products_cache_info = (
        await report_generation_controller.get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
            access_group_uids=access_group_uids,
            start_date=start_date_formatted,
            end_date=end_date_formatted,
            node_type=node_type,
            get_tabular_cache=True,
        )
    )

    root_products_with_cache = [
        root_product_cache
        for root_product_cache in root_products_cache_info
        if report_generation_controller.use_post_calc_cache(
            root_product_cache, max_cache_age_in_days, final_graph_table_criterion
        )
    ]

    root_products_missing_cache = [
        root_product_cache
        for root_product_cache in root_products_cache_info
        if root_product_cache not in root_products_with_cache
    ]

    tasks = []
    for missing_cache in root_products_missing_cache:
        batch_input = BatchInputDto(
            batch=[
                report_generation_controller.generate_calculation_object_for_caching_calculation(
                    input_root=ExistingRootDto(
                        existing_root=NodeIDDto(
                            uid=missing_cache.root_flow_uid, access_group_uid=missing_cache.access_group_uid
                        )
                    )
                )
            ]
        )
        tasks.append(
            create_and_calculate_graphs(
                batch_input_dto=batch_input,
                request=request,
                return_immediately=False,
                use_queue=True,
                priority=Priority.LOW,
                allow_deleted=True,
                credentials=credentials,
                node_controller=node_controller,
                access_controller=access_group_controller,
            )
        )

    results = await asyncio.gather(*tasks)

    incomplete_results = [
        str(result.batch[0].input_root.existing_root.uid)
        for result in results
        if not (
            result.batch[0].final_root
            and result.batch[0].final_root.flow.impact_assessment
            and result.batch[0].final_root.flow.daily_food_unit
            and result.batch[0].final_graph_table
        )
    ]

    final_calculation_table = [
        list(result.batch[0].final_graph_table)
        for result in results
        if str(result.batch[0].input_root.existing_root.uid) not in incomplete_results
    ] + [root_product_with_cache.final_graph_table for root_product_with_cache in root_products_with_cache]

    # TODO: generate some kind of error-statistics also here?
    # report_output.error_statistics = ErrorStatisticsDto(
    #     number_of_successful_calculations=len(all_recipe_infos) + len(all_supply_infos) - len(incomplete_results),
    #     number_of_failed_calculations=len(incomplete_results),
    #     failed_node_uids=incomplete_results,
    # )

    report_output = CSVReportOutputDto(final_graphs=final_calculation_table)

    return report_output
