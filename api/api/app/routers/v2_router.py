import gc
import tracemalloc
from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, status
from starlette.requests import Request
from structlog import get_logger

from api.app.injector import get_node_controller
from api.app.settings import Settings
from api.controller.node_controller import NodeController
from core.service import calc_service

logger = get_logger()
v2_router = APIRouter()
settings = Settings()


mem_snapshot: Optional[tracemalloc.Snapshot] = None


@v2_router.get("/mem_snapshot")
async def get_mem_snapshot(request: Request, cumulative: bool = False) -> list[str]:
    if not request.state.is_superuser:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    if not tracemalloc.is_tracing():
        tracemalloc.start(3)

    global mem_snapshot
    mem_snapshot = take_mem_snapshot()
    top_stats = mem_snapshot.statistics("lineno", cumulative=cumulative)
    print("[ Top 20 ]")
    for stat in top_stats[:20]:
        print(stat)
    return [str(stat) for stat in top_stats[:20]]


@v2_router.get("/mem_diff")
async def get_mem_diff(request: Request, cumulative: bool = False) -> list[str]:
    if not request.state.is_superuser:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    if mem_snapshot is None:
        return ["No snapshot taken yet"]

    snapshot = take_mem_snapshot()
    top_stats = snapshot.compare_to(mem_snapshot, "lineno", cumulative=cumulative)
    print("[ Top 20 differences ]")
    for stat in top_stats[:20]:
        print(stat)
    return [str(stat) for stat in top_stats[:20]]


def take_mem_snapshot() -> tracemalloc.Snapshot:
    gc.collect()
    snapshot = tracemalloc.take_snapshot().filter_traces(
        [
            # tracemalloc.Filter(True, orchestrator.__file__),
            tracemalloc.Filter(True, calc_service.__file__, lineno=129),
        ]
    )
    return snapshot


@v2_router.get("/node_stats")
async def get_node_stats(
    request: Request,
    node_controller: NodeController = Depends(get_node_controller),  # noqa: B008
) -> int:
    if not request.state.is_superuser:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )

    cache_size = len(node_controller.node_service.cache)
    logger.info("cache_size", cache_size=cache_size)
    return cache_size
