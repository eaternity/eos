from typing import List, Optional

from fastapi import APIRouter, Depends, HTTPException, Request, Response, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from api.app.injector import get_access_controller
from api.controller.access_group_controller import AccessGroupController
from api.dto.v2.access_group_dto import (
    AccessGroupId,
    AccessGroupIdWithStatus,
    LegacyAPIResponseBehavior,
    WrappedAccessGroupDto,
    WrappedAccessGroupIdDto,
    WrappedEdgeAccessGroupDto,
    WrappedEdgeAccessGroupWithStatusDto,
    WrappedGetSubAccessGroupsInputDto,
    WrappedRootAccessGroupInfoDto,
    WrappedUpsertAccessGroupInputDto,
)
from api.dto.v2.user_dto import MemberDto, MemberWithStatusDto, WrappedMembersDto, WrappedMembersWithStatusDto
from core.domain.access_group_node import NodeAccessGroupTypeEnum
from core.service.permission_service import (
    check_if_can_add_members,
    check_if_can_create_access_group,
    check_if_can_delete_access_group,
    check_if_can_delete_members,
    check_if_can_give_permissions,
    check_if_can_read_node,
    check_if_can_view_members,
    check_if_is_group_admin,
)

logger = get_logger()

access_group_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@access_group_router.post("/upsert_access_group")
async def upsert_access_group(
    request: Request,
    response: Response,
    wrapped_access_group: WrappedUpsertAccessGroupInputDto,
    legacy_api_request: bool = False,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> WrappedAccessGroupDto:
    """Endpoint handler for upserting access groups.

    :param request: Starlette Request object
    :param response: Starlette Response object
    :param wrapped_access_group: Incoming DTO object that contains data about the access group to be upserted
    :param legacy_api_request: Optional query parameter for specifying whether this request is sent via the legacy API
    :return: Outgoing DTO object that contains data about the upserted access group
    """
    logger.info(f"called upsert_access_group with namespace: {wrapped_access_group.namespace_uid}")

    if not wrapped_access_group.parent_access_group:
        wrapped_access_group.parent_access_group = AccessGroupId()
    # if the target namespace is not specified in the request,
    # we take the token's default one
    if not wrapped_access_group.namespace_uid:
        # for legacy API requests, we automatically set a parent access group
        if legacy_api_request:
            logger.info("Legacy use case of create_new_access_group endpoint detected.")
            wrapped_access_group.parent_access_group.uid = request.state.user_legacy_default_access_group.uid

        wrapped_access_group.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    # if the user has no default access group associated and tries to post a group by XID,
    # we don't know what namespace this group should belong to,
    # so we raise an exception
    if not wrapped_access_group.namespace_uid and not wrapped_access_group.access_group.uid:
        raise HTTPException(
            status_code=status.HTTP_406_NOT_ACCEPTABLE,
            detail="You must either define target namespace in the request, "
            "or set a default access group to get namespace from.",
        )

    # since user's permission to create a child access group
    # has to be stored in another group,
    # we have to use `parent_access_group_xid` or `parent_access_group_uid` query parameter
    if wrapped_access_group.parent_access_group.xid:
        wrapped_access_group.parent_access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_access_group.parent_access_group.xid,
                wrapped_access_group.namespace_uid,
            )
        ).uid

    if wrapped_access_group.parent_access_group.uid:
        permissions = await access_controller.get_user_permissions_by_uid(
            request.state, wrapped_access_group.parent_access_group.uid
        )
        permission = check_if_can_create_access_group(permissions)

        # get the parent group's inherited configurations
        parent_access_group = await access_controller.get_access_group_by_uid(
            wrapped_access_group.parent_access_group.uid
        )
        parent_data = parent_access_group.data
        if parent_data:
            if not wrapped_access_group.access_group.ignore_fields:
                wrapped_access_group.access_group.ignore_fields = parent_data.get("ignore_fields")
            if not wrapped_access_group.access_group.legacyAPI_response_behavior:
                if parent_data.get("legacyAPI_response_behavior"):
                    wrapped_access_group.access_group.legacyAPI_response_behavior = LegacyAPIResponseBehavior(
                        **parent_data.get("legacyAPI_response_behavior")
                    )
    else:
        permission = True

    # a group can be added only by superusers
    # or users with a respective permission
    if permission:
        access_group_dto, is_updated = await access_controller.upsert_access_group(
            namespace_uid=wrapped_access_group.namespace_uid,
            creator_id=request.state.user,
            access_group_dto=wrapped_access_group.access_group,
        )

        # the second condition prevents a case for legacy API
        # where a user with the posted access group has it as their default access group
        # which would create a recursive edge
        if (
            wrapped_access_group.parent_access_group.uid
            and wrapped_access_group.parent_access_group.uid != access_group_dto.uid
        ):
            # create an edge
            await access_controller.upsert_access_group_edge(
                wrapped_access_group.parent_access_group.uid,
                access_group_dto.uid,
            )

        if is_updated:
            response.status_code = status.HTTP_200_OK
        else:
            response.status_code = status.HTTP_201_CREATED

        return WrappedAccessGroupDto(access_group=access_group_dto, namespace_uid=wrapped_access_group.namespace_uid)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@access_group_router.post("/get_access_group_by_id")
async def get_access_group_by_id(
    request: Request,
    wrapped_access_group: WrappedAccessGroupIdDto,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> WrappedAccessGroupDto:
    """Endpoint handler for getting an access group.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the access group to be obtained
    :return: Outgoing DTO object that contains data about the obtained access group
    """
    logger.info(f"called get_access_group_by_id with namespace: {wrapped_access_group.namespace_uid}")

    if not wrapped_access_group.namespace_uid:
        wrapped_access_group.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if wrapped_access_group.access_group.xid:
        wrapped_access_group.access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_access_group.access_group.xid, wrapped_access_group.namespace_uid
            )
        ).uid

    permissions = await access_controller.get_user_permissions_by_uid(
        request.state, wrapped_access_group.access_group.uid
    )
    has_read_permission = check_if_can_read_node(permissions)

    # in endpoints where permissions aren't checked,
    # we have to check user's superuser status explicitly
    if request.state.is_superuser or has_read_permission:
        access_group_dto, group_namespace_uid = await access_controller.get_access_group_dto_by_uid(
            wrapped_access_group.access_group.uid
        )

        return WrappedAccessGroupDto(access_group=access_group_dto, namespace_uid=group_namespace_uid)
    else:
        # TODO: keep this status code and message as they are?
        # instead of raising 401, we raise 404 in order to obfuscate the group's existence
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Not found.",
        )


@access_group_router.post("/get_sub_access_groups")
async def get_sub_access_groups(
    request: Request,
    wrapped_access_group: Optional[WrappedGetSubAccessGroupsInputDto] = None,
    legacy_api_request: bool = False,
    access_group_type: Optional[NodeAccessGroupTypeEnum] = None,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> List[AccessGroupId]:
    """Endpoint handler for getting all access groups in a namespace or below a parent access group.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the
        parent access group and / or the namespace
    :param legacy_api_request: Optional query parameter for specifying whether this request is sent via the legacy API
    :return: List of access group IDs
    """
    if wrapped_access_group is None:
        wrapped_access_group = WrappedGetSubAccessGroupsInputDto()
    if not wrapped_access_group.namespace_uid:
        wrapped_access_group.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    logger.info(f"called get_sub_access_groups with namespace: {wrapped_access_group.namespace_uid}")
    # for legacy API requests, we automatically set a parent access group
    if legacy_api_request:
        logger.info("Legacy use case of create_new_access_group endpoint detected.")
        wrapped_access_group.parent_access_group = AccessGroupId(uid=request.state.user_legacy_default_access_group.uid)

    if wrapped_access_group.parent_access_group is None:  # return all access groups in a namespace
        # in endpoints where permissions aren't checked,
        # we have to check user's superuser status explicitly
        # superusers get a list of all access groups in a namespace
        if request.state.is_superuser:
            return await access_controller.get_all_access_group_ids_by_namespace(wrapped_access_group.namespace_uid)
        # other users get a list of access groups they're in
        else:
            return await access_controller.get_all_group_memberships_by_user(request.state.user)
    else:  # return all sub-access-groups
        if wrapped_access_group.parent_access_group.xid:
            wrapped_access_group.parent_access_group.uid = (
                await access_controller.get_access_group_by_xid(
                    wrapped_access_group.parent_access_group.xid, wrapped_access_group.namespace_uid
                )
            ).uid

        # superusers get a list of all sub access groups of the access group
        if request.state.is_superuser:
            return await access_controller.get_all_sub_access_group_ids_by_uid(
                wrapped_access_group.parent_access_group.uid, wrapped_access_group.depth
            )
        # other users get a list of joined sub access groups of this access group
        # TODO: check for membership status of the parent group before fetching sub access groups?
        else:
            return await access_controller.get_all_sub_group_memberships_by_user_and_uid(
                wrapped_access_group.parent_access_group.uid,
                wrapped_access_group.depth,
                request.state.user,
                access_group_type,
            )


@access_group_router.post("/delete_access_group")
async def delete_access_group(
    request: Request,
    wrapped_access_group: WrappedAccessGroupIdDto,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> None:
    """Endpoint handler for deleting access groups.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the access group to be deleted
    """
    logger.info(f"called delete_access_group with namespace: {wrapped_access_group.namespace_uid}")

    if not wrapped_access_group.namespace_uid:
        wrapped_access_group.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if wrapped_access_group.access_group.xid:
        # get this access group's UUID and verify that it exists
        wrapped_access_group.access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_access_group.access_group.xid,
                wrapped_access_group.namespace_uid,
            )
        ).uid
    else:
        # verify that this access group exists
        await access_controller.get_access_group_by_uid(wrapped_access_group.access_group.uid)

    permissions = await access_controller.get_user_permissions_by_uid(
        request.state, wrapped_access_group.access_group.uid
    )

    if check_if_can_delete_access_group(permissions):
        await access_controller.delete_access_group_by_uid(wrapped_access_group.access_group.uid)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@access_group_router.post("/upsert_edges")
async def upsert_access_group_edge(
    wrapped_edge_access_group: WrappedEdgeAccessGroupDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> WrappedEdgeAccessGroupWithStatusDto:
    """Endpoint handler for upserting access group edges.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the access group edge to be upserted
    :return: Outgoing DTO object that contains data about the upserted access group edge
    """
    logger.info(f"called upsert_access_group_edge with namespace: {wrapped_edge_access_group.namespace_uid}")

    original_namespace_uid = wrapped_edge_access_group.namespace_uid
    if not wrapped_edge_access_group.namespace_uid:
        wrapped_edge_access_group.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if wrapped_edge_access_group.parent_access_group.xid:
        # get this access group's UUID and verify that it exists
        wrapped_edge_access_group.parent_access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_edge_access_group.parent_access_group.xid,
                wrapped_edge_access_group.namespace_uid,
            )
        ).uid

    # here we skip verifying this access group's existence by UUID
    # since it will be done in `add_child_access_group_by_group_uid()` call below
    parent_group_permissions = await access_controller.get_user_permissions_by_uid(
        request.state, wrapped_edge_access_group.parent_access_group.uid
    )

    if check_if_is_group_admin(parent_group_permissions):
        access_group_id_with_status_list: List[AccessGroupIdWithStatus] = []
        for child_access_group in wrapped_edge_access_group.child_access_group_list:
            if child_access_group.uid:
                child_access_group_node = await access_controller.get_access_group_by_uid(child_access_group.uid)
            else:
                child_access_group_node = await access_controller.get_access_group_by_xid(
                    child_access_group.xid,
                    wrapped_edge_access_group.namespace_uid,
                )

            child_group_permissions = await access_controller.get_user_permissions_by_uid(
                request.state, child_access_group_node.uid
            )

            if check_if_can_give_permissions(child_group_permissions):
                await access_controller.upsert_access_group_edge(
                    wrapped_edge_access_group.parent_access_group.uid,
                    child_access_group_node.uid,
                )
                access_group_id_with_status = AccessGroupIdWithStatus(
                    xid=child_access_group.xid,
                    uid=child_access_group_node.uid,
                    statuscode=status.HTTP_200_OK,
                    message="",
                )
            else:
                access_group_id_with_status = AccessGroupIdWithStatus(
                    xid=child_access_group.xid,
                    uid=child_access_group_node.uid,
                    statuscode=status.HTTP_401_UNAUTHORIZED,
                    message="You don't have rights for this action.",
                )
            access_group_id_with_status_list.append(access_group_id_with_status)
        return WrappedEdgeAccessGroupWithStatusDto(
            parent_access_group=wrapped_edge_access_group.parent_access_group,
            child_access_group_list_with_status=access_group_id_with_status_list,
            namespace_uid=original_namespace_uid,
        )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


# don't know yet the input types
@access_group_router.post("/get_edges")
async def get_access_group_edges(
    wrapped_edge_access_group: WrappedEdgeAccessGroupDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> WrappedEdgeAccessGroupWithStatusDto:
    """Endpoint handler for getting access group edges.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the access group edge to be obtained
    :return: Outgoing DTO object that contains data about the obtained access group edge
    """
    logger.info(f"called get_access_group_edges with namespace: {wrapped_edge_access_group.namespace_uid}")

    output_namespace_uid = wrapped_edge_access_group.namespace_uid
    if not wrapped_edge_access_group.namespace_uid:
        wrapped_edge_access_group.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if wrapped_edge_access_group.parent_access_group.xid:
        # get this access group's UUID and verify that it exists
        wrapped_edge_access_group.parent_access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_edge_access_group.parent_access_group.xid,
                wrapped_edge_access_group.namespace_uid,
            )
        ).uid
    is_user_a_parent_group_member = await access_controller.check_if_user_is_group_member_by_group_uid(
        wrapped_edge_access_group.parent_access_group.uid,
        request.state.user,
    )
    access_group_id_with_status_list = []
    for child_access_group in wrapped_edge_access_group.child_access_group_list:
        if child_access_group.xid:
            # get this access group's UUID and verify that it exists
            child_access_group.uid = (
                await access_controller.get_access_group_by_xid(
                    child_access_group.xid,
                    wrapped_edge_access_group.namespace_uid,
                )
            ).uid
        is_user_a_child_group_member = await access_controller.check_if_user_is_group_member_by_group_uid(
            child_access_group.uid,
            request.state.user,
        )
        if request.state.is_superuser or (is_user_a_parent_group_member or is_user_a_child_group_member):
            edge_exists = await access_controller.does_access_group_edge_exist(
                child_access_group.uid,
                wrapped_edge_access_group.parent_access_group.uid,
            )
            if edge_exists:
                access_group_id_with_status = AccessGroupIdWithStatus(
                    xid=child_access_group.xid,
                    uid=child_access_group.uid,
                    statuscode=status.HTTP_200_OK,
                    message="",
                )
            else:
                access_group_id_with_status = AccessGroupIdWithStatus(
                    xid=child_access_group.xid,
                    uid=child_access_group.uid,
                    statuscode=status.HTTP_404_NOT_FOUND,
                    message="Edge was not found.",
                )
        else:
            access_group_id_with_status = AccessGroupIdWithStatus(
                xid=child_access_group.xid,
                uid=child_access_group.uid,
                statuscode=status.HTTP_401_UNAUTHORIZED,
                message="You don't have rights for this action.",
            )
        access_group_id_with_status_list.append(access_group_id_with_status)
    return WrappedEdgeAccessGroupWithStatusDto(
        parent_access_group=wrapped_edge_access_group.parent_access_group,
        child_access_group_list_with_status=access_group_id_with_status_list,
        namespace_uid=output_namespace_uid,
    )


@access_group_router.post("/delete_edges")
async def delete_access_group_edges(
    wrapped_edge_access_group: WrappedEdgeAccessGroupDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> WrappedEdgeAccessGroupWithStatusDto:
    """Endpoint handler for deleting access group edges.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the access group edge to be deleted
    :return: Outgoing DTO object that contains data about the deleted access group edges
    """
    logger.info(f"called delete_access_group_edges with namespace: {wrapped_edge_access_group.namespace_uid}")

    if not wrapped_edge_access_group.namespace_uid:
        wrapped_edge_access_group.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    if wrapped_edge_access_group.parent_access_group.xid:
        # get this access group's UUID and verify that it exists
        wrapped_edge_access_group.parent_access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_edge_access_group.parent_access_group.xid,
                wrapped_edge_access_group.namespace_uid,
            )
        ).uid

    access_group_id_with_status_list = []
    parent_group_permissions = await access_controller.get_user_permissions_by_uid(
        request.state, wrapped_edge_access_group.parent_access_group.uid
    )
    for child_access_group in wrapped_edge_access_group.child_access_group_list:
        if child_access_group.xid:
            # get this access group's UUID and verify that it exists
            child_access_group.uid = (
                await access_controller.get_access_group_by_xid(
                    child_access_group.xid,
                    wrapped_edge_access_group.namespace_uid,
                )
            ).uid

        child_group_permissions = await access_controller.get_user_permissions_by_uid(
            request.state, child_access_group.uid
        )

        if check_if_is_group_admin(parent_group_permissions) and check_if_can_give_permissions(child_group_permissions):
            access_group_edge_deletion_status = await access_controller.delete_access_group_edge(
                child_access_group.uid,
                wrapped_edge_access_group.parent_access_group.uid,
            )
            if access_group_edge_deletion_status:
                access_group_id_with_status = AccessGroupIdWithStatus(
                    xid=child_access_group.xid,
                    uid=child_access_group.uid,
                    statuscode=status.HTTP_200_OK,
                    message="",
                )
            else:
                access_group_id_with_status = AccessGroupIdWithStatus(
                    xid=child_access_group.xid,
                    uid=child_access_group.uid,
                    statuscode=status.HTTP_404_NOT_FOUND,
                    message="Edge was not found.",
                )
        else:
            access_group_id_with_status = AccessGroupIdWithStatus(
                xid=child_access_group.xid,
                uid=child_access_group.uid,
                statuscode=status.HTTP_401_UNAUTHORIZED,
                message="You don't have rights for this action.",
            )
        access_group_id_with_status_list.append(access_group_id_with_status)
    return WrappedEdgeAccessGroupWithStatusDto(
        parent_access_group=wrapped_edge_access_group.parent_access_group,
        child_access_group_list_with_status=access_group_id_with_status_list,
        namespace_uid=wrapped_edge_access_group.namespace_uid,
    )


# TODO: rework update_permissions
@access_group_router.post("/upsert_members")
async def upsert_members_to_access_group(
    wrapped_members_dto: WrappedMembersDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> WrappedMembersWithStatusDto:
    """Endpoint handler for upserting members.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the members to be upserted
    :return: Outgoing DTO object that contains data about the upserted members
    """
    logger.info(f"called upsert_members_to_access_group with namespace: {wrapped_members_dto.namespace_uid}")

    original_namespace_uid = wrapped_members_dto.namespace_uid
    if not wrapped_members_dto.namespace_uid:
        wrapped_members_dto.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    logger.info(f"called add_members_to_the_group with namespace: {wrapped_members_dto.namespace_uid}")

    if wrapped_members_dto.access_group.xid:
        # get this access group's UUID and verify that it exists
        wrapped_members_dto.access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_members_dto.access_group.xid,
                wrapped_members_dto.namespace_uid,
            )
        ).uid
    else:
        # verify that this access group exists
        await access_controller.get_access_group_by_uid(wrapped_members_dto.access_group.uid)

    permissions = await access_controller.get_user_permissions_by_uid(
        request.state, wrapped_members_dto.access_group.uid
    )

    # currently we don't we add if the user is not present and else update the permissions
    has_permission = check_if_can_add_members(permissions) and check_if_can_add_members(permissions)

    # we allow adding users if they have a specific permission
    # that is granted to an admin once they create this group
    if has_permission:
        members_with_status: List[MemberWithStatusDto] = []
        for member in wrapped_members_dto.members:
            await access_controller.add_member_to_the_group_by_group_uid(wrapped_members_dto.access_group.uid, member)
            members_with_status.append(
                MemberWithStatusDto(
                    user_id=member.user_id, permissions=member.permissions, statuscode=status.HTTP_200_OK, message=""
                )
            )
        return WrappedMembersWithStatusDto(
            access_group=wrapped_members_dto.access_group,
            members_with_status=members_with_status,
            namespace_uid=original_namespace_uid,
        )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@access_group_router.post("/get_members")
async def get_members_of_access_group(
    request: Request,
    wrapped_members_dto: WrappedAccessGroupIdDto,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> list[MemberDto]:
    """Endpoint handler for getting members.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the members to be obtained
    :return: Outgoing DTO object that contains data about the obtained members
    """
    logger.info(f"called get_members_of_access_group with namespace: {wrapped_members_dto.namespace_uid}")

    if not wrapped_members_dto.namespace_uid:
        wrapped_members_dto.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    logger.info(f"called get_group_members with namespace: {wrapped_members_dto.namespace_uid}")

    if wrapped_members_dto.access_group.xid:
        # get this access group's UUID and verify that it exists
        wrapped_members_dto.access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_members_dto.access_group.xid, wrapped_members_dto.namespace_uid
            )
        ).uid
    else:
        # verify that this access group exists
        await access_controller.get_access_group_by_uid(wrapped_members_dto.access_group.uid)

    permissions = await access_controller.get_user_permissions_by_uid(
        request.state, wrapped_members_dto.access_group.uid
    )

    if check_if_can_view_members(permissions):
        group_members = await access_controller.get_group_members_by_group_uid(wrapped_members_dto.access_group.uid)
        return group_members
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have rights for this action.",
        )


@access_group_router.post("/delete_members")
async def delete_member_from_access_group(
    request: Request,
    wrapped_members_dto: WrappedMembersDto,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> WrappedMembersWithStatusDto:
    """Endpoint handler for deleting members.

    :param request: Starlette Request object
    :param wrapped_access_group: Incoming DTO object that contains data about the members to be deleted
    :return: Outgoing DTO object that contains data about the deleted members
    """
    logger.info(f"called delete_member_from_access_group with namespace: {wrapped_members_dto.namespace_uid}")

    if not wrapped_members_dto.namespace_uid:
        wrapped_members_dto.namespace_uid = request.state.user_legacy_default_access_group.namespace_uid

    # logger.info(f"called delete_member_from_the_group with namespace: {wrapped_members_dto.namespace_uid}")

    if wrapped_members_dto.access_group.xid:
        # get this access group's UUID and verify that it exists
        wrapped_members_dto.access_group.uid = (
            await access_controller.get_access_group_by_xid(
                wrapped_members_dto.access_group.xid, wrapped_members_dto.namespace_uid
            )
        ).uid
    else:
        # verify that this access group exists
        await access_controller.get_access_group_by_uid(wrapped_members_dto.access_group.uid)

    permissions = await access_controller.get_user_permissions_by_uid(
        request.state, wrapped_members_dto.access_group.uid
    )
    members_with_status: List[MemberWithStatusDto] = []
    for member in wrapped_members_dto.members:
        if check_if_can_delete_members(permissions):
            await access_controller.delete_member_from_the_group_by_uid(
                wrapped_members_dto.access_group.uid, member.user_id
            )
            members_with_status.append(
                MemberWithStatusDto(
                    user_id=member.user_id,
                    permissions=member.permissions,
                    statuscode=status.HTTP_200_OK,
                    message="",
                )
            )
        else:
            members_with_status.append(
                MemberWithStatusDto(
                    user_id=member.user_id,
                    permissions=member.permissions,
                    statuscode=status.HTTP_401_UNAUTHORIZED,
                    message="You don't have rights for this action.",
                )
            )
    return WrappedMembersWithStatusDto(
        access_group=wrapped_members_dto.access_group,
        members_with_status=members_with_status,
        namespace_uid=wrapped_members_dto.namespace_uid,
    )


@access_group_router.get("/all_roots")
async def get_all_root_access_group_info(
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    access_controller: AccessGroupController = Depends(get_access_controller),
) -> WrappedRootAccessGroupInfoDto:
    if request.state.is_superuser:
        root_access_groups = await access_controller.get_all_root_access_group_info()
        return WrappedRootAccessGroupInfoDto(access_groups=root_access_groups)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Endpoint only allowed to superusers.",
        )
