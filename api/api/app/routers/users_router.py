from fastapi import APIRouter, Depends, HTTPException, Request, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from api.app.injector import get_admin_controller
from api.app.settings import Settings
from api.controller.admin_controller import AdminController
from api.dto.v2.user_dto import UserCreateStatusDto, UserDto, WrappedUserCreateDto

logger = get_logger()
settings = Settings()
users_router = APIRouter()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@users_router.put("/{user_id}/")
async def create_user(
    user: WrappedUserCreateDto,
    user_id: str,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    admin_controller: AdminController = Depends(get_admin_controller),
) -> UserCreateStatusDto:
    if not request.state.is_superuser:
        if str(request.state.user) != user_id:
            logger.warn(
                "A normal user tried to modify another user_id. Returning HTTP_401_UNAUTHORIZED.",
                request_user=str(request.state.user),
                target_user=user_id,
            )
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="You don't have rights for this action.",
            )
        if user.user.is_superuser:
            logger.warn(
                "A normal user tried to set the superuser flag. Returning HTTP_401_UNAUTHORIZED.",
                request_user=str(request.state.user),
            )
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="You don't have rights for this action.",
            )

    logger.info(f"called create_user with default access group: {user.legacy_api_default_access_group_uid}")

    customer_create_status_dto = await admin_controller.put_user(
        request.state.is_superuser,
        user.legacy_api_default_access_group_uid,
        user_id,
        user.user,
    )

    return customer_create_status_dto


@users_router.get("/{user_id}/")
async def get_user(
    user_id: str,
    credentials: APIKeyHeader = Depends(api_key_header),
    admin_controller: AdminController = Depends(get_admin_controller),
) -> UserDto:
    return await admin_controller.get_user(user_id)
