from fastapi import APIRouter, Depends, HTTPException, Request, status
from fastapi.security import APIKeyHeader
from structlog import get_logger

from api.app.injector import get_auth_controller
from api.app.settings import Settings
from api.controller.auth_controller import AuthController
from core.domain.token import TokenDto, TokenListingOutputDto, TokenOutputDto

auth_router = APIRouter()
logger = get_logger()
settings = Settings()

api_key_header = APIKeyHeader(name="Authorization", auto_error=True)


@auth_router.get("/validate_token/")
async def validate_token(
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    auth_controller: AuthController = Depends(get_auth_controller),
):
    logger.info(f"called validate_token with token: {request.state.token}")

    if await auth_controller.validate_token(request.state.token):
        return {"status": "authenticated"}
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect API Key.",
            headers={"WWW-Authenticate": "Basic"},
        )


@auth_router.post("/upsert_token/")
async def upsert_token(
    token: TokenDto,
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    auth_controller: AuthController = Depends(get_auth_controller),
) -> TokenOutputDto:
    logger.info(f"called upsert_token with token: {request.state.token}")

    return await auth_controller.upsert_token(request.state.user, token.name)


@auth_router.get("/list_tokens/")
async def list_user_tokens(
    request: Request,
    credentials: APIKeyHeader = Depends(api_key_header),
    auth_controller: AuthController = Depends(get_auth_controller),
) -> TokenListingOutputDto:
    logger.info(f"called list_user_tokens with token: {request.state.token}")

    return await auth_controller.list_user_tokens(request.state.user)
