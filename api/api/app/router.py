from fastapi import APIRouter
from structlog import get_logger

from api.app.routers.access_group_router import access_group_router
from api.app.routers.auth_router import auth_router
from api.app.routers.batch_router import batch_router
from api.app.routers.calc_router import calc_router
from api.app.routers.glossary_link_router import glossary_link_router
from api.app.routers.glossary_router import glossary_router
from api.app.routers.ingredients_declaration_router import ingredients_declaration_mapping_router
from api.app.routers.matching_router import matching_router
from api.app.routers.node_router import node_router
from api.app.routers.users_router import users_router
from api.app.routers.v2_router import v2_router
from api.app.settings import Settings

settings = Settings()
logger = get_logger()
router = APIRouter(prefix=settings.BASE_PATH)

router.include_router(access_group_router, prefix="/access-groups")
router.include_router(auth_router, prefix="/auth")
router.include_router(batch_router, prefix="/batch")
router.include_router(calc_router, prefix="/calculation")
router.include_router(glossary_router, prefix="/glossary")
router.include_router(glossary_link_router, prefix="/glossary-link")
router.include_router(ingredients_declaration_mapping_router, prefix="/ingredients-declaration-mappings")
router.include_router(matching_router, prefix="/matching")
router.include_router(node_router, prefix="/nodes")
router.include_router(users_router, prefix="/users")
router.include_router(v2_router, prefix="/admin")
