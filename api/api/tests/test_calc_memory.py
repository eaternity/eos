"Testing calculation for memory leaks."
import asyncio
import gc
import logging
import os
import tracemalloc
from typing import Generator

import pytest
from structlog import get_logger

from api.app.settings import Settings
from core.domain.calculation import Calculation
from core.service.service_provider import ServiceLocator

from .conftest import FastAPIReturn
from .test_api_for_recipes import TEST_RECIPE
from .test_calc_router import async_client

logger = get_logger()
settings = Settings()
service_locator = ServiceLocator()


def get_calc_objects() -> Generator[Calculation, None, None]:
    for obj in gc.get_objects():
        if hasattr(obj, "__class__") and obj.__class__ == Calculation:
            yield obj
        else:
            continue


class NoOpHandler(logging.Handler):
    def emit(self, record: logging.LogRecord) -> None:
        pass


# Function to replace handlers with NoOpHandler
def replace_handlers_with_noop() -> list[logging.Handler]:
    noop_handler = NoOpHandler()
    root_logger = logging.getLogger()
    original_handlers = root_logger.handlers[:]
    root_logger.handlers = [noop_handler]

    for _logger_name, logger in logging.Logger.manager.loggerDict.items():
        if isinstance(logger, logging.Logger):
            logger.handlers = [noop_handler]

    return original_handlers


# Function to restore original handlers
def restore_handlers(original_handlers: list) -> None:
    root_logger = logging.getLogger()
    root_logger.handlers = original_handlers

    for _logger_name, logger in logging.Logger.manager.loggerDict.items():
        if isinstance(logger, logging.Logger):
            logger.handlers = []


@pytest.mark.asyncio
async def test_calculation_memory(app: FastAPIReturn) -> None:
    """Test calculation endpoint for a simple recipe."""
    # If this test fails, it might help to turn of the coverage in pytest.ini. To do this comment this:
    # addopts =
    # --cov
    # --cov-report=
    # --cov-config ./ci/.coveragerc
    tracemalloc.start(1)

    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        await ac.post(
            "/v2/calculation/graphs",
            json={"batch": TEST_RECIPE},
        )
        # only compare from the second calculation onwards since the first one might have
        # some different execution-branches (e.g. upsert vs insert)
        await ac.post(
            "/v2/calculation/graphs",
            json={"batch": TEST_RECIPE},
        )

        original_handlers = replace_handlers_with_noop()
        try:
            logger.info("Logging is disabled for this section")
            if service_locator.service_provider.matching_service.background_tasks:
                done, _ = await asyncio.wait(
                    service_locator.service_provider.matching_service.background_tasks,
                    return_when=asyncio.ALL_COMPLETED,
                )
            gc.collect()
            snapshot1 = tracemalloc.take_snapshot()
            await ac.post(
                "/v2/calculation/graphs",
                json={"batch": TEST_RECIPE},
            )
            if service_locator.service_provider.matching_service.background_tasks:
                done, _ = await asyncio.wait(
                    service_locator.service_provider.matching_service.background_tasks,
                    return_when=asyncio.ALL_COMPLETED,
                )
            gc.collect()
            snapshot2 = tracemalloc.take_snapshot()
            tracemalloc.stop()
        finally:
            # Restore original handlers
            restore_handlers(original_handlers)
            logger.info("logging is restored")

        # compare the two snapshots (before and after the new calculation):
        stats = snapshot2.compare_to(snapshot1, "lineno")

        def filter_stats(stats: list, base_dir: str) -> list:
            filtered_stats = []
            for stat in stats:
                file_path = stat.traceback[0].filename
                # only include if the file is in the codebase directory
                if base_dir in file_path:
                    # exclude this test file:
                    if __file__ in file_path:
                        continue
                    # only include certain folders to prevent false positives due to
                    # virtual-environments or similar installed in the base folder:
                    name_after_base = file_path.split(base_dir)[1]
                    first_dir = name_after_base.strip(os.sep).split(os.sep)[0]
                    subdirectories_to_include = ["api", "core", "database"]
                    if not any(first_dir == folder for folder in subdirectories_to_include):
                        continue
                    # only include if the count changed:
                    if stat.count_diff > 0:
                        filtered_stats.append(stat)
            return filtered_stats

        base_dir = __file__.rsplit("/", 4)[0]
        stats = filter_stats(stats, base_dir)

        if len(stats) > 0:
            print("\nMemory leak detected in the following files:")
            for stat in stats:
                print(stat)

        assert len(stats) == 0

        # in case there is a memory leak, you can compare traceback to debug:
        # diff = snapshot2.compare_to(snapshot1, "traceback")
        # for entry in diff:
        #     if "calc_service.py" in entry.traceback[-1].filename:
        #         print("\nEntry: {}".format(entry))
        #         print("Traceback:")
        #         for line in entry.traceback:
        #             print("  {}".format(line))


@pytest.mark.asyncio
async def test_calculation_objgraph(app: FastAPIReturn) -> None:
    """Test calculation endpoint for a simple recipe."""
    async with async_client(app) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        await ac.post(
            "/v2/calculation/graphs",
            json={"batch": TEST_RECIPE},
        )
        gc.collect()

        all_calc = [obj for obj in get_calc_objects()]
        assert len(all_calc) == 0

        # in case there is a memory leak, uncomment the following lines to debug
        # import objgraph
        # objgraph.show_backrefs(all_calc, max_depth=10)
        # or to show specific chain of object references from NodeCache to the calculation:
        # objgraph.show_chain(objgraph.find_backref_chain(all_calc[0], predicate=lambda x: isinstance(x, NodeCache)))
