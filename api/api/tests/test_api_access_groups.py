# ruff: noqa: ARG001 fixtures are unused in function body, is ok
# ruff: noqa: F811 fixtures imported are mis-indentified as redefinitions
import base64
import copy
import uuid
from typing import Tuple

import pytest
import pytest_asyncio
from fastapi import FastAPI, Response, status
from httpx import AsyncClient
from structlog import get_logger

from api.app.settings import Settings
from api.tests.test_api_for_products import create_delete_and_get_product_by_admin  # noqa: F401
from api.tests.test_api_for_recipes import (  # noqa: F401
    create_delete_and_get_recipe_by_admin,
    create_delete_and_get_subrecipe_by_admin,
)
from api.tests.test_api_for_supplies import create_delete_and_get_supply_by_admin  # noqa: F401
from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode, SupplySheetActivityNode
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator

from .conftest import (
    CUSTOMER,
    TEST_KITCHEN_BY_ADMIN,
    TEST_KITCHEN_BY_BASIC_USER,
    TEST_KITCHEN_BY_SUPERUSER,
    USER_ADMIN,
    USER_ADMIN_ID,
    USER_BASIC,
    USER_BASIC_ID,
    USER_SUPER,
    USER_SUPER_ID,
)

logger = get_logger()

service_locator = ServiceLocator()


def node_resp_to_dict_by_type(nodes: list) -> dict[str, set[str]]:
    node_dict: dict[str, set[str]] = {}
    for node in nodes:
        node_type = node["node_type"]
        node_dict.setdefault(node_type, set()).add(node["uid"])
    return node_dict


@pytest.mark.asyncio
async def test_create_customer_namespace(create_customer_namespace: str) -> str:
    return create_customer_namespace


@pytest.mark.asyncio
async def test_get_customer_namespace(get_customer_namespace: None) -> None:
    return get_customer_namespace


@pytest.mark.asyncio
async def test_create_users(create_users: None) -> None:
    return create_users


@pytest.mark.asyncio
async def test_create_users_permission_checks(app: FastAPI, create_users: Tuple[str, str, str]) -> None:
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users

        # test that a user can't change details of another user
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "user": USER_BASIC,
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        # test that a user can't give themselves a superuser status
        user_basic_with_superuser_status = copy.deepcopy(USER_BASIC)
        user_basic_with_superuser_status["is_superuser"] = True

        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": user_basic_with_superuser_status,
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}


@pytest.mark.asyncio
async def test_get_user(app: FastAPI, create_users: Tuple[str, str, str]) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, _, encoded_basic_user_token = create_users

        response = await ac.get(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "user_id": USER_SUPER_ID,
            "email": USER_SUPER.get("email"),
            "is_superuser": True,
            "is_service_account": False,
            "legacy_default_access_group_id": None,
        }

        response = await ac.get(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "user_id": USER_ADMIN_ID,
            "email": USER_ADMIN.get("email"),
            "is_superuser": False,
            "is_service_account": False,
            "legacy_default_access_group_id": None,
        }

        response = await ac.get(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "user_id": USER_BASIC_ID,
            "email": USER_BASIC.get("email"),
            "is_superuser": False,
            "is_service_account": False,
            "legacy_default_access_group_id": None,
        }

        fake_user_id = str(uuid.uuid4())

        response = await ac.get(
            f"/v2/users/{fake_user_id}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": f"User {fake_user_id} does not exist."}


@pytest.mark.asyncio
async def test_user_auth_tokens_validation(app: FastAPI, create_users: Tuple[str, str, str]) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, encoded_basic_user_token = create_users

        response = await ac.get(
            "/v2/auth/validate_token/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {"status": "authenticated"}

        response = await ac.get(
            "/v2/auth/validate_token/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {"status": "authenticated"}

        response = await ac.get(
            "/v2/auth/validate_token/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {"status": "authenticated"}


@pytest.mark.asyncio
async def test_user_auth_tokens_update_and_listing(app: FastAPI, create_users: Tuple[str, str, str]) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, _ = create_users

        # adding a new token for superuser
        new_token_name = "another token"

        response = await ac.post(
            "/v2/auth/upsert_token/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"name": new_token_name},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json().get("name") == new_token_name
        new_token_value = response.json().get("token")

        # updating superuser's default token
        response = await ac.post(
            "/v2/auth/upsert_token/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"name": "Default"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json().get("name") == "Default"
        super_user_token = response.json().get("token")
        encoded_super_user_token = base64.b64encode(super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.get(
            "/v2/auth/validate_token/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {"status": "authenticated"}

        # testing if listing of both tokens is correct
        response = await ac.get(
            "/v2/auth/list_tokens/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "tokens": [
                {
                    "name": new_token_name,
                    "token": new_token_value,
                },
                {
                    "name": "Default",
                    "token": super_user_token,
                },
            ],
        }


@pytest.mark.asyncio
async def test_create_and_get_and_delete_and_create_kitchen_by_superuser(
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
) -> None:
    return


@pytest.mark.asyncio
async def test_create_and_get_and_delete_and_create_kitchen_by_admin(
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
) -> None:
    return


@pytest.mark.asyncio
async def test_group_membership_for_basic_user(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[str, str, str],
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, _ = create_users

        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        # add a regular user to admin's group
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            },
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "namespace_uid": CUSTOMER.get("uid"),
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid"), "uid": test_kitchen_by_admin_uid},
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # checking if we can get admin's access group memberlist
        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group memberlist hasn't been received."
        assert {
            "user_id": USER_BASIC_ID,
            "permissions": {
                UserPermissionsEnum.read: True,
            },
        } in response.json()

        # test updating regular user's permissions in admin's group
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                    UserPermissionsEnum.delete: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "namespace_uid": CUSTOMER.get("uid"),
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid"), "uid": test_kitchen_by_admin_uid},
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # checking if we can get admin's access group memberlist
        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group memberlist hasn't been received."
        assert {
            "user_id": USER_BASIC_ID,
            "permissions": {
                UserPermissionsEnum.read: True,
                UserPermissionsEnum.delete: True,
            },
        } in response.json()

        # test updating (in this case removing) regular user's permissions in admin's group
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "namespace_uid": CUSTOMER.get("uid"),
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid"), "uid": test_kitchen_by_admin_uid},
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # checking if we can get admin's access group memberlist
        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group memberlist hasn't been received."
        assert {
            "user_id": USER_BASIC_ID,
            "permissions": {
                UserPermissionsEnum.read: True,
            },
        } in response.json()

        # removing basic user from admin's access group via UUID
        response = await ac.post(
            "/v2/access-groups/delete_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"uid": test_kitchen_by_admin_uid},
                "namespace_uid": CUSTOMER.get("uid"),
                "members": [{"user_id": USER_BASIC_ID}],
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Basic user hasn't been removed from admin's access group."

        # removing basic user from admin's access group via UUID and namespace
        response = await ac.post(
            "/v2/access-groups/delete_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"uid": test_kitchen_by_admin_uid},
                "members": [{"user_id": USER_BASIC_ID}],
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Basic user hasn't been removed from admin's access group."

        # add a regular user to admin's group
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "namespace_uid": CUSTOMER.get("uid"),
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid"), "uid": test_kitchen_by_admin_uid},
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # checking if we can get admin's access group memberlist
        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group memberlist hasn't been received."
        assert {
            "user_id": USER_BASIC_ID,
            "permissions": {
                UserPermissionsEnum.read: True,
            },
        } in response.json()


@pytest_asyncio.fixture(scope="function")
async def group_inaccessibility_for_basic_user(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[str, str, str],
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        # add a regular user to admin's group
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "namespace_uid": CUSTOMER.get("uid"),
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid"), "uid": test_kitchen_by_admin_uid},
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # checking if we can get admin's access group memberlist
        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group memberlist hasn't been received."
        assert {
            "user_id": USER_BASIC_ID,
            "permissions": {
                UserPermissionsEnum.read: True,
            },
        } in response.json()

        # test that a basic user cannot add a new separate group without specifying namespace yet
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"access_group": TEST_KITCHEN_BY_BASIC_USER},
        )
        assert response.status_code == status.HTTP_406_NOT_ACCEPTABLE

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_BASIC_USER,
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_406_NOT_ACCEPTABLE

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_admin_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_admin_uid
        assert response.json().get("status") is True

        # test that a basic user can add a new separate group
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"access_group": TEST_KITCHEN_BY_BASIC_USER},
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_BASIC_USER_WITH_UID = copy.deepcopy(TEST_KITCHEN_BY_BASIC_USER)
        TEST_KITCHEN_BY_BASIC_USER_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_BASIC_USER_WITH_UID,
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # testing if we can get basic user's access group
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
                },
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Basic user's access group hasn't been received."
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_BASIC_USER.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_BASIC_USER.get("location")

        # test that a basic user can't access other group's info
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                },
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": "Not found."}

        return TEST_KITCHEN_BY_BASIC_USER_WITH_UID.get("uid")


@pytest.mark.asyncio
async def test_group_inaccessibility_for_basic_user(group_inaccessibility_for_basic_user: None) -> None:
    return


@pytest.mark.asyncio
async def test_permission_denials_for_basic_user(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[str, str, str],
    # this fixture is required since this test uses a few states affected by it
    group_inaccessibility_for_basic_user: None,
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, _, encoded_basic_user_token = create_users

        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_BASIC_USER,
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            "Admin's access group has been updated by a basic user, " "while it shouldn't happen."
        )

        response = await ac.post(
            "/v2/access-groups/delete_access_group",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"access_group": {"uid": test_kitchen_by_admin_uid}},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            "Admin's access group has been deleted by a basic user, " "while it shouldn't happen."
        )

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "members": [
                    {
                        "user_id": USER_BASIC_ID,
                        "permissions": {
                            UserPermissionsEnum.read: True,
                            UserPermissionsEnum.give_permissions_in_access_group: True,
                        },
                    }
                ],
                "access_group": {"uid": test_kitchen_by_admin_uid},
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            "Basic user has updated their permissions in admin's access group, " "while it shouldn't happen."
        )

        response = await ac.post(
            "/v2/access-groups/delete_members",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                "members": [{"user_id": USER_ADMIN_ID}],
            },
        )
        assert response.json()["members_with_status"][0]["statuscode"] == 401, (
            "Admin has been removed from superuser's access group by a basic user, " "while it shouldn't happen."
        )

        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "access_group": {"uid": test_kitchen_by_superuser_uid},
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            "Superuser's access group memberlist has been received by a basic user, " "while it shouldn't happen."
        )


@pytest.mark.asyncio
async def test_get_all_access_groups_and_get_access_group_by_uid(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_customer_namespace: str,
    create_users: Tuple[str, str, str],
    group_inaccessibility_for_basic_user: None,
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_customer_token = create_customer_namespace
        encoded_super_user_token, _, _ = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin
        test_kitchen_by_basic_user_uid = group_inaccessibility_for_basic_user

        # testing if we can get namespace default access group service user's access group
        # via hitting GET request to `get_all_access_groups()` endpoint
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_customer_token}"},
        )
        assert response.json()[0].get("uid") is not None
        assert response.json()[0].get("xid") is not None
        default_group_uid = response.json()[0].get("uid")
        default_group_xid = response.json()[0].get("xid")

        # test that we've got correct XID and UUID values
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_customer_token}"},
            json={
                "access_group": {
                    "uid": default_group_uid,
                },
            },
        )
        assert response.json() is not None
        assert response.json().get("access_group", {}).get("xid") is not None
        assert response.json().get("access_group", {}).get("uid") is not None

        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_customer_token}"},
            json={
                "access_group": {
                    "xid": default_group_xid,
                },
            },
        )
        assert response.json() is not None
        assert response.json().get("access_group", {}).get("xid") is not None
        assert response.json().get("access_group", {}).get("uid") is not None

        all_groups = [
            {
                "uid": default_group_uid,
                "xid": default_group_xid,
            },
            {
                "uid": test_kitchen_by_superuser_uid,
                "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            {
                "uid": test_kitchen_by_admin_uid,
                "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
            },
            {
                "uid": test_kitchen_by_basic_user_uid,
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            },
        ]

        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == all_groups, "Superuser didn't receive a list of all access groups of this namespace."


@pytest_asyncio.fixture(scope="function")
async def post_access_group_edge(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[str, str, str],
    group_inaccessibility_for_basic_user: None,
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin
        test_kitchen_by_basic_user_uid = group_inaccessibility_for_basic_user

        groups_to_mark_as_child = [
            {
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            },
        ]

        # test that basic user is unable to do this action
        response = await ac.post(
            "/v2/access-groups/upsert_edges",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "child_access_group_list": groups_to_mark_as_child,
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.post(
            "/v2/access-groups/upsert_edges",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "child_access_group_list": groups_to_mark_as_child,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        # giving admin necessary permissions for posting this edge
        members = [
            {
                "user_id": USER_ADMIN_ID,
                "permissions": {
                    UserPermissionsEnum.give_permissions_in_access_group: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "members": members,
                "namespace_uid": CUSTOMER.get("uid"),
                "access_group": {"xid": TEST_KITCHEN_BY_BASIC_USER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"), "uid": test_kitchen_by_basic_user_uid},
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # test that admin user is able to do this action
        response = await ac.post(
            "/v2/access-groups/upsert_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "child_access_group_list": groups_to_mark_as_child,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        groups_to_mark_as_child = [
            {
                "uid": test_kitchen_by_basic_user_uid,
            },
        ]

        # test that posting the group's UUID is valid as well
        response = await ac.post(
            "/v2/access-groups/upsert_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_admin_uid},
                "child_access_group_list": groups_to_mark_as_child,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": groups_to_mark_as_child[0].get("uid"),
                    "xid": None,
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # removing admin's membership
        response = await ac.post(
            "/v2/access-groups/delete_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {"uid": test_kitchen_by_basic_user_uid},
                "members": [{"user_id": USER_ADMIN_ID}],
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert (
            response.status_code == status.HTTP_200_OK
        ), "Admin user hasn't been removed from basic user's access group."


@pytest.mark.asyncio
async def test_post_access_group_edge(post_access_group_edge: None) -> None:
    return


@pytest.mark.asyncio
async def test_get_access_group_edge(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[str, str, str],
    group_inaccessibility_for_basic_user: None,
    post_access_group_edge: None,
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin
        test_kitchen_by_basic_user_uid = group_inaccessibility_for_basic_user

        # testing all variations of this request
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "child_access_group_list": [{"xid": TEST_KITCHEN_BY_BASIC_USER.get("xid")}],
            },
        )
        assert (
            response.status_code == status.HTTP_200_OK
        ), "Basic user's access group child status hasn't been received."
        assert response.json() == {
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            "namespace_uid": None,
        }

        # the same but with namespace
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "child_access_group_list": [{"xid": TEST_KITCHEN_BY_BASIC_USER.get("xid")}],
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )

        assert (
            response.status_code == status.HTTP_200_OK
        ), "Basic user's access group child status hasn't been received."
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # with child uid
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "child_access_group_list": [{"uid": test_kitchen_by_basic_user_uid}],
            },
        )

        assert (
            response.status_code == status.HTTP_200_OK
        ), "Basic user's access group child status hasn't been received."
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": None,
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # with parent uid
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_admin_uid},
                "child_access_group_list": [{"xid": TEST_KITCHEN_BY_BASIC_USER.get("xid")}],
            },
        )
        assert (
            response.status_code == status.HTTP_200_OK
        ), "Basic user's access group child status hasn't been received."
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # with child and parent uid
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_admin_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_basic_user_uid}],
            },
        )
        assert (
            response.status_code == status.HTTP_200_OK
        ), "Basic user's access group child status hasn't been received."
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": None,
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # test if superuser can access this endpoint as well
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_admin_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_basic_user_uid}],
            },
        )
        assert (
            response.status_code == status.HTTP_200_OK
        ), "Basic user's access group child status hasn't been received."
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": None,
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # test negative endpoint response
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_superuser_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_admin_uid}],
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Superuser's access group child status hasn't been received."
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_superuser_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_admin_uid,
                    "xid": None,
                    "statuscode": status.HTTP_404_NOT_FOUND,
                    "message": "Edge was not found.",
                }
            ],
            "namespace_uid": None,
        }

        # test that child group retrieval endpoints returns the same result
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"parent_access_group": {"uid": test_kitchen_by_admin_uid}},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
                "uid": test_kitchen_by_basic_user_uid,
            }
        ], "Superuser didn't receive a list of admin access group child groups."

        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_superuser_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_basic_user_uid}],
            },
        )
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_superuser_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": None,
                    "statuscode": status.HTTP_401_UNAUTHORIZED,
                    "message": "You don't have rights for this action.",
                }
            ],
            "namespace_uid": None,
        }


@pytest.mark.asyncio
async def test_delete_access_group_edge(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[str, str, str],
    group_inaccessibility_for_basic_user: None,
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, _ = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin
        test_kitchen_by_basic_user_uid = group_inaccessibility_for_basic_user

        # giving admin user necessary permissions to do the following command
        members = [
            {
                "user_id": USER_ADMIN_ID,
                "permissions": {
                    UserPermissionsEnum.give_permissions_in_access_group: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "members": members,
                "namespace_uid": CUSTOMER.get("uid"),
                "access_group": {"xid": TEST_KITCHEN_BY_BASIC_USER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"), "uid": test_kitchen_by_basic_user_uid},
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # test deleting the existing edge by an admin of parent group
        response = await ac.post(
            "/v2/access-groups/delete_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_admin_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_basic_user_uid}],
            },
        )
        assert response.status_code == status.HTTP_200_OK

        # test getting this non-existent edge
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_admin_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_basic_user_uid}],
            },
        )
        assert (
            response.status_code == status.HTTP_200_OK
        ), "Basic user's access group child status hasn't been received."
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": None,
                    "statuscode": status.HTTP_404_NOT_FOUND,
                    "message": "Edge was not found.",
                }
            ],
            "namespace_uid": None,
        }

        # restore this edge
        groups_to_mark_as_child = [
            {
                "uid": test_kitchen_by_basic_user_uid,
            },
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_admin_uid},
                "child_access_group_list": groups_to_mark_as_child,
            },
        )
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": None,
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # test getting this restored edge
        response = await ac.post(
            "/v2/access-groups/get_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_admin_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_basic_user_uid}],
            },
        )
        assert (
            response.status_code == status.HTTP_200_OK
        ), "Basic user's access group child status hasn't been received."
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_admin_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": test_kitchen_by_basic_user_uid,
                    "xid": None,
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # removing admin's membership
        response = await ac.post(
            "/v2/access-groups/delete_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {"uid": test_kitchen_by_basic_user_uid},
                "members": [{"user_id": USER_ADMIN_ID}],
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert (
            response.status_code == status.HTTP_200_OK
        ), "Admin user hasn't been removed from basic user's access group."


@pytest.mark.asyncio
async def test_get_all_sub_access_groups(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_customer_namespace: str,
    create_users: Tuple[str, str, str],
    group_inaccessibility_for_basic_user: None,
    post_access_group_edge: None,
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_customer_token = create_customer_namespace
        encoded_super_user_token, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin
        test_kitchen_by_basic_user_uid = group_inaccessibility_for_basic_user

        # get UUID of default namespace group
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_customer_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not []
        new_namespace_default_group_uid = response.json()[0].get("uid")

        # mark superuser's access group as child one to default namespace access group
        groups_to_mark_as_child = [
            {
                "uid": test_kitchen_by_superuser_uid,
            },
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_edges",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "parent_access_group": {"uid": new_namespace_default_group_uid},
                "child_access_group_list": groups_to_mark_as_child,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "parent_access_group": {"uid": new_namespace_default_group_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": groups_to_mark_as_child[0].get("uid"),
                    "xid": None,
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # mark admin's access group as child one to superuser's access group
        groups_to_mark_as_child = [
            {
                "uid": test_kitchen_by_admin_uid,
            },
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_edges",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_superuser_uid},
                "child_access_group_list": groups_to_mark_as_child,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "parent_access_group": {"uid": test_kitchen_by_superuser_uid, "xid": None},
            "child_access_group_list_with_status": [
                {
                    "uid": groups_to_mark_as_child[0].get("uid"),
                    "xid": None,
                    "statuscode": status.HTTP_200_OK,
                    "message": "",
                }
            ],
            "namespace_uid": None,
        }

        # test that superuser can get all subgroups
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"parent_access_group": {"uid": test_kitchen_by_basic_user_uid}},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [], "Superuser didn't receive a list of all sub-access groups of basic user group."

        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"parent_access_group": {"uid": test_kitchen_by_admin_uid}},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_basic_user_uid,
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            }
        ], "Superuser didn't receive a list of all sub-access groups of admin user group."

        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"parent_access_group": {"uid": test_kitchen_by_superuser_uid}},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_admin_uid,
                "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
            },
            {
                "uid": test_kitchen_by_basic_user_uid,
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            },
        ], "Superuser didn't receive a list of all sub-access groups of superuser group."

        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"parent_access_group": {"uid": new_namespace_default_group_uid}},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_superuser_uid,
                "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            {
                "uid": test_kitchen_by_admin_uid,
                "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
            },
            {
                "uid": test_kitchen_by_basic_user_uid,
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            },
        ], "Superuser didn't receive a list of all sub-access groups of default namespace group."

        # test that specifying XID in the query works
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")}},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_admin_uid,
                "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
            },
            {
                "uid": test_kitchen_by_basic_user_uid,
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            },
        ], "Superuser didn't receive a list of all sub-access groups of superuser group via superuser group XID."

        # test that depth query parameter works
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"parent_access_group": {"uid": new_namespace_default_group_uid}, "depth": 2},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_superuser_uid,
                "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            {
                "uid": test_kitchen_by_admin_uid,
                "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
            },
        ], "Superuser didn't receive a list of sub-access groups of default namespace group with depth level 2."

        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"parent_access_group": {"uid": test_kitchen_by_superuser_uid}, "depth": 1},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_admin_uid,
                "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
            },
        ], "Superuser didn't receive a list of sub-access groups of superuser group with depth level 1."

        # test that not specifying access group works

        # since superuser's default group is their own,
        # the response should be the same as the one when specifying superuser's group UUID
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups?legacy_api_request=true",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_admin_uid,
                "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
            },
            {
                "uid": test_kitchen_by_basic_user_uid,
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            },
        ], "Superuser didn't receive a list of all sub-access groups of superuser group."

        # since admin's default group is their own and they are not a member of basic user's group,
        # the response should be empty
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups?legacy_api_request=true",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"namespace_uid": CUSTOMER.get("uid")},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_basic_user_uid,
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            }
        ], "Admin user didn't receive a list of sub-access groups of their default group that they are a member of."

        # test that specifying namespace works
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups?legacy_api_request=true",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"namespace_uid": CUSTOMER.get("uid")},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
            {
                "uid": test_kitchen_by_admin_uid,
                "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
            },
            {
                "uid": test_kitchen_by_basic_user_uid,
                "xid": TEST_KITCHEN_BY_BASIC_USER.get("xid"),
            },
        ], "Superuser didn't receive a list of all sub-access groups of superuser group."

        # test that admin can see only those subgroups of default namespace group that they are a member of
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"parent_access_group": {"uid": new_namespace_default_group_uid}},
        )
        assert response.status_code == status.HTTP_200_OK
        assert (
            response.json() == []
        ), "Admin user received sub-access groups of a namespace group where he is not a member of."

        # test that basic user can see only those subgroups of default namespace group that they are a member of
        response = await ac.post(
            "/v2/access-groups/get_sub_access_groups",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"parent_access_group": {"uid": new_namespace_default_group_uid}},
        )
        assert response.status_code == status.HTTP_200_OK
        assert (
            response.json() == []
        ), "Basic user received sub-access groups of a namespace group where he is not a member of."

        # test inability to remove extra edge
        response = await ac.post(
            "/v2/access-groups/delete_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_superuser_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_admin_uid}],
            },
        )
        assert response.json()["child_access_group_list_with_status"][0]["statuscode"] == status.HTTP_401_UNAUTHORIZED

        response = await ac.post(
            "/v2/access-groups/delete_edges",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "parent_access_group": {"uid": test_kitchen_by_superuser_uid},
                "child_access_group_list": [{"uid": test_kitchen_by_admin_uid}],
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.json()["child_access_group_list_with_status"][0]["statuscode"] == status.HTTP_401_UNAUTHORIZED

        # remove extra edges that were created for this test only
        response = await ac.post(
            "/v2/access-groups/delete_edges",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "parent_access_group": {"uid": new_namespace_default_group_uid},
                "child_access_group_list": [{"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")}],
            },
        )
        assert response.json()["child_access_group_list_with_status"][0]["statuscode"] == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/delete_edges",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                "child_access_group_list": [{"uid": test_kitchen_by_admin_uid}],
            },
        )
        assert response.json()["child_access_group_list_with_status"][0]["statuscode"] == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_get_no_node_types_from_access_group(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[str, str, str],
) -> None:
    """This test checks whether node types endpoint can return node types when a group has no nodes."""
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, _ = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        # getting UUIDs of all nodes in admin's access group
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[{"access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")}}],
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == []
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[{"access_group": {"uid": test_kitchen_by_admin_uid}}],
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == []


@pytest.mark.asyncio
async def test_get_one_node_type_from_access_group(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_delete_and_get_supply_by_admin: None,
    create_users: Tuple[str, str, str],
) -> None:
    """This test checks whether node types endpoint can return node types when a group has nodes only of 1 type."""
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, _ = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin
        test_supply_uid, _, _ = create_delete_and_get_supply_by_admin

        def check_node_type_response(resp: Response) -> None:
            assert resp.status_code == 200
            node_uid_by_type = node_resp_to_dict_by_type(resp.json())
            assert node_uid_by_type[SupplySheetActivityNode.__name__] == {
                test_supply_uid,
            }, "There should be one SupplySheetActivityNode."
            assert len(node_uid_by_type[FoodProductFlowNode.__name__]) == 6, "There should be six FoodProductFlowNode."

        # getting UUIDs of all nodes in admin's access group
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[{"access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")}}],
        )
        check_node_type_response(response)

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[{"access_group": {"uid": test_kitchen_by_admin_uid}}],
        )

        check_node_type_response(response)


@pytest.mark.asyncio
async def test_get_two_node_types_from_access_group(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_delete_and_get_subrecipe_by_admin: str,
    create_delete_and_get_supply_by_admin: None,
    create_delete_and_get_recipe_by_admin: str,
    create_users: Tuple[str, str, str],
) -> None:
    """This test checks whether node types endpoint can return node types when a group has nodes only of 2 types."""
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, _ = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin
        test_recipe_uid = create_delete_and_get_recipe_by_admin
        test_recipe_with_subrecipe_uid = create_delete_and_get_subrecipe_by_admin
        test_supply_uid, _, _ = create_delete_and_get_supply_by_admin

        def check_node_type_response(resp: Response) -> None:
            assert resp.status_code == 200
            node_uid_by_type = node_resp_to_dict_by_type(resp.json())
            assert node_uid_by_type[FoodProcessingActivityNode.__name__] == {
                test_recipe_uid,
                test_recipe_with_subrecipe_uid,
            }
            assert node_uid_by_type[SupplySheetActivityNode.__name__] == {test_supply_uid}
            assert len(node_uid_by_type[FoodProductFlowNode.__name__]) == 13

        # getting UUIDs of all nodes in admin's access group
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[{"access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")}}],
        )
        check_node_type_response(response)

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[{"access_group": {"uid": test_kitchen_by_admin_uid}}],
        )

        check_node_type_response(response)


@pytest.mark.asyncio
async def test_get_all_node_types_from_access_group(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_delete_and_get_product_by_admin: None,
    create_delete_and_get_subrecipe_by_admin: str,
    create_delete_and_get_supply_by_admin: None,
    create_delete_and_get_recipe_by_admin: str,
    create_users: Tuple[str, str, str],
) -> None:
    """This test checks whether node types endpoint can return nodes of all types."""
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, _ = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin
        test_product_id = create_delete_and_get_product_by_admin
        test_recipe_uid = create_delete_and_get_recipe_by_admin
        test_recipe_with_subrecipe_uid = create_delete_and_get_subrecipe_by_admin
        test_supply_uid, _, _ = create_delete_and_get_supply_by_admin

        def check_node_type_response(resp: Response) -> None:
            assert resp.status_code == 200
            node_uid_by_type = node_resp_to_dict_by_type(resp.json())
            assert node_uid_by_type[FoodProcessingActivityNode.__name__] == {
                test_product_id,
                test_recipe_uid,
                test_recipe_with_subrecipe_uid,
            }
            assert node_uid_by_type[SupplySheetActivityNode.__name__] == {test_supply_uid}
            assert len(node_uid_by_type[FoodProductFlowNode.__name__]) == 16

        # getting UUIDs of all nodes in admin's access group
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[{"access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")}}],
        )
        check_node_type_response(response)

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[{"access_group": {"uid": test_kitchen_by_admin_uid}}],
        )

        check_node_type_response(response)


@pytest.mark.asyncio
async def test_basic_user_can_not_get_node_types_from_access_group(
    app: FastAPI,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[str, str, str],
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, _, encoded_basic_user_token = create_users

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                }
            ],
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "ignore_production_amounts, set_legacyAPI_response_behavior",
    [(False, False), (False, True), (True, False), (True, True)],
)
async def test_inheritance_of_access_group_configurations(
    app: FastAPI,
    ignore_production_amounts: bool,
    set_legacyAPI_response_behavior: bool,
) -> None:
    service_provider = service_locator.service_provider
    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        # create a new customer
        settings = Settings()
        customer_namespace_uid = "522712c1-3988-4c42-a103-2e02ea39825a"
        customer_namespace_xid = "customer_xid"
        response = await ac.put(
            f"/v2/batch/customers/{customer_namespace_xid}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": "test customer",
                    "uid": customer_namespace_uid,
                }
            },
        )
        assert response.json().get("status"), "Customer hasn't been created."

        customer_token = response.json().get("auth_token")
        encoded_customer_token = base64.b64encode(customer_token.encode("utf-8")).decode("utf-8")
        namespace_uid = response.json().get("namespace").get("uid")

        # get the default access group of the namespace and check that ignore_fields is None
        access_groups = await postgres_db.access_group_mgr.get_all_access_group_ids_by_namespace(namespace_uid)

        assert len(access_groups) == 1, "At this point there should only be the default access group."
        default_access_group = access_groups[0]

        default_access_group = await postgres_db.access_group_mgr.get_by_uid(default_access_group.uid)
        assert default_access_group.data.get("ignore_fields") is None, "Ignore production amount should be None."
        assert (
            default_access_group.data.get("legacyAPI_response_behavior") is None
        ), "legacyAPI_response_behavior should be None."

        if ignore_production_amounts:
            # manually change the 'ignore_production_amounts'-setting for the default access-group of the namespace
            default_access_group.data["ignore_fields"] = ["production_amount"]

        if set_legacyAPI_response_behavior:
            # manually change the 'legacyAPI_response_behavior'-setting for the default access-group of the namespace
            default_access_group.data["legacyAPI_response_behavior"] = {
                "empty_recipe_behavior": {"remove_fields_from_response": ["rating"]}
            }

        await postgres_db.access_group_mgr.upsert_access_group_by_uid(default_access_group)
        default_access_group = await postgres_db.access_group_mgr.get_by_uid(default_access_group.uid)

        if ignore_production_amounts:
            assert default_access_group.data.get("ignore_fields") == [
                "production_amount"
            ], "Production amount should be ignored."

        if set_legacyAPI_response_behavior:
            assert default_access_group.data.get("legacyAPI_response_behavior") == {
                "empty_recipe_behavior": {"remove_fields_from_response": ["rating"]}
            }, "Legacy API response behavior should be set."

        # create a new kitchen
        kitchen_data = {
            "name": "canteen",
            "location": "zurich, switzerland",
            "email": "test@example.com",
            "language": "en",
            "type": "kitchen",
        }
        response = await ac.post(
            "/v2/access-groups/upsert_access_group?legacy_api_request=true",
            headers={"Authorization": f"Basic {encoded_customer_token}"},
            json={
                "access_group": kitchen_data,
            },
        )
        assert response.status_code == status.HTTP_201_CREATED

        assert response.json().get("access_group").get("location") == kitchen_data.get("location")
        assert response.json().get("access_group").get("name") == kitchen_data.get("name")
        assert response.json().get("access_group").get("email") == kitchen_data.get("email")
        assert response.json().get("access_group").get("language") == kitchen_data.get("language")
        assert response.json().get("access_group").get("type") == kitchen_data.get("type")
        expected_ignore_fields = ["production_amount"] if ignore_production_amounts else None
        assert response.json().get("access_group").get("ignore_fields") == expected_ignore_fields
        expected_legacyAPI_response_behavior = (
            {"empty_recipe_behavior": {"remove_fields_from_response": ["rating"], "statuscode": None, "message": None}}
            if set_legacyAPI_response_behavior
            else None
        )
        assert (
            response.json().get("access_group").get("legacyAPI_response_behavior")
            == expected_legacyAPI_response_behavior
        )


@pytest.mark.asyncio
async def test_get_all_root_access_group_info(
    app: FastAPI, create_kitchens_by_admin_and_superuser: Tuple[bytes, str, bytes, str]
) -> None:
    (
        encoded_super_user_token,
        superuser_access_group_uid,
        encoded_admin_user_token,
        admin_access_group_uid,
    ) = create_kitchens_by_admin_and_superuser
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        response = await ac.get(
            "/v2/access-groups/all_roots",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK

        access_group_info_list = response.json()["access_groups"]
        # Organise access group info by uid.
        access_groups_by_uid = {
            access_group_info["uid"]: access_group_info for access_group_info in access_group_info_list
        }

        assert (
            superuser_access_group_uid in access_groups_by_uid
        ), "Superuser access group should be in access_groups_by_uid."
        assert (
            admin_access_group_uid not in access_groups_by_uid
        ), "Admin access group should be not be in access_groups_by_uid as it is not root."
        assert len(access_groups_by_uid) == 12
        assert access_groups_by_uid[superuser_access_group_uid]["xid"] == TEST_KITCHEN_BY_SUPERUSER.get("xid")
        assert access_groups_by_uid[superuser_access_group_uid]["namespace_uid"] == CUSTOMER.get("uid")
        assert access_groups_by_uid[superuser_access_group_uid]["namespace_name"] == CUSTOMER.get("name")
