import asyncio
import base64
import copy
import dataclasses
import json
import os
import uuid
from typing import Any, AsyncGenerator, Generator, Optional, Tuple
from unittest import mock

import pytest
import pytest_asyncio
from _pytest.fixtures import SubRequest
from fastapi import status
from httpx import AsyncClient, Response
from lxml import etree
from structlog import get_logger

from api.app.injector import get_access_controller, get_node_controller
from api.app.server import app_shutdown, app_startup, fastapi_app
from api.app.settings import Settings
from api.controller.access_group_controller import AccessGroupController
from api.controller.node_controller import NodeController
from api.controller.report_generation_controller import ReportGenerationController
from api.dto.v2.node_dto import BatchInputDto
from core.domain.calculation import Calculation
from core.domain.glossary_link import GlossaryLink
from core.domain.nodes import FoodProcessingActivityNode, LinkingActivityNode
from core.domain.nodes.activity.elementary_resource_emission_node import ElementaryResourceEmissionNode
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.nodes.activity.supply_sheet_activity import SupplySheetActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.root_with_subflows_dto import RootWithSubFlowsDto
from core.domain.props.link_to_uid_prop import LinkToUidProp
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator, ServiceProvider
from core.tests.conftest import add_flow, add_greenhouse_mock_nodes
from database.postgres.settings import PgSettings

CUSTOMER = {
    "name": "test_name",
    "xid": "test_ns_xid",
    "uid": "6d287e53-d3d7-4154-8113-6a3f30417ded",
}

USER_SUPER = {
    "email": "asd@example.com",
    # password is optional right now, leaving this field as
    # a placeholder for future authentication features
    "password": "",
    "is_superuser": True,
}
USER_SUPER_ID = str(uuid.uuid4())

USER_ADMIN = {
    "email": "wasd@example.com",
    "password": "",
    "is_superuser": False,
}
USER_ADMIN_ID = str(uuid.uuid4())

USER_BASIC = {
    "email": "qwe@example.com",
    "password": "",
    "is_superuser": False,
}
USER_BASIC_ID = str(uuid.uuid4())

TEST_KITCHEN_BY_SUPERUSER = {
    "xid": "test_kitchen_id",
    "type": "kitchen",
    "name": "test__kitchen_name",
    "location": "test_location",
    "email": None,
    "language": None,
    "creator": USER_SUPER_ID,
    "ignore_fields": None,
    "legacyAPI_response_behavior": None,
}
TEST_KITCHEN_BY_ADMIN = {
    "xid": "test_kitchen_admin_id",
    "type": "kitchen",
    "name": "test__kitchen_admin_name",
    "location": "test_admin_location",
    "email": None,
    "language": None,
    "creator": USER_ADMIN_ID,
    "ignore_fields": None,
    "legacyAPI_response_behavior": None,
}
TEST_KITCHEN_BY_BASIC_USER = {
    "xid": "test_kitchen_basic_id",
    "type": "kitchen",
    "name": "test__kitchen_basic_name",
    "location": "test_basic_location",
    "email": None,
    "language": None,
    "creator": USER_BASIC_ID,
    "ignore_fields": None,
    "legacyAPI_response_behavior": None,
}

TEST_RECIPE_FLOW_ID = "test_recipe_flow_id"
TEST_RECIPE_ID = "test_recipe_id"
TEST_RECIPE = [
    {
        "input_root": {
            "flow": {
                "xid": TEST_RECIPE_FLOW_ID,
                "node_type": FoodProductFlowNode.__name__,
                "amount_in_original_source_unit": {"value": 150 + 78, "unit": "gram"},
                "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
            },
            "activity": {
                "xid": TEST_RECIPE_ID,
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_location": "Zürich Schweiz",
                "author": "Eckart Witzigmann",
                "activity_date": "2023-06-20",
                "recipe_portions": 140,
                "production_portions": 300,
                "sold_portions": 250,
                "instructions": [
                    {"language": "de", "value": "Den Karottenkuchen im Ofen backen und noch warm geniessen."},
                    {"language": "en", "value": "Bake the carrot cake in the oven and enjoy while still hot."},
                ],
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "100100191",
                    "raw_production": {"value": "standard", "language": "en"},
                    "raw_transport": "air",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "amount_in_original_source_unit": {"value": 150, "unit": "gram"},
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "100100894",
                    "raw_production": {"value": "organic", "language": "en"},
                    "raw_transport": "ground",
                    "flow_location": "france",
                    "raw_labels": {"value": "Bio Suisse", "language": "fr"},
                    "product_name": [{"language": "de", "value": "Zwiebeln"}],
                    "amount_in_original_source_unit": {"value": 78, "unit": "gram"},
                    "raw_conservation": {"value": "dried", "language": "en"},
                    "packaging": "",
                },
            ],
        },
    },
]

TEST_RECIPE_WITH_SUBRECIPE_ID = "test_recipe_with_subrecipe_id"
TEST_RECIPE_WITH_SUBRECIPE = [
    {
        "input_root": {
            "flow": {
                "node_type": FoodProductFlowNode.__name__,
                "amount_in_original_source_unit": {"value": 50 + 78, "unit": "gram"},
                "product_name": [{"language": "de", "value": "Parent Recipe with child recipes"}],
            },
            "activity": {
                "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_location": "Zürich Schweiz",
                "activity_date": "2023-06-20",
                "recipe_portions": 140,
                "production_portions": 300,
                "sold_portions": 250,
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "488533744",
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "link_to_sub_node": {"xid": TEST_RECIPE_ID},
                    "raw_transport": "air",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
                    "amount_in_original_source_unit": {"value": 50, "unit": "gram"},
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "242342343",
                    "raw_production": {"value": "organic", "language": "en"},
                    "raw_transport": "ground",
                    "flow_location": "france",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "amount_in_original_source_unit": {"value": 78, "unit": "gram"},
                    "processing": "",
                    # "raw_conservation": {"value": dried", "language": "en"}  # Removed dried to test greenhouse GFM.
                    "packaging": "",
                },
            ],
        }
    },
]

TEST_SUPPLY_ID = "supply01"
TEST_SUB_FOOD_PRODUCT_ID_01 = "sub_food_product01"
TEST_SUB_FOOD_PRODUCT_ID_02 = "sub_food_product02"
TEST_SUPPLY = [
    {
        "input_root": {
            "activity": {
                "xid": TEST_SUPPLY_ID,
                "node_type": SupplySheetActivityNode.__name__,
                "supplier": "Vegetable Supplies Ltd.",
                "supplier_id": "el3i5y-2in5y-2hbllll01",
                "invoice_id": "778888800000001",
                "activity_date": "2023-06-20",
            },
            "sub_flows": [
                {
                    "xid": "100100101",
                    "node_type": FoodProductFlowNode.__name__,
                    "amount_in_original_source_unit": {"value": 150, "unit": "gram"},
                    "link_to_sub_node": {"xid": TEST_SUB_FOOD_PRODUCT_ID_01},
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "raw_transport": "ground",
                    "flow_location": "Zürich Schweiz",
                    "raw_processing": {"value": "raw", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "xid": "300100191",
                    "node_type": FoodProductFlowNode.__name__,
                    "amount_in_original_source_unit": {"value": 270, "unit": "gram"},
                    "link_to_sub_node": {"xid": TEST_SUB_FOOD_PRODUCT_ID_02},
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "raw_transport": "ground",
                    "flow_location": "Zürich Schweiz",
                    "raw_processing": {"value": "raw", "language": "en"},
                    "packaging": "plastic",
                },
            ],
        },
        "return_final_graph": True,
        "namespace_uid": CUSTOMER.get("uid"),
    },
    {
        "input_root": {
            "activity": {
                "activity_date": "2023-06-20",
                "node_type": LinkingActivityNode.__name__,
                "xid": TEST_SUB_FOOD_PRODUCT_ID_01,
                "name": [
                    {
                        "language": "de",
                        "value": "Zwiebeln_Kartoffeln_Produkt",
                    },
                ],
                "production_amount": {"value": 1.0, "unit": "kilogram"},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "nutrient_values": {  # generated with nutrient_mixer in core/core/test_ingredient_amount_estimator
                        # with ratio  Zwiebeln : Kartoffeln = 7 : 3
                        "energy_kcal": 50.213899999999995,
                        "fat_gram": 0.16999999999999998,
                        "saturated_fat_gram": 0.0,
                        "carbohydrates_gram": 9.579999999999998,
                        "water_gram": 85.91,
                        "sucrose_gram": 5.109999999999999,
                        "protein_gram": 1.5099999999999998,
                        "sodium_chloride_gram": 3.5599999999999996 * 1e-3,
                        "chlorine_milligram": 28.299999999999997,
                    },
                    "ingredients_declaration": [
                        {
                            "language": "de",
                            "value": "Zwiebeln, Kartoffeln",
                        },
                    ],
                    "amount_in_original_source_unit": {"value": 1.0, "unit": "kilogram"},
                }
            ],
        },
        "skip_calc": True,
        "namespace_uid": CUSTOMER.get("uid"),
    },
    {
        "input_root": {
            "activity": {
                "activity_date": "2023-06-20",
                "node_type": LinkingActivityNode.__name__,
                "xid": TEST_SUB_FOOD_PRODUCT_ID_02,
                "name": [
                    {
                        "language": "de",
                        "value": "Kartoffel_Tomaten_Produkt",
                    },
                ],
                "production_amount": {"value": 1.0, "unit": "kilogram"},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "nutrient_values": {  # generated with nutrient_mixer in core/core/test_ingredient_amount_estimator
                        # with ratio Kartoffeln : Tomaten = 6 : 4
                        "energy_kcal": 54.39639999999999,
                        "fat_gram": 0.18,
                        "saturated_fat_gram": 0.0,
                        "carbohydrates_gram": 10.64,
                        "water_gram": 84.74000000000001,
                        "sucrose_gram": 1.7000000000000002,
                        "protein_gram": 1.52,
                        "sodium_milligram": 2.44,
                        "chlorine_milligram": 42.0,
                    },
                    "ingredients_declaration": [
                        {
                            "language": "de",
                            "value": "Kartoffeln, Tomaten",
                        },
                    ],
                    "amount_in_original_source_unit": {"value": 1.0, "unit": "kilogram"},
                }
            ],
        },
        "skip_calc": True,
        "namespace_uid": CUSTOMER.get("uid"),
    },
]

FastAPIReturn = AsyncGenerator[Any, Any]
logger = get_logger()
settings = Settings()

service_locator = ServiceLocator()

TOLERANCE = 1e-6


def is_close(a: float, b: float, tol: float = TOLERANCE) -> bool:
    return abs(a - b) < tol


def is_dict_close(
    dict1: dict | float | int | str,
    dict2: dict | float | int | str,
    scale_by: Optional[dict[str, float]] = None,
    tol: float = TOLERANCE,
) -> bool:
    # Check if keys in both dictionaries are the same
    if dict1.keys() != dict2.keys():
        return False

    for key in dict1:
        val1 = dict1[key]
        val2 = dict2[key]

        if isinstance(val1, dict) and isinstance(val2, dict):
            # Recursively compare nested dictionaries
            if not is_dict_close(val1, val2, scale_by=scale_by, tol=tol):
                logger.error(f"{val1} and {val2} are different.")
                return False
        elif isinstance(val1, list) and isinstance(val2, list):
            assert len(val1) == len(val2)
            for list_item_val1, list_item_val2 in zip(val1, val2):
                if isinstance(list_item_val1, dict) and isinstance(list_item_val2, dict):
                    if not is_dict_close(list_item_val1, list_item_val2, scale_by=scale_by, tol=tol):
                        logger.error(f"{list_item_val1} and {list_item_val2} are different.")
                        return False
                else:
                    # Fallback for other types (e.g., integers, strings)
                    if list_item_val1 != list_item_val2:
                        logger.error(f"{list_item_val1} and {list_item_val2} different.")
                        return False
        elif isinstance(val1, (float, int)) and isinstance(val2, (float, int)):
            # Compare float values with a tolerance
            if scale_by and key in scale_by:
                scaled_val2 = val2 * scale_by[key]
                if isinstance(val1, int):
                    scaled_val2 = int(scaled_val2)
                if abs(val1 - scaled_val2) > tol:
                    logger.error(f"{val1} and {scaled_val2} are different.")
                    return False
            else:
                if abs(val1 - val2) > tol:
                    logger.error(f"{val1} and {val2} are different.")
                    return False
        else:
            # Fallback for other types (e.g., integers, strings)
            if val1 != val2:
                logger.error(f"{val1} and {val2} different.")
                return False

    return True


def find_quantity_value(quantities_dict: dict, quantity_term_name: str) -> Optional[float]:
    """Helper function to extract a quantity value from a dictionary of quantities."""
    for _, value in quantities_dict.items():
        if "quantity_term_name" in value and value["quantity_term_name"] == quantity_term_name:
            return value["quantity"]["value"]
    return None


def check_mutation_log(response: Response, expected_mutation_log: dict) -> None:
    # we now have to check the second mutation, since the first is the addition of the root-flow-node
    first_mutation = response.json().get("batch")[0].get("mutations")[1]
    recursive_compare_dict(first_mutation, expected_mutation_log)


def recursive_compare_dict(response: dict, expected: dict) -> None:
    for expected_key, expected_val in expected.items():
        assert expected_key in response
        response_val = response[expected_key]

        if isinstance(expected_val, dict):
            assert isinstance(response_val, dict)
            recursive_compare_dict(response_val, expected_val)

        elif isinstance(expected_val, list):
            assert isinstance(response_val, list)

            # we recursively check for list equality
            for idx, expected_item_val in enumerate(expected_val):
                assert len(response_val) >= idx
                resp_item = response_val[idx]
                if isinstance(resp_item, dict):
                    assert isinstance(resp_item, dict)
                    recursive_compare_dict(resp_item, expected_item_val)
                else:
                    assert resp_item == expected_item_val

        else:
            assert response_val == expected_val


def change_in_every_item_in_batch(batch: dict, key: str, value: str) -> dict:
    """Helper function to change a key in every item in a batch."""
    new_batch = copy.deepcopy(batch)
    for item in new_batch:
        item[key] = value
    return new_batch


def change_in_every_activity_in_batch(batch: list[dict], key: str, value: str | dict) -> list[dict]:
    """Helper function to change a key in every activity in a batch."""
    new_batch = copy.deepcopy(batch)
    for item in new_batch:
        item["input_root"]["activity"][key] = value
    return new_batch


def check_response_codes_in_batch_calculation(response: Response, expected_response_code: int) -> None:
    """Helper Function to check the response codes in a batch response."""
    assert response.status_code == status.HTTP_200_OK
    for batch in response.json().get("batch"):
        assert batch.get("statuscode") == expected_response_code, (
            f"status code of batch item should " f"be {expected_response_code}"
        )


@pytest.fixture(scope="package")
def event_loop() -> Generator[asyncio.AbstractEventLoop, Any, None]:
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="function")
async def app() -> FastAPIReturn:
    async for app_instance in app_common():
        yield app_instance


@pytest_asyncio.fixture(scope="function")
async def app_with_messaging() -> FastAPIReturn:
    async for app_instance in app_common(mock_messaging=False):
        yield app_instance


@pytest_asyncio.fixture(scope="module")
async def app_module() -> FastAPIReturn:
    async for app_instance in app_common():
        yield app_instance


async def app_common(mock_messaging: bool = True) -> FastAPIReturn:
    service_locator.recreate()
    service_provider = service_locator.service_provider

    if not mock_messaging:
        # usually cache fetching and invalidation is disabled,
        # no other pods are present anyways. We need to enable it for certain tests.
        Settings.override_settings(
            LOAD_CACHES_FROM_RUNNING_PODS=True,
            SUBSCRIBE_CACHE_INVALIDATIONS=True,
        )

    postgres_db = service_provider.postgres_db
    async with AsyncClient(
        app=fastapi_app,
        base_url="http://localhost:8040",
    ):
        await postgres_db.connect(schema="test_pg")
        await postgres_db.reset_db("test_pg")

        settings = PgSettings()

        graph_mgr = postgres_db.get_graph_mgr()

        co2_emission = await graph_mgr.upsert_node_by_uid(
            ElementaryResourceEmissionNode(
                node_type=ElementaryResourceEmissionNode.__name__,
                production_amount={"value": 1.0, "unit": "kilogram"},
                key=["biosphere3", "349b29d1-3e58-4c66-98b9-9d1a076efd2e"],
                name="Carbon dioxide, fossil",
                categories=["air"],
            )
        )
        # necessary for transportation & orchestration unit tests
        await postgres_db.get_product_mgr().bulk_insert_xid_uid_mappings(
            [
                (
                    settings.EATERNITY_NAMESPACE_UUID,
                    "biosphere3_349b29d1-3e58-4c66-98b9-9d1a076efd2e",
                    co2_emission.uid,
                )
            ],
        )

        # add the nodes necessary to for the greenhouse gfm
        await add_greenhouse_mock_nodes(postgres_db, graph_mgr, co2_emission, country_code="FR")
        await add_greenhouse_mock_nodes(postgres_db, graph_mgr, co2_emission, country_code="CH")

        term_mgr = postgres_db.get_term_mgr()
        foodex2_term_group_uid = ""
        groups = await term_mgr.find_all_term_access_groups()
        for group in groups:
            if "FoodEx2" in group.data.get("name"):
                foodex2_term_group_uid = group.uid
                break
        # the cooling terms have to be added always because they might also be relevant if cooling
        # was not explicitly declared in the recipe (e.g. for perishable products)
        cooled_term = await postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
            "J0131", foodex2_term_group_uid
        )

        cooling_process = await graph_mgr.upsert_node_by_uid(
            ModeledActivityNode(
                activity_location="GLO",
                production_amount={"value": 1, "unit": "kg*day"},
                id="('ecoinvent 3.6 cutoff', '001decfdb6f2319cd8578e05c91c247d')",
                key=[
                    "ecoinvent 3.6 cutoff",
                    "001decfdb6f2319cd8578e05c91c247d",
                ],
                flow=None,
                name="market for operation, reefer, cooling",
                type="process",
                database="ecoinvent 3.6 cutoff",
                filename=None,
                reference_product="market for operation, reefer, cooling",
            )
        )

        await postgres_db.get_pg_glossary_link_mgr().insert_glossary_link(
            GlossaryLink(
                gap_filling_module="LinkTermToActivityNode",  # TODO don't hardcode
                term_uids=[cooled_term.uid],
                linked_node_uid=cooling_process.uid,
            )
        )

        await add_flow(graph_mgr, cooling_process, co2_emission, 0.005, "biosphere")

        await app_startup(fastapi_app, schema="test_pg")

    yield fastapi_app

    # this will shutdown the app after each test
    await app_shutdown(fastapi_app)


@pytest_asyncio.fixture(scope="function")
async def term_access_group_uids(app: FastAPIReturn) -> Tuple[str, str, str, str]:
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(app=app, base_url="http://localhost:8040") as _:
        await postgres_db.connect(schema="test_pg")

    eaternity_term_group_uid = ""
    eurofir_term_group_uid = ""
    foodex2_term_group_uid = ""
    nutrient_subdivision_term_group_uid = ""

    groups = await postgres_db.get_term_mgr().find_all_term_access_groups()

    for group in groups:
        if "eaternity" in group.data.get("name"):
            eaternity_term_group_uid = group.uid
        if "EuroFIR" in group.data.get("name"):
            eurofir_term_group_uid = group.uid
        if "FoodEx2" in group.data.get("name"):
            foodex2_term_group_uid = group.uid
        if "Nutrients Subdivision" in group.data.get("name"):
            nutrient_subdivision_term_group_uid = group.uid

    return (
        eaternity_term_group_uid,
        eurofir_term_group_uid,
        foodex2_term_group_uid,
        nutrient_subdivision_term_group_uid,
    )


@pytest_asyncio.fixture(scope="function")
async def basic_router_availability(app: FastAPIReturn) -> None:
    await basic_router_availability_common(app=app)


@pytest_asyncio.fixture(scope="module")
async def basic_router_availability_module(app_module: FastAPIReturn) -> None:
    await basic_router_availability_common(app=app_module)


async def basic_router_availability_common(app: FastAPIReturn) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        response = await ac.get("/v2")
        assert response.status_code == status.HTTP_200_OK, "/v2 should be reachable"
        assert response.json().get("status") == "successful"

        response = await ac.get("/v2/status")
        assert response.status_code == status.HTTP_200_OK, "/v2/status should be reachable"
        assert response.json().get("status") == "successful"


@pytest_asyncio.fixture(scope="function")
async def create_customer_namespace(app: FastAPIReturn, basic_router_availability: None) -> bytes:
    return await create_customer_namespace_common(app=app, basic_router_availability=basic_router_availability)


@pytest_asyncio.fixture(scope="module")
async def create_customer_namespace_module(app_module: FastAPIReturn, basic_router_availability_module: None) -> bytes:
    return await create_customer_namespace_common(
        app=app_module, basic_router_availability=basic_router_availability_module
    )


async def create_customer_namespace_common(app: FastAPIReturn, basic_router_availability: None) -> bytes:
    _ = basic_router_availability
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": "Basic wrong-key"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                    "uid": CUSTOMER.get("uid"),
                }
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "Invalid authentication credentials"}

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                    "uid": CUSTOMER.get("uid"),
                }
            },
        )
        response_payload = response.json()
        assert response.status_code == status.HTTP_200_OK

    assert response_payload.get("namespace", {}).get("uid"), (
        "UUID of the created namespace " "must be present in the response."
    )

    assert response_payload.get("auth_token"), (
        "Authentication token for the created namespace " "must be present in the response."
    )
    encoded_customer_token = response_payload.get("auth_token")

    assert response_payload == {
        "auth_token": encoded_customer_token,
        "status": True,
        "namespace": {
            "name": CUSTOMER.get("name"),
            "uid": CUSTOMER.get("uid"),
            "xid": CUSTOMER.get("xid"),
        },
    }

    encoded_customer_token = base64.b64encode(encoded_customer_token.encode("utf-8")).decode("utf-8")
    return encoded_customer_token


@pytest_asyncio.fixture(scope="function")
async def get_simple_kitchen(app: FastAPIReturn) -> str:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                    "uid": CUSTOMER.get("uid"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
    return encoded_super_user_token


@pytest_asyncio.fixture(scope="function")
async def get_customer_namespace(app: FastAPIReturn, basic_router_availability: None) -> None:
    _ = basic_router_availability
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        response = await ac.get(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": "Basic wrong-key"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "Invalid authentication credentials"}

        # create namespace first
        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                    "uid": CUSTOMER.get("uid"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        assert response.json().get("namespace", {}).get("uid"), (
            "UUID of the created namespace " "must be present in the response."
        )
        response = await ac.get(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_200_OK
        response_payload = response.json()

    assert len(response_payload) == 1
    assert response_payload[0].get("user_id", {}), "UUID of the fetched namespace must be present in the response."


@pytest_asyncio.fixture(scope="function")
async def create_users(app: FastAPIReturn, create_customer_namespace: bytes) -> Tuple[bytes, bytes, bytes]:
    return await create_users_common(app=app, create_customer_namespace=create_customer_namespace)


@pytest_asyncio.fixture(scope="module")
async def create_users_module(
    app_module: FastAPIReturn, create_customer_namespace_module: bytes
) -> Tuple[bytes, bytes, bytes]:
    return await create_users_common(app=app_module, create_customer_namespace=create_customer_namespace_module)


async def create_users_common(app: FastAPIReturn, create_customer_namespace: bytes) -> Tuple[bytes, bytes, bytes]:
    _ = create_customer_namespace
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_ADMIN,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_admin_user_token = response.json().get("auth_token")
        encoded_admin_user_token = base64.b64encode(encoded_admin_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_BASIC,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_basic_user_token = response.json().get("auth_token")
        encoded_basic_user_token = base64.b64encode(encoded_basic_user_token.encode("utf-8")).decode("utf-8")

        return encoded_super_user_token, encoded_admin_user_token, encoded_basic_user_token


@pytest_asyncio.fixture(scope="function")
async def create_and_get_and_delete_and_create_kitchen_by_superuser(
    app: FastAPIReturn, create_users: Tuple[bytes, bytes, bytes]
) -> str:
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, _ = create_users

        # not specifying a namespace here
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
            },
        )
        assert response.status_code == status.HTTP_406_NOT_ACCEPTABLE

        # explicitly setting a namespace here
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_SUPERUSER_WITH_UID = copy.deepcopy(TEST_KITCHEN_BY_SUPERUSER)
        TEST_KITCHEN_BY_SUPERUSER_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # this can work only once we have a default group set for this user,
        # since that's how we get target namespace when it is not specified in the request
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                },
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_SUPERUSER.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_SUPERUSER.get("location")

        # add this access group as a default one for superuser
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "user": USER_SUPER,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid")
        assert response.json().get("status") is True

        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                },
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_SUPERUSER.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_SUPERUSER.get("location")

        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json()[0].get("permissions").get(UserPermissionsEnum.add_users_to_group) is True

        response = await ac.post(
            "/v2/access-groups/delete_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")}},
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                },
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": f"Group {TEST_KITCHEN_BY_SUPERUSER.get('xid')} does not exist."}

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_SUPERUSER_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # add this access group as a default one for superuser
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "user": USER_SUPER,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid")
        assert response.json().get("status") is True

        # now we can send a request without specifying target namespace
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("access_group", {}).get("uid") is not None
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # testing this request via UUID
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
                "parent_access_group": {"uid": TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("access_group", {}).get("xid") is not None
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_SUPERUSER_WITH_UID,
            "namespace_uid": CUSTOMER.get("uid"),
        }

        return TEST_KITCHEN_BY_SUPERUSER_WITH_UID.get("uid")


async def upsert_admin_to_superuser_with_create_permission(
    async_client: AsyncClient, encoded_super_user_token: str
) -> None:
    members = [
        {
            "user_id": USER_ADMIN_ID,
            "permissions": {
                UserPermissionsEnum.create_access_group: True,
            },
        }
    ]

    response = await async_client.post(
        "/v2/access-groups/upsert_members",
        headers={"Authorization": f"Basic {encoded_super_user_token}"},
        json={
            "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            "members": members,
            "namespace_uid": CUSTOMER.get("uid"),
        },
    )
    assert response.status_code == status.HTTP_200_OK, "Admin user hasn't been added to superuser's group."
    assert response.json().get("members_with_status") == [
        {**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members
    ]


async def remove_admin_from_superuser_access_group(async_client: AsyncClient, encoded_super_user_token: str) -> None:
    response = await async_client.post(
        "/v2/access-groups/delete_members",
        headers={"Authorization": f"Basic {encoded_super_user_token}"},
        json={
            "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            "members": [{"user_id": USER_ADMIN_ID}],
        },
    )
    assert response.status_code == status.HTTP_200_OK, "Admin hasn't been removed from superuser's access group."


@pytest_asyncio.fixture(scope="function")
async def create_and_get_and_delete_and_create_kitchen_by_admin(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> str:
    postgres_db = service_locator.service_provider.postgres_db
    _ = create_and_get_and_delete_and_create_kitchen_by_superuser
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, _ = create_users

        # adding admin to superuser's access group just to store group creation permission somewhere
        await upsert_admin_to_superuser_with_create_permission(
            async_client=ac, encoded_super_user_token=encoded_super_user_token
        )

        # posting an access group; not specifying a namespace in this request
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert (
            response.status_code == status.HTTP_406_NOT_ACCEPTABLE
        ), "Admin's access group has been created, while it should not happen."

        # setting target namespace explicitly here
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "namespace_uid": CUSTOMER.get("uid"),
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_201_CREATED, "Admin's access group hasn't been created."
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_ADMIN_WITH_UID = copy.deepcopy(TEST_KITCHEN_BY_ADMIN)
        TEST_KITCHEN_BY_ADMIN_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_ADMIN_WITH_UID,
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # now we can remove admin from superuser's access group
        await remove_admin_from_superuser_access_group(
            async_client=ac, encoded_super_user_token=encoded_super_user_token
        )

        # this can work only once we have a default group set for this user,
        # since that's how we get target namespace when it is not specified in the request
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                }
            },
        )
        assert (
            response.status_code == status.HTTP_404_NOT_FOUND
        ), "Admin's access group has been received without namespace set explicitly."

        # setting origin namespace explicitly
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                },
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group hasn't been received."
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_ADMIN.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_ADMIN.get("location")

        # add this access group as a default one for admin user
        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid")
        assert response.json().get("status") is True

        # try adding a random non-existent one
        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": str(uuid.uuid4()),
            },
        )
        assert response.status_code == status.HTTP_406_NOT_ACCEPTABLE

        # checking if we can get admin's access group
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group hasn't been received."
        assert response.json().get("access_group").get("name") == TEST_KITCHEN_BY_ADMIN.get("name")
        assert response.json().get("access_group").get("location") == TEST_KITCHEN_BY_ADMIN.get("location")

        # checking if we can get admin's access group memberlist
        response = await ac.post(
            "/v2/access-groups/get_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group memberlist hasn't been received."
        assert response.json()[0].get("permissions").get(UserPermissionsEnum.add_users_to_group) is True

        # deleting admin's access group
        response = await ac.post(
            "/v2/access-groups/delete_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")}},
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group hasn't been deleted."

        # checking if admin's access group is deleted indeed
        response = await ac.post(
            "/v2/access-groups/get_access_group_by_id",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {
                    "xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                },
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND, "Admin's access group hasn't been deleted"
        assert response.json() == {"detail": f"Group {TEST_KITCHEN_BY_ADMIN.get('xid')} does not exist."}

        # testing this request with specifying namespace
        response = await ac.post(
            "/v2/access-groups/delete_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND, "Admin's access group hasn't been deleted."

        # trying to create a child access group via admin user,
        # which is impossible since the user is not a member of any group
        # and, therefore, doesn't have "Create new access groups" permission stored anywhere
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN_WITH_UID,
                "namespace_uid": CUSTOMER.get("uid"),
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            "Admin user has created a child access group, " "while they should not be able to do so."
        )

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED, (
            "Admin user has not created an independent access group, " "while they should be able to do so."
        )
        assert response.json().get("access_group", {}).get("uid") is not None
        TEST_KITCHEN_BY_ADMIN_WITH_UID["uid"] = response.json().get("access_group", {}).get("uid")
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_ADMIN_WITH_UID,
            "namespace_uid": CUSTOMER.get("uid"),
        }

        # add this access group as a default one for admin user
        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid")
        assert response.json().get("status") is True

        # testing PUT request method
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "namespace_uid": CUSTOMER.get("uid"),
                "parent_access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Admin's access group has not been updated."
        assert response.json() == {
            "access_group": TEST_KITCHEN_BY_ADMIN_WITH_UID,
            "namespace_uid": CUSTOMER.get("uid"),
        }

        return TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid")


@pytest_asyncio.fixture(scope="function")
async def create_kitchens_by_admin_and_superuser(
    app: FastAPIReturn, create_users: Tuple[bytes, bytes, bytes]
) -> Tuple[bytes, str, bytes, str]:
    return await create_kitchens_by_admin_and_superuser_common(app=app, create_users=create_users)


@pytest_asyncio.fixture(scope="module")
async def create_kitchens_by_admin_and_superuser_module(
    app_module: FastAPIReturn, create_users_module: Tuple[bytes, bytes, bytes]
) -> Tuple[bytes, str, bytes, str]:
    return await create_kitchens_by_admin_and_superuser_common(app=app_module, create_users=create_users_module)


async def create_kitchens_by_admin_and_superuser_common(
    app: FastAPIReturn, create_users: Tuple[bytes, bytes, bytes]
) -> Tuple[bytes, str, bytes, str]:
    encoded_super_user_token, encoded_admin_user_token, _ = create_users
    async with AsyncClient(app=app, base_url="http://localhost:8040") as ac:
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        superuser_access_group_uid = response.json().get("access_group", {}).get("uid")

        # add this access group as a default one for superuser
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"user": USER_SUPER, "legacy_api_default_access_group_uid": superuser_access_group_uid},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == superuser_access_group_uid
        assert response.json().get("status") is True

        # adding admin to superuser's access group just to store group creation permission somewhere
        await upsert_admin_to_superuser_with_create_permission(
            async_client=ac, encoded_super_user_token=encoded_super_user_token
        )

        # setting target namespace explicitly here
        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_ADMIN,
                "namespace_uid": CUSTOMER.get("uid"),
                "parent_access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_201_CREATED, "Admin's access group hasn't been created."
        admin_access_group_uid = response.json().get("access_group", {}).get("uid")

        TEST_KITCHEN_BY_ADMIN_WITH_UID = copy.deepcopy(TEST_KITCHEN_BY_ADMIN)
        TEST_KITCHEN_BY_ADMIN_WITH_UID["uid"] = admin_access_group_uid

        # add this access group as a default one for admin user
        response = await ac.put(
            f"/v2/users/{USER_ADMIN_ID}/",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "user": USER_ADMIN,
                "legacy_api_default_access_group_uid": TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == TEST_KITCHEN_BY_ADMIN_WITH_UID.get("uid")
        assert response.json().get("status") is True

        # now we can remove admin from superuser's access group
        await remove_admin_from_superuser_access_group(
            async_client=ac, encoded_super_user_token=encoded_super_user_token
        )

        return encoded_super_user_token, superuser_access_group_uid, encoded_admin_user_token, admin_access_group_uid


ECOTRANSIT_RESPONSES_FOLDER = "ecotransit_responses"
SAMPLE_JSONS_DIR = os.path.join(
    os.path.dirname(__file__),
    "..",
    "..",
    "..",
    "core",
    "core",
    "tests",
    "sample_jsons",
)


@pytest.fixture(scope="package", autouse=True)
def geolocation_api_mock(request: SubRequest) -> None:
    """Mocks the Google geolocation API to return a fixed result."""

    def mocked_requests_get(url: str) -> Response:
        url_to_check = url.lower()

        if (
            "test_admin_location" in url_to_check
            or "test_location" in url_to_check
            or "schweiz" in url_to_check
            or "paris" in url_to_check
        ):
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "zurich_google_maps_query_response.json",
            )
        elif "spain" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "spain_google_maps_query_response.json",
            )
        elif "netherlands" in url_to_check:
            json_sample_path = os.path.join(
                SAMPLE_JSONS_DIR,
                "netherlands_google_maps_query_response.json",
            )
        else:
            raise NotImplementedError(f"Mocking not implemented for {url}")

        with open(json_sample_path) as response_json_sample_file:
            resp_json = json.load(response_json_sample_file)

        mock_response = Response(status_code=200, content=json.dumps(resp_json).encode("utf-8"))

        return mock_response

    print("Patching 'gap_filling_modules.location_gfm._google_api_lookup'")
    patched = mock.patch("gap_filling_modules.location_gfm._google_api_lookup", new=mocked_requests_get)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'gap_filling_modules.location_gfm._google_api_lookup'")

    request.addfinalizer(unpatch)


XML_FILES_MAPPING = {
    "wsdl": "wsdl.xml",
    "wsdl=1": "wsdl_1.xml",
    "xsd=1": "xsd_1.xml",
    "xsd=2": "xsd_2.xml",
}


@pytest.fixture(scope="package", autouse=True)
def ecotransit_wsdl_call_mock(request: SubRequest) -> None:
    """Mocks the EcoTransIT WSDL call to return a fixed XML schema file."""

    def mocked_wsdls_get(self: None, url: str) -> bytes:
        _ = self
        url_to_check = url.lower()
        schema_file_type = url_to_check.split("?")[-1]

        if schema_file_type not in XML_FILES_MAPPING:
            raise NotImplementedError(f"WSDL load mocking not implemented for {url}")

        xml_sample_path = os.path.join(
            SAMPLE_JSONS_DIR,
            XML_FILES_MAPPING[schema_file_type],
        )

        with open(xml_sample_path, "r") as response_xml_sample_file:
            mock_xml = bytes(response_xml_sample_file.read().encode("utf-8"))

        return mock_xml

    print("Patching 'zeep.transports.Transport._load_remote_data'")
    patched = mock.patch("zeep.transports.Transport._load_remote_data", new=mocked_wsdls_get)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'zeep.transports.Transport._load_remote_data'")

    request.addfinalizer(unpatch)


@pytest.fixture(scope="package", autouse=True)
def ecotransit_call_mock(request: SubRequest) -> None:
    """Mocks the EcoTransIT API call to return a fixed result in XML format."""

    def mocked_ecotransit_send_request(self: None, address: str, message: bytes, headers: dict) -> Response:
        _ = self
        _ = address
        _ = headers
        departure = {}
        destination = {}
        mode = ""

        parsed_message = etree.fromstring(message)
        root = parsed_message.getroottree().getroot()

        for stuff in root.iter():
            if "wgs84" in stuff.tag:
                if stuff.prefix == "ns1":
                    # departure coords
                    departure = dict(stuff.attrib)
                elif stuff.prefix == "ns2":
                    # destination coords
                    destination = dict(stuff.attrib)

            elif stuff.prefix == "ns3":
                if "air" in stuff.tag:
                    mode = "air"
                elif "sea" in stuff.tag:
                    mode = "sea"
                elif "road" in stuff.tag:
                    mode = "road"

            if mode:
                break

        sample_response_xml_file = (
            f"{departure.get('longitude')}_{departure.get('latitude')}_"
            f"{destination.get('longitude')}_{destination.get('latitude')}_{mode}.xml"
        )
        xml_sample_path = os.path.join(
            SAMPLE_JSONS_DIR,
            ECOTRANSIT_RESPONSES_FOLDER,
            sample_response_xml_file,
        )

        if not os.path.isfile(xml_sample_path):
            print(f"WARNING: {xml_sample_path} does not exist. Return 404 status.")
            return Response(status_code=404)

        with open(xml_sample_path, "r") as response_xml_sample_file:
            mock_response = Response(status_code=200, content=bytes(response_xml_sample_file.read().encode("utf-8")))

        return mock_response

    print("Patching 'zeep.transports.Transport.post'")
    patched = mock.patch("zeep.transports.Transport.post", new=mocked_ecotransit_send_request)
    patched.__enter__()

    def unpatch() -> None:
        patched.__exit__(None, None, None)
        print("Patching complete. Unpatching 'zeep.transports.Transport.post'")

    request.addfinalizer(unpatch)


@dataclasses.dataclass
class MockedState:
    is_superuser: bool = True


@dataclasses.dataclass
class MockedRequest:
    state: MockedState


@pytest_asyncio.fixture(scope="function")
async def setup_batch_items(
    app: FastAPIReturn,
) -> tuple[ServiceProvider, AccessGroupController, NodeController, MockedRequest, list[BatchInputDto], list[list]]:
    _ = app
    await service_locator.service_provider.postgres_db.connect(schema="test_pg")
    await service_locator.service_provider.postgres_db.reset_db(schema="test_pg")

    node_controller = get_node_controller()
    access_controller = get_access_controller()

    default_namespace = settings.EATERNITY_NAMESPACE_UUID
    access_group_uid = uuid.UUID(
        (await access_controller.access_service.get_all_access_group_ids_by_namespace(default_namespace))[0].uid
    )

    sub_recipe_uids = []
    recipe_uids = []
    n_subrecipes = 5
    n_recipes = 2

    for _ in range(n_subrecipes):
        sub_recipe_uids.append(uuid.uuid4())

    for _ in range(n_recipes):
        recipe_uids.append(uuid.uuid4())

    # TODO: we should instead use the same parent recipe uid for all batches, but then the test can fail randomly
    #  because the advisory lock is not over the full insert of nodes and edges
    # parent_recipe_uid = uuid.uuid4()
    # for _ in range(n_recipes):
    #     recipe_uids.append(parent_recipe_uid)

    sub_recipe_uids = sorted(sub_recipe_uids, key=lambda x: x.int)
    recipe_uids = sorted(recipe_uids, key=lambda x: x.int)

    potato_uid = uuid.uuid4()
    onion_uid = uuid.uuid4()
    potato = FoodProductFlowNode(
        product_name=[{"language": "de", "value": "Kartoffeln"}],
        access_group_uid=access_group_uid,
        link_to_sub_node=LinkToUidProp(uid=potato_uid),
    )
    onion = FoodProductFlowNode(
        product_name=[{"language": "de", "value": "Zwiebeln"}],
        access_group_uid=access_group_uid,
        link_to_sub_node=LinkToUidProp(uid=onion_uid),
    )

    sub_recipe_batch_items = [
        Calculation(
            input_root=RootWithSubFlowsDto(
                activity=FoodProcessingActivityNode(uid=sub_recipe_uid, access_group_uid=access_group_uid),
                sub_flows=[copy.deepcopy(potato), copy.deepcopy(onion)],
            ),
            namespace_uid=settings.EATERNITY_NAMESPACE_UUID,
        )
        for sub_recipe_uid in sub_recipe_uids
    ]

    linking_batch_items = []

    for linking_name, linking_uid in ((potato, potato_uid), (onion, onion_uid)):
        linking_batch_items.append(
            Calculation(
                input_root=RootWithSubFlowsDto(
                    activity=LinkingActivityNode(uid=linking_uid, access_group_uid=access_group_uid),
                    sub_flows=[
                        FoodProductFlowNode(
                            product_name=copy.deepcopy(linking_name.product_name), access_group_uid=access_group_uid
                        )
                    ],
                ),
                namespace_uid=settings.EATERNITY_NAMESPACE_UUID,
            )
        )

    def create_batch(recipe_index: int, sub_recipe_indices: list[int]) -> BatchInputDto:
        top_recipe = Calculation(
            input_root=RootWithSubFlowsDto(
                activity=FoodProcessingActivityNode(uid=recipe_uids[recipe_index], access_group_uid=access_group_uid),
                sub_flows=[
                    FoodProductFlowNode(
                        link_to_sub_node=LinkToUidProp(uid=sub_recipe_uids[index]), access_group_uid=access_group_uid
                    )
                    for index in sub_recipe_indices
                ],
            ),
            namespace_uid=settings.EATERNITY_NAMESPACE_UUID,
        )
        batch = [copy.deepcopy(top_recipe)]
        batch.extend([copy.deepcopy(sub_recipe_batch_items[i]) for i in sub_recipe_indices])
        batch.extend([copy.deepcopy(linking_batch_item) for linking_batch_item in linking_batch_items])
        return BatchInputDto(batch=batch)

    mocked_request = MockedRequest(MockedState())

    recipe_indices = [[0, 1, 2, 3], [3, 4, 1, 0]]

    batches = [create_batch(i, idx) for i, idx in enumerate(recipe_indices)]

    batch_sub_recipes_uid = [[sub_recipe_uids[i] for i in idx] for idx in recipe_indices]

    service_provider = service_locator.service_provider
    return service_provider, access_controller, node_controller, mocked_request, batches, batch_sub_recipes_uid


@pytest.fixture(scope="package", autouse=True)
def virtual_memory_lock() -> None:
    """Mocks virtual memory such that we do not accidentally exit tests because of lack of memory."""
    with mock.patch("psutil.virtual_memory") as mock_vm:
        mock_vm.return_value.percent = 30
        yield mock_vm


@pytest_asyncio.fixture(scope="module")
async def setup_recipes_and_subrecipes_for_superuser_and_admin(
    app_module: FastAPIReturn, create_kitchens_by_admin_and_superuser_module: Tuple[bytes, str, bytes, str]
) -> Tuple[bytes, str, bytes, str]:
    (
        encoded_super_user_token,
        superuser_access_group_uid,
        encoded_admin_user_token,
        admin_access_group_uid,
    ) = create_kitchens_by_admin_and_superuser_module

    test_recipe_with_new_root_flow = copy.deepcopy(TEST_RECIPE)
    test_recipe_with_new_root_flow[0]["input_root"]["flow"].update(
        {
            "amount_in_original_source_unit": {
                "value": 1.0 / TEST_RECIPE[0]["input_root"]["activity"]["recipe_portions"],
                "unit": "production amount",
            },
        }
    )

    test_recipe_with_sub_recipe_with_root_flow = copy.deepcopy(TEST_RECIPE_WITH_SUBRECIPE)
    test_recipe_with_sub_recipe_with_root_flow[0]["input_root"]["flow"].update(
        {
            "amount_in_original_source_unit": {
                "value": 1.0 / TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["recipe_portions"],
                "unit": "production amount",
            },
        }
    )

    # Add access_group_uids of superuser.
    test_recipe_superuser = change_in_every_activity_in_batch(
        test_recipe_with_new_root_flow, "access_group_uid", superuser_access_group_uid
    )
    del test_recipe_superuser[0]["input_root"]["activity"]["sold_portions"]
    test_recipe_with_subrecipe_superuser = change_in_every_activity_in_batch(
        test_recipe_with_sub_recipe_with_root_flow, "access_group_uid", superuser_access_group_uid
    )
    test_supply_superuser = change_in_every_activity_in_batch(
        TEST_SUPPLY, "access_group_uid", superuser_access_group_uid
    )

    # Add access_group_uids of admin and modify the xids (to avoid posting recipes with the same xid).
    def append_str_to_every_xid(batch: dict, string: str) -> dict:
        for batch_item in batch:
            if "xid" in batch_item["input_root"]["activity"]:
                batch_item["input_root"]["activity"]["xid"] += string
            if "flow" in batch_item["input_root"] and "xid" in batch_item["input_root"]["flow"]:
                batch_item["input_root"]["flow"]["xid"] += string

            for subflow in batch_item["input_root"]["sub_flows"]:
                if "xid" in subflow:
                    subflow["xid"] += string
        return batch

    test_recipe_admin = change_in_every_activity_in_batch(
        test_recipe_with_new_root_flow, "access_group_uid", admin_access_group_uid
    )
    test_recipe_admin = append_str_to_every_xid(test_recipe_admin, "_admin")
    test_recipe_with_subrecipe_admin = change_in_every_activity_in_batch(
        test_recipe_with_sub_recipe_with_root_flow, "access_group_uid", admin_access_group_uid
    )
    test_recipe_with_subrecipe_admin = append_str_to_every_xid(test_recipe_with_subrecipe_admin, "_admin")
    test_supply_admin = change_in_every_activity_in_batch(TEST_SUPPLY, "access_group_uid", admin_access_group_uid)
    test_supply_admin = append_str_to_every_xid(test_supply_admin, "_admin")

    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Post superuser recipes and supplies.
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
        for batch in (test_recipe_superuser, test_recipe_with_subrecipe_superuser, test_supply_superuser):
            for batch_item in batch:
                if not batch_item["input_root"]["activity"]["xid"].startswith(TEST_RECIPE_WITH_SUBRECIPE_ID):
                    batch_item["cache_final_root"] = True
                    batch_item["return_data_as_table"] = True
                    batch_item[
                        "final_graph_table_criterion"
                    ] = ReportGenerationController.get_default_final_graph_table_criterion_for_reports().model_dump()

            response = await ac.post(
                "/v2/calculation/graphs",
                headers={"Authorization": f"Basic {encoded_super_user_token}"},
                json={"batch": batch},
            )

            check_response_codes_in_batch_calculation(response, 200)

        # Post admin recipes and supplies.
        for batch in (test_recipe_admin, test_recipe_with_subrecipe_admin, test_supply_admin):
            for batch_item in batch:
                if not batch_item["input_root"]["activity"]["xid"].startswith(TEST_RECIPE_WITH_SUBRECIPE_ID):
                    batch_item["cache_final_root"] = True
                    batch_item["return_data_as_table"] = True
                    batch_item[
                        "final_graph_table_criterion"
                    ] = ReportGenerationController.get_default_final_graph_table_criterion_for_reports().model_dump()

            response = await ac.post(
                "/v2/calculation/graphs",
                headers={"Authorization": f"Basic {encoded_admin_user_token}"},
                json={"batch": batch},
            )

            check_response_codes_in_batch_calculation(response, 200)

    return encoded_super_user_token, superuser_access_group_uid, encoded_admin_user_token, admin_access_group_uid
