"Definitions of recipes, tests of API endpoints for recipe CRUD."

# ruff: noqa: ARG001
import base64
import copy
import time
import uuid
from typing import Any, Dict, List, Tuple
from unittest import mock

import pytest
import pytest_asyncio
from fastapi import status
from httpx import AsyncClient
from structlog import get_logger

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.nodes.activity.linking_activity_node import LinkingActivityNode
from core.domain.nodes.activity.transport_activity_node import TransportActivityNode
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator

from .conftest import (
    CUSTOMER,
    TEST_KITCHEN_BY_ADMIN,
    TEST_KITCHEN_BY_SUPERUSER,
    TEST_RECIPE,
    TEST_RECIPE_FLOW_ID,
    TEST_RECIPE_ID,
    TEST_RECIPE_WITH_SUBRECIPE,
    TEST_RECIPE_WITH_SUBRECIPE_ID,
    USER_BASIC,
    USER_BASIC_ID,
    USER_SUPER,
    USER_SUPER_ID,
    FastAPIReturn,
    change_in_every_activity_in_batch,
    change_in_every_item_in_batch,
    check_mutation_log,
    check_response_codes_in_batch_calculation,
    find_quantity_value,
    is_close,
    settings,
)

logger = get_logger()
service_locator = ServiceLocator()

ONION_DRYING_UPSCALING = 8.181818181825
TEST_RECIPE_EXPECTED_CO2_WITHOUT_UPSCALING = 0.023166
TEST_RECIPE_EXPECTED_CO2_OF_ONION_TRANSPORT = 0.003896901769800001  # updated 16.04.24: added mocked transport fr->ch
TEST_RECIPE_EXPECTED_CO2 = (
    TEST_RECIPE_EXPECTED_CO2_WITHOUT_UPSCALING * ONION_DRYING_UPSCALING + TEST_RECIPE_EXPECTED_CO2_OF_ONION_TRANSPORT
)
TEST_RECIPE_EXPECTED_DFU_NO_DRYING = 0.04014796363636364
TEST_RECIPE_EXPECTED_DFU = 0.1132100114241322
TEST_RECIPE_EXPECTED_DFU_ING1 = 0.0238
TEST_RECIPE_EXPECTED_DFU_ING2 = TEST_RECIPE_EXPECTED_DFU - TEST_RECIPE_EXPECTED_DFU_ING1
TEST_RECIPE_EXPECTED_VITASCORE = 275.2189616644503
TEST_RECIPE_EXPECTED_VITASCORE_LEGACY = 253.52896166445038
TEST_RECIPE_EXPECTED_CO2_OF_TOMATO_INGREDIENT = 0.0
TEST_RECIPE_EXPECTED_CO2_OF_ONION_INGREDIENT = TEST_RECIPE_EXPECTED_CO2
TEST_RECIPE_EXPECTED_RAINFOREST_CRITICAL_PRODUCTS_AMOUNT_WITHOUT_UPSCALING = 7.8
TEST_RECIPE_EXPECTED_RAINFOREST_CRITICAL_PRODUCTS_AMOUNT = (
    TEST_RECIPE_EXPECTED_RAINFOREST_CRITICAL_PRODUCTS_AMOUNT_WITHOUT_UPSCALING * ONION_DRYING_UPSCALING
)

PROPS_UNUSED = ("processing", "packaging", "instructions", "titles", "date", "author", "id")

TEST_RECIPE_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_RECIPE[0]["input_root"]["activity"]["node_type"],
        "raw_input": {key: val for key, val in TEST_RECIPE[0]["input_root"]["activity"].items() if key in PROPS_UNUSED},
        **{
            key: val
            for key, val in TEST_RECIPE[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
        # transport is moved back to raw_input for activity nodes since no GFM uses transport on activity nodes.
    },
    "mutation_type": "AddNodeMutation",
}


@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_recipe_by_superuser(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> dict:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser

        # test if superuser can create a recipe (should be possible for any access_group_xid)
        TEST_RECIPE_WITH_XID = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
        TEST_RECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_XID, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_RECIPE_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )
        resp_raw_input = resp_node["raw_input"]

        assert resp_node["node_type"] == TEST_RECIPE[0]["input_root"]["activity"]["node_type"]
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert resp_raw_input["author"] == TEST_RECIPE[0]["input_root"]["activity"]["author"]

        # testing deletion
        node_xid_to_delete_xid = TEST_RECIPE_WITH_XID[0]["input_root"]["activity"]["xid"]
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": node_xid_to_delete_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    }
                }
            }
        ]
        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_RECIPE_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_RECIPE_MUTATION_LOG)
        saved_calculation = response.json()["batch"][0]

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "members": members,
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"), "uid": test_kitchen_by_superuser_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_superuser_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_superuser_uid
        assert response.json().get("status") is True

        # and check if they can access this recipe
        node_xid_to_access_xid = TEST_RECIPE_WITH_XID[0]["input_root"]["activity"]["xid"]
        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": node_xid_to_access_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )
        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        # and if they can do the same by specifying the root-flow-id
        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_FLOW_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )
        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        # check if listing of recipe works from all possible endpoints
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                }
            ],
        )
        assert response.status_code == status.HTTP_200_OK
        nodes = response.json()
        assert len(nodes) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                }
            ],
        )
        assert response.status_code == status.HTTP_200_OK
        nodes = response.json()
        assert len(nodes) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=[
                {
                    "access_group": {"uid": test_kitchen_by_superuser_uid},
                }
            ],
        )
        assert response.status_code == status.HTTP_200_OK
        nodes = response.json()
        assert len(nodes) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        nodes = response.json()
        assert len(nodes) == 4

        # check if admin can't access listing of superuser's recipe
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json=[
                {
                    "access_group": {"uid": test_kitchen_by_superuser_uid},
                }
            ],
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Check if superuser can retrieve calculation
        response = await ac.get(
            f"/v2/calculation/graphs/{saved_calculation.get('uid')}",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        saved_calculation_without_root_node_uid = copy.deepcopy(saved_calculation)
        saved_calculation_without_root_node_uid[
            "root_node_uid"
        ] = None  # root node uid is not persisted (only child_of_root_node_uid is).
        assert response.json() == saved_calculation_without_root_node_uid

        # Verify if admin cannot retrieve calculation
        response = await ac.get(
            f"/v2/calculation/graphs/{saved_calculation.get('uid')}",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        return test_recipe_post_response


@pytest.mark.asyncio
async def test_create_delete_and_get_recipe_by_superuser(create_delete_and_get_recipe_by_superuser: dict) -> None:
    test_recipe_post_response = create_delete_and_get_recipe_by_superuser
    batch = test_recipe_post_response.get("batch")
    resp_node = batch[0]["final_root"]["activity"]
    assert is_close(
        find_quantity_value(
            resp_node["impact_assessment"]["amount_for_activity_production_amount"], "IPCC 2013 climate change GWP 100a"
        ),
        TEST_RECIPE_EXPECTED_CO2,
    )


@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_recipe_by_admin(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> str:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        # test if admin can create a recipe (should be possible for any access_group_xid)
        TEST_RECIPE_WITH_XID = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
        TEST_RECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_XID, "access_group_xid", TEST_KITCHEN_BY_ADMIN.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_RECIPE_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )
        resp_raw_input = resp_node["raw_input"]

        assert resp_node["node_type"] == TEST_RECIPE[0]["input_root"]["activity"]["node_type"]
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert resp_raw_input["author"] == TEST_RECIPE[0]["input_root"]["activity"]["author"]

        # testing deletion
        # making sure that an admin can't delete a resource that's not from their group
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_RECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 404)

        # test that the admin can delete a resource from their group
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_RECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: check that it was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_RECIPE_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_RECIPE_MUTATION_LOG)

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK, "adding read permission to user should result in status 200"
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid"), "uid": test_kitchen_by_admin_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_admin_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_admin_uid
        assert response.json().get("status") is True

        # and check if they can access this recipe
        # by specifying the root-flow-id
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_FLOW_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        # and by specifying the root-activity-id
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        assert response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid") is not None
        return response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid")


# this fixture has to be separated so that there's no conflict
# during `test_get_..._node_types_from_access_group` test setup
@pytest_asyncio.fixture(scope="function")
async def check_admin_recipe_listing(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_delete_and_get_recipe_by_admin: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    _ = create_delete_and_get_recipe_by_admin
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        # check if listing of recipe works from all possible endpoints
        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                }
            ],
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                }
            ],
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=[
                {
                    "access_group": {"uid": test_kitchen_by_admin_uid},
                }
            ],
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 4

        response = await ac.post(
            "/v2/nodes/get_nodes",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()) == 4


@pytest.mark.asyncio
async def test_create_delete_and_get_recipe_by_admin(check_admin_recipe_listing: None) -> None:
    _ = check_admin_recipe_listing


TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_BARE_ONION = 0.005080263157894738
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_ONION_UPSCALED = (
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_BARE_ONION * ONION_DRYING_UPSCALING
)
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_ONION_TRANSPORT = 0.0008545837214473687

TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_GREENHOUSE_CONTRIBUTION = 0.0464498462699531
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_TRANSPORT = 0.003896901769800002
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_COOLING_OF_TRANSPORT = 0.0002638025668055555

TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 = (
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_ONION_UPSCALED
    + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_ONION_TRANSPORT
    + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_TRANSPORT
    + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_COOLING_OF_TRANSPORT
    + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_GREENHOUSE_CONTRIBUTION
)
# 14.2.: value changed from 0.00508026315 to 0.05153010942784783 due to added greenhouse for the tomato in FR
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU = 0.03716871226434797
# update 15.11.2024: extended the vita-score (energy, fat & protein)
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_VITASCORE = 268.4757307985283
TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_VITASCORE_LEGACY = 246.7857307985283
TEST_RECIPE_WITH_SUBRECIPE_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["node_type"],
        "raw_input": {
            key: val
            for key, val in TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"].items()
            if key in PROPS_UNUSED
        },
        **{
            key: val
            for key, val in TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
    },
    "mutation_type": "AddNodeMutation",
}
# remove "raw_input" if it is a empty dict
if not TEST_RECIPE_WITH_SUBRECIPE_MUTATION_LOG["new_node"]["raw_input"]:
    del TEST_RECIPE_WITH_SUBRECIPE_MUTATION_LOG["new_node"]["raw_input"]


@pytest.mark.asyncio
async def test_create_delete_and_get_subrecipe_by_superuser(
    app: FastAPIReturn, create_delete_and_get_recipe_by_superuser: dict, create_users: Tuple[bytes, bytes, bytes]
) -> None:
    _ = create_delete_and_get_recipe_by_superuser
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, encoded_basic_user_token = create_users

        # test if superuser can create a recipe (should be possible for any access_group_xid)
        TEST_RECIPE_WITH_SUBRECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE, "xid", TEST_RECIPE_WITH_SUBRECIPE_ID
        )
        TEST_RECIPE_WITH_SUBRECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE_WITH_XID, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_RECIPE_WITH_SUBRECIPE_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]

        assert resp_node["node_type"] == TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["node_type"]
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert (
            resp_node["activity_location"][0]["source_data_raw"]
            == TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["activity_location"]
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        # testing deletion
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_RECIPE_WITH_SUBRECIPE_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_RECIPE_WITH_SUBRECIPE_MUTATION_LOG)

        # check if regular user can access this subrecipe
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )


# this test is encapsulated into a fixture
# for using the final state in `test_get_..._node_types_from_access_group` tests
@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_subrecipe_by_admin(
    app: FastAPIReturn, create_delete_and_get_recipe_by_admin: str, create_users: Tuple[bytes, bytes, bytes]
) -> None:
    _ = create_delete_and_get_recipe_by_admin
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users

        # test if superuser can create a recipe (should be possible for any access_group_xid)
        TEST_RECIPE_WITH_SUBRECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE, "xid", TEST_RECIPE_WITH_SUBRECIPE_ID
        )
        TEST_RECIPE_WITH_SUBRECIPE_WITH_XID = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE_WITH_XID, "access_group_xid", TEST_KITCHEN_BY_ADMIN.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_RECIPE_WITH_SUBRECIPE_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]

        assert resp_node["node_type"] == TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["node_type"]
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert (
            resp_node["activity_location"][0]["source_data_raw"]
            == TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["activity_location"]
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        # testing deletion
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_RECIPE_WITH_SUBRECIPE_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_RECIPE_WITH_SUBRECIPE_MUTATION_LOG)

        # check if regular user can access this subrecipe
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        assert response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid") is not None
        return response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid")


@pytest.mark.asyncio
async def test_create_delete_and_get_subrecipe_by_admin(create_delete_and_get_subrecipe_by_admin: None) -> None:
    _ = create_delete_and_get_subrecipe_by_admin


TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW_ID = "test_recipe_with_subrecipe_with_link_to_sub_flow"


TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW = [
    {
        "input_root": {
            "activity": {
                "xid": TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW_ID,
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_location": "Zürich Schweiz",
                "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
                "activity_date": "2023-06-20",
                "recipe_portions": 140,
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "488533744",
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "link_to_sub_node": {"xid": TEST_RECIPE_FLOW_ID},
                    "raw_transport": "air",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
                    "amount_in_original_source_unit": {"value": 50, "unit": "gram"},
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "242342343",
                    "raw_production": {"value": "organic", "language": "en"},
                    "raw_transport": "ground",
                    "flow_location": "france",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "amount_in_original_source_unit": {"value": 78, "unit": "gram"},
                    "processing": "",
                    "raw_conservation": {"value": "dried", "language": "en"},
                    "packaging": "",
                },
            ],
        }
    },
]


TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["node_type"],
        "raw_input": {
            key: val
            for key, val in TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"].items()
            if key in PROPS_UNUSED
        },
        **{
            key: val
            for key, val in TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
    },
    "mutation_type": "AddNodeMutation",
}


@pytest.mark.asyncio
async def test_create_delete_and_get_subrecipe_with_link_to_sub_flow_by_superuser(
    app: FastAPIReturn, create_delete_and_get_recipe_by_superuser: dict, create_users: Tuple[bytes, bytes, bytes]
) -> None:
    _ = create_delete_and_get_recipe_by_superuser
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, encoded_basic_user_token = create_users

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]

        assert (
            resp_node["node_type"]
            == TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW[0]["input_root"]["activity"]["node_type"]
        )
        assert len(batch[0]["final_root"].get("sub_flows")) == 2
        assert (
            resp_node["activity_location"][0]["source_data_raw"]
            == TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_FLOW[0]["input_root"]["activity"]["activity_location"]
        )

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2
            - TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_GREENHOUSE_CONTRIBUTION  # Tomato is dried: No greenhouse.
            - TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2_TOMATO_COOLING_OF_TRANSPORT,  # Transport doesn't need cooling
        )


@pytest.mark.asyncio
async def test_transient_calculations(
    app: FastAPIReturn,
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
        # not specifying a namespace here

        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") is None
        assert response.json().get("status") is True
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                    "uid": CUSTOMER.get("uid"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED

        TEST_RECIPE_TRANSIENT = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
        TEST_RECIPE_TRANSIENT = change_in_every_activity_in_batch(
            TEST_RECIPE_TRANSIENT, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )
        TEST_RECIPE_TRANSIENT[0]["transient"] = True
        TEST_RECIPE_TRANSIENT[0]["namespace_uid"] = CUSTOMER.get("uid")

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_RECIPE_TRANSIENT},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2,
        )

        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "namespace_uid": CUSTOMER.get("uid"),
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        batch = response.json().get("batch")
        assert batch[0]["final_root"] is None  # Not found because it is transient.


def get_linking_node_batch(linking_node_xid: str, link_to_sub_node_uid: str) -> tuple[list, list]:
    linking_node_batch = [
        {
            "input_root": {
                "activity": {
                    "xid": linking_node_xid,
                    "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    "node_type": FoodProcessingActivityNode.__name__,
                },
                "sub_flows": [
                    {
                        "node_type": FoodProductFlowNode.__name__,
                        "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
                        "link_to_sub_node": {"uid": link_to_sub_node_uid},
                    },
                ],
            },
            "skip_calc": True,
            "namespace_uid": CUSTOMER.get("uid"),
        },
    ]

    batch_to_access = [
        {
            "input_root": {
                "existing_root": {
                    "xid": linking_node_xid,
                    "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                },
            },
            "namespace_uid": CUSTOMER.get("uid"),
        }
    ]
    return linking_node_batch, batch_to_access


@pytest.mark.asyncio
async def test_reuse_xid(app: FastAPIReturn, get_simple_kitchen: str) -> None:
    encoded_super_user_token = get_simple_kitchen
    # Insert basic recipe
    test_recipe_1 = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
    test_recipe_1 = change_in_every_activity_in_batch(
        test_recipe_1, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
    )
    test_recipe_1[0]["namespace_uid"] = CUSTOMER.get("uid")
    test_recipe_1[0]["skip_calc"] = True

    async with AsyncClient(app=app, base_url="http://localhost:8040") as ac:
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": test_recipe_1},
        )
        inserted_node_uid = response.json()["batch"][0]["child_of_root_node_uid"]

        # Insert a new activity-flow pair with link to sub-node.
        linking_node_xid = "linking-node-xid"
        linking_node_batch, batch_to_access = get_linking_node_batch(linking_node_xid, inserted_node_uid)

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch},
        )
        assert response.status_code == 200

        linking_node_batch_with_conservation = copy.deepcopy(linking_node_batch)
        linking_node_batch_with_conservation[0]["input_root"]["activity"]["production_amount"] = {
            "unit": "gram",
            "value": 50,
        }
        linking_node_batch_with_conservation[0]["input_root"]["sub_flows"][0]["raw_conservation"] = {
            "value": "fresh",
            "language": "en",
        }
        linking_node_batch_with_conservation[0]["input_root"]["sub_flows"][0]["amount_in_original_source_unit"] = {
            "unit": "gram",
            "value": 50,
        }
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_conservation},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node")
        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node")["uid"]
            == inserted_node_uid
        )

        test_recipe_final = change_in_every_activity_in_batch(
            TEST_RECIPE_WITH_SUBRECIPE, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )
        test_recipe_final[0]["input_root"]["sub_flows"][0] = {
            "node_type": FoodProductFlowNode.__name__,
            "product_name": [{"language": "de", "value": "Kürbisrisotto"}],
            "amount_in_original_source_unit": {"unit": "gram", "value": 50},
            "raw_production": {"value": "greenhouse", "language": "en"},
            "raw_conservation": {"value": "fresh", "language": "en"},
            "flow_location": "spain",
            "raw_transport": "air",
            "link_to_sub_node": {"xid": linking_node_xid},
        }
        test_recipe_final[0]["namespace_uid"] = CUSTOMER.get("uid")

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": test_recipe_final},
        )
        assert response.status_code == 200
        assert is_close(
            find_quantity_value(
                response.json()["batch"][0]["final_root"]["activity"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        linking_node_batch_with_new_product_name = [
            {
                "input_root": {
                    "activity": {
                        "xid": linking_node_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                        "node_type": FoodProcessingActivityNode.__name__,
                        "production_amount": {"unit": "gram", "value": 50},
                    },
                    "sub_flows": [
                        {
                            "node_type": FoodProductFlowNode.__name__,
                            "product_name": [{"language": "de", "value": "Not Kürbisrisotto"}],
                            "amount_in_original_source_unit": {"unit": "gram", "value": 50},
                        },
                    ],
                },
                "skip_calc": True,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_new_product_name},
        )
        assert response.status_code == 200

        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": linking_node_xid,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "namespace_uid": CUSTOMER.get("uid"),
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        # The product name changed, and the link_to_sub_node should also be 'erased.'
        assert not response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node")


async def add_basic_recipes(app: FastAPIReturn, encoded_super_user_token: str) -> list[str]:
    test_recipe_1 = change_in_every_activity_in_batch(TEST_RECIPE, "xid", TEST_RECIPE_ID)
    test_recipe_1 = change_in_every_activity_in_batch(
        test_recipe_1, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
    )
    test_recipe_1[0]["namespace_uid"] = CUSTOMER.get("uid")
    test_recipe_1[0]["skip_calc"] = True

    # Insert a second basic recipe
    second_test_recipe_id = "second-test-recipe-id"
    test_recipe_2 = change_in_every_activity_in_batch(TEST_RECIPE, "xid", second_test_recipe_id)
    test_recipe_2 = change_in_every_activity_in_batch(
        test_recipe_2, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
    )
    test_recipe_2[0]["namespace_uid"] = CUSTOMER.get("uid")
    test_recipe_2[0]["skip_calc"] = True

    test_recipes = [test_recipe_1, test_recipe_2]
    inserted_node_uids = []

    async with AsyncClient(app=app, base_url="http://localhost:8040") as ac:
        for test_recipe in test_recipes:
            response = await ac.post(
                "/v2/calculation/graphs",
                headers={"Authorization": f"Basic {encoded_super_user_token}"},
                json={"batch": test_recipe},
            )
            inserted_node_uids.append(response.json()["batch"][0]["child_of_root_node_uid"])
    return inserted_node_uids


@pytest.mark.asyncio
async def test_reuse_xid_product_name(app: FastAPIReturn, get_simple_kitchen: str) -> None:
    """Tests to verify whether different combinations of product_name and link_to_sub_node lead to correct behaviour."""
    encoded_super_user_token = get_simple_kitchen
    # Insert basic recipes.
    inserted_node_uids = await add_basic_recipes(app, encoded_super_user_token)

    linking_node_xid = "linking-node-xid"
    linking_node_batch, batch_to_access = get_linking_node_batch(linking_node_xid, inserted_node_uids[0])

    async with AsyncClient(app=app, base_url="http://localhost:8040") as ac:
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch},
        )
        assert response.status_code == 200

        # 1) Try with no link_to_sub_node and matching name.
        #    Existing link_to_sub_node should be "imported" from the db.
        linking_node_batch_with_no_link_to_sub_node = copy.deepcopy(linking_node_batch)
        del linking_node_batch_with_no_link_to_sub_node[0]["input_root"]["sub_flows"][0]["link_to_sub_node"]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_no_link_to_sub_node},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node").get("uid")
            == inserted_node_uids[0]
        ), "The link_to_sub_node from the previous request should be used."

        # 2) Try with link_to_sub_node and matching name. link_to_sub_node should not be overwritten from the db.
        linking_node_batch_with_different_link_to_sub_node = copy.deepcopy(linking_node_batch)
        linking_node_batch_with_different_link_to_sub_node[0]["input_root"]["sub_flows"][0]["link_to_sub_node"][
            "uid"
        ] = inserted_node_uids[1]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_different_link_to_sub_node},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node").get("uid")
            == inserted_node_uids[1]
        ), "The link_to_sub_node should not be overwritten."

        # 3) Try with product name neither in original and second request.
        #    Existing link_to_sub_node should be "imported" from the db.
        linking_node_batch_with_no_product_name = copy.deepcopy(linking_node_batch)
        del linking_node_batch_with_no_product_name[0]["input_root"]["sub_flows"][0]["product_name"]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_no_product_name},
        )
        assert response.status_code == 200

        no_product_name_no_link_to_sub_node = copy.deepcopy(linking_node_batch_with_no_product_name)
        del no_product_name_no_link_to_sub_node[0]["input_root"]["sub_flows"][0]["link_to_sub_node"]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": no_product_name_no_link_to_sub_node},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node").get(
            "uid"
        ) == linking_node_batch_with_no_product_name[0]["input_root"]["sub_flows"][0].get("link_to_sub_node").get(
            "uid"
        ), "The link_to_sub_node from the previous request should be used."


@pytest.mark.asyncio
async def test_reuse_xid_ingredients_declaration(app: FastAPIReturn, get_simple_kitchen: str) -> None:
    """Tests to verify whether different combinations of product_name and link_to_sub_node lead to correct behaviour."""
    encoded_super_user_token = get_simple_kitchen
    # Insert basic recipes.
    inserted_node_uids = await add_basic_recipes(app, encoded_super_user_token)

    linking_node_xid = "linking-node-xid"
    linking_node_batch, batch_to_access = get_linking_node_batch(linking_node_xid, inserted_node_uids[0])
    linking_node_batch[0]["input_root"]["sub_flows"][0]["ingredients_declaration"] = [
        {
            "value": "Tomaten, Zwiebeln, Kartoffeln",
            "language": "de",
        }
    ]

    async with AsyncClient(app=app, base_url="http://localhost:8040") as ac:
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch},
        )
        assert response.status_code == 200

        # 1) Try with no link_to_sub_node and matching name and ingredients_declaration.
        # Existing link_to_sub_node should be "imported" from the db.
        linking_node_batch_with_no_link_to_sub_node = copy.deepcopy(linking_node_batch)
        del linking_node_batch_with_no_link_to_sub_node[0]["input_root"]["sub_flows"][0]["link_to_sub_node"]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_no_link_to_sub_node},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node").get("uid")
            == inserted_node_uids[0]
        ), "The link_to_sub_node from the previous request should be used."

        # 2) Try with no link_to_sub_node and matching name and no ingredients_declaration.
        # Existing link_to_sub_node should be "imported" from the db.
        linking_node_batch_with_no_ing_decl = copy.deepcopy(linking_node_batch)
        del linking_node_batch_with_no_ing_decl[0]["input_root"]["sub_flows"][0]["link_to_sub_node"]
        del linking_node_batch_with_no_ing_decl[0]["input_root"]["sub_flows"][0]["ingredients_declaration"]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_no_ing_decl},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][0].get("ingredients_declaration")
            == linking_node_batch[0]["input_root"]["sub_flows"][0]["ingredients_declaration"]
        ), "The ingredients_declaration from the previous request should be used."
        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node").get("uid")
            == inserted_node_uids[0]
        ), "The link_to_sub_node from the previous request should be used."

        # 3) Try with no link_to_sub_node and a non-matching name and no ingredients_declaration.
        # Existing link_to_sub_node and ingredient_declaration should not be "imported" from the db.
        linking_node_batch_with_new_name_and_dec = copy.deepcopy(linking_node_batch)
        del linking_node_batch_with_new_name_and_dec[0]["input_root"]["sub_flows"][0]["link_to_sub_node"]
        del linking_node_batch_with_new_name_and_dec[0]["input_root"]["sub_flows"][0]["ingredients_declaration"]
        linking_node_batch_with_new_name_and_dec[0]["input_root"]["sub_flows"][0]["product_name"] = [
            {"value": "new name", "language": "en"}
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_new_name_and_dec},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert not response.json()["batch"][0]["final_root"]["sub_flows"][0].get(
            "ingredients_declaration"
        ), "The ingredients_declaration from the previous request should not be used."
        assert not response.json()["batch"][0]["final_root"]["sub_flows"][0].get(
            "link_to_sub_node"
        ), "The link_to_sub_node from the previous request should not be used."

        # 4) Try with no link_to_sub_node and a new ingredients-declaration.
        # link_to_sub_node should not be read from the db.

        # First we need to resend the original recipe to make sure that the old matching is
        # reintroduced.
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch},
        )
        assert response.status_code == 200

        # Now we can check that indeed an unmatching happens
        linking_node_batch_with_different_ingredients_declaration = copy.deepcopy(linking_node_batch)
        del linking_node_batch_with_different_ingredients_declaration[0]["input_root"]["sub_flows"][0][
            "link_to_sub_node"
        ]
        linking_node_batch_with_different_ingredients_declaration[0]["input_root"]["sub_flows"][0][
            "ingredients_declaration"
        ] = [{"value": "Tomaten, Kartoffeln", "language": "de"}]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch_with_different_ingredients_declaration},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert not response.json()["batch"][0]["final_root"]["sub_flows"][0].get(
            "link_to_sub_node"
        ), "The link_to_sub_node should be overwritten."


@pytest.mark.asyncio
async def test_reuse_xid_ingredient_numbers(app: FastAPIReturn, get_simple_kitchen: str) -> None:
    """Tests to verify whether different numbers of ingredients lead to correct behaviour."""
    encoded_super_user_token = get_simple_kitchen
    # Insert basic recipes.
    inserted_node_uids = await add_basic_recipes(app, encoded_super_user_token)
    linking_node_xid = "linking-node-xid"
    linking_node_batch, batch_to_access = get_linking_node_batch(linking_node_xid, inserted_node_uids[0])

    async with AsyncClient(app=app, base_url="http://localhost:8040") as ac:
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch},
        )
        assert response.status_code == 200

        # 1) Try with no link_to_sub_node and matching name in the first ingredient, and additional ingredient.
        batch_with_two_ingredients = copy.deepcopy(linking_node_batch)
        del batch_with_two_ingredients[0]["input_root"]["sub_flows"][0]["link_to_sub_node"]
        batch_with_two_ingredients[0]["input_root"]["sub_flows"].append(
            {
                "node_type": FoodProductFlowNode.__name__,
                "product_name": [{"language": "de", "value": "Risotto"}],
                "link_to_sub_node": {"uid": inserted_node_uids[1]},
            }
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_two_ingredients},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )

        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][0].get("link_to_sub_node").get("uid")
            == inserted_node_uids[0]
        ), "The link_to_sub_node from the previous request should be used."
        assert (
            response.json()["batch"][0]["final_root"]["sub_flows"][1].get("link_to_sub_node").get("uid")
            == inserted_node_uids[1]
        ), "Correct link_to_sub_node should be used."

        batch_with_swapped_ingredients = copy.deepcopy(batch_with_two_ingredients)
        for i in range(2):
            batch_with_swapped_ingredients[0]["input_root"]["sub_flows"][i] = batch_with_two_ingredients[0][
                "input_root"
            ]["sub_flows"][(i + 1) % 2]
            if "link_to_sub_node" in batch_with_swapped_ingredients[0]["input_root"]["sub_flows"][i]:
                del batch_with_swapped_ingredients[0]["input_root"]["sub_flows"][i]["link_to_sub_node"]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_swapped_ingredients},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )
        for i in range(2):
            assert not (
                response.json()["batch"][0]["final_root"]["sub_flows"][i].get("link_to_sub_node")
            ), "The link_to_sub_node should not have been used from the previous request."

        # Try sending a new request with just one ingredient.
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": linking_node_batch},
        )
        assert response.status_code == 200

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )
        assert (
            len(response.json()["batch"][0]["final_root"]["sub_flows"]) == 1
        ), "Only one ingredient should be present."

        # Add unreasonable amounts of ingredients.
        batch_with_five_thousand_ingredients = copy.deepcopy(linking_node_batch)
        activity_uid = str(uuid.uuid4())
        batch_with_five_thousand_ingredients[0]["input_root"]["activity"]["uid"] = activity_uid
        for _ in range(1999):
            batch_with_five_thousand_ingredients[0]["input_root"]["sub_flows"].append(
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "product_name": [{"language": "de", "value": "Risotto"}],
                    "link_to_sub_node": {"uid": inserted_node_uids[1]},
                }
            )
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_five_thousand_ingredients},
        )
        assert response.status_code == 200

        # Check the database to see if 5000 nodes are added.
        edges = await service_locator.service_provider.postgres_db.get_graph_mgr().get_sub_graph_by_uid(
            uid=activity_uid, max_depth=1
        )
        assert len(edges) == 2000

        # Try with 10000 ingredients:
        batch_with_ten_thousand_ingredients = copy.deepcopy(batch_with_five_thousand_ingredients)
        for _ in range(999):
            batch_with_ten_thousand_ingredients[0]["input_root"]["sub_flows"].append(
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "product_name": [{"language": "de", "value": "Risotto"}],
                    "link_to_sub_node": {"uid": inserted_node_uids[1]},
                }
            )

        # enable postgres auto_explain:
        service_locator.service_provider.postgres_db.graph_mgr.analyze = True
        start_ns = time.perf_counter_ns()
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_ten_thousand_ingredients},
        )
        duration_sec = (time.perf_counter_ns() - start_ns) / 1e9
        logger.info(
            "Request with 3k ingredients updating old recipe with 2k ingredients completed",
            duration_sec=duration_sec,
        )
        assert response.json()["batch"][0]["statuscode"] == 200


TEST_DIAMOND_ID = "diamond-top-recipe"
TEST_DIAMOND_SUBRECIPE_1_ID = "diamond-sub-recipe-1"
TEST_DIAMOND_SUBRECIPE_2_ID = "diamond-sub-recipe-2"
TEST_DIAMOND_SUBSUBRECIPE_ID = "diamond-sub-sub-recipe"
TEST_DIAMOND_SUB3RECIPE_ID = "diamond-sub-sub-sub-recipe"
TEST_DIAMOND_LOCATION_ON_ROOT = [
    {
        "return_final_graph": True,
        "namespace_uid": CUSTOMER.get("uid"),
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_ID,
                "name": "Top Activity Node",
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_location": "Zürich Schweiz",
                "activity_date": "2023-06-20",
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUBRECIPE_1_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Kürbisrisotto 1"}],
                    "amount_in_original_source_unit": {"value": 50, "unit": "gram"},
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUBRECIPE_2_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Kürbisrisotto 2"}],
                    "amount_in_original_source_unit": {"value": 100, "unit": "gram"},
                },
            ],
        },
    },
    {
        "namespace_uid": CUSTOMER.get("uid"),
        "skip_calc": True,
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_SUBRECIPE_1_ID,
                "name": "SUBRECIPE 1",
                "node_type": FoodProcessingActivityNode.__name__,
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUBSUBRECIPE_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Common Subrecipe"}],
                    "amount_in_original_source_unit": {"value": 50, "unit": "gram"},
                },
            ],
        },
    },
    {
        "namespace_uid": CUSTOMER.get("uid"),
        "skip_calc": True,
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_SUBRECIPE_2_ID,
                "name": "SUBRECIPE 2",
                "node_type": FoodProcessingActivityNode.__name__,
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUBSUBRECIPE_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Common Subrecipe"}],
                    "amount_in_original_source_unit": {"value": 25, "unit": "gram"},
                },
            ],
        },
    },
    {
        "namespace_uid": CUSTOMER.get("uid"),
        "skip_calc": True,
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_SUBSUBRECIPE_ID,
                "name": "SUBSUBRECIPE",
                "node_type": FoodProcessingActivityNode.__name__,
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "link_to_sub_node": {"xid": TEST_DIAMOND_SUB3RECIPE_ID, "duplicate_sub_node": False},
                    "product_name": [{"language": "de", "value": "Common Subsubrecipe"}],
                    "amount_in_original_source_unit": {"value": 1000, "unit": "gram"},
                },
            ],
        },
    },
    {
        "namespace_uid": CUSTOMER.get("uid"),
        "skip_calc": True,
        "input_root": {
            "activity": {
                "xid": TEST_DIAMOND_SUB3RECIPE_ID,
                "name": "SUBSUBSUBRECIPE",
                "node_type": FoodProcessingActivityNode.__name__,
                "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "100100191",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "amount_in_original_source_unit": {"value": 400, "unit": "gram"},
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "100100894",
                    "product_name": [{"language": "de", "value": "Zwiebeln"}],
                    "amount_in_original_source_unit": {"value": 600, "unit": "gram"},
                },
            ],
        },
    },
]

TEST_DIAMOND_LOCATION_ON_BRANCHES = copy.deepcopy(TEST_DIAMOND_LOCATION_ON_ROOT)
TEST_DIAMOND_LOCATION_ON_BRANCHES[0]["input_root"]["sub_flows"][1]["flow_location"] = "Zürich Schweiz"
TEST_DIAMOND_LOCATION_ON_BRANCHES[1]["input_root"]["sub_flows"][0]["flow_location"] = "Zürich Schweiz"

TEST_DIAMOND_LOCATION_ON_LEAF = copy.deepcopy(TEST_DIAMOND_LOCATION_ON_ROOT)
TEST_DIAMOND_LOCATION_ON_LEAF[-2]["input_root"]["sub_flows"][0]["flow_location"] = "Zürich Schweiz"

TEST_DIAMOND_CONFLICTING_LOCATION_ON_BRANCHES = copy.deepcopy(TEST_DIAMOND_LOCATION_ON_ROOT)
TEST_DIAMOND_CONFLICTING_LOCATION_ON_BRANCHES[0]["input_root"]["sub_flows"][1]["flow_location"] = "france"
TEST_DIAMOND_CONFLICTING_LOCATION_ON_BRANCHES[1]["input_root"]["sub_flows"][0]["flow_location"] = "Zürich Schweiz"

EXPECTED_TOMATO_ORIGIN_AND_TRANSPORT_EXISTENCE = {
    "Italy": True,
    "Morocco": False,
    "Netherlands": True,
    "Spain": True,
    "Switzerland": True,
}
EXPECTED_ONION_ORIGIN_AND_TRANSPORT_EXISTENCE = {
    "Austria": False,
    "Belgium": False,
    "Brazil": False,
    "France": True,
    "Germany": False,
    "Italy": True,
    "Netherlands": True,
    "Spain": True,
    "Switzerland": True,
    "Colombia": False,
    "Poland": False,
}

DIAMOND_EXPECTED_CO2 = 0.04767813956654421


def get_sub_nodes_from_graph(graph: dict, node: dict) -> list[dict]:
    sub_node_uids = node.get("sub_node_uids")
    return [elem for elem in graph if elem.get("uid") in sub_node_uids]


def find_node_from_graph_by_xid(graph: dict, xid: str) -> dict:
    nodes_with_xid = [elem for elem in graph if elem.get("xid") == xid]
    assert len(nodes_with_xid) == 1
    return nodes_with_xid[0]


@pytest.mark.asyncio
async def test_diamond(
    app: FastAPIReturn,
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                    "uid": CUSTOMER.get("uid"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_DIAMOND_LOCATION_ON_ROOT},
        )

        response_json = response.json()
        # Testing that inheritance in Transport GFM has run correctly.

        def get_origins_and_transports_of_nth_ingredient(graph: dict, n: int) -> dict:
            """Finds all origin splits and if transport exists for them."""
            sub3_recipe = find_node_from_graph_by_xid(graph, TEST_DIAMOND_SUB3RECIPE_ID)
            ingredients = get_sub_nodes_from_graph(graph, sub3_recipe)
            ingredient = ingredients[n]

            origin_splits = get_sub_nodes_from_graph(graph, get_sub_nodes_from_graph(graph, ingredient)[0])

            return_dict = {}

            for origin_split in origin_splits:
                country_name = None
                for sub_flow in get_sub_nodes_from_graph(graph, get_sub_nodes_from_graph(graph, origin_split)[0]):
                    if "flow_location" in sub_flow:
                        country_name = sub_flow["flow_location"][0]["term_name"]
                        break

                assert country_name is not None

                if "Main carriage" in [
                    elem["product_name"].get("source_data_raw", [{}])[0].get("value")
                    for elem in get_sub_nodes_from_graph(graph, get_sub_nodes_from_graph(graph, origin_split)[0])
                ]:
                    transport_exists = True
                else:
                    transport_exists = False

                return_dict[country_name] = transport_exists

            return return_dict

        is_close(
            list(
                response_json["batch"][0]["final_root"]["activity"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ].values()
            )[0]["quantity"]["value"],
            DIAMOND_EXPECTED_CO2,
        )

        tomato_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 0)
        onion_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 1)

        assert tomato_origins == EXPECTED_TOMATO_ORIGIN_AND_TRANSPORT_EXISTENCE
        assert onion_origins == EXPECTED_ONION_ORIGIN_AND_TRANSPORT_EXISTENCE

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_DIAMOND_LOCATION_ON_BRANCHES},
        )

        response_json = response.json()

        is_close(
            list(
                response_json["batch"][0]["final_root"]["activity"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ].values()
            )[0]["quantity"]["value"],
            DIAMOND_EXPECTED_CO2,
        )

        tomato_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 0)
        onion_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 1)

        assert tomato_origins == EXPECTED_TOMATO_ORIGIN_AND_TRANSPORT_EXISTENCE
        assert onion_origins == EXPECTED_ONION_ORIGIN_AND_TRANSPORT_EXISTENCE

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_DIAMOND_LOCATION_ON_LEAF},
        )

        response_json = response.json()
        is_close(
            list(
                response_json["batch"][0]["final_root"]["activity"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ].values()
            )[0]["quantity"]["value"],
            DIAMOND_EXPECTED_CO2,
        )

        tomato_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 0)
        onion_origins = get_origins_and_transports_of_nth_ingredient(response_json["batch"][0]["final_graph"], 1)

        assert tomato_origins == EXPECTED_TOMATO_ORIGIN_AND_TRANSPORT_EXISTENCE
        assert onion_origins == EXPECTED_ONION_ORIGIN_AND_TRANSPORT_EXISTENCE

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_DIAMOND_CONFLICTING_LOCATION_ON_BRANCHES},
        )

        response_json = response.json()
        assert response_json["batch"][0]["final_root"]["activity"]["impact_assessment"] is None


@pytest.mark.asyncio
async def test_no_inheritance_conflicts_when_diamond_structure_only_below_modeled_activity_level(
    app: FastAPIReturn,
) -> None:
    """Check that matching to the same ModeledActivityNodes does not lead to inheritance conflicts.

    Below Modeled Activities, we can have diamond structures when two ingredients are matched to the same terms.
    However, we need to make sure that in our code we do not have inheritance conflicts because of these diamond
    structures. This test checks that we do not have inheritance conflicts after OriginGFM even if we
    have diamond structures below ModeledActivityNodes.
    """
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        recipe_with_identical_mono_products = copy.deepcopy(TEST_RECIPE)

        amount_ingredient_1 = TEST_RECIPE[0]["input_root"]["sub_flows"][0]["amount_in_original_source_unit"]["value"]
        amount_ingredient_2 = TEST_RECIPE[0]["input_root"]["sub_flows"][1]["amount_in_original_source_unit"]["value"]

        expected_co2_carrot_without_transport_per_gram = (
            TEST_RECIPE_EXPECTED_CO2_WITHOUT_UPSCALING / amount_ingredient_2
        )
        expected_co2_transport_carrot_france_per_gram = (
            TEST_RECIPE_EXPECTED_CO2_OF_ONION_TRANSPORT / amount_ingredient_2
        )
        expected_co2_transport_carrot_spain_per_gram = 0.00012212659899999998
        expected_co2_transport_carrot_netherlands_per_gram = 5.59465938000001e-05

        recipe_with_identical_mono_products[0]["input_root"]["sub_flows"][0] = copy.deepcopy(
            recipe_with_identical_mono_products[0]["input_root"]["sub_flows"][1]
        )
        recipe_with_identical_mono_products[0]["input_root"]["sub_flows"][0]["xid"] = "100100191"
        # this is in principal a conflicting location
        recipe_with_identical_mono_products[0]["input_root"]["sub_flows"][0]["flow_location"] = "ESP"
        recipe_with_identical_mono_products[0]["input_root"]["sub_flows"][1]["flow_location"] = "NLD"
        # delete the conservation such that we really test the "duplicate-node-mutation" in the OriginGFM
        del recipe_with_identical_mono_products[0]["input_root"]["sub_flows"][0]["raw_conservation"]
        del recipe_with_identical_mono_products[0]["input_root"]["sub_flows"][1]["raw_conservation"]
        recipe_with_identical_mono_products[0]["input_root"]["sub_flows"][0]["amount_in_original_source_unit"][
            "value"
        ] = amount_ingredient_1

        recipe_with_identical_mono_products = change_in_every_activity_in_batch(
            recipe_with_identical_mono_products, "xid", TEST_RECIPE_ID
        )
        recipe_with_identical_mono_products = change_in_every_activity_in_batch(
            recipe_with_identical_mono_products, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )
        recipe_with_identical_mono_products[0]["namespace_uid"] = CUSTOMER.get("uid")

        response = await ac.put(
            f"/v2/users/{USER_SUPER_ID}/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "user": USER_SUPER,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        encoded_super_user_token = response.json().get("auth_token")
        encoded_super_user_token = base64.b64encode(encoded_super_user_token.encode("utf-8")).decode("utf-8")

        response = await ac.put(
            f"/v2/batch/customers/{CUSTOMER.get('xid')}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "customer": {
                    "name": CUSTOMER.get("name"),
                    "uid": CUSTOMER.get("uid"),
                }
            },
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.post(
            "/v2/access-groups/upsert_access_group",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "access_group": TEST_KITCHEN_BY_SUPERUSER,
                "namespace_uid": CUSTOMER.get("uid"),
            },
        )
        assert response.status_code == status.HTTP_201_CREATED

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": recipe_with_identical_mono_products},
        )

        response_json = response.json()

        resp_node = response_json["batch"][0]["final_root"]["activity"]

        expected_co2 = (
            (amount_ingredient_1 + amount_ingredient_2) * expected_co2_carrot_without_transport_per_gram
            + expected_co2_transport_carrot_spain_per_gram * amount_ingredient_1
            + expected_co2_transport_carrot_netherlands_per_gram * amount_ingredient_2
        )

        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            expected_co2,
        )

        recipe_with_identical_combined_products = copy.deepcopy(recipe_with_identical_mono_products)
        recipe_with_identical_combined_products[0]["input_root"]["activity"]["xid"] = "combined-products"
        recipe_with_identical_combined_products[0]["input_root"]["sub_flows"][0]["xid"] = "sub_ingredient_1"
        recipe_with_identical_combined_products[0]["input_root"]["sub_flows"][1]["xid"] = "sub_ingredient_2"
        # add ingredients_declaration such  that they are interpreted as combined products and we can test the
        # combined_product_origin_processing
        recipe_with_identical_combined_products[0]["input_root"]["sub_flows"][0]["ingredients_declaration"] = [
            {
                "value": "Just here such that the product is treated as combined product",
                "language": "de",
            }
        ]
        recipe_with_identical_combined_products[0]["input_root"]["sub_flows"][1]["ingredients_declaration"] = [
            {
                "value": "Just here such that the product is treated as combined product",
                "language": "de",
            }
        ]
        # conflicting locations:
        recipe_with_identical_combined_products[0]["input_root"]["sub_flows"][0]["flow_location"] = "ESP, FRA"
        recipe_with_identical_combined_products[0]["input_root"]["sub_flows"][1]["flow_location"] = "ESP, NLD"
        # patch here such that we can test the combined_product_origin_processing (otherwise it tries and fails
        # to make an origin-split also on the attached onions)
        with mock.patch(
            "gap_filling_modules.ingredient_splitter_gfm.IngredientSplitterGapFillingWorker.run",
            return_value=0,
        ):
            response = await ac.post(
                "/v2/calculation/graphs",
                headers={"Authorization": f"Basic {encoded_super_user_token}"},
                json={"batch": recipe_with_identical_combined_products},
            )

        response_json = response.json()

        resp_node = response_json["batch"][0]["final_root"]["activity"]

        expected_co2 = (
            (amount_ingredient_1 + amount_ingredient_2) * expected_co2_carrot_without_transport_per_gram
            + expected_co2_transport_carrot_spain_per_gram * (0.5 * amount_ingredient_1 + 0.5 * amount_ingredient_2)
            + expected_co2_transport_carrot_france_per_gram * 0.5 * amount_ingredient_1
            + expected_co2_transport_carrot_netherlands_per_gram * 0.5 * amount_ingredient_2
        )

        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            expected_co2,
        )


# amount_in_original_source_unit of the flows below LinkingActivityNodes should be ignored
# since we want that MergeLinkingActivityNodeGFM always uses the amount_in_original_source_unit
# of the parent-flow.
TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE_EXPECTED_CO2 = 0.08413512552626104
TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE = [
    {
        "input_root": {
            "flow": {
                "node_type": FoodProductFlowNode.__name__,
                "nutrient_values": {
                    "energy_kcal": 200,
                    "fat_gram": 12.3,
                    "saturated_fat_gram": 2.5,
                    "carbohydrates_gram": 8.4,
                    "sucrose_gram": 3.2,
                    "protein_gram": 2.2,
                    "sodium_chloride_gram": 0.3,
                },
            },
            "activity": {
                "node_type": FoodProcessingActivityNode.__name__,
                "xid": "test_link_to_monoproducts_as_subrecipe",
                "activity_location": "Zürich Schweiz",
                "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
                "activity_date": "2023-06-20",
                "recipe_portions": 140,
                "production_amount": {"unit": "gram", "value": 50},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "488533744",
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "link_to_sub_node": {"xid": "carrot_ingredient_id"},
                    "transport": "ground",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Karotten 5stk"}],
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "242342343",
                    "raw_production": {"value": "organic", "language": "en"},
                    "link_to_sub_node": {"xid": "tomato_ingredient_id"},
                    "transport": "ground",
                    "flow_location": "france",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "processing": "",
                    "packaging": "",
                },
            ],
        },
        "return_final_graph": True,
    },
    {
        "input_root": {
            "activity": {
                "xid": "carrot_ingredient_id",
                "node_type": LinkingActivityNode.__name__,
                "production_amount": {"unit": "kilogram", "value": 1},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "115",
                    "product_name": [{"language": "de", "value": "Karotten"}, {"language": "en", "value": "carrots"}],
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 1},
                },
            ],
        },
        "skip_calc": True,
    },
    {
        "input_root": {
            "activity": {
                "xid": "tomato_ingredient_id",
                "node_type": LinkingActivityNode.__name__,
                "production_amount": {"unit": "kilogram", "value": 1},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "101",
                    "product_name": [{"language": "de", "value": "Tomaten"}, {"language": "en", "value": "tomato"}],
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 1},
                },
            ],
        },
        "skip_calc": True,
    },
]


TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE_WITH_ONE_MORE_LINKING_ACTIVITY_IN_BETWEEN = [
    {
        "input_root": {
            "flow": {
                "node_type": FoodProductFlowNode.__name__,
                "nutrient_values": {
                    "energy_kcal": 200,
                    "fat_gram": 12.3,
                    "saturated_fat_gram": 2.5,
                    "carbohydrates_gram": 8.4,
                    "sucrose_gram": 3.2,
                    "protein_gram": 2.2,
                    "sodium_chloride_gram": 0.3,
                },
            },
            "activity": {
                "node_type": FoodProcessingActivityNode.__name__,
                "xid": "test_link_to_monoproducts_as_subrecipe_with_one_more_linking",
                "activity_location": "Zürich Schweiz",
                "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
                "activity_date": "2023-06-20",
                "recipe_portions": 140,
                "production_amount": {"unit": "gram", "value": 50},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "488533744",
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "link_to_sub_node": {"xid": "carrot_ingredient_id_link"},
                    "transport": "ground",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Karotten 5stk"}],
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "242342343",
                    "raw_production": {"value": "organic", "language": "en"},
                    "link_to_sub_node": {"xid": "tomato_ingredient_id_link"},
                    "transport": "ground",
                    "flow_location": "france",
                    "product_name": [{"language": "de", "value": "Tomaten"}],
                    "processing": "",
                    "packaging": "",
                },
            ],
        },
        "return_final_graph": True,
    },
    {
        "input_root": {
            "activity": {
                "xid": "carrot_ingredient_id",
                "node_type": LinkingActivityNode.__name__,
                "production_amount": {"unit": "kilogram", "value": 1},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "115",
                    "product_name": [{"language": "de", "value": "Karotten"}, {"language": "en", "value": "carrots"}],
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 1},
                },
            ],
        },
        "skip_calc": True,
    },
    {
        "input_root": {
            "activity": {
                "xid": "tomato_ingredient_id",
                "node_type": LinkingActivityNode.__name__,
                "production_amount": {"unit": "kilogram", "value": 1},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "101",
                    "product_name": [{"language": "de", "value": "Tomaten"}, {"language": "en", "value": "tomato"}],
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 1},
                },
            ],
        },
        "skip_calc": True,
    },
    {
        "input_root": {
            "activity": {
                "xid": "carrot_ingredient_id_link",
                "node_type": LinkingActivityNode.__name__,
                "production_amount": {"unit": "kilogram", "value": 1},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 1},
                    "link_to_sub_node": {"xid": "carrot_ingredient_id"},
                },
            ],
        },
        "skip_calc": True,
    },
    {
        "input_root": {
            "activity": {
                "xid": "tomato_ingredient_id_link",
                "node_type": LinkingActivityNode.__name__,
                "production_amount": {"unit": "kilogram", "value": 1},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 1},
                    "link_to_sub_node": {"xid": "tomato_ingredient_id"},
                },
            ],
        },
        "skip_calc": True,
    },
]


@pytest.mark.parametrize(
    "test_recipe",
    [
        TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE,
        TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE_WITH_ONE_MORE_LINKING_ACTIVITY_IN_BETWEEN,
    ],
)
@pytest.mark.parametrize(
    "location_specification",
    [
        "in_root_recipe",
        "in_sub_recipe",
        "not_specified",
        "in_root_and_sub_recipe",
    ],
)
@pytest.mark.asyncio
async def test_post_subrecipe_with_link_to_monoproducts(
    app: FastAPIReturn,
    location_specification: str,
    test_recipe: List[Dict[str, Any]],
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, _ = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser
        recipe_including_subrecipe = copy.deepcopy(test_recipe)

        # test if superuser can create a recipe (should be possible for any access_group_xid)
        recipe_including_subrecipe = change_in_every_activity_in_batch(
            recipe_including_subrecipe, "access_group_uid", test_kitchen_by_superuser_uid
        )

        # Spain, France, Switzerland from the specification in TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE
        country_codes_specified_in_recipe = {"ES", "FR", "CH"}
        number_of_valid_transports_in_recipe = 2  # spain->zurich, france->zurich
        if location_specification == "in_root_recipe":
            expected_country_codes = country_codes_specified_in_recipe
            expected_number_of_transport_activities = number_of_valid_transports_in_recipe
            expected_co2 = TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE_EXPECTED_CO2
        elif location_specification == "in_sub_recipe":
            recipe_including_subrecipe[1]["input_root"]["sub_flows"][0]["flow_location"] = recipe_including_subrecipe[
                0
            ]["input_root"]["sub_flows"][0]["flow_location"]
            del recipe_including_subrecipe[0]["input_root"]["sub_flows"][0]["flow_location"]
            expected_country_codes = country_codes_specified_in_recipe
            expected_number_of_transport_activities = number_of_valid_transports_in_recipe
            expected_co2 = TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE_EXPECTED_CO2
        elif location_specification == "not_specified":
            del recipe_including_subrecipe[0]["input_root"]["sub_flows"][0]["flow_location"]
            expected_country_codes = country_codes_specified_in_recipe | {"NL", "IT", "DK", "PT"}
            additional_co2 = -0.0029328574226352433  # different transport routes to origin-split
            expected_number_of_transport_activities = number_of_valid_transports_in_recipe + 5
            expected_co2 = TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE_EXPECTED_CO2 + additional_co2
        elif location_specification == "in_root_and_sub_recipe":
            # additional location in the sub-recipe, we expect that this location is ignored.
            recipe_including_subrecipe[1]["input_root"]["sub_flows"][0]["flow_location"] = "netherlands"
            expected_country_codes = country_codes_specified_in_recipe
            expected_number_of_transport_activities = number_of_valid_transports_in_recipe
            expected_co2 = TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE_EXPECTED_CO2

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": recipe_including_subrecipe},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            expected_co2,
        )

        final_graph = batch[0]["final_graph"]
        locations = [x.get("flow_location") or x.get("activity_location") for x in final_graph]
        country_codes = set([x[0].get("country_code") for x in locations if x])
        assert country_codes == expected_country_codes, "There are unexpected country codes in the final graph."
        number_of_transport_activities = len(
            [x for x in final_graph if x.get("node_type") == TransportActivityNode.__name__]
        )
        assert (
            number_of_transport_activities == expected_number_of_transport_activities
        ), "Unexpected number of transport-activities."


TEST_RECIPE_WITH_COMBINED_PRODUCTS_AS_SUBRECIPE_EXPECTED_CO2 = 0.010309908190000001
TEST_RECIPE_WITH_COMBINED_PRODUCTS_AS_SUBRECIPE = [
    {
        "input_root": {
            "flow": {
                "node_type": FoodProductFlowNode.__name__,
                "nutrient_values": {
                    "energy_kcal": 200,
                    "fat_gram": 12.3,
                    "saturated_fat_gram": 2.5,
                    "carbohydrates_gram": 8.4,
                    "sucrose_gram": 3.2,
                    "protein_gram": 2.2,
                    "sodium_chloride_gram": 0.3,
                },
            },
            "activity": {
                "node_type": FoodProcessingActivityNode.__name__,
                "xid": "test_link_to_combined_products_as_subrecipe",
                "activity_location": "Zürich Schweiz",
                "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
                "activity_date": "2023-06-20",
                "recipe_portions": 140,
                "production_amount": {"unit": "gram", "value": 50},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "488533744",
                    "raw_production": {"value": "greenhouse", "language": "en"},
                    "link_to_sub_node": {"xid": "carrot_combined_product_id"},
                    "transport": "ground",
                    "flow_location": "spain",
                    "product_name": [{"language": "de", "value": "Karotten 5stk"}],
                    "processing": "raw",
                    "raw_conservation": {"value": "fresh", "language": "en"},
                    "packaging": "plastic",
                },
            ],
        },
        "return_final_graph": True,
    },
    {
        "input_root": {
            "activity": {
                "xid": "carrot_combined_product_id",
                "node_type": LinkingActivityNode.__name__,
                "production_amount": {"unit": "kilogram", "value": 1},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "product_name": [{"language": "de", "value": "carrot mixture"}],
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 1},
                    "link_to_sub_node": {"xid": "carrot_mixture_id"},
                },
            ],
        },
        "skip_calc": True,
    },
    {
        "input_root": {
            "activity": {
                "xid": "carrot_mixture_id",
                "node_type": FoodProcessingActivityNode.__name__,
                "production_amount": {"unit": "kilogram", "value": 1},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "115",
                    "product_name": [{"language": "de", "value": "Karotten"}, {"language": "en", "value": "carrots"}],
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 0.6},
                    "flow_location": "france",
                },
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "115_copy",
                    "product_name": [{"language": "de", "value": "Karotten"}, {"language": "en", "value": "carrots"}],
                    "amount_in_original_source_unit": {"unit": "kilogram", "value": 0.4},
                    "flow_location": "france",
                },
            ],
        },
        "skip_calc": True,
    },
]


@pytest.mark.parametrize(
    "location_specification",
    [
        "in_root_recipe",
        "in_sub_recipe",
        "not_specified",
        "in_root_and_sub_recipe",
    ],
)
@pytest.mark.asyncio
async def test_post_subrecipe_with_link_to_combined_products(
    app: FastAPIReturn,
    location_specification: str,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, _ = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser
        recipe_including_subrecipe = copy.deepcopy(TEST_RECIPE_WITH_COMBINED_PRODUCTS_AS_SUBRECIPE)

        # test if superuser can create a recipe (should be possible for any access_group_xid)
        recipe_including_subrecipe = change_in_every_activity_in_batch(
            recipe_including_subrecipe, "access_group_uid", test_kitchen_by_superuser_uid
        )

        # Spain, France, Switzerland from the specification in TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE
        country_codes_specified_in_recipe = {"ES", "CH", "FR"}
        number_of_valid_transports_in_recipe = (
            3  # the two carrots being transported FR->ES, and the combined product ES->CH
        )
        if location_specification == "in_root_recipe":
            expected_country_codes = country_codes_specified_in_recipe
            expected_number_of_transport_activities = number_of_valid_transports_in_recipe
            expected_co2 = TEST_RECIPE_WITH_COMBINED_PRODUCTS_AS_SUBRECIPE_EXPECTED_CO2
        elif location_specification == "in_sub_recipe":
            recipe_including_subrecipe[1]["input_root"]["sub_flows"][0]["flow_location"] = recipe_including_subrecipe[
                0
            ]["input_root"]["sub_flows"][0]["flow_location"]
            del recipe_including_subrecipe[0]["input_root"]["sub_flows"][0]["flow_location"]
            expected_country_codes = country_codes_specified_in_recipe
            expected_number_of_transport_activities = number_of_valid_transports_in_recipe
            expected_co2 = TEST_RECIPE_WITH_COMBINED_PRODUCTS_AS_SUBRECIPE_EXPECTED_CO2
        elif location_specification == "not_specified":
            del recipe_including_subrecipe[0]["input_root"]["sub_flows"][0]["flow_location"]
            expected_country_codes = country_codes_specified_in_recipe - {
                "ES"
            }  # there will be no fao-stats for the combined product
            additional_co2 = -0.0076427900255093  # different transport routes to origin-split
            expected_number_of_transport_activities = 2  # directly 2*fr->ch
            expected_co2 = TEST_RECIPE_WITH_COMBINED_PRODUCTS_AS_SUBRECIPE_EXPECTED_CO2 + additional_co2
        elif location_specification == "in_root_and_sub_recipe":
            # additional location in the sub-recipe, we expect this location to be ignored.
            recipe_including_subrecipe[1]["input_root"]["sub_flows"][0]["flow_location"] = "netherlands"
            expected_country_codes = country_codes_specified_in_recipe
            expected_number_of_transport_activities = number_of_valid_transports_in_recipe
            expected_co2 = TEST_RECIPE_WITH_COMBINED_PRODUCTS_AS_SUBRECIPE_EXPECTED_CO2

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": recipe_including_subrecipe},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            expected_co2,
        )

        final_graph = batch[0]["final_graph"]
        locations = [x.get("flow_location") or x.get("activity_location") for x in final_graph]
        country_codes = set([x[0].get("country_code") for x in locations if x])
        assert country_codes == expected_country_codes, "There are unexpected country codes in the final graph."
        number_of_transport_activities = len(
            [x for x in final_graph if x.get("node_type") == TransportActivityNode.__name__]
        )
        assert (
            number_of_transport_activities == expected_number_of_transport_activities
        ), "Unexpected number of transport-activities."


@pytest.mark.asyncio
async def test_post_with_nutrients_but_missing_matching(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    """Test data_errors for missing matching and ingredient_amount_estimation."""
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        encoded_super_user_token, _, _ = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser
        recipe_including_subrecipe_with_missing_matching = copy.deepcopy(TEST_RECIPE_WITH_MONOPRODUCTS_AS_SUBRECIPE)
        recipe_including_subrecipe_with_missing_matching = change_in_every_activity_in_batch(
            recipe_including_subrecipe_with_missing_matching, "access_group_uid", test_kitchen_by_superuser_uid
        )

        # remove the matching for the tomato_ingredient
        del recipe_including_subrecipe_with_missing_matching[0]["input_root"]["sub_flows"][1]["link_to_sub_node"]
        recipe_including_subrecipe_with_missing_matching[0]["input_root"]["sub_flows"][1]["product_name"] = [
            {"language": "de", "value": "strange_name_for_Tomaten"}
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": recipe_including_subrecipe_with_missing_matching},
        )
        data_errors = response.json().get("batch")[0].get("data_errors")

        # we expect only a data-error because of missing matching
        # we do not expect an additional gfm-error in ingredient_amount_estimation_gfm
        assert len(data_errors) == 1
        assert data_errors[0].get("error_classification") == "missing_matching"
