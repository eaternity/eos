import unicodedata
from typing import Any, List, Tuple

import pytest
from fastapi import FastAPI, status
from httpx import AsyncClient
from structlog import get_logger

from api.app.settings import Settings
from core.domain.term import Term
from core.service.matching_service import UNICODE_NORMALIZATION_FORM
from core.service.service_provider import ServiceLocator

logger = get_logger()
settings = Settings()
service_locator = ServiceLocator()

BASE_URL = "http://localhost:8040"


@pytest.mark.asyncio
async def test_matching_item(app: FastAPI, term_access_group_uids: List[str]) -> None:
    async with AsyncClient(app=app, base_url=BASE_URL) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        # getting the term corresponding to the string "Karotten"; there should be only 1 term returned (it was
        # inserted during the test setup)
        response = await ac.get(
            "/v2/matching/Karotten", headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"}
        )
        assert response.status_code == status.HTTP_200_OK, "Failed to get matching item"
        matching_items = response.json().get("matching_items")
        assert len(matching_items) == 1, f"Expected 1 list of terms, got {len(matching_items)}"
        terms = matching_items[0]["terms"]
        assert len(terms) == 1, f"Expected 1 term in the list, got {len(terms)}"
        assert (
            terms[0]["name"] == "10006122 - CARROTS (GS1 GPC)"
        ), f"Expected '10006122 - CARROTS (GS1 GPC)', got {terms[0]['name']}"

        # adding another matching item for 'Karotten', link it to another term
        honey: Term = (
            await service_locator.service_provider.postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
                "A0462",
                foodex2_term_group_uid,
            )
        )
        assert honey.xid == "A0462"

        # check if that uid is in our glossary_service cache
        assert service_locator.service_provider.glossary_service.terms_by_uid[honey.uid]

        response = await ac.put(
            "/v2/matching/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "uid": matching_items[0]["uid"],
                "gap_filling_module": "MatchProductName",
                "matching_string": "Karotten",
                "term_uids": [str(honey.uid)],
                "lang": "de",
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Failed to add matching item"
        assert response.json() == {
            "matching_string": "karotten",
            "term_uids": [str(honey.uid)],
            "matched_node_uid": None,
            "lang": "de",
            "gap_filling_module": "MatchProductName",
            "access_group_uid": None,
            "data": None,
            "terms": None,
            "uid": matching_items[0]["uid"],
        }

        # getting the terms corresponding to the string "Karotten"; now it should return "Honey"
        response = await ac.get(
            "/v2/matching/Karotten", headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"}
        )
        assert response.status_code == status.HTTP_200_OK, "Failed to get matching items"
        matching_items = response.json().get("matching_items")
        assert len(matching_items) == 1, f"Expected 1 list of terms, got {len(matching_items)}"
        terms = matching_items[0]["terms"]
        assert len(terms) == 1, f"Expected 1 term in the list, got {len(terms)}"
        assert terms[0]["name"] == "SUGAR AND HONEY (CIAA)", "should have only Honey"


@pytest.mark.asyncio
async def test_matching_item_normalization(app: FastAPI, term_access_group_uids: List[str]) -> None:
    async with AsyncClient(app=app, base_url=BASE_URL) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        rape_seed_oil: Term = (
            await service_locator.service_provider.postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
                "A036V",
                foodex2_term_group_uid,
            )
        )
        assert rape_seed_oil.xid == "A036V"

        # check if that uid is in our glossary_service cache
        assert service_locator.service_provider.glossary_service.terms_by_uid[rape_seed_oil.uid]

        matching_str1 = "Rapso\u0308l"
        matching_str2 = "rapsöl"
        assert matching_str1 != matching_str2, "the two test strings should be different"

        matching_str_normalized1 = unicodedata.normalize(UNICODE_NORMALIZATION_FORM, matching_str1.casefold())
        matching_str_normalized2 = unicodedata.normalize(UNICODE_NORMALIZATION_FORM, matching_str2.casefold())

        assert matching_str_normalized1 == matching_str_normalized2, (
            "the normalized version of the two strings " "should be the same"
        )

        response = await ac.put(
            "/v2/matching/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "gap_filling_module": "MatchProductName",
                "matching_string": matching_str1,
                "term_uids": [str(rape_seed_oil.uid)],
                "lang": "de",
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Failed to add matching item"
        resp = response.json()
        assert response.json() == {
            "matching_string": matching_str_normalized1,
            "term_uids": [str(rape_seed_oil.uid)],
            "matched_node_uid": None,
            "lang": "de",
            "gap_filling_module": "MatchProductName",
            "access_group_uid": None,
            "data": None,
            "terms": None,
            "uid": resp["uid"],
        }

        response = await ac.post(
            "/v2/matching/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "gap_filling_module": "MatchProductName",
                "matching_string": matching_str2,
                "lang": "de",
                "term_uids": [],
            },
        )
        assert response.status_code == status.HTTP_200_OK, "Failed to get matching items"
        matching_items = response.json().get("matching_items")
        assert len(matching_items) == 1, f"Expected 1 list of matching_items, got {len(matching_items)}"
        terms = matching_items[0]["terms"]
        assert len(terms) == 1, f"Expected 1 term in the list, got {len(terms)}"
        assert terms[0]["name"] == "30990 - RAPE SEED OIL, EDIBLE (EFSA FOODEX2)", "should have rape seed oil"


@pytest.mark.asyncio
async def test_get_unknown_matching_item(app: FastAPI) -> None:
    async with AsyncClient(app=app, base_url=BASE_URL) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        # getting the term corresponding to an unknown string --> should have no term returned
        response = await ac.get(
            "/v2/matching/a_string_that_does_not_exist",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND, "this matching string should not exist"


@pytest.mark.asyncio
async def test_put_matching_item_without_proper_term(app: FastAPI, term_access_group_uids: List[str]) -> None:
    async with AsyncClient(app=app, base_url=BASE_URL) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        carrot: Term = (
            await service_locator.service_provider.postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
                "A1791",
                foodex2_term_group_uid,
            )
        )
        assert carrot.xid == "A1791"

        response = await ac.put(
            "/v2/matching/",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "gap_filling_module": "MatchProductName",
                "matching_string": "carottes",
                "term_uids": ["5f1ce3d1-e937-46aa-8a02-410355c03924"],  # a random uuid that does not exist in our db
                "lang": "de",
            },
        )
        assert (
            response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        ), "Failed to add matching item without proper term"


@pytest.mark.asyncio
async def test_put_matching_item_as_non_superuser(
    app: FastAPI, create_users: Tuple[Any, Any, str], term_access_group_uids: List[str]
) -> None:
    async with AsyncClient(app=app, base_url=BASE_URL) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, encoded_basic_user_token = create_users
        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        carrot: Term = (
            await service_locator.service_provider.postgres_db.get_term_mgr().get_term_by_xid_and_access_group_uid(
                "A1791",
                foodex2_term_group_uid,
            )
        )
        assert carrot.xid == "A1791"

        # test that PUT request is forbidden
        response = await ac.put(
            "/v2/matching/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "gap_filling_module": "MatchProductName",
                "matching_string": "carottes",
                "term_uids": [str(carrot.uid)],
                "lang": "de",
            },
        )
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
        ), "should not be able to update a matching item as a non-superuser"
