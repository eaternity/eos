import pytest
from httpx import AsyncClient
from structlog import get_logger

from api.app.server import app_shutdown, app_startup, fastapi_app
from core.service.service_provider import ServiceLocator

logger = get_logger()
service_locator = ServiceLocator()


@pytest.mark.asyncio
async def test_basic_router_availability(basic_router_availability: None) -> None:
    _ = basic_router_availability
    return


@pytest.mark.asyncio
async def test_no_service_cache_and_gap_filling_modules_duplication() -> None:
    async with AsyncClient(
        app=fastapi_app,
        base_url="http://localhost:8040",
    ):
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")
        await service_locator.service_provider.postgres_db.reset_db("test_pg")
        _, gap_filling_module_loader = await app_startup(fastapi_app, schema="test_pg")

    assert (
        len(service_locator.service_provider.gap_filling_module_loader.gap_filling_modules)
        == len(service_locator.service_provider.gap_filling_module_loader.initialized_modules) + 1
    )
    assert len(service_locator.service_provider.glossary_service.terms_by_uid) < 13500
    assert len(service_locator.service_provider.glossary_service.terms_by_xid_ag_uid) < 13500
    assert len(service_locator.service_provider.matching_service._terms_by_matching_str) < 60
    assert len(service_locator.service_provider.node_service.cache._node_cache) < 17
    assert len(service_locator.service_provider.node_service.cache._node_child_edge_cache) < 3

    await app_shutdown(fastapi_app)
