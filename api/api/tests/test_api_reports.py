import copy
from datetime import date
from typing import Tuple
from uuid import UUID, uuid4

import deepdiff
import pytest
from fastapi import FastAPI, status
from httpx import AsyncClient
from structlog import get_logger

from api.app.injector import get_node_controller, get_report_generation_controller
from core.domain.calculation import Calculation, LinkedSubNodeInfo
from core.domain.props.quantity_prop import RawQuantity
from core.service.messaging_service import Priority
from core.service.service_provider import ServiceLocator

from .conftest import (
    CUSTOMER,
    TEST_KITCHEN_BY_ADMIN,
    TEST_KITCHEN_BY_SUPERUSER,
    TEST_RECIPE,
    TEST_RECIPE_FLOW_ID,
    TEST_RECIPE_WITH_SUBRECIPE,
    TEST_RECIPE_WITH_SUBRECIPE_ID,
    find_quantity_value,
    is_close,
    is_dict_close,
)
from .test_api_for_recipes import (
    TEST_RECIPE_EXPECTED_CO2,
    TEST_RECIPE_EXPECTED_DFU,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
    TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU,
    FoodProcessingActivityNode,
    FoodProductFlowNode,
)
from .test_api_for_supplies import TEST_SUPPLY_EXPECTED_CO2, TEST_SUPPLY_EXPECTED_DFU, SupplySheetActivityNode

logger = get_logger()
service_locator = ServiceLocator()


EXPECTED_MUSTACHE_JSON_USE_SUPPLIES = {
    "snippet-kitchen-total-co2-value-comparison": {
        "this": {
            "value": 0.07272993276931913,
            "reduction-with-respect-to-average": -0.3305539335072921,
            "restaurant-comparison-trees": 0.0,
            "comparison-to-average-percentage": -81.96557342082366,  # the way it is currently implemented,  it is
            # the more positive the more climate friendly
        },
        "average": {
            "value": 0.4032838662766113,
            "reduction-with-respect-to-average": None,
            "restaurant-comparison-trees": None,
            "comparison-to-average-percentage": None,
        },
        "climate-friendly": {
            "value": 0.322627093021289,
            "reduction-with-respect-to-average": -0.08065677325532228,
            "restaurant-comparison-trees": None,
            "comparison-to-average-percentage": -20.000000000000007,
        },
    },
    "snippet-climate-friendly-menu-comparison": {
        "number-of-kitchens": 205,
        "climate-friendly": 46.93,
        "average": 34.65,
        "this": 50.0,
    },
    "snippet-menu-certificate": {
        "all-recipes-average": 1245.92,
        "climate-friendly-recipes-average": 546.7697404819909,
        "this-recipes-average": 673.8546982621289,
        "certificate-percentage": -56.11518071128236,
    },
    "snippet-top-flop-menu": {
        "best-menu-title": [{"language": "de", "value": "Kürbisrisotto"}],
        "best-menu-co2-value-gram": 546.7697404819909,
        "worst-menu-title": [{"language": "de", "value": "Parent Recipe with child recipes"}],
        "worst-menu-co2-value-gram": 800.9396560422674,
        "best-menu-table": [
            {"title": [{"language": "de", "value": "Tomaten"}], "co2-gram": 0.0, "weight-gram": 423.99077074704866},
            {
                "title": [{"language": "de", "value": "Zwiebeln"}],
                "co2-gram": 546.7697404819909,
                "weight-gram": 220.47520078846532,
            },
        ],
        "worst-menu-table": [
            {
                "title": [{"language": "de", "value": "Kürbisrisotto"}],
                "co2-gram": 365.2136056234238,
                "weight-gram": 430.4695811414245,
            },
            {
                "title": [{"language": "de", "value": "Tomaten"}],
                "co2-gram": 435.72605041884356,
                "weight-gram": 671.5325465806222,
            },
        ],
    },
    "snippet-best-worst-ingredients": {
        "worst-ingredients": [],
        "best-ingredients": [  # example check for onion: dfu_per_g=0.00021, kg_co2_per_g=0.000297, amount_in_g=104.9
            # -> co2-reduction = amount_in_g*(AVERAGE_KG_CO2_PER_FOOD_UNIT * dfu_per_g * 5 - kg_co2_per_g) = 0.0546
            {
                "ingredient-weight-gram": 207.1105517581512,
                "co2-reduction-kilogram": 0.25085646890471835,
                "examples": ["Kartoffeln"],
            },
            {
                "ingredient-weight-gram": 108.00000002755387,
                "co2-reduction-kilogram": 0.0665354814333082,
                "examples": ["Tomaten"],
            },
            {
                "ingredient-weight-gram": 104.94069697878233,
                "co2-reduction-kilogram": 0.054809844887101555,
                "examples": ["Zwiebeln"],
            },
        ],
    },
    "snippet-category-comparison": {
        "category-analysis": [
            {
                "category-weight-gram": 420.0512487644874,
                "category-name": [
                    {"language": "en", "value": "Plant based products"},
                    {"language": "de", "value": "Pflanzliche produkte"},
                ],
                "co2-value-gram": 31.16738700269835,
                "co2-improvement-percentage": 92.27323544387812,  # the way it is currently implemented,  it is
                # the more positive the more climate friendly
                "parent-category-name": [{"language": "en", "value": "Eaternity food categories"}],
            },
            {
                "category-weight-gram": 420.0512487644874,
                "category-name": [{"language": "en", "value": "Vegetables"}, {"language": "de", "value": "Gemüse"}],
                "co2-value-gram": 31.16738700269835,
                "co2-improvement-percentage": 92.27323544387812,
                "parent-category-name": [{"language": "en", "value": "Plant based products"}],
            },
        ]
    },
    "best-menus": {
        "table-data": [
            {
                "title": [{"language": "de", "value": "Kürbisrisotto"}],
                "co2-gram": 546.7697404819909,
                "weight-gram": 644.4659715355141,
                "weight-per-portion-gram": 1.6285714285714283,
                "co2-per-portion-gram": 1.3816921554985704,
            },
        ]
    },
}


EXPECTED_MUSTACHE_JSON_USE_RECIPES = {
    "snippet-kitchen-total-co2-value-comparison": {
        "this": {
            "value": 0.2864678255714901,
            "reduction-with-respect-to-average": -0.29903173510960745,
            "restaurant-comparison-trees": 0,
            "comparison-to-average-percentage": -51.07292220027476,
        },
        "average": {
            "value": 0.5854995606810975,
            "reduction-with-respect-to-average": None,
            "restaurant-comparison-trees": None,
            "comparison-to-average-percentage": None,
        },
        "climate-friendly": {
            "value": 0.46839964854487803,
            "reduction-with-respect-to-average": -0.11709991213621951,
            "restaurant-comparison-trees": None,
            "comparison-to-average-percentage": -20.0,
        },
    },
    "snippet-climate-friendly-menu-comparison": {
        "number-of-kitchens": 205,
        "climate-friendly": 46.93,
        "average": 34.65,
        "this": 50.0,
    },
    "snippet-menu-certificate": {
        "all-recipes-average": 1245.92,
        "climate-friendly-recipes-average": 546.7697404819909,
        "this-recipes-average": 673.8546982621289,
        "certificate-percentage": -56.11518071128236,
    },
    "snippet-top-flop-menu": {
        "best-menu-title": [{"language": "de", "value": "Kürbisrisotto"}],
        "best-menu-co2-value-gram": 546.7697404819909,
        "worst-menu-title": [{"language": "de", "value": "Parent Recipe with child recipes"}],
        "worst-menu-co2-value-gram": 800.9396560422674,
        "best-menu-table": [
            {"title": [{"language": "de", "value": "Tomaten"}], "co2-gram": 0.0, "weight-gram": 423.99077074704866},
            {
                "title": [{"language": "de", "value": "Zwiebeln"}],
                "co2-gram": 546.7697404819909,
                "weight-gram": 220.47520078846532,
            },
        ],
        "worst-menu-table": [
            {
                "title": [{"language": "de", "value": "Kürbisrisotto"}],
                "co2-gram": 365.2136056234238,
                "weight-gram": 430.4695811414245,
            },
            {
                "title": [{"language": "de", "value": "Tomaten"}],
                "co2-gram": 435.72605041884356,
                "weight-gram": 671.5325465806222,
            },
        ],
    },
    "snippet-best-worst-ingredients": {
        "worst-ingredients": [],
        "best-ingredients": [
            {
                "ingredient-weight-gram": 778.1339712918658,
                "co2-reduction-kilogram": 0.4064143225246032,
                "examples": ["Zwiebeln"],
            },
            {
                "ingredient-weight-gram": 260.8947368421052,
                "co2-reduction-kilogram": 0.16072923069237968,
                "examples": ["Tomaten"],
            },
        ],
    },
    "snippet-category-comparison": {
        "category-analysis": [
            {
                "category-weight-gram": 1039.028708133971,
                "category-name": [
                    {"language": "en", "value": "Plant based products"},
                    {"language": "de", "value": "Pflanzliche produkte"},
                ],
                "co2-value-gram": 231.10578947368413,
                "co2-improvement-percentage": 71.04842094893638,
                "parent-category-name": [{"language": "en", "value": "Eaternity food categories"}],
            },
            {
                "category-weight-gram": 1039.028708133971,
                "category-name": [{"language": "en", "value": "Vegetables"}, {"language": "de", "value": "Gemüse"}],
                "co2-value-gram": 231.10578947368413,
                "co2-improvement-percentage": 71.04842094893638,
                "parent-category-name": [{"language": "en", "value": "Plant based products"}],
            },
        ]
    },
    "best-menus": {
        "table-data": [
            {
                "title": [{"language": "de", "value": "Kürbisrisotto"}],
                "co2-gram": 546.7697404819909,
                "weight-gram": 644.4659715355141,
                "weight-per-portion-gram": 1.6285714285714283,
                "co2-per-portion-gram": 1.3816921554985704,
            },
        ]
    },
}

EXPECTED_SUPERUSER_FINAL_GRAPHS = [
    [
        {
            "name": "Kürbisrisotto",
            "xid": "test_recipe_id",
            "gtin": "",
            "access_group_xid": "test_kitchen_id",
            "supplier": "",
            "producer": "",
            "level": 0,
            "tree_index": "",
            "is_leaf": False,
            "flow_node_type": "FoodProductFlowNode",
            "activity_node_type": "FoodProcessingActivityNode",
            "general_node_type": "flow,activity",
            "article_number": "",
            "business_unit": "",
            "end_product": "",
            "certificates": "",
            "packaging": "",
            "flow_amount_per_root_node": 0.0016285714285714287,
            "flow_unit": "Kilogram",
            "node_production_amount": 0.228,
            "node_production_unit": "Kilogram",
            "recipe_portions": 140,
            "production_portions": 300,
            "sold_portions": None,
            "production_date": "2023-06-20",
            "location": "CHE.26.13.21_1",
            "transport": "",
            "conservation": "not conserved",
            "perishability": "highly-perishable",
            "processing": "",
            "is_combined_product": True,
            "is_subdivision": False,
            "is_dried": False,
            "nutrient_upscale_ratio": "",
            "matched_terms": "",
            "nutrient_file": "",
            "fao_code": "",
            "brightway_node": "",
            "amount_per_category_in_flow": (
                "Diet high in energy,Diet high in fat,Diet high in protein,Diet high in sodium (salt),"
                "Diet low in fat,Diet low in protein,Diet low in vegetables,Plant based products,Vegetables"
            ),
            "transport_mode_distances": "",
            "transport_mode_options": "",
            "matrix_gfm_error": "",
            "error_squares": "",
            "solution_status": "",
            "kg_co2eq_per_root_node": 0.0013816921554985707,
            "kg_co2eq_reduction_value_per_root_node": 0.0017667591265004199,
            "co2_rating": "A",
            "co2_award": True,
            "co2_value_improvement_percentage": 56.115180711282356,
            "liter_scarce_water_consumption_per_root_node": 0.02170273595454545,
            "liter_scarce_water_consumption_reduction_value_per_root_node": 0.12635978612944457,
            "scarce_water_consumption_rating": "A",
            "scarce_water_consumption_award": True,
            "scarce_water_consumption_improvement_percentage": 85.3421813642791,
            "vita_score_points_per_root_node": 275.2189616644503,
            "diet_low_in_fruits_points_per_root_node": 61.24,
            "diet_low_in_whole_grains_points_per_root_node": 82.86,
            "diet_low_in_vegetables_points_per_root_node": 0.0,
            "diet_low_in_nuts_and_seeds_points_per_root_node": 13.98,
            "diet_low_in_milk_points_per_root_node": 9.42,
            "diet_low_in_fat_points_per_root_node": 86.53735866947305,
            "diet_low_in_protein_points_per_root_node": 21.181602994977325,
            "diet_high_in_sodium_(salt)_points_per_root_node": 0.0,
            "diet_high_in_processed_meat_points_per_root_node": 0.0,
            "diet_high_in_red_meat_points_per_root_node": 0.0,
            "diet_high_in_energy_points_per_root_node": 0.0,
            "diet_high_in_fat_points_per_root_node": 0.0,
            "diet_high_in_protein_points_per_root_node": 0.0,
            "vitascore_rating": "A",
            "vitascore_improvement_percentage": 19.05324656927932,
            "animal_welfare_rating": "A",
            "rainforest_critical_products_rating": "A",
            "kg_co2eq_only_all_transport_per_root_node": 2.7835012641428564e-05,
            "kg_co2eq_only_all_processing_per_root_node": 0.0,
            "kg_co2eq_only_greenhouse_per_root_node": 0.0,
            "kg_co2eq_only_base_per_root_node": 0.0013538571428571421,
            "kg_co2eq_only_truck_transport_per_root_node": 2.7835012641428564e-05,
            "kg_co2eq_only_sea_transport_per_root_node": 0.0,
            "kg_co2eq_only_air_transport_per_root_node": 0.0,
            "kg_co2eq_only_cooling_of_transport_per_root_node": 0.0,
            "kg_co2eq_only_freezing_of_transport_per_root_node": 0.0,
            "kg_co2eq_only_processing_dried_fruit_grinding_per_root_node": 0.0,
            "kg_co2eq_only_processing_grinding_per_root_node": 0.0,
            "kg_co2eq_only_processing_carbonating_per_root_node": 0.0,
            "kg_co2eq_only_processing_cutting_per_root_node": 0.0,
            "kg_co2eq_only_processing_shredding_per_root_node": 0.0,
            "kg_co2eq_only_processing_fermenting_per_root_node": 0.0,
            "kg_co2eq_only_processing_smoking_per_root_node": 0.0,
            "kg_co2eq_only_processing_puffing_per_root_node": 0.0,
            "kg_co2eq_only_processing_freeze-drying_per_root_node": 0.0,
            "kg_co2eq_only_processing_cooling_per_root_node": 0.0,
            "kg_co2eq_only_processing_baking_per_root_node": 0.0,
            "kg_co2eq_only_processing_jam_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_juice_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_yoghurt_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_roasting_per_root_node": 0.0,
            "kg_co2eq_only_processing_chopping_per_root_node": 0.0,
            "kg_co2eq_only_processing_heating_per_root_node": 0.0,
            "kg_co2eq_only_processing_drying_per_root_node": 0.0,
            "kg_co2eq_only_processing_freezing_per_root_node": 0.0,
            "kg_co2eq_only_processing_mixing_per_root_node": 0.0,
            "kg_co2eq_only_processing_transportation_per_root_node": 0.0,
            "EOS_nutrients_sucrose_gram_per_root_node": 0.3533766233766232,
            "EOS_nutrients_chlorine_gram_per_root_node": 0.001187532467532467,
            "EOS_nutrients_protein_gram_per_root_node": 0.0678311688311688,
            "EOS_nutrients_energy_kilocalorie_per_root_node": 2.003735649350648,
            "EOS_nutrients_sodium_gram_per_root_node": 0.00019036363636363624,
            "EOS_nutrients_fibers_gram_per_root_node": 0.09490909090909087,
            "EOS_nutrients_saturated_fat_gram_per_root_node": 0.0,
            "EOS_nutrients_fat_gram_per_root_node": 0.012331168831168828,
            "EOS_nutrients_carbohydrates_gram_per_root_node": 0.3533766233766232,
            "EOS_nutrients_water_gram_per_root_node": 1.0607142857142857,
            "EOS_nutrients_source": "aggregated",
            "aggregated_daily_food_unit_per_root_node": 0.0008086429387438013,
        }
    ],
    [
        {
            "name": "Parent Recipe with child recipes",
            "xid": "test_recipe_with_subrecipe_id",
            "gtin": "",
            "access_group_xid": "test_kitchen_id",
            "supplier": "",
            "producer": "",
            "level": 0,
            "tree_index": "",
            "is_leaf": False,
            "flow_node_type": "FoodProductFlowNode",
            "activity_node_type": "FoodProcessingActivityNode",
            "general_node_type": "flow,activity",
            "article_number": "",
            "business_unit": "",
            "end_product": "",
            "certificates": "",
            "packaging": "",
            "flow_amount_per_root_node": 0.0009142857142857143,
            "flow_unit": "Kilogram",
            "node_production_amount": 0.128,
            "node_production_unit": "Kilogram",
            "recipe_portions": 140,
            "production_portions": 300,
            "sold_portions": 250,
            "production_date": "2023-06-20",
            "location": "CHE.26.13.21_1",
            "transport": "",
            "conservation": "not conserved",
            "perishability": "highly-perishable",
            "processing": "",
            "is_combined_product": True,
            "is_subdivision": False,
            "is_dried": False,
            "nutrient_upscale_ratio": "",
            "matched_terms": "",
            "nutrient_file": "",
            "fao_code": "",
            "brightway_node": "",
            "amount_per_category_in_flow": (
                "Diet high in energy,Diet high in fat,Diet high in protein,Diet high in sodium (salt),"
                "Diet low in fat,Diet low in protein,Diet low in vegetables,Plant based products,Vegetables"
            ),
            "transport_mode_distances": "",
            "transport_mode_options": "",
            "matrix_gfm_error": "",
            "error_squares": "",
            "solution_status": "",
            "kg_co2eq_per_root_node": 0.0006645065985835014,
            "kg_co2eq_reduction_value_per_root_node": 0.00036918183856820457,
            "co2_rating": "B",
            "co2_award": False,
            "co2_value_improvement_percentage": 35.71500128079915,
            "liter_scarce_water_consumption_per_root_node": 0.010123381219856457,
            "liter_scarce_water_consumption_reduction_value_per_root_node": 0.03848798460587291,
            "scarce_water_consumption_rating": "A",
            "scarce_water_consumption_award": True,
            "scarce_water_consumption_improvement_percentage": 79.17486775387356,
            "vita_score_points_per_root_node": 268.4757307985283,
            "diet_low_in_fruits_points_per_root_node": 61.24,
            "diet_low_in_whole_grains_points_per_root_node": 82.86,
            "diet_low_in_vegetables_points_per_root_node": 0.0,
            "diet_low_in_nuts_and_seeds_points_per_root_node": 13.98,
            "diet_low_in_milk_points_per_root_node": 9.42,
            "diet_low_in_fat_points_per_root_node": 80.54893834022758,
            "diet_low_in_protein_points_per_root_node": 20.42679245830072,
            "diet_high_in_sodium_(salt)_points_per_root_node": 0.0,
            "diet_high_in_processed_meat_points_per_root_node": 0.0,
            "diet_high_in_red_meat_points_per_root_node": 0.0,
            "diet_high_in_energy_points_per_root_node": 0.0,
            "diet_high_in_fat_points_per_root_node": 0.0,
            "diet_high_in_protein_points_per_root_node": 0.0,
            "vitascore_rating": "A",
            "vitascore_improvement_percentage": 21.036549765138744,
            "animal_welfare_rating": "A",
            "rainforest_critical_products_rating": "A",
            "kg_co2eq_only_all_transport_per_root_node": 3.582348612894945e-05,
            "kg_co2eq_only_all_processing_per_root_node": 0.0,
            "kg_co2eq_only_greenhouse_per_root_node": 0.00033178461621395064,
            "kg_co2eq_only_base_per_root_node": 0.00029689849624060127,
            "kg_co2eq_only_truck_transport_per_root_node": 3.3939182080338344e-05,
            "kg_co2eq_only_sea_transport_per_root_node": 0.0,
            "kg_co2eq_only_air_transport_per_root_node": 0.0,
            "kg_co2eq_only_cooling_of_transport_per_root_node": 1.8843040486111107e-06,
            "kg_co2eq_only_freezing_of_transport_per_root_node": 0.0,
            "kg_co2eq_only_processing_dried_fruit_grinding_per_root_node": 0.0,
            "kg_co2eq_only_processing_grinding_per_root_node": 0.0,
            "kg_co2eq_only_processing_carbonating_per_root_node": 0.0,
            "kg_co2eq_only_processing_cutting_per_root_node": 0.0,
            "kg_co2eq_only_processing_shredding_per_root_node": 0.0,
            "kg_co2eq_only_processing_fermenting_per_root_node": 0.0,
            "kg_co2eq_only_processing_smoking_per_root_node": 0.0,
            "kg_co2eq_only_processing_puffing_per_root_node": 0.0,
            "kg_co2eq_only_processing_freeze-drying_per_root_node": 0.0,
            "kg_co2eq_only_processing_cooling_per_root_node": 0.0,
            "kg_co2eq_only_processing_baking_per_root_node": 0.0,
            "kg_co2eq_only_processing_jam_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_juice_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_yoghurt_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_roasting_per_root_node": 0.0,
            "kg_co2eq_only_processing_chopping_per_root_node": 0.0,
            "kg_co2eq_only_processing_heating_per_root_node": 0.0,
            "kg_co2eq_only_processing_drying_per_root_node": 0.0,
            "kg_co2eq_only_processing_freezing_per_root_node": 0.0,
            "kg_co2eq_only_processing_mixing_per_root_node": 0.0,
            "kg_co2eq_only_processing_transportation_per_root_node": 0.0,
            "EOS_nutrients_sucrose_gram_per_root_node": 0.09532344497607652,
            "EOS_nutrients_chlorine_gram_per_root_node": 0.0004275666438824332,
            "EOS_nutrients_protein_gram_per_root_node": 0.019332399179767597,
            "EOS_nutrients_energy_kilocalorie_per_root_node": 0.5579255697197536,
            "EOS_nutrients_sodium_gram_per_root_node": 5.066069719753927e-05,
            "EOS_nutrients_fibers_gram_per_root_node": 0.027499111414900877,
            "EOS_nutrients_saturated_fat_gram_per_root_node": 0.0,
            "EOS_nutrients_fat_gram_per_root_node": 0.004375632262474367,
            "EOS_nutrients_carbohydrates_gram_per_root_node": 0.09532344497607652,
            "EOS_nutrients_water_gram_per_root_node": 0.7552127819548872,
            "EOS_nutrients_source": "aggregated",
            "aggregated_daily_food_unit_per_root_node": 0.0002654908018881998,
        },
        {
            "name": "    Kürbisrisotto",
            "xid": "488533744",
            "gtin": "",
            "access_group_xid": "test_kitchen_id",
            "supplier": "",
            "producer": "",
            "level": 2,
            "tree_index": ".1.1",
            "is_leaf": False,
            "flow_node_type": "FoodProductFlowNode",
            "activity_node_type": "FoodProcessingActivityNode",
            "general_node_type": "flow,activity",
            "article_number": "",
            "business_unit": "",
            "end_product": "",
            "certificates": "",
            "packaging": "",
            "flow_amount_per_root_node": 0.00035714285714285714,
            "flow_unit": "Kilogram",
            "node_production_amount": 0.228,
            "node_production_unit": "Kilogram",
            "recipe_portions": 140,
            "production_portions": 300,
            "sold_portions": None,
            "production_date": "2023-06-20",
            "location": "CHE.26.13.21_1",
            "transport": "",
            "conservation": "generic conserved",
            "perishability": "highly-perishable",
            "processing": "",
            "is_combined_product": True,
            "is_subdivision": False,
            "is_dried": False,
            "nutrient_upscale_ratio": "",
            "matched_terms": "",
            "nutrient_file": "",
            "fao_code": "",
            "brightway_node": "",
            "amount_per_category_in_flow": (
                "Diet high in energy,Diet high in fat,Diet high in protein,Diet high in sodium (salt),"
                "Diet low in fat,Diet low in protein,Diet low in vegetables,Plant based products,Vegetables"
            ),
            "transport_mode_distances": "",
            "transport_mode_options": "",
            "matrix_gfm_error": "",
            "error_squares": "",
            "solution_status": "",
            "kg_co2eq_per_root_node": 0.00030300266567951105,
            "kg_co2eq_reduction_value_per_root_node": 0.09635997529042985,
            "co2_rating": "A",
            "co2_award": True,
            "co2_value_improvement_percentage": 99.68653700508058,
            "liter_scarce_water_consumption_per_root_node": 0.004759371919856457,
            "liter_scarce_water_consumption_reduction_value_per_root_node": 0.02771047941435188,
            "scarce_water_consumption_rating": "A",
            "scarce_water_consumption_award": True,
            "scarce_water_consumption_improvement_percentage": 85.34218136427911,
            "vita_score_points_per_root_node": 275.2189616644503,
            "diet_low_in_fruits_points_per_root_node": 61.24,
            "diet_low_in_whole_grains_points_per_root_node": 82.86,
            "diet_low_in_vegetables_points_per_root_node": 0.0,
            "diet_low_in_nuts_and_seeds_points_per_root_node": 13.98,
            "diet_low_in_milk_points_per_root_node": 9.42,
            "diet_low_in_fat_points_per_root_node": 86.53735866947304,
            "diet_low_in_protein_points_per_root_node": 21.181602994977325,
            "diet_high_in_sodium_(salt)_points_per_root_node": 0.0,
            "diet_high_in_processed_meat_points_per_root_node": 0.0,
            "diet_high_in_red_meat_points_per_root_node": 0.0,
            "diet_high_in_energy_points_per_root_node": 0.0,
            "diet_high_in_fat_points_per_root_node": 0.0,
            "diet_high_in_protein_points_per_root_node": 0.0,
            "vitascore_rating": "A",
            "vitascore_improvement_percentage": 19.05324656927932,
            "animal_welfare_rating": "A",
            "rainforest_critical_products_rating": "A",
            "kg_co2eq_only_all_transport_per_root_node": 6.104169438909773e-06,
            "kg_co2eq_only_all_processing_per_root_node": 0.0,
            "kg_co2eq_only_greenhouse_per_root_node": 0.0,
            "kg_co2eq_only_base_per_root_node": 0.00029689849624060127,
            "kg_co2eq_only_truck_transport_per_root_node": 6.104169438909773e-06,
            "kg_co2eq_only_sea_transport_per_root_node": 0.0,
            "kg_co2eq_only_air_transport_per_root_node": 0.0,
            "kg_co2eq_only_cooling_of_transport_per_root_node": 0.0,
            "kg_co2eq_only_freezing_of_transport_per_root_node": 0.0,
            "kg_co2eq_only_processing_dried_fruit_grinding_per_root_node": 0.0,
            "kg_co2eq_only_processing_grinding_per_root_node": 0.0,
            "kg_co2eq_only_processing_carbonating_per_root_node": 0.0,
            "kg_co2eq_only_processing_cutting_per_root_node": 0.0,
            "kg_co2eq_only_processing_shredding_per_root_node": 0.0,
            "kg_co2eq_only_processing_fermenting_per_root_node": 0.0,
            "kg_co2eq_only_processing_smoking_per_root_node": 0.0,
            "kg_co2eq_only_processing_puffing_per_root_node": 0.0,
            "kg_co2eq_only_processing_freeze-drying_per_root_node": 0.0,
            "kg_co2eq_only_processing_cooling_per_root_node": 0.0,
            "kg_co2eq_only_processing_baking_per_root_node": 0.0,
            "kg_co2eq_only_processing_jam_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_juice_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_yoghurt_production_per_root_node": 0.0,
            "kg_co2eq_only_processing_roasting_per_root_node": 0.0,
            "kg_co2eq_only_processing_chopping_per_root_node": 0.0,
            "kg_co2eq_only_processing_heating_per_root_node": 0.0,
            "kg_co2eq_only_processing_drying_per_root_node": 0.0,
            "kg_co2eq_only_processing_freezing_per_root_node": 0.0,
            "kg_co2eq_only_processing_mixing_per_root_node": 0.0,
            "kg_co2eq_only_processing_transportation_per_root_node": 0.0,
            "EOS_nutrients_sucrose_gram_per_root_node": 0.07749487354750507,
            "EOS_nutrients_chlorine_gram_per_root_node": 0.00026042378673957607,
            "EOS_nutrients_protein_gram_per_root_node": 0.014875256322624738,
            "EOS_nutrients_energy_kilocalorie_per_root_node": 0.4394157125768965,
            "EOS_nutrients_sodium_gram_per_root_node": 4.174641148325356e-05,
            "EOS_nutrients_fibers_gram_per_root_node": 0.02081339712918659,
            "EOS_nutrients_saturated_fat_gram_per_root_node": 0.0,
            "EOS_nutrients_fat_gram_per_root_node": 0.0027042036910457957,
            "EOS_nutrients_carbohydrates_gram_per_root_node": 0.07749487354750507,
            "EOS_nutrients_water_gram_per_root_node": 0.23261278195488716,
            "EOS_nutrients_source": "aggregated",
            "aggregated_daily_food_unit_per_root_node": 0.00017733397779469325,
        },
    ],
]

EXPECTED_ADMIN_FINAL_GRAPHS = copy.deepcopy(EXPECTED_SUPERUSER_FINAL_GRAPHS)
EXPECTED_ADMIN_FINAL_GRAPHS[0][0]["sold_portions"] = 250
EXPECTED_ADMIN_FINAL_GRAPHS[0][0]["xid"] = f"{EXPECTED_ADMIN_FINAL_GRAPHS[0][0]['xid']}_admin"
EXPECTED_ADMIN_FINAL_GRAPHS[0][0]["access_group_xid"] = EXPECTED_ADMIN_FINAL_GRAPHS[0][0]["access_group_xid"].replace(
    "id", "admin_id"
)
EXPECTED_ADMIN_FINAL_GRAPHS[1][0]["xid"] = f"{EXPECTED_ADMIN_FINAL_GRAPHS[1][0]['xid']}_admin"
EXPECTED_ADMIN_FINAL_GRAPHS[1][0]["access_group_xid"] = EXPECTED_ADMIN_FINAL_GRAPHS[1][0]["access_group_xid"].replace(
    "id", "admin_id"
)
EXPECTED_ADMIN_FINAL_GRAPHS[1][1]["xid"] = f"{EXPECTED_ADMIN_FINAL_GRAPHS[1][1]['xid']}_admin"


@pytest.mark.asyncio
async def test_setup_recipes_and_subrecipes_for_superuser_and_admin(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    (
        encoded_super_user_token,
        _,
        encoded_admin_user_token,
        admin_access_group_uid,
    ) = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get recipe with sub-recipe and ensure that cached sub-recipe is
        # not used, even if use_cached_final_root is true.
        batch_to_access = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_RECIPE_WITH_SUBRECIPE_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "use_cached_final_root": True,
                "return_final_graph": True,
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_access},
        )
        assert len(response.json()["batch"][0]["final_graph"]) == 39

        # Get superuser & admin supplies as superuser.
        response = await ac.post(
            f"/v2/nodes/get_nodes?start_date=2023-06-01&end_date=2023-06-30&node_type={SupplySheetActivityNode.__name__}",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
                {
                    "access_group": {"uid": admin_access_group_uid},
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        response_json = response.json()
        assert len(response_json) == 2
        assert all(node.get("uid") is not None for node in response_json)
        assert all(node.get("node_type") == SupplySheetActivityNode.__name__ for node in response_json)

        # Get superuser & admin supplies as admin.
        response = await ac.post(
            f"/v2/nodes/get_nodes?start_date=2023-06-01&end_date=2023-06-30&node_type={SupplySheetActivityNode.__name__}",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
                {
                    "access_group": {"uid": admin_access_group_uid},
                },
            ],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Get admin recipes as admin.
        response = await ac.post(
            f"/v2/nodes/get_nodes?start_date=2023-06-01&end_date=2023-06-30&node_type={FoodProcessingActivityNode.__name__}",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        response_json = response.json()
        assert len(response_json) == 2
        assert all(node.get("uid") is not None for node in response_json)
        assert all(node.get("node_type") == FoodProcessingActivityNode.__name__ for node in response_json)

        # Get admin supplies as admin.
        response = await ac.post(
            f"/v2/nodes/get_nodes?start_date=2023-06-01&end_date=2023-06-30&node_type={SupplySheetActivityNode.__name__}",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        response_json = response.json()
        assert len(response_json) == 1
        assert all(node.get("uid") is not None for node in response_json)
        assert all(node.get("node_type") == SupplySheetActivityNode.__name__ for node in response_json)

        # Get admin nodes as admin.
        response = await ac.post(
            "/v2/nodes/get_nodes?start_date=2023-06-01&end_date=2023-06-30",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        response_json = response.json()
        assert len(response_json) == 5

        # Get admin nodes on wrong dates as admin.
        response = await ac.post(
            "/v2/nodes/get_nodes?start_date=2024-06-01",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        response_json = response.json()
        assert len(response_json) == 0


@pytest.mark.asyncio
async def test_explicit_attachment_of_elementary_resource_emission_nodes(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    encoded_super_user_token, _, _, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get superuser flow nodes as superuser.
        response = await ac.post(
            f"/v2/nodes/get_nodes?node_type={FoodProductFlowNode.__name__}",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        response_json = response.json()
        recipe_node = [node for node in response_json if node["xid"] == TEST_RECIPE_FLOW_ID][0]
        recipe_node_uid = recipe_node["uid"]

        recipe_portions = TEST_RECIPE[0]["input_root"]["activity"]["recipe_portions"]

        node_controller = get_node_controller()
        calculation = Calculation(
            uid=uuid4(),
            linked_sub_node_infos=[
                LinkedSubNodeInfo(
                    uid=UUID(recipe_node_uid),
                    flow_amount=RawQuantity.unvalidated_construct(value=1.0, unit="production amount"),
                )
            ],
            use_cached_final_root=True,
            explicitly_attach_cached_elementary_resource_emission=True,
            return_final_graph=True,
        )
        result = await node_controller.perform_calculation(
            calculation=calculation, use_queue=False, priority=Priority.LOW
        )
        assert (
            len(
                [
                    flow_to_elem_resource_emission
                    for flow_to_elem_resource_emission in result.final_graph
                    if flow_to_elem_resource_emission["node_type"] == "FlowNode"
                ]
            )
            == 3
        ), "There should be 3 flows to the environmental flows"
        assert len(result.final_graph) == 9, "There should be 9 nodes in the graph."
        assert is_close(
            list(result.final_root.flow.impact_assessment.quantities.values())[0].value,
            TEST_RECIPE_EXPECTED_CO2 / recipe_portions,
        )


@pytest.mark.asyncio
async def test_reports_get_aggregated_superuser_data_as_superuser(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    (
        encoded_super_user_token,
        superuser_access_group_uid,
        _,
        admin_access_group_uid,
    ) = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get aggregated superuser data.
        response = await ac.post(
            "/v2/calculation/reports/aggregated-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&return_only_mustache_json=false",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )

        recipe_portions = TEST_RECIPE[0]["input_root"]["activity"]["recipe_portions"]
        recipe_with_subrecipe_portions = TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["recipe_portions"]

        # The sum of expected recipe and recipe with subrecipe CO2.
        assert is_close(
            find_quantity_value(
                response.json()["recipe"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 / recipe_with_subrecipe_portions,
        )

        # The sum of expected recipe and recipe with subrecipe DFU.
        assert is_close(
            response.json()["recipe"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU / recipe_with_subrecipe_portions,
        )

        # The expected CO2 of supply (there is only one in the whole access group).
        assert is_close(
            find_quantity_value(
                response.json()["supply"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_SUPPLY_EXPECTED_CO2,
        )

        # Expected daily_food_unit of supply
        assert is_close(
            response.json()["supply"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_SUPPLY_EXPECTED_DFU,
        )

        # There should not be any missing caches in the database for superuser:
        report_generation_controller = get_report_generation_controller()
        node_cache_infos = (
            await report_generation_controller.get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
                access_group_uids=[superuser_access_group_uid],
                start_date=date(year=2023, month=6, day=1),
                end_date=date(year=2023, month=6, day=30),
                node_type=FoodProcessingActivityNode.__name__,
            )
        )
        assert len(node_cache_infos) == 2
        assert all(cache_info.cache_present for cache_info in node_cache_infos)

        # There should be 1 missing cache in the database for admin user:
        node_cache_infos = (
            await report_generation_controller.get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
                access_group_uids=[admin_access_group_uid],
                start_date=date(year=2023, month=6, day=1),
                end_date=date(year=2023, month=6, day=30),
                node_type=FoodProcessingActivityNode.__name__,
            )
        )
        assert len(node_cache_infos) == 2
        assert len([cache_info for cache_info in node_cache_infos if cache_info.cache_present]) == 1

        assert is_dict_close(response.json()["mustache_json"], EXPECTED_MUSTACHE_JSON_USE_SUPPLIES)


@pytest.mark.asyncio
async def test_reports_get_aggregated_superuser_and_admin_data_as_superuser(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    (
        encoded_super_user_token,
        superuser_access_group_uid,
        _,
        admin_access_group_uid,
    ) = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get aggregated superuser & admin user data.
        response = await ac.post(
            "/v2/calculation/reports/aggregated-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&return_only_mustache_json=false",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )

        recipe_portions = TEST_RECIPE[0]["input_root"]["activity"]["recipe_portions"]
        recipe_with_subrecipe_portions = TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["recipe_portions"]

        # Twice the sum of expected recipe and recipe with subrecipe CO2.
        assert is_close(
            find_quantity_value(
                response.json()["recipe"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            2
            * (
                TEST_RECIPE_EXPECTED_CO2 / recipe_portions
                + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 / recipe_with_subrecipe_portions
            ),
        )

        # Twice the sum of expected recipe and recipe with subrecipe DFU.
        assert is_close(
            response.json()["recipe"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            2
            * (
                TEST_RECIPE_EXPECTED_DFU / recipe_portions
                + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU / recipe_with_subrecipe_portions
            ),
        )

        # Twice the expected CO2 of supply (one for admin and one for superuser).
        assert is_close(
            find_quantity_value(
                response.json()["supply"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            2 * TEST_SUPPLY_EXPECTED_CO2,
        )

        # Twice the expected DFU of supply (one for admin and one for superuser).
        assert is_close(
            response.json()["supply"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            2 * TEST_SUPPLY_EXPECTED_DFU,
        )

        # There should not be any missing caches in the database for superuser:
        report_generation_controller = get_report_generation_controller()
        node_cache_infos = (
            await report_generation_controller.get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
                access_group_uids=[superuser_access_group_uid, admin_access_group_uid],
                start_date=date(year=2023, month=6, day=1),
                end_date=date(year=2023, month=6, day=30),
                node_type=FoodProcessingActivityNode.__name__,
            )
        )
        assert len(node_cache_infos) == 4
        assert all(cache_info.cache_present for cache_info in node_cache_infos)

        expected_mustache_json_use_supplies = copy.deepcopy(EXPECTED_MUSTACHE_JSON_USE_SUPPLIES)

        # the same recipe is added twice, once for the superuser and once for the admin user,
        # each of the recipes have a different id but the same name and amounts
        expected_mustache_json_use_supplies["best-menus"]["table-data"].append(
            expected_mustache_json_use_supplies["best-menus"]["table-data"][0]
        )
        assert is_dict_close(
            response.json()["mustache_json"],
            expected_mustache_json_use_supplies,
            scale_by={
                "value": 2.0,
                "reduction-with-respect-to-average": 2.0,
                "co2-reduction-kilogram": 2.0,
                "category-weight-gram": 2.0,
                "ingredient-weight-gram": 2.0,
                "co2-value-gram": 2.0,
            },
        )


@pytest.mark.asyncio
async def test_reports_get_aggregated_superuser_and_admin_data_as_superuser_wrong_date_range(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    encoded_super_user_token, _, _, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get aggregated superuser & admin user data.
        response = await ac.post(
            "/v2/calculation/reports/aggregated-access-groups-data?start_date=2024-06-01&end_date=2024-06-30&return_only_mustache_json=false",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )

    assert response.json()["recipe"] is None
    assert response.json()["supply"] is None


@pytest.mark.asyncio
async def test_reports_get_aggregated_admin_data_as_admin(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    _, _, encoded_admin_user_token, admin_access_group_uid = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get aggregated admin data.
        response = await ac.post(
            "/v2/calculation/reports/aggregated-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&return_only_mustache_json=false",
            json=[{"access_group": {"uid": admin_access_group_uid}}],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )

        recipe_portions = TEST_RECIPE[0]["input_root"]["activity"]["recipe_portions"]
        recipe_with_subrecipe_portions = TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["recipe_portions"]

        # The sum of expected recipe and recipe with subrecipe CO2.
        assert is_close(
            find_quantity_value(
                response.json()["recipe"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 / recipe_with_subrecipe_portions,
        )

        # The sum of expected recipe and recipe with subrecipe DFU.
        assert is_close(
            response.json()["recipe"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU / recipe_with_subrecipe_portions,
        )

        # The expected CO2 of supply (there is only one in the whole access group).
        assert is_close(
            find_quantity_value(
                response.json()["supply"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_SUPPLY_EXPECTED_CO2,
        )

        # Expected daily_food_unit of supply
        assert is_close(
            response.json()["supply"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_SUPPLY_EXPECTED_DFU,
        )
        assert is_dict_close(response.json()["mustache_json"], EXPECTED_MUSTACHE_JSON_USE_SUPPLIES)


@pytest.mark.asyncio
async def test_reports_get_aggregated_superuser_and_admin_data_as_admin(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    (
        _,
        superuser_access_group_uid,
        encoded_admin_user_token,
        admin_access_group_uid,
    ) = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Attempt to get aggregated superuser & admin data.
        # Get aggregated admin data.
        response = await ac.post(
            "/v2/calculation/reports/aggregated-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&return_only_mustache_json=false",
            json=[
                {"access_group": {"uid": admin_access_group_uid}},
                {"access_group": {"uid": superuser_access_group_uid}},
            ],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        # Should be unauthorized.
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.asyncio
async def test_reports_get_aggregated_superuser_data_as_admin(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    _, superuser_access_group_uid, encoded_admin_user_token, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Attempt to get aggregated superuser data.
        response = await ac.post(
            "/v2/calculation/reports/aggregated-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&return_only_mustache_json=false",
            json=[{"access_group": {"uid": superuser_access_group_uid}}],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        # Should be unauthorized.
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.asyncio
async def test_supply_from_recipe_with_recipe_portions(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    encoded_super_user_token, _, _, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get aggregated superuser data.
        response = await ac.post(
            (
                "/v2/calculation/reports/aggregated-access-groups-data"
                "?start_date=2023-06-01"
                "&end_date=2023-06-30"
                "&supply_from_recipe_config=use_recipe_portions"
                "&return_only_mustache_json=false"
            ),
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("namespace"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )

        recipe_portions = TEST_RECIPE[0]["input_root"]["activity"]["recipe_portions"]
        recipe_with_subrecipe_portions = TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["recipe_portions"]

        # The sum of expected recipe and recipe with subrecipe CO2.
        assert is_close(
            find_quantity_value(
                response.json()["recipe"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 / recipe_with_subrecipe_portions,
        )

        # The sum of expected recipe and recipe with subrecipe DFU.
        assert is_close(
            response.json()["recipe"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU / recipe_with_subrecipe_portions,
        )

        # The expected CO2 of supply (recipe scaled by recipe portions).
        assert is_close(
            find_quantity_value(
                response.json()["supply"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2,
        )

        # Expected daily_food_unit of supply (recipe scaled by recipe portions).
        assert is_close(
            response.json()["supply"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU,
        )

        assert is_dict_close(response.json()["mustache_json"], EXPECTED_MUSTACHE_JSON_USE_RECIPES)


@pytest.mark.asyncio
async def test_supply_from_recipe_with_production_portions(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    encoded_super_user_token, _, _, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get aggregated superuser data.
        response = await ac.post(
            (
                "/v2/calculation/reports/aggregated-access-groups-data"
                "?start_date=2023-06-01"
                "&end_date=2023-06-30"
                "&supply_from_recipe_config=use_recipe_production_portions"
                "&return_only_mustache_json=false"
            ),
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("namespace"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )

        recipe_portions = TEST_RECIPE[0]["input_root"]["activity"]["recipe_portions"]
        recipe_with_subrecipe_portions = TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["recipe_portions"]

        # The sum of expected recipe and recipe with subrecipe CO2.
        assert is_close(
            find_quantity_value(
                response.json()["recipe"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 / recipe_with_subrecipe_portions,
        )

        # The sum of expected recipe and recipe with subrecipe DFU.
        assert is_close(
            response.json()["recipe"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU / recipe_with_subrecipe_portions,
        )

        recipe_scale_ratio = TEST_RECIPE[0]["input_root"]["activity"]["production_portions"] / recipe_portions
        recipe_with_subrecipe_scale_ratio = (
            TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["production_portions"]
            / recipe_with_subrecipe_portions
        )

        # The expected CO2 of supply (recipe scaled by recipe portions).
        assert is_close(
            find_quantity_value(
                response.json()["supply"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 * recipe_scale_ratio
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 * recipe_with_subrecipe_scale_ratio,
        )

        # Expected daily_food_unit of supply (recipe scaled by recipe portions).
        assert is_close(
            response.json()["supply"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU * recipe_scale_ratio
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU * recipe_with_subrecipe_scale_ratio,
        )
        assert is_dict_close(
            response.json()["mustache_json"],
            EXPECTED_MUSTACHE_JSON_USE_RECIPES,
            scale_by={
                "value": recipe_scale_ratio,
                "reduction-with-respect-to-average": recipe_scale_ratio,
                "co2-reduction-kilogram": recipe_scale_ratio,
                "category-weight-gram": recipe_scale_ratio,
                "ingredient-weight-gram": recipe_scale_ratio,
                "co2-value-gram": recipe_scale_ratio,
            },
        )
        assert response.json()["error_statistics"] == {
            "failed_node_uids": [],
            "number_of_successful_calculations": 4,
            "number_of_failed_calculations": 0,
        }


@pytest.mark.asyncio
async def test_supply_from_recipe_with_missing_sold_portions(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    encoded_super_user_token, _, _, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get aggregated superuser data.
        response = await ac.post(
            (
                "/v2/calculation/reports/aggregated-access-groups-data"
                "?start_date=2023-06-01"
                "&end_date=2023-06-30"
                "&supply_from_recipe_config=use_recipe_sold_portions"
                "&return_only_mustache_json=false"
            ),
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("namespace"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )

        assert response.status_code == status.HTTP_200_OK
        error_statistics = response.json()["error_statistics"]
        assert len(error_statistics["failed_node_uids"]) == 1
        failed_uid = error_statistics["failed_node_uids"][0]
        assert response.json()["error_statistics"] == {
            "failed_node_uids": [failed_uid],
            "number_of_successful_calculations": 3,
            "number_of_failed_calculations": 1,
        }


@pytest.mark.asyncio
async def test_supply_from_recipe_with_sold_portions(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    _, _, encoded_admin_user_token, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get aggregated superuser data.
        response = await ac.post(
            (
                "/v2/calculation/reports/aggregated-access-groups-data"
                "?start_date=2023-06-01"
                "&end_date=2023-06-30"
                "&supply_from_recipe_config=use_recipe_sold_portions"
                "&return_only_mustache_json=false"
            ),
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("namespace"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )

        recipe_portions = TEST_RECIPE[0]["input_root"]["activity"]["recipe_portions"]
        recipe_with_subrecipe_portions = TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["recipe_portions"]

        # The sum of expected recipe and recipe with subrecipe CO2.
        assert is_close(
            find_quantity_value(
                response.json()["recipe"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 / recipe_with_subrecipe_portions,
        )

        # The sum of expected recipe and recipe with subrecipe DFU.
        assert is_close(
            response.json()["recipe"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU / recipe_portions
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU / recipe_with_subrecipe_portions,
        )

        recipe_scale_ratio = TEST_RECIPE[0]["input_root"]["activity"]["sold_portions"] / recipe_portions
        recipe_with_subrecipe_scale_ratio = (
            TEST_RECIPE_WITH_SUBRECIPE[0]["input_root"]["activity"]["sold_portions"] / recipe_with_subrecipe_portions
        )

        # The expected CO2 of supply (recipe scaled by recipe portions).
        assert is_close(
            find_quantity_value(
                response.json()["supply"]["final_root"]["flow"]["impact_assessment"][
                    "amount_for_activity_production_amount"
                ],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_RECIPE_EXPECTED_CO2 * recipe_scale_ratio
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_CO2 * recipe_with_subrecipe_scale_ratio,
        )

        # Expected daily_food_unit of supply (recipe scaled by recipe portions).
        assert is_close(
            response.json()["supply"]["final_root"]["flow"]["daily_food_unit"]["amount_for_activity_production_amount"],
            TEST_RECIPE_EXPECTED_DFU * recipe_scale_ratio
            + TEST_RECIPE_WITH_SUBRECIPE_EXPECTED_DFU * recipe_with_subrecipe_scale_ratio,
        )
        assert is_dict_close(
            response.json()["mustache_json"],
            EXPECTED_MUSTACHE_JSON_USE_RECIPES,
            scale_by={
                "value": recipe_scale_ratio,
                "reduction-with-respect-to-average": recipe_scale_ratio,
                "co2-reduction-kilogram": recipe_scale_ratio,
                "category-weight-gram": recipe_scale_ratio,
                "ingredient-weight-gram": recipe_scale_ratio,
                "co2-value-gram": recipe_scale_ratio,
            },
        )


@pytest.mark.asyncio
async def test_reports_get_tabular_superuser_data_as_superuser(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    (
        encoded_super_user_token,
        _,
        _,
        _,
    ) = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get tabular superuser data.
        response = await ac.post(
            "/v2/calculation/reports/tabular-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&node_type=FoodProcessingActivityNode",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        diff_dict = deepdiff.DeepDiff(
            response.json()["final_graphs"],
            EXPECTED_SUPERUSER_FINAL_GRAPHS,
            ignore_order=True,
            verbose_level=2,
            significant_digits=6,
        )
        assert not diff_dict, diff_dict


@pytest.mark.asyncio
async def test_reports_get_tabular_superuser_and_admin_data_as_superuser(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    (
        encoded_super_user_token,
        _,
        _,
        _,
    ) = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get tabular superuser & admin user data.
        response = await ac.post(
            "/v2/calculation/reports/tabular-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&node_type=FoodProcessingActivityNode",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )
        expected_final_graphs = EXPECTED_SUPERUSER_FINAL_GRAPHS + EXPECTED_ADMIN_FINAL_GRAPHS

        diff_dict = deepdiff.DeepDiff(
            response.json()["final_graphs"],
            expected_final_graphs,
            ignore_order=True,
            verbose_level=2,
            significant_digits=6,
        )
        assert not diff_dict, diff_dict


@pytest.mark.asyncio
async def test_reports_get_tabular_superuser_and_admin_data_as_superuser_wrong_date_range(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    encoded_super_user_token, _, _, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get tabular superuser & admin user data.
        response = await ac.post(
            "/v2/calculation/reports/tabular-access-groups-data?start_date=2024-06-01&end_date=2024-06-30&node_type=FoodProcessingActivityNode",
            json=[
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
                {
                    "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
                    "namespace_uid": CUSTOMER.get("uid"),
                },
            ],
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
        )

        final_graphs = response.json()["final_graphs"]
        assert len(final_graphs) == 0


@pytest.mark.asyncio
async def test_reports_get_tabular_admin_data_as_admin(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    _, _, encoded_admin_user_token, admin_access_group_uid = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Get tabular admin data.
        response = await ac.post(
            "/v2/calculation/reports/tabular-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&node_type=FoodProcessingActivityNode",
            json=[{"access_group": {"uid": admin_access_group_uid}}],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )

        diff_dict = deepdiff.DeepDiff(
            response.json()["final_graphs"],
            EXPECTED_ADMIN_FINAL_GRAPHS,
            ignore_order=True,
            verbose_level=2,
            significant_digits=6,
        )
        assert not diff_dict, diff_dict


@pytest.mark.asyncio
async def test_reports_get_tabular_superuser_and_admin_data_as_admin(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    (
        _,
        superuser_access_group_uid,
        encoded_admin_user_token,
        admin_access_group_uid,
    ) = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Attempt to get tabular superuser & admin data.
        # Get tabular admin data.
        response = await ac.post(
            "/v2/calculation/reports/tabular-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&node_type=FoodProcessingActivityNode",
            json=[
                {"access_group": {"uid": admin_access_group_uid}},
                {"access_group": {"uid": superuser_access_group_uid}},
            ],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        # Should be unauthorized.
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.asyncio
async def test_reports_get_tabular_superuser_data_as_admin(
    app_module: FastAPI, setup_recipes_and_subrecipes_for_superuser_and_admin: Tuple[bytes, str, bytes, str]
) -> None:
    _, superuser_access_group_uid, encoded_admin_user_token, _ = setup_recipes_and_subrecipes_for_superuser_and_admin
    async with AsyncClient(
        app=app_module,
        base_url="http://localhost:8040",
    ) as ac:
        # Attempt to get tabular superuser data.
        response = await ac.post(
            "/v2/calculation/reports/tabular-access-groups-data?start_date=2023-06-01&end_date=2023-06-30&node_type=FoodProcessingActivityNode",
            json=[{"access_group": {"uid": superuser_access_group_uid}}],
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
        )
        # Should be unauthorized.
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
