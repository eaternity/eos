from typing import Tuple

import pytest
import pytest_asyncio
from fastapi import status
from httpx import AsyncClient

from api.tests.test_api_for_recipes import find_node_from_graph_by_xid, get_sub_nodes_from_graph
from core.domain.nodes import SupplySheetActivityNode
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator

from .conftest import (
    TEST_KITCHEN_BY_ADMIN,
    TEST_KITCHEN_BY_SUPERUSER,
    TEST_SUB_FOOD_PRODUCT_ID_01,
    TEST_SUB_FOOD_PRODUCT_ID_02,
    TEST_SUPPLY,
    TEST_SUPPLY_ID,
    USER_BASIC,
    USER_BASIC_ID,
    FastAPIReturn,
    change_in_every_activity_in_batch,
    change_in_every_item_in_batch,
    check_mutation_log,
    check_response_codes_in_batch_calculation,
    find_quantity_value,
    is_close,
)

service_locator = ServiceLocator()

TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_01 = 7.0 / 3.0
TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_02 = 6.0 / 4.0

TEST_SUPPLY_EXPECTED_CO2_ING1 = 0.03386590174933045  # update 31.07.2024: fixed bug in fao-statistics of onion
TEST_SUPPLY_EXPECTED_CO2_ING2 = 0.038864031019988476  # update 31.07.2024: fixed bug in fao-statistics of onion
TEST_SUPPLY_EXPECTED_CO2 = TEST_SUPPLY_EXPECTED_CO2_ING1 + TEST_SUPPLY_EXPECTED_CO2_ING2

TEST_SUPPLY_EXPECTED_DFU_ING1 = 0.03609367515272727
TEST_SUPPLY_EXPECTED_DFU_ING2 = 0.06748507565672726
TEST_SUPPLY_EXPECTED_DFU = TEST_SUPPLY_EXPECTED_DFU_ING1 + TEST_SUPPLY_EXPECTED_DFU_ING2

PROPS_UNUSED = (
    "processing",
    "invoice_id",
    "supplier_id",
    "supply_date",
    "supplier",
)
TEST_SUPPLY_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_SUPPLY[0]["input_root"]["activity"]["node_type"],
        "raw_input": {key: val for key, val in TEST_SUPPLY[0]["input_root"]["activity"].items() if key in PROPS_UNUSED},
        **{
            key: val
            for key, val in TEST_SUPPLY[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
        # transport is moved back to raw_input for activity nodes since no GFM uses transport on activity nodes.
    },
}


@pytest.mark.asyncio
async def test_create_delete_and_get_supply_by_superuser(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, encoded_basic_user_token = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser

        TEST_SUPPLY_WITH_XID = change_in_every_activity_in_batch(
            TEST_SUPPLY, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_SUPPLY_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_SUPPLY_EXPECTED_CO2,
        )

        assert resp_node.get("node_type") == SupplySheetActivityNode.__name__

        # check that the ingredient-amount-estimation worked as expected
        final_graph = batch[0]["final_graph"]
        top_recipe = find_node_from_graph_by_xid(final_graph, TEST_SUPPLY_ID)
        sub_flows_with_ingredient_declaration = get_sub_nodes_from_graph(final_graph, top_recipe)
        # for sub-recipe 1
        activity_in_between = get_sub_nodes_from_graph(final_graph, sub_flows_with_ingredient_declaration[0])
        ingredients = get_sub_nodes_from_graph(final_graph, activity_in_between[0])
        assert is_close(
            ingredients[0].get("amount").get("amount_for_activity_production_amount")
            / ingredients[1].get("amount").get("amount_for_activity_production_amount"),
            TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_01,
            1e-2,
        )
        # for sub-recipe 2
        activity_in_between = get_sub_nodes_from_graph(final_graph, sub_flows_with_ingredient_declaration[1])
        ingredients = get_sub_nodes_from_graph(final_graph, activity_in_between[0])
        assert is_close(
            ingredients[0].get("amount").get("amount_for_activity_production_amount")
            / ingredients[1].get("amount").get("amount_for_activity_production_amount"),
            TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_02,
            1e-2,
        )

        # testing deletion
        node_to_delete_uid = resp_node.get("uid")
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "uid": node_to_delete_uid,
                        "access_group_uid": test_kitchen_by_superuser_uid,
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_SUPPLY_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_SUPPLY_EXPECTED_CO2,
        )
        # check that the ingredient-amount-estimation worked as expected
        final_graph = batch[0]["final_graph"]
        top_recipe = find_node_from_graph_by_xid(final_graph, TEST_SUPPLY_ID)
        sub_flows_with_ingredient_declaration = get_sub_nodes_from_graph(final_graph, top_recipe)
        # for sub-recipe 1
        activity_in_between = get_sub_nodes_from_graph(final_graph, sub_flows_with_ingredient_declaration[0])
        ingredients = get_sub_nodes_from_graph(final_graph, activity_in_between[0])
        assert is_close(
            ingredients[0].get("amount").get("amount_for_activity_production_amount")
            / ingredients[1].get("amount").get("amount_for_activity_production_amount"),
            TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_01,
            1e-2,
        )
        # for sub-recipe 2
        activity_in_between = get_sub_nodes_from_graph(final_graph, sub_flows_with_ingredient_declaration[1])
        ingredients = get_sub_nodes_from_graph(final_graph, activity_in_between[0])
        assert is_close(
            ingredients[0].get("amount").get("amount_for_activity_production_amount")
            / ingredients[1].get("amount").get("amount_for_activity_production_amount"),
            TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_02,
            1e-2,
        )
        check_mutation_log(response, TEST_SUPPLY_MUTATION_LOG)

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "members": members,
                "access_group": {"uid": test_kitchen_by_superuser_uid},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": None, "uid": test_kitchen_by_superuser_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_superuser_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_superuser_uid
        assert response.json().get("status") is True

        # and check if they can get this supply
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_SUPPLY_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_SUPPLY_EXPECTED_CO2,
        )


# this test is encapsulated into a fixture
# for using the final state in `test_get_..._node_types_from_access_group` tests
@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_supply_by_admin(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> str:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        TEST_SUPPLY_WITH_XID = change_in_every_activity_in_batch(
            TEST_SUPPLY, "access_group_xid", TEST_KITCHEN_BY_ADMIN.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_SUPPLY_WITH_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_SUPPLY_EXPECTED_CO2,
        )
        assert resp_node.get("node_type") == SupplySheetActivityNode.__name__
        # check that the ingredient-amount-estimation worked as expected
        final_graph = batch[0]["final_graph"]
        top_recipe = find_node_from_graph_by_xid(final_graph, TEST_SUPPLY_ID)
        sub_flows_with_ingredient_declaration = get_sub_nodes_from_graph(final_graph, top_recipe)
        # for sub-recipe 1
        activity_in_between = get_sub_nodes_from_graph(final_graph, sub_flows_with_ingredient_declaration[0])
        ingredients = get_sub_nodes_from_graph(final_graph, activity_in_between[0])
        assert is_close(
            ingredients[0].get("amount").get("amount_for_activity_production_amount")
            / ingredients[1].get("amount").get("amount_for_activity_production_amount"),
            TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_01,
            1e-2,
        )
        # for sub-recipe 2
        activity_in_between = get_sub_nodes_from_graph(final_graph, sub_flows_with_ingredient_declaration[1])
        ingredients = get_sub_nodes_from_graph(final_graph, activity_in_between[0])
        assert is_close(
            ingredients[0].get("amount").get("amount_for_activity_production_amount")
            / ingredients[1].get("amount").get("amount_for_activity_production_amount"),
            TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_02,
            1e-2,
        )

        # testing deletion
        node_to_delete_uid = resp_node.get("uid")
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "uid": node_to_delete_uid,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )

        check_response_codes_in_batch_calculation(response, 200)
        # TODO: no test yet whether the node was really deleted

        # testing mutation log response
        batch_with_return_log = change_in_every_item_in_batch(TEST_SUPPLY_WITH_XID, "return_log", True)
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_with_return_log},
        )
        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_SUPPLY_EXPECTED_CO2,
        )
        # check that the ingredient-amount-estimation worked as expected
        final_graph = batch[0]["final_graph"]
        top_recipe = find_node_from_graph_by_xid(final_graph, TEST_SUPPLY_ID)
        sub_flows_with_ingredient_declaration = get_sub_nodes_from_graph(final_graph, top_recipe)
        # for sub-recipe 1
        activity_in_between = get_sub_nodes_from_graph(final_graph, sub_flows_with_ingredient_declaration[0])
        ingredients = get_sub_nodes_from_graph(final_graph, activity_in_between[0])
        assert is_close(
            ingredients[0].get("amount").get("amount_for_activity_production_amount")
            / ingredients[1].get("amount").get("amount_for_activity_production_amount"),
            TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_01,
            1e-2,
        )
        # for sub-recipe 2
        activity_in_between = get_sub_nodes_from_graph(final_graph, sub_flows_with_ingredient_declaration[1])
        ingredients = get_sub_nodes_from_graph(final_graph, activity_in_between[0])
        assert is_close(
            ingredients[0].get("amount").get("amount_for_activity_production_amount")
            / ingredients[1].get("amount").get("amount_for_activity_production_amount"),
            TEST_SUPPLY_EXPECTED_RATIO_SUB_FOOD_PRODUCT_02,
            1e-2,
        )

        check_mutation_log(response, TEST_SUPPLY_MUTATION_LOG)

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "access_group": {"uid": test_kitchen_by_admin_uid},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": None, "uid": test_kitchen_by_admin_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_admin_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_admin_uid
        assert response.json().get("status") is True

        # and check if they can get this supply
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_SUPPLY_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            },
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_SUB_FOOD_PRODUCT_ID_01,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            },
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_SUB_FOOD_PRODUCT_ID_02,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    },
                },
            },
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_activity_production_amount"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_SUPPLY_EXPECTED_CO2,
        )

        final_root1 = response.json().get("batch")[0].get("final_root").get("activity", {})
        final_root2 = response.json().get("batch")[1].get("final_root").get("activity", {})
        final_root3 = response.json().get("batch")[2].get("final_root").get("activity", {})
        assert final_root1.get("uid") is not None

        return final_root1.get("uid"), final_root2.get("uid"), final_root3.get("uid")


@pytest.mark.asyncio
async def test_create_delete_and_get_supply_by_admin(create_delete_and_get_supply_by_admin: str) -> None:
    _ = create_delete_and_get_supply_by_admin
