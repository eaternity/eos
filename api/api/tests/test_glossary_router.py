import copy
import uuid
from typing import Tuple

import pytest
import pytest_asyncio
from fastapi import status
from httpx import AsyncClient
from structlog import get_logger

from api.app.settings import Settings
from api.tests.conftest import USER_BASIC, USER_BASIC_ID, FastAPIReturn
from core.domain.user import UserPermissionsEnum
from core.service.namespace_service import NamespaceService
from core.service.service_provider import ServiceLocator

logger = get_logger()
settings = Settings()
service_locator = ServiceLocator()

CARROT_XID = "A1791"
CARROT_UNIT_WEIGHT = 62.1
PRODUCT_TYPE_XID = "A0361"


@pytest.mark.asyncio
async def test_get_full_glossary(app: FastAPIReturn) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        response = await ac.get("/v2/glossary/", headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"})
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        all_terms = response.json().get("terms")
        assert len(all_terms) > 13000
        assert len(all_terms) < 13500


@pytest.mark.asyncio
async def test_get_subtree_glossary(app: FastAPIReturn, term_access_group_uids: Tuple[str, str, str, str]) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        # get UUID of "Product type" term
        response = await ac.get(
            f"/v2/glossary/by-xid/{PRODUCT_TYPE_XID}/{foodex2_term_group_uid}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        product_type_term_uid = response.json().get("uid")

        # get subterms of "Product type" term
        response = await ac.get(
            f"/v2/glossary/subtree/?term_uid={product_type_term_uid}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        num_product_terms = len(response.json().get("terms", []))
        assert num_product_terms < 7000
        assert num_product_terms > 6000

        # test receiving an empty response
        response = await ac.get(
            f"/v2/glossary/subtree/?term_uid={str(uuid.uuid4())}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert len(response.json().get("terms", [])) == 0


@pytest.mark.asyncio
async def test_get_nonexistent_glossary_term(
    app: FastAPIReturn, term_access_group_uids: Tuple[str, str, str, str]
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        invalid_xid = "INVALID"

        response = await ac.get(
            f"/v2/glossary/by-xid/{invalid_xid}", headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"}
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": f"Terms with XID '{invalid_xid}' do not exist."}

        response = await ac.get(
            f"/v2/glossary/by-xid/{invalid_xid}/{foodex2_term_group_uid}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": "term does not exist."}


@pytest_asyncio.fixture(scope="function")
async def modify_glossary(
    app: FastAPIReturn, term_access_group_uids: Tuple[str, str, str, str]
) -> Tuple[dict, uuid.UUID]:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        response = await ac.get(
            f"/v2/glossary/by-xid/{CARROT_XID}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        carrot_term = response.json()

        initial_carrot_term = copy.deepcopy(carrot_term)
        carrot_term_uid = carrot_term.get("uid")

        # test adding some more data
        carrot_term["data"]["more_data"] = "dummy_data"

        # test adding some more data via XID
        response = await ac.put(
            f"/v2/glossary/{carrot_term_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.get(
            f"/v2/glossary/by-xid/{CARROT_XID}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        reloaded_carrot_term = response.json()
        assert reloaded_carrot_term["data"]["more_data"] == "dummy_data"

        carrot_term["data"]["more_data"] = "extra_dummy_data"

        # test adding some more data via XID
        response = await ac.put(
            f"/v2/glossary/by-xid/{CARROT_XID}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.get(
            f"/v2/glossary/by-xid/{CARROT_XID}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        reloaded_carrot_term = response.json()
        assert reloaded_carrot_term["data"]["more_data"] == "extra_dummy_data"

        # test posting a new term
        new_xid = "NEWXID"
        carrot_term["data"]["more_data"] = "more_dummy_data"
        carrot_term["xid"] = new_xid

        response = await ac.post(
            "/v2/glossary/",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.get(
            f"/v2/glossary/by-xid/{new_xid}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        reloaded_carrot_term = response.json()
        assert reloaded_carrot_term["data"]["more_data"] == "more_dummy_data"

        return initial_carrot_term, carrot_term_uid


@pytest.mark.asyncio
async def test_modify_glossary(modify_glossary: Tuple[dict, uuid.UUID]) -> None:
    _ = modify_glossary
    return


@pytest.mark.asyncio
async def test_put_term_with_empty_dto(app: FastAPIReturn, term_access_group_uids: Tuple[str, str, str, str]) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        response = await ac.get(
            f"/v2/glossary/by-xid/{CARROT_XID}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        original_carrot_term = response.json()

        response = await ac.put(
            f"/v2/glossary/by-xid/{CARROT_XID}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "name": original_carrot_term.get("name"),
            },
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

        response = await ac.put(
            f"/v2/glossary/by-xid/{CARROT_XID}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "sub_class_of": original_carrot_term.get("sub_class_of"),
            },
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

        response = await ac.put(
            f"/v2/glossary/by-xid/{CARROT_XID}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "name": original_carrot_term.get("name"),
                "sub_class_of": "",
            },
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        fake_sub_class = "asd-asd-asd-asd"

        response = await ac.put(
            f"/v2/glossary/by-xid/{CARROT_XID}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "name": original_carrot_term.get("name"),
                "sub_class_of": fake_sub_class,
            },
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        fake_sub_class = "00000000-0000-0000-0000-000000000000"

        response = await ac.put(
            f"/v2/glossary/by-xid/{CARROT_XID}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "name": original_carrot_term.get("name"),
                "sub_class_of": fake_sub_class,
            },
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.asyncio
async def test_put_term_with_bare_minimum_dto(
    app: FastAPIReturn, term_access_group_uids: Tuple[str, str, str, str]
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        response = await ac.get(
            f"/v2/glossary/by-xid/{CARROT_XID}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        original_carrot_term = response.json()

        # this request returns 400 since its body doesn't specify `access_group_uid` JSON field
        response = await ac.put(
            f"/v2/glossary/by-xid/{CARROT_XID}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "name": original_carrot_term.get("name"),
                "sub_class_of": original_carrot_term.get("sub_class_of"),
            },
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        response = await ac.put(
            f"/v2/glossary/by-xid/{CARROT_XID}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "name": original_carrot_term.get("name"),
                "sub_class_of": original_carrot_term.get("sub_class_of"),
                "access_group_uid": original_carrot_term.get("access_group_uid"),
            },
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.get(
            f"/v2/glossary/by-xid/{CARROT_XID}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json() == original_carrot_term


@pytest.mark.asyncio
async def test_put_term_with_nonexistent_xid(
    app: FastAPIReturn, term_access_group_uids: Tuple[str, str, str, str]
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        _, _, foodex2_term_group_uid, _ = term_access_group_uids

        response = await ac.get(
            f"/v2/glossary/by-xid/{CARROT_XID}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        original_carrot_term = response.json()

        new_xid = "ANOTHERNEWXID"
        original_carrot_term["data"]["more_data"] = "this is a test term"
        original_carrot_term["xid"] = new_xid

        response = await ac.put(
            f"/v2/glossary/by-xid/{new_xid}",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json=original_carrot_term,
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.get(
            f"/v2/glossary/by-xid/{new_xid}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None

        # since this new term has a different UUID,
        # we have to change the original dict's UUID for successful comparison
        original_carrot_term["uid"] = response.json().get("uid")
        assert response.json() == original_carrot_term


@pytest.mark.asyncio
async def test_glossary_modification_inaccessibility_without_permissions(
    app: FastAPIReturn,
    create_users: Tuple[bytes, bytes, bytes],
    modify_glossary: Tuple[dict, uuid.UUID],
    term_access_group_uids: Tuple[str, str, str, str],
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, encoded_basic_user_token = create_users
        _, _, foodex2_term_group_uid, _ = term_access_group_uids
        carrot_term, carrot_term_uid = modify_glossary

        # test that both POST and PUT requests are forbidden
        response = await ac.post(
            "/v2/glossary/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.put(
            f"/v2/glossary/{carrot_term_uid}",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        # add permission for POST request
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.create_terms: True,
                },
            },
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "members": members,
                "access_group": {"uid": foodex2_term_group_uid},
            },
        )

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"uid": foodex2_term_group_uid, "xid": None},
            "namespace_uid": None,
        }

        # set FoodEx2 general access group as the default one for basic user
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": foodex2_term_group_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == foodex2_term_group_uid
        assert response.json().get("status") is True

        # test if POST request works and PUT request doesn't
        new_carrot = copy.deepcopy(carrot_term)
        new_carrot["xid"] = "NEW_XID_TO_PREVENT_HAVING_DUPLICATE_KEYS"
        response = await ac.post(
            "/v2/glossary/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=new_carrot,
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.put(
            f"/v2/glossary/{carrot_term_uid}",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        # add PUT request permission instead of POST request permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.edit_terms: True,
                },
            },
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "members": members,
                "access_group": {"uid": foodex2_term_group_uid},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"uid": foodex2_term_group_uid, "xid": None},
            "namespace_uid": None,
        }

        # test if POST request doesn't work and PUT request does
        response = await ac.post(
            "/v2/glossary/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "You don't have rights for this action."}

        response = await ac.put(
            f"/v2/glossary/{carrot_term_uid}",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_200_OK

        # add both POST and PUT requests permissions
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.create_terms: True,
                    UserPermissionsEnum.edit_terms: True,
                },
            },
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
            json={
                "members": members,
                "access_group": {"uid": foodex2_term_group_uid},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"uid": foodex2_term_group_uid, "xid": None},
            "namespace_uid": None,
        }

        # test if both POST request and PUT request work
        new_carrot = copy.deepcopy(carrot_term)
        new_carrot["xid"] = "NEW_XID_TO_PREVENT_HAVING_DUPLICATE_KEYS2"
        response = await ac.post(
            "/v2/glossary/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=new_carrot,
        )
        assert response.status_code == status.HTTP_200_OK

        response = await ac.put(
            f"/v2/glossary/{carrot_term_uid}",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json=carrot_term,
        )
        assert response.status_code == status.HTTP_200_OK


async def check_term_namespace(
    ac: AsyncClient,
    namespace_service: NamespaceService,
    access_group_uid: str,
    namespace_name: str,
) -> None:
    response = await ac.post(
        "/v2/access-groups/get_access_group_by_id",
        headers={"Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}"},
        json={"access_group": {"uid": access_group_uid}},
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json() is not None
    namespace_uid = response.json().get("namespace_uid")

    namespace_info = await namespace_service.find_namespace_by_uid(namespace_uid)
    assert namespace_info.name == namespace_name


@pytest.mark.asyncio
async def test_glossary_terms_having_correct_access_groups(
    app: FastAPIReturn, term_access_group_uids: Tuple[str, str, str, str]
) -> None:
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await service_locator.service_provider.postgres_db.connect(schema="test_pg")

        eaternity_term_group_uid, eurofir_term_group_uid, foodex2_term_group_uid, _ = term_access_group_uids

        # testing FoodEx2 namespace

        response = await ac.get(
            f"/v2/glossary/by-xid/{CARROT_XID}/{foodex2_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        await check_term_namespace(
            ac,
            service_locator.service_provider.namespace_service,
            response.json().get("access_group_uid"),
            "FoodEx2 Glossary Terms namespace",
        )

        # testing EuroFIR namespace

        response = await ac.get(
            f"/v2/glossary/by-xid/Root_Nutrients/{eurofir_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        await check_term_namespace(
            ac,
            service_locator.service_provider.namespace_service,
            response.json().get("access_group_uid"),
            "EuroFIR Glossary Terms namespace",
        )

        # testing Eaternity default namespace

        response = await ac.get(
            f"/v2/glossary/by-xid/EOS_electricity/{eaternity_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        await check_term_namespace(
            ac,
            service_locator.service_provider.namespace_service,
            response.json().get("access_group_uid"),
            "eaternity",
        )

        # testing basic term

        response = await ac.get(
            f"/v2/glossary/by-xid/EOS_data_error/{eaternity_term_group_uid}",
            headers={
                "Authorization": f"Basic {settings.EATERNITY_AUTH_KEY}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() is not None
        assert response.json().get("name") == "data error"
        await check_term_namespace(
            ac,
            service_locator.service_provider.namespace_service,
            response.json().get("access_group_uid"),
            "eaternity",
        )
