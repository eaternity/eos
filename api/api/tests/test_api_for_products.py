import uuid
from typing import Tuple

import pytest
import pytest_asyncio
from fastapi import status
from httpx import AsyncClient

from core.domain.nodes import FoodProcessingActivityNode, FoodProductFlowNode
from core.domain.user import UserPermissionsEnum
from core.service.service_provider import ServiceLocator

from .conftest import (
    CUSTOMER,
    TEST_KITCHEN_BY_ADMIN,
    TEST_KITCHEN_BY_SUPERUSER,
    USER_BASIC,
    USER_BASIC_ID,
    FastAPIReturn,
    change_in_every_activity_in_batch,
    change_in_every_item_in_batch,
    check_mutation_log,
    check_response_codes_in_batch_calculation,
    find_quantity_value,
    is_close,
)

service_locator = ServiceLocator()

TEST_PRODUCT_ID = "product01"
TEST_PRODUCT_ROOT_FLOW_ID = "root_flow_product01"

TEST_PRODUCT_EXPECTED_CO2 = 0.15657958919637416
TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW = TEST_PRODUCT_EXPECTED_CO2 / 2.0
# Currently we don't have mocked the ecotransit-response for all origins of the tomato.
# Transport is only added for those countries where we have mocked the ecotransit response.
# 5.2.2024: value changed from 0.0011682613319559231 to 0.0012772935540758409 because of added cooling to transport
# of perishable products (the carrot in this case)
# 6.2.: value changed from 0.0012772935540758409 to 0.001297974541645213 because of changed travel speeds and
# therefore different co2 values for cooling
# 14.2: value changed from 0.001297974541645213 to 0.006295863591951494 because we have added an fao-term for tomato
# leading to an origin split and to transports from those origins.
# 19.3: changed amount_in_original_source_unit from 200 to 100 & request amount_for_root_node to make sure that the
# information from the root-flow are picked up everywhere
# 21.3: changed behavior of "production": "standard" -> now greenhouse is added
TEST_PRODUCT_EXPECTED_DFU = 0.09285620811

# Fields that are not picked up by Kale are commented out.
TEST_PRODUCT = [
    {
        "input_root": {
            "flow": {
                "node_type": FoodProductFlowNode.__name__,
                "xid": TEST_PRODUCT_ROOT_FLOW_ID,
                "nutrient_values": {
                    "energy_kcal": 200,
                    "fat_gram": 12.3,
                    "saturated_fat_gram": 2.5,
                    "carbohydrates_gram": 8.4,
                    "sucrose_gram": 3.2,
                    "protein_gram": 2.2,
                    "sodium_chloride_gram": 0.3,
                },
                "raw_production": {"value": "standard", "language": "en"},
                "amount_in_original_source_unit": {"value": 100, "unit": "gram"},
                "flow_location": "paris, frankreich",
                # "conservation": "fresh", -> to check: is this picked up?
                # "transport": "ground",
            },
            "activity": {
                "node_type": FoodProcessingActivityNode.__name__,
                "activity_date": "2023-06-20",
                "xid": TEST_PRODUCT_ID,
                # "activity_location": "paris, frankreich",
                # "date": "2020-02-02",
                # "gtin": "00123456789023",
                "name": [
                    {
                        "language": "de",
                        "value": "Karottenpuree",
                    },
                ],
                "production_amount": {"value": 200, "unit": "gram"},
                # "producer": "hipp",
                # "processing": "raw",
                # "packaging": "none",
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "ingredients_declaration": [
                        {
                            "language": "de",
                            "value": "Karotten, Tomaten",
                        },
                    ],
                }
            ],
        },
        "requested_quantity_references": ["amount_for_root_node"],
    }
]

PROPS_UNUSED = ("gtin", "producer", "processing", "packaging", "transport", "raw_conservation", "name", "date")

TEST_PRODUCT_MUTATION_LOG = {
    "created_by_module": "Orchestrator",
    "new_node": {
        "node_type": TEST_PRODUCT[0]["input_root"]["activity"]["node_type"],
        "raw_input": {
            key: val for key, val in TEST_PRODUCT[0]["input_root"]["activity"].items() if key in PROPS_UNUSED
        },
        **{
            key: val
            for key, val in TEST_PRODUCT[0]["input_root"]["activity"].items()
            if key not in PROPS_UNUSED + ("xid", "node_type")
        },
        # transport is moved back to raw_input for activity nodes since no GFM uses transport on activity nodes.
    },
}


@pytest.mark.asyncio
async def test_create_delete_and_get_product_by_superuser_and_calc_router_endpoints(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_superuser: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        encoded_super_user_token, _, encoded_basic_user_token = create_users
        test_kitchen_by_superuser_uid = create_and_get_and_delete_and_create_kitchen_by_superuser

        TEST_PRODUCT_WITH_GROUP_XID = change_in_every_activity_in_batch(
            TEST_PRODUCT, "access_group_xid", TEST_KITCHEN_BY_SUPERUSER.get("xid")
        )

        # test all possible combinations for POST endpoint
        response = await ac.post(
            "/v2/calculation/graphs?use_queue=true",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None

        # TODO: Probably it would be better to add all required ecotransit responses as mocked jsons
        # to prevent this error:
        data_errors = response.json()["batch"][0]["data_errors"]
        assert data_errors[0]["message"] == (
            "Cannot connect to EcoTransit!"  # the other errors come from Meteo-data not being mocked for all countries
        )
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        TEST_PRODUCT_WITH_GROUP_XID_AND_NAMESPACE_UID = change_in_every_item_in_batch(
            TEST_PRODUCT_WITH_GROUP_XID, "namespace_uid", CUSTOMER.get("uid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_XID_AND_NAMESPACE_UID},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        TEST_PRODUCT_WITH_GROUP_UID = change_in_every_activity_in_batch(
            TEST_PRODUCT, "access_group_uid", test_kitchen_by_superuser_uid
        )
        TEST_PRODUCT_WITH_GROUP_UID_AND_NAMESPACE = change_in_every_item_in_batch(
            TEST_PRODUCT_WITH_GROUP_UID, "namespace_uid", CUSTOMER.get("uid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_UID_AND_NAMESPACE},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        TEST_PRODUCT_WITH_UID_AND_GROUP_UID = change_in_every_activity_in_batch(
            TEST_PRODUCT_WITH_GROUP_UID, "uid", str(uuid.uuid4())
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_UID_AND_GROUP_UID},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None

        batch = response.json().get("batch")
        resp_node = batch[0]["final_root"]["activity"]
        assert resp_node.get("node_type") == FoodProcessingActivityNode.__name__
        assert resp_node.get("raw_input").get("name")[0].get("value") == TEST_PRODUCT[0].get("input_root").get(
            "activity"
        ).get("name")[0].get("value")

        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )
        # testing deletion
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_PRODUCT_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)
        # TODO: find a way to check if the deletion was successful

        # testing mutation log response for all possible combinations
        # with node_uid and group_xid
        TEST_PRODUCT_WITH_UID_AND_GROUP_XID = change_in_every_activity_in_batch(
            TEST_PRODUCT_WITH_GROUP_XID,
            "uid",
            TEST_PRODUCT_WITH_UID_AND_GROUP_UID[0]["input_root"]["activity"]["uid"],
        )
        TEST_PRODUCT_WITH_UID_AND_GROUP_XID_RETRUN_LOG = change_in_every_item_in_batch(
            TEST_PRODUCT_WITH_UID_AND_GROUP_XID, "return_log", True
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_UID_AND_GROUP_XID_RETRUN_LOG},
        )

        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_PRODUCT_MUTATION_LOG)

        # with node_xid and group_xid
        TEST_PRODUCT_WITH_GROUP_XID_RETRUN_LOG = change_in_every_item_in_batch(
            TEST_PRODUCT_WITH_GROUP_XID, "return_log", True
        )
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_XID_RETRUN_LOG},
        )

        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_PRODUCT_MUTATION_LOG)

        # with node_xid and group_uid
        TEST_PRODUCT_WITH_GROUP_UID_RETRUN_LOG = change_in_every_item_in_batch(
            TEST_PRODUCT_WITH_GROUP_UID, "return_log", True
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_UID_RETRUN_LOG},
        )

        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_PRODUCT_MUTATION_LOG)

        # with node_xid and group_xid and namespace
        TEST_PRODUCT_WITH_GROUP_XID_AND_NAMESPACE_UID_RETURN_LOG = change_in_every_item_in_batch(
            TEST_PRODUCT_WITH_GROUP_XID_AND_NAMESPACE_UID, "return_log", True
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_XID_AND_NAMESPACE_UID_RETURN_LOG},
        )

        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_PRODUCT_MUTATION_LOG)

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_super_user_token}"},
            json={
                "members": members,
                "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"), "uid": test_kitchen_by_superuser_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_superuser_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_superuser_uid
        assert response.json().get("status") is True

        # and check if they can access this product
        # with xid and group_xid
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_PRODUCT_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "requested_quantity_references": ["amount_for_root_node"],
            },
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )
        # and the same for the root-flow
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_PRODUCT_ROOT_FLOW_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "requested_quantity_references": ["amount_for_root_node"],
            },
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # with xid, group_xid and namespace
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_PRODUCT_ID,
                        "access_group_xid": TEST_KITCHEN_BY_SUPERUSER.get("xid"),
                    },
                },
                "namespace_uid": CUSTOMER.get("uid"),
                "requested_quantity_references": ["amount_for_root_node"],
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # with xid and group_uid
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_PRODUCT_ID,
                        "access_group_uid": test_kitchen_by_superuser_uid,
                    },
                },
                "requested_quantity_references": ["amount_for_root_node"],
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # with xid
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_PRODUCT_ID,
                    },
                },
                "requested_quantity_references": ["amount_for_root_node"],
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # with uid
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "uid": response.json().get("batch")[0].get("final_root").get("activity").get("uid"),
                    },
                },
                "requested_quantity_references": ["amount_for_root_node"],
            }
        ]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # check if they can't post a new product
        TEST_PRODUCT_WITH_GROUP_XID_AND_NEW_XID = change_in_every_activity_in_batch(
            TEST_PRODUCT_WITH_GROUP_XID, "xid", "random_xid"
        )
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_XID_AND_NEW_XID},
        )
        check_response_codes_in_batch_calculation(response, 401)


# this test is encapsulated into a fixture
# for using the final state in `test_get_..._node_types_from_access_group` tests
@pytest_asyncio.fixture(scope="function")
async def create_delete_and_get_product_by_admin(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> str:
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users
        test_kitchen_by_admin_uid = create_and_get_and_delete_and_create_kitchen_by_admin

        TEST_PRODUCT_WITH_GROUP_XID = change_in_every_activity_in_batch(
            TEST_PRODUCT, "access_group_xid", TEST_KITCHEN_BY_ADMIN.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None

        # TODO: Probably it would be better to add all required ecotransit responses as mocked jsons
        # to prevent this error:
        data_errors = response.json()["batch"][0]["data_errors"]
        assert data_errors[0]["message"] == (
            "Cannot connect to EcoTransit!"  # the other errors come from Meteo-data not being mocked for all countries
        )

        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        batch = response.json().get("batch")
        resp_node = batch[0].get("final_root", {}).get("activity", {})
        assert resp_node.get("node_type") == FoodProcessingActivityNode.__name__
        assert resp_node.get("raw_input").get("name")[0].get("value") == TEST_PRODUCT[0].get("input_root", {}).get(
            "activity"
        ).get("name")[0].get("value")

        # testing deletion
        batch_to_delete = [
            {
                "nodes_dto": {
                    "existing_node": {
                        "xid": TEST_PRODUCT_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                }
            }
        ]

        response = await ac.post(
            "/v2/calculation/delete_graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": batch_to_delete},
        )
        check_response_codes_in_batch_calculation(response, 200)

        # testing mutation log response
        TEST_PRODUCT_WITH_GROUP_XID_RETRUN_LOG = change_in_every_item_in_batch(
            TEST_PRODUCT_WITH_GROUP_XID, "return_log", True
        )
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_XID_RETRUN_LOG},
        )
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        check_response_codes_in_batch_calculation(response, 200)
        check_mutation_log(response, TEST_PRODUCT_MUTATION_LOG)

        # add a regular user to this group and give them a 'read' permission
        members = [
            {
                "user_id": USER_BASIC_ID,
                "permissions": {
                    UserPermissionsEnum.read: True,
                },
            }
        ]

        response = await ac.post(
            "/v2/access-groups/upsert_members",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={
                "members": members,
                "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid")},
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == {
            "members_with_status": [{**member, "statuscode": status.HTTP_200_OK, "message": ""} for member in members],
            "access_group": {"xid": TEST_KITCHEN_BY_ADMIN.get("xid"), "uid": test_kitchen_by_admin_uid},
            "namespace_uid": None,
        }

        # set this group as their default one
        response = await ac.put(
            f"/v2/users/{USER_BASIC_ID}/",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={
                "user": USER_BASIC,
                "legacy_api_default_access_group_uid": test_kitchen_by_admin_uid,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json().get("legacy_default_access_group_id") == test_kitchen_by_admin_uid
        assert response.json().get("status") is True

        # and check if they can access this product

        # by specifying the root-flow
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_PRODUCT_ROOT_FLOW_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                },
                "requested_quantity_references": ["amount_for_root_node"],
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # and by specifying the root-activity
        batch_to_get = [
            {
                "input_root": {
                    "existing_root": {
                        "xid": TEST_PRODUCT_ID,
                        "access_group_xid": TEST_KITCHEN_BY_ADMIN.get("xid"),
                    }
                },
                "requested_quantity_references": ["amount_for_root_node"],
            }
        ]
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_basic_user_token}"},
            json={"batch": batch_to_get},
        )
        check_response_codes_in_batch_calculation(response, 200)
        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        assert response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid") is not None
        return response.json().get("batch")[0].get("final_root", {}).get("activity", {}).get("uid")


@pytest.mark.asyncio
async def test_create_delete_and_get_product_by_admin(create_delete_and_get_product_by_admin: str) -> None:
    _ = create_delete_and_get_product_by_admin


TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_PRODUCT_ID = "test_recipe_with_subrecipe_with_link_to_sub_product"
EMISSIONS_DUE_TO_GREENHOUSE = 0.07514157237
TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_PRODUCT = [
    {
        "input_root": {
            "flow": {
                "node_type": FoodProductFlowNode.__name__,
                "amount_in_original_source_unit": {"value": 100, "unit": "gram"},
            },
            "activity": {
                "xid": TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_PRODUCT_ID,
                "node_type": FoodProcessingActivityNode.__name__,
                "titles": [{"language": "de", "value": "Parent Recipe with child recipes"}],
                "date": "2013-10-14",
                "production_amount": {"value": 100, "unit": "gram"},
            },
            "sub_flows": [
                {
                    "node_type": FoodProductFlowNode.__name__,
                    "xid": "488533744",
                    "amount_in_original_source_unit": {"value": 100, "unit": "gram"},
                    "link_to_sub_node": {"xid": TEST_PRODUCT_ROOT_FLOW_ID},
                }
            ],
        },
        "requested_quantity_references": ["amount_for_root_node"],
    },
]


@pytest.mark.asyncio
async def test_create_delete_and_get_subrecipe_with_link_to_sub_product_by_admin(
    app: FastAPIReturn,
    create_and_get_and_delete_and_create_kitchen_by_admin: str,
    create_users: Tuple[bytes, bytes, bytes],
) -> None:
    postgres_db = service_locator.service_provider.postgres_db
    async with AsyncClient(
        app=app,
        base_url="http://localhost:8040",
    ) as ac:
        await postgres_db.connect(schema="test_pg")

        _, encoded_admin_user_token, encoded_basic_user_token = create_users
        _ = create_and_get_and_delete_and_create_kitchen_by_admin

        # step 1: post the original product
        TEST_PRODUCT_WITH_GROUP_XID = change_in_every_activity_in_batch(
            TEST_PRODUCT, "access_group_xid", TEST_KITCHEN_BY_ADMIN.get("xid")
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_PRODUCT_WITH_GROUP_XID},
        )

        check_response_codes_in_batch_calculation(response, 200)
        assert response.json() is not None

        resp_node = response.json()["batch"][0]["final_root"]["activity"]
        assert is_close(
            find_quantity_value(
                resp_node["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # step 2: post a recipe that has a subrecipe that links to the root-flow of the product with identical
        # configurations and check that we get the same result as for the product
        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_PRODUCT},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # step 3: post a recipe that has a subrecipe that directly links to the root-activity of the
        # product with identical configurations and check that we get the same result as for the product
        test_recipe_with_link_to_sub_product_via_activity = TEST_RECIPE_WITH_SUBRECIPE_WITH_LINK_TO_SUB_PRODUCT.copy()
        test_recipe_with_link_to_sub_product_via_activity[0]["input_root"]["sub_flows"][0]["link_to_sub_node"][
            "xid"
        ] = TEST_PRODUCT_ID
        test_recipe_with_link_to_sub_product_via_activity[0]["input_root"]["sub_flows"][0][
            "nutrient_values"
        ] = TEST_PRODUCT[0]["input_root"]["flow"]["nutrient_values"]
        test_recipe_with_link_to_sub_product_via_activity[0]["input_root"]["sub_flows"][0][
            "flow_location"
        ] = TEST_PRODUCT[0]["input_root"]["flow"]["flow_location"]
        test_recipe_with_link_to_sub_product_via_activity[0]["input_root"]["sub_flows"][0][
            "raw_production"
        ] = TEST_PRODUCT[0]["input_root"]["flow"]["raw_production"]

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": test_recipe_with_link_to_sub_product_via_activity},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,
        )

        # step 4: send a recipe where we change a field compared to the root-flow in the original root-flow
        # of the product (use twice the amount)
        test_recipe_with_link_to_sub_product_via_activity_and_modified_flow = (
            test_recipe_with_link_to_sub_product_via_activity.copy()
        )
        test_recipe_with_link_to_sub_product_via_activity_and_modified_flow[0]["input_root"]["sub_flows"][0][
            "amount_in_original_source_unit"
        ]["value"] = (
            2
            * test_recipe_with_link_to_sub_product_via_activity_and_modified_flow[0]["input_root"]["sub_flows"][0][
                "amount_in_original_source_unit"
            ]["value"]
        )

        response = await ac.post(
            "/v2/calculation/graphs",
            headers={"Authorization": f"Basic {encoded_admin_user_token}"},
            json={"batch": test_recipe_with_link_to_sub_product_via_activity_and_modified_flow},
        )

        check_response_codes_in_batch_calculation(response, 200)
        test_recipe_post_response = response.json()
        assert test_recipe_post_response is not None

        batch = response.json().get("batch")

        assert is_close(
            find_quantity_value(
                batch[0]["final_root"]["activity"]["impact_assessment"]["amount_for_root_node"],
                "IPCC 2013 climate change GWP 100a",
            ),
            2 * TEST_PRODUCT_EXPECTED_CO2_WITH_ROOT_FLOW,  # factor 2 because we doubled the amount
        )
