from typing import List, Optional

from fastapi import HTTPException, status
from structlog import get_logger

from api.dto.v2.customer_dto import CreateCustomerDto, CustomerCreateStatusDto, CustomerDto
from api.dto.v2.user_dto import UserCreateDto, UserCreateStatusDto, UserDto
from core.service.access_group_service import AccessGroupService
from core.service.namespace_service import NamespaceService
from core.service.user_service import UserService
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


class AdminController:
    def __init__(
        self,
        postgres_db: PostgresDb,
        access_gr_service: AccessGroupService,
        namespace_service: NamespaceService,
        user_service: UserService,
    ):
        self.postgres_db = postgres_db
        self.access_service = access_gr_service
        self.namespace_service = namespace_service
        self.user_service = user_service

    async def reset_db(self) -> None:
        await self.postgres_db.reset_db()

    async def create_namespace_by_xid(
        self, namespace_xid: str, customer_dto: CreateCustomerDto, auth_token: Optional[str] = None
    ) -> CustomerCreateStatusDto:
        namespace_object, token = await self.namespace_service.upsert_namespace(
            auth_token=auth_token,
            name=customer_dto.name,
            xid=namespace_xid,
            uid=customer_dto.uid,
        )

        return CustomerCreateStatusDto(auth_token=str(token), status=True, namespace=namespace_object)

    async def get_namespace_by_xid(self, namespace_xid: str) -> CustomerDto:
        namespace = await self.namespace_service.find_namespace_by_xid(namespace_xid)
        if not namespace:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Namespace {namespace_xid} does not exist.",
            )
        return CustomerDto(name=namespace.name, xid=namespace.xid, uid=namespace.uid)

    async def get_user(self, user_id: str) -> UserDto:
        user = await self.user_service.find_user_by_id(user_id)

        if not user:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"User {user_id} does not exist.",
            )

        return UserDto(
            user_id=user.user_id,
            email=user.email,
            is_superuser=user.is_superuser,
            is_service_account=user.is_service_account,
            legacy_default_access_group_id=user.legacy_api_default_access_group_uid,
        )

    async def get_users_from_namespace(self, namespace_xid: str) -> List[UserDto]:
        namespace = await self.namespace_service.find_namespace_by_xid(namespace_xid)
        namespace_uid = namespace.uid
        users = await self.user_service.list_users_from_namespace(namespace_uid)

        if not users:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"No users for customer {namespace_uid} found.",
            )

        return [
            UserDto(
                user_id=user.user_id,
                email=user.email,
                is_superuser=user.is_superuser,
                is_service_account=user.is_service_account,
                legacy_default_access_group_id=user.legacy_api_default_access_group_uid,
            )
            for user in users
        ]

    async def put_user(
        self, is_superuser: bool, legacy_api_default_access_group_uid: str, user_id: str, user_dto: UserCreateDto
    ) -> UserCreateStatusDto:
        # only superusers can set a default access group as any access group
        if legacy_api_default_access_group_uid and not is_superuser:
            is_member_of_target_group = await self.access_service.verify_access_group_membership_by_user_and_group_uid(
                legacy_api_default_access_group_uid, user_id
            )

            if not is_member_of_target_group:
                raise HTTPException(
                    status_code=status.HTTP_406_NOT_ACCEPTABLE,
                    detail="This user is not a member of target default access group.",
                )

        token = await self.user_service.create_user(
            user_id=user_id,
            is_superuser=user_dto.is_superuser,
            email=user_dto.email,
            password=user_dto.password,
            legacy_api_default_access_group_uid=legacy_api_default_access_group_uid,
        )

        return UserCreateStatusDto(
            auth_token=str(token),
            legacy_default_access_group_id=legacy_api_default_access_group_uid,
            status=True,
        )
