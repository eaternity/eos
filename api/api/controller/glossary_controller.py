import uuid
from typing import Optional
from uuid import UUID

from structlog import get_logger

from api.dto.v2.glossary_dto import GetAllTermsDto, TermDto, TermDtoWithoutUid
from core.domain.term import Term
from core.service.glossary_service import GlossaryService

logger = get_logger()


class GlossaryController:
    def __init__(self, glossary_service: GlossaryService):
        self.glossary_service = glossary_service

    async def find_all_glossary(self) -> GetAllTermsDto:
        cached_terms: dict[UUID, Term] = self.glossary_service.terms_by_uid
        terms = [
            TermDto(
                uid=str(term.uid),
                xid=term.xid,
                data=term.data,
                name=term.name,
                sub_class_of=str(term.sub_class_of) if term.sub_class_of else None,
                access_group_uid=str(term.access_group_uid) if term.access_group_uid else None,
            )
            for term in cached_terms.values()
        ]

        return GetAllTermsDto(terms=terms)

    def get_binary_glossary_cache(self) -> bytes:
        if self.glossary_service.terms_by_uid_binary is None:
            self.glossary_service._update_binary_cache()
        return self.glossary_service.terms_by_uid_binary

    async def get_terms_by_xid(self, term_xid: str = None) -> Optional[GetAllTermsDto]:
        terms = await self.glossary_service.get_terms_by_xid(term_xid)

        if not terms:
            return None

        term_dtos = [
            TermDto(
                uid=str(term.uid),
                xid=term.xid,
                data=term.data,
                name=term.name,
                sub_class_of=str(term.sub_class_of) if term.sub_class_of else None,
                access_group_uid=str(term.access_group_uid) if term.access_group_uid else None,
            )
            for term in terms
        ]

        return GetAllTermsDto(terms=term_dtos)

    async def get_term_by_xid_and_access_group_uid(
        self,
        term_xid: str,
        access_group_uid: str = None,
    ) -> Optional[TermDto]:
        term: Term = await self.glossary_service.get_term_by_xid_and_access_group_uid(term_xid, access_group_uid)

        if not term:
            return None

        term_dto = TermDto(
            uid=str(term.uid),
            xid=term.xid,
            data=term.data,
            name=term.name,
            sub_class_of=str(term.sub_class_of) if term.sub_class_of else None,
            access_group_uid=str(term.access_group_uid) if term.access_group_uid else None,
        )

        return term_dto

    async def get_term_by_uid(self, term_uid: UUID) -> Optional[TermDto]:
        term: Term = self.glossary_service.get_term_by_id(term_uid)

        if not term:
            return None

        term_dto = TermDto(
            uid=str(term.uid),
            xid=term.xid,
            data=term.data,
            name=term.name,
            sub_class_of=str(term.sub_class_of) if term.sub_class_of else None,
            access_group_uid=str(term.access_group_uid) if term.access_group_uid else None,
        )

        return term_dto

    def get_sub_terms_by_uid(self, term_uid: UUID, depth: int = 10) -> GetAllTermsDto:
        terms: [Term] = self.glossary_service.get_sub_terms_by_uid(term_uid=term_uid, depth=depth)
        terms_dto = [
            TermDto(
                uid=str(term.uid),
                xid=term.xid,
                data=term.data,
                name=term.name,
                sub_class_of=str(term.sub_class_of) if term.sub_class_of else None,
                access_group_uid=str(term.access_group_uid) if term.access_group_uid else None,
            )
            for term in terms
        ]

        return GetAllTermsDto(terms=terms_dto)

    async def put_term_by_uid(
        self,
        term_dto: TermDtoWithoutUid,
        term_uid: UUID,
    ) -> TermDto:
        term: Term = self.glossary_service.get_term_by_id(term_uid)
        if term:
            if term_dto.xid is not None:
                term.xid = term_dto.xid
            if term_dto.data is not None:
                term.data = term_dto.data
            if term_dto.access_group_uid is not None:
                term.access_group_uid = UUID(term_dto.access_group_uid)
            term.name = term_dto.name
            term.sub_class_of = UUID(term_dto.sub_class_of)
        else:
            term: Term = Term(
                uid=term_uid,
                xid=term_dto.xid,
                data=term_dto.data,
                name=term_dto.name,
                sub_class_of=UUID(term_dto.sub_class_of),
                access_group_uid=UUID(term_dto.access_group_uid),
            )

        term: Term = await self.glossary_service.put_term_by_uid(term)

        return TermDto(
            uid=str(term.uid),
            xid=term.xid,
            data=term.data,
            name=term.name,
            sub_class_of=str(term.sub_class_of),
            access_group_uid=str(term.access_group_uid),
        )

    async def put_term_by_xid(
        self,
        term_dto: TermDtoWithoutUid,
        term_xid: Optional[str] = None,
    ) -> TermDto:
        term: Term = await self.glossary_service.get_term_by_xid_and_access_group_uid(
            term_xid, term_dto.access_group_uid
        )

        if term:
            # use xid from json if both are provided
            if term_dto.xid is not None:
                term.xid = term_dto.xid
            if term_dto.data is not None:
                term.data = term_dto.data
            if term_dto.access_group_uid is not None:
                term.access_group_uid = UUID(term_dto.access_group_uid)
            term.name = term_dto.name
            term.sub_class_of = UUID(term_dto.sub_class_of)
        else:
            # create new one:
            term: Term = Term(
                uid=uuid.uuid4(),
                xid=term_dto.xid if term_dto.xid is not None else term_xid,
                data=term_dto.data,
                name=term_dto.name,
                sub_class_of=UUID(term_dto.sub_class_of),
                access_group_uid=UUID(term_dto.access_group_uid),
            )

        term: Term = await self.glossary_service.put_term_by_uid(term)

        return TermDto(
            uid=str(term.uid),
            xid=term.xid,
            data=term.data,
            name=term.name,
            sub_class_of=str(term.sub_class_of),
            access_group_uid=str(term.access_group_uid),
        )

    async def create_term(self, term_dto: TermDtoWithoutUid) -> TermDto:
        term: Term = Term(
            uid=uuid.uuid4(),
            xid=term_dto.xid,
            data=term_dto.data,
            name=term_dto.name,
            sub_class_of=UUID(term_dto.sub_class_of),
            access_group_uid=UUID(term_dto.access_group_uid),
        )

        term: Term = await self.glossary_service.put_term_by_uid(term)

        return TermDto(
            uid=str(term.uid),
            xid=term.xid,
            data=term.data,
            name=term.name,
            sub_class_of=str(term.sub_class_of),
            access_group_uid=str(term.access_group_uid),
        )
