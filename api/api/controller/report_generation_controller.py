import asyncio
import math
from datetime import date, datetime, timedelta
from enum import Enum
from typing import List, NamedTuple, Optional, Tuple, Union
from uuid import UUID, uuid4

from structlog import get_logger

from api.controller.node_controller import NodeController
from api.dto.v2.report_dto import (
    BestMenuItem,
    CategoryAnalysis,
    KitchenComparisonDataDto,
    MatchingWithReductionValueAndExamplesDto,
    MustacheJsonDto,
    ReportOutputDto,
    SnippetBestMenus,
    SnippetBestWorstIngredients,
    SnippetCategoryComparison,
    SnippetClimateFriendlyMenuComparisonDto,
    SnippetKitchenTotalCO2ValueComparisonDto,
    SnippetMenuCertificateDto,
    SnippetTopFlopMenuDto,
    TopOrFlopMenuIngredientDto,
)
from core.domain.calculation import Calculation, LinkedSubNodeInfo
from core.domain.deep_mapping_view import DeepListView
from core.domain.final_graph_criterion import FinalGraphTableCriterion, KeepCustomerSendData
from core.domain.nodes.activity.food_processing_activity_node import FoodProcessingActivityNode
from core.domain.nodes.activity.supply_sheet_activity import SupplySheetActivityNode
from core.domain.nodes.flow.food_product_flow_node import FoodProductFlowNode
from core.domain.nodes.root_with_subflows_dto import ExistingRootDto, RootWithSubFlowsDto
from core.domain.post_calc_cache import NodeCacheInfo
from core.domain.props.glossary_term_prop import GlossaryTermProp
from core.domain.props.impact_assessment_prop import AVERAGE_KG_CO2_PER_FOOD_UNIT, ImpactAssessmentProp
from core.domain.props.names_prop import NamesProp, RawName
from core.domain.props.quantity_prop import QuantityProp, RawQuantity
from core.service.glossary_service import GlossaryService
from core.service.messaging_service import Priority
from core.service.report_generation_service import ReportGenerationService

logger = get_logger()

CLIMATE_FRIENDLY_KG_CO2_PER_FOOD_UNIT = AVERAGE_KG_CO2_PER_FOOD_UNIT * 0.8

DISPLAY_FOOD_UNIT = 1.6  # Conversion factor from food unit (1/5 of DFU) to display food unit (1/3.125 DFU)

TREE_COMPENSATING_RATIO_KG = 8.83

# In Javaland the following the three following constants are computed dynamically, but
# in the daily, it was decided with Manuel that we will fix these to reasonable values.
# The median value of Javaland's database from the values obtained by the following query
# is taken (There are 757 rows satisfying where conditions):
# SELECT
#   gkd1.climateFriendlyRecipePercentage,
#   gkd2.climateFriendlyRecipePercentage,
#   gkd1.numberOfKitchens
# FROM eaternitydb.GeneralKitchenDatas gkd1
# JOIN eaternitydb.GeneralKitchenDatas gkd2
#   ON gkd1.namespace = gkd2.namespace
#       AND gkd1.`month` = gkd2.`month`
#       AND gkd1.`year` = gkd2.`year`
# WHERE gkd1.kitchenId = 'average-kitchen'
#   AND gkd2.kitchenId = 'climate-friendly-kitchen'
#   AND gkd1.numberOfKitchens > 100
#   AND gkd1.`year` > 2021
# ORDER BY gkd2.climateFriendlyRecipePercentage
# LIMIT 1 OFFSET 378;
CLIMATE_FRIENDLY_KITCHEN_CLIMATE_FRIENDLY_RECIPE_PERCENTAGE = 46.93
AVERAGE_KITCHEN_CLIMATE_FRIENDLY_RECIPE_PERCENTAGE = 34.65
NUMBER_OF_KITCHEN = 205


class SupplyFromRecipeConfigEnum(str, Enum):
    use_recipe_portions = "use_recipe_portions"  # generate supplies from recipes scaled by recipe-portions.
    use_recipe_production_portions = (
        "use_recipe_production_portions"  # generate supplies from recipes scaled by production-portions.
    )
    use_recipe_sold_potions = "use_recipe_sold_portions"  # generate supplies from recipes scaled by sold-portions.


class ReportGenerationController:
    def __init__(self, report_generation_service: ReportGenerationService):
        self.report_generation_service = report_generation_service

    @staticmethod
    def get_default_final_graph_table_criterion_for_reports() -> FinalGraphTableCriterion:
        return FinalGraphTableCriterion(keep_customer_send_data=KeepCustomerSendData(depth=2))

    @staticmethod
    def generate_calculation_object_for_caching_calculation(
        input_root: Union[ExistingRootDto, RootWithSubFlowsDto],
    ) -> Calculation:
        caching_calculation = Calculation(
            input_root=input_root,
            cache_final_root=True,
            return_data_as_table=True,
            final_graph_table_criterion=ReportGenerationController.get_default_final_graph_table_criterion_for_reports(),
        )
        return caching_calculation

    @staticmethod
    def use_post_calc_cache(
        node_info: NodeCacheInfo,
        max_cache_age_in_days: Optional[int] = None,
        final_graph_table_criterion: Optional[FinalGraphTableCriterion] = None,
    ) -> bool:
        return (
            node_info.cache_present
            and (
                max_cache_age_in_days is None
                or node_info.post_calc_data_meta_information.updated_at
                > (datetime.now().date() - timedelta(days=max_cache_age_in_days))
            )
            and (
                final_graph_table_criterion is None
                or (
                    node_info.final_graph_table is not None
                    and len(node_info.final_graph_table) > 0
                    and node_info.post_calc_data_meta_information.final_graph_table_criterion
                    == final_graph_table_criterion
                )
            )
        )

    async def get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
        self,
        access_group_uids: List[str],
        start_date: date,
        end_date: date,
        node_type: Optional[str] = None,
        get_tabular_cache: bool = False,
    ) -> List[NodeCacheInfo]:
        node_cache_infos = (
            await self.report_generation_service.get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
                access_group_uids,
                start_date,
                end_date,
                node_type,
                get_tabular_cache=get_tabular_cache,
            )
        )
        return node_cache_infos

    async def get_all_recipe_infos(
        self,
        access_group_uids: List[str],
        start_date: date,
        end_date: date,
        max_cache_age_in_days: Optional[int] = None,
    ) -> Tuple[List[NodeCacheInfo], List[LinkedSubNodeInfo], List[NodeCacheInfo]]:
        all_recipe_cache_info = await self.get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
            access_group_uids=access_group_uids,
            start_date=start_date,
            end_date=end_date,
            node_type=FoodProcessingActivityNode.__name__,
        )
        all_recipe_infos = [
            LinkedSubNodeInfo(
                uid=recipe_info.root_flow_uid,
                flow_amount=RawQuantity.unvalidated_construct(value=1.0, unit="production amount"),
            )
            for recipe_info in all_recipe_cache_info
        ]

        recipes_missing_cache = [
            recipe_info
            for recipe_info in all_recipe_cache_info
            if not ReportGenerationController.use_post_calc_cache(recipe_info, max_cache_age_in_days)
        ]
        logger.info(f"Found {len(all_recipe_infos)} recipe nodes")
        return all_recipe_cache_info, all_recipe_infos, recipes_missing_cache

    async def get_all_supply_infos(
        self,
        access_group_uids: List[str],
        start_date: date,
        end_date: date,
        supply_from_recipe_config: SupplyFromRecipeConfigEnum,
        all_recipe_cache_info: List[NodeCacheInfo],
        max_cache_age_in_days: Optional[int] = None,
    ) -> Tuple[List[NodeCacheInfo], List[LinkedSubNodeInfo], List[NodeCacheInfo], List[str]]:
        logger.info(f"{supply_from_recipe_config} for generating supplies.")
        recipes_missing_portion_numbers: List[str] = []
        match supply_from_recipe_config:
            case SupplyFromRecipeConfigEnum.use_recipe_portions.value:
                for recipe_info in all_recipe_cache_info:
                    if recipe_info.recipe_portions is None:
                        recipe_portions = 1
                    else:
                        recipe_portions = recipe_info.recipe_portions
                all_supply_infos = [
                    LinkedSubNodeInfo(
                        uid=recipe_info.root_flow_uid,
                        flow_amount=RawQuantity.unvalidated_construct(value=recipe_portions, unit="production amount"),
                    )
                    for recipe_info in all_recipe_cache_info
                ]
                supplies_missing_cache = []
            case SupplyFromRecipeConfigEnum.use_recipe_production_portions.value:
                for recipe_info in all_recipe_cache_info:
                    if recipe_info.production_portions is None:
                        logger.warning(f"{recipe_info.root_flow_uid} is missing production_portions")
                        recipes_missing_portion_numbers.append(str(recipe_info.root_flow_uid))
                all_supply_infos = [
                    LinkedSubNodeInfo(
                        uid=recipe_info.root_flow_uid,
                        flow_amount=RawQuantity.unvalidated_construct(
                            value=recipe_info.production_portions, unit="production amount"
                        ),
                    )
                    for recipe_info in all_recipe_cache_info
                ]
                supplies_missing_cache = []
            case SupplyFromRecipeConfigEnum.use_recipe_sold_potions.value:
                for recipe_info in all_recipe_cache_info:
                    if recipe_info.sold_portions is None:
                        logger.warning(f"{recipe_info.root_flow_uid} is missing sold_portions")
                        recipes_missing_portion_numbers.append(str(recipe_info.root_flow_uid))
                all_supply_infos = [
                    LinkedSubNodeInfo(
                        uid=recipe_info.root_flow_uid,
                        flow_amount=RawQuantity.unvalidated_construct(
                            value=recipe_info.sold_portions, unit="production amount"
                        ),
                    )
                    for recipe_info in all_recipe_cache_info
                ]
                supplies_missing_cache = []
            case _:
                all_supply_cache_info = await self.get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
                    access_group_uids=access_group_uids,
                    start_date=start_date,
                    end_date=end_date,
                    node_type=SupplySheetActivityNode.__name__,
                )
                all_supply_infos = [
                    LinkedSubNodeInfo(
                        uid=supply_info.root_flow_uid,
                        flow_amount=RawQuantity.unvalidated_construct(value=1.0, unit="production amount"),
                    )
                    for supply_info in all_supply_cache_info
                ]
                supplies_missing_cache = [
                    supply_info
                    for supply_info in all_supply_cache_info
                    if not ReportGenerationController.use_post_calc_cache(supply_info, max_cache_age_in_days)
                ]
                logger.info(f"Found {len(all_supply_infos)} supply nodes")
        return all_supply_infos, supplies_missing_cache, recipes_missing_portion_numbers

    def get_snippet_kitchen_total_co2_value_comparison(
        self, report_output: ReportOutputDto
    ) -> Optional[SnippetKitchenTotalCO2ValueComparisonDto]:
        supply_results = report_output.supply
        kg_co2 = None
        food_unit = None
        if supply_results and supply_results.final_root:
            impact_assessment: Optional[ImpactAssessmentProp] = supply_results.final_root.flow.impact_assessment
            if impact_assessment:
                kg_co2 = self.impact_assessment_in_kilogram_co2eq(impact_assessment=impact_assessment)

            daily_food_unit: Optional[QuantityProp] = supply_results.final_root.flow.daily_food_unit
            if daily_food_unit:
                food_unit = daily_food_unit.amount_for_activity_production_amount().value * 5.0
        else:
            logger.warning(f"{report_output.supply} is either None or doesn't have final root.")
            return

        if (kg_co2 is not None) and (food_unit is not None):
            average_kg_co2 = AVERAGE_KG_CO2_PER_FOOD_UNIT * food_unit
            climate_friendly_kg_co2 = CLIMATE_FRIENDLY_KG_CO2_PER_FOOD_UNIT * food_unit
            snippet_kitchen_total_co2_value_comparison = SnippetKitchenTotalCO2ValueComparisonDto(
                this=KitchenComparisonDataDto(
                    value=kg_co2,
                    comparison_to_average_percentage=(kg_co2 - average_kg_co2) / average_kg_co2 * 100,
                    reduction_with_respect_to_average=(kg_co2 - average_kg_co2),
                    restaurant_comparison_trees=(
                        math.floor(abs(kg_co2 - average_kg_co2) / TREE_COMPENSATING_RATIO_KG)
                        if kg_co2 < average_kg_co2
                        else None
                    ),
                ),
                average=KitchenComparisonDataDto(value=average_kg_co2),
                climate_friendly=KitchenComparisonDataDto(
                    value=climate_friendly_kg_co2,
                    comparison_to_average_percentage=(climate_friendly_kg_co2 - average_kg_co2) / average_kg_co2 * 100,
                    reduction_with_respect_to_average=(climate_friendly_kg_co2 - average_kg_co2),
                ),
            )
            return snippet_kitchen_total_co2_value_comparison
        else:
            logger.warning(f"{report_output.supply.final_root.flow} is either missing CO2 or DFU.")
        return None

    def get_snippet_climate_friendly_menu_comparison(
        self, report_output: ReportOutputDto
    ) -> Optional[SnippetClimateFriendlyMenuComparisonDto]:
        recipe_results = report_output.recipe
        if recipe_results and recipe_results.final_root:
            recipes_with_dfu = [
                sub_flow
                for sub_flow in recipe_results.final_root.sub_flows
                if sub_flow.daily_food_unit and sub_flow.daily_food_unit.value > 0
            ]  # Filter out all recipes without daily food unit.

            number_of_recipes = len(recipes_with_dfu)
            if number_of_recipes > 0:
                serialized_impacts = [
                    sub_flow.impact_assessment.model_dump()
                    for sub_flow in recipes_with_dfu
                    if sub_flow.impact_assessment
                ]
                if len(serialized_impacts) == number_of_recipes:
                    number_of_climate_friendly_recipe = sum(
                        1 for serialized_impact in serialized_impacts if serialized_impact.get("co2_award", False)
                    )
                    climate_friendly_recipe_percentage = number_of_climate_friendly_recipe / number_of_recipes * 100
                    return SnippetClimateFriendlyMenuComparisonDto(
                        number_of_kitchens=int(NUMBER_OF_KITCHEN),
                        climate_friendly=CLIMATE_FRIENDLY_KITCHEN_CLIMATE_FRIENDLY_RECIPE_PERCENTAGE,
                        average=AVERAGE_KITCHEN_CLIMATE_FRIENDLY_RECIPE_PERCENTAGE,
                        this=climate_friendly_recipe_percentage,
                    )
                else:
                    logger.warning(f"Some of {recipes_with_dfu} are missing impact assessment.")
        else:
            logger.warning(f"{report_output.recipe} is either None or doesn't have final root.")

        return None

    def get_snippet_best_worst_ingredients(
        self, report_output: ReportOutputDto
    ) -> Optional[SnippetBestWorstIngredients]:
        supply_results = report_output.supply
        matchings = {}
        examples = {}
        weights = {}
        if supply_results and supply_results.final_root:
            matching_amount_prop = supply_results.final_root.flow.amount_per_matching_required_for_flow
            impact_assessment_per_matching = supply_results.final_root.flow.impact_assessment_per_matching
            if matching_amount_prop and impact_assessment_per_matching:
                if not set(matching_amount_prop.quantities.keys()) == set(
                    impact_assessment_per_matching.quantities.keys()
                ):
                    logger.warning(
                        "Matching amount and impact assessment quantities are not matching for "
                        f"{report_output.supply.final_root.flow}."
                    )
                for uid in matching_amount_prop.quantities.keys():
                    if uid in impact_assessment_per_matching.quantities:
                        assert matching_amount_prop.quantities[uid].get_unit_term().xid == "EOS_gram"
                        matching_amount_in_g = matching_amount_prop.quantities[uid].value
                        matching_examples = matching_amount_prop.quantities[uid].examples
                        dfu_per_100g = matching_amount_prop.quantities[uid].dfu_per_100g
                        if dfu_per_100g:
                            fu_value_per_kg = (
                                dfu_per_100g * 5 * 10
                            )  # times 5 to convert to food-unit, times 10 to convert to 1kg
                            assert impact_assessment_per_matching.quantities[uid].get_unit_term().xid == "EOS_kg-co2-eq"
                            co2_value_in_kg = impact_assessment_per_matching.quantities[uid].value
                            co2_reduction_value = ImpactAssessmentProp.get_co2_value_reduction_value(
                                co2_value_in_kg, fu_value_per_kg * matching_amount_in_g / 1000.0
                            )
                            matchings[uid] = co2_reduction_value
                            examples[uid] = matching_examples
                            weights[uid] = matching_amount_in_g
                        else:
                            logger.warning(
                                "DFU per 100g is missing for matching", matching_uid=uid, examples=matching_examples
                            )
            else:
                logger.warning(
                    f"Matching amount or impact assessment is missing for {report_output.supply.final_root.flow}."
                )
        else:
            logger.warning(f"Supply result {supply_results} is either None or doesn't have final root.")

        # get the 10 best and the 10 worst ingredients:
        # Sort items by value
        sorted_items = sorted(matchings.items(), key=lambda item: item[1])

        # Get the 10 smallest negative values
        smallest_negatives = {k: v for k, v in sorted_items if v < 0}
        smallest_negatives = dict(list(smallest_negatives.items())[:10])

        worst_ingredients = []
        for uid, co2_reduction_value in smallest_negatives.items():
            examples_for_uid = examples[uid]
            worst_ingredients.append(
                MatchingWithReductionValueAndExamplesDto(
                    co2_reduction_kilogram=co2_reduction_value,
                    examples=examples_for_uid,
                    ingredient_weight_gram=weights[uid],
                )
            )

        # Get the 10 largest positive values
        largest_positives = {k: v for k, v in reversed(sorted_items) if v > 0}
        largest_positives = dict(list(largest_positives.items())[:10])
        best_ingredients = []
        for uid, co2_reduction_value in largest_positives.items():
            examples_for_uid = examples[uid]
            best_ingredients.append(
                MatchingWithReductionValueAndExamplesDto(
                    co2_reduction_kilogram=co2_reduction_value,
                    examples=examples_for_uid,
                    ingredient_weight_gram=weights[uid],
                )
            )

        return SnippetBestWorstIngredients(
            worst_ingredients=worst_ingredients,
            best_ingredients=best_ingredients,
        )

    def get_snippet_category_comparison(self, report_output: ReportOutputDto) -> Optional[SnippetCategoryComparison]:
        supply_results = report_output.supply
        categories: List[CategoryAnalysis] = []
        if supply_results and supply_results.final_root:
            assert isinstance(supply_results.final_root.flow, FoodProductFlowNode)
            dfu_per_category = supply_results.final_root.flow.dfu_per_category_required_for_flow
            impact_assessment_per_category = supply_results.final_root.flow.impact_assessment_per_category
            amount_per_category = supply_results.final_root.flow.amount_per_category_required_for_flow
            if dfu_per_category and impact_assessment_per_category and amount_per_category:
                if not set(dfu_per_category.quantities.keys()) == set(impact_assessment_per_category.quantities.keys()):
                    logger.warning(
                        f"DFU and impact assessment quantities are not matching for "
                        f"{report_output.supply.final_root.flow}."
                    )
                if not set(dfu_per_category.quantities.keys()) == set(amount_per_category.quantities.keys()):
                    logger.warning(
                        f"DFU and amount quantities are not matching for {report_output.supply.final_root.flow}."
                    )
                for uid in dfu_per_category.quantities.keys():
                    if uid in impact_assessment_per_category.quantities and uid in amount_per_category.quantities:
                        assert dfu_per_category.quantities[uid].get_unit_term().xid == "EOS_daily_food_unit"
                        fu_value = dfu_per_category.quantities[uid].value * 5  # times 5 to convert to food-unit
                        assert impact_assessment_per_category.quantities[uid].get_unit_term().xid == "EOS_kg-co2-eq"
                        co2_value_in_kg = impact_assessment_per_category.quantities[uid].value
                        assert amount_per_category.quantities[uid].get_unit_term().xid == "EOS_gram"
                        amount_in_gram = amount_per_category.quantities[uid].value
                        if fu_value and co2_value_in_kg and amount_in_gram:
                            co2_improvement_percentage = ImpactAssessmentProp.get_co2_value_improvement_percentage(
                                co2_value_in_kg, fu_value
                            )
                            category_term = GlossaryTermProp.get_term_from_uid(uid)
                            category_name = [RawName(value=category_term.name, language="en")]
                            if category_term.data.get("alternative_names"):
                                category_name.append(
                                    RawName(
                                        value=category_term.data["alternative_names"]["name"],
                                        language=category_term.data["alternative_names"]["lang"],
                                    )
                                )
                            parent_category = GlossaryTermProp.get_term_from_uid(category_term.sub_class_of)
                            parent_category_name = [RawName(value=parent_category.name, language="en")]
                            categories.append(
                                CategoryAnalysis(
                                    category_name=category_name,
                                    co2_value_gram=co2_value_in_kg * 1000,
                                    co2_improvement_percentage=co2_improvement_percentage,
                                    category_weight_gram=amount_in_gram,
                                    parent_category_name=parent_category_name,
                                )
                            )
            else:
                logger.warning(
                    f"DFU, impact assessment or amount is missing for {report_output.supply.final_root.flow}."
                )
        else:
            logger.warning(f"{supply_results} is either None or doesn't have final root.")

        categories.sort(key=lambda c: c.category_name[0]["value"])
        return SnippetCategoryComparison(
            category_analysis=categories,
        )

    def get_snippet_menu_certificate(self, report_output: ReportOutputDto) -> Optional[SnippetMenuCertificateDto]:
        recipe_results = report_output.recipe
        if recipe_results and recipe_results.final_root:
            recipes_with_dfu = [
                sub_flow
                for sub_flow in recipe_results.final_root.sub_flows
                if sub_flow.daily_food_unit and sub_flow.daily_food_unit.value > 0
            ]  # Filter out all recipes without daily food unit.

            number_of_recipes = len(recipes_with_dfu)
            if number_of_recipes > 0:

                class ImpactDfu(NamedTuple):
                    impact: ImpactAssessmentProp
                    dfu: QuantityProp

                all_impacts_dfus = [
                    ImpactDfu(impact=sub_flow.impact_assessment, dfu=sub_flow.daily_food_unit)
                    for sub_flow in recipes_with_dfu
                    if sub_flow.impact_assessment
                ]
                if len(all_impacts_dfus) == number_of_recipes:
                    climate_friendly_impact_dfus = [
                        impact_dfu
                        for impact_dfu in all_impacts_dfus
                        if impact_dfu.impact.model_dump().get("co2_award", False)
                    ]

                    def average_impact_per_fu(impact_dfus: list[ImpactDfu]) -> float:
                        sum_impact_per_fu = sum(
                            self.impact_assessment_in_gram_co2eq(impact_assessment=impact_dfu.impact)
                            / (impact_dfu.dfu.amount_for_activity_production_amount().value * 5)
                            for impact_dfu in impact_dfus
                        )
                        return sum_impact_per_fu / len(impact_dfus)

                    all_recipes_average = AVERAGE_KG_CO2_PER_FOOD_UNIT * DISPLAY_FOOD_UNIT * 1000
                    # kg-co2eq to g-co2eq
                    if len(climate_friendly_impact_dfus) == 0:
                        climate_friendly_recipes_average = None
                        certificate_percentage = None
                    else:
                        climate_friendly_recipes_average = (
                            average_impact_per_fu(climate_friendly_impact_dfus) * DISPLAY_FOOD_UNIT
                        )
                        certificate_percentage = (
                            (climate_friendly_recipes_average - all_recipes_average) * 100 / all_recipes_average
                        )

                    return SnippetMenuCertificateDto(
                        all_recipes_average=all_recipes_average,
                        this_recipes_average=average_impact_per_fu(all_impacts_dfus) * DISPLAY_FOOD_UNIT,
                        climate_friendly_recipes_average=climate_friendly_recipes_average,
                        certificate_percentage=certificate_percentage,
                    )
                else:
                    logger.warning(f"Some of {recipes_with_dfu} are missing impact assessment.")
        else:
            logger.warning(f"{report_output.recipe} is either None or doesn't have final root.")

        return None

    def get_menu_title(self, root_flow: FoodProductFlowNode) -> list[RawName]:
        product_name = root_flow.product_name
        if isinstance(product_name, NamesProp) and product_name.source_data_raw:
            return list(product_name.source_data_raw)
        if isinstance(product_name, (list, DeepListView)) and len(product_name) > 0:
            return list(product_name)
        menu_line_name = getattr(root_flow.raw_input, "menu_line_name", None)
        if menu_line_name:  # In javaland we did not store the language of the menu line name
            return [RawName(value=menu_line_name, language="unspecified")]
        else:
            return [{"value": "", "language": ""}]

    async def get_snippet_top_flop_menu(
        self, report_output: ReportOutputDto, node_controller: NodeController
    ) -> Optional[SnippetTopFlopMenuDto]:
        recipe_results = report_output.recipe
        if recipe_results and recipe_results.final_root:
            recipes_with_dfu = [
                sub_flow
                for sub_flow in recipe_results.final_root.sub_flows
                if sub_flow.daily_food_unit and sub_flow.daily_food_unit.value > 0
            ]  # Filter out all recipes without daily food unit.

            number_of_recipes = len(recipes_with_dfu)
            if number_of_recipes > 0:

                class UidImpactImpactPerFu(NamedTuple):
                    uid: UUID
                    impact_per_display_food_unit: float
                    impact: ImpactAssessmentProp

                all_impacts = [
                    UidImpactImpactPerFu(
                        uid=sub_flow.original_node_uid.uid,
                        impact=sub_flow.impact_assessment,
                        impact_per_display_food_unit=self.impact_assessment_in_gram_co2eq_per_display_food_unit(
                            sub_flow.impact_assessment, sub_flow.daily_food_unit
                        ),
                    )
                    for sub_flow in recipes_with_dfu
                    if sub_flow.impact_assessment
                ]
                if len(all_impacts) == number_of_recipes:
                    climate_friendly_impacts = [
                        impact for impact in all_impacts if impact.impact.model_dump().get("co2_award", False)
                    ]
                    if len(climate_friendly_impacts) == 0:
                        logger.info(f"No recipe is climate friendly in {report_output}.")
                        best_menu = None
                    else:
                        best_menu = min(all_impacts, key=lambda x: x.impact_per_display_food_unit)

                    worst_menu = max(all_impacts, key=lambda x: x.impact_per_display_food_unit)

                    tasks = []
                    for menu in (best_menu, worst_menu):
                        if menu is not None:
                            calculation = Calculation(uid=uuid4(), root_node_uid=menu.uid)
                            tasks.append(
                                node_controller.perform_calculation(
                                    calculation=calculation, use_queue=False, priority=Priority.LOW
                                )
                            )

                    results = await asyncio.gather(*tasks)

                    async def get_menu_ingredients_from_calc_result(
                        result: Calculation,
                    ) -> List[TopOrFlopMenuIngredientDto]:
                        if result.final_root is None:
                            raise ValueError(f"Calculation failed for calculation: {calculation}.")

                        ingredients = []
                        for sub_flow in result.final_root.sub_flows:
                            if (
                                (sub_flow.impact_assessment is None)
                                or (sub_flow.amount is None)
                                or (result.final_root.flow.daily_food_unit is None)
                            ):
                                raise ValueError(f"CO2 or DailyFoodUnit missing in {sub_flow}")
                            if sub_flow.product_name is None:
                                title = [{"value": "", "language": ""}]
                            else:
                                title = (
                                    list(sub_flow.product_name.source_data_raw)
                                    if isinstance(sub_flow.product_name, NamesProp)
                                    else list(sub_flow.product_name)
                                )

                            # Note that the scaling is by recipe's daily_food_unit.
                            co2_gram = self.impact_assessment_for_root_in_gram_co2eq_per_display_food_unit(
                                impact_assessment=sub_flow.impact_assessment,
                                daily_food_unit=result.final_root.flow.daily_food_unit,
                            )
                            weight_gram = await self.amount_for_root_in_gram_per_display_food_unit(
                                amount=sub_flow.amount,
                                daily_food_unit=result.final_root.flow.daily_food_unit,
                                glossary_service=node_controller.glossary_service,
                            )
                            ingredients.append(
                                TopOrFlopMenuIngredientDto(
                                    title=title,
                                    co2_gram=co2_gram,
                                    weight_gram=weight_gram,
                                )
                            )
                        return ingredients

                    def get_menu_co2_value_gram(result: Calculation) -> int:
                        if (result.final_root.flow.impact_assessment is None) or (
                            result.final_root.flow.daily_food_unit is None
                        ):
                            raise ValueError(f"CO2 or DailyFoodUnit missing in {result.final_root.flow}")
                        return self.impact_assessment_for_root_in_gram_co2eq_per_display_food_unit(
                            result.final_root.flow.impact_assessment, result.final_root.flow.daily_food_unit
                        )

                    if len(results) < 2:
                        best_menu_ingredients = None
                        worst_menu_ingredients = await get_menu_ingredients_from_calc_result(results[0])
                        best_menu_title = None
                        worst_menu_title = self.get_menu_title(results[0].final_root.flow)
                        best_menu_co2_value_gram = None
                        worst_menu_co2_value_gram = get_menu_co2_value_gram(result=results[0])
                    else:
                        best_menu_ingredients = await get_menu_ingredients_from_calc_result(results[0])
                        worst_menu_ingredients = await get_menu_ingredients_from_calc_result(results[1])
                        best_menu_title = self.get_menu_title(results[0].final_root.flow)
                        worst_menu_title = self.get_menu_title(results[1].final_root.flow)
                        best_menu_co2_value_gram = get_menu_co2_value_gram(result=results[0])
                        worst_menu_co2_value_gram = get_menu_co2_value_gram(result=results[1])

                    return SnippetTopFlopMenuDto(
                        best_menu_title=best_menu_title,
                        best_menu_co2_value_gram=best_menu_co2_value_gram,
                        worst_menu_title=worst_menu_title,
                        worst_menu_co2_value_gram=worst_menu_co2_value_gram,
                        best_menu_table=best_menu_ingredients,
                        worst_menu_table=worst_menu_ingredients,
                    )
                else:
                    logger.warning(f"Some of {recipes_with_dfu} are missing impace assessment.")

        return

    async def get_snippet_best_menus(
        self, report_output: ReportOutputDto, node_controller: NodeController
    ) -> Optional[SnippetTopFlopMenuDto]:
        recipe_results = report_output.recipe
        if recipe_results and recipe_results.final_root:
            recipes_with_dfu = [
                sub_flow
                for sub_flow in recipe_results.final_root.sub_flows
                if sub_flow.daily_food_unit and sub_flow.daily_food_unit.value > 0
            ]  # Filter out all recipes without daily food unit.

            number_of_recipes = len(recipes_with_dfu)
            if number_of_recipes > 0:

                class UidImpactImpactPerFu(NamedTuple):
                    uid: UUID
                    impact_per_display_food_unit: float
                    impact: ImpactAssessmentProp
                    root_flow: FoodProductFlowNode

                all_impacts = [
                    UidImpactImpactPerFu(
                        uid=sub_flow.original_node_uid.uid,
                        impact=sub_flow.impact_assessment,
                        impact_per_display_food_unit=self.impact_assessment_in_gram_co2eq_per_display_food_unit(
                            sub_flow.impact_assessment, sub_flow.daily_food_unit
                        ),
                        root_flow=sub_flow,
                    )
                    for sub_flow in recipes_with_dfu
                    if sub_flow.impact_assessment
                ]
                if len(all_impacts) == number_of_recipes:
                    climate_friendly_impacts = [
                        impact for impact in all_impacts if impact.impact.model_dump().get("co2_award", False)
                    ]
                    if len(climate_friendly_impacts) == 0:
                        logger.info(f"No recipe is climate friendly in {report_output}.")
                        best_menus = []
                    else:
                        best_menus = sorted(climate_friendly_impacts, key=lambda x: x.impact_per_display_food_unit)[:10]

                    best_menus_calculated = []
                    for menu in best_menus:
                        title = []
                        # in the final calculation we add another flow-activity-pair between the child-of-root and the
                        # actual root-flow. This is why we need to get the title from original_root_flow.
                        if menu.root_flow.original_node_uid:
                            original_uid = menu.root_flow.original_node_uid.uid
                            original_root_flow = await node_controller.node_service.find_by_uid(original_uid)
                            if original_root_flow:
                                title = self.get_menu_title(original_root_flow)
                            else:
                                logger.warning(f"Original root flow {original_uid} not found in the database.")
                        else:
                            title = self.get_menu_title(menu.root_flow)

                        weight_gram = await self.amount_for_root_in_gram_per_display_food_unit(
                            amount=menu.root_flow.amount,
                            daily_food_unit=menu.root_flow.daily_food_unit,
                            glossary_service=node_controller.glossary_service,
                        )
                        weight_per_portion_gram = await self.amount_for_root_in_gram(
                            amount=menu.root_flow.amount,
                            glossary_service=node_controller.glossary_service,
                        )
                        co2_per_portion_gram = menu.impact_per_display_food_unit / weight_gram * weight_per_portion_gram
                        best_menus_calculated.append(
                            BestMenuItem(
                                title=title,
                                co2_gram=menu.impact_per_display_food_unit,
                                weight_gram=weight_gram,
                                co2_per_portion_gram=co2_per_portion_gram,
                                weight_per_portion_gram=weight_per_portion_gram,
                            )
                        )

                    return SnippetBestMenus(best_menus=best_menus_calculated)

                else:
                    logger.warning(f"Some of {recipes_with_dfu} are missing impact assessment.")

        return

    async def compile_mustache_json_for_report_generation(
        self, report_output: ReportOutputDto, node_controller: NodeController
    ) -> None:
        report_output.mustache_json = MustacheJsonDto(
            snippet_kitchen_total_co2_value_comparison=(
                self.get_snippet_kitchen_total_co2_value_comparison(report_output=report_output)
            ),
            snippet_climate_friendly_menu_comparison=self.get_snippet_climate_friendly_menu_comparison(
                report_output=report_output
            ),
            snippet_menu_certificate=self.get_snippet_menu_certificate(report_output=report_output),
            snippet_top_flop_menu=await self.get_snippet_top_flop_menu(
                report_output=report_output, node_controller=node_controller
            ),
            snippet_best_worst_ingredients=self.get_snippet_best_worst_ingredients(report_output=report_output),
            snippet_category_comparison=self.get_snippet_category_comparison(report_output=report_output),
            snippet_best_menus=await self.get_snippet_best_menus(
                report_output=report_output, node_controller=node_controller
            ),
        )

    def impact_assessment_in_kilogram_co2eq(self, impact_assessment: ImpactAssessmentProp) -> float:
        return list(impact_assessment.amount_for_activity_production_amount().quantities.values())[0].value

    def impact_assessment_in_gram_co2eq(self, impact_assessment: ImpactAssessmentProp) -> float:
        return self.impact_assessment_in_kilogram_co2eq(impact_assessment=impact_assessment) * 1000

    def impact_assessment_for_root_in_kilogram_co2eq(self, impact_assessment: ImpactAssessmentProp) -> float:
        return list(impact_assessment.amount_for_root_node().quantities.values())[0].value

    def impact_assessment_for_root_in_gram_co2eq(self, impact_assessment: ImpactAssessmentProp) -> float:
        return self.impact_assessment_for_root_in_kilogram_co2eq(impact_assessment=impact_assessment) * 1000

    def impact_assessment_in_gram_co2eq_per_display_food_unit(
        self, impact_assessment: ImpactAssessmentProp, daily_food_unit: QuantityProp
    ) -> float:
        impact_per_display_food_unit = self.impact_assessment_in_gram_co2eq(impact_assessment) / (
            daily_food_unit.amount_for_activity_production_amount().value * 5 / DISPLAY_FOOD_UNIT
        )
        return impact_per_display_food_unit

    def impact_assessment_for_root_in_gram_co2eq_per_display_food_unit(
        self, impact_assessment: ImpactAssessmentProp, daily_food_unit: QuantityProp
    ) -> float:
        impact_per_display_food_unit = self.impact_assessment_for_root_in_gram_co2eq(impact_assessment) / (
            daily_food_unit.amount_for_root_node().value * 5 / DISPLAY_FOOD_UNIT
        )
        return impact_per_display_food_unit

    async def amount_for_root_in_gram(self, amount: QuantityProp, glossary_service: GlossaryService) -> float:
        amount_for_root = amount.amount_for_root_node()
        original_unit_term = await glossary_service.get_term_by_uid(amount.unit_term_uid)
        gram_unit_term = glossary_service.terms_by_xid_ag_uid[
            ("EOS_gram", glossary_service.root_subterms["EOS_units"].access_group_uid)
        ]
        return QuantityProp.convert_between_same_unit_types(
            amount_for_root.value, original_unit_term, gram_unit_term, "mass-in-g"
        )

    async def amount_for_root_in_gram_per_display_food_unit(
        self, amount: QuantityProp, daily_food_unit: QuantityProp, glossary_service: GlossaryService
    ) -> float:
        amount_for_root_gram = await self.amount_for_root_in_gram(amount=amount, glossary_service=glossary_service)
        return amount_for_root_gram / (daily_food_unit.amount_for_root_node().value * 5 / DISPLAY_FOOD_UNIT)
