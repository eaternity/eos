from typing import List, Optional, Tuple

from fastapi import HTTPException, status
from starlette.datastructures import State
from structlog import get_logger

from api.dto.v2.access_group_dto import AccessGroupDto, AccessGroupId, RootAccessGroupInfo, WrappedAccessGroupIdDto
from api.dto.v2.user_dto import MemberDto
from core.domain.access_group_node import AccessGroup, NodeAccessGroupTypeEnum
from core.domain.user import UserGroupRoleEnum
from core.service.access_group_service import AccessGroupService
from core.service.permission_service import check_if_all_access_groups_can_read_node

logger = get_logger()


class AccessGroupController:
    def __init__(self, access_gr_service: AccessGroupService):
        self.access_service = access_gr_service

    # not used anywhere -- deprecate?
    async def create_access_group(
        self,
        access_group_dto: AccessGroupDto,
        namespace_uid: str,
        user_id: str,
    ) -> Tuple[AccessGroupDto, bool]:
        new_access_group, is_updated = await self.access_service.create_access_group(
            namespace_uid=namespace_uid,
            xid=access_group_dto.xid,
            type=access_group_dto.type,
            data=access_group_dto.get_model_meta_data(),
            creator=user_id,
        )

        return (
            AccessGroupDto(
                xid=new_access_group.xid,
                uid=new_access_group.uid,
                creator=user_id,
                type=access_group_dto.type,
                **new_access_group.data,
            ),
            is_updated,
        )

    async def get_all_root_access_group_info(self) -> List[RootAccessGroupInfo]:
        root_access_groups = await self.access_service.get_all_root_access_group_info()
        return root_access_groups

    async def get_all_access_group_ids_by_namespace(self, namespace_uid: str) -> List[AccessGroupId]:
        access_groups = await self.access_service.get_all_access_group_ids_by_namespace(namespace_uid)

        return access_groups

    async def get_all_sub_access_group_ids_by_uid(self, access_group_uid: str, depth: int) -> List[AccessGroupId]:
        access_groups = await self.access_service.get_all_sub_access_group_ids_by_uid(access_group_uid, depth)

        return access_groups

    async def get_all_group_memberships_by_user(self, user_id: str) -> List[AccessGroupId]:
        access_groups = await self.access_service.get_all_group_memberships_by_user(user_id)

        return access_groups

    async def get_all_sub_group_memberships_by_user_and_uid(
        self,
        access_group_uid: str,
        depth: int,
        user_id: str,
        access_group_type: Optional[NodeAccessGroupTypeEnum] = None,
    ) -> List[AccessGroupId]:
        access_groups = await self.access_service.get_all_sub_group_memberships_by_user_and_uid(
            access_group_uid,
            depth,
            user_id,
            access_group_type,
        )

        return access_groups

    # TODO: unused -- deprecate?
    async def get_all_created_access_groups_by_user(self, user_id: str) -> List[AccessGroupId]:
        access_groups = await self.access_service.get_all_created_access_groups_by_user(user_id)

        return access_groups

    async def get_access_group_by_xid(self, access_group_xid: str, namespace_uid: str) -> AccessGroup:
        access_group_node = await self.access_service.get_access_group_by_xid(access_group_xid, namespace_uid)

        if not access_group_node:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Group {access_group_xid} does not exist.",
            )

        return access_group_node

    async def get_access_group_by_uid(self, access_group_uid: str) -> AccessGroup:
        access_group_node = await self.access_service.get_access_group_by_uid(access_group_uid)

        if not access_group_node:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Group {access_group_uid} does not exist.",
            )

        return access_group_node

    async def get_access_group_dto_by_uid(self, access_group_uid: str) -> tuple[AccessGroupDto, str]:
        access_group_node = await self.get_access_group_by_uid(access_group_uid)

        return (
            AccessGroupDto(
                xid=access_group_node.xid,
                uid=access_group_node.uid,
                type=access_group_node.type,
                **access_group_node.data,
            ),
            access_group_node.namespace_uid,
        )

    async def check_if_user_is_group_member_by_group_uid(self, access_group_uid: str, user_id: str) -> bool:
        return await self.access_service.verify_access_group_membership_by_user_and_group_uid(access_group_uid, user_id)

    async def upsert_access_group(
        self, namespace_uid: str, creator_id: str, access_group_dto: AccessGroupDto
    ) -> tuple[AccessGroupDto, bool]:
        updated_access_group, is_updated = await self.access_service.upsert_access_group_by_xid_or_uid(
            namespace_uid=namespace_uid,
            uid=access_group_dto.uid,
            xid=access_group_dto.xid,
            creator=creator_id,
            type=access_group_dto.type,
            data=access_group_dto.get_model_meta_data(),
        )

        return (
            AccessGroupDto(
                xid=updated_access_group.xid,
                uid=updated_access_group.uid,
                type=access_group_dto.type,
                creator=creator_id,
                **updated_access_group.data,
            ),
            is_updated,
        )

    # not used anywhere -- deprecate?
    async def put_access_group_by_uid(
        self, namespace_uid: str, group_uid: str, creator_id: str, access_group_dto: AccessGroupDto
    ) -> (AccessGroupDto, bool):
        updated_access_group, is_updated = await self.access_service.upsert_access_group_by_xid_or_uid(
            namespace_uid=namespace_uid,
            uid=group_uid,
            creator=creator_id,
            type=access_group_dto.type,
            data=access_group_dto.get_model_meta_data(),
        )

        return (
            AccessGroupDto(
                xid=updated_access_group.xid,
                uid=updated_access_group.uid,
                type=access_group_dto.type,
                creator=creator_id,
                **updated_access_group.data,
            ),
            is_updated,
        )

    async def upsert_access_group_edge(self, parent_group_uid: str, child_group_uid: str) -> None:
        await self.access_service.upsert_access_group_edge(
            parent_access_group_uid=parent_group_uid,
            child_access_group_uid=child_group_uid,
        )

    async def does_access_group_edge_exist(
        self,
        child_access_group_uid: str,
        parent_access_group_uid: str,
    ) -> bool:
        edge = await self.access_service.get_access_group_edge(child_access_group_uid, parent_access_group_uid)
        return edge is not None

    async def delete_access_group_edge(
        self,
        child_access_group_uid: str,
        parent_access_group_uid: str,
    ) -> bool:
        return await self.access_service.delete_access_group_edge(child_access_group_uid, parent_access_group_uid)

    async def delete_access_group_by_uid(self, access_group_uid: str) -> None:
        return await self.access_service.delete_access_group_by_uid(access_group_uid)

    async def add_member_to_the_group_by_group_uid(self, access_group_uid: str, new_member: MemberDto) -> None:
        await self.access_service.access_mgr.upsert_member_into_group_by_group_uid(
            new_member.user_id, access_group_uid, new_member.permissions
        )

    async def delete_member_from_the_group_by_uid(self, access_group_uid: str, member_id: str) -> None:
        await self.access_service.access_mgr.delete_member_from_group_by_group_uid(member_id, access_group_uid)

    async def get_group_members_by_group_uid(self, access_group_uid: str) -> list[MemberDto]:
        return await self.access_service.access_mgr.get_all_members_by_group_uid(access_group_uid)

    async def get_user_permissions_by_uid(
        self,
        request_state: State,
        access_group_uid: str,
    ) -> dict:
        if request_state.is_superuser:
            return {permission: True for permission in UserGroupRoleEnum.SUPERUSER}

        user_id = request_state.user

        return await self.access_service.access_mgr.get_member_permissions(access_group_uid, user_id)

    async def get_user_permissions_by_multiple_uids(
        self,
        request_state: State,
        access_group_uids: List[str],
    ) -> dict:
        if request_state.is_superuser:
            return {
                access_group_uid: {permission: True for permission in UserGroupRoleEnum.SUPERUSER}
                for access_group_uid in access_group_uids
            }

        user_id = request_state.user

        return await self.access_service.access_mgr.get_member_permissions_multiple_access_groups(
            access_group_uids, user_id
        )

    async def fill_in_missing_info_in_wrapped_access_group_id(
        self,
        request_state: State,
        wrapped_access_group_id: Optional[WrappedAccessGroupIdDto] = None,
    ) -> WrappedAccessGroupIdDto:
        if not wrapped_access_group_id:
            wrapped_access_group_id = WrappedAccessGroupIdDto(access_group=AccessGroupId())

        # If neither access group XID nor UID is present, set the access group UID and namesapce according
        # to the legacy default.
        if not wrapped_access_group_id.access_group.xid and not wrapped_access_group_id.access_group.uid:
            wrapped_access_group_id.access_group.uid = request_state.user_legacy_default_access_group.uid
            wrapped_access_group_id.namespace_uid = request_state.user_legacy_default_access_group.namespace_uid

        # If access group UID is present, but not the namespace UID, find the access group from the database
        # to set the namespace UID.
        if wrapped_access_group_id.access_group.uid and not wrapped_access_group_id.namespace_uid:
            access_group = await self.get_access_group_by_uid(wrapped_access_group_id.access_group.uid)
            wrapped_access_group_id.namespace_uid = access_group.namespace_uid

        # If the namespace UID is still not available, set it to the legacy default.
        if not wrapped_access_group_id.namespace_uid:
            wrapped_access_group_id.namespace_uid = request_state.user_legacy_default_access_group.namespace_uid

        logger.info(f"called get_nodes with namespace: {wrapped_access_group_id.namespace_uid}")

        if wrapped_access_group_id.access_group.xid:
            # get this access group's UUID and verify that it exists
            wrapped_access_group_id.access_group.uid = (
                await self.get_access_group_by_xid(
                    wrapped_access_group_id.access_group.xid, wrapped_access_group_id.namespace_uid
                )
            ).uid
        else:
            # verify that this access group exists
            await self.get_access_group_by_uid(wrapped_access_group_id.access_group.uid)
        return wrapped_access_group_id

    async def multiple_access_group_control(
        self, wrapped_access_group_ids: Optional[List[WrappedAccessGroupIdDto]], request_state: State
    ) -> List[str]:
        if not wrapped_access_group_ids:
            wrapped_access_group_ids = [None]

        for wrapped_access_group_id in wrapped_access_group_ids:
            await self.fill_in_missing_info_in_wrapped_access_group_id(request_state, wrapped_access_group_id)

        access_group_uids = list(
            set(wrapped_access_group_id.access_group.uid for wrapped_access_group_id in wrapped_access_group_ids)
        )

        permissions_by_access_group_uid = await self.get_user_permissions_by_multiple_uids(
            request_state=request_state, access_group_uids=access_group_uids
        )
        if not check_if_all_access_groups_can_read_node(permissions_by_access_group_uid, access_group_uids):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Incorrect API Key.",
                headers={"WWW-Authenticate": "Basic"},
            )
        return access_group_uids
