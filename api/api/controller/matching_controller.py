from typing import Optional
from uuid import UUID

from gap_filling_modules.match_product_name_gfm import MATCH_PRODUCT_GFM_NAME
from structlog import get_logger

from api.dto.v2.glossary_dto import TermDto
from api.dto.v2.matching_dto import MatchingItemDto, MatchingResultDto
from core.domain.matching_item import MatchingItem
from core.service.glossary_service import GlossaryService
from core.service.matching_service import MatchingService

logger = get_logger()


class MatchingController:
    def __init__(self, matching_service: MatchingService, glossary_service: GlossaryService):
        self.matching_service = matching_service
        self.glossary_service = glossary_service

    def get_binary_matching_items_cache(self) -> bytes:
        if self.matching_service._terms_by_matching_str_binary is None:
            self.matching_service._update_binary_cache()
        return self.matching_service._terms_by_matching_str_binary

    async def get_matching_by_string(
        self,
        matching_string: str,
        gfm_name: str = MATCH_PRODUCT_GFM_NAME,
        filter_lang: Optional[str] = None,
        filter_context: Optional[dict] = None,
    ) -> MatchingResultDto:
        """Returns a list of Matching Items that match the given *lowercased* matching string."""
        matching_items: [MatchingItem] = self.matching_service.get_matching_by_string(
            matching_string, gfm_name, filter_lang=filter_lang, filter_context=filter_context
        )

        matching_items_dto: [MatchingItemDto] = []
        for item in matching_items:
            terms = [self.glossary_service.get_term_by_id(UUID(term_uid)) for term_uid in item.term_uids]
            matching_items_dto.append(
                MatchingItemDto(
                    uid=item.uid,
                    gap_filling_module=item.gap_filling_module,
                    lang=item.lang,
                    matched_node_uid=item.matched_node_uid,
                    data=item.data,
                    term_uids=item.term_uids,
                    terms=[
                        TermDto(
                            uid=str(term.uid),
                            xid=term.xid,
                            data=term.data,
                            name=term.name,
                            sub_class_of=str(term.sub_class_of) if term.sub_class_of else None,
                            access_group_uid=str(term.access_group_uid),
                        )
                        for term in terms
                    ],
                )
            )

        return MatchingResultDto(matching_items=matching_items_dto)

    async def put_matching_string(
        self,
        matching_item: MatchingItemDto,
    ) -> MatchingItemDto:
        """Creates a new matching item. Note that the matching string is lowercased."""
        # check for existence of terms
        for term_uid in matching_item.term_uids:
            try:
                self.glossary_service.get_term_by_id(UUID(term_uid))
            except KeyError as ke:
                raise KeyError(f"Term with uid {term_uid} not found") from ke

        matching_item = MatchingItem(
            uid=matching_item.uid,
            gap_filling_module=matching_item.gap_filling_module,
            matching_string=matching_item.matching_string,
            term_uids=matching_item.term_uids,
            lang=matching_item.lang,
            matched_node_uid=matching_item.matched_node_uid,
            data=matching_item.data,
        )
        matching_item = await self.matching_service.put_matching_item(matching_item)

        return MatchingItemDto(
            uid=matching_item.uid,
            gap_filling_module=matching_item.gap_filling_module,
            matching_string=matching_item.matching_string,
            term_uids=matching_item.term_uids,
            lang=matching_item.lang,
            matched_node_uid=matching_item.matched_node_uid,
            data=matching_item.data,
        )
