"Node controller."
import uuid
from datetime import date, datetime
from typing import List, Optional, Union

from fastapi import HTTPException, status
from structlog import get_logger

from core.domain.calculation import Calculation
from core.domain.edge import EdgeToUpsert, XidAccessGroupUid
from core.domain.nodes.node import Node
from core.domain.nodes.root_with_subflows_dto import ExistingRootDto, NodeIDDto
from core.domain.props.index_prop import IndexProp
from core.service.access_group_service import AccessGroupService
from core.service.calc_service import CalcService
from core.service.glossary_service import GlossaryService
from core.service.messaging_service import Priority
from core.service.node_service import NodeService
from database.postgres.pg_product_mgr import PgProductMgr

logger = get_logger()


class NodeController:
    """Controller for nodes."""

    def __init__(
        self,
        node_service: NodeService,
        calc_service: CalcService,
        access_service: AccessGroupService,
        glossary_service: GlossaryService,
        product_mgr: PgProductMgr,
    ):
        self.node_service = node_service
        self.calc_service = calc_service
        self.access_service = access_service
        self.glossary_service = glossary_service
        self.product_mgr = product_mgr

    def get_binary_node_cache(self) -> bytes:
        if self.node_service.cache._binary_cache is None:
            self.node_service.cache._update_binary_cache()
        return self.node_service.cache._binary_cache

    async def get_all_nodes_by_access_group_uid(
        self, access_group_uid: str, node_type: Optional[str] = None
    ) -> list[Node]:
        """Get all nodes in an access group and determine their xid. Filter by node_type if specified."""
        nodes = await self.node_service.graph_mgr.get_nodes_by_access_group_uid(access_group_uid, node_type=node_type)
        for node in nodes:
            node.xid = await self.product_mgr.get_xid_by_uid(str(node.uid))
        return nodes

    async def get_all_nodes_by_access_group_uids(
        self, access_group_uids: List[str], start_date: date, end_date: date, node_type: Optional[str] = None
    ) -> list[Node]:
        """Get all nodes in an access group and determine their xid. Filter by node_type if specified."""
        nodes = await self.node_service.graph_mgr.get_nodes_by_access_group_uids(
            access_group_uids, node_type=node_type, start_date=start_date, end_date=end_date
        )
        for node in nodes:
            node.xid = await self.product_mgr.get_xid_by_uid(str(node.uid))
        return nodes

    async def generate_or_get_uid(
        self, node: Node, namespace_uid: Optional[str] = None, assign_new_uid: Optional[bool] = False
    ) -> uuid.UUID:
        """Generate a new uid or get the existing one (either defined by the uid or by the xid field)."""
        if node.uid:
            new_object_id = node.uid
        elif node.xid:
            new_object_id = await self.product_mgr.find_or_create_uid_by_xid(xid=node.xid, namespace_uid=namespace_uid)
        else:
            if assign_new_uid:
                new_object_id = uuid.uuid4()
            else:
                new_object_id = None
        return new_object_id

    async def get_uid_by_xid(self, namespace_uid: str, node_xid: str) -> Optional[uuid.UUID]:
        """Get the uid of a node."""
        node_uid = await self.product_mgr.find_uid_by_xid(xid=node_xid, namespace_uid=namespace_uid)
        return node_uid

    async def get_uid(self, namespace_uid: str, node: Union[Node, NodeIDDto]) -> uuid.UUID:
        """Get the uid of a node."""
        if node.uid:
            new_object_id = node.uid
        elif node.xid:
            new_object_id = await self.product_mgr.find_uid_by_xid(xid=node.xid, namespace_uid=namespace_uid)
            if not new_object_id:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"Node with XID {node.xid} not found.",
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Node {node} has neither a xid nor an uid.",
            )
        return new_object_id

    async def get_root_flow_or_activity_node(
        self, namespace_uid: str, existing_root: ExistingRootDto, allow_deleted: bool = False
    ) -> Optional[Node]:
        """Get the root-flow or the root-activity defined by existing_root.

        This method also add parent and child nodes if corresponding edges exist in the database.
        """
        existing_root.existing_root.uid = await self.get_uid(namespace_uid, existing_root.existing_root)
        return await self.node_service.get_root_flow_or_activity(
            existing_root.existing_root.uid, allow_deleted=allow_deleted
        )

    async def upsert_recipe_node(
        self, item_in: Calculation
    ) -> tuple[list[EdgeToUpsert], list[EdgeToUpsert], str, Optional[str], bool, Optional[uuid.UUID]]:
        """Upsert a recipe with all its subflows."""
        nodes_dto = item_in.input_root
        if nodes_dto.sub_flows:
            for declaration_index, sub_flow in enumerate(nodes_dto.sub_flows):
                sub_flow.uid = await self.generate_or_get_uid(
                    sub_flow, namespace_uid=item_in.namespace_uid, assign_new_uid=False
                )

                # We need to make sure that the sub_flow is in the same access group as the root_activity because only
                # that permission was checked before:
                sub_flow.access_group_uid = nodes_dto.activity.access_group_uid
                sub_flow.access_group_xid = nodes_dto.activity.access_group_xid

                sub_flow.declaration_index = IndexProp(value=declaration_index)
                nodes_dto.activity.add_sub_node(sub_flow)
                sub_flow.add_parent_node(nodes_dto.activity)

        if nodes_dto.flow:
            nodes_dto.flow.uid = await self.generate_or_get_uid(
                nodes_dto.flow, namespace_uid=item_in.namespace_uid, assign_new_uid=False
            )

            # We need to make sure that the root_flow is in the same access group as the root_activity because only
            # that permission was checked before:
            nodes_dto.flow.access_group_uid = nodes_dto.activity.access_group_uid
            nodes_dto.flow.access_group_xid = nodes_dto.activity.access_group_xid

            nodes_dto.activity.add_parent_node(nodes_dto.flow)
            nodes_dto.flow.add_sub_node(nodes_dto.activity)

        return await self.node_service.upsert_by_recipe_id(recipe_node=nodes_dto.activity)

    async def load_old_calculation_result(self, calculation_uid: str) -> tuple[Calculation | None, uuid.UUID | None]:
        """Load the result of a calculation from the database and store all the requested results."""
        calculation = await self.calc_service.find_by_uid(calculation_uid)
        if calculation:
            access_group_node = await self.node_service.find_by_uid(calculation.root_node_uid)
            if not access_group_node:
                access_group_node = await self.node_service.find_by_uid(calculation.child_of_root_node_uid)

            if access_group_node:
                access_group_uid = access_group_node.access_group_uid
                return calculation, access_group_uid
            else:
                return None, None
        else:
            return None, None

    async def init_calculation(self, calculation: Calculation) -> Calculation:
        """Initialize calculation."""
        calculation.uid = uuid.uuid4()

        # persist this calculation to db (without mutations and results yet):
        await self.calc_service.service_provider.postgres_db.pg_calc_mgr.insert_calculation(calculation)

        return calculation

    async def perform_calculation(
        self, calculation: Calculation, use_queue: bool = True, priority: Priority = Priority.LOW
    ) -> Calculation:
        """Perform the calculation for a given output item and store all the requested results."""
        if use_queue:
            calculation = await self.calc_service.service_provider.messaging_service.enqueue_calculation(
                calculation, priority
            )
        else:
            calculation, result_root_node = await self.calc_service.calculate(calculation)

        if calculation.final_root:
            calculation.final_root.activity.set_calculation(calculation)
        return calculation

    async def delete_node_by_xid(self, node_xid: str, namespace_uid: str) -> bool:
        """Delete a node by its XID."""
        node_uid = await self.product_mgr.find_uid_by_xid(xid=node_xid, namespace_uid=namespace_uid)
        if node_uid is None:
            return False
        return await self.node_service.delete_node_and_edges(node_uid)

    async def delete_node_by_uid(self, node_uid: uuid.UUID) -> bool:
        """Delete a node by its UID."""
        return await self.node_service.delete_node_and_edges(node_uid)

    async def process_link_to_subnode_edges(
        self,
        batch_activity_uid: str,
        edges_to_insert: [EdgeToUpsert],
        edges_to_delete: [EdgeToUpsert],
        md5_hash: Optional[uuid.UUID],
        xid_access_group_uids_to_map_to_uid: list[XidAccessGroupUid],
    ) -> tuple[str, bool]:
        return await self.node_service.process_link_to_subnode_edges(
            batch_activity_uid, edges_to_insert, edges_to_delete, md5_hash, xid_access_group_uids_to_map_to_uid
        )

    @staticmethod
    def parse_date(date_str: Optional[str]) -> Optional[date]:
        if date_str is None:
            return date_str

        try:
            return datetime.strptime(date_str, "%Y-%m-%d").date()
        except ValueError as e:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Incorrect date for '{date_str}', should be YYYY-MM-DD and must be a valid date.",
            ) from e
