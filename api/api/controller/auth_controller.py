import uuid

from structlog import get_logger

from core.domain.token import TokenListingOutputDto, TokenOutputDto
from core.service.user_service import UserService

logger = get_logger()


class AuthController:
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    async def validate_token(self, auth_token: str) -> bool:
        return await self.user_service.validate_token(auth_token)

    async def upsert_token(self, user_id: str, name: str) -> TokenOutputDto:
        new_token = str(uuid.uuid4())

        await self.user_service.upsert_token(user_id=user_id, token=new_token, name=name)

        return TokenOutputDto(name=name, token=new_token)

    async def list_user_tokens(self, user_id: str) -> TokenListingOutputDto:
        tokens = await self.user_service.list_user_tokens(user_id)

        return TokenListingOutputDto(tokens=[TokenOutputDto(name=token.name, token=token.token) for token in tokens])
