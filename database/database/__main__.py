import argparse
import asyncio

from structlog import get_logger

from core.service.service_provider import ServiceProvider
from database.postgres.postgres_db import PostgresDb

logger = get_logger()


async def reinit_db() -> None:
    service_provider = ServiceProvider()
    postgres_db = PostgresDb(service_provider)
    await postgres_db.connect()
    logger.info("start reset_db...")
    await postgres_db.reset_db()
    logger.info("reset_db finished. now disconnect from postgres...")
    await postgres_db.disconnect()


async def cleanup_db() -> None:
    service_provider = ServiceProvider()
    postgres_db = PostgresDb(service_provider)
    await postgres_db.connect()
    logger.info("start cleanup_db...")
    await postgres_db.cleanup_db()
    logger.info("cleanup_db finished. now disconnect from postgres...")
    await postgres_db.disconnect()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Database.")
    parser.add_argument("--reinit", action="store_true")
    parser.add_argument("--cleanup", action="store_true")
    args = parser.parse_args()

    if args.reinit:
        logger.info("start reinit of db")
        asyncio.run(reinit_db())
        logger.info("done")
    elif args.cleanup:
        logger.info("start cleanup of db")
        asyncio.run(cleanup_db())
        logger.info("done")
