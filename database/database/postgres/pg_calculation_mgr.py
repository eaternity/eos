from typing import List, Optional

import asyncpg
from structlog import get_logger

from core.domain.calculation import Calculation
from core.service.glossary_service import GlossaryService

logger = get_logger()


class PostgresCalculationMgr:
    """Manager for calculations in the postgres database."""

    def __init__(self, pool: asyncpg.pool.Pool, glossary_service: GlossaryService):
        self.pool = pool
        self.glossary_service = glossary_service

    async def clean_up(self) -> None:
        query = """
        DELETE FROM calculation c
        WHERE c.updated_at < NOW() - INTERVAL '7 days'
        """
        await self.pool.execute(query)

    @staticmethod
    def _convert_pg_record_to_calculation(record: asyncpg.Record) -> Calculation:
        """Convert the dictionary coming from a record in the database to a Calculation object."""
        calculation_dict = {
            "uid": record["uid"],
            "retry_count": record["retry_count"],
            "child_of_root_node_uid": record[
                "root_node_uid"
            ],  # FIXME: As a hack, we store child_of_root_node_uid as rood_node_uid in the database.
        }
        calculation = Calculation(**calculation_dict)
        calculation.set_fields_from_compressed_data(record["data_input"])
        calculation.set_fields_from_compressed_data(record["data_result"])
        calculation.set_mutations_from_compressed_data(record["data_mutations"])
        return calculation

    @staticmethod
    def _convert_calculation_to_pg_dict(calculation: Calculation) -> dict:
        """Convert a Calculation object to a dictionary that can be stored in the database."""
        calculation_dict = calculation.model_dump(mode="json")
        db_record_dict = {
            "uid": calculation_dict["uid"],
            "root_node_uid": calculation_dict[
                "child_of_root_node_uid"
            ],  # FIXME: As a hack, we store child_of_root_node_uid as rood_node_uid in the database.
            "data_input": calculation.get_compressed_input(),
            "data_mutations": calculation.get_compressed_mutations(),
            "data_result": calculation.get_compressed_result(),
        }
        return db_record_dict

    async def decrement_retry_count(self, uid: str) -> None:
        """Decrement the retry count for a calculation."""
        await self.pool.execute("UPDATE calculation SET retry_count = retry_count - 1 WHERE uid = $1", uid)

    async def find_by_uid(self, uid: str, increment_retry_count: bool = False) -> Optional[Calculation]:
        """Find a calculation by its uid."""
        if increment_retry_count:
            record = await self.pool.fetchrow(
                "UPDATE calculation SET retry_count = retry_count + 1 WHERE uid = $1 RETURNING *", uid
            )
        else:
            record = await self.pool.fetchrow("SELECT * FROM calculation WHERE uid = $1", uid)
        if record is None:
            return None
        try:
            calculation = self._convert_pg_record_to_calculation(record)
            logger.debug("got calculation from db.")
            return calculation
        except TypeError as e:
            raise ValueError(f"Could not deserialize {record} to calculation: {e}") from e

    async def find_uid_by_root_node_uid(self, root_node_uid: str) -> list[str]:
        """Find a calculation by its uid."""
        records = await self.pool.fetch("SELECT uid FROM calculation WHERE root_node_uid = $1", root_node_uid)
        if not records:
            return []

        calculation_uids = []
        for record in records:
            calculation_uids.append(str(record["uid"]))
            logger.debug("got calculation from db.")
        return calculation_uids

    async def find_all_by_root_node_uids(self, uids: List[str]) -> List[Calculation]:
        """Find all calculations by their root node uids."""
        records = await self.pool.fetch("SELECT * FROM calculation WHERE root_node_uid = ANY($1::uuid[])", uids)
        if not records:
            return []

        calculations = []
        for record in records:
            calculation = self._convert_pg_record_to_calculation(record)
            calculations.append(calculation)
        logger.debug(f"got {len(calculations)} calculations from db.")
        return calculations

    async def insert_calculation(self, calculation: Calculation) -> Calculation:
        """Insert a calculation into the database."""
        db_record_dict = self._convert_calculation_to_pg_dict(calculation)

        await self.pool.execute(
            """
            INSERT INTO calculation(uid, root_node_uid, data_input, data_mutations, data_result)
            VALUES($1, $2, $3, $4, $5)
            """,
            db_record_dict["uid"],
            db_record_dict["root_node_uid"],
            db_record_dict["data_input"],
            db_record_dict["data_mutations"],
            db_record_dict["data_result"],
        )

        logger.debug("inserted calculation into db.", calculation_uid=calculation.uid)

        return calculation

    async def upsert_by_uid(self, calculation: Calculation) -> Calculation:
        """Upsert a calculation into the database."""
        db_record_dict = self._convert_calculation_to_pg_dict(calculation)

        await self.pool.fetchrow(
            """
            INSERT INTO calculation(
                uid,
                root_node_uid,
                data_input,
                data_mutations,
                data_result,
                updated_at
            )
            VALUES($1, $2, $3, $4, $5, NOW())
            ON CONFLICT (uid)
            DO UPDATE SET
                root_node_uid = $2,
                data_input = $3,
                data_mutations = $4,
                data_result = $5,
                updated_at = NOW()
            RETURNING *;
            """,
            db_record_dict["uid"],
            db_record_dict["root_node_uid"],
            db_record_dict["data_input"],
            db_record_dict["data_mutations"],
            db_record_dict["data_result"],
        )

        logger.debug("upserted calculation into db.")

        return calculation

    async def delete_calculation_by_id(self, uid: str) -> None:
        """Delete a calculation by its uid."""
        await self.pool.execute(
            "DELETE FROM calculation WHERE uid = $1",
            uid,
        )
