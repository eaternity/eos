import hashlib
import uuid
from os import urandom
from typing import Optional

import asyncpg
from asyncpg import Record
from structlog import get_logger

from core.domain.access_group_node import AccessGroup
from core.domain.namespace import Namespace
from core.domain.token import Token
from core.domain.user import User, UserGroupRoleEnum
from database.postgres.pg_access_mgr import PostgresAccessMgr
from database.postgres.pg_users_mgr import PostgresUsersMgr
from database.postgres.settings import PgSettings

logger = get_logger()


class PostgresNamespaceMgr:
    def __init__(self, pool: asyncpg.pool.Pool, pg_access_mgr: PostgresAccessMgr, pg_user_mgr: PostgresUsersMgr):
        self.pool = pool
        self.access_mgr = pg_access_mgr
        self.pg_user_mgr = pg_user_mgr

    # TODO: unused for now
    async def find_all(self) -> list[Namespace]:
        result_namespaces = await self.pool.fetch(
            "SELECT * FROM Namespace;",
        )

        if len(result_namespaces) > 0:
            namespaces = []

            for namespace_row in result_namespaces:
                namespace_row = dict(namespace_row)

                namespace = Namespace(
                    uid=str(namespace_row.get("uid")),
                    name=namespace_row.get("name"),
                )
                namespaces.append(namespace)

            return namespaces
        else:
            return []

    # TODO: used only by glossary router unit tests for now
    async def find_by_namespace_uid(self, namespace_uid: str) -> Optional[Namespace]:
        row = await self.pool.fetchrow(
            "SELECT * FROM Namespace WHERE uid = $1",
            namespace_uid,
        )

        return await self.process_namespace_entry(row)

    async def find_by_namespace_name(self, namespace_name: str) -> Optional[Namespace]:
        row = await self.pool.fetchrow(
            "SELECT * FROM Namespace WHERE name = $1",
            namespace_name,
        )

        return await self.process_namespace_entry(row)

    async def find_by_namespace_xid(self, namespace_xid: str) -> Optional[Namespace]:
        row = await self.pool.fetchrow(
            "SELECT * FROM Namespace WHERE xid = $1",
            namespace_xid,
        )

        return await self.process_namespace_entry(row)

    @staticmethod
    async def process_namespace_entry(row: Record) -> Optional[Namespace]:
        logger.info(f"record {row}")

        if row:
            namespace = Namespace(
                uid=str(row.get("uid")),
                xid=row.get("xid"),
                name=row.get("name"),
            )
            logger.info("got namespace from db.", namespace=namespace)

            return namespace
        else:
            return None

    async def insert_or_update_namespace_wrapper(
        self, namespace: Namespace, auth_token: str = None, access_group_xid: Optional[str] = None
    ) -> tuple[Namespace, str, AccessGroup]:
        """Wrapper method for creating/updating a namespace and its associated service account user."""
        # create/update a namespace
        namespace = await self.insert_or_update_namespace_by_uid_or_xid(namespace=namespace)

        # create/update a service account
        _, service_account = await self.insert_or_update_service_account_user(
            namespace_uid=namespace.uid,
            legacy_api_default_access_group_uid=None,
        )

        # create an access group
        default_access_group = AccessGroup(
            xid=access_group_xid if access_group_xid else "default",
            namespace_uid=namespace.uid,
            creator=service_account.user_id,
            data={
                "name": f"Default access group for namespace {namespace.name}",
                # TODO: maybe change this behaviour? users can change this location via service account though
                "location": "CH",
            },
        )

        # post this access group and add service account to it with "Admin" role
        await self.access_mgr.upsert_access_group_by_xid(node=default_access_group)

        # fill service user's `legacy_api_default_access_group_uid` field
        token, service_account = await self.insert_or_update_service_account_user(
            namespace_uid=namespace.uid,
            legacy_api_default_access_group_uid=default_access_group.uid,
            auth_token=auth_token,
        )

        return namespace, token, default_access_group

    async def insert_or_update_namespace_by_uid_or_xid(self, namespace: Namespace) -> Namespace:
        # inserting/updating a namespace entry
        row = await self.pool.fetchrow(
            """
            INSERT INTO Namespace(uid, xid, name, updated_at)
            VALUES(
                CASE
                    WHEN $1::uuid IS NULL THEN gen_random_uuid()
                    ELSE $1::uuid
                END,
                $2, $3, NOW()
            )
            ON CONFLICT (xid)
            DO UPDATE SET
                name = $3,
                updated_at = NOW()
            RETURNING uid;
            """,
            namespace.uid,
            namespace.xid,
            namespace.name,
        )
        namespace.uid = str(row[0])
        logger.info("upserted namespace into db.", namespace=namespace)

        return namespace

    async def insert_or_update_service_account_user(
        self, namespace_uid: str, legacy_api_default_access_group_uid: str | None, auth_token: str = None
    ) -> tuple[str, User]:
        # creating a service account user
        # generated user id is based on namespace's id
        service_account_user_id = uuid.UUID(hashlib.md5(namespace_uid.encode("utf-8")).hexdigest())
        service_account_user = User(
            user_id=str(service_account_user_id),
            email=None,
            # TODO: rework this line once we add password-based authentication
            password="",
            is_superuser=False,
            is_service_account=True,
            legacy_api_default_access_group_uid=legacy_api_default_access_group_uid,
        )

        # receiving this user's token
        token = await self.pg_user_mgr.insert_or_update_user(service_account_user, auth_token)

        logger.info(
            "upserted service account user into db.",
            namespace=namespace_uid,
            legacy_api_default_access_group_uid=legacy_api_default_access_group_uid,
            token=token,
        )

        return token, service_account_user

    async def seed_namespaces(self) -> AccessGroup:
        settings = PgSettings()

        namespace = Namespace(
            name=settings.EATERNITY_NAMESPACE_NAME,
            xid=settings.EATERNITY_NAMESPACE_XID,
            uid=settings.EATERNITY_NAMESPACE_UUID,
        )
        _, _, default_access_group = await self.insert_or_update_namespace_wrapper(namespace)

        salt = urandom(16).hex()
        hashed_password = hashlib.scrypt(
            settings.SEED_SUPERUSER_PASSWORD.encode("utf-8"),
            salt=bytes.fromhex(salt),
            n=16384,
            r=8,
            p=1,
            maxmem=32 * 1024 * 1024,
            dklen=64,
        )
        superuser = User(
            user_id=str(uuid.uuid4()),
            email="superuser@eaternity.ch",
            password=f"{salt}:{hashed_password.hex()}",
            legacy_api_default_access_group_uid=default_access_group.uid,
            is_superuser=True,
            is_service_account=False,
        )
        await self.pg_user_mgr.insert_or_update_user(superuser)

        # add the superuser with group_permissions to the default_access_group:
        permissions = {permission: True for permission in UserGroupRoleEnum.ADMIN}
        await self.access_mgr.upsert_member_into_group_by_group_uid(
            superuser.user_id, default_access_group.uid, permissions
        )

        updated_token = Token(
            user_id=superuser.user_id,
            token=settings.EATERNITY_TOKEN,
            name="Default",
        )

        # update the access token to be as defined in env variable:
        await self.pg_user_mgr.update_access_token(updated_token)

        return default_access_group
