
ALTER TABLE public.matching_required 
ADD COLUMN root_activity_date DATE,
ADD COLUMN root_create_date DATE,
ADD COLUMN root_namespace_uid uuid;

CREATE INDEX IF NOT EXISTS idx_matching_required_activity_date ON matching_required(root_activity_date);
CREATE INDEX IF NOT EXISTS idx_matching_required_root_create_date ON matching_required(root_create_date);
CREATE INDEX IF NOT EXISTS idx_matching_required_root_namespace ON matching_required(root_namespace_uid);

CREATE OR REPLACE FUNCTION update_matching_required_metadata()
RETURNS TRIGGER AS $$
BEGIN
    SELECT 
        (root_activity.data->>'activity_date')::DATE,
        rn.created_at::DATE,
        rns.uid
    INTO 
        NEW.root_activity_date,
        NEW.root_create_date,
        NEW.root_namespace_uid
    FROM node rn
    INNER JOIN access_group_node ragn ON ragn.uid = rn.access_group_uid
    INNER JOIN namespace rns ON rns.uid = ragn.namespace_uid
    INNER JOIN edge e ON e.parent_uid = NEW.root_node_uid and e.edge_type = 'root_to_recipe'
    INNER JOIN node root_activity ON root_activity.uid = e.child_uid
    WHERE rn.uid = NEW.root_node_uid;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER matching_required_metadata_trigger
    BEFORE INSERT OR UPDATE ON matching_required
    FOR EACH ROW
    EXECUTE FUNCTION update_matching_required_metadata();
    
-- One-time update for existing rows
WITH update_data AS (
    SELECT 
        mr.node_uid,
        mr.root_node_uid,
        (root_activity.data->>'activity_date')::DATE as activity_date,
        rn.created_at::DATE as root_create_date,
        rns.uid as root_namespace_uid
    FROM matching_required mr
    INNER JOIN node rn ON rn.uid = mr.root_node_uid
    INNER JOIN access_group_node ragn ON ragn.uid = rn.access_group_uid
    INNER JOIN namespace rns ON rns.uid = ragn.namespace_uid
    INNER JOIN edge e ON e.parent_uid = mr.root_node_uid
    INNER JOIN node root_activity ON root_activity.uid = e.child_uid
)
UPDATE matching_required mr
SET 
    root_activity_date = ud.activity_date,
    root_create_date = ud.root_create_date,
    root_namespace_uid = ud.root_namespace_uid
FROM update_data ud
WHERE mr.node_uid = ud.node_uid 
AND mr.root_node_uid = ud.root_node_uid;
