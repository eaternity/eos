CREATE TABLE IF NOT EXISTS public.matching_applied (
    node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    namespace_uid uuid REFERENCES public.namespace (uid) ON DELETE CASCADE,
    matching_uid uuid REFERENCES public.matching (uid) ON DELETE CASCADE,
    matching_context JSONB,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_matching_applied PRIMARY KEY (node_uid, matching_uid)
);

CREATE INDEX IF NOT EXISTS matching_applied_node_uid_idx ON public.matching_applied (node_uid);
CREATE INDEX IF NOT EXISTS matching_applied_namespace_uid_idx ON public.matching_applied (namespace_uid);
CREATE INDEX IF NOT EXISTS matching_applied_matching_uid_idx ON public.matching_applied (matching_uid);
