-- first drop the old index if it exists
DROP INDEX IF EXISTS idx_required_matching_exists;

-- then create the new index
CREATE INDEX idx_required_matching_exists ON public.node
    USING btree ((public.node."data" ? 'required_matching'))
    WHERE public.node."data" ? 'required_matching';