
CREATE OR REPLACE FUNCTION mark_recipe_as_deleted_with_lock(recipe_uid uuid)
RETURNS void AS $$
BEGIN
    -- Acquire the lock first
    PERFORM pg_advisory_xact_lock(hashtext(recipe_uid::text));
    -- Then perform the update
    UPDATE node
    SET deleted_at = NOW()
    WHERE uid IN (
        SELECT uid AS relevant_uid
        FROM node
        WHERE uid = recipe_uid
        UNION
        SELECT child_uid AS relevant_uid
        FROM edge
        WHERE parent_uid = recipe_uid
        UNION
        SELECT parent_uid AS relevant_uid
        FROM edge
        WHERE child_uid = recipe_uid
        AND edge_type = 'root_to_recipe'
    );
END;
$$ LANGUAGE plpgsql;
