-- Migration to add the post_calc_graph column to the node table (the post_calc_data will not be used anymore)
ALTER TABLE public.node
ADD COLUMN post_calc_cache JSONB;
