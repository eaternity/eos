CREATE OR REPLACE FUNCTION upsert_recipe_with_lock(
    transient bool, 
    activity_uid uuid, 
    activity_access_group_uid uuid, 
    activity_node_type text, 
    activity_data jsonb,
    incoming_child_jsonb jsonb,
    incoming_parent_uid uuid,
    incoming_parent_access_group_uid uuid,
    incoming_parent_node_type text,
    incoming_parent_data jsonb,
    unmatch_based_on_ingredients_declaration boolean,
    incoming_md5_hash uuid
)
RETURNS jsonb AS $$
DECLARE
    result jsonb;
BEGIN

    -- CREATE TEMP TABLE existing_child_nodes AS
    -- SELECT
    --     n.uid,
    --     n.data,
    --     e.parent_uid,
    --     (n.data->>'declaration_index')::integer AS declaration_index,
    --     n.data->'product_name' AS jsonb,
    --     (n.data->>'ingredients_declaration')::integer AS jsonb
    -- FROM node n
    -- JOIN edge e ON n.uid = e.child_uid
    -- WHERE e.parent_uid = activity_uid::uuid;

    -- Create temporary table for the operation
    CREATE TEMP TABLE temp_children ON COMMIT DROP
    AS
    SELECT
        (elem ->> 0)::uuid AS uid,
        (elem ->> 1)::uuid AS access_group_uid,
        (elem ->> 2)::text AS node_type,
        (elem ->> 3)::jsonb AS data,
        CASE 
            WHEN (elem -> 3 ? 'declaration_index') THEN ((elem -> 3 -> 'declaration_index' ->> 'value')::integer)
            ELSE NULL 
        END AS declaration_index
    FROM jsonb_array_elements(incoming_child_jsonb) AS elem;

    SET LOCAL plan_cache_mode = 'force_custom_plan';

    -- Acquire the lock
    PERFORM pg_advisory_xact_lock(hashtext(activity_uid::text));

    -- In the main query, replace the original subquery with the temp table
    WITH upserted_children AS (
        INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at)
        SELECT
            COALESCE(
                tc.uid,
                (
                    SELECT n.uid
                    FROM node n JOIN edge e ON n.uid = e.child_uid
                    WHERE e.parent_uid = activity_uid::uuid AND
                    (n.data -> 'declaration_index' ->> 'value')::integer = (tc.data -> 'declaration_index' ->> 'value')::integer
                    LIMIT 1
                ),
                public.uuid_generate_v4()
            ),
            tc.access_group_uid, tc.node_type,
            COALESCE(
                (
                    SELECT
                        CASE
                            WHEN
                            (
                                (
                                    (n.data ->> 'product_name')::jsonb = (tc.data ->> 'product_name')::jsonb
                                    OR
                                    (
                                        EXISTS (
                                            SELECT 1
                                            FROM jsonb_array_elements(n.data -> 'product_name') AS n_elem,
                                                    jsonb_array_elements(tc.data -> 'product_name') AS inc_elem
                                            WHERE n_elem.value = inc_elem.value
                                        )
                                    )
                                )
                                AND (
                                    unmatch_based_on_ingredients_declaration = FALSE
                                    OR
                                    (tc.data ->> 'ingredients_declaration')::jsonb IS NULL
                                    OR (
                                        (n.data ->> 'ingredients_declaration')::jsonb IS NOT DISTINCT FROM
                                        (tc.data ->> 'ingredients_declaration')::jsonb
                                    )
                                )
                            )
                            THEN
                                tc.data::jsonb ||
                                jsonb_build_object(
                                    'link_to_sub_node',
                                    COALESCE(
                                        (tc.data ->> 'link_to_sub_node')::jsonb,
                                        (n.data ->> 'link_to_sub_node')::jsonb
                                    )
                                ) ||
                                jsonb_build_object(
                                    'ingredients_declaration',
                                    COALESCE(
                                        (tc.data ->> 'ingredients_declaration')::jsonb,
                                        (n.data ->> 'ingredients_declaration')::jsonb
                                    )
                                )
                            ELSE
                                jsonb_set(
                                    tc.data::jsonb,
                                    '{ingredients_declaration}',
                                    COALESCE(
                                        (tc.data ->> 'ingredients_declaration')::jsonb,
                                        (n.data ->> 'ingredients_declaration')::jsonb
                                    ),
                                    true
                                )
                        END
                    FROM node n JOIN edge e ON n.uid = e.child_uid
                    WHERE e.parent_uid = activity_uid::uuid AND
                    (n.data -> 'declaration_index' ->> 'value')::integer = (tc.data -> 'declaration_index' ->> 'value')::integer
                ),
                tc.data::jsonb
            ) AS data,
            NOW(),
            CASE
                WHEN transient = TRUE THEN NOW()
                ELSE NULL
            END AS deleted_at
        FROM temp_children tc
        ON CONFLICT (uid) DO UPDATE SET
        access_group_uid = EXCLUDED.access_group_uid,
        node_type = EXCLUDED.node_type,
        data = EXCLUDED.data,
        updated_at = EXCLUDED.updated_at,
        deleted_at = EXCLUDED.deleted_at
        RETURNING uid, data, access_group_uid
    ),
    deleted_child_nodes AS (
        -- Delete children nodes that were not upserted (i.e., deleted)
        DELETE FROM node
        USING edge
        LEFT JOIN upserted_children uc ON edge.child_uid = uc.uid
        WHERE node.uid = edge.child_uid
        AND edge.parent_uid = activity_uid::uuid
        AND uc.uid IS NULL
        RETURNING *
    ),
    child_edges_of_upserted_children_to_delete AS (
        SELECT edge.parent_uid, edge.child_uid, edge.edge_type FROM edge
        JOIN upserted_children ON
        edge.parent_uid = upserted_children.uid
        WHERE edge.edge_type = 'link_to_subrecipe'
    ),
    children_link_to_sub_nodes AS (
        SELECT uc.uid AS uid, (uc.data ->> 'link_to_sub_node')::jsonb AS link_to_sub_node, access_group_uid
        FROM upserted_children AS uc
    ),
    newly_added_child_edges_of_upserted_children AS (
        SELECT clsn.uid AS parent_uid,
        CASE
            WHEN ((clsn.link_to_sub_node ->> 'prop_type')::text = 'LinkToUidProp') THEN
                json_build_object(
                    'uid', (clsn.link_to_sub_node ->> 'uid')::uuid
                )
            WHEN ((clsn.link_to_sub_node ->> 'prop_type')::text = 'LinkToXidProp') THEN
                json_build_object(
                    'xid', (clsn.link_to_sub_node ->> 'xid')::text,
                    'access_group_uid', clsn.access_group_uid
                )
            ELSE
                NULL
        END AS child_xid_or_uid
        FROM children_link_to_sub_nodes AS clsn
    ),
    upserted_parent AS (
        INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at)
        SELECT
            COALESCE(
                incoming_parent.uid::uuid,
                (
                    SELECT n.uid
                    FROM node n JOIN edge e ON n.uid = e.parent_uid
                    WHERE e.child_uid = activity_uid::uuid
                    AND e.edge_type = 'root_to_recipe'
                    LIMIT 1  -- Limit the result to ensure only one value is returned
                ),
                public.uuid_generate_v4()
            ),
            incoming_parent.access_group_uid::uuid, incoming_parent.node_type::text,
            incoming_parent.data::jsonb,
            NOW(),
            CASE
                WHEN transient = TRUE THEN NOW()
                ELSE NULL
            END AS deleted_at
        FROM (
            VALUES (incoming_parent_uid, incoming_parent_access_group_uid, incoming_parent_node_type, incoming_parent_data)
        )
        AS incoming_parent (uid, access_group_uid, node_type, data)
        WHERE incoming_parent_node_type IS NOT NULL
        ON CONFLICT (uid) DO UPDATE SET
        access_group_uid = EXCLUDED.access_group_uid, node_type = EXCLUDED.node_type, data = EXCLUDED.data,
        updated_at = EXCLUDED.updated_at, deleted_at = EXCLUDED.deleted_at
        RETURNING uid
    ),
    deleted_parent_node AS (
        -- Delete parent nodes that were not upserted (i.e., deleted)
        DELETE FROM node
        USING edge
        LEFT JOIN upserted_parent up on edge.parent_uid = up.uid
        WHERE node.uid = edge.parent_uid
        AND edge.child_uid = activity_uid::uuid
        AND edge.edge_type = 'root_to_recipe'
        AND up.uid IS NULL
        RETURNING *
    ),
    deleted_other_children_of_parent_node AS (
        -- Delete edges to other children nodes of the parent node: A root flow can only have a single recipe.
        DELETE FROM edge
        WHERE edge.parent_uid IN (SELECT uid FROM upserted_parent)
        AND edge.child_uid != activity_uid::uuid
        RETURNING *
    ),
    upserted_self_node AS (
        INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at, md5_hash)
        SELECT
            self_node.uid::uuid, self_node.access_group_uid::uuid, self_node.node_type::text, self_node.data::jsonb,
            NOW(),
            CASE
                WHEN transient = TRUE THEN NOW()
                ELSE NULL
            END AS deleted_at,
            NULL::uuid
        FROM (
            VALUES (activity_uid, activity_access_group_uid, activity_node_type, activity_data)
        )
        AS self_node (uid, access_group_uid, node_type, data)
        ON CONFLICT (uid) DO UPDATE SET
        access_group_uid = EXCLUDED.access_group_uid, node_type = EXCLUDED.node_type, data = EXCLUDED.data,
        updated_at = EXCLUDED.updated_at, deleted_at = EXCLUDED.deleted_at, md5_hash = NULL::uuid
        RETURNING uid
    ),
    inserted_edges AS (
        INSERT INTO edge (parent_uid, child_uid, edge_type)
        SELECT
            CASE
                WHEN EXISTS (SELECT 1 FROM upserted_parent) THEN (SELECT uid FROM upserted_parent)
                ELSE NULL
            END AS parent_uid,
            (SELECT uid FROM upserted_self_node) AS child_uid,
            'root_to_recipe' AS edge_type
        WHERE EXISTS (SELECT 1 FROM upserted_parent)
        UNION ALL
        SELECT
            (SELECT uid FROM upserted_self_node) AS parent_uid,
            uc.uid AS child_uid,
            'ingredient' AS edge_type
        FROM upserted_children uc
        ON CONFLICT DO NOTHING
    )
    SELECT json_build_object (
        'edges_to_delete',
        CASE
            WHEN EXISTS (SELECT 1 FROM upserted_children) THEN
                (
                    SELECT json_agg(child_edges_of_upserted_children_to_delete)
                    FROM child_edges_of_upserted_children_to_delete
                )
            ELSE NULL
        END,
        'edges_to_insert',
        CASE
            WHEN EXISTS (SELECT 1 FROM upserted_children) THEN
                (
                    SELECT json_agg(newly_added_child_edges_of_upserted_children)
                    FROM newly_added_child_edges_of_upserted_children
                )
            ELSE NULL
        END,
        'self_uid', (SELECT uid FROM upserted_self_node),
        'parent_uid',
        CASE
            WHEN EXISTS (SELECT 1 FROM upserted_parent) THEN (SELECT uid FROM upserted_parent)
            ELSE NULL
        END
    )::jsonb INTO result;

    -- now check if edges_to_delete and edges_to_insert are empty and if so, set md5_hash:
    IF (result->>'edges_to_insert' IS NULL OR jsonb_array_length(result->'edges_to_insert') = 0) AND 
       (result->>'edges_to_delete' IS NULL OR jsonb_array_length(result->'edges_to_delete') = 0) THEN
        UPDATE node SET md5_hash = incoming_md5_hash WHERE uid = activity_uid;
    END IF;
    
    RETURN result;
END;
$$ LANGUAGE plpgsql;

-- Add new indexes:
CREATE INDEX idx_node_declaration_index ON public.node (((data -> 'declaration_index' ->> 'value')::integer))
WHERE data ? 'declaration_index';

CREATE INDEX idx_node_product_name ON public.node ((data->>'product_name')) 
WHERE data ? 'product_name';

CREATE INDEX idx_edge_parent_ingredient ON edge (parent_uid, edge_type, child_uid) 
WHERE edge_type = 'ingredient';

CREATE INDEX idx_edge_link_to_subrecipe ON edge (edge_type, parent_uid, child_uid) 
WHERE edge_type = 'link_to_subrecipe';

CREATE INDEX idx_edge_parent_child ON edge (parent_uid, child_uid, edge_type);
CREATE INDEX idx_node_data_product ON node ((data->>'product_name'), uid) 
WHERE data ? 'product_name';
