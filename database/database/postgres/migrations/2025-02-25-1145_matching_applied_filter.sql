
DROP TABLE IF EXISTS public.matching_applied CASCADE;

-- table storing information about the applied matching
CREATE TABLE public.matching_applied (
    node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    root_node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    root_activity_date DATE,
    root_create_date DATE,
    root_namespace_uid uuid REFERENCES public.namespace (uid) ON DELETE CASCADE,
    matching_uid uuid REFERENCES public.matching (uid) ON DELETE CASCADE,
    matching_context JSONB,
    CONSTRAINT pk_matching_applied PRIMARY KEY (node_uid, root_node_uid, matching_uid)
);
CREATE INDEX IF NOT EXISTS matching_applied_node_uid_idx ON public.matching_applied (node_uid);
CREATE INDEX IF NOT EXISTS matching_applied_root_node_uid_idx ON public.matching_applied (root_node_uid);
CREATE INDEX IF NOT EXISTS matching_applied_activity_date_idx ON matching_applied(root_activity_date);
CREATE INDEX IF NOT EXISTS matching_applied_root_create_date_idx ON matching_applied(root_create_date);
CREATE INDEX IF NOT EXISTS matching_applied_root_namespace_uid_idx ON matching_applied(root_namespace_uid);
CREATE INDEX IF NOT EXISTS matching_applied_matching_uid_idx ON public.matching_applied (matching_uid);

CREATE TRIGGER matching_applied_metadata_trigger
    BEFORE INSERT OR UPDATE ON matching_applied
    FOR EACH ROW
    EXECUTE FUNCTION update_matching_required_metadata(); 
