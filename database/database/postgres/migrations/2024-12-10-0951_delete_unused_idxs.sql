-- Remove unused indexes:

DROP INDEX IF EXISTS idx_edge_link_to_subrecipe;
DROP INDEX IF EXISTS idx_edge_parent_ingredient;
DROP INDEX IF EXISTS idx_node_declaration_index_value;
DROP INDEX IF EXISTS idx_required_matching_exists;
DROP INDEX IF EXISTS idx_node_data_product;
DROP INDEX IF EXISTS idx_node_declaration_index;
DROP INDEX IF EXISTS idx_node_product_name;
