
ALTER TABLE public.matching
ADD COLUMN data JSONB;

ALTER TABLE public.matching DROP CONSTRAINT uq_gfm_lang_matching_string; 
CREATE UNIQUE INDEX ON public.matching (gap_filling_module, lang, matching_string, jsonb_hash_extended(coalesce(data, '{}'::jsonb), 0));

CREATE TABLE public.matching_required (
    node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    root_node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_matching_required PRIMARY KEY (node_uid, root_node_uid)
);
