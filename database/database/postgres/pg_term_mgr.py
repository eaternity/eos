import json
import os
import re
import time
import uuid
from enum import Enum
from typing import Dict, NamedTuple, Optional

import asyncpg
from structlog import get_logger

from core.domain.access_group_node import AccessGroup
from core.domain.term import Term
from database.postgres.settings import PgSettings

logger = get_logger()
IPCC_2013_GWP_100_XID = "ipcc-2013cg.bd5af3f67229a1cc291b8ecb7f316fcf"
IPCC_2013_GWP_20_XID = "ipcc-2013cg.13c1130fb359a78e9b2f59a87653fc37"
IPCC_2013_GWP_100 = "IPCC 2013 climate change GWP 100a"
IPCC_2013_GWP_20 = "IPCC 2013 climate change GWP 20a"


class TmrelUnitEnum(str, Enum):
    g = "g"
    kcal = "kcal"
    percent = "%"


class TmrelNormalizationEnum(str, Enum):
    per_dfu = "per_dfu"
    per_total_energy = "per_total_energy"
    per_self = "per_self"


class GbdNumbers(NamedTuple):
    current_daly: float
    legacy_daly: float
    tmrel_lower: float
    tmrel_upper: float
    tmrel_unit: TmrelUnitEnum
    tmrel_normalization: TmrelNormalizationEnum


class Effect(str, Enum):
    positive = "positive"
    negative = "negative"


class GbdData(NamedTuple):
    description: str
    health_effect: Effect
    gbd_numbers: GbdNumbers


class PgTermMgr:
    """Class for managing terms in the database."""

    def __init__(self, pool: asyncpg.pool.Pool) -> None:
        self.pool = pool

        self.root_term_uid = uuid.UUID(PgSettings().ROOT_GLOSSARY_TERM_UUID)
        self.insert_instead_of_upsert = False

    async def find_all(self) -> list[Term]:
        """Find all terms in the database."""
        result_terms = await self.pool.fetch("SELECT * FROM Term")
        values = [dict(record) for record in result_terms]

        if len(values) > 0:
            terms = []

            for term_row in values:
                term = Term(
                    uid=term_row.get("uid"),
                    xid=term_row.get("xid"),
                    name=term_row.get("name"),
                    data=term_row.get("data"),
                    sub_class_of=term_row.get("sub_class_of"),
                    access_group_uid=term_row.get("access_group_uid"),
                )
                terms.append(term)

            return terms
        else:
            return []

    async def find_all_term_access_groups(self) -> list[AccessGroup]:
        """Find all access groups that have terms in them."""
        result_groups = await self.pool.fetch(
            """
            SELECT * FROM access_group_node
            JOIN
            (SELECT DISTINCT access_group_uid FROM term) AS term_groups
            ON term_groups.access_group_uid = access_group_node.uid;
            """
        )
        values = [dict(record) for record in result_groups]

        if len(values) > 0:
            access_groups = []

            for row in values:
                access_group = AccessGroup(
                    uid=str(row.get("uid")),
                    xid=str(row.get("xid")),
                    type=row.get("type"),
                    data=row.get("data"),
                    creator=row.get("creator"),
                    namespace_uid=str(row.get("namespace_uid")),
                )
                access_groups.append(access_group)

            return access_groups
        else:
            return []

    async def get_sub_terms_by_uid(
        self, sub_class_of_uid: str, max_depth: int = 10, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Term]:
        """Get all sub terms of a term by its uid."""
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            WITH RECURSIVE cte (uid, sub_class_of, xid, name, data, access_group_uid, depth) AS (
                    SELECT uid, sub_class_of, xid, name, data, access_group_uid, 1
                    FROM term
                    WHERE term.sub_class_of = $1
                UNION ALL
                    SELECT term.uid, term.sub_class_of, term.xid, term.name, term.data, term.access_group_uid, depth+1
                    FROM term
                    JOIN cte r
                    ON term.sub_class_of = r.uid
                    WHERE depth < $2
            )
            SELECT * FROM cte;
            """,
            sub_class_of_uid,
            max_depth,
        )

        logger.debug("got records from db.", records=records)

        values = [dict(record) for record in records]

        if len(values) > 0:
            terms = []

            for term_row in values:
                term = Term(
                    uid=term_row.get("uid"),
                    xid=term_row.get("xid"),
                    name=term_row.get("name"),
                    data=term_row.get("data"),
                    sub_class_of=term_row.get("sub_class_of"),
                    access_group_uid=term_row.get("access_group_uid"),
                )
                terms.append(term)

            return terms
        else:
            return []

    async def get_terms_by_xid(self, term_xid: str) -> Optional[list[Term]]:
        """Get all terms by their xid."""
        records = await self.pool.fetch(
            "SELECT * FROM Term WHERE xid = $1",
            term_xid,
        )

        try:
            records = [dict(record) for record in records]
        except TypeError:
            return None

        return [
            Term(
                uid=term.get("uid"),
                xid=term.get("xid"),
                name=term.get("name"),
                data=term.get("data"),
                sub_class_of=term.get("sub_class_of"),
                access_group_uid=term.get("access_group_uid"),
            )
            for term in records
        ]

    async def get_term_by_xid_and_access_group_uid(self, term_xid: str, access_group_uid: str) -> Optional[Term]:
        """Get a term by its xid and access group uid."""
        record = await self.pool.fetchrow(
            "SELECT * FROM Term WHERE xid = $1 AND access_group_uid = $2",
            term_xid,
            access_group_uid,
        )

        try:
            row = dict(record)
        except TypeError:
            return None

        return Term(
            uid=row.get("uid"),
            xid=row.get("xid"),
            name=row.get("name"),
            data=row.get("data"),
            sub_class_of=row.get("sub_class_of"),
            access_group_uid=row.get("access_group_uid"),
        )

    async def get_term_by_uid(self, uid: uuid.UUID) -> Optional[Term]:
        """Get a term by its uid."""
        record = await self.pool.fetchrow("SELECT * FROM Term WHERE uid = $1", uid)

        try:
            row = dict(record)
        except TypeError:
            return None

        return Term(
            uid=row.get("uid"),
            xid=row.get("xid"),
            name=row.get("name"),
            data=row.get("data"),
            sub_class_of=row.get("sub_class_of"),
            access_group_uid=row.get("access_group_uid"),
        )

    async def insert_term(self, term: Term) -> Term:
        """Insert a term into the database."""
        if term.uid is None:
            term.uid = uuid.uuid4()

        await self.pool.execute(
            """
            INSERT INTO Term(uid, xid, name, data, sub_class_of, access_group_uid)
            VALUES($1, $2, $3, $4, $5, $6)
            ON CONFLICT (uid) DO UPDATE SET xid = $2, name = $3, data = $4, sub_class_of = $5, access_group_uid = $6;
            """,
            term.uid,
            term.xid,
            term.name,
            term.data,
            term.sub_class_of,
            term.access_group_uid,
        )

        return term

    async def add_term_data(self, term_uid: uuid.UUID, term_data: dict) -> None:
        """Append the data in term_data to the data of the term with uid term_uid."""
        await self.pool.execute(
            """
            UPDATE term SET data = term.data::jsonb || $2 WHERE uid = $1;
            """,
            term_uid,
            term_data,
        )

    async def insert_many_terms(self, terms: list[Term]) -> None:
        """Insert many terms into the database.

        This is supposed to be faster than inserting each term individually if the list of terms is long.
        If the uid a term already exists in the database, the term will be updated instead of inserted.
        """
        collected_values = []

        for term in terms:
            collected_values.append(
                (
                    str(uuid.uuid4()) if term.uid is None else term.uid,
                    term.xid,
                    term.name,
                    term.data,
                    term.sub_class_of,
                    term.access_group_uid,
                )
            )

        await self.pool.executemany(
            """
            INSERT INTO Term(uid, xid, name, data, sub_class_of, access_group_uid)
            VALUES($1, $2, $3, $4, $5, $6)
            ON CONFLICT (uid) DO UPDATE SET xid = $2, name = $3, data = $4, sub_class_of = $5, access_group_uid = $6;
            """,
            collected_values,
        )

    async def find_correct_uid(self, term_to_upsert: Term) -> uuid.UUID:
        """Return the uid of a matching term in the DB if it exists, else return the uid of the input term."""
        term_in_db = await self.get_term_by_xid_and_access_group_uid(
            term_to_upsert.xid, str(term_to_upsert.access_group_uid)
        )
        if term_in_db:
            # uid was found in DB -> use the uid from the DB
            return term_in_db.uid
        else:
            # uid was not found in DB -> use the original uid if it had a UUID, else create a new UUID
            return uuid.uuid4() if term_to_upsert.uid is None else term_to_upsert.uid

    async def upsert_term(self, term_to_upsert: Term, upserted_terms: Optional[list[Term]] = None) -> Term:
        """Upsert a term into the database."""
        if not self.insert_instead_of_upsert:
            term_to_upsert.uid = await self.find_correct_uid(term_to_upsert)
        inserted_term = await self.insert_term(term_to_upsert)
        if upserted_terms is not None:
            upserted_terms.append(inserted_term)
        return inserted_term

    async def upsert_many_terms(self, terms: [Term], upserted_terms: Optional[list[Term]] = None) -> None:
        """Upsert many terms into the database.

        This is supposed to be faster than upserting each term individually if the list of terms is long.
        """
        if not self.insert_instead_of_upsert:
            old_to_new_uid_dict = {}
            # first update all uids with uid from DB if they exist
            for term in terms:
                old_uid = term.uid
                term.uid = await self.find_correct_uid(term)
                new_uid = term.uid
                if old_uid != new_uid:
                    old_to_new_uid_dict[old_uid] = new_uid
            # then update all sub_class_of with new uids
            for term in terms:
                if term.sub_class_of in old_to_new_uid_dict:
                    term.sub_class_of = old_to_new_uid_dict[term.sub_class_of]
        # and insert them to the DB
        await self.insert_many_terms(terms)
        if upserted_terms is not None:
            upserted_terms.extend(terms)

    async def seed_glossary(
        self,
        default_eaternity_access_group_uid: str,
        fao_codes_access_group_uid: str,
        foodex2_access_group_uid: str,
        eurofir_access_group_uid: str,
        gadm_access_group_uid: str,
        perishability_access_group_uid: str,
        node_tag_access_group_uid: str,
        impact_assessment_access_group_uid: str,
        subdivision_access_group_uid: str,
        transport_access_group_uid: str,
        upserted_terms: Optional[list[Term]] = None,
    ) -> tuple[Term, Term, Term, Term, Term, Term]:
        """Seed the glossary into the database."""
        start_time = time.time()

        logger.info("start seeding glossary...")
        my_path = os.path.abspath(os.path.dirname(__file__))

        glossary_path = []
        basic_glossary_term = Term(
            uid=uuid.UUID(PgSettings().ROOT_GLOSSARY_TERM_UUID),
            name="glossary",
            sub_class_of=None,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        glossary_path.append(basic_glossary_term)
        await self.upsert_term(basic_glossary_term, upserted_terms=upserted_terms)

        # root impact assessments term
        impact_assessment_access_group_uid = impact_assessment_access_group_uid
        root_glossary_term_uuid = PgSettings().ROOT_GLOSSARY_TERM_UUID
        logger.info("Adding root impact assessments term.")
        impact_assessment_root_term = Term(
            uid=uuid.uuid4(),
            name="Impact Assessments",
            sub_class_of=uuid.UUID(root_glossary_term_uuid),
            xid="Root_Impact_Assessments",
            data={},
            access_group_uid=impact_assessment_access_group_uid,
        )
        await self.upsert_term(impact_assessment_root_term, upserted_terms=upserted_terms)

        dfu_term = Term(
            uid=uuid.uuid4(),
            xid="EOS_daily_food_unit",
            name="Daily food unit",
            sub_class_of=impact_assessment_root_term.uid,
            access_group_uid=uuid.UUID(impact_assessment_access_group_uid),
            data={
                "description": "Daily food unit computed as ("
                "proteins in g / 50 + fats in g / 66 + energy in kJ / 6000 "
                "+ water in g / 2500 + dry mass in g / 600"
                ") / 5"
            },
        )
        await self.upsert_term(dfu_term, upserted_terms=upserted_terms)

        vitascore_term = Term(
            uid=uuid.uuid4(),
            xid="EOS_vita-score",
            name="Vita score",
            sub_class_of=impact_assessment_root_term.uid,
            access_group_uid=uuid.UUID(impact_assessment_access_group_uid),
            data={"description": "Vita score computed from global burden of diseases study."},
        )
        await self.upsert_term(vitascore_term, upserted_terms=upserted_terms)

        # root nutrients Term
        nutrients_root_term = Term(
            uid=uuid.uuid4(),
            name="Nutrients",
            sub_class_of=basic_glossary_term.uid,
            xid="Root_Nutrients",  # used later when importing nutrients from EDB
            data={},
            access_group_uid=uuid.UUID(eurofir_access_group_uid),
        )
        await self.upsert_term(nutrients_root_term, upserted_terms=upserted_terms)

        # root water scarcity Term
        water_scarcity_root_term = Term(
            uid=uuid.uuid4(),
            name="Water Scarcity",
            sub_class_of=basic_glossary_term.uid,
            xid="Root_Water_Scarcity",  # used later when importing water scarcity from EDB
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(water_scarcity_root_term, upserted_terms=upserted_terms)

        # root critical product content Term
        critical_product_content = Term(
            uid=uuid.uuid4(),
            name="Critical Product Content",
            sub_class_of=basic_glossary_term.uid,
            xid="Root_Critical_Product_Content",  # used later when importing water scarcity from EDB
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(critical_product_content, upserted_terms=upserted_terms)

        # root GADM Term
        gadm_root_term = Term(
            uid=uuid.uuid4(),
            name="GADM",
            sub_class_of=basic_glossary_term.uid,
            xid="Root_GADM",  # used later when importing GADM
            data={},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_root_term, upserted_terms=upserted_terms)

        # root FAO Term
        fao_root_term = Term(
            uid=uuid.uuid4(),
            name="FAO Codes",
            sub_class_of=basic_glossary_term.uid,
            xid="Root_FAO",  # used later when importing FAO
            data={},
            access_group_uid=uuid.UUID(fao_codes_access_group_uid),
        )
        await self.upsert_term(fao_root_term, upserted_terms=upserted_terms)

        # custom FAO terms
        fao_local_term = Term(
            uid=uuid.uuid4(),
            name="Local production FAO code",
            sub_class_of=fao_root_term.uid,
            xid="100000",
            data={},
            access_group_uid=uuid.UUID(fao_codes_access_group_uid),
        )
        await self.upsert_term(fao_local_term, upserted_terms=upserted_terms)

        fao_default_term = Term(
            uid=uuid.uuid4(),
            name="Default production FAO code",
            sub_class_of=fao_root_term.uid,
            xid="200000",
            data={},
            access_group_uid=uuid.UUID(fao_codes_access_group_uid),
        )
        await self.upsert_term(fao_default_term, upserted_terms=upserted_terms)

        fao_unknown_fish_term = Term(
            uid=uuid.uuid4(),
            name="Unknown fish production FAO code",
            sub_class_of=fao_root_term.uid,
            xid="300000",
            data={},
            access_group_uid=uuid.UUID(fao_codes_access_group_uid),
        )
        await self.upsert_term(fao_unknown_fish_term, upserted_terms=upserted_terms)

        fao_failed_term = Term(
            uid=uuid.uuid4(),
            name="Failed to estimate production FAO code",
            sub_class_of=fao_root_term.uid,
            xid="400000",
            data={},
            access_group_uid=uuid.UUID(fao_codes_access_group_uid),
        )
        await self.upsert_term(fao_failed_term, upserted_terms=upserted_terms)

        # import data from FoodEx2
        logger.info("start seeding glossary with FoodEx2...")

        # TODO (minor): maybe we need to create a separate term to be a parent for ALL FoodEx2 terms
        #  instead of each FoodEx2 category being a child of root glossary node on its own?

        terms_to_insert: Dict[str, Term] = {}

        with open(os.path.join(my_path, "..", "..", "seed_data", "TREE.txt")) as f:
            for line in f:
                chunks = re.findall("(\t*)(.*) \\[(.*)]", line)[0]
                num_tabs = len(chunks[0])
                name = chunks[1]
                xid = chunks[2]

                term = Term(
                    uid=uuid.uuid4(),  # explicitly create uuid, so that setting sub_class_of below works
                    xid=xid,
                    name=name,
                    sub_class_of=None,
                    data={},
                    access_group_uid=uuid.UUID(foodex2_access_group_uid),
                )

                if xid in terms_to_insert:
                    term = terms_to_insert[xid]

                lvl = num_tabs + 2

                # check if we are going one hierarchy level up or down:
                if len(glossary_path) < lvl:
                    # go deeper into a sublevel of the glossary hierarchy:
                    glossary_path.append(term)
                elif len(glossary_path) > lvl:
                    # go up in hierarchy by removing the previous added term and replacing the parent in the path:
                    while len(glossary_path) > lvl:
                        glossary_path.pop()
                    glossary_path[-1] = term
                else:
                    # stay at the same level by replacing the previous sibling with the new term:
                    glossary_path[-1] = term

                if xid not in terms_to_insert:
                    # The previous to the last item in the path is the parent of the new term:
                    term.sub_class_of = glossary_path[-2].uid
                    terms_to_insert[xid] = term

        terms_to_insert_lst = list(terms_to_insert.values())
        await self.upsert_many_terms(terms_to_insert_lst, upserted_terms=upserted_terms)

        # add some more Eaternity terms:
        production_environment = await self.get_term_by_xid_and_access_group_uid("Z0206", foodex2_access_group_uid)

        term = Term(
            xid="EOS_prod_standard",
            name="Standard",
            sub_class_of=production_environment.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        term = Term(
            xid="EOS_prod_greenhouse",
            name="Greenhouse",
            sub_class_of=production_environment.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        label_or_labeling_claim = await self.get_term_by_xid_and_access_group_uid("P0160", foodex2_access_group_uid)

        # Animal welfare certification
        # Animal welfare certified.
        term = Term(
            xid="EOS_animal_welfare_certified",
            name="Animal Welfare Certified",
            sub_class_of=label_or_labeling_claim.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        # Not certified.
        term = Term(
            xid="EOS_not_certified_for_animal_welfare",
            name="Not Certified For Animal Welfare",
            sub_class_of=label_or_labeling_claim.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        # Rainforest protection certification
        # Certified with protection of high conservation value areas.
        term = Term(
            xid="EOS_rainforest_conservation_certified",
            name="Rainforest Conservation Certified",
            sub_class_of=label_or_labeling_claim.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        # Certified without specific requirements.
        term = Term(
            xid="EOS_certified_rainforest_not_specified",
            name="Certified Rainforest Not Specified",
            sub_class_of=label_or_labeling_claim.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        # Not certified.
        term = Term(
            xid="EOS_not_certified_for_rainforest",
            name="Not Certified For Rainforest",
            sub_class_of=label_or_labeling_claim.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        term = Term(
            xid="EOS_data_error",
            name="data error",
            sub_class_of=basic_glossary_term.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        term = Term(
            xid="EOS_electricity",
            name="electricity",
            sub_class_of=None,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(term, upserted_terms=upserted_terms)

        # import sample nutrients
        logger.info("start seeding glossary with EuroFIR nutrients...")

        # root Nutrients Subdivision Term
        nutrients_subdivision_root_term = Term(
            uid=uuid.uuid4(),
            name="Nutrients Subdivision",
            sub_class_of=basic_glossary_term.uid,
            xid="Root_Nutrients_Subdivision",  # used later when importing nutrients from EDB
            data={},
            access_group_uid=uuid.UUID(subdivision_access_group_uid),
        )
        await self.upsert_term(nutrients_subdivision_root_term, upserted_terms=upserted_terms)

        logger.info("start seeding glossary with Eaternity nutrient name terms...")

        nutrient_names_subclass = Term(
            xid="EOS_nutrient_names",
            name="Nutrient names",
            sub_class_of=basic_glossary_term.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(nutrient_names_subclass, upserted_terms=upserted_terms)

        nutrients = [
            "energy",
            "fat",
            "saturated_fat",
            "carbohydrates",
            "sucrose",
            "protein",
            "sodium_chloride",
            "monounsaturated_fat",
            "polyunsaturated_fat",
            "cholesterol",
            "fibers",
            "water",
            "vitamine_a1",
            "vitamine_b1",
            "vitamine_b2",
            "vitamine_b6",
            "vitamine_b12",
            "vitamine_c",
            "vitamine_d",
            "vitamine_e",
            "vitamine_h",
            "vitamine_k",
            "beta_carotene",
            "niacin",
            "pantohen",
            "folic_acid",
            "sodium",
            "potassium",
            "chlorine",
            "calcium",
            "magnesium",
            "phosphorus",
            "iron",
            "zinc",
            "copper",
            "manganese",
            "flouride",
            "iodine",
            "purine",
            "uric_acid",
            "alcohol",
        ]

        nutrient_names = [
            Term(
                xid=f"EOS_{re.sub(' ', '_', nutr_name.lower())}",
                name=nutr_name.capitalize(),
                sub_class_of=nutrient_names_subclass.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            )
            for nutr_name in nutrients
        ]

        await self.upsert_many_terms(nutrient_names, upserted_terms=upserted_terms)

        # root measurement units terms
        logger.info("start seeding glossary with Eaternity units terms...")

        units_subclass = Term(
            xid="EOS_units",
            name="Measurement units",
            sub_class_of=basic_glossary_term.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(units_subclass, upserted_terms=upserted_terms)

        dimensionless_term = Term(
            xid="EOS_dimensionless",
            name="Dimensionless",
            sub_class_of=units_subclass.uid,
            data={"description": "Unit placeholder for dimensionless quantities, such as relative errors."},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(dimensionless_term, upserted_terms=upserted_terms)

        mass_term = Term(
            xid="EOS_units_mass",
            name="Mass units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(mass_term, upserted_terms=upserted_terms)

        time_term = Term(
            xid="EOS_units_time",
            name="Time units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(time_term, upserted_terms=upserted_terms)

        energy_term = Term(
            xid="EOS_units_energy",
            name="Energy units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(energy_term, upserted_terms=upserted_terms)

        volume_term = Term(
            xid="EOS_units_volume",
            name="Volume units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(volume_term, upserted_terms=upserted_terms)

        daily_food_unit_term = Term(
            xid="EOS_daily_food_unit",
            name="daily food unit",
            sub_class_of=units_subclass.uid,
            data={
                "description": "Daily food unit computed as ("
                "proteins in g / 50 + fats in g / 66 + energy in kJ / 6000 + water in g / 2500 + dry mass in g / 600"
                ") / 5"
            },
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(daily_food_unit_term, upserted_terms=upserted_terms)

        emission_term = Term(
            xid="EOS_units_emission",
            name="Emission units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(emission_term, upserted_terms=upserted_terms)

        electric_current_term = Term(
            xid="EOS_units_electric_current",
            name="Electric current units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(electric_current_term, upserted_terms=upserted_terms)

        length_term = Term(
            xid="EOS_units_length",
            name="Length units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(length_term, upserted_terms=upserted_terms)

        transportation_units_term = Term(
            xid="EOS_units_transportation",
            name="Transportation units",
            sub_class_of=units_subclass.uid,
            data={"Reference": "https://en.wikipedia.org/wiki/Units_of_measurement_in_transportation"},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(transportation_units_term, upserted_terms=upserted_terms)

        freezing_units_term = Term(
            xid="EOS_units_freezing",
            name="Freezing units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(freezing_units_term, upserted_terms=upserted_terms)

        impact_assessment_units_term = Term(
            xid="EOS_units_impact_assessment",
            name="Impact assessment units",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(impact_assessment_units_term, upserted_terms=upserted_terms)

        weight_conversion_term = Term(
            xid="EOS_weight_conversion",
            name="Weight Conversion",
            sub_class_of=units_subclass.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(weight_conversion_term, upserted_terms=upserted_terms)

        units = [
            # percentage
            Term(
                xid="EOS_percentage",
                name="Percentage",
                sub_class_of=units_subclass.uid,
                data={"alias": ["%", "percent"]},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # mass units
            Term(
                xid="EOS_microgram",
                name="Microgram",
                sub_class_of=mass_term.uid,
                data={
                    "mass-in-g": 1e-6,
                    "alias": [
                        "mcg",
                        "mug",
                        "μg",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_milligram",
                name="Milligram",
                sub_class_of=mass_term.uid,
                data={
                    "mass-in-g": 1e-3,
                    "alias": [
                        "mg",
                        "miligram",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_gram",
                name="Gram",
                sub_class_of=mass_term.uid,
                data={
                    "mass-in-g": 1,
                    "alias": [
                        "g",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_kilogram",
                name="Kilogram",
                sub_class_of=mass_term.uid,
                data={
                    "mass-in-g": 1e3,
                    "alias": [
                        "kg",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_ton",
                name="Ton",
                sub_class_of=mass_term.uid,
                data={"mass-in-g": 1e6},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # volume units
            Term(
                xid="EOS_drop",
                name="Drop",
                sub_class_of=volume_term.uid,
                data={"volume-in-ml": 1.0 / 30.0},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_cubic-meter",
                name="Cubic-meter",
                sub_class_of=volume_term.uid,
                data={"volume-in-ml": 1e6},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_pinch",
                name="Pinch",
                sub_class_of=volume_term.uid,
                data={"volume-in-ml": 0.3125},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_teaspoon",
                name="Teaspoon",
                sub_class_of=volume_term.uid,
                data={"volume-in-ml": 5},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_tablespoon",
                name="Tablespoon",
                sub_class_of=volume_term.uid,
                data={"volume-in-ml": 15},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_milliliter",
                name="Milliliter",
                sub_class_of=volume_term.uid,
                data={
                    "volume-in-ml": 1,
                    "alias": [
                        "ml",
                        "mililiter",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_centiliter",
                name="Centiliter",
                sub_class_of=volume_term.uid,
                data={
                    "volume-in-ml": 1e1,
                    "alias": [
                        "cl",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_shot",
                name="Shot",
                sub_class_of=volume_term.uid,
                data={"volume-in-ml": 25.0},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_deciliter",
                name="Deciliter",
                sub_class_of=volume_term.uid,
                data={
                    "volume-in-ml": 1e2,
                    "alias": [
                        "dl",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_cup",
                name="Cup",
                sub_class_of=volume_term.uid,
                data={"volume-in-ml": 2e2},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_liter",
                name="Liter",
                sub_class_of=volume_term.uid,
                data={
                    "volume-in-ml": 1e3,
                    "alias": [
                        "l",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_volume-percent",
                name="Volume percent",
                sub_class_of=volume_term.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # time units:
            Term(
                xid="EOS_year",
                name="Year",
                sub_class_of=time_term.uid,
                data={
                    "time-in-hour": 365 * 24,
                    "alias": ["y", "yr"],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_day",
                name="Day",
                sub_class_of=time_term.uid,
                data={
                    "time-in-hour": 24,
                    "alias": ["d"],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_hour",
                name="Hour",
                sub_class_of=time_term.uid,
                data={
                    "time-in-hour": 1,
                    "alias": ["hr"],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_minute",
                name="Minute",
                sub_class_of=time_term.uid,
                data={
                    "time-in-hour": 1 / 60,
                    "alias": ["min"],  # m conflicts with meter?
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_second",
                name="Second",
                sub_class_of=time_term.uid,
                data={
                    "time-in-hour": 1 / 3600,
                    "alias": ["sec", "s"],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # electric current units : FIXME Watt hour should be a unit of energy.
            Term(
                xid="EOS_watt-hour",
                name="Watt hour",
                sub_class_of=electric_current_term.uid,
                data={
                    "current-in-wh": 1,
                    "alias": [
                        "Wh",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_kilowatt-hour",
                name="Kilowatt hour",
                sub_class_of=electric_current_term.uid,
                data={
                    "current-in-wh": 1e3,
                    "alias": [
                        "kWh",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # length units
            Term(
                xid="EOS_millimeter",
                name="Millimeter",
                sub_class_of=length_term.uid,
                data={
                    "length-in-m": 1e-3,
                    "alias": [
                        "mm",
                        "milimeter",
                        "millimetre",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_centimeter",
                name="Centimeter",
                sub_class_of=length_term.uid,
                data={
                    "length-in-m": 1e-2,
                    "alias": [
                        "cm",
                        "centimetre",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_decimeter",
                name="Decimeter",
                sub_class_of=length_term.uid,
                data={
                    "length-in-m": 1e-1,
                    "alias": [
                        "dm",
                        "decimetre",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_meter",
                name="Meter",
                sub_class_of=length_term.uid,
                data={
                    "length-in-m": 1,
                    "alias": [
                        "m",
                        "metre",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_kilometer",
                name="Kilometer",
                sub_class_of=length_term.uid,
                data={
                    "length-in-m": 1e3,
                    "alias": [
                        "km",
                        "kilometre",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # energy units
            Term(
                xid="EOS_kilojoule",
                name="Kilojoule",
                sub_class_of=energy_term.uid,
                data={"energy-in-kj": 1, "alias": ["kj"]},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_kilocalorie",
                name="Kilocalorie",
                sub_class_of=energy_term.uid,
                data={"energy-in-kj": 4.184, "alias": ["kcal"]},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # transportation units
            Term(
                xid="EOS_ton-kilometer",
                name="Ton-kilometer",
                sub_class_of=transportation_units_term.uid,
                data={
                    "payload-distance-in-ton-km": 1,
                    "alias": [
                        "tonne-kilometre",
                        "ton kilometre",
                        "ton-kilometre",
                        "kilometre-tonne",
                        "tonkm",
                        "tkm",
                        "kmt",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_kilogram-kilometer",
                name="Kilogram-kilometer",
                sub_class_of=transportation_units_term.uid,
                data={
                    "payload-distance-in-ton-km": 1e-3,
                    "alias": [
                        "kilogram-kilometre",
                        "kilometre-kilogram",
                        "kg⋅km",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # freezing units
            Term(
                xid="EOS_kilogram-day",
                name="Kilogram-day",
                sub_class_of=freezing_units_term.uid,
                data={
                    "freezing-in-kg-day": 1,
                    "alias": [
                        "kg*day",
                        "kg-day",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_kilogram-hour",
                name="Kilogram-hour",
                sub_class_of=freezing_units_term.uid,
                data={
                    "freezing-in-kg-day": 1 / 24,
                    "alias": [
                        "kg*hour",
                        "kg-hour",
                    ],
                },
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # emission units (used by Brightway emission nodes)
            Term(
                xid="EOS_square-meter-year",
                name="Square-meter-year",
                sub_class_of=emission_term.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_cubic-meter-year",
                name="Cubic-meter-year",
                sub_class_of=emission_term.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_kilo-becquerel",
                name="Kilo-Becquerel",
                sub_class_of=emission_term.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_square-meter",
                name="Square-meter",
                sub_class_of=emission_term.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_megajoule",
                name="Megajoule",
                sub_class_of=emission_term.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # production amount unit term (used for impact_assessment_supply_for_root).
            Term(
                xid="EOS_production_amount",
                name="Production_amount",
                sub_class_of=units_subclass.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            # impact assessment unit terms: more are imported in bw_importer:
            Term(
                xid="EOS_kg-co2-eq",
                name="kg CO2-Eq",
                sub_class_of=impact_assessment_units_term.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_dalys",
                name="DALYs",
                sub_class_of=impact_assessment_units_term.uid,
                data={},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
            Term(
                xid="EOS_piece",
                name="piece",
                sub_class_of=units_subclass.uid,
                data={"alias": ["pieces", "pcs", "pcs.", "st.", "st", "stück", "stuck"]},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            ),
        ]
        await self.upsert_many_terms(units, upserted_terms=upserted_terms)

        # root perishability terms
        logger.info("start seeding glossary with Eaternity perishability terms...")

        perishability_subclass_term = Term(
            xid="EOS_Perishability",
            name="Perishability",
            sub_class_of=basic_glossary_term.uid,
            data={},
            access_group_uid=uuid.UUID(perishability_access_group_uid),
        )
        await self.upsert_term(perishability_subclass_term, upserted_terms=upserted_terms)

        # TODO: decide on term XID names
        perishability_terms = [
            Term(
                xid="EOS_STABLE",
                name="Shelf-stable",
                sub_class_of=perishability_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(perishability_access_group_uid),
            ),
            Term(
                xid="EOS_PERISHABLE",
                name="Perishable",
                sub_class_of=perishability_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(perishability_access_group_uid),
            ),
            Term(
                xid="EOS_HIGH-PERISHABLE",
                name="Highly perishable",
                sub_class_of=perishability_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(perishability_access_group_uid),
            ),
        ]
        await self.upsert_many_terms(perishability_terms, upserted_terms=upserted_terms)

        # root combined product terms
        logger.info("start seeding glossary with Eaternity combined-product terms...")

        combined_product_mono_product_subclass_term = Term(
            xid="EOS_Combined_Product_Mono_Product",
            name="Combined product and mono product",
            sub_class_of=basic_glossary_term.uid,
            data={},
            access_group_uid=uuid.UUID(node_tag_access_group_uid),
        )
        await self.upsert_term(combined_product_mono_product_subclass_term, upserted_terms=upserted_terms)

        combined_product_mono_product_terms = [
            Term(
                xid="EOS_COMBINED_PRODUCT",
                name="Combined product",
                sub_class_of=combined_product_mono_product_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(node_tag_access_group_uid),
            ),
            Term(
                xid="EOS_MONO_PRODUCT",
                name="Mono product",
                sub_class_of=combined_product_mono_product_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(node_tag_access_group_uid),
            ),
        ]
        await self.upsert_many_terms(combined_product_mono_product_terms, upserted_terms=upserted_terms)

        logger.info("start seeding glossary with Eaternity scarce water consumption term...")
        scarce_water_consumption_term = Term(
            xid="EOS_Scarce_Water_Consumption",
            name="Scarce Water Consumption",
            sub_class_of=basic_glossary_term.uid,
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        await self.upsert_term(scarce_water_consumption_term, upserted_terms=upserted_terms)

        # root transportation terms
        logger.info("start seeding glossary with Eaternity transportation terms...")

        transport_subclass_term = Term(
            xid="EOS_Transportation",
            name="Transportation",
            sub_class_of=basic_glossary_term.uid,
            data={},
            access_group_uid=uuid.UUID(transport_access_group_uid),
        )
        await self.upsert_term(transport_subclass_term, upserted_terms=upserted_terms)

        transport_terms = [
            Term(
                xid="EOS_AIR",
                name="Air transport",
                sub_class_of=transport_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(transport_access_group_uid),
            ),
            Term(
                xid="EOS_GROUND",
                name="Truck transport",
                sub_class_of=transport_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(transport_access_group_uid),
            ),
            Term(
                xid="EOS_SEA",
                name="Sea transport",
                sub_class_of=transport_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(transport_access_group_uid),
            ),
            Term(
                xid="EOS_TRAIN",
                name="Train transport",
                sub_class_of=transport_subclass_term.uid,
                data={},
                access_group_uid=uuid.UUID(transport_access_group_uid),
            ),
        ]
        await self.upsert_many_terms(transport_terms, upserted_terms=upserted_terms)

        # root food categories Term
        food_category_terms_to_insert: list[Term] = []
        food_categories_root_term = Term(
            uid=uuid.uuid4(),
            name="Food Categories",
            sub_class_of=basic_glossary_term.uid,
            xid="Root_Food_Categories",
            data={},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        food_category_terms_to_insert.append(food_categories_root_term)

        logger.info("start seeding glossary with Eaternity food category terms...")
        # Eaternity food categories Term
        eaternity_food_categories = Term(
            uid=uuid.uuid4(),
            xid="EOS_Eaternity-food-categories",
            name="Eaternity food categories",
            sub_class_of=food_categories_root_term.uid,
            data={"description": "Food categories used for Eaternity's PDF report generation."},
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        food_category_terms_to_insert.append(eaternity_food_categories)

        # Eaternity food major categories Term
        names_en = ["Plant based products", "Animal products", "Other"]
        names_de = ["Pflanzliche produkte", "Tierische produkte", "Anderes"]
        major_category_terms: dict[str, Term] = {}
        for name_en, name_de in zip(names_en, names_de):
            major_category_term_xid = f"EOS_{re.sub(' ','-',name_en)}"
            major_category_terms[major_category_term_xid] = Term(
                uid=uuid.uuid4(),
                xid=f"EOS_{re.sub(' ','-', name_en)}",
                name=name_en,
                sub_class_of=eaternity_food_categories.uid,
                data={"alternative_names": {"name": name_de, "lang": "de"}},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            )
            food_category_terms_to_insert.append(major_category_terms[major_category_term_xid])

        # Eaternity food animal subcategories Term
        names_en = ["Meat products", "Eggs", "Dairy", "Honey", "Other animal products"]
        names_de = ["Fleisch", "Eier", "Milchprodukte", "Honig", "Andere tierische Produkte"]
        animal_subcategory_terms: dict[str, Term] = {}
        for name_en, name_de in zip(names_en, names_de):
            animal_subcategory_terms_xid = f"EOS_{re.sub(' ','-',name_en)}"
            animal_subcategory_terms[animal_subcategory_terms_xid] = Term(
                uid=uuid.uuid4(),
                xid=f"EOS_{re.sub(' ','-', name_en)}",
                name=name_en,
                sub_class_of=major_category_terms["EOS_Animal-products"].uid,
                data={"alternative_names": {"name": name_de, "lang": "de"}},
                access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
            )
            food_category_terms_to_insert.append(animal_subcategory_terms[animal_subcategory_terms_xid])

        # Eaternity plant products categories
        names_en = [
            "Grains",
            "Rice",
            "Plant based oil",
            "Legumes",
            "Fruits",
            "Vegetables",
            "Mushrooms",
            "Nuts and seeds",
            "Sugar",
            "Salt",
            "Spices and condiments",
            "Preservatives and Baking additives",
            "Tea leaves",
            "Juice",
            "Coffee beans",
            "Alcohol",
            "Other plant-based",
        ]
        names_de = [
            "Getreide",
            "Reis",
            "Pflanzliches Öl",
            "Hülsenfrüchte",
            "Früchte",
            "Gemüse",
            "Pilze",
            "Nüsse & Samen",
            "Zucker",
            "Salz",
            "Gewürze & Würzmittel",
            "Konservierungs- und Backmittel",
            "Teeblätter",
            "Fruchtsaft",
            "Kaffeebohnen",
            "Alkohol",
            "Andere pflanzliche Produckte",
        ]
        for name_en, name_de in zip(names_en, names_de):
            food_category_terms_to_insert.append(
                Term(
                    uid=uuid.uuid4(),
                    xid=f"EOS_{re.sub(' ','-', name_en)}",
                    name=name_en,
                    sub_class_of=major_category_terms["EOS_Plant-based-products"].uid,
                    data={"alternative_names": {"name": name_de, "lang": "de"}},
                    access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
                )
            )

        # Eaternity meat categories
        names_en = ["Beef and veal", "Pork", "Poultry", "Fish and seafood"]
        names_de = ["Rind und Kalb", "Schwein", "Geflügel", "Fisch & Meeresfrüchte"]
        for name_en, name_de in zip(names_en, names_de):
            food_category_terms_to_insert.append(
                Term(
                    uid=uuid.uuid4(),
                    xid=f"EOS_{re.sub(' ','-', name_en)}",
                    name=name_en,
                    sub_class_of=animal_subcategory_terms["EOS_Meat-products"].uid,
                    data={"alternative_names": {"name": name_de, "lang": "de"}},
                    access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
                )
            )

        # Eaternity milk product categories
        names_en = ["Milk", "Butter", "Ripened cheese", "Fresh cheese", "Cream", "Curd and yoghurt"]
        names_de = ["Milch", "Butter", "Käse gereift", "Käse weich und frisch", "Rahm", "Quark & Joghurt"]
        for name_en, name_de in zip(names_en, names_de):
            food_category_terms_to_insert.append(
                Term(
                    uid=uuid.uuid4(),
                    xid=f"EOS_{re.sub(' ','-', name_en)}",
                    name=name_en,
                    sub_class_of=animal_subcategory_terms["EOS_Dairy"].uid,
                    data={"alternative_names": {"name": name_de, "lang": "de"}},
                    access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
                )
            )

        logger.info("start seeding glossary with global burden of disease (GBD) dietary risk category terms...")
        # Global burden of disease (GBD) project dietary risk food categories Term
        gbd_food_categories = Term(
            uid=uuid.uuid4(),
            xid="EOS_GBD-Risk-Food-Categories",
            name="GBD Risk Food Categories",
            sub_class_of=food_categories_root_term.uid,
            data={
                "description": "Dietary risk factor food categories in Global burden of disease (GBD) study.",
                "references": ["Forouzanfar, MH., et al., The Lancet 2015;388(10053):1659-1724"],
            },
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        food_category_terms_to_insert.append(gbd_food_categories)

        gbd_positive_categories = Term(
            uid=uuid.uuid4(),
            xid="EOS_GBD-Positive",
            name="GBD Positive Food Categories",
            sub_class_of=gbd_food_categories.uid,
            data={
                "description": "Positive dietary risk factor food categories in Global burden of disease (GBD) study.",
            },
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        food_category_terms_to_insert.append(gbd_positive_categories)

        gbd_negative_categories = Term(
            uid=uuid.uuid4(),
            xid="EOS_GBD-Negative",
            name="GBD Negative Food Categories",
            sub_class_of=gbd_food_categories.uid,
            data={
                "description": "Negative dietary risk factor food categories in Global burden of disease (GBD) study.",
            },
            access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
        )
        food_category_terms_to_insert.append(gbd_negative_categories)

        # Current DALY is for Germany, and legacy DALY is for Switzerland.
        # TODO: Implement country dependent DALYs.
        gbd_dietary_risk_factors_dict = {
            "Diet low in fruits": GbdData(
                description="Average daily consumption of fruits (fresh, frozen, cooked, canned, or dried, "
                "excluding fruit juices and salted or pickled fruits)",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=61.24,
                    legacy_daly=39.82,
                    tmrel_lower=200,
                    tmrel_upper=300,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet low in whole grains": GbdData(
                description="Average daily consumption of whole grains (bran, germ, "
                "and endosperm in their natural proportion) from breakfast) "
                "cereals, bread, rice, pasta, biscuits, muffins, tortillas, "
                "pancakes, and other sources",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=82.86,
                    legacy_daly=94.77,
                    tmrel_lower=100,
                    tmrel_upper=150,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet low in vegetables": GbdData(
                description="Average daily consumption of vegetables (fresh, frozen, "
                "cooked, canned or dried vegetables including legumes but "
                "excluding salted or pickled vegetables, juices, nuts and "
                "seeds, and starchy vegetables such as potatoes or corn)",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=30.57,
                    legacy_daly=20.18,
                    tmrel_lower=340,
                    tmrel_upper=500,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet low in nuts and seeds": GbdData(
                description="Average daily consumption of nut and seed foods",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=13.98,
                    legacy_daly=5.99,
                    tmrel_lower=16,
                    tmrel_upper=25,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet low in omega-3": GbdData(
                description="Average daily intake of eicosapentaenoic acid and docosahexaenoic acid",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=23.95,
                    legacy_daly=10.57,
                    tmrel_lower=200,
                    tmrel_upper=300,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet low in fibre": GbdData(
                description="Average daily intake of fibre from all sources, including fruits, vegetables, "
                "grains, legumes, and pulses",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=39.57,
                    legacy_daly=36.53,
                    tmrel_lower=19,
                    tmrel_upper=28,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet low in poly unsaturated fatty acids (PUFA)": GbdData(
                description="Average daily intake of omega-6 fatty acids from all "
                "sources, mainly liquid vegetable oils, including "
                "soybean oil, corn oil, and safflower oil",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=25.26,
                    legacy_daly=16.51,
                    tmrel_lower=9,
                    tmrel_upper=13,
                    tmrel_unit=TmrelUnitEnum.percent,
                    tmrel_normalization=TmrelNormalizationEnum.per_total_energy,
                ),
            ),
            "Diet low in calcium": GbdData(
                description="Average daily intake of calcium from all sources, " "including milk, yogurt, and cheese",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=2.38,
                    legacy_daly=1.30,
                    tmrel_lower=1,
                    tmrel_upper=1.5,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet low in milk": GbdData(
                description="Average daily consumption of milk, including non-fat, lowfat, and full-fat milk, "
                "excluding soy milk and other plant derivatives",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=9.42,
                    legacy_daly=5.23,
                    tmrel_lower=350,
                    tmrel_upper=520,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet high in sodium (salt)": GbdData(
                description="24 h urinary sodium measured in g per day",
                health_effect=Effect.negative,
                gbd_numbers=GbdNumbers(
                    current_daly=29.43,
                    legacy_daly=13.14,
                    tmrel_lower=1,
                    tmrel_upper=5,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet high in processed meat": GbdData(
                description="Average daily consumption of meat preserved by smoking,"
                " curing, salting, or addition of chemical preservatives",
                health_effect=Effect.negative,
                gbd_numbers=GbdNumbers(
                    current_daly=96.94,
                    legacy_daly=82.12,
                    tmrel_lower=0,
                    tmrel_upper=4,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet high in trans fats": GbdData(
                description="Average daily intake of trans fat from all sources, mainly partially "
                "hydrogenated vegetable oils and ruminant products",
                health_effect=Effect.negative,
                gbd_numbers=GbdNumbers(
                    current_daly=5.01,
                    legacy_daly=1.51,
                    tmrel_lower=0,
                    tmrel_upper=1,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet high in red meat": GbdData(
                description="Average daily consumption of red meat (beef, pork, lamb, and goat but excluding "
                "poultry, fish, eggs, and all processed meats",
                health_effect=Effect.negative,
                gbd_numbers=GbdNumbers(
                    current_daly=74.25,
                    legacy_daly=69.32,
                    tmrel_lower=18,
                    tmrel_upper=27,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet high in sweetened beverages": GbdData(
                description="Average daily consumption of beverages with ≥50kcal per 226·8 g "
                "serving, including carbonated beverages, sodas, energy drinks, and "
                "fruit drinks, but excluding 100% fruit and vegetable juices",
                health_effect=Effect.negative,
                gbd_numbers=GbdNumbers(
                    current_daly=31.18,
                    legacy_daly=35.27,
                    tmrel_lower=0,
                    tmrel_upper=5,
                    tmrel_unit=TmrelUnitEnum.g,
                    tmrel_normalization=TmrelNormalizationEnum.per_dfu,
                ),
            ),
            "Diet high in energy": GbdData(  # 88.66 corresponds to 531.95*0.5/3
                description="",  # TODO: what should be the legacy value here?
                health_effect=Effect.negative,
                gbd_numbers=GbdNumbers(
                    current_daly=88.66,
                    legacy_daly=88.66,
                    tmrel_lower=650,
                    tmrel_upper=1250,
                    tmrel_unit=TmrelUnitEnum.kcal,
                    tmrel_normalization=TmrelNormalizationEnum.per_self,
                ),
            ),
            "Diet low in fat": GbdData(
                description="",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=88.66,
                    legacy_daly=88.66,
                    tmrel_lower=0.05,
                    tmrel_upper=0.275,
                    tmrel_unit=TmrelUnitEnum.percent,
                    tmrel_normalization=TmrelNormalizationEnum.per_total_energy,
                ),
            ),
            "Diet high in fat": GbdData(
                description="",
                health_effect=Effect.negative,
                gbd_numbers=GbdNumbers(
                    current_daly=88.66,
                    legacy_daly=88.66,
                    tmrel_lower=0.275,
                    tmrel_upper=0.5,
                    tmrel_unit=TmrelUnitEnum.percent,
                    tmrel_normalization=TmrelNormalizationEnum.per_total_energy,
                ),
            ),
            "Diet low in protein": GbdData(
                description="",
                health_effect=Effect.positive,
                gbd_numbers=GbdNumbers(
                    current_daly=88.66,
                    legacy_daly=88.66,
                    tmrel_lower=-0.15,
                    tmrel_upper=0.225,
                    tmrel_unit=TmrelUnitEnum.percent,
                    tmrel_normalization=TmrelNormalizationEnum.per_total_energy,
                ),
            ),
            "Diet high in protein": GbdData(
                description="",
                health_effect=Effect.negative,
                gbd_numbers=GbdNumbers(
                    current_daly=88.66,
                    legacy_daly=88.66,
                    tmrel_lower=0.225,
                    tmrel_upper=0.6,
                    tmrel_unit=TmrelUnitEnum.percent,
                    tmrel_normalization=TmrelNormalizationEnum.per_total_energy,
                ),
            ),
        }

        for gbd_name, gbd_data in gbd_dietary_risk_factors_dict.items():
            if gbd_data.health_effect == Effect.positive:
                sub_class_of = gbd_positive_categories
            elif gbd_data.health_effect == Effect.negative:
                sub_class_of = gbd_negative_categories
            else:
                sub_class_of = gbd_food_categories

            food_category_terms_to_insert.append(
                Term(
                    uid=uuid.uuid4(),
                    xid=f"EOS_{re.sub(' ','-', gbd_name)}",
                    name=gbd_name,
                    sub_class_of=sub_class_of.uid,
                    data={
                        "description": gbd_data.description,
                        "legacy": {
                            "amount": gbd_data.gbd_numbers.legacy_daly,
                            "unit": "DALYs per 100,000",
                        },
                        "current": {"amount": gbd_data.gbd_numbers.current_daly, "unit": "DALYs per 100,000"},
                        "tmrel": {
                            "lower": gbd_data.gbd_numbers.tmrel_lower,
                            "upper": gbd_data.gbd_numbers.tmrel_upper,
                            "unit": gbd_data.gbd_numbers.tmrel_unit.value,
                            "tmrel_normalization": gbd_data.gbd_numbers.tmrel_normalization,
                        },
                    },
                    access_group_uid=uuid.UUID(default_eaternity_access_group_uid),
                )
            )

        await self.upsert_many_terms(food_category_terms_to_insert, upserted_terms=upserted_terms)

        logger.info(f"seeded glossary, took {time.time() - start_time:.2f}s")

        return (
            fao_root_term,
            water_scarcity_root_term,
            critical_product_content,
            weight_conversion_term,
            gadm_root_term,
            nutrients_root_term,
            impact_assessment_root_term,
        )

    async def seed_sample_data(
        self,
        fao_codes_access_group_uid: str,
        eurofir_access_group_uid: str,
        gadm_access_group_uid: str,
        foodex2_access_group_uid: str,
        impact_assessment_access_group_uid: str,
        fao_root_term: Term,
        water_scarcity_root_term: Term,
        critical_product_content: Term,
        weight_conversion_term: Term,
        gadm_root_term: Term,
        nutrients_root_term: Term,
        impact_assessment_root_term: Term,
        upserted_terms: Optional[list[Term]] = None,
    ) -> None:
        """Seed sample data into the database."""
        start_time = time.time()

        logger.info("start seeding glossary with sample data...")
        my_path = os.path.abspath(os.path.dirname(__file__))

        product_type_term = await self.get_term_by_xid_and_access_group_uid("A0361", foodex2_access_group_uid)
        await self.upsert_term(
            Term(
                xid="EAT-0002",
                name="NON-FOOD",
                sub_class_of=product_type_term.uid,
                data={},
                access_group_uid=uuid.UUID(foodex2_access_group_uid),
            )
        )

        # nutrient file 733 has been edited (swapped g in water to mg) for one of unit tests
        for nutrient_file in [
            "131_Carrot__raw-nutr.json",
            "132_Carrot__dried-nutr.json",
            "491_Tomato__raw-nutr.json",
            "110_Onion__raw-nutr.json",
            "508_Potato__peeled__raw-nutr.json",
            "732_Cocoa_powder_partially_defatted-nutr.json",
            "733_Cocoa_powder_low_fat-nutr.json",
            "680_Eggs__chicken__pasteurized-nutr.json",
            "244_Chicken__whole__with_skin__raw-nutr.json",
            "528_Additives-nutr.json",
        ]:
            with open(os.path.join(my_path, "..", "..", "seed_data", "nutrs", nutrient_file)) as f:
                nutrient_data = json.load(f)
                nutrient_term = Term(
                    xid=nutrient_data["id"],
                    name=nutrient_data["name"],
                    sub_class_of=nutrients_root_term.uid,
                    data={
                        "comment": nutrient_data.get("comment", ""),
                        "country": nutrient_data.get("country", ""),
                        "original-id": nutrient_data.get("original-id", ""),
                        "nutr-vals": nutrient_data.get("nutr-vals", {}),
                        "format": "euroFIR",
                    },
                    access_group_uid=uuid.UUID(eurofir_access_group_uid),
                )
                await self.upsert_term(nutrient_term, upserted_terms=upserted_terms)

        # import FAO codes
        logger.info("start seeding glossary with FAO codes...")

        fao_carrot_term = Term(
            xid="426",
            name="Carrots and turnips",
            sub_class_of=fao_root_term.uid,
            data={
                "definition": "Daucus carota. Trade data may include turnips (Brassica rapa var. rapifera).",
            },
            access_group_uid=uuid.UUID(fao_codes_access_group_uid),
        )
        await self.upsert_term(fao_carrot_term, upserted_terms=upserted_terms)

        carrot_water_scarcity = Term(
            xid="WS_sample_1",
            name="Sample carrot",
            sub_class_of=water_scarcity_root_term.uid,
            data={
                "CH": 0.0000293,
                "DK": 0.010001893,
                "ES": 0.021405036,
                "IT": 0.008656551,
                "NL": 0.004257543,
                "PT": 0.034059635,
                "median": 0.009329222,
            },
            access_group_uid=water_scarcity_root_term.access_group_uid,
        )
        await self.upsert_term(carrot_water_scarcity, upserted_terms=upserted_terms)

        potato_water_scarcity = Term(
            xid="WS_sample_2",
            name="Sample potato",
            sub_class_of=water_scarcity_root_term.uid,
            data={"CH": 0.0000374, "median": 0.0000374},
            access_group_uid=water_scarcity_root_term.access_group_uid,
        )
        await self.upsert_term(potato_water_scarcity, upserted_terms=upserted_terms)

        tomato_water_scarcity = Term(
            xid="WS_sample_3",
            name="Sample tomato",
            sub_class_of=water_scarcity_root_term.uid,
            data={"ES": 0.009627709, "median": 0.009627709},
            access_group_uid=water_scarcity_root_term.access_group_uid,
        )
        await self.upsert_term(tomato_water_scarcity, upserted_terms=upserted_terms)

        onion_water_scarcity = Term(
            xid="WS_sample_4",
            name="Sample onion",
            sub_class_of=water_scarcity_root_term.uid,
            data={"FR": 0.002498076, "median": 0.002498076},
            access_group_uid=water_scarcity_root_term.access_group_uid,
        )
        await self.upsert_term(onion_water_scarcity, upserted_terms=upserted_terms)

        fao_tomato_term = Term(
            xid="388",
            name="Tomatoes",
            sub_class_of=fao_root_term.uid,
            data={},
            access_group_uid=uuid.UUID(fao_codes_access_group_uid),
        )
        await self.upsert_term(fao_tomato_term, upserted_terms=upserted_terms)

        # These sample values for critical product content are not accurate.
        carrot_critical_production_content = Term(
            xid="CriticalProduct_sample_1",
            name="Sample carrot",
            sub_class_of=critical_product_content.uid,
            data={
                "soy": 10,
                "palm-oil": 25,
            },
            access_group_uid=critical_product_content.access_group_uid,
        )
        await self.upsert_term(carrot_critical_production_content, upserted_terms=upserted_terms)

        onion_critical_production_content = Term(
            xid="CriticalProduct_sample_2",
            name="Sample onion",
            sub_class_of=critical_product_content.uid,
            data={
                "soy": 30,
                "palm-oil": 10,
            },
            access_group_uid=critical_product_content.access_group_uid,
        )
        await self.upsert_term(onion_critical_production_content, upserted_terms=upserted_terms)

        # seed weight conversion data.
        carrot_weight_conversion_term = Term(
            xid="Weight_conversion_carrot",
            name="Carrot weight conversion",
            sub_class_of=weight_conversion_term.uid,
            data={"unit_weight": 62.1},
            access_group_uid=weight_conversion_term.access_group_uid,
        )
        await self.upsert_term(carrot_weight_conversion_term, upserted_terms=upserted_terms)

        tomato_weight_conversion_term = Term(
            xid="Weight_conversion_tomato",
            name="Tomato weight conversion",
            sub_class_of=weight_conversion_term.uid,
            data={"unit_weight": 155.1},
            access_group_uid=weight_conversion_term.access_group_uid,
        )
        await self.upsert_term(tomato_weight_conversion_term, upserted_terms=upserted_terms)

        onion_weight_conversion_term = Term(
            xid="Weight_conversion_onion",
            name="Onion weight conversion",
            sub_class_of=weight_conversion_term.uid,
            data={"unit_weight": 82.8},
            access_group_uid=weight_conversion_term.access_group_uid,
        )
        await self.upsert_term(onion_weight_conversion_term, upserted_terms=upserted_terms)

        apple_juice_weight_conversion_term = Term(
            xid="Weight_conversion_apple_juice",
            name="Apple juice weight conversion",
            sub_class_of=weight_conversion_term.uid,
            data={"density": 1.04},
            access_group_uid=weight_conversion_term.access_group_uid,
        )
        await self.upsert_term(apple_juice_weight_conversion_term, upserted_terms=upserted_terms)

        # seed sample GADM terms
        gadm_switzerland_term = Term(
            xid="CHE",
            name="Switzerland",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 41235436471.76332, "centroid_lat": 46.80098660274844, "centroid_lon": 8.229784569419007},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_switzerland_term, upserted_terms=upserted_terms)

        gadm_zurich_canton_term = Term(
            xid="CHE.26_1",
            name="Zürich",
            sub_class_of=gadm_switzerland_term.uid,
            data={"area_m2": 1737955956.157701, "centroid_lat": 47.417407459757314, "centroid_lon": 8.655987523987276},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_zurich_canton_term, upserted_terms=upserted_terms)

        gadm_zurich_term = Term(
            xid="CHE.26.13_1",
            name="Zürich",
            sub_class_of=gadm_zurich_canton_term.uid,
            data={"area_m2": 91996005.96941169, "centroid_lat": 47.38631622245828, "centroid_lon": 8.533227779974764},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_zurich_term, upserted_terms=upserted_terms)

        gadm_hochschulen_term = Term(
            xid="CHE.26.13.13_1",
            name="Hochschulen",
            sub_class_of=gadm_zurich_term.uid,
            data={"area_m2": 1046726.994608853, "centroid_lat": 47.37833023150006, "centroid_lon": 8.537353540608791},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_hochschulen_term, upserted_terms=upserted_terms)

        gadm_rathouse_term = Term(
            xid="CHE.26.13.21_1",
            name="Rathouse",
            sub_class_of=gadm_zurich_term.uid,
            data={"area_m2": 610196.0560273826, "centroid_lat": 47.37669998926548, "centroid_lon": 8.544405748321738},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_rathouse_term, upserted_terms=upserted_terms)

        gadm_unterstrass_term = Term(
            xid="CHE.26.13.27_1",
            name="Unterstrass",
            sub_class_of=gadm_zurich_term.uid,
            data={"area_m2": 2526744.19319468, "centroid_lat": 47.40089587073444, "centroid_lon": 8.540692919293951},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_unterstrass_term, upserted_terms=upserted_terms)

        gadm_spain_term = Term(
            xid="ESP",
            name="Spain",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 506044320524.19244, "centroid_lat": 40.22689843187332, "centroid_lon": -3.647061329429458},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_spain_term, upserted_terms=upserted_terms)

        gadm_italy_term = Term(
            xid="ITA",
            name="Italy",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 300733693968.72656, "centroid_lat": 42.78942391508048, "centroid_lon": 12.078197343722202},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_italy_term, upserted_terms=upserted_terms)

        gadm_denmark_term = Term(
            xid="DNK",
            name="Denmark",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 43144848690.88687, "centroid_lat": 55.95776830227537, "centroid_lon": 10.055912629646846},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_denmark_term, upserted_terms=upserted_terms)

        gadm_netherlands_term = Term(
            xid="NLD",
            name="Netherlands",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 37686935422.50127, "centroid_lat": 52.26301804806406, "centroid_lon": 5.5736507873325305},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_netherlands_term, upserted_terms=upserted_terms)

        gadm_portugal_term = Term(
            xid="PRT",
            name="Portugal",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 91878032767.60814, "centroid_lat": 39.596548132967335, "centroid_lon": -8.514975675472721},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_portugal_term, upserted_terms=upserted_terms)

        gadm_france_term = Term(
            xid="FRA",
            name="France",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 549516509835.94806, "centroid_lat": 46.55891399068149, "centroid_lon": 2.553552057064248},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_france_term, upserted_terms=upserted_terms)

        gadm_germany_term = Term(
            xid="DEU",
            name="Germany",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 357823908549.52466, "centroid_lat": 51.10989550925564, "centroid_lon": 10.394235886469259},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_germany_term, upserted_terms=upserted_terms)

        gadm_austria_term = Term(
            xid="AUT",
            name="Austria",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 83946895594.08253, "centroid_lat": 47.58757184862118, "centroid_lon": 14.142130592440134},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_austria_term, upserted_terms=upserted_terms)

        gadm_usa_term = Term(
            xid="USA",
            name="United States",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 9472888251768.996, "centroid_lat": 45.67643891453739, "centroid_lon": -112.4709372360601},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_usa_term, upserted_terms=upserted_terms)

        gadm_brazil_term = Term(
            xid="BRA",
            name="Brazil",
            sub_class_of=gadm_root_term.uid,
            data={
                "area_m2": 8509083330357.114,
                "centroid_lat": -10.779810978395716,
                "centroid_lon": -53.07028364723311,
            },
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_brazil_term, upserted_terms=upserted_terms)

        gadm_uk_term = Term(
            xid="GBR",
            name="United Kingdom",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 245293955910.1076, "centroid_lat": 54.31441855600016, "centroid_lon": -1.9180557014999238},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_uk_term, upserted_terms=upserted_terms)

        gadm_belgium_term = Term(
            xid="BEL",
            name="Belgium",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 245293955910.1076, "centroid_lat": 54.31441855600016, "centroid_lon": -1.9180557014999238},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_belgium_term, upserted_terms=upserted_terms)

        gadm_morocco_term = Term(
            xid="MAR",
            name="Morocco",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 413477272822.8238, "centroid_lat": 31.84656177197235, "centroid_lon": -6.276109160009557},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_morocco_term, upserted_terms=upserted_terms)
        gadm_poland_term = Term(
            xid="POL",
            name="Poland",
            sub_class_of=gadm_root_term.uid,
            data={"area_m2": 312400071470.1772, "centroid_lat": 52.12953037565219, "centroid_lon": 19.393496386869927},
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_poland_term, upserted_terms=upserted_terms)

        gadm_columbia_term = Term(
            xid="COL",
            name="Colombia",
            sub_class_of=gadm_root_term.uid,
            data={
                "area_m2": 1137241296633.9578,
                "centroid_lat": 3.8983735829331545,
                "centroid_lon": -73.07848023586101,
            },
            access_group_uid=uuid.UUID(gadm_access_group_uid),
        )
        await self.upsert_term(gadm_columbia_term, upserted_terms=upserted_terms)

        ipcc_2013_gwp_100a_term = Term(
            uid=uuid.uuid4(),
            xid=IPCC_2013_GWP_100_XID,
            name=IPCC_2013_GWP_100,
            sub_class_of=impact_assessment_root_term.uid,
            access_group_uid=impact_assessment_access_group_uid,
            data={},
        )
        await self.upsert_term(ipcc_2013_gwp_100a_term)

        ipcc_2013_gwp_20a_term = Term(
            uid=uuid.uuid4(),
            xid=IPCC_2013_GWP_20_XID,
            name=IPCC_2013_GWP_20,
            sub_class_of=impact_assessment_root_term.uid,
            access_group_uid=impact_assessment_access_group_uid,
            data={},
        )
        await self.upsert_term(ipcc_2013_gwp_20a_term)

        logger.info(f"seeded sample data in glossary, took {time.time() - start_time:.2f}s")
