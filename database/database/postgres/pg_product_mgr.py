import uuid
from typing import Optional
from uuid import UUID

import asyncpg
from structlog import get_logger

from core.domain.edge import XidAccessGroupUid

logger = get_logger()


class PgProductMgr:
    def __init__(self, pool: asyncpg.pool.Pool):
        self.pool = pool

    def get_pool(self) -> asyncpg.pool.Pool:
        return self.pool

    async def get_xid_by_uid(self, uid: str, conn: asyncpg.connection.Connection = None) -> Optional[str]:
        if conn is None:
            conn = self.pool

        row = await conn.fetchrow(
            """
            SELECT xid FROM namespace_xid_mappings WHERE uid = $1;
            """,
            uid,
        )

        if not row:
            return None

        xid = row["xid"]
        logger.debug("got xid from db.", xid=xid)

        return xid

    async def map_xid_access_group_uids_to_uids(
        self, xid_access_group_uids_to_map_to_uid: list[XidAccessGroupUid], conn: asyncpg.connection.Connection = None
    ) -> dict[XidAccessGroupUid, UUID]:
        mapping_dict = {}
        if conn is None:
            conn = self.pool

        chunk_size = 10_000  # Avoid issues due to overflowing argument numbers.
        for i in range(0, len(xid_access_group_uids_to_map_to_uid), chunk_size):
            chunk = xid_access_group_uids_to_map_to_uid[i : i + chunk_size]

            query = """
            SELECT nxm.xid as xid, ag.uid as access_group_uid, nxm.uid as uid FROM namespace_xid_mappings nxm
            JOIN access_group_node ag ON nxm.namespace_uid = ag.namespace_uid
            WHERE {}"""

            # Generate placeholders for the chunk
            where_clauses = " OR ".join(
                "nxm.xid = ${} AND ag.uid = ${}::UUID".format(*(2 * i + 1, 2 * i + 2)) for i in range(len(chunk))
            )

            # Concatenate the placeholders into the query
            query_formatted = query.format(where_clauses)

            # Execute the query with the chunk as parameters
            records = await conn.fetch(
                query_formatted, *sum(((item.xid, str(item.access_group_uid)) for item in chunk), ())
            )  # Flattening the list of tuples
            for record in records:
                mapping_dict[
                    XidAccessGroupUid(xid=record["xid"], access_group_uid=uuid.UUID(int=record["access_group_uid"].int))
                ] = UUID(int=record["uid"].int)
        return mapping_dict

    async def find_uid_by_xid(
        self, namespace_uid: str, xid: str, conn: asyncpg.connection.Connection = None
    ) -> Optional[UUID]:
        if conn is None:
            conn = self.pool

        row = await conn.fetchrow(
            """
               SELECT uid FROM namespace_xid_mappings WHERE namespace_uid = $1 and xid = $2;
            """,
            namespace_uid,
            xid,
        )

        if not row:
            return None

        return UUID(int=row["uid"].int)

    async def find_or_create_uid_by_xid(
        self, namespace_uid: str, xid: str, conn: asyncpg.connection.Connection = None
    ) -> UUID:
        if conn is None:
            conn = self.pool

        row = await conn.fetch(
            """
            WITH input_rows(xid, namespace_uid) AS (
               VALUES
                  ($1, $2::uuid)
               )
            , ins AS (
               INSERT INTO namespace_xid_mappings (xid, namespace_uid)
               SELECT * FROM input_rows
               ON CONFLICT (xid, namespace_uid) DO NOTHING
               RETURNING uid
               )
            SELECT 'created' AS source
                 , uid
            FROM   ins
            UNION  ALL
            SELECT 'existed' AS source
                 , c.uid
            FROM   input_rows
            JOIN   namespace_xid_mappings c USING (xid, namespace_uid);
            """,
            xid,
            namespace_uid,
        )

        uid = row[0]["uid"]
        logger.debug("got uid from db.", uid=uid)

        return UUID(int=uid.int)

    async def get_all_id_mappings_of_namespace(
        self, namespace_uid: str, conn: asyncpg.connection.Connection = None
    ) -> dict:
        if conn is None:
            conn = self.pool

        rows = await conn.fetch(
            """
               SELECT * FROM namespace_xid_mappings WHERE namespace_uid = $1;
            """,
            namespace_uid,
        )

        return {r["xid"]: uuid.UUID(str(r["uid"])) for r in rows}

    async def get_all_uid_to_xid_mappings_of_namespace(
        self, namespace_uid: str, conn: asyncpg.connection.Connection = None
    ) -> dict:
        if conn is None:
            conn = self.pool

        rows = await conn.fetch(
            """
               SELECT * FROM namespace_xid_mappings WHERE namespace_uid = $1;
            """,
            namespace_uid,
        )

        return {uuid.UUID(str(r["uid"])): r["xid"] for r in rows}

    async def bulk_insert_xid_uid_mappings(
        self, namespace_uid_xid_uids_to_insert: list[tuple[str, str, uuid.UUID]]
    ) -> None:
        """(faster) Bulk insert."""
        await self.pool.executemany(
            """
            INSERT INTO namespace_xid_mappings (namespace_uid, xid, uid)
            VALUES($1, $2, $3)
            ON CONFLICT (xid, namespace_uid)
            DO UPDATE SET uid = $3;
            """,
            namespace_uid_xid_uids_to_insert,
        )

    async def bulk_delete_xid_uid_mappings(
        self, namespace_uid_xid_uids_to_insert: list[tuple[str, str, uuid.UUID]]
    ) -> None:
        """(faster) Bulk delete."""
        await self.pool.executemany(
            """
            DELETE FROM namespace_xid_mappings
            WHERE namespace_uid = $1 AND xid = $2 AND uid = $3;
            """,
            namespace_uid_xid_uids_to_insert,
        )
