import uuid
from typing import Optional

import asyncpg
from structlog import get_logger

from core.domain.access_group_node import AccessGroup
from core.domain.token import Token
from core.domain.user import User
from database.postgres.pg_access_mgr import PostgresAccessMgr

logger = get_logger()


class PostgresUsersMgr:
    def __init__(self, pool: asyncpg.pool.Pool, pg_access_group_mgr: PostgresAccessMgr):
        self.pool = pool
        self.pg_access_group_mgr = pg_access_group_mgr

    def get_pool(self) -> asyncpg.pool.Pool:
        return self.pool

    async def find_user_info_by_user_id(
        self, user_id: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> User | None:
        if conn is None:
            conn = self.pool

        row = await conn.fetchrow(
            "SELECT * FROM users WHERE user_id = $1",
            user_id,
        )

        if row:
            user = User(
                email=row.get("email"),
                password=row.get("password"),
                is_superuser=row.get("is_superuser"),
                user_id=user_id,
                legacy_api_default_access_group_uid=row.get("legacy_api_default_access_group_uid"),
            )

            return user
        else:
            return None

    async def find_user_and_default_group_info_by_auth_token(
        self, auth_token: str, conn: Optional[asyncpg.connection.Connection] = None
    ) -> tuple[User | None, AccessGroup | None]:
        if conn is None:
            conn = self.pool

        try:
            row = await conn.fetchrow(
                """
                SELECT
                email, password, is_superuser, users.user_id, legacy_api_default_access_group_uid,
                uid, access_group_node.namespace_uid
                FROM users
                    JOIN user_auth_tokens ON users.user_id = user_auth_tokens.user_id AND access_token = $1
                    LEFT JOIN access_group_node ON users.legacy_api_default_access_group_uid = access_group_node.uid
                """,
                auth_token,
            )
        except asyncpg.exceptions.DataError:
            return None, None

        if row:
            user = User(
                email=row.get("email"),
                password=row.get("password"),
                is_superuser=row.get("is_superuser"),
                user_id=row.get("user_id"),
                legacy_api_default_access_group_uid=row.get("legacy_api_default_access_group_uid"),
            )

            default_access_group = AccessGroup(
                uid=str(row.get("uid")) if row.get("uid") else None,
                namespace_uid=str(row.get("namespace_uid")) if row.get("namespace_uid") else None,
            )

            return user, default_access_group
        else:
            return None, None

    async def validate_token(self, auth_token: str, conn: Optional[asyncpg.connection.Connection] = None) -> bool:
        if conn is None:
            conn = self.pool

        try:
            row = await conn.fetchrow(
                "SELECT * FROM user_auth_tokens " "WHERE access_token = $1",
                auth_token,
            )
        except asyncpg.exceptions.DataError:
            return False

        if row:
            return True

    async def insert_or_update_user(
        self, user: User, auth_token: str = None, conn: Optional[asyncpg.connection.Connection] = None
    ) -> str:
        if conn is None:
            conn = self.pool

        if user.user_id is None:
            user.user_id = str(uuid.uuid4())

        new_user_id = await conn.fetchval(
            """
            INSERT INTO users (
                user_id,
                email,
                password,
                is_superuser,
                is_service_account,
                legacy_api_default_access_group_uid,
                updated_at
            )
            VALUES ($1, $2, $3, $4, $5, $6, NOW())
            ON CONFLICT (user_id)
            DO UPDATE SET
                email = $2,
                password = $3,
                is_superuser = $4,
                legacy_api_default_access_group_uid = $6,
                updated_at = NOW()
            RETURNING user_id;
            """,
            user.user_id,
            user.email,
            user.password,
            user.is_superuser,
            user.is_service_account,
            user.legacy_api_default_access_group_uid,
        )

        token = await conn.fetchval(
            """
            INSERT INTO user_auth_tokens (user_id, name, access_token)
            VALUES ($1, $2, COALESCE($3, gen_random_uuid()::text))
            ON CONFLICT (user_id, name)
            DO UPDATE SET
                access_token = COALESCE($3, user_auth_tokens.access_token),
                updated_at = NOW()
            RETURNING access_token;
            """,
            new_user_id,
            "Default",
            auth_token,
        )

        logger.info("upserted user into db.", user=user)

        return token

    async def update_access_token(self, token: Token, conn: Optional[asyncpg.connection.Connection] = None) -> str:
        if conn is None:
            conn = self.pool

        await conn.execute(
            """
            INSERT INTO user_auth_tokens (user_id, access_token, name)
            VALUES ($1, $2, $3)
            ON CONFLICT (user_id, name)
            DO UPDATE SET access_token = $2, updated_at = NOW()
            """,
            token.user_id,
            token.token,
            token.name,
        )

        return token.token

    async def list_user_tokens(self, user_id: str, conn: Optional[asyncpg.connection.Connection] = None) -> list[Token]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            SELECT user_id, name, access_token FROM user_auth_tokens
            WHERE user_id = $1;
            """,
            user_id,
        )

        return [
            Token(
                user_id=record.get("user_id"),
                name=record.get("name"),
                token=record.get("access_token"),
            )
            for record in records
        ]

    async def list_users_from_namespace(
        self, namespace_uid: uuid.UUID, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[User]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            SELECT users.*
            FROM access_group_node AS agn
                JOIN group_membership
                    ON agn.uid = group_membership.access_group_uid
                JOIN users ON group_membership.user_id = users.user_id
            WHERE agn.namespace_uid = $1;
            """,
            namespace_uid,
        )

        return [
            User(
                email=record.get("email"),
                password=record.get("password"),
                is_superuser=record.get("is_superuser"),
                user_id=str(record.get("user_id")),
                legacy_api_default_access_group_uid=str(record.get("legacy_api_default_access_group_uid")),
            )
            for record in records
        ]
