CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
-- speeding up full-text index search using trigrams
CREATE EXTENSION IF NOT EXISTS "pg_trgm" WITH SCHEMA public;
-- support for full-text search using GIN (Generalized Inverted Index)
CREATE EXTENSION IF NOT EXISTS "btree_gin" WITH SCHEMA public;

CREATE SCHEMA IF NOT EXISTS public;

CREATE TABLE public.namespace (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(), -- globally unique
    xid TEXT UNIQUE, -- globally unique
    name TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE public.users (
    user_id uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    email varchar(255) UNIQUE,
    -- TODO: remove this field once we have authentication via Cognito implemented
    password TEXT,
    is_superuser BOOLEAN,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    is_service_account BOOLEAN,
    legacy_api_default_access_group_uid uuid
);
CREATE INDEX IF NOT EXISTS users_legacy_api_default_access_group_uid_idx ON public.users (legacy_api_default_access_group_uid);

CREATE TABLE public.access_group_node (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    xid TEXT NOT NULL DEFAULT public.uuid_generate_v4() CHECK (xid <> ''),
	type TEXT, -- i.e. 'kitchen' 'producer' 'factory'
    data JSONB,
	namespace_uid uuid NOT NULL REFERENCES public.namespace (uid) ON DELETE CASCADE,
	creator uuid REFERENCES public.users(user_id) ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT xid_namespace UNIQUE (xid, namespace_uid)
);
-- index for faster lookup by `verify_access_group_creation_by_user()` method
CREATE INDEX access_group_creator_index ON public.access_group_node (xid, creator);

-- adding a foreign key to `legacy_api_default_access_group_uid` column
-- of `user` table
ALTER TABLE public.users
    ADD CONSTRAINT users_legacy_api_default_access_group_uid_fk
        FOREIGN KEY (legacy_api_default_access_group_uid)
            REFERENCES public.access_group_node (uid)
            ON DELETE SET NULL;

CREATE TABLE public.namespace_xid_mappings (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    xid TEXT DEFAULT public.uuid_generate_v4(),
    namespace_uid uuid REFERENCES public.namespace (uid) ON DELETE CASCADE,
    UNIQUE(xid, namespace_uid)
);
CREATE INDEX IF NOT EXISTS namespace_xid_mappings_namespace_uid_idx ON public.namespace_xid_mappings (namespace_uid);

CREATE TABLE public.term (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    xid TEXT,  -- an optional external id provided by the data source (to allow later updates)
    name TEXT,
    sub_class_of uuid REFERENCES public.term (uid) ON DELETE CASCADE,
    data JSONB,
	namespace_uid uuid NOT NULL REFERENCES public.namespace (uid) ON DELETE CASCADE,
    access_group_uid uuid NOT NULL REFERENCES public.access_group_node (uid) ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    UNIQUE (namespace_uid, xid)
);
CREATE INDEX IF NOT EXISTS term_sub_class_of_idx ON public.term (sub_class_of);
CREATE INDEX IF NOT EXISTS term_namespace_uid_idx ON public.term (namespace_uid);
CREATE INDEX IF NOT EXISTS term_access_group_uid_idx ON public.term (access_group_uid);

-- Create a function that automatically sets the namespace_uid in term based on the access_group_uid
CREATE OR REPLACE FUNCTION set_namespace_uid()
RETURNS TRIGGER AS $$
BEGIN
    NEW.namespace_uid := (
        SELECT namespace_uid
        FROM public.access_group_node
        WHERE uid = NEW.access_group_uid
    );
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Create a trigger on the term table to execute before insert
CREATE TRIGGER set_namespace_uid_trigger
BEFORE INSERT ON public.term
FOR EACH ROW
EXECUTE FUNCTION set_namespace_uid();

CREATE TABLE public.group_membership(
    user_id uuid REFERENCES public.users (user_id) ON DELETE CASCADE,
    access_group_uid uuid REFERENCES public.access_group_node (uid) ON DELETE CASCADE,
    permissions JSONB,
    CONSTRAINT pk_membership PRIMARY KEY (user_id, access_group_uid)
);
CREATE INDEX IF NOT EXISTS group_membership_user_id_idx ON public.group_membership (user_id);
CREATE INDEX IF NOT EXISTS group_membership_access_group_uid_idx ON public.group_membership (access_group_uid);
-- index for faster lookup by `verify_access_group_membership_by_user()` methods
CREATE INDEX user_group_membership_index ON public.group_membership (user_id, access_group_uid);


CREATE TABLE public.node (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    access_group_uid uuid REFERENCES public.access_group_node (uid) ON DELETE CASCADE, -- optional (only for kitchen resources)
    node_type TEXT,  -- "recipe", "ingredient", "supply", "product", etc
    data JSONB,
    post_calc_cache JSONB,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMPTZ,
    updated_by uuid REFERENCES public.users(user_id) ON DELETE SET NULL,
    md5_hash uuid
);
CREATE INDEX node_type_index ON public.node (node_type); -- faster for populating pg_graph_mgr's cache
CREATE INDEX access_group_uid_index ON public.node (access_group_uid); -- faster for searching
CREATE INDEX idx_required_matching_exists ON public.node
    USING btree ((public.node."data" ? 'required_matching'))
    WHERE public.node."data" ? 'required_matching';

-- Add generated column for declaration_index_value
ALTER TABLE public.node ADD COLUMN declaration_index_value INTEGER 
    GENERATED ALWAYS AS ((data -> 'declaration_index' ->> 'value')::integer) STORED;

---------------------------------------------------------------------
--------- Our graph of recipes and products and supplies: -----------
---------------------------------------------------------------------

-- Example: we have a recipe with id=parent_recipe01 with two ingredients id=sub_recipe01, id=sub_recipe02
-- We would have the following rows in the "node" table
-- node:   (uid=parent_recipe01, data={...})
-- node:   (uid=sub_recipe01, data={...})
-- node:   (uid=sub_recipe02, data={...})
-- We would have the following entries in the "edge" table:
-- edge:   (parent_uid=parent_recipe01, child_uid=sub_recipe01)
-- edge:   (parent_uid=parent_recipe01, child_uid=sub_recipe02)

CREATE TABLE public.edge (
    edge_type TEXT,
    parent_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    child_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT pkey_link PRIMARY KEY (parent_uid, child_uid, edge_type)
);
CREATE INDEX edge_type_index ON public.edge (edge_type); -- faster for populating pg_graph_mgr's cache
CREATE INDEX edge_parent_uid_index ON public.edge (parent_uid);
CREATE INDEX edge_child_uid_index ON public.edge (child_uid); -- faster for upsert_recipe

CREATE TABLE public.calculation (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    root_node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    data_input bytea,
    data_mutations bytea,
    data_result bytea,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    retry_count INTEGER DEFAULT 0
);
CREATE INDEX IF NOT EXISTS calculation_root_node_uid_idx ON public.calculation (root_node_uid);

-- polymorphic association table, using "exclusive belongs to" pattern
-- see https://hashrocket.com/blog/posts/modeling-polymorphic-associations-in-a-relational-database
CREATE TABLE public.glossary_link (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    gap_filling_module TEXT,
    linked_term_uid uuid REFERENCES public.term (uid) ON DELETE CASCADE,         -- link to a Term (used for nutrition)
    linked_node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,  -- or link to a Node (used for processes)
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE INDEX IF NOT EXISTS glossary_link_linked_term_uid_idx ON public.glossary_link (linked_term_uid);
CREATE INDEX IF NOT EXISTS glossary_link_linked_node_uid_idx ON public.glossary_link (linked_node_uid);

-- to attach a group of terms (e.g. "dried", "banana") to a nutrition file (term) or a process (node)
CREATE TABLE public.glossary_grouping (
    term_uid uuid REFERENCES public.term (uid) ON DELETE CASCADE,
    glossary_link_uid uuid REFERENCES public.glossary_link (uid) ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE INDEX IF NOT EXISTS glossary_grouping_term_uid_idx ON public.glossary_grouping (term_uid);
CREATE INDEX IF NOT EXISTS glossary_grouping_glossary_link_uid_idx ON public.glossary_grouping (glossary_link_uid);

CREATE OR REPLACE FUNCTION compute_data_hash()
RETURNS TRIGGER AS $$
BEGIN
    -- Ensure consistent JSON representation by using jsonb_pretty()
    -- and hashing the result of that consistent representation
    NEW.data_hash := md5(jsonb_pretty(NEW.data)::text);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE public.matching (
    uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    gap_filling_module TEXT,
    access_group_uid uuid,
    lang TEXT,
    matching_string TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_by uuid REFERENCES public.users(user_id) ON DELETE SET NULL,
    matched_node_uid uuid REFERENCES public.node (uid),
    data JSONB
);
CREATE UNIQUE INDEX ON public.matching (gap_filling_module, lang, matching_string, jsonb_hash_extended(coalesce(data, '{}'::jsonb), 0));
CREATE INDEX IF NOT EXISTS matching_matched_node_uid_idx ON public.matching (matched_node_uid);

-- association table to allow a matching_string to map to multiple terms
CREATE TABLE public.matching_terms (
    matching_uid uuid REFERENCES public.matching (uid) ON DELETE CASCADE,
    term_uid uuid REFERENCES public.term (uid) ON DELETE CASCADE,
    CONSTRAINT pk_matching_terms PRIMARY KEY (matching_uid, term_uid)
);
CREATE INDEX IF NOT EXISTS matching_terms_matching_uid_idx ON public.matching_terms (matching_uid);
CREATE INDEX IF NOT EXISTS matching_terms_term_uid_idx ON public.matching_terms (term_uid);

-- table storing information about what is the root node 
CREATE TABLE public.matching_required (
    node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    root_node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    root_activity_date DATE,
    root_create_date DATE,
    root_namespace_uid uuid,
    CONSTRAINT pk_matching_required PRIMARY KEY (node_uid, root_node_uid)
);
CREATE INDEX IF NOT EXISTS matching_required_node_uid_idx ON public.matching_required (node_uid);
CREATE INDEX IF NOT EXISTS matching_required_root_node_uid_idx ON public.matching_required (root_node_uid);
CREATE INDEX IF NOT EXISTS idx_matching_required_activity_date ON matching_required(root_activity_date);
CREATE INDEX IF NOT EXISTS idx_matching_required_root_create_date ON matching_required(root_create_date);
CREATE INDEX IF NOT EXISTS idx_matching_required_root_namespace ON matching_required(root_namespace_uid);


-- table storing information about the applied matching
CREATE TABLE public.matching_applied (
    node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    root_node_uid uuid REFERENCES public.node (uid) ON DELETE CASCADE,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    root_activity_date DATE,
    root_create_date DATE,
    root_namespace_uid uuid REFERENCES public.namespace (uid) ON DELETE CASCADE,
    matching_uid uuid REFERENCES public.matching (uid) ON DELETE CASCADE,
    matching_context JSONB,
    CONSTRAINT pk_matching_applied PRIMARY KEY (node_uid, root_node_uid, matching_uid)
);
CREATE INDEX IF NOT EXISTS matching_applied_node_uid_idx ON public.matching_applied (node_uid);
CREATE INDEX IF NOT EXISTS matching_applied_root_node_uid_idx ON public.matching_applied (root_node_uid);
CREATE INDEX IF NOT EXISTS matching_applied_activity_date_idx ON matching_applied(root_activity_date);
CREATE INDEX IF NOT EXISTS matching_applied_root_create_date_idx ON matching_applied(root_create_date);
CREATE INDEX IF NOT EXISTS matching_applied_root_namespace_uid_idx ON matching_applied(root_namespace_uid);
CREATE INDEX IF NOT EXISTS matching_applied_matching_uid_idx ON public.matching_applied (matching_uid);

-- Automatically insert the root_activity_date and namespace_uid into the matching_required table using trigger:
CREATE OR REPLACE FUNCTION update_matching_required_metadata()
RETURNS TRIGGER AS $$
BEGIN
    SELECT 
        (root_activity.data->>'activity_date')::DATE,
        rn.created_at::DATE,
        rns.uid
    INTO 
        NEW.root_activity_date,
        NEW.root_create_date,
        NEW.root_namespace_uid
    FROM node rn
    INNER JOIN access_group_node ragn ON ragn.uid = rn.access_group_uid
    INNER JOIN namespace rns ON rns.uid = ragn.namespace_uid
    INNER JOIN edge e ON e.parent_uid = NEW.root_node_uid and e.edge_type = 'root_to_recipe'
    INNER JOIN node root_activity ON root_activity.uid = e.child_uid
    WHERE rn.uid = NEW.root_node_uid;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER matching_required_metadata_trigger
    BEFORE INSERT OR UPDATE ON matching_required
    FOR EACH ROW
    EXECUTE FUNCTION update_matching_required_metadata(); 

CREATE TRIGGER matching_applied_metadata_trigger
    BEFORE INSERT OR UPDATE ON matching_applied
    FOR EACH ROW
    EXECUTE FUNCTION update_matching_required_metadata(); 

CREATE TABLE public.access_group_edge (
    parent_access_group_uid uuid REFERENCES public.access_group_node (uid) ON DELETE CASCADE,
--     TODO: make this field unique?
    child_access_group_uid uuid REFERENCES public.access_group_node (uid)  ON DELETE CASCADE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_group_edge PRIMARY KEY (child_access_group_uid, parent_access_group_uid)
);
CREATE INDEX IF NOT EXISTS access_group_edge_parent_access_group_uid_idx ON public.access_group_edge (parent_access_group_uid);
CREATE INDEX IF NOT EXISTS access_group_edge_child_access_group_uid_idx ON public.access_group_edge (child_access_group_uid);

CREATE TABLE user_auth_tokens (
  user_id uuid REFERENCES public.users (user_id) ON DELETE CASCADE,
  access_token TEXT UNIQUE DEFAULT public.uuid_generate_v4()::text CHECK (access_token <> ''),
  name TEXT,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  CONSTRAINT pk_user_token_name PRIMARY KEY (user_id, name)
);
CREATE INDEX IF NOT EXISTS user_auth_tokens_user_id_idx ON public.user_auth_tokens (user_id);

CREATE TABLE public.ingredients_declaration_matching (
    declaration_uid uuid PRIMARY KEY DEFAULT public.uuid_generate_v4(),
    ingredients_declaration TEXT UNIQUE NOT NULL CHECK (ingredients_declaration <> ''),
    fixed_ingredients_declaration TEXT NOT NULL CHECK (fixed_ingredients_declaration <> ''),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
-- creating GIN index for faster text search query performance
CREATE INDEX faulty_ingredients_declaration_index
    ON public.ingredients_declaration_matching
        USING gin(ingredients_declaration);

CREATE TABLE gap_filling_modules_cache (
  gfm_name TEXT,
  cache_key TEXT,
  cache_data JSONB,
  load_on_boot BOOLEAN DEFAULT FALSE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  CONSTRAINT pk_gfm_cache_key PRIMARY KEY (gfm_name, cache_key)
);

CREATE OR REPLACE FUNCTION mark_recipe_as_deleted_with_lock(recipe_uid uuid)
RETURNS void AS $$
BEGIN
    -- Acquire the lock first
    PERFORM pg_advisory_xact_lock(hashtext(recipe_uid::text));
    -- Then perform the update
    UPDATE node
    SET deleted_at = NOW()
    WHERE uid IN (
        SELECT uid AS relevant_uid
        FROM node
        WHERE uid = recipe_uid
        UNION
        SELECT child_uid AS relevant_uid
        FROM edge
        WHERE parent_uid = recipe_uid
        UNION
        SELECT parent_uid AS relevant_uid
        FROM edge
        WHERE child_uid = recipe_uid
        AND edge_type = 'root_to_recipe'
    );
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION upsert_recipe_with_lock(
    transient bool, 
    activity_uid uuid, 
    activity_access_group_uid uuid, 
    activity_node_type text, 
    activity_data jsonb,
    incoming_child_jsonb jsonb,
    incoming_parent_uid uuid,
    incoming_parent_access_group_uid uuid,
    incoming_parent_node_type text,
    incoming_parent_data jsonb,
    unmatch_based_on_ingredients_declaration boolean,
    incoming_md5_hash uuid
)
RETURNS jsonb AS $$
DECLARE
    result jsonb;
BEGIN

    -- Create temporary table for the the nodes that will be upserted
    CREATE TEMP TABLE temp_new_children ON COMMIT DROP
    AS
    SELECT
        COALESCE(
            (new_child ->> 0)::uuid,
            old_child.uid,
            public.uuid_generate_v4()
        ) AS uid,
        (new_child ->> 1)::uuid AS access_group_uid,
        (new_child ->> 2)::text AS node_type,
        COALESCE(
            CASE
                WHEN
                (
                    (
                        old_child.data -> 'product_name' IS NOT DISTINCT FROM new_child -> 3 -> 'product_name'
                        OR
                        (
                            EXISTS (
                                SELECT 1
                                FROM jsonb_array_elements(old_child.data -> 'product_name') AS n_elem,
                                        jsonb_array_elements(new_child -> 3 -> 'product_name') AS inc_elem
                                WHERE n_elem.value = inc_elem.value
                            )
                        )
                    )
                    AND 
                    (
                        unmatch_based_on_ingredients_declaration = FALSE
                        OR
                        (new_child -> 3 -> 'ingredients_declaration') IS NULL
                        OR (
                            (old_child.data -> 'ingredients_declaration') IS NOT DISTINCT FROM
                            (new_child -> 3 -> 'ingredients_declaration')
                        )
                    )
                )
                THEN
                    (new_child -> 3)::jsonb ||
                    jsonb_strip_nulls(
                        jsonb_build_object(
                            'link_to_sub_node',
                            COALESCE(new_child -> 3 -> 'link_to_sub_node', old_child.data -> 'link_to_sub_node')
                        )
                    )
                    ||
                    jsonb_strip_nulls(
	                    jsonb_build_object(
    	                    'ingredients_declaration',
        	                COALESCE(new_child -> 3 -> 'ingredients_declaration', old_child.data -> 'ingredients_declaration')
            	        )
                    )
            END,
            (new_child -> 3)::jsonb
        ) AS data
    FROM jsonb_array_elements(incoming_child_jsonb) AS new_child
    LEFT JOIN (
        SELECT n.uid, n.declaration_index_value, n.data
        FROM node n
        JOIN edge e ON n.uid = e.child_uid
        WHERE e.parent_uid = activity_uid::uuid
    ) old_child ON (new_child -> 3 -> 'declaration_index' ->> 'value')::integer = old_child.declaration_index_value::integer;

    SET LOCAL plan_cache_mode = 'force_custom_plan';

    -- Acquire the lock
    PERFORM pg_advisory_xact_lock(hashtext(activity_uid::text));

    -- In the main query, replace the original subquery with the temp table
    WITH upserted_children AS (
        INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at)
        SELECT 
            tc.uid, tc.access_group_uid, tc.node_type, tc.data,
            NOW(),
            CASE
                WHEN transient = TRUE THEN NOW()
                ELSE NULL
            END AS deleted_at
        FROM temp_new_children tc
        ON CONFLICT (uid) DO UPDATE SET
        access_group_uid = EXCLUDED.access_group_uid,
        node_type = EXCLUDED.node_type,
        data = EXCLUDED.data,
        updated_at = EXCLUDED.updated_at,
        deleted_at = EXCLUDED.deleted_at
        RETURNING uid, data, access_group_uid
    ),
    deleted_child_nodes AS (
        -- Delete children nodes that were not upserted (i.e., deleted)
        DELETE FROM node
        USING edge
        LEFT JOIN upserted_children uc ON edge.child_uid = uc.uid
        WHERE node.uid = edge.child_uid
        AND edge.parent_uid = activity_uid::uuid
        AND uc.uid IS NULL
        RETURNING *
    ),
    child_edges_of_upserted_children_to_delete AS (
        SELECT edge.parent_uid, edge.child_uid, edge.edge_type FROM edge
        JOIN upserted_children ON
        edge.parent_uid = upserted_children.uid
        WHERE edge.edge_type = 'link_to_subrecipe'
    ),
    children_link_to_sub_nodes AS (
        SELECT uc.uid AS uid, (uc.data ->> 'link_to_sub_node')::jsonb AS link_to_sub_node, access_group_uid
        FROM upserted_children AS uc
    ),
    newly_added_child_edges_of_upserted_children AS (
        SELECT clsn.uid AS parent_uid,
        CASE
            WHEN ((clsn.link_to_sub_node ->> 'prop_type')::text = 'LinkToUidProp') THEN
                json_build_object(
                    'uid', (clsn.link_to_sub_node ->> 'uid')::uuid
                )
            WHEN ((clsn.link_to_sub_node ->> 'prop_type')::text = 'LinkToXidProp') THEN
                json_build_object(
                    'xid', (clsn.link_to_sub_node ->> 'xid')::text,
                    'access_group_uid', clsn.access_group_uid
                )
            ELSE
                NULL
        END AS child_xid_or_uid
        FROM children_link_to_sub_nodes AS clsn
    ),
    upserted_parent AS (
        INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at)
        SELECT
            COALESCE(
                incoming_parent.uid::uuid,
                (
                    SELECT n.uid
                    FROM node n JOIN edge e ON n.uid = e.parent_uid
                    WHERE e.child_uid = activity_uid::uuid
                    AND e.edge_type = 'root_to_recipe'
                    LIMIT 1  -- Limit the result to ensure only one value is returned
                ),
                public.uuid_generate_v4()
            ),
            incoming_parent.access_group_uid::uuid, incoming_parent.node_type::text,
            incoming_parent.data::jsonb,
            NOW(),
            CASE
                WHEN transient = TRUE THEN NOW()
                ELSE NULL
            END AS deleted_at
        FROM (
            VALUES (incoming_parent_uid, incoming_parent_access_group_uid, incoming_parent_node_type, incoming_parent_data)
        )
        AS incoming_parent (uid, access_group_uid, node_type, data)
        WHERE incoming_parent_node_type IS NOT NULL
        ON CONFLICT (uid) DO UPDATE SET
        access_group_uid = EXCLUDED.access_group_uid, node_type = EXCLUDED.node_type, data = EXCLUDED.data,
        updated_at = EXCLUDED.updated_at, deleted_at = EXCLUDED.deleted_at
        RETURNING uid
    ),
    deleted_parent_node AS (
        -- Delete parent nodes that were not upserted (i.e., deleted)
        DELETE FROM node
        USING edge
        LEFT JOIN upserted_parent up on edge.parent_uid = up.uid
        WHERE node.uid = edge.parent_uid
        AND edge.child_uid = activity_uid::uuid
        AND edge.edge_type = 'root_to_recipe'
        AND up.uid IS NULL
        RETURNING *
    ),
    deleted_other_children_of_parent_node AS (
        -- Delete edges to other children nodes of the parent node: A root flow can only have a single recipe.
        DELETE FROM edge
        WHERE edge.parent_uid IN (SELECT uid FROM upserted_parent)
        AND edge.child_uid != activity_uid::uuid
        RETURNING *
    ),
    upserted_self_node AS (
        INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at, md5_hash)
        SELECT
            self_node.uid::uuid, self_node.access_group_uid::uuid, self_node.node_type::text, self_node.data::jsonb,
            NOW(),
            CASE
                WHEN transient = TRUE THEN NOW()
                ELSE NULL
            END AS deleted_at,
            NULL::uuid
        FROM (
            VALUES (activity_uid, activity_access_group_uid, activity_node_type, activity_data)
        )
        AS self_node (uid, access_group_uid, node_type, data)
        ON CONFLICT (uid) DO UPDATE SET
        access_group_uid = EXCLUDED.access_group_uid, node_type = EXCLUDED.node_type, data = EXCLUDED.data,
        updated_at = EXCLUDED.updated_at, deleted_at = EXCLUDED.deleted_at, md5_hash = NULL::uuid
        RETURNING uid
    ),
    inserted_edges AS (
        INSERT INTO edge (parent_uid, child_uid, edge_type)
        SELECT
            CASE
                WHEN EXISTS (SELECT 1 FROM upserted_parent) THEN (SELECT uid FROM upserted_parent)
                ELSE NULL
            END AS parent_uid,
            (SELECT uid FROM upserted_self_node) AS child_uid,
            'root_to_recipe' AS edge_type
        WHERE EXISTS (SELECT 1 FROM upserted_parent)
        UNION ALL
        SELECT
            (SELECT uid FROM upserted_self_node) AS parent_uid,
            uc.uid AS child_uid,
            'ingredient' AS edge_type
        FROM upserted_children uc
        ON CONFLICT DO NOTHING
    )
    SELECT json_build_object (
        'edges_to_delete',
        CASE
            WHEN EXISTS (SELECT 1 FROM upserted_children) THEN
                (
                    SELECT json_agg(child_edges_of_upserted_children_to_delete)
                    FROM child_edges_of_upserted_children_to_delete
                )
            ELSE NULL
        END,
        'edges_to_insert',
        CASE
            WHEN EXISTS (SELECT 1 FROM upserted_children) THEN
                (
                    SELECT json_agg(newly_added_child_edges_of_upserted_children)
                    FROM newly_added_child_edges_of_upserted_children
                )
            ELSE NULL
        END,
        'self_uid', (SELECT uid FROM upserted_self_node),
        'parent_uid',
        CASE
            WHEN EXISTS (SELECT 1 FROM upserted_parent) THEN (SELECT uid FROM upserted_parent)
            ELSE NULL
        END
    )::jsonb INTO result;

    -- now check if edges_to_delete and edges_to_insert are empty and if so, set md5_hash:
    IF (result->>'edges_to_insert' IS NULL OR jsonb_array_length(result->'edges_to_insert') = 0) AND 
       (result->>'edges_to_delete' IS NULL OR jsonb_array_length(result->'edges_to_delete') = 0) THEN
        UPDATE node SET md5_hash = incoming_md5_hash WHERE uid = activity_uid;
    END IF;
    
    RETURN result;
END;
$$ LANGUAGE plpgsql;

-- Add new indexes:
CREATE INDEX idx_edge_parent_child ON edge (parent_uid, child_uid, edge_type);