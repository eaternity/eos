import hashlib
import json
import time
from datetime import date
from enum import Enum
from typing import List, Optional, Type
from uuid import UUID, uuid4

import asyncpg
from structlog import get_logger

from core.domain.deep_mapping_view import DeepListView
from core.domain.edge import Edge, EdgeToUpsert, EdgeTypeEnum, XidAccessGroupUid
from core.domain.nodes import node_class_from_type
from core.domain.nodes.activity.modeled_activity_node import ModeledActivityNode
from core.domain.nodes.flow_node import FlowNode
from core.domain.nodes.node import Node, PreAndPostCalcNodes
from core.domain.post_calc_cache import NodeCacheInfo, PostCalcCache, PostCalcDataMetaInformation
from core.domain.prop import Prop
from core.domain.props.quantity_prop import RawQuantity
from core.envprops import EnvProps
from core.service.glossary_service import GlossaryService

logger = get_logger()


class RelationTypeEnum(str, Enum):
    self = "self"
    child = "child"
    parent = "parent"


class PostgresGraphMgr:
    def __init__(self, pool: asyncpg.pool.Pool, glossary_service: GlossaryService):
        self.pool = pool
        self.glossary_service = glossary_service
        self.analyze = False

    def get_pool(self) -> asyncpg.pool.Pool:
        return self.pool

    @staticmethod
    def _convert_pg_record_to_node(
        record: asyncpg.Record, include_post_calc_data: bool = False
    ) -> Node | PreAndPostCalcNodes:
        init_data = {
            "uid": UUID(int=record["uid"].int),
            "access_group_uid": record.get("access_group_uid", None),
            "node_type": record["node_type"],
        } | record["data"]

        node_class = node_class_from_type(node_type=record["node_type"])
        node = node_class(**init_data)

        if not include_post_calc_data:
            return node
        else:
            if record.get("post_calc_cache"):
                post_calc_data = {
                    "uid": UUID(int=record["uid"].int),
                    "access_group_uid": record.get("access_group_uid", None),
                    "node_type": record["node_type"],
                } | record["post_calc_cache"]["node"]
                post_calc_node = node_class(**post_calc_data)
                return PreAndPostCalcNodes(pre_calc=node, post_calc=post_calc_node)
            else:
                return PreAndPostCalcNodes(pre_calc=node)

    @staticmethod
    def _data_dict_from_node(node: Node) -> dict:
        node_data_dict = node.model_dump(
            mode="json", exclude_none=True, exclude={"uid", "access_group_uid", "node_type"}
        )
        return node_data_dict

    @staticmethod
    def _convert_pg_record_to_edge(record: asyncpg.Record) -> Edge:
        return Edge(
            parent_uid=UUID(int=record["parent_uid"].int),
            child_uid=UUID(int=record["child_uid"].int),
            edge_type=EdgeTypeEnum[record["edge_type"]],
        )

    def _convert_node_to_pg_dict(self, node: Node) -> dict:
        """Convert a Calculation object to a dictionary that can be stored in the database."""
        db_record_dict = {
            "uid": node.uid,
            "access_group_uid": node.access_group_uid,
            "node_type": node.node_type,
            "data": self._data_dict_from_node(node),
        }
        return db_record_dict

    def _convert_post_calc_cache_to_pg_dict(self, post_calc_cache: PostCalcCache) -> dict:
        return {
            "node": self._convert_node_to_pg_dict(post_calc_cache.node)["data"],
            "final_graph_table": post_calc_cache.final_graph_table,
            "meta_information": post_calc_cache.meta_information.model_dump(),
        }

    async def get_nodes_by_type(
        self, node_types: [str], conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Node]:
        if conn is None:
            conn = self.pool
        records = await conn.fetch(
            """
                SELECT *
                FROM node
                WHERE node_type = ANY($1)
                """,
            node_types,
        )
        try:
            nodes = []
            for record in records:
                node = self._convert_pg_record_to_node(record)
                node.is_persistent = True
                nodes.append(node)
            logger.debug("got nodes from db.", node=nodes)
            return nodes
        except TypeError:
            return []

    async def get_uids_of_inventory_nodes_to_cache(
        self, conn: Optional[asyncpg.connection.Connection] = None
    ) -> set[UUID]:
        if conn is None:
            conn = self.pool
        records = await conn.fetch(
            """
            SELECT jsonb_array_elements_text(gfm_cache.cache_data -> 'uids')::text AS uid
            FROM gap_filling_modules_cache gfm_cache
            WHERE gfm_name = 'InventoryConnectorGapFillingFactory'
            AND cache_key = 'pruned_nodes_to_cache';
            """
        )
        try:
            return set(UUID(record.get("uid")) for record in records if isinstance(record.get("uid"), str))
        except TypeError:
            return set()

    async def get_pruned_nodes_to_cache(self, conn: Optional[asyncpg.connection.Connection] = None) -> list[Node]:
        if conn is None:
            conn = self.pool
        records = await conn.fetch(
            """
            SELECT *
            FROM node
            WHERE node_type = 'ElementaryResourceEmissionNode' OR
            (
                (node_type = 'ModeledActivityNode' OR node_type = 'FlowNode') AND uid IN (
                    SELECT jsonb_array_elements_text(gfm_cache.cache_data -> 'uids')::uuid
                    FROM gap_filling_modules_cache gfm_cache
                    WHERE gfm_name = 'InventoryConnectorGapFillingFactory'
                    AND cache_key = 'pruned_nodes_to_cache'
                )
            );
            """
        )
        try:
            nodes = []
            for record in records:
                node = self._convert_pg_record_to_node(record)
                node.is_persistent = True
                nodes.append(node)
            logger.debug("got nodes from db.", node=nodes)
            return nodes
        except TypeError:
            return []

    async def get_edges_below_pruned_nodes_to_cache(
        self, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Edge]:
        if conn is None:
            conn = self.pool
        records = await conn.fetch(
            """
            SELECT e.*
            FROM edge e
            JOIN node n ON e.parent_uid = n.uid
            WHERE n.uid IN (
                SELECT jsonb_array_elements_text(gfm_cache.cache_data -> 'uids')::uuid
                FROM gap_filling_modules_cache gfm_cache
                WHERE gfm_name = 'InventoryConnectorGapFillingFactory'
                AND cache_key = 'pruned_nodes_to_cache'
            );
            """
        )
        try:
            edges = []

            for record in records:
                edge: Edge = self._convert_pg_record_to_edge(record)
                edges.append(edge)
            return edges
        except TypeError:
            return []

    async def get_lca_links(self, conn: Optional[asyncpg.connection.Connection] = None) -> list[Edge]:
        if conn is None:
            conn = self.pool
        records = await conn.fetch("SELECT * FROM edge WHERE edge_type = 'lca_link'")
        try:
            edges = []

            for record in records:
                edge: Edge = self._convert_pg_record_to_edge(record)
                edges.append(edge)
            return edges
        except TypeError:
            return []

    async def get_nodes_by_access_group_uid(
        self,
        access_group_uid: str,
        node_type: Optional[str] = None,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> list[Node]:
        if conn is None:
            conn = self.pool

        query = "SELECT * FROM node WHERE access_group_uid = $1"
        args = [access_group_uid]
        if node_type:
            query += " AND node_type = $2"
            args.append(node_type)

        records = await conn.fetch(query, *args)
        try:
            nodes = []
            for record in records:
                node = self._convert_pg_record_to_node(record)
                node.is_persistent = True
                nodes.append(node)
            logger.debug("got nodes from db.", node=nodes)
            return nodes
        except TypeError:
            return []

    async def get_nodes_by_access_group_uids(
        self,
        access_group_uids: List[str],
        node_type: Optional[str] = None,
        start_date: Optional[date] = None,
        end_date: Optional[date] = None,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> list[Node]:
        if conn is None:
            conn = self.pool

        args = []
        query = """
                SELECT * FROM node n
                WHERE n.access_group_uid = ANY($1)
                """

        if node_type:
            args.append(node_type)
            query += f" AND n.node_type = ${len(args) + 1}"
        if start_date:
            args.append(start_date.strftime("%Y-%m-%d"))
            query += f" AND n.data ->> 'activity_date' >= ${len(args) + 1}"
        if end_date:
            args.append(end_date.strftime("%Y-%m-%d"))
            query += f" AND n.data ->> 'activity_date' <= ${len(args) + 1}"

        if args:
            records = await conn.fetch(query, access_group_uids, *args)
        else:
            records = await conn.fetch(query, access_group_uids)
        try:
            nodes = []
            for record in records:
                node = self._convert_pg_record_to_node(record)
                node.is_persistent = True
                nodes.append(node)
            logger.debug("got nodes from db.", node=nodes)
            return nodes
        except TypeError:
            return []

    async def get_all_root_node_cache_info_by_time_interval_and_access_group_uids(
        self,
        access_group_uids: List[str],
        start_date: date,
        end_date: date,
        node_type: Optional[str] = None,
        ignore_deleted: bool = False,
        get_tabular_cache: bool = False,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> List[NodeCacheInfo]:
        if conn is None:
            conn = self.pool

        args = [start_date.strftime("%Y-%m-%d"), end_date.strftime("%Y-%m-%d")]
        query = """
                SELECT
                    pn.uid::UUID AS root_flow_uid,
                    n.uid::UUID AS uid,
                    (n.data ->> 'recipe_portions')::INTEGER AS recipe_portions,
                    (n.data ->> 'production_portions')::INTEGER AS production_portions,
                    (n.data ->> 'sold_portions')::INTEGER AS sold_portions,
                    n.access_group_uid::UUID AS access_group_uid,
                    (pn.post_calc_cache->'node' IS NOT NULL) AND
                        (n.post_calc_cache->'node' IS NOT NULL) AS cache_present,
                """
        if get_tabular_cache:
            query += """
                    pn.post_calc_cache->'final_graph_table' AS final_graph_table,
                    """

        query += f"""
                pn.post_calc_cache->'meta_information' AS pn_post_calc_data_meta_information,
                n.post_calc_cache->'meta_information' AS n_post_calc_data_meta_information
                FROM edge e
                JOIN node n ON n.uid = e.child_uid
                JOIN node pn ON pn.uid = e.parent_uid
                WHERE e.edge_type = '{EdgeTypeEnum.root_to_recipe.value}'
                AND n.access_group_uid = ANY($1)
                AND n.data ->> 'activity_date' BETWEEN $2 AND $3
                """
        if node_type:
            query += " AND n.node_type = $4"
            args.append(node_type)

        if not ignore_deleted:
            query += " AND n.deleted_at IS NULL"

        records = await conn.fetch(query, access_group_uids, *args)
        return [
            NodeCacheInfo(
                uid=UUID(int=record["uid"].int),
                root_flow_uid=UUID(int=record["root_flow_uid"].int),
                recipe_portions=record["recipe_portions"],
                sold_portions=record["sold_portions"],
                production_portions=record["production_portions"],
                access_group_uid=UUID(int=record["access_group_uid"].int) if record.get("access_group_uid") else None,
                cache_present=record["cache_present"],
                final_graph_table=record.get("final_graph_table"),
                root_flow_post_calc_data_meta_information=(
                    PostCalcDataMetaInformation(**record["pn_post_calc_data_meta_information"])
                    if record.get("pn_post_calc_data_meta_information")
                    else None
                ),
                post_calc_data_meta_information=(
                    PostCalcDataMetaInformation(**record["n_post_calc_data_meta_information"])
                    if record.get("n_post_calc_data_meta_information")
                    else None
                ),
            )
            for record in records
        ]

    async def get_parents_by_uid(self, uid: UUID, conn: Optional[asyncpg.connection.Connection] = None) -> list[Edge]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
                SELECT child_uid, parent_uid, edge_type
                FROM edge
                WHERE edge.child_uid = $1
            """,
            uid,
        )

        return [self._convert_pg_record_to_edge(record) for record in records]

    async def get_sub_graph_by_uid(
        self, uid: UUID, max_depth: int = 10, conn: Optional[asyncpg.connection.Connection] = None
    ) -> list[Edge]:
        if conn is None:
            conn = self.pool

        records = await conn.fetch(
            """
            WITH RECURSIVE cte (child_uid, parent_uid, edge_type, depth) AS (
                    SELECT child_uid, parent_uid, edge_type, 1
                    FROM edge
                    WHERE edge.parent_uid = $1
                UNION ALL
                    SELECT edge.child_uid, edge.parent_uid, edge.edge_type, depth+1
                    FROM edge
                    JOIN cte r
                    ON edge.parent_uid = r.child_uid
                    WHERE depth < $2
            )
            SELECT * FROM cte;
            """,
            uid,
            max_depth,
        )

        return [self._convert_pg_record_to_edge(record) for record in records]

    async def get_infinite_depth_sub_graph_by_uid(self, uid: UUID) -> list[Edge]:
        async with self.pool.acquire() as conn:
            records = await conn.fetch(
                """
                WITH RECURSIVE cte (child_uid, parent_uid, edge_type) AS (
                        SELECT child_uid, parent_uid, edge_type
                        FROM edge
                        WHERE edge.parent_uid = $1
                    UNION ALL
                        SELECT edge.child_uid, edge.parent_uid, edge.edge_type
                        FROM edge
                        JOIN cte r
                        ON edge.parent_uid = r.child_uid
                )
                SELECT DISTINCT * FROM cte;
                """,
                uid,
            )

        return [self._convert_pg_record_to_edge(record) for record in records]

    async def find_many_by_uids(
        self,
        uids: list[UUID],
        exclude_node_types: list[str] = None,
        included_deleted_nodes: bool = False,
        include_post_calc_data: bool = False,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> dict[UUID, PreAndPostCalcNodes]:
        mapping_dict = {}

        if conn is None:
            conn = self.pool

        chunk_size = 30_000  # Avoid issues due to overflowing argument numbers.
        for i in range(0, len(uids), chunk_size):
            chunk = uids[i : i + chunk_size]

            if not include_post_calc_data:
                query = """
                SELECT uid, access_group_uid, node_type, data, created_at, updated_at, deleted_at, updated_by
                FROM node n
                WHERE n.uid IN {}"""
            else:
                query = """
                SELECT * FROM node n
                WHERE n.uid IN {}"""

            # Generate placeholders for the chunk
            where_clauses = ", ".join("${}".format(i + 1) for i in range(len(chunk)))
            where_clauses = f"({where_clauses.strip()})"
            if exclude_node_types:
                for exclude_node_type in exclude_node_types:
                    where_clauses = f"{where_clauses} AND node_type <> '{exclude_node_type}'"

            # Concatenate the placeholders into the query
            query_formatted = query.format(where_clauses)

            # Execute the query with the chunk as parameters
            records = await conn.fetch(query_formatted, *chunk)
            for record in records:
                if not (record is None or (record.get("deleted_at") and not included_deleted_nodes)):
                    if not include_post_calc_data:
                        node = self._convert_pg_record_to_node(record)
                        node.is_persistent = True
                        mapping_dict[node.uid] = PreAndPostCalcNodes(pre_calc=node)
                    else:
                        pre_and_post_calc_nodes = self._convert_pg_record_to_node(
                            record, include_post_calc_data=include_post_calc_data
                        )
                        for node in (pre_and_post_calc_nodes.pre_calc, pre_and_post_calc_nodes.post_calc):
                            if node:
                                node.is_persistent = True
                        mapping_dict[pre_and_post_calc_nodes.pre_calc.uid] = pre_and_post_calc_nodes
        return mapping_dict

    async def find_by_uid(
        self,
        uid: UUID,
        conn: Optional[asyncpg.connection.Connection] = None,
        lock_for_update: bool = False,
        include_deleted_nodes: bool = False,
    ) -> Optional[Node]:
        if conn is None:
            conn = self.pool

        query = "SELECT * FROM node WHERE uid = $1"
        if lock_for_update:
            query += " FOR UPDATE"

        record = await conn.fetchrow(query, uid)

        if record is None or (record.get("deleted_at") and not include_deleted_nodes):
            # node does not exist
            return None
        try:
            node = self._convert_pg_record_to_node(record)
            node.is_persistent = True
            logger.debug("got node from db.", node=node)
            return node
        except TypeError as e:
            logger.exception("exception while getting node from db.", e=e)
            return None

    async def upsert_recipe(
        self, recipe_node: Node, transient: bool = False
    ) -> tuple[list[EdgeToUpsert], list[EdgeToUpsert], str, Optional[str], bool, Optional[UUID]]:
        start_upsert_recipe_ns = time.perf_counter_ns()

        recipe_dict = self._convert_node_to_pg_dict(recipe_node)

        sub_node_jsonb_params = []
        for sub_node in recipe_node.get_sub_nodes():
            sub_node_dict = self._convert_node_to_pg_dict(sub_node)
            sub_node_jsonb_params.append(
                [
                    str(sub_node_dict["uid"]) if sub_node_dict["uid"] else None,
                    str(sub_node_dict["access_group_uid"]) if sub_node_dict["access_group_uid"] else None,
                    sub_node_dict["node_type"],
                    sub_node_dict["data"],
                ]
            )

        parent_node = recipe_node.get_parent_nodes()[0] if recipe_node.get_parent_nodes() else None
        parent_dict = self._convert_node_to_pg_dict(parent_node) if parent_node else {}

        # Calculate the md5 hash of the recipe to check if it has changed:
        to_hash = [
            transient,
            str(recipe_dict["uid"]),
            str(recipe_dict["access_group_uid"]),
            recipe_dict["node_type"],
            recipe_dict["data"],
            [str(sub[1]) for sub in sub_node_jsonb_params],
            [sub[2:] for sub in sub_node_jsonb_params],
            str(parent_dict["uid"]) if parent_node else None,
            str(parent_dict["access_group_uid"]) if parent_node else None,
            parent_dict["node_type"] if parent_node else None,
            parent_dict["data"] if parent_node else None,
            EnvProps().UNMATCH_BASED_ON_INGREDIENTS_DECLARATION,
        ]
        md5_hash = UUID(hashlib.md5(json.dumps(to_hash).encode()).hexdigest())
        existing_node = await self.pool.fetchrow(
            "SELECT md5_hash, deleted_at IS NOT NULL AS is_deleted FROM node WHERE uid = $1", recipe_dict["uid"]
        )
        existing_md5_hash = None
        is_deleted = False
        if existing_node is not None:
            existing_md5_hash = existing_node["md5_hash"]
            is_deleted = existing_node["is_deleted"]

        if existing_md5_hash == md5_hash and transient == is_deleted:
            # skip upserting if the recipe has not changed:
            parent_uid = str(parent_dict.get("uid")) if parent_dict and parent_dict.get("uid") else None
            logger.info("skipping upserting recipe", recipe_uid=str(recipe_dict["uid"]))
            return ([], [], str(recipe_dict["uid"]), parent_uid, True, None)
        else:
            logger.info("starting upsert recipe", recipe_uid=str(recipe_dict["uid"]))

        # Upsert the recipe:
        async with self.pool.acquire() as conn:
            if self.analyze:
                await conn.execute("LOAD 'auto_explain'")
                await conn.execute("SET auto_explain.log_min_duration = 100")
                await conn.execute("SET auto_explain.log_analyze = true")
                await conn.execute("SET auto_explain.log_parameter_max_length = 30")
                await conn.execute("SET auto_explain.log_nested_statements = true")

            record = await conn.fetchrow(
                "SELECT upsert_recipe_with_lock($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)",
                transient,
                recipe_dict["uid"],
                recipe_dict["access_group_uid"],
                recipe_dict["node_type"],
                recipe_dict["data"],
                sub_node_jsonb_params,
                parent_dict["uid"] if parent_node else None,
                parent_dict["access_group_uid"] if parent_node else None,
                parent_dict["node_type"] if parent_node else None,
                parent_dict["data"] if parent_node else None,
                EnvProps().UNMATCH_BASED_ON_INGREDIENTS_DECLARATION,
                md5_hash,
            )

        record_json = record["upsert_recipe_with_lock"]

        dur_upsert_recipe_sec = (time.perf_counter_ns() - start_upsert_recipe_ns) / 1e9
        logger.info(f"Upserted recipe {recipe_node}.", duration_sec=dur_upsert_recipe_sec)

        edges_to_insert = record_json["edges_to_insert"]
        edges_to_insert_typed = []
        if edges_to_insert:
            for edge_to_insert in edges_to_insert:
                if edge_to_insert["parent_uid"] and edge_to_insert["child_xid_or_uid"]:
                    child_xid_or_uid = edge_to_insert["child_xid_or_uid"]
                    if "uid" in child_xid_or_uid:
                        edges_to_insert_typed.append(
                            EdgeToUpsert(
                                parent_uid=UUID(edge_to_insert["parent_uid"]),
                                child_uid=UUID(child_xid_or_uid["uid"]),
                            )
                        )
                    else:
                        child_xid_access_group_uid = XidAccessGroupUid(
                            xid=child_xid_or_uid["xid"],
                            access_group_uid=UUID(child_xid_or_uid["access_group_uid"]),
                        )
                        edges_to_insert_typed.append(
                            EdgeToUpsert(
                                parent_uid=UUID(edge_to_insert["parent_uid"]),
                                child_xid_access_group_uid=child_xid_access_group_uid,
                            )
                        )

        edges_to_delete_typed = (
            [
                EdgeToUpsert(parent_uid=edge["parent_uid"], child_uid=edge["child_uid"])
                for edge in record_json["edges_to_delete"]
            ]
            if record_json["edges_to_delete"]
            else []
        )
        return (
            edges_to_delete_typed,
            edges_to_insert_typed,
            record_json["self_uid"],
            record_json["parent_uid"],
            False,
            md5_hash,
        )

    async def get_root_flow_or_activity(
        self, uid: UUID, conn: Optional[asyncpg.connection.Connection] = None, allow_deleted: bool = False
    ) -> Optional[Node]:
        if conn is None:
            conn = self.pool

        query = """
        SELECT $2 AS relationship_type, '' AS edge_type, n.*
        FROM node n
        WHERE n.uid = $1

        UNION ALL

        SELECT $3 AS relationship_type, e1.edge_type AS edge_type, p.*
        FROM node n
        JOIN edge e1 ON n.uid = e1.child_uid
        JOIN node p ON e1.parent_uid = p.uid
        WHERE n.uid = $1

        UNION ALL

        SELECT $4 AS relationship_type, e2.edge_type AS edge_type, c.*
        FROM node n
        JOIN edge e2 ON n.uid = e2.parent_uid
        JOIN node c ON e2.child_uid = c.uid
        WHERE n.uid = $1;
        """

        records = await conn.fetch(query, uid, RelationTypeEnum.self, RelationTypeEnum.parent, RelationTypeEnum.child)

        if records is None:
            return

        self_node = None
        parent_nodes = []
        child_nodes = []

        try:
            for record in records:
                if allow_deleted or not record.get("deleted_at"):
                    node_from_record = self._convert_pg_record_to_node(record)
                    node_from_record.is_persistent = True
                    if record.get("relationship_type") == RelationTypeEnum.self:
                        self_node = node_from_record
                    elif (
                        record.get("relationship_type") == RelationTypeEnum.parent
                        and record.get("edge_type") == EdgeTypeEnum.root_to_recipe
                    ):
                        parent_nodes.append(node_from_record)
                    elif (
                        record.get("relationship_type") == RelationTypeEnum.child
                        and record.get("edge_type") == EdgeTypeEnum.root_to_recipe
                    ):
                        child_nodes.append(node_from_record)
        except TypeError as e:
            logger.exception("exception while getting node from db.", e=e)
            return

        if not self_node:
            return

        for parent_node in parent_nodes:
            self_node.add_parent_node(parent_node)
        for child_node in child_nodes:
            self_node.add_sub_node(child_node)

        if isinstance(self_node, FlowNode) and (not self_node.get_sub_nodes()):
            logger.debug("The flow node is not a root flow as it does not have its child activity.")
            return

        return self_node

    async def find_access_group_uid_by_uid(
        self,
        uid: UUID,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> Optional[str]:
        if conn is None:
            conn = self.pool

        query = "SELECT access_group_uid FROM node WHERE uid = $1"

        record = await conn.fetchrow(query, uid)

        if record is None:
            # node does not exist
            return None
        try:
            if access_group_uid := record["access_group_uid"]:
                return str(access_group_uid)
            else:
                raise TypeError("access_group_uid is set to null.")
        except (TypeError, KeyError) as e:
            logger.exception("exception while getting node from db.", e=e)
            raise e

    # TODO: unused for now
    async def find_by_data(self, query: str) -> Optional[Node]:
        """:param query: a SQL query on the JSONB 'data' field, e.g. '{"raw_input":{"name": "carrot"}}'."""
        record = await self.pool.fetchrow(f"SELECT * FROM node WHERE data @> '{query}'")
        try:
            node = self._convert_pg_record_to_node(record)
            node.is_persistent = True
            logger.debug("got node from db.", node=node)
            return node
        except TypeError:
            return None

    async def upsert_node_by_uid(
        self, node: Node, conn: Optional[asyncpg.connection.Connection] = None, set_as_deleted: bool = False
    ) -> Node:
        if node.uid is None:
            node.uid = uuid4()

        if conn is None:
            conn = self.pool

        node_dict = self._convert_node_to_pg_dict(node)
        if set_as_deleted:
            query = """
            INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at)
            VALUES($1, $2, $3, $4::jsonb, NOW(), NOW())
            ON CONFLICT (uid)
            DO UPDATE SET node_type = $3, data = $4::jsonb, updated_at = NOW(), deleted_at = NOW();
            """
        else:
            query = """
            INSERT INTO node(uid, access_group_uid, node_type, data, updated_at, deleted_at)
            VALUES($1, $2, $3, $4::jsonb, NOW(), NULL)
            ON CONFLICT (uid)
            DO UPDATE SET node_type = $3, data = $4::jsonb, updated_at = NOW(), deleted_at = NULL;
            """

        await conn.execute(
            query,
            node_dict["uid"],
            node_dict["access_group_uid"],
            node_dict["node_type"],
            node_dict["data"],
        )
        if not node.is_persistent:
            node.is_persistent = True
        logger.debug("saved node in db.", node_uid=node.uid)

        return node

    async def add_post_calc_data_to_node_by_uid(
        self,
        uid: UUID,
        post_calc_cache: PostCalcCache,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> bool:
        if conn is None:
            conn = self.pool

        query = """
        WITH updated AS (
            UPDATE node
            SET post_calc_cache = $2::jsonb
            WHERE uid = $1
            RETURNING uid
        )
        SELECT COUNT(*) AS updated_rows FROM updated;
        """
        result = await conn.fetchval(query, str(uid), self._convert_post_calc_cache_to_pg_dict(post_calc_cache))

        if result > 0:
            return True
        else:
            return False

    async def update_node_access_group_uid(
        self,
        node_uid: UUID,
        access_group_uid: UUID,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> tuple[Optional[str], Optional[dict]]:
        if conn is None:
            conn = self.pool

        results = await conn.fetchrow(
            """
            UPDATE node
            SET access_group_uid = $2, updated_at = NOW()
            WHERE uid = $1
            RETURNING node_type, data;
            """,
            node_uid,
            access_group_uid,
        )
        logger.debug("updated access_group_uid of node in db.", node_uid=node_uid)
        if results:
            return results.get("node_type"), results.get("data")
        else:
            return None, None

    async def update_node_prop(
        self,
        node_uid: UUID,
        prop_name: str,
        prop: Prop | list[Prop] | int,
        append: bool = False,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> tuple[Optional[str], Optional[dict]]:
        if conn is None:
            conn = self.pool

        if isinstance(prop, int):
            prop_json = prop
        elif isinstance(prop, (DeepListView, list)):
            prop_json = [p.model_dump() for p in prop]
        else:
            prop_json = prop.model_dump()
        if append:
            if not isinstance(prop_json, list):
                prop_json = [prop_json]
            results = await conn.fetchrow(
                """
                UPDATE node
                SET data = jsonb_set(
                    data,
                    ('{'||$2||'}')::text[],
                    COALESCE(
                        data->($2::text) || $3::jsonb,
                        $3::jsonb
                    ),
                    true
                ),
                updated_at = NOW()
                WHERE uid = $1
                RETURNING node_type, data;
                """,
                node_uid,
                prop_name,
                prop_json,
            )
            logger.debug("appended prop in db.", node_uid=node_uid)
            if results:
                return results.get("node_type"), results.get("data")
            else:
                return None, None
        else:
            results = await conn.fetchrow(
                """
                UPDATE node
                SET data = jsonb_set(data, ('{'||$2||'}')::text[], $3::jsonb), updated_at = NOW()
                WHERE uid = $1
                RETURNING node_type, data;
                """,
                node_uid,
                prop_name,
                prop_json,
            )
            logger.debug("updated prop of node in db.", node_uid=node_uid)
            if results:
                return results.get("node_type"), results.get("data")
            else:
                return None, None

    async def update_node_props(
        self,
        node: Node,
        conn: Optional[asyncpg.connection.Connection] = None,
    ) -> bool:
        """Update node properties that are set in the input node."""
        if conn is None:
            conn = self.pool

        node_json = self._data_dict_from_node(node)
        results = await conn.fetchrow(
            """
            UPDATE node
            SET data = COALESCE(data, '{}'::jsonb) || $2::jsonb, updated_at = NOW()
            WHERE uid = $1 AND node_type = $3
            RETURNING uid;
            """,
            node.uid,
            node_json,
            node.node_type,
        )
        logger.debug("updated props of node in db.", node_uid=node.uid, node_type=node.node_type)
        return results is not None

    async def add_nodes(self, nodes: list[Node]) -> None:
        """(faster) Bulk insert."""
        collected_values = []
        for node in nodes:
            if node.uid is None:
                node.uid = uuid4()
            node_dict = self._convert_node_to_pg_dict(node)
            collected_values.append(
                (
                    node_dict["uid"],
                    node_dict["access_group_uid"],
                    node_dict["node_type"],
                    node_dict["data"],
                )
            )

        await self.pool.executemany(
            """
            INSERT INTO node(uid, access_group_uid, node_type, data)
            VALUES($1, $2, $3, $4::jsonb)
            ON CONFLICT (uid)
            DO UPDATE SET node_type = $3, data = $4::jsonb, updated_at = NOW();
            """,
            collected_values,
        )

    async def add_edge(self, edge: Edge, conn: Optional[asyncpg.connection.Connection] = None) -> Edge:
        if conn is None:
            conn = self.pool

        if edge.parent_uid is None:
            raise ValueError("need parent_uid")
        if edge.child_uid is None:
            raise ValueError("need child_uid")
        if edge.edge_type is None:
            raise ValueError("need edge_type")

        await conn.execute(
            """
            INSERT INTO edge(parent_uid, child_uid, edge_type, updated_at)
            VALUES($1, $2, $3, NOW())
            ON CONFLICT (parent_uid, child_uid, edge_type)
            DO NOTHING;
            """,
            edge.parent_uid,
            edge.child_uid,
            edge.edge_type.name,
        )
        logger.debug("saved edge in db.", edge=edge)
        return edge

    async def add_edges(self, edges: list[Edge], conn: Optional[asyncpg.connection.Connection] = None) -> None:
        """(faster) Bulk insert."""
        collected_values = []
        for edge in edges:
            if edge.parent_uid is None:
                raise ValueError("need parent_uid")
            if edge.child_uid is None:
                raise ValueError("need child_uid")
            if edge.edge_type is None:
                raise ValueError("need edge_type")
            collected_values.append((edge.parent_uid, edge.child_uid, edge.edge_type.name))

        if conn is None:
            conn = self.pool

        await conn.executemany(
            """
            INSERT INTO edge(parent_uid, child_uid, edge_type)
            VALUES($1, $2, $3)
            ON CONFLICT (parent_uid, child_uid, edge_type)
            DO NOTHING;
            """,
            collected_values,
        )

    async def delete_node_and_edges(self, uid: str, conn: Optional[asyncpg.connection.Connection] = None) -> bool:
        if conn is None:
            conn = self.pool

        result_rows = await conn.execute(
            # the following is using on delete cascade, so will also do:
            # DELETE FROM edge WHERE parent_uid = $1 OR child_uid = $1;
            """
            DELETE FROM node
            WHERE uid IN (
                SELECT uid FROM node WHERE uid = $1
                UNION
                SELECT e.child_uid
                FROM node parent
                JOIN edge e ON e.parent_uid = parent.uid
                WHERE parent.uid = $1 AND parent.node_type = 'LinkingActivityNode'
            );
            """,
            uid,
        )

        # checking if DELETE operation finished successfully
        if result_rows.startswith("DELETE "):
            logger.info("deleted node in db.", uid=uid)
            return True
        else:
            return False

    async def delete_many_nodes_and_edges(self, uids: list[str]) -> None:
        await self.pool.executemany(
            # the following is using on delete cascade, so will also do:
            # DELETE FROM edge WHERE parent_uid = $1 OR child_uid = $1;
            """
            DELETE FROM node WHERE uid = $1;
            """,
            [(uid,) for uid in uids],
        )

    async def delete_then_insert_many_edges(
        self,
        edges_to_delete: list[Edge],
        edges_to_insert: list[Edge],
        lock_on_uid: Optional[str],
        md5_hash: Optional[UUID],
    ) -> None:
        async with self.pool.acquire() as conn:
            async with conn.transaction():
                if lock_on_uid is not None:
                    await conn.execute("SELECT pg_advisory_xact_lock(hashtext($1))", lock_on_uid)

                await conn.executemany(
                    """
                    WITH all_edges (parent_uid, child_uid, edge_type) AS (
                        VALUES ($1::uuid, $2::uuid, $3::TEXT)
                    ),
                    deleted_edges AS (
                        DELETE FROM edge e USING all_edges ae
                        WHERE e.parent_uid = ae.parent_uid
                        AND e.child_uid = ae.child_uid
                        AND ae.edge_type = 'to_delete'
                    )
                    INSERT INTO edge(parent_uid, child_uid, edge_type)
                    SELECT ae.parent_uid, ae.child_uid, ae.edge_type FROM all_edges ae
                    WHERE ae.edge_type <> 'to_delete'
                    ON CONFLICT (parent_uid, child_uid, edge_type)
                    DO NOTHING
                    """,
                    [
                        (edge_to_delete.parent_uid, edge_to_delete.child_uid, "to_delete")
                        for edge_to_delete in edges_to_delete
                    ]
                    + [
                        (edge_to_insert.parent_uid, edge_to_insert.child_uid, edge_to_insert.edge_type)
                        for edge_to_insert in edges_to_insert
                    ],
                )

                if md5_hash is not None:
                    await conn.execute("UPDATE node SET md5_hash = $1 WHERE uid = $2", md5_hash, lock_on_uid)

    async def delete_many_edges(self, edges_to_delete: list[Edge], lock_on_uid: Optional[str]) -> None:
        async with self.pool.acquire() as conn:
            async with conn.transaction():
                if lock_on_uid is not None:
                    await conn.execute("SELECT pg_advisory_xact_lock(hashtext($1))", lock_on_uid)

                await conn.executemany(
                    """
                    WITH edges_to_delete (parent_uid, child_uid) AS (
                        VALUES ($1::uuid, $2::uuid)
                    )
                    DELETE FROM edge e USING edges_to_delete
                    WHERE e.parent_uid = edges_to_delete.parent_uid
                    AND e.child_uid = edges_to_delete.child_uid
                    """,
                    [
                        (
                            edge_to_delete.parent_uid,
                            edge_to_delete.child_uid,
                        )
                        for edge_to_delete in edges_to_delete
                    ],
                )

    async def mark_recipe_as_deleted(self, recipe_uid: UUID) -> None:
        await self.pool.execute("SELECT mark_recipe_as_deleted_with_lock($1)", recipe_uid)

    async def clear_mock_legacy_co2_nodes(self, conn: Optional[asyncpg.connection.Connection] = None) -> list[UUID]:
        if conn is None:
            conn = self.pool

        results = await conn.fetch(
            """
            WITH deleted_node_uids AS (
                DELETE FROM node
                USING namespace_xid_mappings
                WHERE node.uid = namespace_xid_mappings.uid
                AND namespace_xid_mappings.xid like '%_mock_legacy_%'
                AND node.node_type = 'ModeledActivityNode'
                RETURNING node.uid
            ),
            deleted_namespace_xids AS (
                DELETE FROM namespace_xid_mappings
                WHERE uid IN (SELECT uid FROM deleted_node_uids)
                RETURNING namespace_xid_mappings.uid
            ),
            updated_node_uids AS (
                UPDATE node
                SET data = jsonb_set(
                              data,
                              '{raw_input}',
                              (data -> 'raw_input') - 'mocked_legacy_co2'
                          ) - 'aggregated_cache'
                WHERE node.node_type = 'ModeledActivityNode'
                AND (data -> 'raw_input' ->> 'mocked_legacy_co2')::boolean = true
                RETURNING node.uid
            )
            SELECT uid FROM deleted_node_uids
            UNION ALL
            SELECT uid FROM updated_node_uids
            """
        )
        return [UUID(int=result.get("uid").int) for result in results]

    async def seed_sample_processing_data(
        self, methane_emission_uid: UUID, country_codes_for_additional_low_voltage_electricity_processes: list[str]
    ) -> tuple[
        ModeledActivityNode,
        ModeledActivityNode,
        ModeledActivityNode,
        ModeledActivityNode,
        ModeledActivityNode,
        list[ModeledActivityNode],
    ]:
        """Seed one sample processing Brightway node each for region specific and region unspecified processing."""
        from core.tests.test_processing_gfm import (
            EXPECTED_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY,
            EXPECTED_OTHER_AMOUNT,
            METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC,
            METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_GLO,
            METHANE_EMISSION_UNIT_PROCESSING,
            RAW_MATERIAL_AMOUNT,
            RAW_MATERIAL_AMOUNT_EXAMPLE_WITH_ELECTRICITY,
        )

        nodes_to_upsert: list[ModeledActivityNode | FlowNode] = []
        edges_to_upsert = []

        def add_flow(
            parent_uid: UUID,
            child_uid: UUID,
            amount: float,
            unit: str,
            node_type: Type[FlowNode],
        ) -> None:
            flow_node = node_type(uid=uuid4(), amount_in_original_source_unit=RawQuantity(value=amount, unit=unit))
            nodes_to_upsert.append(flow_node)
            edges_to_upsert.append(
                Edge(
                    parent_uid=parent_uid,
                    child_uid=flow_node.uid,
                    edge_type=EdgeTypeEnum.lca_link,
                )
            )
            edges_to_upsert.append(
                Edge(
                    parent_uid=flow_node.uid,
                    child_uid=child_uid,
                    edge_type=EdgeTypeEnum.lca_link,
                )
            )

        region_unspecified_example_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            name="average drying process",
            database="EDB",
        )
        nodes_to_upsert.append(region_unspecified_example_process)

        region_unspecified_with_raw_material_example_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            name="bread baking",
            database="EDB",
        )
        nodes_to_upsert.append(region_unspecified_with_raw_material_example_process)

        region_specific_example_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            name="milk processing, to yoghurt",
            database="EDB",
        )
        nodes_to_upsert.append(region_specific_example_process)

        raw_material_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilogram"},
            name="Raw material",
            database="EDB",
        )
        nodes_to_upsert.append(raw_material_process)

        low_voltage_electricity_process = ModeledActivityNode(
            uid=uuid4(),
            activity_location="GLO",
            production_amount={"value": 1.0, "unit": "kilowatt hour"},
            name="market group for electricity, low voltage",
            database="ecoinvent 3.6 cutoff",
        )
        nodes_to_upsert.append(low_voltage_electricity_process)

        additional_low_voltage_electricity_processes: list[ModeledActivityNode] = []
        for country_code in country_codes_for_additional_low_voltage_electricity_processes:
            country_specific_electricity_mix = ModeledActivityNode(
                uid=uuid4(),
                activity_location=country_code,
                production_amount={"value": 1.0, "unit": "kilowatt hour"},
                name="market for electricity, low voltage",
                database="ecoinvent 3.6 cutoff",
            )
            nodes_to_upsert.append(country_specific_electricity_mix)
            additional_low_voltage_electricity_processes.append(country_specific_electricity_mix)

        add_flow(
            region_unspecified_with_raw_material_example_process.uid,
            methane_emission_uid,
            METHANE_EMISSION_UNIT_PROCESSING,
            "kilogram",
            FlowNode,
        )
        add_flow(
            region_unspecified_with_raw_material_example_process.uid,
            raw_material_process.uid,
            RAW_MATERIAL_AMOUNT,
            "kilogram",
            FlowNode,
        )
        add_flow(
            region_unspecified_example_process.uid,
            methane_emission_uid,
            METHANE_EMISSION_UNIT_PROCESSING,
            "kilogram",
            FlowNode,
        )
        add_flow(
            low_voltage_electricity_process.uid,
            methane_emission_uid,
            METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_GLO,
            "kilogram",
            FlowNode,
        )
        for country_specific_electricity_mix in additional_low_voltage_electricity_processes:
            add_flow(
                country_specific_electricity_mix.uid,
                methane_emission_uid,
                METHANE_EMISSION_UNIT_ELECTRICITY_LOW_VOLTAGE_COUNTRY_SPECIFIC,
                "kilogram",
                FlowNode,
            )
        add_flow(
            region_specific_example_process.uid,
            region_unspecified_example_process.uid,
            EXPECTED_OTHER_AMOUNT,
            "kilogram",
            FlowNode,
        )
        add_flow(
            region_specific_example_process.uid,
            low_voltage_electricity_process.uid,
            EXPECTED_ELECTRICITY_EXAMPLE_WITH_ELECTRICITY,
            "kilogram",
            FlowNode,
        )
        add_flow(
            region_specific_example_process.uid,
            raw_material_process.uid,
            RAW_MATERIAL_AMOUNT_EXAMPLE_WITH_ELECTRICITY,
            "kilogram",
            FlowNode,
        )

        await self.add_nodes(nodes_to_upsert)
        await self.add_edges(edges_to_upsert)

        return (
            region_unspecified_example_process,
            region_unspecified_with_raw_material_example_process,
            region_specific_example_process,
            raw_material_process,
            low_voltage_electricity_process,
            additional_low_voltage_electricity_processes,
        )
